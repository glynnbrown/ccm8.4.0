#region Header Information

// Copyright � Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31763 : A.Silva
//  Created.
// V8-31819 : A.Silva
//  Updated to use the PlanogramComparison model objects.
// V8-31820 : A.Silva
//  Added DateLastCompared property.
// V8-31913 : A.Silva
//  Added IgnoreNonPlacedProducts property.
// V8-31946 : A.Silva
//  Amended ApplyCommand_CanExecute() so that it will not be enabled when there are no other planograms to compare selected.
// V8-31960 : A.Silva
//  Removed code related to managing the available and selected planograms.
// V8-32733 : A.Silva
//  Added loading user's default comparison template if there is one.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Windows;
using Galleria.Ccm.Common.Wpf;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using CommonMessage = Galleria.Ccm.Common.Wpf.Resources.Language.Message;
using CommonImageResources = Galleria.Ccm.Common.Wpf.Resources.ImageResources;
using PlanComparison = Galleria.Framework.Planograms.Model.PlanogramComparison;
using FrameworkHelper = Galleria.Framework.Controls.Wpf.Helpers;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.PlanogramComparison
{
    public sealed class PlanogramComparisonEditorViewModel : WindowViewModelBase
    {
        #region Constants

        private const String ExceptionCategory = "PlanogramComparisonEditor";

        #endregion

        #region Fields

        private Boolean _fieldPickerViewModelsInitialized;

        private Boolean _allowExecute = true;

        private readonly Boolean _hasRepositoryConnection;

        private readonly ModelPermission<PlanogramComparisonTemplate> _itemRepositoryPerms;

        #endregion

        #region Properties

        #region SelectedPlanogramComparisonTemplate

        public static readonly PropertyPath SelectedPlanogramComparisonTemplateProperty =
            GetPropertyPath<PlanogramComparisonEditorViewModel>(p => p.SelectedPlanogramComparisonTemplate);

        public PlanogramComparisonTemplate SelectedPlanogramComparisonTemplate
        {
            get { return _selectedPlanogramComparisonTemplate; }
            set
            {
                _selectedPlanogramComparisonTemplate = value;

                OnPropertyChanged(SelectedPlanogramComparisonTemplateProperty);
                OnSelectedTemplateChanged(value, SelectedPlanogramComparisonTemplate);
            }
        }

        #endregion

        #region PlanComparisonEdit

        /// <summary>
        ///     The model for editing the plan comparison.
        /// </summary>
        private IPlanogramComparisonSettings _planComparisonEdit = PlanogramComparisonTemplate.NewPlanogramComparisonTemplate(App.ViewState.EntityId);

        /// <summary>
        ///     Get/set the model for editing the plan comparison.
        /// </summary>
        public IPlanogramComparisonSettings PlanComparisonEdit
        {
            get { return _planComparisonEdit; }
            set
            {
                _planComparisonEdit = value;
                OnPropertyChanged(PlanComparisonEditProperty);
            }
        }

        /// <summary>
        ///     The <see cref="PropertyPath"/> for the <see cref="PlanComparisonEdit"/> property.
        /// </summary>
        public static readonly PropertyPath PlanComparisonEditProperty = GetPropertyPath(viewModel => viewModel.PlanComparisonEdit);

        #endregion

        #region IsInstance

        /// <summary>
        ///     Get whether the current <see cref="PlanComparisonEdit"/> is an instance in a planogram or a saved object..
        /// </summary>
        public Boolean IsInstance { get; private set; }

        /// <summary>
        ///     The <see cref="PropertyPath"/> for the <see cref="IsInstance"/> property.
        /// </summary>
        public static readonly PropertyPath IsInstanceProperty = GetPropertyPath(viewModel => viewModel.IsInstance);

        #endregion

        #region Dialog Result

        private Boolean? _dialogResult;

        /// <summary>
        ///     Gets or sets the result and closes the attached dialog.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;
                CloseDialog(value);
            }
        }

        #endregion

        #region Product Field Picker View Model

        private Ccm.Common.Wpf.Selectors.FieldPickerViewModel _productFieldPickerViewModel;

        public Ccm.Common.Wpf.Selectors.FieldPickerViewModel ProductFieldPickerViewModel { get { return _productFieldPickerViewModel; } }

        public static readonly PropertyPath ProductFieldPickerViewModelProperty = GetPropertyPath(o => o.ProductFieldPickerViewModel);

        #endregion

        #region Position Field Picker View Model

        private Ccm.Common.Wpf.Selectors.FieldPickerViewModel _positionFieldPickerViewModel;

        public Ccm.Common.Wpf.Selectors.FieldPickerViewModel PositionFieldPickerViewModel { get { return _positionFieldPickerViewModel; } }

        public static readonly PropertyPath PositionFieldPickerViewModelProperty = GetPropertyPath(o => o.PositionFieldPickerViewModel);

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of <see cref="PlanogramComparisonEditorViewModel"/>.
        /// </summary>
        public PlanogramComparisonEditorViewModel() : this(null, false) {}

        /// <summary>
        ///     Initializes a new instance of <see cref="PlanogramComparisonEditorViewModel"/>.
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="allowExecute"></param>
        public PlanogramComparisonEditorViewModel(IPlanogramComparisonSettings settings, Boolean allowExecute)
        {
            originalSettings = settings;

            //  Check repository connection.
            _hasRepositoryConnection = CCMClient.ViewState.EntityId > 0;

            //  Get permissions for current repository.
            _itemRepositoryPerms = _hasRepositoryConnection
                                       ? new ModelPermission<PlanogramComparisonTemplate>(PlanogramComparisonTemplate.GetUserPermissions())
                                       : ModelPermission<PlanogramComparisonTemplate>.DenyAll();

            InitializeModelEdit();
            if (!(IsInstance && allowExecute)) InitializeUiForEditOnly();
            InitializeFieldPickerViewModels();
        }

        #endregion

        #region Commands

        #region Open

        /// <summary>
        ///     Instance of the <see cref="RelayCommand"/> that opens a saved settings file or repository record.
        /// </summary>
        private RelayCommand _openCommand;

        /// <summary>
        ///     Opens from the repository or file based on the availability of the repository connection.
        /// </summary>
        public RelayCommand OpenCommand
        {
            get
            {
                if (_openCommand != null) return _openCommand;

                _openCommand =
                    new RelayCommand(p => OpenCommand_Executed())
                    {
                        FriendlyName = Message.Generic_Open,
                        SmallIcon = ImageResources.Open_16
                    };
                RegisterCommand(_openCommand);

                return _openCommand;
            }
        }

        /// <summary>
        ///     Invoked when the <see cref="OpenCommand"/> is executed.
        /// </summary>
        private void OpenCommand_Executed()
        {
            if (OpenFromRepositoryCommand.CanExecute())
            {
                OpenFromRepositoryCommand.Execute();
            }
            else
            {
                OpenFromFileCommand.Execute();
            }
        }

        #endregion

        #region OpenFromRepository

        /// <summary>
        ///     Instance of the <see cref="RelayCommand"/> that opens a saved settings Repository.
        /// </summary>
        private RelayCommand _openFromRepositoryCommand;

        /// <summary>
        ///     Allows the user to select a Repository template to load data from.
        /// </summary>
        public RelayCommand OpenFromRepositoryCommand
        {
            get
            {
                if (_openFromRepositoryCommand != null) return _openFromRepositoryCommand;

                _openFromRepositoryCommand =
                    new RelayCommand(OpenFromRepository_Executed, o => OpenFromRepository_CanExecute())
                    {
                        FriendlyName = CommonMessage.Generic_OpenFromRepository
                    };
                RegisterCommand(_openFromRepositoryCommand);

                return _openFromRepositoryCommand;
            }
        }

        private Boolean OpenFromRepository_CanExecute()
        {
            //  Check if there is a connection.
            if (!_hasRepositoryConnection)
            {
                OpenFromRepositoryCommand.DisabledReason = CommonMessage.Generic_NoRepositoryConnection;
                return false;
            }

            //  Check if the user has open permission.
            if (!_itemRepositoryPerms.CanFetch)
            {
                OpenFromRepositoryCommand.DisabledReason = CommonMessage.Generic_NoFetchPermission;
            }

            return true;
        }

        /// <summary>
        ///     Invoked when the <see cref="OpenFromRepositoryCommand"/> is executed.
        /// </summary>
        /// <param name="id"></param>
        private void OpenFromRepository_Executed(Object id)
        {
            //  Get the template id to load.
            if (id == null)
            {
                var window = new GenericSingleItemSelectorWindow
                             {
                                 ItemSource = PlanogramComparisonTemplateInfoList
                                     .FetchByEntityId(CCMClient.ViewState.EntityId).ToList(),
                                 SelectionMode = System.Windows.Controls.DataGridSelectionMode.Single
                             };
                GetWindowService().ShowDialog<GenericSingleItemSelectorWindow>(window);
                if (window.DialogResult != true) return;

                id = window.SelectedItems.Cast<PlanogramComparisonTemplateInfo>().First().Id;
            }

            //  Load the template.
            ShowWaitCursor(true);
            try
            {
                PlanComparisonEdit.Load(PlanogramComparisonTemplate.FetchById(id, true));
                SetComparisonFields();
            }
            catch (Exception ex)
            {
                ShowWaitCursor(false);
                RecordException(ex);
                GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                return;
            }

            ShowWaitCursor(false);
        }

        private PlanogramComparisonTemplate _selectedPlanogramComparisonTemplate;

        #endregion

        #region OpenFromFile

        /// <summary>
        ///     Instance of the <see cref="RelayCommand"/> that opens a saved settings file.
        /// </summary>
        private RelayCommand _openFromFileCommand;

        /// <summary>
        ///     Allows the user to select a file template to load data from.
        /// </summary>
        public RelayCommand OpenFromFileCommand
        {
            get
            {
                if (_openFromFileCommand != null) return _openFromFileCommand;

                _openFromFileCommand =
                    new RelayCommand(OpenFromFileCommand_Executed)
                    {
                        FriendlyName = CommonMessage.Generic_OpenFromFile
                    };
                RegisterCommand(_openFromFileCommand);

                return _openFromFileCommand;
            }
        }

        /// <summary>
        ///     Invoked when the <see cref="OpenFromFileCommand"/> is executed.
        /// </summary>
        private void OpenFromFileCommand_Executed(Object args)
        {
            String fileName;
            if (!GetWindowService().ShowOpenFileDialog(App.ViewState.GetSessionDirectory(SessionDirectory.PlanogramComparisonTemplate), 
                String.Format(CommonMessage.PlanogramComparisonFilePromptMask, PlanogramComparisonTemplate.FileExtension), 
                out fileName)) return;

            // Update the session directory.
            App.ViewState.SetSessionDirectory(SessionDirectory.PlanogramComparisonTemplate, Path.GetDirectoryName(fileName));

            ShowWaitCursor(true);
            try
            {
                //  Load the data from the file into this instance.
                PlanogramComparisonTemplate source = PlanogramComparisonTemplate.FetchByFilename(fileName, true);
                PlanComparisonEdit.Load(source);
                SetComparisonFields();

                //  Unlock the file as we are done with it.
                PlanogramComparisonTemplate.UnlockPlanogramComparisonTemplateByFileName((String)source.Id);
            }
            catch (Exception ex)
            {
                ShowWaitCursor(false);

                LocalHelper.RecordException(ex, ExceptionCategory);
                FrameworkHelper.ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                return;
            }

            ShowWaitCursor(false);
        }

        #endregion

        #region SaveAs

        /// <summary>
        ///     The command to save the current data to the default target (repository/file).
        /// </summary>
        private RelayCommand _saveAsCommand;

        /// <summary>
        ///     Get the command to save the current data to the default target (repository/file).
        /// </summary>
        /// <remarks>If the command has not been initialized yet, it does so before returning it.</remarks>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand != null) return _saveAsCommand;

                _saveAsCommand =
                    new RelayCommand(p => SaveAs_Executed(), p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        SmallIcon = ImageResources.SaveAs_16,
                        Icon = ImageResources.SaveAs_32
                    };
                RegisterCommand(_saveAsCommand);

                return _saveAsCommand;
            }
        }

        private Boolean SaveAs_CanExecute()
        {
            return true;
        }

        /// <summary>
        ///     Invoked when the <see cref="SaveAsCommand"/> is executed.
        /// </summary>
        private void SaveAs_Executed()
        {
            if (SaveAsToRepositoryCommand.CanExecute())
            {
                SaveAsToRepositoryCommand.Execute();
            }
            else
            {
                SaveAsToFileCommand.Execute();
            }
        }

        #endregion

        #region SaveToRepository

        /// <summary>
        ///     The command to save the current data to the repository.
        /// </summary>
        private RelayCommand _saveAsToRepositoryCommand;

        /// <summary>
        ///     Get the command to save the current data to the repository.
        /// </summary>
        /// <remarks>If the command has not been initialized yet, it does so before returning it.</remarks>
        public RelayCommand SaveAsToRepositoryCommand
        {
            get
            {
                if (_saveAsToRepositoryCommand != null) return _saveAsToRepositoryCommand;

                _saveAsToRepositoryCommand =
                    new RelayCommand(o => SaveAsToRepository_Executed(), o => SaveAsToRepository_CanExecute())
                    {
                        FriendlyName = CommonMessage.Generic_SaveToRepository
                    };
                RegisterCommand(_saveAsToRepositoryCommand);

                return _saveAsToRepositoryCommand;
            }
        }

        private Boolean SaveAsToRepository_CanExecute()
        {
            if (!_hasRepositoryConnection)
            {
                SaveAsToRepositoryCommand.DisabledReason = CommonMessage.Generic_NoRepositoryConnection;
                return false;
            }

            if (!_itemRepositoryPerms.CanCreate)
            {
                SaveAsToRepositoryCommand.DisabledReason = CommonMessage.Generic_NoCreatePermission;
                return false;
            }

            return true;
        }

        /// <summary>
        ///     Invoked when the <see cref="SaveAsToRepositoryCommand"/> is executed.
        /// </summary>
        private void SaveAsToRepository_Executed()
        {
            String copyName;

            PlanogramComparisonTemplateInfoList existingItems;
            try
            {
                existingItems = PlanogramComparisonTemplateInfoList.FetchByEntityId(CCMClient.ViewState.EntityId);
            }
            catch (Exception ex)
            {
                RecordException(ex);
                GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Save);
                return;
            }
            Predicate<String> isUniqueCheck = s => { return !existingItems.Select(p => p.Name).Contains(s, StringComparer.OrdinalIgnoreCase); };
            Boolean nameAccepted = GetWindowService().PromptForSaveAsName(isUniqueCheck, String.Empty, out copyName);
            if (!nameAccepted) return;

            ShowWaitCursor(true);

            PlanogramComparisonTemplate itemToSave = PlanogramComparisonTemplate.NewPlanogramComparisonTemplate(App.ViewState.EntityId, PlanComparisonEdit);
            itemToSave.Name = copyName;

            try
            {
                SyncComparisonFields(itemToSave);
                itemToSave.Save();
            }
            catch (Exception ex)
            {
                ShowWaitCursor(false);
                RecordException(ex);
                GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Save);
                return;
            }

            ShowWaitCursor(false);
        }

        #endregion

        #region SaveToFile

        /// <summary>
        ///     The command to save the current data to a file.
        /// </summary>
        private RelayCommand _saveAsToFileCommand;

        /// <summary>
        ///     Get the command to save the current data to a file.
        /// </summary>
        /// <remarks>If the command has not been initialized yet, it does so before returning it.</remarks>
        public RelayCommand SaveAsToFileCommand
        {
            get
            {
                if (_saveAsToFileCommand != null) return _saveAsToFileCommand;

                _saveAsToFileCommand =
                    new RelayCommand(SaveAsToFile_Executed)
                    {
                        FriendlyName = CommonMessage.Generic_SaveToFile
                    };
                RegisterCommand(_saveAsToFileCommand);

                return _saveAsToFileCommand;
            }
        }

        /// <summary>
        ///     Invoked when the <see cref="SaveAsToFileCommand"/> is executed.
        /// </summary>
        private void SaveAsToFile_Executed(Object args)
        {
            PlanogramComparisonTemplate itemToSave = PlanogramComparisonTemplate.NewPlanogramComparisonTemplate(App.ViewState.EntityId, PlanComparisonEdit);

            //  Get the file path to save to.
            var filePath = args as String;
            if (String.IsNullOrEmpty(filePath))
            {
                String fileName;

                Boolean result =
                    CommonHelper.GetWindowService().ShowSaveFileDialog(
                        null,
                        CCMClient.ViewState.GetSessionDirectory(SessionDirectory.PlanogramComparisonTemplate),
                        String.Format(CultureInfo.InvariantCulture, "Galleria Planogram Comparison File ({0})|*{0}", PlanogramComparisonTemplate.FileExtension),
                        out fileName);

                if (result)
                {
                    //  Update the session directory;
                    CCMClient.ViewState.SetSessionDirectory(SessionDirectory.PlanogramComparisonTemplate, Path.GetDirectoryName(fileName));
                }

                filePath = fileName;
            }
            if (String.IsNullOrEmpty(filePath)) return;

            //  Save the file.
            ShowWaitCursor(true);

            try
            {
                SyncComparisonFields(itemToSave);
                itemToSave = itemToSave.SaveAsFile(filePath);

                //  Unlock the file right away.
                itemToSave.Dispose();
            }
            catch (Exception ex)
            {
                ShowWaitCursor(false);
                RecordException(ex);
                GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Save);
                return;
            }

            ShowWaitCursor(false);
        }

        #endregion

        #region Apply

        /// <summary>
        ///     Instance of the <see cref="RelayCommand"/> that applies changes made to the settings.
        /// </summary>
        private RelayCommand _applyCommand;

        /// <summary>
        ///     Applies any pending changes made to the settings.
        /// </summary>
        public RelayCommand ApplyCommand
        {
            get
            {
                if (_applyCommand != null) return _applyCommand;

                _applyCommand =
                    new RelayCommand(p => ApplyCommand_Executed(), p => ApplyCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_ApplyAndClose,
                    };
                RegisterCommand(_applyCommand);

                return _applyCommand;
            }
        }

        /// <summary>
        ///     Determine whether the <see cref="ApplyCommand"/> can be executed or not.
        /// </summary>
        /// <returns><c>True</c> if the command is available to be executed, <c>false</c> if not.</returns>
        /// <remarks>All values need to be correct, and mandatory fields need to be entered.</remarks>
        [DebuggerStepThrough]
        private Boolean ApplyCommand_CanExecute()
        {
            //  Check that there are enough open planograms selected to be compared.
            if (!_allowExecute)
            {
                ApplyCommand.DisabledReason = Message.PlanogramComparisonEditor_NoOtherPlansSelected;
                return false;
            }
            
            //  Return success.
            return true;
        }

        /// <summary>
        ///     Invoked when the <see cref="ApplyCommand"/> is executed.
        /// </summary>
        private void ApplyCommand_Executed()
        {
            SyncComparisonFields(PlanComparisonEdit);
            originalSettings.Load(PlanComparisonEdit);
            DialogResult = true;
        }

        private void SyncComparisonFields(IPlanogramComparisonSettings settings)
        {
            settings.ResetComparisonFields();
            settings.AddComparisonFields(PlanogramItemType.Product, ProductFieldPickerViewModel.FieldPickerRows);
            settings.AddComparisonFields(PlanogramItemType.Position, PositionFieldPickerViewModel.FieldPickerRows);
        }

        #endregion

        #region Close

        /// <summary>
        ///     Instance of the <see cref="RelayCommand"/> that applies changes made to the settings.
        /// </summary>
        private RelayCommand _closeCommand;

        private readonly IPlanogramComparisonSettings originalSettings;

        /// <summary>
        ///     Applies any pending changes made to the settings.
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand != null) return _closeCommand;

                _closeCommand =
                    new RelayCommand(p => CloseCommand_Executed())
                    {
                        FriendlyName = Message.Generic_Close,
                    };
                RegisterCommand(_closeCommand);

                return _closeCommand;
            }
        }

        /// <summary>
        ///     Invoked when the <see cref="CloseCommand"/> is executed.
        /// </summary>
        private void CloseCommand_Executed()
        {
            DialogResult = false;
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        ///     Initialize the model to edit. If there is no model to edit, create a new one to edit.
        /// </summary>
        /// <param name="model">The model to edit, or <c>null</c> if a new model should be edited.</param>
        private void InitializeModelEdit()
        {
            //  If there is a model to edit, clone it.
            if (originalSettings != null)
            {
                PlanComparisonEdit.Load(originalSettings);
                var planogramChild = originalSettings as PlanComparison;
                if (planogramChild == null) return;

                //  This model is also embedded in a planogram, get the name right.
                IsInstance = true;
                PlanComparisonEdit.Name = planogramChild.Parent.Name;
                return;
            }

            //  Get a new model to edit, and configure the UI
            //  to reflect this is a new model.
            PlanComparisonEdit = PlanComparison.NewPlanogramComparison();
            ApplyCommand.FriendlyName = CommonMessage.Generic_Create;
            CloseCommand.FriendlyName = CommonMessage.Generic_Cancel;
        }

        /// <summary>
        ///     Initialize the UI with values for edit only mode.
        /// </summary>
        private void InitializeUiForEditOnly()
        {
            _allowExecute = false;

            //  Set the command names to reflect that this configuration can be executed.
            ApplyCommand.FriendlyName = CommonMessage.Generic_Apply;
            ApplyCommand.FriendlyDescription = "Apply current changes to the comparison settings.";
        }

        private void InitializeFieldPickerViewModels(Boolean dispose = false)
        {
            if (_fieldPickerViewModelsInitialized)
            {
                _fieldPickerViewModelsInitialized = false;

                _productFieldPickerViewModel.Dispose();
                _positionFieldPickerViewModel.Dispose();

                _productFieldPickerViewModel = null;
                _positionFieldPickerViewModel = null;
            }

            if (dispose) return;

            List<ObjectFieldInfo> fieldInfos = GetModelObjectFieldInfos();
            _productFieldPickerViewModel = new Ccm.Common.Wpf.Selectors.FieldPickerViewModel(fieldInfos.Where(IsProductFieldGroup));
            _positionFieldPickerViewModel = new Ccm.Common.Wpf.Selectors.FieldPickerViewModel(fieldInfos);

            SetComparisonFields();

            _fieldPickerViewModelsInitialized = true;
        }

        private void SetComparisonFields()
        {
            List<IPlanogramComparisonSettingsField> productComparisonFields;
            Boolean missingProductComparisonFields;
            List<IPlanogramComparisonSettingsField> positionComparisonFields;
            Boolean missingPositionComparisonFields;
            GetComparisonFields(out productComparisonFields,
                                out missingProductComparisonFields,
                                out positionComparisonFields,
                                out missingPositionComparisonFields);

            if (missingProductComparisonFields || missingPositionComparisonFields)
            {
                String comparisonTemplateId = App.ViewState.Settings.Model.DefaultPlanogramComparisonTemplateId;
                if (comparisonTemplateId != null)
                {
                    //  Try to get the user's default template.
                    PlanogramComparisonTemplate comparisonTemplate = null;
                    try
                    {
                        comparisonTemplate = comparisonTemplateId.EndsWith(PlanogramComparisonTemplate.FileExtension)
                                                 ? PlanogramComparisonTemplate.FetchByFilename(comparisonTemplateId)
                                                 : PlanogramComparisonTemplate.FetchById(comparisonTemplateId);
                    }
                    catch
                    {
                        // Unable to load the default template, then just load the hard-coded defaults.
                    }
                    if (comparisonTemplate != null)
                    {
                        //  Load the user's default template.
                        PlanComparisonEdit.Load(comparisonTemplate);
                        GetComparisonFields(out productComparisonFields,
                                            out missingProductComparisonFields,
                                            out positionComparisonFields,
                                            out missingPositionComparisonFields);
                    }
                }
            }

            if (missingProductComparisonFields)
            {
                productComparisonFields = PlanogramComparerHelper.EnumerateDefaultProductComparisonFields().ToList();
            }

            if (missingPositionComparisonFields)
            {
                positionComparisonFields = PlanogramComparerHelper.EnumerateDefaultPositionComparisonFields().ToList();
            }

            _productFieldPickerViewModel.SetObjectFieldInfos(productComparisonFields);
            _positionFieldPickerViewModel.SetObjectFieldInfos(positionComparisonFields);
        }

        private void GetComparisonFields(out List<IPlanogramComparisonSettingsField> productComparisonFields,
                                         out Boolean missingProductComparisonFields,
                                         out List<IPlanogramComparisonSettingsField> positionComparisonFields,
                                         out Boolean missingPositionComparisonFields)
        {
            Dictionary<PlanogramItemType, List<IPlanogramComparisonSettingsField>> fieldsByType =
                PlanComparisonEdit.ComparisonFields.GroupBy(o => o.ItemType).ToLookupDictionary(g => g.Key, g => g.ToList());

            fieldsByType.TryGetValue(PlanogramItemType.Product, out productComparisonFields);
            missingProductComparisonFields = productComparisonFields == null ||
                                             productComparisonFields.Count == 0;

            fieldsByType.TryGetValue(PlanogramItemType.Position, out positionComparisonFields);
            missingPositionComparisonFields = positionComparisonFields == null ||
                                              positionComparisonFields.Count == 0;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Responds to a change of selected setting.
        /// </summary>
        private void OnSelectedTemplateChanged(PlanogramComparisonTemplate oldValue, PlanogramComparisonTemplate newValue)
        {
            //if (oldValue != null)
            //{
            //    oldValue.PropertyChanged -= SelectedTemplate_PropertyChanged;
            //    oldValue.ChildChanged -= SelectedTemplate_ChildChanged;
            //    oldValue.ComparisonFields.BulkCollectionChanged -= SelectedTemplate_FieldsBulkCollectionChanged;
            //}

            //if (newValue != null)
            //{
            //    newValue.PropertyChanged += SelectedTemplate_PropertyChanged;
            //    newValue.ChildChanged += SelectedTemplate_ChildChanged;
            //    newValue.ComparisonFields.BulkCollectionChanged += SelectedTemplate_FieldsBulkCollectionChanged;
            //}

            //SelectedTemplate_FieldsBulkCollectionChanged(newValue, new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));
        }

        #endregion

        #region Helper Methods

        /// <summary>
        ///     Helper method to get the property path for the <see cref="PlanogramComparisonEditorViewModel" /> property indicated by the
        ///     <paramref name="expression" />.
        /// </summary>
        /// <param name="expression">Lambda expression to access the property.</param>
        /// <returns>
        ///     Returns a <see cref="PropertyPath" /> object with the path to the property in the
        ///     <paramref name="expression" />.
        /// </returns>
        private static PropertyPath GetPropertyPath(Expression<Func<PlanogramComparisonEditorViewModel, Object>> expression)
        {
            return WpfHelper.GetPropertyPath(expression);
        }

        private static List<ObjectFieldInfo> GetModelObjectFieldInfos()
        {
            List<ObjectFieldInfo> fieldInfos = PlanogramFieldHelper.EnumerateAllFields().ToList();
            foreach (ObjectFieldInfo field in fieldInfos)
            {
                field.GroupName = field.OwnerFriendlyName;
                field.PropertyName = String.Format("{0}.{1}", field.OwnerType, field.PropertyName);
            }
            return fieldInfos;
        }

        private static Boolean IsProductFieldGroup(ObjectFieldInfo objectFieldInfo)
        {
            var allowedTypes = new List<Type> { typeof(PlanogramProduct), typeof(Planogram), typeof(PlanogramInventory) };
            return allowedTypes.Contains(objectFieldInfo.OwnerType);
        }

        #endregion

        #region IDisposable Support

        protected override void Dispose(Boolean disposing)
        {
            if (IsDisposed) return;

            if (disposing)
            {
                _productFieldPickerViewModel.Dispose();
                _productFieldPickerViewModel = null;

                _positionFieldPickerViewModel.Dispose();
                _productFieldPickerViewModel = null;
            }

            IsDisposed = true;
        }

        #endregion
    }

    /// <summary>
    ///     Wrapper for a <see cref="PlanControllerViewModel"/> to display its planogram on a row.
    /// </summary>
    public sealed class PlanogramComparisonPlanogramRow : INotifyPropertyChanged
    {
        #region Properties

        #region PlanController

        /// <summary>
        ///     Gets or sets the plan controller containing the planogram represented by this instance.
        /// </summary>
        public PlanControllerViewModel PlanController { get; set; }

        #endregion

        #region IsCurrentPlanogram

        /// <summary>
        ///     Gets or sets whether this row represents the current planogram.
        /// </summary>
        public Boolean IsCurrentPlanogram { get; set; }

        #endregion

        #region IsSelected

        /// <summary>
        ///     Whether this row is selected.
        /// </summary>
        private Boolean _isSelected;

        /// <summary>
        ///     Gets or sets whether this row is selected.
        /// </summary>
        public Boolean IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        #endregion

        #region DisplayName

        /// <summary>
        ///     The display name for the planogram represented by this instance.
        /// </summary>
        private String _displayName;

        public String DisplayName
        {
            get { return String.IsNullOrWhiteSpace(_displayName) ? Name : _displayName; }
            set
            {
                _displayName = String.IsNullOrWhiteSpace(_displayName) ? Name : value;
                OnPropertyChanged("DisplayName");
            }
        }

        #endregion

        #region Name

        /// <summary>
        ///     Get the underlying planogram name.
        /// </summary>
        public String Name { get { return PlanController.SourcePlanogram.Name; } }

        #endregion

        #region CategoryCode

        /// <summary>
        ///     Get the underlying planogram category code.
        /// </summary>
        public String CategoryCode { get { return PlanController.SourcePlanogram.CategoryCode; } }

        #endregion

        #region CategoryName

        /// <summary>
        ///     Get the underlying planogram category name.
        /// </summary>
        public String CategoryName { get { return PlanController.SourcePlanogram.CategoryName; } }

        #endregion

        #region Path

        /// <summary>
        ///     Get the underlying planogram path.
        /// </summary>
        public String Path { get { return PlanController.SourcePlanogram.ParentPackageView.GetLocationPath(); } }

        #endregion

        #region Width

        /// <summary>
        ///     Get the underlying planogram width.
        /// </summary>
        public Single Width { get { return PlanController.SourcePlanogram.Width; } }

        #endregion

        #region Height

        /// <summary>
        ///     Get the underlying planogram height.
        /// </summary>
        public Single Height { get { return PlanController.SourcePlanogram.Height; } }

        #endregion

        #region Depth

        /// <summary>
        ///     Get the underlying planogram depth.
        /// </summary>
        public Single Depth { get { return PlanController.SourcePlanogram.Depth; } }

        #endregion

        #region DisplayUnits

        /// <summary>
        ///     Get the underlying planogram display units.
        /// </summary>
        public DisplayUnitOfMeasureCollection DisplayUnits { get { return PlanController.SourcePlanogram.DisplayUnits; } }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        ///     Initialize a new instance of <see cref="PlanogramComparisonPlanogramRow"/>.
        /// </summary>
        /// <param name="planController">The <see cref="PlanControllerViewModel"/> whose planogram is represented by this instance.</param>
        public PlanogramComparisonPlanogramRow(PlanControllerViewModel planController)
        {
            PlanController = planController;
            DisplayName = planController.SourcePlanogram.Name;
        }

        #endregion

        #region Factory Methods

        public static PlanogramComparisonPlanogramRow NewFrom(PlanControllerViewModel parentController,
                                                              PlanControllerViewModel planController,
                                                              Boolean includeAllPlanograms)
        {
            Boolean isCurrentPlanogram = planController == parentController;
            return new PlanogramComparisonPlanogramRow(planController)
            {
                IsSelected = includeAllPlanograms || isCurrentPlanogram,
                IsCurrentPlanogram = isCurrentPlanogram
            };
        }

        #endregion

        #region INotifyPropertyChanged Support

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}