#region Header Information

// Copyright � Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31742 : A.Silva
//  Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.PlanogramComparison
{
    /// <summary>
    ///     Enumeration of possible values to filter Planogram Comparison results
    /// </summary>
    public enum PlanogramComparisonFilterType
    {
        /// <summary>
        ///     Filter results from a product perspective.
        /// </summary>
        Products = 0,
        /// <summary>
        ///     Filter results from a position perspective.
        /// </summary>
        Positions = 1
    }

    /// <summary>
    ///     Helper class for the <see cref="PlanogramComparisonFilterType"/> enumeration.
    /// </summary>
    public static class PlanogramComparisonFilterTypeHelper
    {
        /// <summary>
        ///     A dictionary of <see cref="PlanogramComparisonFilterType"/> values and their matching <c>Friendly Name</c> <see cref="String"/>.
        /// </summary>
        public static readonly Dictionary<PlanogramComparisonFilterType, String> FriendlyNames =
            new Dictionary<PlanogramComparisonFilterType, String>
            {
                {PlanogramComparisonFilterType.Products, Message.Enum_PlanogramComparisonFilterType_Products},
                {PlanogramComparisonFilterType.Positions, Message.Enum_PlanogramComparisonFilterType_Positions}
            };
    }
}