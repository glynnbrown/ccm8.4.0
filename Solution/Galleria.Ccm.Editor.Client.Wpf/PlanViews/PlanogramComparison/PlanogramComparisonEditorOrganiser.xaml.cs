﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31763 : A.Silva
//  Created.

#endregion

#endregion

using System.Windows;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.PlanogramComparison
{
    /// <summary>
    /// Interaction logic for PlanogramComparisonEditorOrganiser.xaml
    /// </summary>
    public partial class PlanogramComparisonEditorOrganiser
    {
        #region Properties

        #region ViewModel

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel",
                                        typeof (PlanogramComparisonEditorViewModel),
                                        typeof (PlanogramComparisonEditorOrganiser),
                                        new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var senderCtrl = (PlanogramComparisonEditorOrganiser) sender;

            if (e.OldValue != null)
            {
                var oldModel = (PlanogramComparisonEditorViewModel) e.OldValue;
                oldModel.UnregisterWindowControl();
            }

            if (e.NewValue == null) return;

            var newModel = (PlanogramComparisonEditorViewModel) e.NewValue;
            newModel.RegisterWindowControl(senderCtrl);
        }

        public PlanogramComparisonEditorViewModel ViewModel
        {
            get { return (PlanogramComparisonEditorViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public PlanogramComparisonEditorOrganiser(PlanogramComparisonEditorViewModel viewModel)
        {
            InitializeComponent();

            ViewModel = viewModel;
        }

        #endregion
    }
}