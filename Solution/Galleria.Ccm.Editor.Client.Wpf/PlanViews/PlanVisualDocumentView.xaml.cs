﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
// V8-25395  : A.Probyn
//      Changed over from ProductInfo to Product
// CCM-25917 : N.Haywood
//  Added ResizedItemValueChange
// V8-25475 : A.Probyn
//  Added code to fix Drag/Drop of plan item on fixture library panel
// V8 - 24575 : A.Probyn
//  Added zoom to fit width to on drop
// CCM-25977 : N.Haywood
//  Stopped dragging and dropping of fixtures while in design view
// V8-26702 : A.Kuszyk
//  Updated OnDragOver and OnDrop to handle PlanogramAssortmentProducts.
// V8-27477 : L.Ineson
//  Fixes to position drag indicator.
// V8-25922 : L.Ineson
//  Added PasteClipboardAt(Point viewerHitPoint)
// V8-25093 : L.Ineson
//  Perspective camera scrolling now uses pan vector as look at was causing zoom issues.
// V8-27648 : A.Probyn ~ Updated to support ProductLibraryProduct
// V8-27724 : L.Ineson
//  Position drag indicator now shown when dragging from the product list.
// V8-27876 : L.Ineson
//  Added increase and decrease position facings commands to context menu.
// V8-28104 : L.ineson
//  Manipulator viewer is only hittestvisible when manipulator controllers are active.
// V8-28385 : L.Ineson
//  Slight change to how plan items are snapped when dragging a single item.
// V8-28367 : A.Probyn
//  Commented out ZoomToFitWidth on planogram context menu.
// V8-28542 : A.Kuszyk
//  Removed Fixture re-position from PasteClipboardAt, because this is handled by PlanogramView.AddPlanItemCopy().
#endregion

#region Version History: (CCM 8.01)
// V8-28942 : L.Ineson
// Changed how scrollbars update the camera position to stop products from disappearing randomly.
// V8-25227 : A.Kuszyk
//  Added check in BeginPlanItemDrag to make sure a PlanItem is actually being dragged and that we 
//  haven't incorrectly picked up a click.
#endregion

#region Version History: (CCM 8.02)
// V8-28905 : L.Luong
//  Draging positions between plans will now cut and paste
// V8-29039 : D.Pleasance
//  Amended xViewer_MouseDoubleClick, so that image provider is started to fetch images for selected position.
#endregion

#region Version History: (CCM 8.03)
// V8-29217 : L.Ineson
//  Position drag indicator arrow now gets sized according to camera zoom.
#endregion

#region Version History: (CCM 8.10)
//  CCM-28803 : M.Brumby
//      Dragged items inherit dynamic text size setting
// CCM-29856 : L.Ineson
//  It is no longer possible to drag items between plans.
// V8-28904 : M.Pettit
//  Prevent SizeZ going -ve under some circumstances
// V8-29189 : M.Brumby
//  Stopped components from snapping to nearest notch if they are pasted
//  into whitespace
// V8-29465 : M.Brumby
//  Maintain apparent zoom level with orthographic cameras when the view area
//  width changes.
#endregion

#region Version History: (CCM 8.1.1)
// V8-30311 : N.Haywood
//  Removed Camera flip in zoombyrectangle
// V8-30488 : L.Ineson
//  Added fix to stop GetLocalUnhinderedMerchandisingSpaceBounds trying to set negative values.
// V8-30600 : A.Kuszyk
//  Corrected axis label typo in xViewer_MouseMove method.
// V8-30672 : M.Brumby
//  Othographic width resize on view resize fix for floating the view.
#endregion

#region Version History: (CCM 8.2.0)
// V8-30324 : L.Luong
//  Should now only register double left click to open planogram properties 
// V8-30795 : A.Kuszyk
//  Ensured that products are sequenced when rubber band selected in the same way as when they are shift selected.
// V8-30890 : A.Kuszyk
//  Added Update Product Colour from Highlight menu item to context menu.
#endregion

#region Version History: (CCM 8.3.0)
// V8-31589 : L.Ineson
//  Added call to lighting helper.
// V8-31659 : L.Ineson
//  Added GetNearestSubComponent hit to stop insert of components snapping to existing positions.
// V8-31558 / V8-29333 : M.Shelley
//  Added code to allow drag and drop of fixtures / components / assemblies from one plan to another.
// V8-31564 : M.Shelley
//  Added PegProngOffsetX and PegProngOffsetY properties
// V8-32081 : N.Haywood
//  Added dragging from one fixture list to another plan
// V8-32159 : L.Ineson
//  Added handler for highlight legend item mouse right up event.
// V8-32080 : A.Silva
//  Refactored code to handle the dropping of Planogram Product Views directly or from a product list row.
// V8-32331 : L.Ineson
//  Fixes to drag drop from other plans.
// V8-32327 : A.Silva
//  Amended Drop and paste from clipboard methods to respect visual position order.
// V8-32386 : A.Silva
//  Amended DragSelectionAdorner_SelectionCompleted so that partial selections are included (no longer the whole item needs to be in the selection area).
// V8-32436 : L.Ineson
//  Default zoom on load changed from fit to height to fit all.
// V8-31788 : A.Silva
//  Amended BeginPlanItemDrag so that preview models are not messed up for components being dragged with partial positions selected.
//  Amended OnPlanItemDropped and PasteClipboardAt so that no duplicate positions are copied when dealing with components and positions at the same time.
// V8-32402  : J.Pickup
//  Further fix to drag drop from other plans. Updating of actualmodeldata to ensure has new model3dcosntruct parents etc.
// V8-32502 : L.Ineson
// Removed change using _lastPreviewPoint as it broke plan dragging.
// V8-32523 : L.Ineson
//  Added support for component level annotations.
// V8-32396 : A.Probyn
//  Added defensive code
// V8-32518 : N.Haywood
//  Added paste to all
// V8-31981 : L.Ineson
//  Added PegX and PegY to IPlanPositionRenderable
// V8-32685 : N.Haywood
//  Removed dependency on selecting a fixture for paste to all
// V8-32503 : L.Ineson
//  Updated GetNearestMerchandisable sub to search wider plan if no match was
//  found on nearest fixture to cover for shelves which go accross multiple bays.
// V8-32795 : L.Bailey
//  Dragging bays between plans mixes up products
// V8-32981 : L.Ineson
//  Fixed an issue with scrollbar suppressing getting stuck.
// V8-32327 : A.Silva
//  Amended PasteClipboardAt so that positions are pasted in the same order as they appeared originally.
//  Refactored to use PlanogramSubComponentView.PastePositionViews() and cleaned up a bit.
// CCM-13671 : G.Richards
//  Copy and paste buttons not enabling correctly when right clicking from one plan to another when the plan windows are tiled.
// CCM-18577 : A.Silva
//  Amended PasteClipboardAt to use AddPlanItemCopies, and refactored to reduce complexity.
#endregion

#region Version History: (CCM 8.3.2)
// CCM-18938 : M.Pettit
//  Added PegX2,PegY2, PegX3, PegY3 properties
#endregion

#endregion

using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Common.Rendering;
using Galleria.Ccm.Editor.Client.Wpf.FixtureLibrary;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews.ProductsList;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Controls.Wpf.HelperClasses;
using Galleria.Framework.Controls.Wpf.Rendering;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Controls.Wpf.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Rendering;
using Galleria.Framework.Rendering.Interfaces;
using Galleria.Framework.ViewModel;
using HelixToolkit.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// Interaction logic for PlanVisualDocumentView.xaml
    /// </summary>
    public sealed partial class PlanVisualDocumentView : UserControl, IPlanDocumentView
    {
        #region Nested Classes

        private sealed class PositionDragPreview : IPlanPositionRenderable
        {
            private sealed class ProductRenderable : IPlanProductRenderable
            {
                private IPlanogramProduct _product;

                public ProductRenderable(IPlanogramProduct planProduct)
                {
                    _product = planProduct;
                }

                bool IPlanProductRenderable.IsUpdating { get { return false; } }
                float IPlanProductRenderable.TrayHeight { get { return _product.TrayHeight; } }
                float IPlanProductRenderable.TrayWidth { get { return _product.TrayWidth; } }
                float IPlanProductRenderable.TrayDepth { get { return _product.TrayDepth; } }
                float IPlanProductRenderable.TrayThickHeight { get { return _product.TrayThickHeight; } }
                float IPlanProductRenderable.TrayThickWidth { get { return _product.TrayThickWidth; } }
                float IPlanProductRenderable.TrayThickDepth { get { return _product.TrayThickDepth; } }
                byte IPlanProductRenderable.TrayHigh { get { return _product.TrayHigh; } }
                byte IPlanProductRenderable.TrayWide { get { return _product.TrayWide; } }
                byte IPlanProductRenderable.TrayDeep { get { return _product.TrayDeep; } }
                byte IPlanProductRenderable.NumberOfPegHoles { get { return _product.NumberOfPegHoles; } }
                float IPlanProductRenderable.PegX { get { return _product.PegX; } }
                float IPlanProductRenderable.PegY { get { return _product.PegY; } }
                float IPlanProductRenderable.PegX2 { get { return _product.PegX2; } }
                float IPlanProductRenderable.PegY2 { get { return _product.PegY2; } }
                float IPlanProductRenderable.PegX3 { get { return _product.PegX3; } }
                float IPlanProductRenderable.PegY3 { get { return _product.PegY3; } }
                float IPlanProductRenderable.PegDepth { get { return _product.PegDepth; } }
                float IPlanProductRenderable.NestingHeight { get { return _product.NestingHeight; } }
                float IPlanProductRenderable.NestingWidth { get { return _product.NestingWidth; } }
                float IPlanProductRenderable.NestingDepth { get { return _product.NestingDepth; } }
                PlanogramProductShapeType IPlanProductRenderable.ShapeType { get { return _product.ShapeType; } }
                PlanogramProductFillPatternType IPlanProductRenderable.FillPatternType { get { return _product.FillPatternType; } }
                int IPlanProductRenderable.FillColour { get { return _product.FillColour; } }
                Single IPlanProductRenderable.PegDefaultHeight { get { return 1; } }
                Single IPlanProductRenderable.PegDefaultWidth { get { return 1; } }

                public event PropertyChangedEventHandler PropertyChanged;
                
            }

            #region Fields
            private IPlanogramProduct _product;
            private ProductRenderable _productRenderable;
            #endregion

            #region Constructors
            public PositionDragPreview(IPlanogramProduct product)
            {
                _product = product;
                _productRenderable = new ProductRenderable(product);

                FacingsHigh = 1;
                FacingsWide = 1;
                FacingsDeep = 1;
            }
            #endregion

            #region IPlanPositionRenderable Members

            public short FacingsHigh { get; set; }
            public short FacingsWide { get; set; }
            public short FacingsDeep { get; set; }

            IPlanProductRenderable IPlanPositionRenderable.Product { get { return _productRenderable; } }
            IPlanSubComponentRenderable IPlanPositionRenderable.ParentSubComponent { get { return null; } }
            bool IPlanPositionRenderable.IsUpdating { get { return false; } }
            float IPlanPositionRenderable.X { get { return 0; } }
            float IPlanPositionRenderable.Y { get { return 0; } }
            float IPlanPositionRenderable.Z { get { return 0; } }
            float IPlanPositionRenderable.Angle { get { return 0; } }
            float IPlanPositionRenderable.Slope { get { return 0; } }
            float IPlanPositionRenderable.Roll { get { return 0; } }

            float IPlanPositionRenderable.Height { get { return _product.Height; } }
            float IPlanPositionRenderable.Width { get { return _product.Width; } }
            float IPlanPositionRenderable.Depth { get { return _product.Depth; } }

            public Object Id { get { return 0; } }

            PlanogramProductOrientationType IPlanPositionRenderable.UnitOrientationType
            {
                get { return _product.OrientationType; }
            }

            bool IPlanPositionRenderable.IsMerchandisedAsTrays
            {
                get { return (_product.MerchandisingStyle == PlanogramProductMerchandisingStyle.Tray); }
            }

            float IPlanPositionRenderable.UnitHeight { get { return _product.Height; } }
            float IPlanPositionRenderable.UnitWidth { get { return _product.Width; } }
            float IPlanPositionRenderable.UnitDepth { get { return _product.Depth; } }

            byte[] IPlanPositionRenderable.FrontImageData { get { return null; } }
            byte[] IPlanPositionRenderable.BackImageData { get { return null; } }
            byte[] IPlanPositionRenderable.TopImageData { get { return null; } }
            byte[] IPlanPositionRenderable.BottomImageData { get { return null; } }
            byte[] IPlanPositionRenderable.LeftImageData { get { return null; } }
            byte[] IPlanPositionRenderable.RightImageData { get { return null; } }
            float IPlanPositionRenderable.BlockMainStartX { get { return 0; } }
            float IPlanPositionRenderable.BlockMainStartY { get { return 0; } }
            float IPlanPositionRenderable.BlockMainStartZ { get { return 0; } }
            short IPlanPositionRenderable.FacingsXHigh { get { return 0; } }
            short IPlanPositionRenderable.FacingsXWide { get { return 0; } }
            short IPlanPositionRenderable.FacingsXDeep { get { return 0; } }
            PlanogramProductOrientationType IPlanPositionRenderable.UnitXOrientationType { get { return _product.OrientationType; } }
            bool IPlanPositionRenderable.IsXMerchandisedAsTrays { get { return false; } }
            float IPlanPositionRenderable.UnitXHeight { get { return 0; } }
            float IPlanPositionRenderable.UnitXWidth { get { return 0; } }
            float IPlanPositionRenderable.UnitXDepth { get { return 0; } }
            bool IPlanPositionRenderable.IsXPlacedLeft { get { return false; } }
            byte[] IPlanPositionRenderable.FrontXImageData { get { return null; } }
            byte[] IPlanPositionRenderable.BackXImageData { get { return null; } }
            byte[] IPlanPositionRenderable.TopXImageData { get { return null; } }
            byte[] IPlanPositionRenderable.BottomXImageData { get { return null; } }
            byte[] IPlanPositionRenderable.LeftXImageData { get { return null; } }
            byte[] IPlanPositionRenderable.RightXImageData { get { return null; } }
            float IPlanPositionRenderable.BlockXStartX { get { return 0; } }
            float IPlanPositionRenderable.BlockXStartY { get { return 0; } }
            float IPlanPositionRenderable.BlockXStartZ { get { return 0; } }
            short IPlanPositionRenderable.FacingsYHigh { get { return 0; } }
            short IPlanPositionRenderable.FacingsYWide { get { return 0; } }
            short IPlanPositionRenderable.FacingsYDeep { get { return 0; } }
            PlanogramProductOrientationType IPlanPositionRenderable.UnitYOrientationType { get { return _product.OrientationType; } }
            bool IPlanPositionRenderable.IsYMerchandisedAsTrays { get { return false; } }
            float IPlanPositionRenderable.UnitYHeight { get { return 0; } }
            float IPlanPositionRenderable.UnitYWidth { get { return 0; } }
            float IPlanPositionRenderable.UnitYDepth { get { return 0; } }
            bool IPlanPositionRenderable.IsYPlacedBottom { get { return false; } }
            byte[] IPlanPositionRenderable.FrontYImageData { get { return null; } }
            byte[] IPlanPositionRenderable.BackYImageData { get { return null; } }
            byte[] IPlanPositionRenderable.TopYImageData { get { return null; } }
            byte[] IPlanPositionRenderable.BottomYImageData { get { return null; } }
            byte[] IPlanPositionRenderable.LeftYImageData { get { return null; } }
            byte[] IPlanPositionRenderable.RightYImageData { get { return null; } }
            float IPlanPositionRenderable.BlockYStartX { get { return 0; } }
            float IPlanPositionRenderable.BlockYStartY { get { return 0; } }
            float IPlanPositionRenderable.BlockYStartZ { get { return 0; } }
            short IPlanPositionRenderable.FacingsZHigh { get { return 0; } }
            short IPlanPositionRenderable.FacingsZWide { get { return 0; } }
            short IPlanPositionRenderable.FacingsZDeep { get { return 0; } }
            PlanogramProductOrientationType IPlanPositionRenderable.UnitZOrientationType { get { return _product.OrientationType; } }
            bool IPlanPositionRenderable.IsZMerchandisedAsTrays { get { return false; } }
            float IPlanPositionRenderable.UnitZHeight { get { return 0; } }
            float IPlanPositionRenderable.UnitZWidth { get { return 0; } }
            float IPlanPositionRenderable.UnitZDepth { get { return 0; } }
            bool IPlanPositionRenderable.IsZPlacedFront { get { return false; } }
            byte[] IPlanPositionRenderable.FrontZImageData { get { return null; } }
            byte[] IPlanPositionRenderable.BackZImageData { get { return null; } }
            byte[] IPlanPositionRenderable.TopZImageData { get { return null; } }
            byte[] IPlanPositionRenderable.BottomZImageData { get { return null; } }
            byte[] IPlanPositionRenderable.LeftZImageData { get { return null; } }
            byte[] IPlanPositionRenderable.RightZImageData { get { return null; } }
            float IPlanPositionRenderable.BlockZStartX { get { return 0; } }
            float IPlanPositionRenderable.BlockZStartY { get { return 0; } }
            float IPlanPositionRenderable.BlockZStartZ { get { return 0; } }
            bool IPlanPositionRenderable.IsOnPegs { get { return false; } }
            bool IPlanPositionRenderable.IsOnTopDownComponent { get { return false; } }
            float IPlanPositionRenderable.FacingSpaceX { get { return 0; } }
            float IPlanPositionRenderable.FacingSpaceY { get { return 0; } }
            float IPlanPositionRenderable.FacingSpaceZ { get { return 0; } }

            float IPlanPositionRenderable.PegX { get { return _product.PegX; } }
            float IPlanPositionRenderable.PegY { get { return _product.PegY; } }
            float IPlanPositionRenderable.PegX2 { get { return _product.PegX2; } }
            float IPlanPositionRenderable.PegY2 { get { return _product.PegY2; } }
            float IPlanPositionRenderable.PegX3 { get { return _product.PegX3; } }
            float IPlanPositionRenderable.PegY3 { get { return _product.PegY3; } }

            public event PropertyChangedEventHandler PropertyChanged;

            bool IPlanPositionRenderable.IsTrayImage(PlanogramPositionBlockType blockType, PlanogramProductFaceType faceType)
            {
                return false;
            }

            public event EventHandler ParentRotationChanged;
            #endregion

        }

        #endregion

        #region Fields

        private const Double _gridlineAutosizeMargin = 60D;

        private ModelConstruct3D _modelVisual;
        private Boolean _isGridlineRefreshRequired;
        private Boolean _supressScrollbarUpdate;

        private Transform3DController _transformController;
        private Rotation3DController _rotationController;
        private Boolean _lockControllers;

        private Boolean _isDraggingAdorner;
        private DragSelectionAdorner _dragSelectionAdorner;
        private DragSelectionAdorner _orthPartCreator;
        //private Model3DCreator _perspectivePartCreator;
        private ModelConstruct3D[] _previewModels = null; //models used to preview drags
        private PlanItemDragDropArgs _lastDragArgs;

        private Point? _lastPreviewPoint; //the last mouse point at which a preview was processed.

        private ArrowVisual3D _dragIndicatorArrow;
        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanVisualDocument), typeof(PlanVisualDocumentView),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PlanVisualDocumentView senderControl = (PlanVisualDocumentView)obj;

            Plan3DData newPlanogramModel = null;

            if (e.OldValue != null)
            {
                PlanVisualDocument oldModel = (PlanVisualDocument)e.OldValue;
                oldModel.AttachedControl = null;

                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
                oldModel.SelectedPlanItems.BulkCollectionChanged -= senderControl.ViewModel_SelectedPlanItemsCollectionChanged;
                oldModel.Planogram.BoundsChanged -= senderControl.ViewModel_SourcePlanogramBoundsChanged;
                oldModel.Zooming -= senderControl.ViewModel_Zooming;
            }

            if (e.NewValue != null)
            {
                PlanVisualDocument newModel = (PlanVisualDocument)e.NewValue;
                newModel.AttachedControl = senderControl;

                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
                newModel.SelectedPlanItems.BulkCollectionChanged += senderControl.ViewModel_SelectedPlanItemsCollectionChanged;
                newModel.Planogram.BoundsChanged += senderControl.ViewModel_SourcePlanogramBoundsChanged;
                newModel.Zooming += senderControl.ViewModel_Zooming;

                newPlanogramModel = newModel.PlanogramModelData;
            }

            Plan3DData oldPlanogramModel = (senderControl._modelVisual != null) ? senderControl._modelVisual.ModelData as Plan3DData : null;
            senderControl.ViewModel_PlanogramModelChanged(oldPlanogramModel, newPlanogramModel);
        }



        /// <summary>
        /// Gets the viewmodel controller for this view.
        /// </summary>
        public PlanVisualDocument ViewModel
        {
            get { return (PlanVisualDocument)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new document view of this type.
        /// </summary>
        /// <param name="documentController"></param>
        public PlanVisualDocumentView(PlanVisualDocument documentController)
        {
            this.AddHandler(HighlightLegend.LegendItemMouseRightUpEvent, (RoutedEventHandler)HighlightLegend_LegendItemMouseRightUp);
            this.AddHandler(HighlightLegend.CloseRequestedEvent, (RoutedEventHandler)HighlightLegend_CloseRequested);

            InitializeComponent();

            this.ViewModel = documentController;

            SetupForViewType();

            PlanCameraHelper.SetPanningMode(this.xViewer, false);
        }


        #endregion

        #region Event Handlers

        #region Local Overrides

        /// <summary>
        /// Called whenever a key up occurs on this control
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewKeyUp(KeyEventArgs e)
        {
            base.OnPreviewKeyUp(e);

            if (e.Key == Key.LeftCtrl || e.Key == Key.RightCtrl
                || e.Key == Key.LeftShift || e.Key == Key.RightShift)
            {
                this.ViewModel.Planogram.EndUndoableAction();
            }

        }

        /// <summary>
        /// Called when a drag drop action is moved over this.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnDragOver(DragEventArgs e)
        {
            base.OnDragOver(e);

            if (DragDropBehaviour.IsUnpackedType<PlanItemDragDropArgs>(e.Data))
            {
                OnPlanItemDragOver(DragDropBehaviour.UnpackData<PlanItemDragDropArgs>(e.Data), e);
            }
            else if (DragDropBehaviour.IsUnpackedType<FixtureLibraryItemDropEventArgs>(e.Data))
            {
                OnFixtureLibraryDragOver(DragDropBehaviour.UnpackData<FixtureLibraryItemDropEventArgs>(e.Data), e);
            }
            else if (DragDropBehaviour.IsUnpackedType<ExtendedDataGridRowDropEventArgs>(e.Data))
            {
                ExtendedDataGridRowDropEventArgs rowArgs = DragDropBehaviour.UnpackData<ExtendedDataGridRowDropEventArgs>(e.Data);
                if (rowArgs.Items.Any())
                {
                    if (rowArgs.Items.First() is ProductListRow
                        || rowArgs.Items.First() is Product
                        || rowArgs.Items.First() is ProductLibraryProduct
                        || rowArgs.Items.First() is PlanogramAssortmentProduct)
                    {
                        OnProductDragOver(rowArgs, e);
                    }
                    else if (rowArgs.Items.First() is IPlanItem)
                    {
                        if (_lastDragArgs == null)
                        {
                            PlanItemDragDropArgs dragArgs = new PlanItemDragDropArgs();
                            foreach (object item in rowArgs.Items)
                            {
                                IPlanItem planItem = item as IPlanItem;
                                if (planItem == null) continue;
                                dragArgs.DragItems.Add(new PlanDragDropItem { PlanItem = planItem });
                            }
                            _lastDragArgs = dragArgs;
                        }

                        //call the normal drag over handler.
                        OnPlanItemDragOver(_lastDragArgs, e);
                    }
                }
                else
                {
                    ClearPositionDragArrow();
                }
            }
        }

        /// <summary>
        /// Called whenever a drag behaviour drops here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void OnDrop(DragEventArgs e)
        {
            base.OnDrop(e);

            if (e.Data == null)
            {
                return;
            }

            if (DragDropBehaviour.IsUnpackedType<FixtureLibraryItemDropEventArgs>(e.Data))
            {
                OnFixtureLibraryDropped(DragDropBehaviour.UnpackData<FixtureLibraryItemDropEventArgs>(e.Data), e);
            }
            else if (DragDropBehaviour.IsUnpackedType<PlanItemDragDropArgs>(e.Data))
            {
                OnPlanItemDropped(DragDropBehaviour.UnpackData<PlanItemDragDropArgs>(e.Data), e);
            }
            else if (DragDropBehaviour.IsUnpackedType<ExtendedDataGridRowDropEventArgs>(e.Data))
            {
                ExtendedDataGridRowDropEventArgs rowArgs = DragDropBehaviour.UnpackData<ExtendedDataGridRowDropEventArgs>(e.Data);
                if (rowArgs.Items.Count > 0)
                {
                    if (rowArgs.Items.First() is ProductListRow)
                    {
                        OnProductRowsDropped(rowArgs, e);
                    }
                    else if (rowArgs.Items.First() is Product)
                    {
                        OnProductLibraryDropped(rowArgs, e);
                    }
                    else if (rowArgs.Items.First() is ProductLibraryProduct)
                    {
                        OnProductLibraryProductDropped(rowArgs, e);
                    }
                    else if (rowArgs.Items.First() is PlanogramProductView)
                    {
                        OnPlanogramProductViewDropped(rowArgs, e);
                    }
                    else if (rowArgs.Items.First() is IPlanItem)
                    {
                        if (_lastDragArgs != null) OnPlanItemDropped(_lastDragArgs, e);
                    }
                }
            }


            //tidy up any leftover dragging models
            ClearPreviewModels();


            //TODO: Find out why this is breaking drag drop.
            FrameworkElement parent = this;
            while (parent != null)
            {
                parent.AllowDrop = true;

                parent = VisualTreeHelper.GetParent(parent) as FrameworkElement;
            }

            //Ensure adorner is removed
            AdornerLayer aLayer = AdornerLayer.GetAdornerLayer(this);
            Adorner existingAdorner = aLayer.FindVisualDescendent<Adorner>();
            if (existingAdorner != null)
            {
                aLayer.Remove(existingAdorner);
            }
        }

        /// <summary>
        /// Called when a drag leaves the scope of this.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnDragLeave(DragEventArgs e)
        {
            base.OnDragLeave(e);

            //Remove any preview models.
            ClearPreviewModels();
        }

        #endregion

        #region ViewModel Event Handlers

        /// <summary>
        /// Called when the planogram model changes
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        private void ViewModel_PlanogramModelChanged(Plan3DData oldModel, Plan3DData newModel)
        {
            if (_modelVisual != null)
            {
                _modelVisual = null;
            }

            //clear the viewer children.
            if (this.xViewer != null)
            {
                foreach (Visual3D visual in this.xViewer.Children.ToList())
                {
                    if (visual is ModelConstruct3D)
                    {
                        this.xViewer.Children.Remove(visual);
                    }
                }
            }



            if (newModel != null)
            {
                _modelVisual = new ModelConstruct3D(newModel);
                _modelVisual.IsDynamicTextScalingEnabled = this.ViewModel.IsDynamicTextScalingEnabled;

                if (this.xViewer != null)
                {
                    this.xViewer.Children.Add(_modelVisual);
                    UpdateGridlines();
                    UpdateScrollbars();
                }
            }

        }

        /// <summary>
        /// Called when a property changes on the viewmodel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectionColour") return;

            if (e.PropertyName == PlanVisualDocument.IsActiveDocumentProperty.Path)
            {
                if (this.ViewModel.IsActiveDocument)
                {
                    //force keyboard focus on the viewer
                    if (Keyboard.FocusedElement != this.xViewer)
                    {
                        Keyboard.Focus(this.xViewer);
                    }
                }
            }
            else if (e.PropertyName == PlanVisualDocument.PlanogramModelDataProperty.Path)
            {
                Plan3DData oldData = (_modelVisual != null) ? (Plan3DData)_modelVisual.ModelData : null;
                ViewModel_PlanogramModelChanged(oldData, this.ViewModel.PlanogramModelData);
            }
            else if (e.PropertyName == PlanVisualDocument.IsInZoomToAreaModeProperty.Path)
            {
                PlanCameraHelper.SetZoomToAreaMode(this.xViewer, this.ViewModel.IsInZoomToAreaMode);
            }
            else if (e.PropertyName == PlanVisualDocument.IsInPanningModeProperty.Path)
            {
                PlanCameraHelper.SetPanningMode(this.xViewer, this.ViewModel.IsInPanningMode);
            }
            else if (e.PropertyName == PlanVisualDocument.ShowGridlinesProperty.Path
                || e.PropertyName == PlanVisualDocument.AutosizeGridlinesProperty.Path
                || e.PropertyName == PlanVisualDocument.GridlineFixedDepthProperty.Path
                || e.PropertyName == PlanVisualDocument.GridlineFixedHeightProperty.Path
                || e.PropertyName == PlanVisualDocument.GridlineFixedWidthProperty.Path
                || e.PropertyName == PlanVisualDocument.GridlineMajorDistProperty.Path
                || e.PropertyName == PlanVisualDocument.GridlineMinorDistProperty.Path)
            {
                UpdateGridlines();
            }
            else if (e.PropertyName == PlanVisualDocument.MouseToolTypeProperty.Path)
            {
                //CancelPerspectivePartCreator();

                this.Cursor =
                    (MouseToolTypeHelper.IsCreateType(this.ViewModel.MouseToolType)) ?
                    Cursors.Cross : null;

                ClearPreviewModels();
                SetModelController();
            }
            else if (e.PropertyName == PlanVisualDocument.SuppressPerformanceIntensiveUpdatesProperty.Path)
            {
                OnViewModelSuppressPerformanceIntensiveUpdatesChanged();
            }
            else if (e.PropertyName == PlanVisualDocument.IsDynamicTextScalingEnabledProperty.Path)
            {
                //update the viewport attached property
                if (_modelVisual != null)
                {
                    _modelVisual.IsDynamicTextScalingEnabled = this.ViewModel.IsDynamicTextScalingEnabled;
                }
            }
        }

        /// <summary>
        /// Called when the contents of the viewmodel SelectedPlanItems collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_SelectedPlanItemsCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            SetModelController();
        }

        /// <summary>
        /// Called whenever the bounds of the source planogram changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_SourcePlanogramBoundsChanged(object sender, EventArgs e)
        {
            UpdateGridlines();
        }

        /// <summary>
        /// Called whenever the viewmodel is requesting a zoom action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_Zooming(object sender, ZoomEventArgs e)
        {
            switch (e.Mode)
            {
                case ZoomType.ZoomIn:
                    PlanCameraHelper.ZoomIn(this.xViewer);
                    break;

                case ZoomType.ZoomItems:
                    ZoomItems((IEnumerable<IPlanItem>)e.Parameter);
                    break;

                case ZoomType.ZoomOut:
                    PlanCameraHelper.ZoomOut(this.xViewer);
                    break;

                case ZoomType.ZoomToFit:
                    ZoomToFit();
                    break;

                case ZoomType.ZoomToFitHeight:
                    ZoomToFitHeight();
                    break;

                case ZoomType.ZoomToFitWidth:
                    ZoomToFitWidth();
                    break;

                default:
                    Debug.Fail("Not implemented");
                    ZoomToFit();
                    break;
            }

        }

        /// <summary>
        /// Called whenever the value of the viewmodel SuppressPerformanceIntensiveUpdates property changes.
        /// </summary>
        private void OnViewModelSuppressPerformanceIntensiveUpdatesChanged()
        {
            if (!this.ViewModel.SuppressPerformanceIntensiveUpdates)
            {
                if (_isGridlineRefreshRequired)
                {
                    UpdateGridlines();
                }
            }
        }

        #endregion

        #region Viewer Event Handlers

        /// <summary>
        /// Called when the xviewer is initially loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xViewer_Loaded(object sender, RoutedEventArgs e)
        {
            this.xViewer.Loaded -= xViewer_Loaded;

            if (this.xViewer.CameraController == null)
            {
                //force assignment.
                this.xViewer.ApplyTemplate();
            }

            if (this.xViewer.CameraController != null)
            {
                //stupid fix to stop this mucking up the docpanel.
                // as otherwise camera controller keeps stealing keyboard focus
                this.xViewer.CameraController.Focusable = false;

                if (!this.xViewer.IsMeasureValid)
                {

                    this.xViewer.SizeChanged += xViewer_InitialSizeSet;
                }
                else
                {
                    ZoomToFit();
                }

            }
        }

        /// <summary>
        /// Called whene viewer size is initialized
        /// </summary>
        private void xViewer_InitialSizeSet(object sender, SizeChangedEventArgs e)
        {
            if (this.xViewer.IsMeasureValid)
            {
                this.xViewer.SizeChanged -= xViewer_InitialSizeSet;
                ZoomToFit();
            }
        }

        /// <summary>
        /// Called when any mouse button is pressed on the viewer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xViewer_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //force keyboard focus on the viewer
            Boolean isZoomingByRect = (this.ViewModel != null && this.ViewModel.IsInZoomToAreaMode);

            if (!isZoomingByRect && Keyboard.FocusedElement != this.xViewer)
            {
                Keyboard.Focus(this.xViewer);
            }
        }

        /// <summary>
        /// Responds to a mouse left click on the viewport
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xViewer_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //return straight out if we have no viewmodel.
            if (this.ViewModel == null) { return; }
            if (this.ViewModel.IsInZoomToAreaMode) { return; } //or if we are in zoom to area mode
            if (this.ViewModel.IsInPanningMode) { return; } //or if we are in panning mode.

            if (MouseToolTypeHelper.IsCreateType(this.ViewModel.MouseToolType))
            {
                //we in create part mode.
                if (this.ViewModel.ViewType == CameraViewType.Perspective)
                {
                    //BeginPerspectivePartCreator(e);
                }
                else
                {
                    BeginOrthPartCreator(e);
                }

                e.Handled = true;
                return;
            }

            //update the current selection
            this.ViewModel.ProcessUserClickSelection(GetHitPlanItem(e, false), Keyboard.Modifiers, /*mouseUp*/false);


            //start a drag selection if shift modifier and pointer are active
            if (Keyboard.Modifiers == ModifierKeys.Shift
                && this.ViewModel.MouseToolType == MouseToolType.Pointer)
            {
                _dragSelectionAdorner = new DragSelectionAdorner(this.xViewerCanvas, e.GetPosition(this.xViewerCanvas));
            }
        }

        /// <summary>
        /// Responds to a mouse left up on the viewport
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xViewer_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Boolean handled = false;

            handled = this.ViewModel.IsInZoomToAreaMode;

            if (!handled)
            {

                //remove selected item
                this.ViewModel.ProcessUserClickSelection(GetHitPlanItem(e), Keyboard.Modifiers, /*mouseUp*/true);

                //handled = PerspectivePartCreator_ProcesssMouseLeftButtonUp(e);
            }

            if (!handled)
            {
                handled = MouseToolType_ProcessMouseUp(e);
            }
        }

        /// <summary>
        /// Called when the viewer is double clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xViewer_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.ViewModel != null)
            {
                if (e.ChangedButton == MouseButton.Left)
                {
                    if (MainPageCommands.ShowSelectedItemProperties.CanExecute())
                    {
                        MainPageCommands.ShowSelectedItemProperties.Execute();
                    }
                    else
                    {
                        MainPageCommands.ShowPlanogramProperties.Execute();
                    }
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// Called when a preview mouse right button down on the viewer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xViewer_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            //nb cannot do this for perspective view as it messes with rotation!
            if (this.ViewModel != null && this.ViewModel.ViewType != CameraViewType.Perspective)
            {
                App.MainPageViewModel.SwitchToView(this.ViewModel);

                this.ViewModel.ProcessUserClickSelection(GetHitPlanItem(e), Keyboard.Modifiers, /*isMouseUp*/false);

                if (Keyboard.Modifiers == ModifierKeys.None)
                {
                    ShowContextMenu();
                }
            }
            ClearPreviewModels();
        }

        /// <summary>
        /// Called whenever the viewer notifies of a preview mouse move event
        /// </summary>
        private void xViewer_PreviewMouseMove(Object sender, MouseEventArgs e)
        {
            //return straight out if the camera controller is processing.
            if (this.ViewModel != null
                && (this.ViewModel.IsInZoomToAreaMode || this.ViewModel.IsInPanningMode)) { return; }


            if (e.LeftButton == MouseButtonState.Pressed && !DragDropBehaviour.IsDragging)
            {
                if (Keyboard.Modifiers == ModifierKeys.None || Keyboard.Modifiers == ModifierKeys.Control)
                {
                    if (this.ViewModel != null && this.ViewModel.MouseToolType == MouseToolType.Pointer
                        && !_isDraggingAdorner)
                    {
                        BeginPlanItemDrag(e);
                    }
                }
            }
        }

        /// <summary>
        /// Called as the mouse moves over the viewer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xViewer_MouseMove(object sender, MouseEventArgs e)
        {
            Boolean handled = false;

            handled = this.ViewModel.IsInZoomToAreaMode || this.ViewModel.IsInPanningMode;

            if (!handled && MouseToolTypeHelper.IsCreateType(this.ViewModel.MouseToolType))
            {
                if (this.ViewModel.ViewType == CameraViewType.Perspective)
                {
                    //handled = PerspectivePartCreator_ProcessMouseMove(e);
                    //handled = OrthPartCreator_ProcessMouseMove(e);
                }
                else
                {
                    handled = OrthPartCreator_ProcessMouseMove(e);
                }
            }

            if (!handled)
            {
                #region Update status bar

                //update the status bar with the hovered over item

                if (this.ViewModel != null)
                {
                    this.ViewModel.UpdateStatusBarText(GetHitPlanItem(e));

                    PointValue hitPoint3D = ModelConstruct3DHelper.GetModelPoint3D(e.GetPosition(this.xViewer), this.xViewer.Viewport);

                    ProjectionCamera camera = this.xViewer.Camera;
                    Vector3D upDirection = camera.UpDirection;
                    Vector3D normal = Vector3D.CrossProduct(camera.LookDirection, camera.UpDirection);
                    normal.Normalize();

                    Double right = hitPoint3D.X;
                    Double up = hitPoint3D.Y;

                    // Work out the normal axis label.
                    String normalAxis = AxisTypeHelper.FriendlyNames[AxisType.X];
                    if (normal.Y != 0)
                    {
                        right = hitPoint3D.Y;
                        normalAxis = AxisTypeHelper.FriendlyNames[AxisType.Y];
                    }
                    else if (normal.Z != 0)
                    {
                        right = hitPoint3D.Z;
                        normalAxis = AxisTypeHelper.FriendlyNames[AxisType.Z];
                    }

                    // Work out the up axis label.
                    String upAxis = AxisTypeHelper.FriendlyNames[AxisType.Y];
                    if (upDirection.X != 0)
                    {
                        up = hitPoint3D.X;
                        upAxis = AxisTypeHelper.FriendlyNames[AxisType.X];
                    }
                    else if (upDirection.Z != 0)
                    {
                        up = hitPoint3D.Z;
                        upAxis = AxisTypeHelper.FriendlyNames[AxisType.Z];
                    }


                    App.MainPageViewModel.StatusBarMouseText =
                        String.Format("({0}: {1}, {2}: {3})", normalAxis, Math.Round(right, 2), upAxis, Math.Round(up, 2));

                }
                #endregion

                handled = MouseToolType_ProcessMouseMove(e);
            }

            if (!handled)
            {
                //process any ongoing drag selection
                handled = DragSelectionAdorner_ProcessMouseMove(e);
            }
        }

        /// <summary>
        /// Called when the mouse leaves the viewer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xViewer_MouseLeave(object sender, MouseEventArgs e)
        {
            //reset parent controller status text back to null.
            if (this.ViewModel != null)
            {
                this.ViewModel.UpdateStatusBarText(null);
                App.MainPageViewModel.StatusBarMouseText = null;
            }

            if (!this.IsMouseOver)
            {
                ClearPreviewModels();
            }
        }

        /// <summary>
        /// Called when the view camera changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xViewer_CameraChanged(object sender, RoutedEventArgs e)
        {
            //copy over the camera to the manipulators view.
            CameraHelper.Copy(this.xViewer.Camera, this.xManipulatorViewer.Camera as ProjectionCamera);

                UpdateScrollbars();
            }

        /// <summary>
        /// Called when the view size changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            //update the highlight legend position
            if (this.xHighlightLegend != null)
            {
                // check the position of the highlight.
                this.xHighlightLegend.ConstrainPosition();
            }

            //Maintain apparent zoom level for othographic cameras when changing width
            //EDIT: But only if we have some sizes to work with!
            if (e.WidthChanged && e.NewSize.Width > 0 && e.PreviousSize.Width > 0)
            {
                OrthographicCamera camera = xViewer.Camera as OrthographicCamera;
                if (camera != null)
                {

                    camera.Width = (camera.Width / e.PreviousSize.Width) * e.NewSize.Width;
                }
            }

            UpdateScrollbars();
        }

        /// <summary>
        /// Called when the viewer zooms by rectangle
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xViewer_ZoomedByRectangle(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel != null
                && this.ViewModel.ViewType != CameraViewType.Perspective)
            {
                ProjectionCamera camera = this.xViewer.Camera as ProjectionCamera;


                //flip the camera look direction.
                Vector3D newLookDirection =
                    new Vector3D(camera.LookDirection.X, camera.LookDirection.Y, camera.LookDirection.Z);

                switch (this.ViewModel.ViewType)
                {
                    case CameraViewType.Design:
                    case CameraViewType.Front:
                    case CameraViewType.Back:
                        newLookDirection.Z = -newLookDirection.Z;
                        break;

                    case CameraViewType.Left:
                    case CameraViewType.Right:
                        newLookDirection.X = -newLookDirection.X;
                        break;

                    case CameraViewType.Top:
                    case CameraViewType.Bottom:
                        newLookDirection.Y = -newLookDirection.Y;
                        break;
                }


                camera.LookDirection = newLookDirection;


                if (this.ViewModel.IsInZoomToAreaMode)
                {
                    this.ViewModel.IsInZoomToAreaMode = false;
                }
            }
        }

        #endregion

        #region Highlight Legend

        /// <summary>
        /// Called whenever the user right clicks on a legend item
        /// Shows a context menu to select the related positions for the group.
        /// </summary>
        private void HighlightLegend_LegendItemMouseRightUp(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel == null) return;

            PlanogramHighlightResultGroup group = e.OriginalSource as PlanogramHighlightResultGroup;
            if (group == null) return;

            Fluent.ContextMenu contextMenu = new Fluent.ContextMenu();

            Fluent.MenuItem menuItem = new Fluent.MenuItem();
            menuItem.Tag = group;
            menuItem.Header = String.Format(CultureInfo.CurrentCulture, Message.HighlightLegend_SelectPositions, group.Label);
            menuItem.Click += SelectHighlightGroupPositions_Click;
            contextMenu.Items.Add(menuItem);

            contextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Mouse;
            contextMenu.StaysOpen = false;
            contextMenu.IsOpen = true;
        }

        /// <summary>
        /// Called when the select highlight group positions context menu item is clicked.
        /// </summary>
        private void SelectHighlightGroupPositions_Click(object sender, RoutedEventArgs e)
        {
            Fluent.MenuItem menuItem = (Fluent.MenuItem)sender;
            menuItem.Click -= SelectHighlightGroupPositions_Click;

            if (this.ViewModel == null) return;

            PlanogramHighlightResultGroup group = menuItem.Tag as PlanogramHighlightResultGroup;
            if (group == null) return;

            //update the selection to the positions.
            this.ViewModel.SelectHighlightGroupPositions(group);

        }

        /// <summary>
        /// Called when the highlight legend wants to close.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HighlightLegend_CloseRequested(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.IsHighlightLegendVisible = false;
            }
        }

        #endregion

        /// <summary>
        /// Called whenever the value of the horizontal scrollbar changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xHorizontalScrollbar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<Double> e)
        {
            if (this.ViewModel == null) return;
            if (this.xViewer == null) return;
            if (_supressScrollbarUpdate) return;

            _supressScrollbarUpdate = true;

            try
            {

                if (this.ViewModel.ViewType == CameraViewType.Perspective)
                {
                    //Can't use the look at method for this type as its causing it to zoom
                    // instead of pan.
                    Double f = 1.2;
                    Double change = e.NewValue - e.OldValue;
                    Vector3D panVector = FindPanVector(f * -change, 0, this.xViewer.CameraController);
                    this.xViewer.CameraController.CameraPosition += panVector;
                }
                else
                {
                    Double change = e.NewValue - e.OldValue;

                    ProjectionCamera camera = this.xViewer.Camera as ProjectionCamera;
                    if (camera != null)
                    {
                        //PAN:
                        CameraController controller = this.xViewer.CameraController;
                        Point3D panPoint3D = controller.CameraTarget;
                        Point mousedownPoint = new Point(this.xViewer.ActualWidth / 2F, this.xViewer.ActualHeight / 2);
                        Point3D? lastPoint3D = this.UnProject(mousedownPoint, panPoint3D, controller.CameraLookDirection);

                        Vector delta = new Vector(-change, 0);
                        var mousePoint = mousedownPoint/*this.LastPoint*/ + delta;

                        var thisPoint3D = this.UnProject(mousePoint, panPoint3D, controller.CameraLookDirection);

                        if (lastPoint3D != null && thisPoint3D != null)
                        {
                            Vector3D delta3D = lastPoint3D.Value - thisPoint3D.Value;
                            controller.CameraPosition += delta3D;
                        }

                        //Old:
                        //Point3D nr, far;

                        //Double viewportWidth = this.xViewer.Viewport.ActualWidth;
                        //Double viewportHeight = this.xViewer.Viewport.ActualHeight;


                        //Viewport3DHelper.Point2DtoPoint3D(this.xViewer.Viewport, new Point(viewportWidth / 2, viewportHeight / 2), out nr, out far);

                        //Point3D curCenter = nr;


                        //Viewport3DHelper.Point2DtoPoint3D(this.xViewer.Viewport, new Point((viewportWidth / 2) + 1, viewportHeight / 2), out nr, out far);
                        //Point3D offCenter = nr;

                        //Vector3D changeVector = ModelConstruct3DHelper.FindItemMoveVector(this.xViewer.Camera, 1, 0);
                        //Vector3D diffVector = offCenter - curCenter;


                        //Vector3D direction = ModelConstruct3DHelper.FindItemMoveVector(this.xViewer.Camera, change, 0);

                        ////get the current center
                        //Point3D newLookAt =
                        //    new Point3D()
                        //    {
                        //        X = curCenter.X + (changeVector.X * diffVector.X * change),
                        //        Y = curCenter.Y + (changeVector.Y * diffVector.Y * change),
                        //        Z = curCenter.Z + (changeVector.Z * diffVector.Z * change),
                        //    };

                        //camera.LookAt(newLookAt, 0);
                    }
                }
            }
            catch (Exception) { }


            _supressScrollbarUpdate = false;

        }

        /// <summary>
        /// Called whenever the value of the vertical scrollbar changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xVerticalScrollbar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<Double> e)
        {
            if (_supressScrollbarUpdate) return;

            _supressScrollbarUpdate = true;

            try
            {
                Double change = e.NewValue - e.OldValue;

                //PAN:
                CameraController controller = this.xViewer.CameraController;
                Point3D panPoint3D = controller.CameraTarget;
                Point mousedownPoint = new Point(this.xViewer.ActualWidth / 2F, this.xViewer.ActualHeight / 2);
                Point3D? lastPoint3D = this.UnProject(mousedownPoint, panPoint3D, controller.CameraLookDirection);

                Vector delta = new Vector(0, -change);
                var mousePoint = mousedownPoint/*this.LastPoint*/ + delta;

                var thisPoint3D = this.UnProject(mousePoint, panPoint3D, controller.CameraLookDirection);

                if (lastPoint3D != null && thisPoint3D != null)
                {
                    Vector3D delta3D = lastPoint3D.Value - thisPoint3D.Value;
                    controller.CameraPosition += delta3D;
                }

                //Old:
                //Double f = 1.2;
                //Vector3D panVector = FindPanVector(0, f * -change, this.xViewer.CameraController);
                //this.xViewer.CameraController.CameraPosition += panVector;

            }
            catch (Exception) { }

            _supressScrollbarUpdate = false;

        }

        /// <summary>
        /// Called whenever the context menu paste button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PasteContextMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var pasteItem = (Fluent.MenuItem)sender;
            pasteItem.Click -= PasteContextMenuItem_Click;
            PasteClipboardAt((Point)pasteItem.Tag);
        }

        /// <summary>
        /// Called whenever the context menu paste to all button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PasteToAllContextMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var pasteItem = (Fluent.MenuItem)sender;
            pasteItem.Click -= PasteToAllContextMenuItem_Click;
            //PasteClipboardAt((Point)pasteItem.Tag);
            MainPageCommands.PasteToAllPlanograms.Execute();
        }

        #endregion

        #region Camera

        /// <summary>
        /// Performs any setup actions required for the selected viewtype.
        /// </summary>
        private void SetupForViewType()
        {
            if (this.xViewer == null || this.xManipulatorViewer == null)
            {
                ApplyTemplate();
            }

            //apply viewport settings.
            RenderOptions.SetBitmapScalingMode(this.xViewer, BitmapScalingMode.NearestNeighbor);

            CameraViewType viewType = this.ViewModel.ViewType;

            //Add lighting.
            PlanLightingHelper.SetupDefaultLighting(this.xViewer, viewType);

            //update gridlines.
            GridLinesVisual3D gridlines = this.xGridlines;
            gridlines.Material = new DiffuseMaterial(Brushes.DarkGray);
            gridlines.BackMaterial = gridlines.Material;
            gridlines.Thickness = 0.5;

            //reset the manipulator camera
            Viewport3D manipulatorViewport = this.xManipulatorViewer;
            if (viewType == CameraViewType.Perspective)
            {
                manipulatorViewport.Camera = new PerspectiveCamera();
            }
            else
            {
                RenderOptions.SetEdgeMode(manipulatorViewport, EdgeMode.Aliased);
                manipulatorViewport.Camera = new OrthographicCamera();
            }

            //set the main camera
            PlanCameraHelper.SetupForCameraType(this.xViewer, viewType);

        }

        /// <summary>
        /// Updates the scrollbar properties
        /// </summary>
        private void UpdateScrollbars()
        {
            if (!_supressScrollbarUpdate)
            {
                _supressScrollbarUpdate = true;

                Viewport3D viewport = this.xViewer.Viewport;


                Rect modelBounds = ModelConstruct3DHelper.GetModel2DBounds(viewport, _modelVisual);
                if (!modelBounds.IsEmpty
                    && !Double.IsNaN(modelBounds.Width)
                    && !Double.IsNaN(modelBounds.Height)
                    && !Double.IsInfinity(modelBounds.Width))
                {

                    var horizScrollbar = this.xHorizontalScrollbar;
                    var vertScrollvar = this.xVerticalScrollbar;


                    Double modelWidth = modelBounds.Width;
                    if (modelBounds.X > 0 &&
                        (modelBounds.X + modelWidth) < viewport.ActualWidth)
                    {
                        //all visible
                        horizScrollbar.Maximum = 0;
                        horizScrollbar.Value = 0;
                    }
                    else
                    {
                        //calculate extent beyond screen
                        Double extent = 0;
                        if ((modelBounds.X + modelWidth) > viewport.ActualWidth)
                        {
                            extent += (modelBounds.X + modelWidth) - viewport.ActualWidth;
                        }
                        if (modelBounds.X < 0)
                        {
                            extent += (-modelBounds.X * 2);
                        }
                        else
                        {
                            extent += modelBounds.X;
                        }

                        horizScrollbar.Maximum = extent;
                        horizScrollbar.Value = -modelBounds.X;
                    }

                    Double modelHeight = modelBounds.Height;
                    if (modelBounds.Y > 0 &&
                        (modelBounds.Y + modelHeight) < viewport.ActualHeight)
                    {
                        //all visible
                        vertScrollvar.Maximum = 0;
                        vertScrollvar.Value = 0;
                    }
                    else
                    {
                        //calculate extent beyond screen
                        Double extent = 0;
                        if ((modelBounds.Y + modelHeight) > viewport.ActualHeight)
                        {
                            extent += (modelBounds.Y + modelHeight) - viewport.ActualHeight;
                        }
                        if (modelBounds.Y < 0)
                        {
                            extent += -modelBounds.Y;
                        }
                        vertScrollvar.Maximum = extent;
                        vertScrollvar.Value = -modelBounds.Y;
                    }
                }

                _supressScrollbarUpdate = false;
            }

        }

        /// <summary>
        /// Zooms the plan to fit.
        /// </summary>
        private void ZoomToFit()
        {
            UpdateGridlines();
            PlanCameraHelper.ZoomToFit(this.xViewer, _modelVisual);
        }

        /// <summary>
        /// Zooms to fit the model by height.
        /// </summary>
        private void ZoomToFitHeight()
        {
            if (this.ViewModel != null)
            {
                UpdateGridlines();

                if (this.ViewModel.Planogram.EnumerateAllComponents().FirstOrDefault() != null)
                {
                    PlanCameraHelper.ZoomToFitHeight(this.xViewer, _modelVisual);
                }
                else
                {
                    PlanCameraHelper.ZoomToFit(this.xViewer, _modelVisual);
                }
            }
        }

        /// <summary>
        /// Zooms to fit the model by width
        /// </summary>
        private void ZoomToFitWidth()
        {
            if (this.ViewModel != null)
            {
                UpdateGridlines();

                if (this.ViewModel.Planogram.EnumerateAllComponents().FirstOrDefault() != null)
                {
                    PlanCameraHelper.ZoomToFitWidth(this.xViewer, _modelVisual);
                }
                else
                {
                    PlanCameraHelper.ZoomToFit(this.xViewer, _modelVisual);
                }
            }
        }

        /// <summary>
        /// Zooms to fit the currently selected plan items.
        /// </summary>
        private void ZoomItems(IEnumerable<IPlanItem> items)
        {
            if (items.Any())
            {
                if (this.ViewModel != null)
                {
                    //get all of the selected models
                    List<ModelVisual3D> selectedModels = new List<ModelVisual3D>();
                    foreach (IPlanItem item in items)
                    {
                        ModelVisual3D visual = RenderControlsHelper.FindItemModel(_modelVisual, item) as ModelVisual3D;
                        if (visual != null)
                        {
                            selectedModels.Add(visual);
                        }
                    }

                    //Perform the zoom
                    PlanCameraHelper.ZoomItems(this.xViewer, selectedModels);
                }
            }
            else
            {
                ZoomToFit();
            }
        }

        #endregion

        #region Model Controllers / Mouse Tools

        #region Transform / Rotate Controllers

        /// <summary>
        /// Updates the model controller state.
        /// </summary>
        private void SetModelController()
        {
            Viewport3D viewer = this.xManipulatorViewer;

            if (this.ViewModel != null && viewer != null)
            {
                //set the viewer to not hit test visible
                viewer.IsHitTestVisible = false;

                Boolean clearTransformController = !_lockControllers;
                Boolean clearRotationController = !_lockControllers;

                //tidy up any unrequired controllers
                if (clearTransformController && _transformController != null)
                {
                    _transformController.Visibility = System.Windows.Visibility.Collapsed;
                    BindingOperations.ClearBinding(_transformController, Transform3DController.CenterProperty);

                }
                if (clearRotationController && _rotationController != null)
                {
                    _rotationController.Visibility = System.Windows.Visibility.Collapsed;
                    BindingOperations.ClearBinding(_rotationController, Transform3DController.CenterProperty);
                }



                PlanItemSelection selection = this.ViewModel.SelectedPlanItems;
                if (selection.Count > 0 && this.ViewModel.MouseToolType != MouseToolType.Pointer)
                {
                    //make the viewer hit test visible while the manipulator control is active.
                    viewer.IsHitTestVisible = true;

                    IPlanItem item = selection.Last();

                    switch (this.ViewModel.MouseToolType)
                    {
                        case MouseToolType.Transform:
                            {
                                clearTransformController = false;

                                //create the controller if necessary
                                if (_transformController == null)
                                {
                                    _transformController = new Transform3DController();
                                    _transformController.Visibility = Visibility.Collapsed;
                                    viewer.Children.Add(_transformController);

                                    SetEventHandlers(_transformController, /*isSubscribe*/true);
                                }

                                //attach the controller to the item.
                                SetTransformController(_transformController, item, _modelVisual, viewer);
                            }
                            break;

                        case MouseToolType.Rotate:
                            {
                                clearRotationController = false;

                                if (_rotationController == null)
                                {
                                    _rotationController = new Rotation3DController();
                                    _rotationController.Visibility = System.Windows.Visibility.Collapsed;
                                    viewer.Children.Add(_rotationController);

                                    SetEventHandlers(_rotationController, /*isSubscribe*/true);
                                }

                                //attach the controller to the item.
                                SetRotationController(_rotationController, item, _modelVisual, viewer);
                            }
                            break;

                    }

                }

            }

        }

        /// <summary>
        /// Updates the given transform controller as per the given item.
        /// </summary>
        /// <param name="transformController"></param>
        /// <param name="item"></param>
        /// <param name="parentPlanModel"></param>
        /// <param name="viewer"></param>
        public static void SetTransformController(Transform3DController transformController, IPlanItem item,
            ModelConstruct3D parentPlanModel, Viewport3D viewer)
        {
            ModelConstruct3D model = null;

            IModelConstruct3DData modelData = RenderControlsHelper.FindItemModelData(parentPlanModel.ModelData as ModelConstruct3DData, item);
            if (modelData != null)
            {
                model = parentPlanModel.FindModel(modelData);
            }

            if (model != null)
            {
                model.UpdateModelPosition();

                BindingOperations.SetBinding(transformController, Transform3DController.CenterProperty,
                   new Binding(ModelConstruct3D.WorldCenterProperty.Name) { Source = model });

                if (viewer.Camera is OrthographicCamera)
                {
                    Vector3D moveVector = ModelConstruct3DHelper.FindItemMoveVector(viewer.Camera as ProjectionCamera, 2, 2);
                    transformController.CanTransformX = (moveVector.X != 0);
                    transformController.CanTransformY = (moveVector.Y != 0);
                    transformController.CanTransformZ = (moveVector.Z != 0);

                    Rect3D modelBounds = model.GetBounds();
                    if (!modelBounds.IsEmpty)
                    {
                        Double length = transformController.Length;
                        Double minBoundsLength = new Double[] { modelBounds.SizeX, modelBounds.SizeY, modelBounds.SizeY }.Min();
                        length = Math.Max(length, minBoundsLength * 0.1);

                        transformController.Length = length;
                    }

                }


                transformController.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Updates the given rotation controller as per the given item.
        /// </summary>
        /// <param name="transformController"></param>
        /// <param name="item"></param>
        /// <param name="parentPlanModel"></param>
        /// <param name="viewer"></param>
        public static void SetRotationController(Rotation3DController rotateController, IPlanItem item,
            ModelConstruct3D parentPlanModel, Viewport3D viewer)
        {
            ModelConstruct3D model = null;

            IModelConstruct3DData modelData = RenderControlsHelper.FindItemModelData(parentPlanModel.ModelData as ModelConstruct3DData, item);
            if (modelData != null)
            {
                model = parentPlanModel.FindModel(modelData);
            }

            if (model != null)
            {
                model.UpdateModelPosition();

                BindingOperations.SetBinding(rotateController, Rotation3DController.CenterProperty,
                       new Binding(ModelConstruct3D.WorldCenterProperty.Name) { Source = model });

                rotateController.Length = 1D;
                rotateController.InnerDiameter = 25D;
                rotateController.Diameter = 26D;


                if (viewer.Camera is OrthographicCamera)
                {
                    Vector3D moveVector = ModelConstruct3DHelper.FindItemMoveVector(viewer.Camera as ProjectionCamera, 2, 2);
                    rotateController.CanRotateX = (moveVector.X == 0);
                    rotateController.CanRotateY = (moveVector.Y == 0);
                    rotateController.CanRotateZ = (moveVector.Z == 0);
                }
            }

            rotateController.Visibility = Visibility.Visible;
        }


        /// <summary>
        /// Hooks/Unhooks the event handlers for the given transform controller
        /// </summary>
        /// <param name="ctl"></param>
        /// <param name="isSubscribe"></param>
        private void SetEventHandlers(Transform3DController ctl, Boolean isSubscribe)
        {
            if (isSubscribe)
            {
                ctl.PositionValueChangeBeginning += PositionController_ValueChangeBeginning;
                ctl.PositionValueChanged += PositionController_ValueChanged;
                ctl.PositionValueChangeComplete += Model3DController_ValueChangeComplete;

                ctl.SizeValueChangeBeginning += SizeController_ValueChangeBeginning;
                ctl.SizeValueChanged += SizeController_ValueChanged;
                ctl.SizeValueChangeComplete += Model3DController_ValueChangeComplete;
            }
            else
            {
                ctl.PositionValueChangeBeginning -= PositionController_ValueChangeBeginning;
                ctl.PositionValueChanged -= PositionController_ValueChanged;
                ctl.PositionValueChangeComplete -= Model3DController_ValueChangeComplete;

                ctl.SizeValueChangeBeginning -= SizeController_ValueChangeBeginning;
                ctl.SizeValueChanged -= SizeController_ValueChanged;
                ctl.SizeValueChangeComplete -= Model3DController_ValueChangeComplete;
            }

        }

        /// <summary>
        /// Hooks/Unhooks the event handlers for the given rotation controller
        /// </summary>
        /// <param name="ctl"></param>
        /// <param name="isSubscribe"></param>
        private void SetEventHandlers(Rotation3DController ctl, Boolean isSubscribe)
        {
            if (isSubscribe)
            {
                ctl.ValueChangeBeginning += RotateController_ValueChangeBeginning;
                ctl.ValueChanged += RotateController_ValueChanged;
                ctl.ValueChangeComplete += Model3DController_ValueChangeComplete;
            }
            else
            {
                ctl.ValueChangeBeginning -= RotateController_ValueChangeBeginning;
                ctl.ValueChanged -= RotateController_ValueChanged;
                ctl.ValueChangeComplete -= Model3DController_ValueChangeComplete;
            }

        }

        /// <summary>
        /// Called when a size controller begins its manipulation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SizeController_ValueChangeBeginning(object sender, EventArgs e)
        {
            this.ViewModel.Planogram.BeginUndoableAction(Message.ModelUndoAction_ResizeItem);

            //suppress updates to improve performance
            this.ViewModel.SuppressPerformanceIntensiveUpdates = true;
        }

        /// <summary>
        /// Called whenever a size controller value changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SizeController_ValueChanged(object sender, Controller3DValueChangedArgs e)
        {
            var vector = e.ChangeVector;
            e.Cancelled = !PlanItemHelper.ResizeItems(this.ViewModel.SelectedPlanItems, vector.X, vector.Y, vector.Z);
        }

        /// <summary>
        /// Called when a position controller begins its manipulation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PositionController_ValueChangeBeginning(object sender, EventArgs e)
        {
            _lockControllers = true;
            this.ViewModel.Planogram.BeginUndoableAction(Message.ModelUndoAction_MoveItem);

            //if ctrl is pressed then create a new copy of the items.
            if (Keyboard.Modifiers == ModifierKeys.Control)
            {
                List<IPlanItem> newItems = new List<IPlanItem>();
                foreach (IPlanItem item in this.ViewModel.SelectedPlanItems.ToList())
                {
                    newItems.Add(this.ViewModel.Planogram.AddPlanItemCopy(
                        item,
                        App.ViewState.SelectionMode != PlanItemSelectionType.OnlyComponents,
                        /*shiftHeight*/false));
                }
                this.ViewModel.SelectedPlanItems.Clear();
                this.ViewModel.SelectedPlanItems.AddRange(newItems);
            }


            //suppress updates to improve performance
            this.ViewModel.SuppressPerformanceIntensiveUpdates = true;
        }

        /// <summary>
        /// Called whenever a position controller value changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PositionController_ValueChanged(object sender, Controller3DValueChangedArgs e)
        {
            var vector = e.ChangeVector;
            PlanItemHelper.RepositionItems(this.ViewModel.SelectedPlanItems, vector.X, vector.Y, vector.Z);
        }

        /// <summary>
        /// Called when a rotate controller begins its manipulation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RotateController_ValueChangeBeginning(object sender, EventArgs e)
        {
            this.ViewModel.Planogram.BeginUndoableAction(Message.ModelUndoAction_RotateItem);

            //suppress updates to improve performance
            this.ViewModel.SuppressPerformanceIntensiveUpdates = true;
        }

        /// <summary>
        /// Called whenever a rotate controller value changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RotateController_ValueChanged(object sender, EventArgs<Vector3D> e)
        {
            PlanItemHelper.RotateItems(this.ViewModel.SelectedPlanItems,
                e.ReturnValue.X, e.ReturnValue.Y, e.ReturnValue.Z);
        }

        /// <summary>
        /// Called when a mouse tool controller completes its manipulation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model3DController_ValueChangeComplete(object sender, EventArgs e)
        {
            this.ViewModel.Planogram.EndUndoableAction();

            //unsupress updates
            this.ViewModel.SuppressPerformanceIntensiveUpdates = false;

            _lockControllers = false;
        }

        #region OLD

        ///// <summary>
        ///// Removes all model controllers
        ///// </summary>
        //private void ClearAllModelControllers()
        //{
        //    Viewport3D viewer = this.xManipulatorViewer;

        //    foreach (var entry in _modelControllers)
        //    {
        //        Visual3D controller = entry.Value;

        //        if (controller is Rotation3DController)
        //        {
        //            SetEventHandlers((Rotation3DController)controller, /*isSubscribe*/false);
        //        }
        //        else if (controller is Transform3DController)
        //        {
        //            SetEventHandlers((Transform3DController)controller, /*isSubscribe*/false);
        //        }

        //        viewer.Children.Remove(controller);
        //    }

        //    _modelControllers.Clear();
        //}

        ///// <summary>
        ///// Shows/Hides the model controllers for the given item.
        ///// </summary>
        ///// <param name="item"></param>
        ///// <param name="isSelected"></param>
        //private void SetModelControllers(IPlanItem item, Boolean isSelected)
        //{
        //    Viewport3D viewer = this.xManipulatorViewer;

        //    if (this.ViewModel != null && viewer != null)
        //    {
        //        if (isSelected && this.ViewModel.MouseToolType != MouseToolType.Pointer)
        //        {
        //            //clear out existing
        //            if (_modelControllers.ContainsKey(item))
        //            {
        //                Visual3D oldController = _modelControllers[item];
        //                if (oldController is Rotation3DController)
        //                {
        //                    SetEventHandlers((Rotation3DController)oldController, /*isSubscribe*/false);
        //                }
        //                else if (oldController is Transform3DController)
        //                {
        //                    SetEventHandlers((Transform3DController)oldController, /*isSubscribe*/false);
        //                }

        //                viewer.Children.Remove(oldController);
        //                _modelControllers.Remove(item);
        //            }

        //            //add new controller
        //            Visual3D controller = null;
        //            switch (this.ViewModel.MouseToolType)
        //            {
        //                case MouseToolType.Transform:
        //                    Transform3DController transformController = AddTransformModelController(item, _modelVisual, viewer);
        //                    SetEventHandlers(transformController, /*isSubscribe*/true);
        //                    controller = transformController;
        //                    break;

        //                case MouseToolType.Rotate:
        //                    Rotation3DController rotateController = AddRotateModelController(item, _modelVisual, viewer);
        //                    SetEventHandlers(rotateController, /*isSubscribe*/true);
        //                    controller = rotateController;
        //                    break;

        //            }

        //            _modelControllers.Add(item, controller);
        //        }
        //        else
        //        {
        //            //remove any controllers associated with this item.
        //            Visual3D controller;
        //            if (_modelControllers.TryGetValue(item, out controller))
        //            {
        //                if (controller is Rotation3DController)
        //                {
        //                    SetEventHandlers((Rotation3DController)controller, /*isSubscribe*/false);
        //                }
        //                else if (controller is Transform3DController)
        //                {
        //                    SetEventHandlers((Transform3DController)controller, /*isSubscribe*/false);
        //                }

        //                viewer.Children.Remove(controller);
        //                _modelControllers.Remove(item);
        //            }
        //        }

        //    }
        //}

        ///// <summary>
        ///// Adds a new transform model controller
        ///// </summary>
        ///// <param name="item"></param>
        ///// <param name="parentPlanModel"></param>
        ///// <param name="viewer"></param>
        ///// <returns></returns>
        //public static Transform3DController AddTransformModelController(IPlanItem item, ModelConstruct3D parentPlanModel, Viewport3D viewer)
        //{
        //    Transform3DController controller = null;

        //    ModelConstruct3D model = null;

        //    IModelConstruct3DData modelData = PlanDocumentHelper.GetItemSelectionModel(parentPlanModel.ModelData as ModelConstruct3DData, item);
        //    if (modelData != null)
        //    {
        //        model = parentPlanModel.FindModel(modelData);
        //    }

        //    if (model != null)
        //    {
        //        model.UpdateModelPosition();

        //        var transformController = new Transform3DController();
        //        controller = transformController;

        //        BindingOperations.SetBinding(transformController, Transform3DController.CenterProperty,
        //           new Binding(ModelConstruct3D.WorldCenterProperty.Name) { Source = model });

        //        if (viewer.Camera is OrthographicCamera)
        //        {
        //            Vector3D moveVector = Model3DHelper.FindItemMoveVector(viewer.Camera as ProjectionCamera, 2, 2);
        //            transformController.CanTransformX = (moveVector.X != 0);
        //            transformController.CanTransformY = (moveVector.Y != 0);
        //            transformController.CanTransformZ = (moveVector.Z != 0);

        //            Rect3D modelBounds = model.GetBounds();
        //            if (!modelBounds.IsEmpty)
        //            {
        //                Double length = transformController.Length;
        //                Double minBoundsLength = new Double[] { modelBounds.SizeX, modelBounds.SizeY, modelBounds.SizeY }.Min();
        //                length = Math.Max(length, minBoundsLength * 0.1);

        //                transformController.Length = length;
        //            }

        //        }

        //        transformController.Visibility = Visibility.Collapsed;
        //        viewer.Children.Add(transformController);
        //        transformController.Visibility = Visibility.Visible;

        //    }

        //    return controller;
        //}

        ///// <summary>
        ///// Adds a new rotate model controller
        ///// </summary>
        ///// <param name="item"></param>
        ///// <param name="parentPlanModel"></param>
        ///// <param name="viewer"></param>
        ///// <returns></returns>
        //public static Rotation3DController AddRotateModelController(IPlanItem item, ModelConstruct3D parentPlanModel, Viewport3D viewer)
        //{
        //    Rotation3DController controller = null;

        //    ModelConstruct3D model = null;

        //    IModelConstruct3DData modelData = PlanDocumentHelper.GetItemSelectionModel(parentPlanModel.ModelData as ModelConstruct3DData, item);
        //    if (modelData != null)
        //    {
        //        model = parentPlanModel.FindModel(modelData);
        //    }

        //    if (model != null)
        //    {
        //        model.UpdateModelPosition();

        //        var rotateController = new Rotation3DController()
        //        {
        //            Length = 1D,
        //            InnerDiameter = 25D,
        //            Diameter = 26D
        //        };
        //        controller = rotateController;

        //        if (viewer.Camera is OrthographicCamera)
        //        {
        //            Vector3D moveVector = Model3DHelper.FindItemMoveVector(viewer.Camera as ProjectionCamera, 2, 2);
        //            rotateController.CanRotateX = (moveVector.X == 0);
        //            rotateController.CanRotateY = (moveVector.Y == 0);
        //            rotateController.CanRotateZ = (moveVector.Z == 0);
        //        }


        //        BindingOperations.SetBinding(rotateController, Rotation3DController.CenterProperty,
        //               new Binding(ModelConstruct3D.WorldPositionProperty.Name) { Source = model });

        //        rotateController.Visibility = Visibility.Collapsed;
        //        viewer.Children.Add(rotateController);
        //        rotateController.Visibility = Visibility.Visible;

        //    }

        //    return controller;
        //}

        #endregion

        #endregion

        #region Mouse Tool Type Previewing

        /// <summary>
        /// Carries out any mouse move processing required by the current mouse tool.
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        private Boolean MouseToolType_ProcessMouseMove(MouseEventArgs e)
        {
            Boolean handled = false;

            //don't bother processing if the mouse has not moved.
            Point mousePoint = e.GetPosition(this.xViewer);
            if (_lastPreviewPoint == mousePoint) { return false; }
            _lastPreviewPoint = mousePoint;

            if (this.ViewModel != null)
            {
                if (this.ViewModel.MouseToolType == MouseToolType.CreateText)
                {
                    #region Annotation

                    // the mouse is not pressed
                    if (_orthPartCreator == null
                        //&& _perspectivePartCreator == null
                        && e.LeftButton == MouseButtonState.Released)
                    {
                        ModelConstruct3D previewModel = null;
                        if (_previewModels == null)
                        {
                            _previewModels = new ModelConstruct3D[1];

                            //add a new preview
                            PreviewComponent component = PreviewComponent.CreateAnnotationPreview();
                            previewModel = new ModelConstruct3D(new PlanComponent3DData(component, new PlanRenderSettings()));
                            previewModel.ModelData.IsWireframe = true;
                            _previewModels[0] = previewModel;
                            this.xManipulatorViewer.Children.Add(previewModel);
                        }
                        else
                        {
                            previewModel = _previewModels[0];
                        }

                        Plan3DData planModelData = this.ViewModel.PlanogramModelData;
                        PreviewComponent previewComponent = (PreviewComponent)(((PlanComponent3DData)previewModel.ModelData).Component);

                        PlanHitTestResult hitTestResult = GetNearestHit(this.xViewer.Viewport, mousePoint, planModelData);
                        if (hitTestResult.NearestSubComponent != null)
                        {
                            previewModel.ModelData.IsVisible = true;


                            PlanSubComponent3DData subHitData = hitTestResult.NearestSubComponent;
                            PointValue subHitLocalPos = hitTestResult.GetSubComponentLocalHitPoint();
                            PointValue relativeHitPoint = new PointValue()
                            {
                                X = subHitData.Position.X + subHitLocalPos.X - (previewComponent.Width / 2F),
                                Y = subHitData.Position.Y + subHitLocalPos.Y - (previewComponent.Height / 2F),
                                Z = subHitData.Position.Z + subHitLocalPos.Z
                            };


                            //calculate the snapped values.
                            PointValue snappedWorldPos;
                            RotationValue snappedWorldRotation;

                            PlanVisualDocument.SnapAndRotateToWorld(subHitData, previewModel.ModelData.Size,
                                relativeHitPoint, new RotationValue(), hitTestResult.HitFace,
                                out snappedWorldPos, out snappedWorldRotation);

                            //apply
                            previewComponent.X = snappedWorldPos.X;
                            previewComponent.Y = snappedWorldPos.Y;
                            previewComponent.Z = snappedWorldPos.Z;
                            previewComponent.Angle = snappedWorldRotation.Angle;
                            previewComponent.Slope = snappedWorldRotation.Slope;
                            previewComponent.Roll = snappedWorldRotation.Roll;

                        }
                        else
                        {
                            previewModel.ModelData.IsVisible = false;
                        }

                        handled = true;
                    }

                    #endregion
                }
                else
                {
                    //we have a create part tool selected
                    PlanogramComponentType? previewType = LocalHelper.MouseToolTypeToPlanogramComponentType(this.ViewModel.MouseToolType);

                    if (previewType.HasValue && previewType != PlanogramComponentType.Panel)
                    {
                        // the mouse is not pressed
                        if (_orthPartCreator == null
                            //&& _perspectivePartCreator == null
                            && e.LeftButton == MouseButtonState.Released)
                        {
                            var defaultSettings = App.ViewState.Settings.Model;

                            //force the manipulater viewer to not be hit test visible
                            if (this.xManipulatorViewer.IsHitTestVisible)
                            {
                                this.xManipulatorViewer.IsHitTestVisible = false;
                            }

                            ModelConstruct3D previewModel = null;
                            if (_previewModels == null)
                            {
                                _previewModels = new ModelConstruct3D[1];

                                //add a new preview
                                PreviewComponent component = PreviewComponent.CreatePreview(previewType.Value, defaultSettings);
                                previewModel = new ModelConstruct3D(new PlanComponent3DData(component, new PlanRenderSettings()));
                                previewModel.ModelData.IsWireframe = true;
                                _previewModels[0] = previewModel;
                                this.xManipulatorViewer.Children.Add(previewModel);
                            }
                            else
                            {
                                previewModel = _previewModels[0];
                            }

                            Plan3DData planModelData = this.ViewModel.PlanogramModelData;
                            PreviewComponent previewComponent = (PreviewComponent)(((PlanComponent3DData)previewModel.ModelData).Component);


                            //apply the default size to the component.
                            WidthHeightDepthValue newSize = LocalHelper.GetDefaultComponentSize((FixtureComponentType)previewType.Value, defaultSettings);
                            previewComponent.SetSize(newSize);


                            PlanHitTestResult hitTestResult = GetNearestSubComponentHit(this.xViewer.Viewport, mousePoint, planModelData);
                            if (hitTestResult.NearestSubComponent != null)
                            {
                                previewModel.ModelData.IsVisible = true;

                                //update the position and size of the preview based on the mouse position
                                PointValue hitPoint3D = ModelConstruct3DHelper.GetModelPoint3D(mousePoint, this.xViewer.Viewport);

                                PointValue newPosition = new PointValue(
                                    hitPoint3D.X,
                                    hitPoint3D.Y - previewComponent.Height / 2F,
                                    hitPoint3D.Z);

                                RotationValue newRotation = new RotationValue();


                                Snap(previewComponent, previewType.Value, hitTestResult, newPosition, newRotation, /*canSnapSize*/true, /*snapToNotch*/true);
                            }
                            else
                            {
                                previewModel.ModelData.IsVisible = false;
                            }

                            handled = true;
                        }
                    }
                }
            }

            return handled;
        }

        /// <summary>
        /// Carries out any mouse up processing required by the current mouse tool.
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        private Boolean MouseToolType_ProcessMouseUp(MouseEventArgs e)
        {
            Boolean handled = false;

            if (_orthPartCreator != null && !_isDraggingAdorner)
            {
                CancelOrthPartCreator();
            }

            if (_previewModels != null)
            {
                ModelConstruct3D previewModel = _previewModels[0];

                if (this.ViewModel.MouseToolType != MouseToolType.Pointer
                    && previewModel.ModelData is PlanComponent3DData)
                {
                    Boolean isAnnotation = (this.ViewModel.MouseToolType == MouseToolType.CreateText);

                    PreviewComponent previewComponent = (PreviewComponent)(((PlanComponent3DData)previewModel.ModelData).Component);

                    PlanFixture3DData nearestFixtureData =
                        GetNearestFixtureData(this.xViewer.Viewport, e.GetPosition(this.xViewer), this.ViewModel.PlanogramModelData);

                    PlanogramFixtureView nearestFixture = (nearestFixtureData != null) ? ((IPlanItem)nearestFixtureData.Fixture).Fixture : null;

                    PointValue worldPos = new PointValue(previewComponent.X, previewComponent.Y, previewComponent.Z);
                    RotationValue worldRotation = new RotationValue(previewComponent.Angle, previewComponent.Slope, previewComponent.Roll);
                    WidthHeightDepthValue size = new WidthHeightDepthValue(previewComponent.Width, previewComponent.Height, previewComponent.Depth);


                    //Add the component.
                    this.ViewModel.AddNewComponent(this.ViewModel.MouseToolType, worldPos, worldRotation, size, nearestFixture);
                    this.ViewModel.MouseToolType = MouseToolType.Pointer;



                    //clear off any leftover preview models.
                    ClearPreviewModels();


                    if (isAnnotation)
                    {
                        //immediately show the properties window.
                        MainPageCommands.ShowSelectedItemProperties.Execute();
                    }

                    handled = true;
                }
                else
                {
                    //something has gone funky here so just clean up the preview models.
                    ClearPreviewModels();
                }
            }

            return handled;
        }

        #endregion

        #region Orth Part Creator

        /// <summary>
        /// Starts a new orth part creator if we are not already
        /// processing one.
        /// </summary>
        private void BeginOrthPartCreator(MouseEventArgs e)
        {
            if (_orthPartCreator == null)
            {
                _orthPartCreator = new DragSelectionAdorner(this.xViewerCanvas, e.GetPosition(this.xViewerCanvas));
                _orthPartCreator.PenColour = Brushes.DarkRed;
                _orthPartCreator.PenDashStyle = null;
            }
        }

        /// <summary>
        /// Carries out any orth part creator processing required when the mouse moves.
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        private Boolean OrthPartCreator_ProcessMouseMove(MouseEventArgs e)
        {
            Boolean handled = false;

            if (_orthPartCreator != null)
            {
                if (_isDraggingAdorner == false && e.LeftButton == MouseButtonState.Pressed)
                {
                    Point curMousePos = e.GetPosition(this.xViewerCanvas);
                    Point startPos = _orthPartCreator.StartPoint.Value;
                    Double difTolerance = 4;

                    //only start the drag if the mouse has moved enough.
                    if ((curMousePos.X - startPos.X) > difTolerance
                        || (startPos.X - curMousePos.X) > difTolerance
                        || (curMousePos.Y - startPos.Y) > difTolerance
                        || (startPos.Y - curMousePos.Y) > difTolerance)
                    {
                        AdornerLayer aLayer = AdornerLayer.GetAdornerLayer(this.xViewerCanvas);
                        if (aLayer != null)
                        {
                            _orthPartCreator.DragSizeChanged += OrthPartCreator_DragSizeChanged;
                            _orthPartCreator.DragSelectionCompleted += OrthPartCreator_PartDefinitionCompleted;
                            aLayer.Add(_orthPartCreator);
                        }

                        _isDraggingAdorner = true;
                        handled = true;
                    }
                }
            }


            return handled;
        }

        /// <summary>
        /// Cancels the ongoing orth part creator tool.
        /// </summary>
        private void CancelOrthPartCreator()
        {
            if (_orthPartCreator != null)
            {
                _orthPartCreator.DragSizeChanged -= OrthPartCreator_DragSizeChanged;
                _orthPartCreator.DragSelectionCompleted -= OrthPartCreator_PartDefinitionCompleted;

                _isDraggingAdorner = false;
                _orthPartCreator.ReleaseMouseCapture();
                _orthPartCreator = null;
            }
        }

        /// <summary>
        /// Called when the drag size changes on the orth part creator.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OrthPartCreator_DragSizeChanged(object sender, RoutedEventArgs e)
        {
            DragSelectionAdorner senderControl = (DragSelectionAdorner)sender;

            Rect rct = senderControl.GetSelectionRect();
            if (!rct.IsEmpty)
            {
                String displayTxt = null;

                RectValue partBounds = GetPartDefinitionSize(rct, this.ViewModel.MouseToolType);
                if (!partBounds.IsEmpty())
                {
                    displayTxt = String.Format("{0} x {1} x {2}", partBounds.Height, partBounds.Width, partBounds.Depth);
                }

                senderControl.DisplayText = displayTxt;
            }
        }

        /// <summary>
        /// Called when the model 3d creator completes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OrthPartCreator_PartDefinitionCompleted(object sender, RoutedEventArgs e)
        {
            DragSelectionAdorner senderControl = (DragSelectionAdorner)sender;
            CancelOrthPartCreator();

            MouseToolType toolType = this.ViewModel.MouseToolType;
            if (MouseToolTypeHelper.IsCreateType(toolType))
            {
                Rect rct = senderControl.GetSelectionRect();
                if (!rct.IsEmpty)
                {
                    RectValue partBounds = GetPartDefinitionSize(rct, toolType);
                    if (!partBounds.IsEmpty())
                    {
                        WidthHeightDepthValue modelSize = new WidthHeightDepthValue(partBounds.Width, partBounds.Height, partBounds.Depth);
                        PointValue modelPos = new PointValue(partBounds.X, partBounds.Y, partBounds.Z);
                        RotationValue modelRotation = new RotationValue();

                        PlanHitTestResult hitResult =
                            GetNearestHit(this.xViewer.Viewport,
                            new Point(rct.BottomLeft.X, rct.BottomLeft.Y),
                            this.ViewModel.PlanogramModelData);


                        IModelConstruct3DData hitModelData = hitResult.NearestSubComponent;
                        if (hitModelData != null)
                        {
                            modelRotation = ModelConstruct3DHelper.GetWorldRotation(hitModelData);
                            switch (hitResult.HitFace)
                            {
                                case ModelConstruct3DHelper.HitFace.Back:
                                    modelRotation.Angle += CommonHelper.ToRadians(-180);
                                    break;

                                case ModelConstruct3DHelper.HitFace.Left:
                                    modelRotation.Angle += CommonHelper.ToRadians(-90);
                                    break;

                                case ModelConstruct3DHelper.HitFace.Right:
                                    modelRotation.Angle += CommonHelper.ToRadians(90);
                                    break;
                            }

                            PointValue subHitLocalPos = hitResult.GetSubComponentLocalHitPoint();

                            PointValue newLocalPosition = new PointValue();
                            newLocalPosition.X = hitModelData.Position.X + subHitLocalPos.X;
                            newLocalPosition.Y = hitModelData.Position.Y + subHitLocalPos.Y;
                            newLocalPosition.Z = hitModelData.Position.Z + subHitLocalPos.Z;

                            modelPos = ModelConstruct3DHelper.ToWorld(newLocalPosition, hitModelData);
                        }

                        //create the part
                        Boolean isAnnotation = (toolType == MouseToolType.CreateText);

                        this.ViewModel.AddNewComponent(toolType, modelPos, modelRotation, modelSize, hitResult.GetNearestFixtureView());
                        this.ViewModel.MouseToolType = MouseToolType.Pointer;

                        if (isAnnotation)
                        {
                            //immediately show the properties window.
                            MainPageCommands.ShowSelectedItemProperties.Execute();
                        }
                    }

                }
            }

        }

        /// <summary>
        /// Returns the bounding size rectable for the given orth dimensions.
        /// </summary>
        /// <param name="rct"></param>
        /// <param name="toolType"></param>
        /// <returns></returns>
        private RectValue GetPartDefinitionSize(Rect rct, MouseToolType toolType)
        {
            RectValue returnRect = new RectValue();
            if (!rct.IsEmpty)
            {
                Viewport3D viewport = this.xViewer.Viewport;

                PointValue btmLeft = ModelConstruct3DHelper.GetModelPoint3D(rct.BottomLeft, viewport);
                PointValue topRight = ModelConstruct3DHelper.GetModelPoint3D(rct.TopRight, viewport);

                Single partWidth = 0;
                Single partHeight = 0;
                Single partDepth = 0;

                Single posX = 0;
                Single posY = 0;
                Single posZ = 0;

                var settings = App.ViewState.Settings.Model;

                //Set defaults
                WidthHeightDepthValue size = new WidthHeightDepthValue();
                FixtureComponentType? toolComponentType = LocalHelper.MouseToolTypeToComponentType(toolType);
                if (toolComponentType.HasValue)
                {
                    size = LocalHelper.GetDefaultComponentSize(toolComponentType.Value, settings);

                    partHeight = size.Height;
                    partWidth = size.Width;
                    partDepth = size.Depth;
                }
                else
                {
                    switch (toolType)
                    {
                        case MouseToolType.CreateText:
                            partHeight = 20;
                            partWidth = 40;
                            partDepth = 1;
                            break;

                        default:
                            throw new NotImplementedException();
                    }
                }



                CameraViewType camType = this.ViewModel.ViewType;

                switch (this.ViewModel.ViewType)
                {
                    case CameraViewType.Design:
                    case CameraViewType.Front:
                        {
                            partWidth = topRight.X - btmLeft.X;
                            partHeight = topRight.Y - btmLeft.Y;
                            posX = btmLeft.X;
                            posY = btmLeft.Y;
                        }
                        break;

                    case CameraViewType.Back:
                        {
                            partWidth = btmLeft.X - topRight.X;
                            partHeight = topRight.Y - btmLeft.Y;
                            posX = topRight.X;
                            posY = btmLeft.Y;
                        }
                        break;

                    case CameraViewType.Top:
                        {
                            partWidth = topRight.X - btmLeft.X;
                            partDepth = btmLeft.Z - topRight.Z;
                            posX = btmLeft.X;
                            posZ = topRight.Z;
                        }
                        break;

                    case CameraViewType.Bottom:
                        {
                            partWidth = topRight.X - btmLeft.X;
                            partDepth = topRight.Z - btmLeft.Z;
                            posX = btmLeft.X;
                            posZ = btmLeft.Z;
                        }
                        break;

                    case CameraViewType.Left:
                        {
                            partDepth = topRight.Z - btmLeft.Z;
                            partHeight = topRight.Y - btmLeft.Y;
                            posY = btmLeft.Y;
                            posZ = btmLeft.Z;
                        }
                        break;

                    case CameraViewType.Right:
                        {
                            partDepth = btmLeft.Z - topRight.Z;
                            partHeight = topRight.Y - btmLeft.Y;
                            posY = btmLeft.Y;
                            posZ = topRight.Z;
                        }
                        break;

                }


                returnRect = new RectValue(posX, posY, posZ,
                    Convert.ToSingle(Math.Round(partWidth)),
                    Convert.ToSingle(Math.Round(partHeight)),
                    Convert.ToSingle(Math.Round(partDepth)));
            }

            return returnRect;
        }

        #endregion

        #region Perspective Part Creator

        ///// <summary>
        ///// Starts a new perspective part creator if we are not
        ///// already processing one.
        ///// </summary>
        //private void BeginPerspectivePartCreator(MouseEventArgs e)
        //{
        //    if (_perspectivePartCreator == null)
        //    {
        //        _perspectivePartCreator = new Model3DCreator();
        //        _perspectivePartCreator.PartDefinitionCompleted += PerspectivePartCreator_PartDefinitionCompleted;
        //        this.xManipulatorViewer.Children.Add(_perspectivePartCreator);
        //        _perspectivePartCreator.StartMouseCapture(e, this.xViewer.Viewport);
        //    }
        //}

        ///// <summary>
        ///// Carries out any perspective part creator processing required when the mouse moves.
        ///// </summary>
        ///// <param name="e"></param>
        ///// <returns></returns>
        //private Boolean PerspectivePartCreator_ProcessMouseMove(MouseEventArgs e)
        //{
        //    Boolean handled = false;

        //    if (_perspectivePartCreator != null)
        //    {
        //        _perspectivePartCreator.ProcessMouseMove(e);
        //        //UpdateStatusBar(_perspectivePartCreator.GetStatusText());
        //        handled = true;
        //    }

        //    return handled;
        //}

        ///// <summary>
        ///// Processes mouse left button up for the perspective part creator as required.
        ///// </summary>
        //private Boolean PerspectivePartCreator_ProcesssMouseLeftButtonUp(MouseButtonEventArgs e)
        //{
        //    Boolean handled = false;

        //    if (_perspectivePartCreator != null)
        //    {
        //        _perspectivePartCreator.ProcessMouseUp(e);
        //        handled = true;
        //    }

        //    return handled;
        //}

        ///// <summary>
        ///// Called when the model 3d creator completes.
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void PerspectivePartCreator_PartDefinitionCompleted(object sender, EventArgs e)
        //{
        //    Model3DCreator senderControl = (Model3DCreator)sender;

        //    Point3D pos = senderControl.Position;
        //    Size3D size = senderControl.Size;

        //    PlanogramFixtureView nearestFixture = null;

        //    Rect3D totalFixtureBounds;
        //    var boundsDict = PlanDocumentHelper.GetFixtureBounds(this.ViewModel.Planogram, out totalFixtureBounds);
        //    foreach(var entry in boundsDict)
        //    {
        //        if(entry.Value.Contains(pos))
        //        {
        //            nearestFixture = entry.Key;
        //            break;
        //        }
        //    }

        //    this.ViewModel.AddNewComponent(
        //        this.ViewModel.MouseToolType,
        //        new PointValue(pos.X, pos.Y, pos.Z),
        //        new RotationValue(),
        //        new WidthHeightDepthValue(size.X, size.Y, size.Z),
        //        nearestFixture);


        //    CancelPerspectivePartCreator();
        //}

        ///// <summary>
        ///// Cancels the perspective part creation mode
        ///// </summary>
        //private void CancelPerspectivePartCreator()
        //{
        //    if (_perspectivePartCreator != null)
        //    {
        //        Model3DCreator partCreator = _perspectivePartCreator;

        //        //cancel part creation.
        //        partCreator.ReleaseMouseCapture();

        //        partCreator.PartDefinitionCompleted -= PerspectivePartCreator_PartDefinitionCompleted;

        //        this.xManipulatorViewer.Children.Remove(partCreator);

        //        _perspectivePartCreator = null;

        //        if (this.ViewModel != null)
        //        {
        //            this.ViewModel.MouseToolType = MouseToolType.Pointer;
        //        }
        //    }
        //}

        #endregion

        #endregion

        #region Rubberband Selection

        /// <summary>
        /// Processes mouse move actions for the drag selection as required
        /// </summary>
        /// <param name="e"></param>
        private Boolean DragSelectionAdorner_ProcessMouseMove(MouseEventArgs e)
        {
            Boolean handled = false;

            if (_dragSelectionAdorner != null && _isDraggingAdorner == false
                    && e.LeftButton == MouseButtonState.Pressed)
            {
                AdornerLayer aLayer = AdornerLayer.GetAdornerLayer(this.xViewerCanvas);
                if (aLayer != null)
                {
                    _dragSelectionAdorner.DragSelectionCompleted += DragSelectionAdorner_SelectionCompleted;
                    aLayer.Add(_dragSelectionAdorner);
                }

                _isDraggingAdorner = true;

                handled = true;
            }

            return handled;
        }

        /// <summary>
        /// Called when a drag selector completes its selection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DragSelectionAdorner_SelectionCompleted(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            DragSelectionAdorner senderControl = (DragSelectionAdorner)sender;
            senderControl.DragSelectionCompleted -= DragSelectionAdorner_SelectionCompleted;

            _isDraggingAdorner = false;
            senderControl.ReleaseMouseCapture();
            _dragSelectionAdorner = null;

            Viewport3D viewport = this.xViewer.Viewport;

            Rect selectionRect = senderControl.GetSelectionRect();
            //  Check whether there is a selection rect or not.
            if (selectionRect.IsEmpty ||
                (!(selectionRect.Size.Height > 1) && !(selectionRect.Size.Width > 1)))
            {
                Mouse.OverrideCursor = null;
                return;
            }

            //  Get all the plan items that intersect with the selection rect.
            var selectionItems = new List<IPlanItem>();
            foreach (ModelConstruct3D model in _modelVisual.GetAllChildModels())
            {
                //TODO: We are selecting using 2d bounds but this might have issues where angles are involved..
                Rect bounds = ModelConstruct3DHelper.GetModel2DBounds(viewport, model, Rect.Empty, /*thisOnly*/true);
                if (!selectionRect.IntersectsWith(bounds)) continue;

                //  Check whether there is a selectable plan item.
                var planPart3DData = model.ModelData as IPlanPart3DData;
                if (planPart3DData == null) continue;
                var item = planPart3DData.PlanPart as IPlanItem;

                //  For Fixtures, make sure all of it is selected.
                if (item == null ||
                    (item.PlanItemType == PlanItemType.Fixture || item.PlanItemType == PlanItemType.Component ||
                     item.PlanItemType == PlanItemType.SubComponent && !item.SubComponent.IsMerchandisable) &&
                    !selectionRect.Contains(bounds)) continue;

                item = PlanItemHelper.GetSelectableItem(item);

                //  Check whether there really is a selectable item that is not already in the selection.
                if (item == null ||
                    selectionItems.Contains(item)) continue;

                switch (item.PlanItemType)
                {
                    case PlanItemType.SubComponent:
                        selectionItems.Remove(item.Component);
                        selectionItems.Remove(item.Assembly);
                        selectionItems.Remove(item.Fixture);
                        break;

                    case PlanItemType.Component:
                        selectionItems.Remove(item.Assembly);
                        selectionItems.Remove(item.Fixture);
                        break;

                    case PlanItemType.Assembly:
                        selectionItems.Remove(item.Fixture);
                        break;
                }

                //  Check whether the user can select the item.
                if (!this.ViewModel.CanUserSelect(item)) continue;

                //  Add it to the selection.
                selectionItems.Add(item);
            }

            //  Check whether there is actually anything selected.
            if (selectionItems.Count <= 0)
            {
                Mouse.OverrideCursor = null;
                return;
            }

            if (this.ViewModel.SelectedPlanItems.Count > 0) this.ViewModel.SelectedPlanItems.Clear();

            //  NB: This should not be needed here. Any process using a selection of plan items should 
            //      make sure it is processing them in the correct order for the operation.
            // At this stage, we've got a collection of plan items ready to add to the view model's collection.
            // However, any positions are arranged in the order that they were selected. This isn't consistent
            // with shift select behaviour, where the positions are arranged in sequence order. In order to solve this
            // we split out any non-position plan items, sequence the position plan items, and then union the 
            // two collections together before adding the view model.
            IEnumerable<IPlanItem> nonPositionPlanItems = selectionItems.Where(p => p.Position == null);
            IEnumerable<IPlanItem> positionPlanItems = selectionItems.Where(p => p.Position != null);
            if (positionPlanItems.Any())
            {
                Dictionary<PlanogramPositionView, IPlanItem> positionPlanItemsByPositionView =
                    positionPlanItems.ToDictionary(p => p.Position, p => p);

                IEnumerable<PlanogramPositionView> sequenceSnake =
                    PlanogramSequenceGroup.EnumerateSequenceSnake<PlanogramPositionView>(
                        PlanogramBlockingGroup.NewPlanogramBlockingGroup(),
                        positionPlanItems.Select(p => p.Position));

                positionPlanItems = sequenceSnake.Select(p => positionPlanItemsByPositionView[p]);
            }

            this.ViewModel.SelectedPlanItems.AddRange(nonPositionPlanItems.Union(positionPlanItems));

            Mouse.OverrideCursor = null;
        }

        #endregion

        #region Drag/Drop Actions

        #region Existing Plan Item Drag/Drop

        /// <summary>
        /// Starts a drag action on the currently selected plan items.
        /// </summary>
        /// <param name="e"></param>
        private void BeginPlanItemDrag(MouseEventArgs e)
        {
            _lastPreviewPoint = null;

            PointValue viewportHitCenter = ModelConstruct3DHelper.GetModelPoint3D(e.GetPosition(this.xViewer), this.xViewer.Viewport);

            PlanItemDragDropArgs itemArgs = new PlanItemDragDropArgs { IsCreateCopy = (Keyboard.Modifiers == ModifierKeys.Control) };

            //get a list of items to be moved but with positions at the end in sequence order
            List<IPlanItem> itemsToMove = new List<IPlanItem>(this.ViewModel.SelectedPlanItems.Count);

            // Check that the cursor is actually over one of the selected items and that we haven't
            // just picked up a click whilst the mouse was in motion.
            if (ViewModel.SelectedPlanItems.All(p => !Equals(p, GetHitPlanItem(e)))) return;

            // Override the above to allow fixtures to be dragged within the plan
            itemsToMove.AddRange(this.ViewModel.SelectedPlanItems);

            // only include components where we are not moving their parent
            foreach (IPlanItem item in itemsToMove.ToArray())
            {
                Boolean remove = false;
                switch (item.PlanItemType)
                {
                    case PlanItemType.Component:
                        if (item.Assembly != null && itemsToMove.Contains(item.Assembly)) remove = true;
                        if (itemsToMove.Contains(item.Fixture)) remove = true;
                        break;

                    case PlanItemType.Assembly:
                        if (itemsToMove.Contains(item.Fixture)) remove = true;
                        break;

                    case PlanItemType.Annotation:
                        if (itemsToMove.Contains(item.Component)) remove = true;
                        if (item.Assembly != null && itemsToMove.Contains(item.Assembly)) remove = true;
                        if (itemsToMove.Contains(item.Fixture)) remove = true;
                        break;
                }

                if (remove) itemsToMove.Remove(item);
            }

            //  NB Am I not getting this code or is it not doing anything really?
            //  All positions have been added previously and never removed,
            //      And the two last lines... remove the item and then add it back?
            //      Is this just to sort them?
            //      If that is it, the ones that have their parents dragged along will be kept in their original place in the list.
            //      The same result is better obtained filtering and sorting when using the collection.
            foreach (IPlanItem posItem in OrderPositionsAtTheEnd(ViewModel.SelectedPlanItems).Where(p => p.PlanItemType == PlanItemType.Position))
            {
                //only include the position if we are not moving any of its parents
                if (itemsToMove.Contains(posItem.Component)) continue;
                if (posItem.Assembly != null && itemsToMove.Contains(posItem.Assembly)) continue;
                if (itemsToMove.Contains(posItem.Fixture)) continue;

                if (itemsToMove.Contains(posItem)) itemsToMove.Remove(posItem);
                itemsToMove.Add(posItem);
            }

            //Create the item models and drag arguments
            foreach (IPlanItem item in itemsToMove)
            {
                ModelConstruct3D modelVis = RenderControlsHelper.FindItemModel(_modelVisual, item);
                if (modelVis != null)
                {
                    PointValue worldPos = ModelConstruct3DHelper.GetWorldPosition(modelVis.ModelData);

                    PointValue dragOffset =
                        new PointValue()
                        {
                            X = worldPos.X - viewportHitCenter.X,
                            Y = worldPos.Y - viewportHitCenter.Y,
                            Z = worldPos.Z - viewportHitCenter.Z
                        };

                    itemArgs.DragItems.Add(
                        new PlanDragDropItem()
                        {
                            PlanItem = item,
                            Model3DVis = modelVis,
                            DragOffset = dragOffset
                        });
                }
            }

            //  When dragging Positions with other items, remove any positions that are not going with their parent.
            itemArgs.RemoveOrphanedDragDropPositions();

            itemArgs.DragSource = this;
            DragDropBehaviour dragArgs = new DragDropBehaviour(itemArgs);
            dragArgs.DragComplete += PlanItemDragDrop_DragComplete;

            var mainDockMgr = FindParentByName(this, "xDockingManagerMain");
            if (mainDockMgr == null)
            {
                Console.WriteLine("Could not locate the root DockingManager");
                dragArgs.DragArea = this;
            }
            else
            {
                dragArgs.DragArea = mainDockMgr;
            }

            // Make an intermediate copy across plans
            MainPageCommands.DragCopy.Execute();

            dragArgs.BeginDrag();
        }

        private FrameworkElement FindParentByName(FrameworkElement startElement, String parentElementName)
        {
            if (startElement == null || string.IsNullOrEmpty(parentElementName))
            {
                return startElement;
            }

            var returnElement = startElement;
            var currentElement = startElement;

            currentElement = (FrameworkElement)VisualTreeHelper.GetParent(currentElement);

            while (currentElement != null)
            {
                if (currentElement.Name == parentElementName)
                {
                    // we have a name match on the controls so return this
                    returnElement = currentElement;
                    break;
                }

                currentElement = (FrameworkElement)VisualTreeHelper.GetParent(currentElement);
            }

            return returnElement;
        }

        /// <summary>
        /// Called whenever a plan item is dragged over this control
        /// </summary>
        private void OnPlanItemDragOver(PlanItemDragDropArgs planItemDragArgs, DragEventArgs e)
        {
            //don't bother processing if the mouse has not moved.
            Point mousePoint = e.GetPosition(this.xViewer);
            if (_lastPreviewPoint == mousePoint) { return; }
            _lastPreviewPoint = mousePoint;

            // Check there are some actual dragged items
            if (planItemDragArgs.DragItems.Count == 0)
            {
                return;
            }

            // Create drag models if they don't yet exist
            if (_previewModels == null)
            {
                _previewModels = planItemDragArgs.CreatePreviewModels(ViewModel.IsDynamicTextScalingEnabled);
                if (_previewModels != null)
                {
                    foreach (ModelConstruct3D previewModel in _previewModels)
                    {
                        xManipulatorViewer.Children.Add(previewModel);
                    }
                }
            }

            if (_previewModels != null)
            {
                //+ Update dragging model positions.
                Viewport3D vp = this.xViewer.Viewport;
                Point hitPoint = e.GetPosition(this.xViewer);
                PointValue hitPoint3D = ModelConstruct3DHelper.GetModelPoint3D(hitPoint, vp);
                VectorValue changeVector = ModelConstruct3DHelper.FindItemMoveVector(this.xViewer.Camera, 1, 1).ToVectorValue();

                //create a list of models to be ignored while hit testing.
                List<IModelConstruct3DData> ignoreList = new List<IModelConstruct3DData>();
                for (Int32 i = 0; i < planItemDragArgs.DragItems.Count; i++)
                {
                    PlanDragDropItem dragItem = planItemDragArgs.DragItems[i];
                    ModelConstruct3D previewModel = _previewModels[i];

                    if (dragItem.Model3DVis == null)
                        dragItem.Model3DVis = previewModel;

                    if (dragItem.Model3DVis == null) continue;

                    if (PlanItemHelper.IsFixtureType(dragItem.PlanItem.PlanItemType))
                    {
                        ignoreList.AddRange(ModelConstruct3DHelper.FindChildrenOfType<PlanSubComponent3DData>(dragItem.Model3DVis.ModelData));
                    }
                    else
                    {
                        ignoreList.Add(dragItem.Model3DVis.ModelData);
                    }
                }

                PlanHitTestResult posDragSubHitResult = null;
                RotationValue posDragRotation = new RotationValue();
                List<PlanogramPositionView> draggedPositions = new List<PlanogramPositionView>();
                List<PointValue> positionWorldDropPoints = new List<PointValue>();

                for (Int32 i = 0; i < planItemDragArgs.DragItems.Count; i++)
                {
                    PlanDragDropItem dragItem = planItemDragArgs.DragItems[i];
                    ModelConstruct3D previewModel = _previewModels[i];
                    IModelConstruct3DData actualModelData = dragItem.Model3DVis.ModelData;

                    PointValue oldWorldPos = ModelConstruct3DHelper.GetWorldPosition(actualModelData);

                    PointValue newPosition =
                                new PointValue
                                {
                                    X = (changeVector.X != 0) ? (hitPoint3D.X + dragItem.DragOffset.X) : oldWorldPos.X,
                                    Y = (changeVector.Y != 0) ? (hitPoint3D.Y + dragItem.DragOffset.Y) : oldWorldPos.Y,
                                    Z = (changeVector.Z != 0) ? (hitPoint3D.Z + dragItem.DragOffset.Z) : oldWorldPos.Z
                                };

                    switch (dragItem.PlanItem.PlanItemType)
                    {
                        #region Fixture

                        case PlanItemType.Fixture:
                            {
                                //limit the position to above 0 as fixtures are not
                                // allowed to move outside of those bounds.
                                newPosition.X = Math.Max(0, newPosition.X);
                                newPosition.Y = Math.Max(0, newPosition.Y);
                                newPosition.Z = Math.Max(0, newPosition.Z);

                                //determine the mouse intersection with the floor.
                                Point3D flooredPos = newPosition.ToPoint3D();

                                Point3D? floorIntersection =
                                    Viewport3DHelper.GetRay(vp, hitPoint).PlaneIntersection(new Point3D(0, 0, 0), new Vector3D(0, 1, 0));
                                if (floorIntersection.HasValue)
                                {
                                    flooredPos = floorIntersection.Value;
                                }

                                flooredPos.X = Math.Max(0, newPosition.X);
                                flooredPos.Y = 0;
                                flooredPos.Z = Math.Max(0, newPosition.Z);

                                //try to get a hit based on the fixture position.
                                Point fixtureHitPoint = Viewport3DHelper.Point3DtoPoint2D(vp, flooredPos);

                                PlanHitTestResult subHitResult = GetNearestHit(this.xViewer.Viewport,
                                                                               fixtureHitPoint,
                                                                               this.ViewModel.PlanogramModelData,
                                                                               ignoreList);

                                dragItem.LastHitTestResult = subHitResult;

                                //set the new position and rotation against the drag model
                                previewModel.ModelData.Position = flooredPos.ToPointValue();
                                previewModel.ModelData.Rotation = ModelConstruct3DHelper.GetWorldRotation(actualModelData);
                            }
                            break;

                        #endregion

                        #region Assembly/Component/Annotation

                        case PlanItemType.Assembly:
                        case PlanItemType.Component:
                        case PlanItemType.SubComponent:
                        case PlanItemType.Annotation:
                            {
                                PlanHitTestResult subHitResult = null;
                                Point fixtureHitPoint = Viewport3DHelper.Point3DtoPoint2D(vp, newPosition.ToPoint3D());

                                if (planItemDragArgs.DragItems.Count == 1)
                                {
                                    //Try to snap to the backboard.


                                    //[V8-28385] get the hit based on the mouse pointer
                                    // this makes moving a single item between bays much easier

                                    //[V8-30190] Still use the hitpoint y offset though so that the mouse
                                    // does not snap to the bottom of the component.

                                    Point testPoint = new Point(hitPoint.X, fixtureHitPoint.Y);

                                    subHitResult =
                                        GetNearestHit(this.xViewer.Viewport,
                                                      testPoint,
                                                      this.ViewModel.PlanogramModelData,
                                                      ignoreList);

                                    //if the item width does not match the 
                                    PlanogramSubComponentView hitSubView =
                                        (subHitResult != null && subHitResult.NearestSubComponent != null)
                                            ? subHitResult.NearestSubComponent.SubComponent as PlanogramSubComponentView
                                            : null;

                                    if (hitSubView == null ||
                                        hitSubView.Component.ComponentType != PlanogramComponentType.Backboard
                                        ||
                                        dragItem.PlanItem.Width != hitSubView.Width)
                                    {
                                        subHitResult = null;
                                    }
                                }

                                if (subHitResult == null)
                                {
                                    //try to get a hit based on the fixture position.
                                    subHitResult =
                                        GetNearestSubComponentHit(this.xViewer.Viewport,
                                                                  fixtureHitPoint,
                                                                  this.ViewModel.PlanogramModelData,
                                                                  ignoreList);

                                    dragItem.LastHitTestResult = subHitResult;
                                }
                                dragItem.LastHitTestResult = subHitResult;


                                if (subHitResult.NearestSubComponent != null)
                                {
                                    PlanSubComponent3DData subHitData = subHitResult.NearestSubComponent;
                                    PointValue subHitLocalPos = subHitResult.GetSubComponentLocalHitPoint();
                                    PointValue relativeHitPoint = new PointValue()
                                                                  {
                                                                      X = subHitData.Position.X + subHitLocalPos.X,
                                                                      Y = subHitData.Position.Y + subHitLocalPos.Y,
                                                                      Z = subHitData.Position.Z + subHitLocalPos.Z
                                                                  };


                                    //calculate the snapped values.
                                    PointValue snappedWorldPos;
                                    RotationValue snappedWorldRotation;

                                    PlanVisualDocument.SnapAndRotateToWorld(subHitData,
                                                                            previewModel.ModelData.Size,
                                                                            relativeHitPoint,
                                                                            new RotationValue(),
                                                                            subHitResult.HitFace,
                                                                            out snappedWorldPos,
                                                                            out snappedWorldRotation);

                                    if (snappedWorldPos.Z != previewModel.ModelData.Position.Z + 1)
                                    {
                                        snappedWorldPos.Z = previewModel.ModelData.Position.Z;
                                    }

                                    //apply
                                    previewModel.ModelData.Position = snappedWorldPos;
                                    previewModel.ModelData.Rotation = snappedWorldRotation;
                                }
                                else
                                {
                                    //set the new position and rotation against the drag model
                                    previewModel.ModelData.Position = newPosition;
                                    previewModel.ModelData.Rotation = ModelConstruct3DHelper.GetWorldRotation(actualModelData);
                                }
                            }
                            break;

                        #endregion

                        #region Position

                        case PlanItemType.Position:
                            if (posDragSubHitResult == null)
                            {
                                posDragSubHitResult = GetNearestHit(this.xViewer.Viewport, hitPoint, this.ViewModel.PlanogramModelData, ignoreList);

                                if (posDragSubHitResult.NearestSubComponent != null)
                                {
                                    posDragRotation = ModelConstruct3DHelper.GetWorldRotation(posDragSubHitResult.NearestSubComponent);

                                    //amend the position by the rotation change
                                    RotationValue origRotation = ModelConstruct3DHelper.GetWorldRotation(actualModelData.ParentModelData);
                                    RotationValue offsetRotation = new RotationValue(
                                        posDragRotation.Angle - origRotation.Angle,
                                        posDragRotation.Slope - origRotation.Slope,
                                        posDragRotation.Roll - origRotation.Roll);

                                    if (offsetRotation.Angle > 0 ||
                                        offsetRotation.Slope > 0 ||
                                        offsetRotation.Roll > 0)
                                    {
                                        PointValue rotationOffset = dragItem.DragOffset.Transform(MatrixValue.CreateRotationMatrix(offsetRotation));
                                        newPosition.X = newPosition.X + rotationOffset.X;
                                        newPosition.Y = newPosition.Y + rotationOffset.Y;
                                        newPosition.Z = newPosition.Z + rotationOffset.Z;
                                    }
                                }
                            }

                            dragItem.LastHitTestResult = posDragSubHitResult;

                            previewModel.ModelData.Rotation = posDragRotation;
                            previewModel.ModelData.Position = newPosition;


                            draggedPositions.Add(dragItem.PlanItem.Position);
                            positionWorldDropPoints.Add(previewModel.ModelData.Position);
                            break;

                        #endregion
                    }
                }


                //Position Drop arrow:
                if (draggedPositions.Count > 0 && draggedPositions.Count == planItemDragArgs.DragItems.Count)
                {
                    ShowPositionDragArrow(vp, hitPoint, posDragSubHitResult, draggedPositions);
                }
            }
        }

        /// <summary>
        /// Called whenever a plan item drag action is dropped.
        /// </summary>
        private void OnPlanItemDropped(PlanItemDragDropArgs planItemDragArgs, DragEventArgs e)
        {
            if (planItemDragArgs == null) return;
            if (_previewModels == null) { return; }
            if (planItemDragArgs.DragItems.Count == 0) { return; }

            // Check if the source is different from the target
            //if so, then we should always copy.
            Boolean draggedFromSamePlan = (planItemDragArgs.DragSource == this);
            if (!draggedFromSamePlan) planItemDragArgs.IsCreateCopy = true;

            PlanogramView plan = ViewModel.Planogram;
            Plan3DData planModelData = ViewModel.PlanogramModelData;
            Viewport3D vp = xViewer.Viewport;
            ProjectionCamera camera = xViewer.Camera;
            Point hitPoint = e.GetPosition(xViewer);

            //clear the drag indicator arrow.
            if (_dragIndicatorArrow != null)
            {
                xManipulatorViewer.Children.Remove(_dragIndicatorArrow);
                _dragIndicatorArrow = null;
            }

            //start a new undoable action.
            plan.BeginUndoableAction();

            List<IPlanItem> newSelection = new List<IPlanItem>();

            List<Tuple<PlanogramPositionView, PointValue>> draggedPositions = new List<Tuple<PlanogramPositionView, PointValue>>();
            PlanHitTestResult posDragHitResult = null;

            //  Get all plan items that have positions already selected to be moved. These should not copy their children.
            List<IPlanItem> planItemsWithPositions = planItemDragArgs.GetPlanItemsWithPositions();
            Boolean createCopy = planItemDragArgs.IsCreateCopy;
            for (Int32 i = 0; i < planItemDragArgs.DragItems.Count; i++)
            {
                PlanDragDropItem dragItem = planItemDragArgs.DragItems[i];
                IPlanItem item = dragItem.PlanItem;
                ModelConstruct3D previewModel = _previewModels[i];
                IModelConstruct3DData actualModelData = dragItem.Model3DVis?.ModelData;

                PlanHitTestResult lastHitResult = dragItem.LastHitTestResult as PlanHitTestResult;
                if (lastHitResult == null) continue;

                switch (item.PlanItemType)
                {
                    #region Fixture/Assembly/Component

                    case PlanItemType.Fixture:
                    case PlanItemType.Assembly:
                    case PlanItemType.Component:
                    case PlanItemType.SubComponent:
                        {

                            PointValue newWorldPos = previewModel.ModelData.Position;
                            RotationValue newWorldRotation = previewModel.ModelData.Rotation;

                            //create the copy if required.
                            if (createCopy)
                            {
                                item = this.ViewModel.Planogram.AddPlanItemCopy(item,
                                    /*shiftHeight*/false,
                                    /*copyChildPositions*/App.ViewState.SelectionMode != PlanItemSelectionType.OnlyComponents && !planItemsWithPositions.Contains(item), null, false);
                                if (item == null) { continue; }

                                //GEM32402 - update the model data here also as may change plan (different parents).
                                actualModelData = RenderControlsHelper.FindItemModelData(planModelData, item);
                            }

                            //Check if the item needs to be moved to another fixture.
                            if (lastHitResult != null
                                && lastHitResult.GetNearestFixtureView() != item.Fixture)
                            {
                                PlanogramFixtureView nearestFixtureView = lastHitResult.GetNearestFixtureView();
                                if (nearestFixtureView != null)
                                {
                                    if (item.PlanItemType == PlanItemType.Component)
                                    {
                                        item = nearestFixtureView.MoveComponent(item.Component);
                                    }
                                    else if (item.PlanItemType == PlanItemType.Assembly)
                                    {
                                        item = nearestFixtureView.MoveAssembly(item.Assembly);
                                    }

                                    //update the model data.
                                    actualModelData = RenderControlsHelper.FindItemModelData(planModelData, item);
                                }
                            }

                            // update the item position.
                            if (actualModelData != null)
                            {
                                if (lastHitResult != null && lastHitResult.NearestSubComponent != null)
                                {
                                    PlanogramSubComponentView subView = (PlanogramSubComponentView)lastHitResult.NearestSubComponent.SubComponent;

                                    //don't need to resnap as this will have already been calculated by drag over.
                                    PlanItemHelper.SetPosition(item, ModelConstruct3DHelper.ToLocal(newWorldPos, actualModelData).Round());

                                    RotationValue curWorldRotation = ModelConstruct3DHelper.GetWorldRotation(actualModelData);
                                    PlanItemHelper.SetRotation(item, new RotationValue(
                                                newWorldRotation.Angle - curWorldRotation.Angle,
                                                newWorldRotation.Slope - curWorldRotation.Slope,
                                                newWorldRotation.Roll - curWorldRotation.Roll));

                                    //if this is a component then snap to the nearest notch.
                                    if (item.PlanItemType == PlanItemType.Component)
                                    {
                                        item.Component.SnapToNearestNotch();
                                    }
                                }
                                else
                                {
                                    Vector3D changeVector = ModelConstruct3DHelper.FindItemMoveVector(camera, 1, 1);
                                    PointValue newLocalPos = ModelConstruct3DHelper.ToLocal(newWorldPos, actualModelData);
                                    if (changeVector.X != 0) item.X = Convert.ToSingle(newLocalPos.X);
                                    if (changeVector.Y != 0) item.Y = Convert.ToSingle(newLocalPos.Y);
                                    if (changeVector.Z != 0) item.Z = Convert.ToSingle(newLocalPos.Z);
                                }
                            }

                            newSelection.Add(item);
                        }
                        break;

                    #endregion

                    #region Annotation

                    case PlanItemType.Annotation:
                        {
                            PointValue newWorldPos = previewModel.ModelData.Position;
                            RotationValue newWorldRotation = previewModel.ModelData.Rotation;

                            //create the copy if required.
                            if (createCopy)
                            {
                                item = this.ViewModel.Planogram.AddPlanItemCopy(item,
                                    /*shiftHeight*/false,
                                    /*copyChildPositons*/App.ViewState.SelectionMode != PlanItemSelectionType.OnlyComponents);
                                if (item == null) { continue; }

                                //GEM32402 - update the model data here also as may change plan (different parents).
                                actualModelData = RenderControlsHelper.FindItemModelData(planModelData, item);
                            }

                            //Check if the item needs to be moved to another fixture.
                            //we only do this for fixture and subcomponent annotations.
                            if (lastHitResult != null)
                            {
                                if (item.Annotation.IsFixtureAnnotation)
                                {
                                    if (lastHitResult.NearestFixture != null && lastHitResult.NearestFixture.Fixture != item.Fixture)
                                    {
                                        PlanogramFixtureView fixtureView = lastHitResult.GetNearestFixtureView();
                                        if (fixtureView != null)
                                        {
                                            item = fixtureView.MoveAnnotation(item.Annotation);
                                            actualModelData = RenderControlsHelper.FindItemModelData(planModelData, item);
                                        }
                                    }
                                }
                                else if (item.Annotation.IsSubComponentAnnotation)
                                {
                                    if (lastHitResult.NearestSubComponent != null
                                                && lastHitResult.NearestSubComponent.SubComponent != item.SubComponent)
                                    {
                                        PlanogramSubComponentView subView = lastHitResult.NearestSubComponent.SubComponent as PlanogramSubComponentView;
                                        if (subView != null)
                                        {
                                            item = subView.MoveAnnotation(item.Annotation);
                                            actualModelData = RenderControlsHelper.FindItemModelData(planModelData, item);
                                        }
                                    }
                                }


                                //Update the item position and rotation.
                                PlanItemHelper.SetPosition(item, ModelConstruct3DHelper.ToLocal(newWorldPos, actualModelData).Round());

                                RotationValue curWorldRotation = ModelConstruct3DHelper.GetWorldRotation(actualModelData);
                                PlanItemHelper.SetRotation(item, new RotationValue(
                                            newWorldRotation.Angle - curWorldRotation.Angle,
                                            newWorldRotation.Slope - curWorldRotation.Slope,
                                            newWorldRotation.Roll - curWorldRotation.Roll));
                            }
                            else
                            {
                                //update the item position using the change vector.
                                Vector3D changeVector = ModelConstruct3DHelper.FindItemMoveVector(camera, 1, 1);
                                PointValue newLocalPos = ModelConstruct3DHelper.ToLocal(newWorldPos, actualModelData).Round();
                                if (changeVector.X != 0) item.X = Convert.ToSingle(newLocalPos.X);
                                if (changeVector.Y != 0) item.Y = Convert.ToSingle(newLocalPos.Y);
                                if (changeVector.Z != 0) item.Z = Convert.ToSingle(newLocalPos.Z);
                            }

                            newSelection.Add(item);
                        }
                        break;

                    #endregion

                    #region Position

                    case PlanItemType.Position:
                        {
                            draggedPositions.Add(new Tuple<PlanogramPositionView, PointValue>(item.Position, previewModel.ModelData.Position));
                            posDragHitResult = lastHitResult;
                        }
                        break;

                    #endregion
                }
            }

            #region Positions

            //Process any dragged positions.
            if (draggedPositions.Any())
            {
                PlanSubComponent3DData nearestMerchSubData = GetNearestMerchandisableSubComponent(vp, hitPoint, planModelData);
                if (nearestMerchSubData != null)
                {
                    PlanogramSubComponentView subComponent = (PlanogramSubComponentView)nearestMerchSubData.SubComponent;
                    subComponent.BeginUpdate();

                    List<PlanogramPositionView> newPositions = new List<PlanogramPositionView>();

                    PlanPosition3DData anchorData;
                    PlanogramPositionAnchorDirection? anchorDirection;
                    ViewModel.GetPositionAnchor(posDragHitResult, nearestMerchSubData, draggedPositions.Select(i=> i.Item1), out anchorData, out anchorDirection);

                    if (anchorData != null && anchorDirection.HasValue)
                    {
                        if (createCopy)
                        {
                            newPositions.AddRange(subComponent.AddPositionCopies(draggedPositions.Select(i => i.Item1),
                                (PlanogramPositionView)anchorData.PlanPosition, anchorDirection.Value));
                        }
                        else
                        {
                            var itemPlanogram = draggedPositions.First().Item1.Planogram;
                            if (itemPlanogram != plan)
                            {
                                newPositions.AddRange(subComponent.AddPositionCopies(draggedPositions.Select(i => i.Item1),
                                    (PlanogramPositionView)anchorData.PlanPosition, anchorDirection.Value));
                                itemPlanogram.Model.Positions.RemoveList(draggedPositions.Select(o => o.Item1.Model));
                            }
                            else
                            {
                                newPositions.AddRange(subComponent.MovePositions(draggedPositions.Select(i => i.Item1),
                                    (PlanogramPositionView)anchorData.PlanPosition, anchorDirection.Value));
                            }
                        }
                    }
                    else
                    {
                        //add each position according to their drop points
                        //so that we do not lose drag offsets.
                        foreach (var p in draggedPositions)
                        {
                            newPositions.AddRange(subComponent.PastePositionViews(
                                new List<PlanogramPositionView> { p.Item1 },
                                ModelConstruct3DHelper.ToLocal(p.Item2, nearestMerchSubData)
                                , createCopy));
                        }

                    }

                    subComponent.EndUpdate();
                    newSelection.AddRange(newPositions);
                }

            }
            #endregion


            //if the items are from a different plan and ctrl is not pressed
            //then we should remove the originals.
            if (!draggedFromSamePlan
                && Keyboard.Modifiers != ModifierKeys.Control)
            {
                MainPageCommands.RemoveSelected.Execute();
            }


            plan.EndUndoableAction();

            //apply the new selection
            ViewModel.SelectedPlanItems.SetSelection(newSelection);
        }

        /// <summary>
        /// Cleans up when the plan item drag has completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanItemDragDrop_DragComplete(object sender, EventArgs e)
        {
            DragDropBehaviour dragArgs = sender as DragDropBehaviour;
            dragArgs.DragComplete -= PlanItemDragDrop_DragComplete;

            ClearPreviewModels();
        }

        #endregion

        #region Product Drag/Drop

        /// <summary>
        /// Carries out actions required to process a product row drag over.
        /// </summary>
        private void OnProductDragOver(ExtendedDataGridRowDropEventArgs args, DragEventArgs e)
        {
            if (args.Items.Count == 0) return;

            //don't bother processing if the mouse has not moved.
            Point mousePoint = e.GetPosition(this.xViewer);
            if (_lastPreviewPoint == mousePoint) { return; }
            _lastPreviewPoint = mousePoint;


            Viewport3D vp = this.xViewer.Viewport;
            Point hitPoint = e.GetPosition(this.xViewer);
            PlanHitTestResult posDragSubHitResult = GetNearestHit(this.xViewer.Viewport, hitPoint, this.ViewModel.PlanogramModelData);

            //Position Drop arrow:
            ShowPositionDragArrow(vp, hitPoint, posDragSubHitResult, new List<PlanogramPositionView>());
        }

        /// <summary>
        /// Called whenever a product list row is dropped here
        /// </summary>
        private void OnProductRowsDropped(ExtendedDataGridRowDropEventArgs args, DragEventArgs e)
        {
            ClearPositionDragArrow();
            if (!args.Items.Any()) return;

            CompletePlanogramProductViewsDrop(args.Items.Cast<ProductListRow>().Select(i => i.PlanItem.Product), e.GetPosition(xViewer));
        }

        /// <summary>
        /// Carries out actions when ProductLibrary items are dropped
        /// </summary>
        private void OnProductLibraryDropped(ExtendedDataGridRowDropEventArgs args, DragEventArgs e)
        {
            ClearPositionDragArrow();
            if (args.Items.Count == 0) { return; }

            List<Product> dragProducts = args.Items.Cast<Product>().ToList();

            Viewport3D vp = this.xViewer.Viewport;
            Point hitPoint = e.GetPosition(this.xViewer);
            PointValue hitPoint3D = ModelConstruct3DHelper.GetModelPoint3D(hitPoint, vp);

            PlanHitTestResult posDragHitResult = GetNearestHit(this.xViewer.Viewport, hitPoint, this.ViewModel.PlanogramModelData);
            if (posDragHitResult == null) return;

            PlanSubComponent3DData nearestMerchSubData = GetNearestMerchandisableSubComponent(vp, hitPoint, this.ViewModel.PlanogramModelData);
            if (nearestMerchSubData == null) return;

            PlanogramSubComponentView subComponent = (PlanogramSubComponentView)nearestMerchSubData.SubComponent;

            PlanPosition3DData anchorData;
            PlanogramPositionAnchorDirection? anchorDirection;
            this.ViewModel.GetPositionAnchor(posDragHitResult, nearestMerchSubData, new List<PlanogramPositionView>(), out anchorData, out anchorDirection);

            if (anchorData != null && anchorDirection.HasValue)
            {
                this.ViewModel.AddNewPositions(subComponent, dragProducts, (PlanogramPositionView)anchorData.PlanPosition, anchorDirection.Value);
            }
            else
            {
                //just add to the shelf
                this.ViewModel.AddNewPositions(subComponent, ModelConstruct3DHelper.ToLocal(hitPoint3D, nearestMerchSubData), dragProducts);
            }
        }

        /// <summary>
        /// Carries out actions when ProductLibraryProduct items are dropped
        /// </summary>
        private void OnProductLibraryProductDropped(ExtendedDataGridRowDropEventArgs args, DragEventArgs e)
        {
            ClearPositionDragArrow();
            if (args.Items.Count == 0) { return; }

            List<ProductLibraryProduct> dragProducts = args.Items.Cast<ProductLibraryProduct>().ToList();

            Viewport3D vp = this.xViewer.Viewport;
            Point hitPoint = e.GetPosition(this.xViewer);
            PointValue hitPoint3D = ModelConstruct3DHelper.GetModelPoint3D(hitPoint, vp);

            PlanHitTestResult posDragHitResult = GetNearestHit(this.xViewer.Viewport, hitPoint, this.ViewModel.PlanogramModelData);
            if (posDragHitResult == null) return;

            PlanSubComponent3DData nearestMerchSubData = GetNearestMerchandisableSubComponent(vp, hitPoint, this.ViewModel.PlanogramModelData);
            if (nearestMerchSubData == null) return;

            PlanogramSubComponentView subComponent = (PlanogramSubComponentView)nearestMerchSubData.SubComponent;

            PlanPosition3DData anchorData;
            PlanogramPositionAnchorDirection? anchorDirection;
            this.ViewModel.GetPositionAnchor(posDragHitResult, nearestMerchSubData, new List<PlanogramPositionView>(), out anchorData, out anchorDirection);

            if (anchorData != null && anchorDirection.HasValue)
            {
                this.ViewModel.AddNewPositions(subComponent, dragProducts, (PlanogramPositionView)anchorData.PlanPosition, anchorDirection.Value);
            }
            else
            {
                //just add to the shelf
                this.ViewModel.AddNewPositions(subComponent, ModelConstruct3DHelper.ToLocal(hitPoint3D, nearestMerchSubData), dragProducts);
            }
        }

        /// <summary>
        /// Carries out actions when PlanogramAssortmentProduct items are dropped
        /// </summary>
        private void OnPlanogramProductViewDropped(ExtendedDataGridRowDropEventArgs args, DragEventArgs e)
        {
            ClearPositionDragArrow();

            if (!args.Items.Any()) return;

            CompletePlanogramProductViewsDrop(args.Items.Cast<PlanogramProductView>(), e.GetPosition(xViewer));
        }

        /// <summary>
        ///     Complete a drag drop operation for a collection of <see cref="PlanogramProductView"/> around a known <paramref name="hitPoint"/>.
        /// </summary>
        /// <param name="dragProducts"></param>
        /// <param name="hitPoint"></param>
        private void CompletePlanogramProductViewsDrop(IEnumerable<PlanogramProductView> dragProducts, Point hitPoint)
        {
            Viewport3D vp = xViewer.Viewport;

            PlanHitTestResult posDragHitResult = GetNearestHit(vp, hitPoint, ViewModel.PlanogramModelData);
            if (posDragHitResult == null) return;

            PlanSubComponent3DData nearestMerchSubData = GetNearestMerchandisableSubComponent(vp, hitPoint, ViewModel.PlanogramModelData);
            if (nearestMerchSubData == null) return;

            var subComponent = (PlanogramSubComponentView)nearestMerchSubData.SubComponent;

            PlanPosition3DData anchorData;
            PlanogramPositionAnchorDirection? anchorDirection;
            ViewModel.GetPositionAnchor(posDragHitResult, nearestMerchSubData, new List<PlanogramPositionView>(), out anchorData, out anchorDirection);
            if (anchorData != null &&
                anchorDirection.HasValue)
                ViewModel.AddNewPositions(subComponent, dragProducts, (PlanogramPositionView)anchorData.PlanPosition, anchorDirection.Value);
            else
            {
                //just add to the shelf
                PointValue hitPoint3D = ModelConstruct3DHelper.GetModelPoint3D(hitPoint, vp);
                ViewModel.AddNewPositions(subComponent, ModelConstruct3DHelper.ToLocal(hitPoint3D, nearestMerchSubData), dragProducts);
            }
        }

        #endregion

        #region FixtureLibrary Drag/Drop

        /// <summary>
        /// Carries out actions required to process a fixture library item drag over.
        /// </summary>
        /// <param name="args"></param>
        /// <param name="e"></param>
        private void OnFixtureLibraryDragOver(FixtureLibraryItemDropEventArgs args, DragEventArgs e)
        {
            //Don't show a preview atm as fixtures will always get placed to the right.
            if (args.Fixture.FixtureCount > 0) return;

            //don't bother processing if the mouse has not moved.
            Point mousePoint = e.GetPosition(this.xViewer);
            if (_lastPreviewPoint == mousePoint) { return; }
            _lastPreviewPoint = mousePoint;


            //+ Create drag models if they don't yet exist
            if (_previewModels == null || _previewModels.Count() != 1)
            {
                ModelConstruct3D[] models = new ModelConstruct3D[1];

                //create a very simple box
                ModelConstruct3DData boxModelData = new ModelConstruct3DData();
                boxModelData.Size = new WidthHeightDepthValue(args.Fixture.Width, args.Fixture.Height, args.Fixture.Depth);

                BoxModelPart3DData boxPartData = new BoxModelPart3DData(boxModelData);
                boxPartData.Size = new WidthHeightDepthValue(args.Fixture.Width, args.Fixture.Height, args.Fixture.Depth);
                boxModelData.AddPart(boxPartData);

                boxModelData.IsWireframe = true;

                ModelConstruct3D boxModel = new ModelConstruct3D(boxModelData);
                this.xManipulatorViewer.Children.Add(boxModel);
                models[0] = boxModel;

                _previewModels = models;
            }


            ModelConstruct3D model = _previewModels[0];

            //+ Update dragging model positions.
            Viewport3D vp = this.xViewer.Viewport;
            Point hitPoint = e.GetPosition(this.xViewer);
            PointValue hitPoint3D = ModelConstruct3DHelper.GetModelPoint3D(hitPoint, vp);
            VectorValue changeVector = ModelConstruct3DHelper.FindItemMoveVector(this.xViewer.Camera, 1, 1).ToVectorValue();

            if (args.Fixture.FixtureCount == 0)
            {

                PointValue newPosition =
                            new PointValue()
                            {
                                X = (changeVector.X != 0) ? (hitPoint3D.X - model.Size.Width / 2) : model.Position.X,
                                Y = (changeVector.Y != 0) ? (hitPoint3D.Y - model.Size.Height / 2) : model.Position.Y,
                                Z = (changeVector.Z != 0) ? (hitPoint3D.Z - model.Size.Depth / 2) : model.Position.Z
                            };

                //try to get a hit based on the fixture position.
                Point fixtureHitPoint = Viewport3DHelper.Point3DtoPoint2D(vp, newPosition.ToPoint3D());

                PlanHitTestResult subHitResult = GetNearestHit(this.xViewer.Viewport, fixtureHitPoint, this.ViewModel.PlanogramModelData, null);

                if (subHitResult.NearestSubComponent != null)
                {
                    PlanSubComponent3DData subHitData = subHitResult.NearestSubComponent;
                    PointValue subHitLocalPos = subHitResult.GetSubComponentLocalHitPoint();
                    PointValue relativeHitPoint = new PointValue()
                    {
                        X = subHitData.Position.X + subHitLocalPos.X,
                        Y = subHitData.Position.Y + subHitLocalPos.Y,
                        Z = subHitData.Position.Z + subHitLocalPos.Z
                    };


                    //calculate the snapped values.
                    PointValue snappedWorldPos;
                    RotationValue snappedWorldRotation;

                    PlanVisualDocument.SnapAndRotateToWorld(subHitData, model.ModelData.Size,
                        relativeHitPoint, new RotationValue(), subHitResult.HitFace,
                        out snappedWorldPos, out snappedWorldRotation);

                    //apply
                    model.ModelData.Position = snappedWorldPos;
                    model.ModelData.Rotation = snappedWorldRotation;
                }
                else
                {
                    model.ModelData.Position =
                     new PointValue()
                     {
                         X = Math.Max(0, (changeVector.X != 0) ? (hitPoint3D.X - model.Size.Width / 2) : model.Position.X),
                         Y = Math.Max(0, (changeVector.Y != 0) ? (hitPoint3D.Y - model.Size.Height / 2) : model.Position.Y),
                         Z = Math.Max(0, (changeVector.Z != 0) ? (hitPoint3D.Z - model.Size.Depth / 2) : model.Position.Z)
                     };

                    model.ModelData.Rotation = new RotationValue();
                }

            }
            else
            {
                //floor it.
                model.ModelData.Position =
                    new PointValue()
                    {
                        X = Math.Max(0, (changeVector.X != 0) ? (hitPoint3D.X - model.Size.Width / 2) : model.Position.X),
                        Y = 0,
                        Z = Math.Max(0, (changeVector.Z != 0) ? (hitPoint3D.Z - model.Size.Depth / 2) : model.Position.Z)
                    };
            }
        }

        /// <summary>
        /// Processes a drop of a fixture library item.
        /// </summary>
        /// <param name="args"></param>
        /// <param name="e"></param>
        private void OnFixtureLibraryDropped(FixtureLibraryItemDropEventArgs args, DragEventArgs e)
        {
            if (args.Fixture != null)
            {
                Plan3DData planModelData = this.ViewModel.PlanogramModelData;

                var subHitResult = GetNearestHit(this.xViewer.Viewport, e.GetPosition(this.xViewer), planModelData);

                List<IPlanItem> addedItems = this.ViewModel.AddFixtureLibraryItem(args.Fixture, subHitResult);

                if (addedItems != null && _previewModels != null && _previewModels.Any())
                {
                    ModelConstruct3D previewModel = _previewModels[0];

                    //if we have a preview model then make use of its position.
                    if (addedItems.Count == 1)
                    {
                        IModelConstruct3DData modelData = RenderControlsHelper.FindItemModelData(planModelData, (addedItems[0]));
                        if (modelData != null)
                        {
                            PlanItemHelper.SetPosition(addedItems[0], ModelConstruct3DHelper.ToLocal(previewModel.Position, modelData).Round());
                        }
                    }
                }
            }
            ClearPreviewModels();
        }

        #endregion

        /// <summary>
        /// Shows the drag indicator arrow for positions
        /// </summary>
        /// <param name="draggedPositions">the positions being dragged</param>
        /// <param name="vp">the viewport3d</param>
        /// <param name="hitPoint">the mouse hit point</param>
        /// <param name="posDragSubHitResult">the hit result.</param>
        private void ShowPositionDragArrow(Viewport3D vp, Point hitPoint, PlanHitTestResult posDragSubHitResult, List<PlanogramPositionView> ignorePositions)
        {
            PlanSubComponent3DData nearestMerchSubData = GetNearestMerchandisableSubComponent(vp, hitPoint, this.ViewModel.PlanogramModelData);

            if (nearestMerchSubData == null)
            {
                //we aren't close to any merchandisable sub so just make sure the arrow isn't showing.
                ClearPositionDragArrow();
                return;
            }


            PlanogramSubComponentView sub = (PlanogramSubComponentView)nearestMerchSubData.SubComponent;


            //create and add the arrow if we don't already have one.
            if (_dragIndicatorArrow == null)
            {
                _dragIndicatorArrow = new ArrowVisual3D()
                {
                    Diameter = 1.5D,
                    HeadLength = 2,
                    Fill = this.Resources["PositionIndicatorBrush"] as Brush
                };
                this.xManipulatorViewer.Children.Add(_dragIndicatorArrow);
            }

            //point to the top of the sub
            Point3D indicatorStart = new Point3D(sub.Width / 2, sub.Height, sub.Depth);
            Vector3D arrowDirection = new Vector3D(0, -1, 0);


            PlanPosition3DData anchorPosData;
            PlanogramPositionAnchorDirection? anchorDirection;
            this.ViewModel.GetPositionAnchor(posDragSubHitResult, nearestMerchSubData, ignorePositions, out anchorPosData, out anchorDirection);

            if (anchorPosData != null && anchorDirection != null)
            {
                IPlanPositionRenderable pos = anchorPosData.PlanPosition;

                switch (anchorDirection.Value)
                {
                    case PlanogramPositionAnchorDirection.InFront:
                        arrowDirection = new Vector3D(0, 0, -1);
                        indicatorStart = new Point3D(pos.Width / 2, pos.Height / 2, pos.Depth);
                        break;

                    case PlanogramPositionAnchorDirection.Behind:
                        arrowDirection = new Vector3D(0, 0, 1);
                        indicatorStart = new Point3D(pos.Width / 2, pos.Height / 2, 0);
                        break;

                    case PlanogramPositionAnchorDirection.Above:
                        arrowDirection = new Vector3D(0, -1, 0);
                        indicatorStart = new Point3D(pos.Width / 2, pos.Height, pos.Depth / 2);
                        break;

                    case PlanogramPositionAnchorDirection.Below:
                        arrowDirection = new Vector3D(0, 1, 0);
                        indicatorStart = new Point3D(pos.Width / 2, 0, pos.Depth / 2);
                        break;

                    case PlanogramPositionAnchorDirection.ToLeft:
                        arrowDirection = new Vector3D(1, 0, 0);
                        indicatorStart = new Point3D(0, pos.Height / 2, pos.Depth / 2);
                        break;

                    case PlanogramPositionAnchorDirection.ToRight:
                        arrowDirection = new Vector3D(-1, 0, 0);
                        indicatorStart = new Point3D(pos.Width, pos.Height / 2, pos.Depth / 2);
                        break;
                }


            }



            //Double arrowLength = 8;


            Double arrowScaleFactor = GetScreenDist();
            _dragIndicatorArrow.Diameter = 4D / arrowScaleFactor;
            Double arrowLength = 55 / arrowScaleFactor;


            Point3D indicatorEnd =
                new Point3D()
                {
                    X = indicatorStart.X - (arrowDirection.X * arrowLength),
                    Y = indicatorStart.Y - (arrowDirection.Y * arrowLength),
                    Z = indicatorStart.Z - (arrowDirection.Z * arrowLength),
                };

            _dragIndicatorArrow.Point1 = indicatorEnd;
            _dragIndicatorArrow.Point2 = indicatorStart;

            //rotate the arrow according to the anchor position rotation
            if (anchorDirection.HasValue && anchorPosData != null)
            {
                RotationValue anchorRotation = ModelConstruct3DHelper.GetWorldRotation(anchorPosData);
                PointValue anchorPosition = ModelConstruct3DHelper.GetWorldPosition(anchorPosData);
                _dragIndicatorArrow.Transform = ModelConstruct3DHelper.CreateTransform(anchorRotation, anchorPosition);
            }
            else
            {
                //use the sub rotation
                RotationValue anchorRotation = ModelConstruct3DHelper.GetWorldRotation(nearestMerchSubData);
                PointValue anchorPosition = ModelConstruct3DHelper.GetWorldPosition(nearestMerchSubData);
                _dragIndicatorArrow.Transform = ModelConstruct3DHelper.CreateTransform(anchorRotation, anchorPosition);
            }

        }

        /// <summary>
        /// Clears off the position drag arrow
        /// </summary>
        private void ClearPositionDragArrow()
        {
            if (_dragIndicatorArrow != null)
            {
                this.xManipulatorViewer.Children.Remove(_dragIndicatorArrow);
                _dragIndicatorArrow = null;
            }
        }

        private Double GetScreenDist()
        {
            Viewport3D vp = this.xViewer.Viewport;
            if (vp == null) return 1;

            Vector3D upVector = ModelConstruct3DHelper.FindItemMoveVector(vp.Camera as ProjectionCamera, 0, 1);

            Point3D pt1 = new Point3D(0, 0, 0);
            Point3D pt2 = new Point3D(upVector.X, upVector.Y, upVector.Z);
            Double actualDist = pt1.Distance(pt2);


            //project both points to the screen
            Point screenp1 = Viewport3DHelper.Point3DtoPoint2D(vp, pt1);
            Point screenp2 = Viewport3DHelper.Point3DtoPoint2D(vp, pt2);
            Double screenDist = screenp1.Distance(screenp2);

            return screenDist;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the size and placement of the gridlines based on the model.
        /// </summary>
        private void UpdateGridlines()
        {
            GridLinesVisual3D gridlines = this.xGridlines;
            if (gridlines != null && this.ViewModel != null)
            {
                //if we are supressing for performance then
                //just flag this to be updated later.
                if (this.ViewModel.SuppressPerformanceIntensiveUpdates)
                {
                    _isGridlineRefreshRequired = true;
                    return;
                }


                gridlines.Visible = this.ViewModel.ShowGridlines;

                Boolean isAutoSized = this.ViewModel.AutosizeGridlines;
                Double planLinesMargin = (isAutoSized) ? _gridlineAutosizeMargin : 0;

                //Determine the line bounds.
                Rect3D bounds = Rect3D.Empty;
                if (isAutoSized)
                {
                    bounds = ModelConstruct3DHelper.FindBounds(_modelVisual);
                    if (bounds.IsEmpty)
                    {
                        bounds = new Rect3D();

                        if (this.ViewModel.Planogram.Width > 0)
                        {
                            bounds.SizeX = this.ViewModel.Planogram.Width;
                            bounds.SizeY = this.ViewModel.Planogram.Height;
                            bounds.SizeZ = this.ViewModel.Planogram.Depth;
                        }
                        else
                        {
                            bounds.SizeX = 100;
                            bounds.SizeY = 100;
                            bounds.SizeZ = 100;
                        }
                    }
                }
                else
                {
                    bounds =
                        new Rect3D()
                        {
                            SizeX = this.ViewModel.GridlineFixedWidth,
                            SizeY = this.ViewModel.GridlineFixedHeight,
                            SizeZ = this.ViewModel.GridlineFixedDepth
                        };
                }


                //update the gridlines.
                if (!bounds.IsEmpty)
                {
                    gridlines.MajorDistance = this.ViewModel.GridlineMajorDist;
                    gridlines.MinorDistance = this.ViewModel.GridlineMinorDist;

                    Double centerX = bounds.X + (bounds.SizeX / 2);
                    Double centerY = bounds.Y + (bounds.SizeY / 2);
                    Double centerZ = bounds.Z + (bounds.SizeZ / 2);

                    switch (this.ViewModel.ViewType)
                    {
                        case CameraViewType.Perspective:
                            gridlines.Normal = new Vector3D(0, 1, 0);
                            gridlines.LengthDirection = new Vector3D(1, 0, 0);
                            gridlines.Center = new Point3D(centerX, bounds.Y, centerZ);
                            gridlines.Length = bounds.SizeX + planLinesMargin;
                            gridlines.Width = bounds.SizeZ + planLinesMargin;
                            break;

                        case CameraViewType.Front:
                        case CameraViewType.Design:
                            gridlines.Normal = new Vector3D(0, 0, 1);
                            gridlines.LengthDirection = new Vector3D(0, 1, 0);
                            gridlines.Center = new Point3D(centerX, centerY, bounds.Z);
                            gridlines.Length = bounds.SizeY + planLinesMargin;
                            gridlines.Width = bounds.SizeX + planLinesMargin;
                            break;

                        case CameraViewType.Back:
                            gridlines.Normal = new Vector3D(0, 0, 1);
                            gridlines.LengthDirection = new Vector3D(0, 1, 0);
                            gridlines.Center = new Point3D(centerX, centerY, bounds.Z + bounds.SizeZ);
                            gridlines.Length = bounds.SizeY + planLinesMargin;
                            gridlines.Width = bounds.SizeX + planLinesMargin;
                            break;

                        case CameraViewType.Left:
                            gridlines.Normal = new Vector3D(1, 0, 0);
                            gridlines.LengthDirection = new Vector3D(0, 1, 0);
                            gridlines.Center = new Point3D(bounds.X + bounds.SizeX, centerY, centerZ);
                            gridlines.Length = bounds.SizeY + planLinesMargin;
                            gridlines.Width = bounds.SizeZ + planLinesMargin;
                            break;

                        case CameraViewType.Right:
                            gridlines.Normal = new Vector3D(1, 0, 0);
                            gridlines.LengthDirection = new Vector3D(0, 1, 0);
                            gridlines.Center = new Point3D(bounds.X, centerY, centerZ);
                            gridlines.Length = bounds.SizeY + planLinesMargin;
                            gridlines.Width = bounds.SizeZ + planLinesMargin;
                            break;

                        case CameraViewType.Top:
                            gridlines.Normal = new Vector3D(0, 1, 0);
                            gridlines.LengthDirection = new Vector3D(1, 0, 0);
                            gridlines.Center = new Point3D(centerX, bounds.Y, centerZ);
                            gridlines.Length = bounds.SizeX + planLinesMargin;
                            gridlines.Width = bounds.SizeZ + planLinesMargin;
                            break;

                        case CameraViewType.Bottom:
                            gridlines.Normal = new Vector3D(0, 1, 0);
                            gridlines.LengthDirection = new Vector3D(1, 0, 0);
                            gridlines.Center = new Point3D(centerX, bounds.Y + bounds.SizeY, centerZ);
                            gridlines.Length = bounds.SizeX + planLinesMargin;
                            gridlines.Width = bounds.SizeZ + planLinesMargin;
                            break;

                        default:
                            Debug.Fail("Not Implemented");
                            break;
                    }
                }


            }
        }

        /// <summary>
        /// Creates and shows the context menu
        /// </summary>
        private void ShowContextMenu()
        {
            //show the context menu
            Fluent.ContextMenu contextMenu = new Fluent.ContextMenu();
            RelayCommand cmd;


            if (this.ViewModel.SelectedPlanItems.Any())
            {
                Boolean propertiesAdded = false;

                //position properties
                if (MainPageCommands.ShowPositionProperties.CanExecute())
                {
                    cmd = MainPageCommands.ShowPositionProperties;
                    contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });
                    propertiesAdded = true;
                }

                //component properties
                if (MainPageCommands.ShowComponentProperties.CanExecute())
                {
                    cmd = MainPageCommands.ShowComponentProperties;
                    contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });
                    propertiesAdded = true;
                }

                if (!propertiesAdded
                    && MainPageCommands.ShowSelectedItemProperties.CanExecute())
                {
                    //general properties
                    cmd = MainPageCommands.ShowSelectedItemProperties;
                    contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });
                }

                contextMenu.Items.Add(new Separator());

                //position commands
                if (MainPageCommands.IncreasePositionFacings.CanExecute())
                {
                    cmd = MainPageCommands.IncreasePositionFacings;
                    contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });

                    cmd = MainPageCommands.DecreasePositionFacings;
                    contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });

                    contextMenu.Items.Add(new Separator());
                }


                //cut command
                cmd = MainPageCommands.Cut;
                contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });

                //copy
                cmd = MainPageCommands.Copy;
                contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });
            }

            //paste
            cmd = MainPageCommands.Paste;
            var pasteItemHeader = new Fluent.MenuItem() { Header = cmd.FriendlyName, Icon = cmd.SmallIcon };
            contextMenu.Items.Add(pasteItemHeader);

            var pasteItem = new Fluent.MenuItem() { Header = cmd.FriendlyName, Icon = cmd.SmallIcon, IsEnabled = cmd.CanExecute() };
            pasteItem.Tag = Mouse.GetPosition(this.xViewer);
            pasteItem.Click += PasteContextMenuItem_Click;
            pasteItemHeader.Items.Add(pasteItem);


            cmd = MainPageCommands.PasteToAllPlanograms;
            var pasteItemToAll = new Fluent.MenuItem() { Header = cmd.FriendlyName, Icon = cmd.SmallIcon, IsEnabled = cmd.CanExecute() };
            pasteItemToAll.Tag = Mouse.GetPosition(this.xViewer);
            pasteItemToAll.Click += PasteToAllContextMenuItem_Click;
            pasteItemHeader.Items.Add(pasteItemToAll);

            contextMenu.Items.Add(new Separator());

            if (MainPageCommands.CreateAssembly.CanExecute() || MainPageCommands.SplitAssembly.CanExecute())
            {
                if (MainPageCommands.CreateAssembly.CanExecute())
                {
                    //create assembly
                    cmd = MainPageCommands.CreateAssembly;
                    contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });
                }

                if (MainPageCommands.SplitAssembly.CanExecute())
                {
                    //split assembly
                    cmd = MainPageCommands.SplitAssembly;
                    contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });
                }


                contextMenu.Items.Add(new Separator());
            }

            if (MainPageCommands.LinkAnnotation.CanExecute() || MainPageCommands.UnlinkAnnotation.CanExecute())
            {
                //link annotation:
                if (MainPageCommands.LinkAnnotation.CanExecute())
                {
                    cmd = MainPageCommands.LinkAnnotation;
                    contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });
                }

                //unlink annotation:
                if (MainPageCommands.UnlinkAnnotation.CanExecute())
                {
                    cmd = MainPageCommands.UnlinkAnnotation;
                    contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });
                }

                contextMenu.Items.Add(new Separator());
            }


            //add to fixture library
            if (MainPageCommands.AddToFixtureLibrary.CanExecute())
            {
                cmd = MainPageCommands.AddToFixtureLibrary;
                contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });
            }

            //edit component parts
            if (MainPageCommands.EditComponent.CanExecute())
            {
                cmd = MainPageCommands.EditComponent;
                contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });
            }

            if (MainPageCommands.AddToFixtureLibrary.CanExecute() || MainPageCommands.EditComponent.CanExecute())
            {
                contextMenu.Items.Add(new Separator());
            }

            // Update product colour from highlight
            if (MainPageCommands.ApplyHighlightAsProductColour.CanExecute())
            {
                cmd = MainPageCommands.ApplyHighlightAsProductColourToSelection;
                contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });
                contextMenu.Items.Add(new Separator());
            }

            //If we can move the fixture either left or right include the move fixture options
            if (MainPageCommands.FixtureMoveLeft.CanExecute() || MainPageCommands.FixtureMoveRight.CanExecute())
            {
                //Fixture Move Left
                cmd = MainPageCommands.FixtureMoveLeft;
                contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });

                //Fixture Move Right
                cmd = MainPageCommands.FixtureMoveRight;
                contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });

                contextMenu.Items.Add(new Separator());
            }

            //planogram properties
            cmd = MainPageCommands.ShowPlanogramProperties;
            contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });

            //Zooming
            contextMenu.Items.Add(new Separator());

            var zoomHeader = new Fluent.MenuItem() { Header = Message.Ribbon_GroupHeader_Zoom };
            contextMenu.Items.Add(zoomHeader);

            cmd = MainPageCommands.ZoomIn;
            zoomHeader.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });

            cmd = MainPageCommands.ZoomOut;
            zoomHeader.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });

            zoomHeader.Items.Add(new Separator());

            cmd = MainPageCommands.ToggleZoomToAreaMode;
            zoomHeader.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName });

            zoomHeader.Items.Add(new Separator());

            cmd = MainPageCommands.ZoomToFit;
            zoomHeader.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName });

            cmd = MainPageCommands.ZoomToFitHeight;
            zoomHeader.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName });
            
            contextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.MousePoint;
            contextMenu.IsOpen = true;
        }

        /// <summary>
        /// Returns the hit plan item for the given mouse args
        /// </summary>
        private IPlanItem GetHitPlanItem(MouseEventArgs mouseArgs, Boolean isSelectableOnly = true)
        {
            IPlanItem hitPlanItem = null;

            Viewport3D vp = this.xViewer.Viewport;

            var hitResult = VisualTreeHelper.HitTest(vp, mouseArgs.GetPosition(vp));
            if (hitResult != null)
            {
                hitPlanItem = PlanDocumentHelper.GetHitPlanItem(hitResult.VisualHit, isSelectableOnly);
            }

            return hitPlanItem;
        }

        /// <summary>
        /// Clears all preview models from the visual.
        /// </summary>
        private void ClearPreviewModels()
        {
            if (_previewModels != null)
            {
                foreach (var model in _previewModels)
                {
                    this.xManipulatorViewer.Children.Remove(model);
                    model.Dispose();
                }
                _previewModels = null;
                _lastPreviewPoint = null;
            }
            //clear the drag indicator arrow.
            if (_dragIndicatorArrow != null)
            {
                this.xManipulatorViewer.Children.Remove(_dragIndicatorArrow);
                _dragIndicatorArrow = null;
            }

            //also tidy up any old drag args.
            _lastDragArgs = null;
        }

        /// <summary>
        /// Pastes the clipboard at the given mouse position
        /// </summary>
        /// <param name="viewerHitPoint"></param>
        private void PasteClipboardAt(Point viewerHitPoint)
        {
            ReadOnlyCollection<IPlanItem> clipboard = MainPageCommands.GetClipboardContent();

            ViewModel.Planogram.BeginUndoableAction();

            IPlanItem targetItem = null;
            if (clipboard.ToList().All(item => item.Position != null))
            {
                PlanSubComponent3DData nearestMerchSubData = GetNearestMerchandisableSubComponent(xViewer.Viewport, viewerHitPoint, ViewModel.PlanogramModelData);
                if (nearestMerchSubData != null) targetItem = (PlanogramSubComponentView) nearestMerchSubData.SubComponent;
            }

            //first paste all the items back into their original places
            List<IPlanItem> pastedItems = ViewModel.Planogram.AddPlanItemCopies(clipboard.ToList(), targetItem);

            //move the remaining pasted items
            List<IPlanItem> newSelectedItems = MovePlanItemsByChangeVector(viewerHitPoint, pastedItems);

            ViewModel.Planogram.EndUndoableAction();

            //update the selection to the new items.
            ViewModel.SelectedPlanItems.SetSelection(newSelectedItems);
        }

        /// <summary>
        ///     Move all the <paramref name="items"/> relative to the given <paramref name="viewerHitPoint"/>.
        /// </summary>
        /// <param name="viewerHitPoint"></param>
        /// <param name="items"></param>
        /// <returns></returns>
        private List<IPlanItem> MovePlanItemsByChangeVector(Point viewerHitPoint, ICollection<IPlanItem> items)
        {
            //  Return if no items or all are positions/products
            var newSelectedItems = new List<IPlanItem>();
            if (!items.Any() || items.All(item => item.Position != null || item.Product != null)) return newSelectedItems;

            PlanHitTestResult hitResult = GetNearestHit(xViewer.Viewport, viewerHitPoint, ViewModel.PlanogramModelData);
            PointValue worldCoordinate = hitResult.GetSubComponentWorldHitPoint();
            if (hitResult.NearestSubComponent == null) worldCoordinate = ModelConstruct3DHelper.GetModelPoint3D(viewerHitPoint, xViewer.Viewport);

            // Build a Dictionary of the pasted items and their world positions.
            Dictionary<IPlanItem, RectValue> worldPosEntries = new Dictionary<IPlanItem, RectValue>();
            foreach (IPlanItem item in items)
            {
                switch (item.PlanItemType)
                {
                    case PlanItemType.Fixture:
                        worldPosEntries.Add(item, item.Fixture.GetWorldSpaceBounds());
                        break;

                    case PlanItemType.Assembly:
                        worldPosEntries.Add(item, item.Assembly.GetWorldSpaceBounds());
                        break;

                    case PlanItemType.Component:
                        worldPosEntries.Add(item, item.Component.GetWorldSpaceBounds());
                        break;

                    case PlanItemType.SubComponent:
                        worldPosEntries.Add(item, item.SubComponent.GetWorldSpaceBounds());
                        break;

                    case PlanItemType.Annotation:
                        var anno = item.Annotation;
                        worldPosEntries.Add(item, new RectValue(anno.WorldX, anno.WorldY, anno.WorldZ, anno.Width, anno.Height, anno.Depth));
                        break;
                }
            }

            VectorValue changeVector = ModelConstruct3DHelper.FindItemMoveVector(xViewer.Camera, 1, 1).ToVectorValue();

            var curItemsWorldPos = new PointValue(worldPosEntries.Values.Min(p => p.X),
                                                         worldPosEntries.Values.Min(p => p.Y),
                                                         worldPosEntries.Values.Min(p => p.Z));
            var newItemsWorldPos =
                new PointValue
                {
                    X = worldCoordinate.X - (worldPosEntries.Values.Max(p => p.X + p.Width) - worldPosEntries.Values.Min(p => p.X))/2F,
                    Y = worldCoordinate.Y - (worldPosEntries.Values.Max(p => p.Y + p.Height) - worldPosEntries.Values.Min(p => p.Y))/2F,
                    Z = worldCoordinate.Z - (worldPosEntries.Values.Max(p => p.Z + p.Depth) - worldPosEntries.Values.Min(p => p.Z))/2F
                };

            // For an assembly, just use the existing co-ordinates i.e. do not subtract half the assembly width / height / depth
            PointValue newAssemblyItemsWorldPos = new PointValue()
                                                  {
                                                      X = worldCoordinate.X,
                                                      Y = worldCoordinate.Y,
                                                      Z = worldCoordinate.Z
                                                  };

            VectorValue offset = (newItemsWorldPos - curItemsWorldPos);
            offset.X = offset.X*changeVector.X;
            offset.Y = offset.Y*changeVector.Y;
            offset.Z = offset.Z*changeVector.Z;

            VectorValue assemblyOffset = (newAssemblyItemsWorldPos - curItemsWorldPos);
            assemblyOffset.X = assemblyOffset.X*changeVector.X;
            assemblyOffset.Y = assemblyOffset.Y*changeVector.Y;
            assemblyOffset.Z = assemblyOffset.Z*changeVector.Z;

            foreach (IPlanItem item in worldPosEntries.Keys)
            {
                RectValue origBounds = worldPosEntries[item];
                VectorValue itemOffset = new PointValue(origBounds.X, origBounds.Y, origBounds.Z) - curItemsWorldPos;

                PointValue newWorldPos = new PointValue(
                    curItemsWorldPos.X + itemOffset.X + offset.X,
                    curItemsWorldPos.Y + itemOffset.Y + offset.Y,
                    curItemsWorldPos.Z + itemOffset.Z + offset.Z);

                switch (item.PlanItemType)
                {
                    case PlanItemType.Fixture:
                        // We don't need to do anything with fixtures, because the AddPlanItemCopy
                        // method will pasted move the fixture to the right of the copied fixture.
                        // Any further adjustments here could place the fixture in a bizarre location.
                        break;

                    case PlanItemType.Assembly:
                    {
                        PointValue newAssemblyWorldPos = new PointValue(
                            curItemsWorldPos.X + itemOffset.X + assemblyOffset.X,
                            curItemsWorldPos.Y + itemOffset.Y + assemblyOffset.Y,
                            curItemsWorldPos.Z + itemOffset.Z + assemblyOffset.Z);

                        PlanogramAssemblyView assembly = item.Assembly;

                        if (hitResult.NearestSubComponent != null)
                        {
                            PointValue snappedWorldPos;
                            RotationValue snappedWorldRotation;

                            PlanVisualDocument.SnapAndRotateToWorld(hitResult.NearestSubComponent,
                                                                    new WidthHeightDepthValue(assembly.Width, assembly.Height, assembly.Depth),
                                                                    ModelConstruct3DHelper.ToLocal(newAssemblyWorldPos, hitResult.NearestSubComponent),
                                                                    new RotationValue(),
                                                                    hitResult.HitFace,
                                                                    out snappedWorldPos,
                                                                    out snappedWorldRotation);

                            assembly.SetWorldCoordinates(snappedWorldPos);
                        }
                        else
                            assembly.SetWorldCoordinates(newWorldPos);

                        //nb - agreed with Steve that for the time being we will not snap this to notch. 

                        //reget the view of the assembly as it may have changed
                        assembly = ViewModel.Planogram.EnumerateAllAssemblies().FirstOrDefault(c => c.AssemblyModel == item.Assembly.AssemblyModel);
                        if (assembly != null) newSelectedItems.Add(assembly);
                    }
                        break;

                    case PlanItemType.Component:
                    {
                        PlanogramComponentView component = item.Component;

                        if (hitResult.NearestSubComponent != null)
                        {
                            PointValue snappedWorldPos;
                            RotationValue snappedWorldRotation;

                            PlanVisualDocument.SnapAndRotateToWorld(hitResult.NearestSubComponent,
                                                                    new WidthHeightDepthValue(component.Width, component.Height, component.Depth),
                                                                    ModelConstruct3DHelper.ToLocal(newWorldPos, hitResult.NearestSubComponent),
                                                                    new RotationValue(),
                                                                    hitResult.HitFace,
                                                                    out snappedWorldPos,
                                                                    out snappedWorldRotation);

                            component.SetWorldCoordinates(snappedWorldPos);
                            component.SnapToNearestNotch();
                        }
                        else
                            component.SetWorldCoordinates(newWorldPos);

                        //reget the view of the component as it may have changed
                        component = ViewModel.Planogram.EnumerateAllComponents().FirstOrDefault(c => c.ComponentModel == item.Component.ComponentModel);
                        if (component != null) newSelectedItems.Add(component);
                    }
                        break;

                    case PlanItemType.Annotation:
                        item.Annotation.SetWorldCoordinates(newWorldPos);
                        break;
                }
            }

            return newSelectedItems;
        }

        private Point3D? UnProject(Point p, Point3D position, Vector3D normal)
        {
            Ray3D ray = GetRay(p);
            if (ray == null)
            {
                return null;
            }

            Point3D i;
            return ray.PlaneIntersection(position, normal, out i) ? (Point3D?)i : null;
        }

        /// <summary>
        /// Get the ray into the view volume given by the position in 2D (screen coordinates)
        /// </summary>
        /// <param name="position">
        /// A 2D point.
        /// </param>
        /// <returns>
        /// A ray
        /// </returns>
        private Ray3D GetRay(Point position)
        {
            Point3D point1, point2;
            bool ok = Viewport3DHelper.Point2DtoPoint3D(this.xViewer.Viewport, position, out point1, out point2);
            if (!ok)
            {
                return null;
            }

            return new Ray3D { Origin = point1, Direction = point2 - point1 };
        }

        #endregion

        #region IPlanDocumentView Members

        public IPlanDocument GetIPlanDocument()
        {
            return this.ViewModel as IPlanDocument;
        }

        public void Dispose()
        {
            this.ViewModel = null;
        }

        #endregion

        #region Static Helpers

        /// <summary>
        /// Returns the pan vector for the controller
        /// </summary>
        /// <param name="dx"></param>
        /// <param name="dy"></param>
        /// <param name="controller"></param>
        /// <returns></returns>
        private Vector3D FindPanVector(Double dx, Double dy, CameraController controller)
        {
            var axis1 = Vector3D.CrossProduct(controller.CameraLookDirection, controller.CameraUpDirection);
            var axis2 = Vector3D.CrossProduct(axis1, controller.CameraLookDirection);
            axis1.Normalize();
            axis2.Normalize();
            Double l = controller.CameraLookDirection.Length;
            Double f = l * 0.001;
            var move = (-axis1 * f * dx) + (axis2 * f * dy);

            // this should be dependent on distance to target?
            return move;
        }

        #region Snapping / Collision Detection

        /// <summary>
        /// Returns a result for the nearest hit model.
        /// </summary>
        private static PlanHitTestResult GetNearestHit(Viewport3D vp, Point hitPoint, Plan3DData planModelData)
        {
            return GetNearestHit(vp, hitPoint, planModelData, new List<IModelConstruct3DData>());
        }

        /// <summary>
        /// Returns a result for the nearest hit model ignoring hits against
        /// models in the ignore list.
        /// </summary>
        private static PlanHitTestResult GetNearestHit(Viewport3D vp, Point hitPoint, Plan3DData planModelData, List<IModelConstruct3DData> ignoreList)
        {
            PlanHitTestResult hitResult = new PlanHitTestResult();
            hitResult.ViewerHitPoint = hitPoint;
            hitResult.ViewerHitPoint3D = ModelConstruct3DHelper.GetModelPoint3D(hitPoint, vp);

            var constructHits = ModelConstruct3DHelper.FindModelConstructHits(vp, hitPoint);

            foreach (var h in constructHits)
            {
                if (ignoreList == null || !ignoreList.Contains(h.Model.ModelData))
                {

                    hitResult.ActualHitModelData = h.Model.ModelData;

                    hitResult.HitFace = ModelConstruct3DHelper.NormalToFacing(h.Normal);
                    hitResult.ModelLocalHitPoint = h.RayPointHit.ToPointValue();

                    hitResult.NearestSubComponent = ModelConstruct3DHelper.FindAncestorData<PlanSubComponent3DData>(hitResult.ActualHitModelData);
                    hitResult.NearestFixture = ModelConstruct3DHelper.FindAncestorData<PlanFixture3DData>(hitResult.ActualHitModelData);

                    //correct the hit face if possible
                    ModelConstruct3DHelper.HitFace? hitFace = ModelConstruct3DHelper.GetFace(hitResult.ActualHitModelData, hitResult.ModelLocalHitPoint);
                    if (hitFace.HasValue)
                    {
                        hitResult.HitFace = hitFace.Value;
                    }

                    break;
                }
            }

            //if no fixture found, then try to get one directly.
            if (hitResult.NearestFixture == null)
            {
                hitResult.NearestFixture = GetNearestFixtureData(vp, hitPoint, planModelData);
            }


            return hitResult;
        }

        /// <summary>
        /// Returns a result for the nearest hit model ignoring hits against
        /// models in the ignore list.
        /// </summary>
        private static PlanHitTestResult GetNearestSubComponentHit(Viewport3D vp, Point hitPoint, Plan3DData planModelData, List<IModelConstruct3DData> ignoreList = null)
        {
            PlanHitTestResult hitResult = new PlanHitTestResult();
            hitResult.ViewerHitPoint = hitPoint;
            hitResult.ViewerHitPoint3D = ModelConstruct3DHelper.GetModelPoint3D(hitPoint, vp);

            var constructHits = ModelConstruct3DHelper.FindModelConstructHits(vp, hitPoint);

            foreach (var h in constructHits)
            {
                if (h.Model.ModelData is PlanSubComponent3DData
                    && (ignoreList == null || !ignoreList.Contains(h.Model.ModelData)))
                {

                    hitResult.ActualHitModelData = h.Model.ModelData;

                    hitResult.HitFace = ModelConstruct3DHelper.NormalToFacing(h.Normal);
                    hitResult.ModelLocalHitPoint = h.RayPointHit.ToPointValue();

                    hitResult.NearestSubComponent = ModelConstruct3DHelper.FindAncestorData<PlanSubComponent3DData>(hitResult.ActualHitModelData);
                    hitResult.NearestFixture = ModelConstruct3DHelper.FindAncestorData<PlanFixture3DData>(hitResult.ActualHitModelData);

                    //correct the hit face if possible
                    ModelConstruct3DHelper.HitFace? hitFace = ModelConstruct3DHelper.GetFace(hitResult.ActualHitModelData, hitResult.ModelLocalHitPoint);
                    if (hitFace.HasValue)
                    {
                        hitResult.HitFace = hitFace.Value;
                    }

                    break;
                }
            }

            //if no fixture found, then try to get one directly.
            if (hitResult.NearestFixture == null)
            {
                hitResult.NearestFixture = GetNearestFixtureData(vp, hitPoint, planModelData);
            }


            return hitResult;
        }

        /// <summary>
        /// Snaps the given preview component based on the hit test result.
        /// </summary>
        /// <param name="component"></param>
        /// <param name="componentType"></param>
        /// <param name="hitTestResult"></param>
        /// <param name="proposedWorldPos"></param>
        /// <param name="proposedWorldRotation"></param>
        /// <param name="canChangeSize"></param>
        /// <param name="snapToNotch"></param>
        private static void Snap(PreviewComponent component, PlanogramComponentType componentType,
            PlanHitTestResult hitTestResult,
            PointValue proposedWorldPos, RotationValue proposedWorldRotation,
            Boolean canChangeSize, Boolean snapToNotch)
        {
            IModelConstruct3DData nearestSubData = hitTestResult.NearestSubComponent;

            PlanogramSubComponentView nearestSubComponent = hitTestResult.NearestSubComponent.SubComponent as PlanogramSubComponentView;
            if (nearestSubComponent != null)
            {
                PointValue subHitLocalPos = hitTestResult.GetSubComponentLocalHitPoint();

                RotationValue subWorldRotation = ModelConstruct3DHelper.GetWorldRotation(nearestSubData);

                PointValue newLocalPosition = new PointValue();
                RotationValue newWorldRotation = new RotationValue(
                    subWorldRotation.Angle, subWorldRotation.Slope, subWorldRotation.Roll);

                switch (componentType)
                {
                    #region Default
                    default:
                        {

                            newLocalPosition.X = nearestSubData.Position.X + subHitLocalPos.X;
                            newLocalPosition.Y = nearestSubData.Position.Y + subHitLocalPos.Y - (component.Height / 2F);
                            newLocalPosition.Z = nearestSubData.Position.Z + subHitLocalPos.Z;

                            switch (hitTestResult.HitFace)
                            {
                                case ModelConstruct3DHelper.HitFace.Front:
                                    if (canChangeSize) component.Width = Convert.ToSingle(nearestSubData.Size.Width);
                                    newLocalPosition.X = nearestSubData.Position.X;
                                    break;

                                case ModelConstruct3DHelper.HitFace.Back:
                                    if (canChangeSize) component.Width = Convert.ToSingle(nearestSubData.Size.Width);
                                    newLocalPosition.X = nearestSubData.Position.X + nearestSubData.Size.Width;
                                    newWorldRotation.Angle += Convert.ToSingle(CommonHelper.ToRadians(-180));
                                    break;

                                case ModelConstruct3DHelper.HitFace.Top:
                                    if (canChangeSize)
                                    {
                                        component.Width = Convert.ToSingle(nearestSubData.Size.Width);
                                        component.Depth = Convert.ToSingle(nearestSubData.Size.Depth);
                                    }
                                    newLocalPosition.X = nearestSubData.Position.X;
                                    newLocalPosition.Z = nearestSubData.Position.Z;
                                    break;

                                case ModelConstruct3DHelper.HitFace.Bottom:
                                    if (canChangeSize)
                                    {
                                        component.Width = Convert.ToSingle(nearestSubData.Size.Width);
                                        component.Depth = Convert.ToSingle(nearestSubData.Size.Depth);
                                    }
                                    newLocalPosition.X = nearestSubData.Position.X;
                                    newLocalPosition.Y = nearestSubData.Position.Y - component.Height;
                                    newLocalPosition.Z = nearestSubData.Position.Z;
                                    break;

                                case ModelConstruct3DHelper.HitFace.Left:
                                    if (canChangeSize) component.Width = Convert.ToSingle(nearestSubData.Size.Depth);
                                    newLocalPosition.Z = nearestSubData.Position.Z + component.Width;
                                    newWorldRotation.Angle += Convert.ToSingle(CommonHelper.ToRadians(-90));
                                    break;

                                case ModelConstruct3DHelper.HitFace.Right:
                                    if (canChangeSize) component.Width = Convert.ToSingle(nearestSubData.Size.Depth);
                                    newLocalPosition.Z = nearestSubData.Position.Z;
                                    newWorldRotation.Angle += Convert.ToSingle(CommonHelper.ToRadians(90));
                                    break;
                            }


                            if (snapToNotch)
                            {
                                Single? notchSnap =
                                        PlanogramComponentView.GetNearestNotchY(nearestSubComponent, component.Height,
                                        hitTestResult.HitFace.ToPlanogramSubComponentFaceType(), newLocalPosition.Y);

                                if (notchSnap.HasValue)
                                {
                                    newLocalPosition.Y = notchSnap.Value;
                                }
                            }
                        }
                        break;
                    #endregion

                    #region Rod/Clipstrip
                    case PlanogramComponentType.Rod:
                    case PlanogramComponentType.ClipStrip:
                        {
                            newLocalPosition.X = nearestSubData.Position.X + subHitLocalPos.X - component.Width / 2F;
                            newLocalPosition.Y = nearestSubData.Position.Y + subHitLocalPos.Y - component.Height / 2F;
                            newLocalPosition.Z = nearestSubData.Position.Z + subHitLocalPos.Z;

                            switch (hitTestResult.HitFace)
                            {
                                case ModelConstruct3DHelper.HitFace.Front:
                                    //no change to size or position.
                                    break;

                                case ModelConstruct3DHelper.HitFace.Back:
                                    //no change to size or position.
                                    newWorldRotation.Angle += Convert.ToSingle(CommonHelper.ToRadians(-180));
                                    break;

                                case ModelConstruct3DHelper.HitFace.Top:
                                    //no change to size or position
                                    newWorldRotation.Slope += Convert.ToSingle(CommonHelper.ToRadians(-180));
                                    break;

                                case ModelConstruct3DHelper.HitFace.Bottom:
                                    //no change to size or position
                                    newWorldRotation.Slope += Convert.ToSingle(CommonHelper.ToRadians(180));
                                    break;

                                case ModelConstruct3DHelper.HitFace.Left:
                                    //no change to size or position
                                    newWorldRotation.Angle += Convert.ToSingle(CommonHelper.ToRadians(-90));
                                    break;

                                case ModelConstruct3DHelper.HitFace.Right:
                                    //no change to size or position
                                    newWorldRotation.Angle += Convert.ToSingle(CommonHelper.ToRadians(90));
                                    break;
                            }


                            if (snapToNotch)
                            {
                                Single? notchSnap =
                                        PlanogramComponentView.GetNearestNotchY(nearestSubComponent, component.Height,
                                        hitTestResult.HitFace.ToPlanogramSubComponentFaceType(), newLocalPosition.Y);

                                if (notchSnap.HasValue)
                                {
                                    newLocalPosition.Y = notchSnap.Value;
                                }
                            }

                        }
                        break;
                    #endregion

                }

                //convert the position back to world space.
                PointValue wp = ModelConstruct3DHelper.ToWorld(newLocalPosition, nearestSubData);
                component.X = Convert.ToSingle(wp.X);
                component.Y = Convert.ToSingle(wp.Y);
                component.Z = Convert.ToSingle(wp.Z);

                component.Angle = Convert.ToSingle(newWorldRotation.Angle);
                component.Slope = Convert.ToSingle(newWorldRotation.Slope);
                component.Roll = Convert.ToSingle(newWorldRotation.Roll);
            }
            else
            {
                component.SetPosition(proposedWorldPos);
                component.SetRotation(proposedWorldRotation);
            }

            if (component.Y < 0) component.Y = 0;//force y above floor.

        }

        /// <summary>
        /// Returns the model data for the nearest fixture to the given hit point
        /// </summary>
        /// <param name="vp"></param>
        /// <param name="hitPoint"></param>
        /// <param name="planModelData"></param>
        /// <returns></returns>
        private static PlanFixture3DData GetNearestFixtureData(Viewport3D vp, Point hitPoint, Plan3DData planModelData)
        {
            ProjectionCamera camera = vp.Camera as ProjectionCamera;
            if (camera == null) { return null; }


            PlanFixture3DData nearestFixture = null;

            Vector3D normal = new Vector3D(camera.LookDirection.X, camera.LookDirection.Y, camera.LookDirection.Z);
            normal.Negate();
            normal.Normalize();

            //get fixture bounds
            Rect3D overallBounds = Rect3D.Empty;
            Dictionary<PlanFixture3DData, Rect3D> fixtureBounds = new Dictionary<PlanFixture3DData, Rect3D>();
            foreach (var fixture in planModelData.EnumerateFixtureModels())
            {
                Rect3D fixtureRect = new Rect3D();
                fixtureRect.SizeX = fixture.Size.Width >= 0 ? fixture.Size.Width : 0;
                fixtureRect.SizeY = fixture.Size.Height >= 0 ? fixture.Size.Height : 0;
                fixtureRect.SizeZ = fixture.Size.Depth >= 0 ? fixture.Size.Depth : 0;

                MatrixTransform3D transform = ModelConstruct3DHelper.CreateTransform(fixture.Rotation, fixture.Position);
                fixtureRect = transform.TransformBounds(fixtureRect);

                fixtureBounds.Add(fixture, fixtureRect);

                overallBounds = Rect3D.Union(overallBounds, fixtureRect);
            }


            //determine possible intersection points
            List<Point3D> posIntersections = new List<Point3D>();
            posIntersections = ModelConstruct3DHelper.GetBoxPlaneIntersections(overallBounds, Viewport3DHelper.Point2DtoRay3D(vp, hitPoint));


            //cycle through the fixtures determining which one is intersected first.
            foreach (var fx in fixtureBounds.OrderBy(v => ModelConstruct3DHelper.GetCameraDistance(v.Value, camera.Position)))
            {
                Boolean found = false;

                foreach (Point3D point in posIntersections)
                {
                    if (fx.Value.Contains(point))
                    {
                        nearestFixture = fx.Key;
                        found = true;
                        break;
                    }
                }

                if (found) break;
            }


            return nearestFixture;
        }


        /// <summary>
        /// Returns the nearest merchandisiable subcomponent based on the given hit point.
        /// </summary>
        /// <param name="vp"></param>
        /// <param name="hitPoint"></param>
        /// <param name="planModelData"></param>
        /// <returns></returns>
        private static PlanSubComponent3DData GetNearestMerchandisableSubComponent(Viewport3D vp, Point hitPoint, Plan3DData planModelData)
        {
            //first check if we hit a merchandisable subcomponent directly.
            PlanHitTestResult subHitResult = GetNearestHit(vp, hitPoint, planModelData);
            PlanSubComponent3DData nearestSubData = subHitResult.NearestSubComponent;

            if (nearestSubData == null || !nearestSubData.SubComponent.IsMerchandisable)
            {
                //no merch sub was hit directly so now lets see if we hit any when merch bounds are included.


                //first check against the hit fixture
                PlanogramFixtureView hitFixture = subHitResult.GetNearestFixtureView();
                PlanFixture3DData fixtureModelData =
                    (hitFixture != null) ?
                        RenderControlsHelper.FindItemModelData(planModelData, hitFixture) as PlanFixture3DData
                        : null;


                if (fixtureModelData != null)
                {
                    nearestSubData =
                        FindNearestHitMerchandisableSubComponent(
                            fixtureModelData.GetAllChildModels().OfType<PlanSubComponent3DData>().ToList(),
                            vp, vp.Camera as ProjectionCamera, hitPoint);
                }


                //if nothing was found then widen the search to the entire plan
                if (nearestSubData == null)
                {
                    List<PlanSubComponent3DData> otherSubData =
                        planModelData.EnumerateFixtureModels()
                                     .Where(f => f != fixtureModelData)
                                     .SelectMany(f => f.EnumerateComponentModels()
                                                       .SelectMany(c => c.EnumerateSubComponentModels()))
                                     .ToList();


                    nearestSubData =
                        FindNearestHitMerchandisableSubComponent(
                            otherSubData, vp, vp.Camera as ProjectionCamera, hitPoint);
                }

            }

            if (nearestSubData != null && !nearestSubData.SubComponent.IsMerchandisable)
            {
                //What happened??
                nearestSubData = null;
            }

            return nearestSubData;
        }

        /// <summary>
        /// Checks through the list of subcomponent model data and returns the nearest hit merchandisable item.
        /// </summary>
        private static PlanSubComponent3DData FindNearestHitMerchandisableSubComponent(List<PlanSubComponent3DData> subsToCheck,
            Viewport3D vp, ProjectionCamera camera, Point hitPoint)
        {
            Dictionary<PlanSubComponent3DData, Rect3D> subToBounds = new Dictionary<PlanSubComponent3DData, Rect3D>();
            foreach (var sub in subsToCheck)
            {
                if (sub.SubComponent.IsMerchandisable)
                {
                    subToBounds.Add(sub, GetUnhinderedMerchandisingSpaceBounds(sub, /*toWorld*/true));
                }
            }
            if (!subToBounds.Any()) return null;


            PlanSubComponent3DData nearestSubData = null;
            Ray3D hitRay = Viewport3DHelper.Point2DtoRay3D(vp, hitPoint);

            Double lastDist = Double.PositiveInfinity;
            foreach (var entry in subToBounds.OrderBy(v => ModelConstruct3DHelper.GetCameraDistance(v.Value, camera.Position)))
            {
                List<Point3D> rayPlaneIntersections = ModelConstruct3DHelper.GetBoxPlaneIntersections(entry.Value, hitRay);
                foreach (Point3D ri in rayPlaneIntersections.OrderBy(ri => camera.Position.DistanceTo(ri)))
                {
                    if (entry.Value.Contains(ri))
                    {
                        Double distance = ri.DistanceTo(entry.Value.Location);

                        //if the subcomponent is a hanging type then measure to its top left.
                        PlanogramSubComponentView subView = entry.Key.SubComponent as PlanogramSubComponentView;
                        if (subView != null)
                        {
                            if (subView.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang)
                            {
                                //Measure to top left.
                                distance = ri.DistanceTo(
                                    new Point3D(entry.Value.X, entry.Value.Y + entry.Value.SizeY, entry.Value.SizeZ));
                            }
                        }


                        Boolean found = (nearestSubData == null)
                            || (distance < lastDist);


                        if (found)
                        {
                            nearestSubData = entry.Key;
                            lastDist = distance;
                        }
                        break;
                    }
                }

            }
            return nearestSubData;
        }



        /// <summary>
        /// Returns the merchandising bounds of the subcomponent unhindered by any collisions.
        /// </summary>
        /// <param name="subComponent"></param>
        /// <returns></returns>
        private static Rect3D GetUnhinderedMerchandisingSpaceBounds(PlanSubComponent3DData subModelData, Boolean toWorld)
        {
            Rect3D merchBounds = Rect3D.Empty;

            PlanogramSubComponentView subComponent = (PlanogramSubComponentView)subModelData.SubComponent;
            PlanFixture3DData parentFixtureData = ModelConstruct3DHelper.FindAncestorData<PlanFixture3DData>(subModelData) as PlanFixture3DData;
            if (parentFixtureData != null)
            {
                Rect3D fixtureWorldBounds = ModelConstruct3DHelper.GetWorldSpaceBounds(parentFixtureData);
                Rect3D subWorldBounds = ModelConstruct3DHelper.GetWorldSpaceBounds(subModelData);

                merchBounds = GetLocalUnhinderedMerchandisingSpaceBounds(subComponent.Model, fixtureWorldBounds, subWorldBounds);


                //convert to worldspace if required.
                if (!merchBounds.IsEmpty && !toWorld)
                {
                    Point3D localPos = ModelConstruct3DHelper.ToLocal(merchBounds.Location, subModelData);

                    merchBounds.X = localPos.X;
                    merchBounds.Y = localPos.Y;
                    merchBounds.Z = localPos.Z;
                }
            }

            return merchBounds;
        }

        /// <summary>
        /// Returns the merchandising bounds of the subcomponent unhindered by any collisions.
        /// </summary>
        /// <param name="subComponent"></param>
        /// <returns></returns>
        private static Rect3D GetLocalUnhinderedMerchandisingSpaceBounds(PlanogramSubComponent sub, Rect3D fixtureWorldBounds, Rect3D subWorldBounds)
        {
            //TODO: Refactor this to use merch space. 
            // Need to be aware however that if the camera type is design we need a design merch space.

            if (sub.MerchandisingType == PlanogramSubComponentMerchandisingType.None) return Rect3D.Empty;

            Double merchX = subWorldBounds.X;
            Double merchY = subWorldBounds.Y;
            Double merchZ = subWorldBounds.Z;
            Double merchSizeX = subWorldBounds.SizeX;
            Double merchSizeY = Math.Max(sub.MerchandisableHeight, 1);
            Double merchSizeZ = subWorldBounds.SizeZ;

            switch (sub.MerchandisingType)
            {
                #region Stack
                case PlanogramSubComponentMerchandisingType.Stack:
                    {
                        if (!sub.HasFaceThickness())
                        {
                            //products on top
                            merchX = subWorldBounds.X;
                            merchY = subWorldBounds.Y + subWorldBounds.SizeY;
                            merchZ = subWorldBounds.Z;

                            merchSizeX = subWorldBounds.SizeX;
                            merchSizeY = sub.MerchandisableHeight;
                            merchSizeZ = subWorldBounds.SizeZ;


                            //Determine the available height
                            if (merchSizeY == 0)
                            {
                                Double toFixtureTop =
                                (fixtureWorldBounds.Y + fixtureWorldBounds.SizeY) - merchY;
                                merchSizeY = Math.Max(0, toFixtureTop);
                            }

                        }
                        else
                        {
                            //products inside
                            merchX = subWorldBounds.X + sub.FaceThicknessLeft;
                            merchY = subWorldBounds.Y + sub.FaceThicknessBottom;
                            merchZ = subWorldBounds.Z + sub.FaceThicknessBack;

                            merchSizeX = subWorldBounds.SizeX - (sub.FaceThicknessLeft + sub.FaceThicknessRight);
                            merchSizeY = subWorldBounds.SizeY - sub.FaceThicknessBottom + sub.FaceThicknessTop;
                            merchSizeZ = subWorldBounds.SizeZ - (sub.FaceThicknessBack + sub.FaceThicknessFront);


                            //Determine the available height
                            if (sub.MerchandisableHeight > 0)
                            {
                                merchSizeY = sub.MerchandisableHeight;
                            }
                            else
                            {
                                Double toChestTop = (subWorldBounds.Y + subWorldBounds.SizeY) - merchY;
                                merchSizeY = Math.Max(0, toChestTop);
                            }
                        }
                    }
                    break;
                #endregion

                #region Hang
                case PlanogramSubComponentMerchandisingType.Hang:
                    {
                        merchX = subWorldBounds.X;
                        //merchY = 0;
                        merchZ = subWorldBounds.Z + subWorldBounds.SizeZ;

                        merchSizeX = subWorldBounds.SizeX;
                        //merchSizeY = 0;
                        merchSizeZ = sub.MerchandisableDepth;


                        if (sub.MerchandisableHeight > 0)
                        {
                            merchSizeY = sub.MerchandisableHeight;
                            merchY = sub.Y - sub.MerchandisableHeight + subWorldBounds.SizeY;
                        }
                        else
                        {
                            //to the bottom of the fixture..
                            merchSizeY = (subWorldBounds.Y + subWorldBounds.SizeY) - fixtureWorldBounds.Y;
                            merchY = fixtureWorldBounds.Y;
                        }


                        if (sub.MerchandisableDepth == 0)
                        {
                            //z reaches to fixture front
                            merchSizeZ = fixtureWorldBounds.Z + fixtureWorldBounds.SizeZ - merchZ;
                        }
                    }
                    break;
                #endregion

                #region HangFromBottom
                case PlanogramSubComponentMerchandisingType.HangFromBottom:
                    {
                        merchX = subWorldBounds.X;
                        //merchY = 0;
                        merchZ = subWorldBounds.Z + subWorldBounds.SizeZ;

                        merchSizeX = subWorldBounds.SizeX;
                        //merchSizeY = 0;
                        merchSizeZ = sub.MerchandisableDepth;


                        if (sub.MerchandisableHeight > 0)
                        {
                            merchSizeY = sub.MerchandisableHeight;
                            merchY = sub.Y - sub.MerchandisableHeight + subWorldBounds.SizeY;
                        }
                        else
                        {
                            //to the bottom of the fixture..
                            merchSizeY = Math.Max((subWorldBounds.Y + subWorldBounds.SizeY) - fixtureWorldBounds.Y, 0);
                            merchY = fixtureWorldBounds.Y;
                        }

                        if (sub.MerchandisableDepth == 0)
                        {
                            //z reaches to fixture front
                            // V8-28904 : prevent SizeZ being -ve
                            merchSizeZ = Math.Max(fixtureWorldBounds.Z + fixtureWorldBounds.SizeZ - merchZ, 0);
                        }
                    }
                    break;
                #endregion

                case PlanogramSubComponentMerchandisingType.None:
                    break;

                default:
                    Debug.Fail("Not handled");
                    break;
            }


            // return the merch bounds,making sure that size values can never be
            // negative. (V8-30488)
            Rect3D merchBounds = new Rect3D()
                {
                    X = merchX,
                    Y = merchY,
                    Z = merchZ,
                    SizeX = (merchSizeX >= 0) ? merchSizeX : 1,
                    SizeY = (merchSizeY >= 0) ? merchSizeY : 1,
                    SizeZ = (merchSizeZ >= 0) ? merchSizeZ : 1,
                };


            return merchBounds;
        }

        #endregion

        /// <summary>
        ///     Orders those <see cref="IPlanItem"/> items that are of type <see cref="PlanItemType.Position"/> 
        ///     at the end of the collection.
        /// </summary>
        /// <param name="items">The collection of <see cref="IPlanItem"/> to order.</param>
        /// <returns>
        ///     An enumeration with the same items as the source but with the position items ordered and at the end.
        /// </returns>
        private static IEnumerable<IPlanItem> OrderPositionsAtTheEnd(IEnumerable<IPlanItem> items)
        {
            List<IPlanItem> itemsWithChildrenStripped = PlanItemHelper.StripOutCommonChildren(items);
            IEnumerable<IPlanItem> nonPositionItems = itemsWithChildrenStripped.Where(item => item.PlanItemType != PlanItemType.Position);
            IOrderedEnumerable<IPlanItem> orderedPositions =
                itemsWithChildrenStripped.Where(item => item.PlanItemType == PlanItemType.Position)
                                         .OrderBy(item => item.Fixture.BaySequenceNumber)
                                         .ThenBy(item => item.Component.ComponentSequenceNumber)
                                         .ThenBy(item => item.Position.X)
                                         .ThenBy(item => item.Position.Y)
                                         .ThenBy(item => item.Position.Z);
            return nonPositionItems.Union(orderedPositions);
        }

        #endregion
    }

    #region PlanHitTestResult
    public sealed class PlanHitTestResult
    {
        public Point ViewerHitPoint { get; set; }
        public PointValue ViewerHitPoint3D { get; set; }

        public IModelConstruct3DData ActualHitModelData { get; set; }
        public PointValue ModelLocalHitPoint { get; set; }
        public ModelConstruct3DHelper.HitFace HitFace { get; set; }

        public PlanSubComponent3DData NearestSubComponent { get; set; }
        public PlanFixture3DData NearestFixture { get; set; }


        public PointValue GetSubComponentLocalHitPoint()
        {
            if (this.NearestSubComponent != null)
            {
                PlanSubComponent3DData subData = this.NearestSubComponent;

                PointValue localHitPoint = new PointValue(ModelLocalHitPoint.X, ModelLocalHitPoint.Y, ModelLocalHitPoint.Z);

                //corect the local hit point according to the hit face.
                switch (this.HitFace)
                {
                    case ModelConstruct3DHelper.HitFace.Front:
                        localHitPoint.Z = subData.Position.Z + subData.Size.Depth;
                        break;

                    case ModelConstruct3DHelper.HitFace.Back:
                        localHitPoint.Z = subData.Position.Z;
                        break;

                    case ModelConstruct3DHelper.HitFace.Left:
                        localHitPoint.X = subData.Position.X;
                        break;

                    case ModelConstruct3DHelper.HitFace.Right:
                        localHitPoint.X = subData.Position.X + subData.Size.Width;
                        break;

                    case ModelConstruct3DHelper.HitFace.Top:
                        localHitPoint.Y = subData.Position.Y + subData.Size.Height;
                        break;

                    case ModelConstruct3DHelper.HitFace.Bottom:
                        localHitPoint.Y = subData.Position.Y;
                        break;
                }

                return localHitPoint;
            }
            else
            {
                return new PointValue();
            }
        }

        public PointValue GetSubComponentWorldHitPoint()
        {
            if (this.NearestSubComponent != null)
            {
                return ModelConstruct3DHelper.ToWorld(GetSubComponentLocalHitPoint(), this.NearestSubComponent);
            }
            else
            {
                return new PointValue();
            }

        }

        public PlanogramFixtureView GetNearestFixtureView()
        {
            if (this.NearestFixture != null)
            {
                IPlanItem planPart = this.NearestFixture.Fixture as IPlanItem;
                if (planPart != null)
                {
                    return planPart.Fixture;
                }
            }
            return null;
        }
    }
    #endregion

}