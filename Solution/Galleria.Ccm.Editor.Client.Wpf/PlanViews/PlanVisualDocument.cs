﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0)

// L.Hodson ~ Created.
// V8-24265 : J.Pickup
//  LabelSettingsView is now LabelItem
// V8-25395  : A.Probyn
//      Changed over from ProductInfo to Product
// V8-26702 : A.Kuszyk
//  Overloaded AddNewPositions to accept PlanogramAssortmentProducts.
// V8-27122 : A.Kuszyk
//  Ensured that products added from the Assortment document are added with the correct number of facings.
// V8-27245 : A.Kuszyk
//  Amended AddNewPositions for assortment products to deal with a library loaded from file when connected to the repo.
// V8-27257 : A.Kuszyk
//  Amended AddNewPositions to check if adding a product from a library added from the repo, when the connection  is no
//  longer open.
// V8-27477 : L.Ineson
//  Added GetPositionAnchor.
// V8-25278 : L.Ineson
//  Amended ProductHighlights so that they get applied for new products
//  or when moving products
// V8-25209 : L.Ineson
//  Highlights are no longer lost when saving.
// V8-27825 : L.Ineson
//  Added document freezing
// V8-27648 : A.Probyn 
//      ~ Updated to support ProductLibraryProduct 
//      ~ Added AddNewPositionsPerformance method which getting s triggered when ProductLibraryProducts are 
//      added to a planogram.
// v8-28057 : L.Ineson
//  Made sure that highlights always get updated on a planogram change.
// V8-25968 : L.Ineson
//  Keyboard shortcuts for rotation, resize and move are no longer applied for perspective view type.
// V8-28345 : L.Ineson
//  Reworked how highlights are processed and applied to improve performance.
// V8-27828 : L.Ineson
//  Added selection blink
// V8-28510 : L.Ineson
//  Removed the selection blink horror.
// V8-27622 : L.Luong
//  Positions should now be unselected if ShowPositions is false

#endregion

#region Version History : (CCM 801)

// V8-28765 : A.Kuszyk
//  Amended ProcessUserClickSelection to not deselect multi-selected positions on mouse up.

#endregion

#region Version History : (CCM 802)

// V8-27828 : J.Pickup
//  Added a thread to handle "Blink Horror" with a .6s sleep time.
// V8-29083 : D.Pleasance
//  Amended ProcessUserClickSelection and how products are selected via shift action, now selects any product that colides with the area of the items selected. 

#endregion

#region Version History (CCM 8.03)

// V8-29334 : L.Ineson
//  Added ShowTopDownProductLabelsAsFront
// V8-29217 : L.Ineson
//  Top down view now makes it easier to place products behind.

#endregion

#region Version History (CCM 8.10)

// V8-29052 : L.ineson
//  Assortment products not longer get set to ranged when a position is added.

#endregion

#region Version History (CCM 8.11)

// V8-30256 : A.Probyn 
//  ~ Added new IsPlanogramEmpty
// V8-30368 : N.Haywood
//  Fixed issue where having a product list open, selecting a product and then right clicking on a position keeps the product from the product list selected
// V8-30468 : A.Silva
//  Amended ProcessUserClickSelection to keep it from clearing all selected items to remove product items.. now it just removes the product items directly.
// V8-30514 : J.Pickup
//  When an assortment recomends 0 facings it no longer places the position with 0 facings as this is an impossible quantity. It sets at one instead.
// V8-30546 : L.Ineson
//  When determining the position anchor arrow placement, Y axis is now prioritized last for stacked type subcomponents.
#endregion

#region Version History : CCM 820
// V8-30795 : A.Kuszyk
//  Updated ProcessUserClickSelection to select using sequence snake when shift selecting.
// V8-30932 : L.Ineson
//  Updated ProcessUserClickSelection to consider new selection mode.
// V8-31193 : J.Pickup
//  Cntrl + a now selects based on item selection type.
// V8-31193 : J.Pickup
//  Cntrl + a now selects based on item selection type. Process user click selection is not to remove all selected products as this broke the sync between the two docs.
// V8-31448 : L.Ineson
//  Arrow keys now work in perspective view again.
#endregion

#region Version History : CCM 830
// V8-32159 : L.Ineson
//  Added SelectHighlightGroupPositions
// V8-32080 : A.Silva
//  Removed obsolete AddNewPosition methods for Plan Assortment Products, those are added as Planogram Product Views.
//V8-32212 : L.Ineson
//  Made changes to stop updates when this document is not actually in view.
// V8-32347 : L.ineson 
// Changes to selection blinking to improve performance.
// V8-32661 : A.Kuszyk
//  Added commands for sequence template context tab and panel.
// V8-32816 : A.Kuszyk
//  Added ReverseSequenceOrderCommand.
// V8-32891 : D.Pleasance
//  Amended RefreshSequenceGroups(), Added defensive code to validate components that dont have a sequence colour value
// CCM-14034 : L.Ineson
//  Made ShowSequenceGroupsCommand toggle the panel
// CCM-13958 : D.Pleasance
//  Change applied to reset SelectedSequenceSubGroup when SelectedSequenceGroup changes.
#endregion

#endregion

using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Common.Rendering;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf.Rendering;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Controls.Wpf.Helpers;
using Galleria.Framework.Planograms.Controls.Wpf.ViewModel;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Rendering;
using Galleria.Framework.Rendering.Interfaces;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Media3D;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// Plan document controller for providing a visual view of the planogram
    /// </summary>
    public sealed class PlanVisualDocument : PlanDocument<PlanVisualDocumentView>, IPlanRenderSettings
    {
        #region Fields

        private Plan3DData _planogramModelData;
        private readonly CameraViewType _viewType;

        private readonly BulkObservableCollection<PlanogramHighlightResultGroup> _highlightLegendItems = new BulkObservableCollection<PlanogramHighlightResultGroup>();
        private ReadOnlyBulkObservableCollection<PlanogramHighlightResultGroup> _highlightLegendItemsRO;

        private Boolean _ignoreNextMouseUpSelection;

        private PlanogramPositionHighlightColours _positionHighlights;
        private PlanogramPositionLabelTexts _positionLabelText;
        private PlanogramFixtureLabelTexts _fixtureLabelText;
        private Boolean _isPlanogramEmpty = false;

        private Boolean _isSelectionBlinking;
        private Boolean _isSelectionAlternateOn = false;

        private Boolean _isRefreshRequired;
        private Boolean _isHighlightUpdateRequired;

        private BulkObservableCollection<PlanogramSequenceGroupViewModel> _sequenceGroups =
            new BulkObservableCollection<PlanogramSequenceGroupViewModel>();
        private PlanogramSequenceGroupSubGroup _selectedSequenceSubGroup;
        private PlanogramSequenceGroupViewModel _selectedSequenceGroup;
        private Boolean _suppressSequenceGroupComponentSelection = false;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath PlanogramModelDataProperty = WpfHelper.GetPropertyPath<PlanVisualDocument>(p => p.PlanogramModelData);
        public static readonly PropertyPath ViewTypeProperty = WpfHelper.GetPropertyPath<PlanVisualDocument>(p => p.ViewType);

        public static readonly PropertyPath MouseToolTypeProperty = WpfHelper.GetPropertyPath<PlanVisualDocument>(p => p.MouseToolType);
        public static readonly PropertyPath AutosizeGridlinesProperty = WpfHelper.GetPropertyPath<PlanVisualDocument>(p => p.AutosizeGridlines);
        public static readonly PropertyPath GridlineFixedHeightProperty = WpfHelper.GetPropertyPath<PlanVisualDocument>(p => p.GridlineFixedHeight);
        public static readonly PropertyPath GridlineFixedWidthProperty = WpfHelper.GetPropertyPath<PlanVisualDocument>(p => p.GridlineFixedWidth);
        public static readonly PropertyPath GridlineFixedDepthProperty = WpfHelper.GetPropertyPath<PlanVisualDocument>(p => p.GridlineFixedDepth);
        public static readonly PropertyPath GridlineMajorDistProperty = WpfHelper.GetPropertyPath<PlanVisualDocument>(p => p.GridlineMajorDist);
        public static readonly PropertyPath GridlineMinorDistProperty = WpfHelper.GetPropertyPath<PlanVisualDocument>(p => p.GridlineMinorDist);
        public static readonly PropertyPath IsDynamicTextScalingEnabledProperty = WpfHelper.GetPropertyPath<PlanVisualDocument>(p => p.IsDynamicTextScalingEnabled);
        public static readonly PropertyPath IsPlanogramEmptyProperty = WpfHelper.GetPropertyPath<PlanVisualDocument>(p => p.IsPlanogramEmpty);
        public static readonly PropertyPath SelectedSequenceGroupProperty = WpfHelper.GetPropertyPath<PlanVisualDocument>(p => p.SelectedSequenceGroup);
        public static readonly PropertyPath SequenceGroupsProperty = WpfHelper.GetPropertyPath<PlanVisualDocument>(p => p.SequenceGroups);
        public static readonly PropertyPath ShowSequenceGroupsCommandProperty = WpfHelper.GetPropertyPath<PlanVisualDocument>(p => p.ShowSequenceGroupsCommand);
        public static readonly PropertyPath AddSequenceSubGroupCommandProperty = WpfHelper.GetPropertyPath<PlanVisualDocument>(p => p.AddSequenceSubGroupCommand);
        public static readonly PropertyPath RemoveSequenceSubGroupCommandProperty = WpfHelper.GetPropertyPath<PlanVisualDocument>(p => p.RemoveSequenceSubGroupCommand);
        public static readonly PropertyPath AddSequenceSubGroupProductCommandProperty = WpfHelper.GetPropertyPath<PlanVisualDocument>(p => p.AddSequenceSubGroupProductCommand);
        public static readonly PropertyPath RemoveSequenceSubGroupProductCommandProperty = WpfHelper.GetPropertyPath<PlanVisualDocument>(p => p.RemoveSequenceSubGroupProductCommand);
        public static readonly PropertyPath SelectedSequenceSubGroupProperty = WpfHelper.GetPropertyPath<PlanVisualDocument>(p => p.SelectedSequenceSubGroup);
        public static readonly PropertyPath ReverseSequenceOrderCommandProperty = WpfHelper.GetPropertyPath<PlanVisualDocument>(p => p.ReverseSequenceOrderCommand);

        #endregion

        #region Properties

        public override DocumentType DocumentType
        {
            get
            {
                switch (_viewType)
                {
                    default:
                    case CameraViewType.Design: return DocumentType.Design;
                    case CameraViewType.Perspective: return DocumentType.Perspective;
                    case CameraViewType.Front: return DocumentType.Front;
                    case CameraViewType.Back: return DocumentType.Back;
                    case CameraViewType.Top: return Model.DocumentType.Top;
                    case CameraViewType.Bottom: return DocumentType.Bottom;
                    case CameraViewType.Left: return DocumentType.Left;
                    case CameraViewType.Right: return DocumentType.Right;
                }
            }
        }

        /// <summary>
        /// Returns the document title
        /// </summary>
        public override String Title
        {
            get { return CameraViewTypeHelper.FriendlyNames[this.ViewType]; }
        }

        /// <summary>
        /// Gets the model data for this document
        /// </summary>
        public Plan3DData PlanogramModelData
        {
            get
            {
                if (_planogramModelData == null)
                {
                    //create the _planogram model data
                    if (this.ViewType == CameraViewType.Design)
                    {
                        _planogramModelData = new Plan3DData(this.Planogram.FlattenedView, this);
                    }
                    else
                    {
                        _planogramModelData = new Plan3DData(this.Planogram, this);
                    }

                    BeginSelectionBlinking();
                }

                return _planogramModelData;
            }
        }

        /// <summary>
        /// Returns the camera type shown by this view.
        /// </summary>
        public CameraViewType ViewType
        {
            get { return _viewType; }
        }

        /// <summary>
        /// Returns the collection of legend items processed for the current highlight.
        /// </summary>
        public override ReadOnlyBulkObservableCollection<PlanogramHighlightResultGroup> HighlightLegendItems
        {
            get
            {
                if (_highlightLegendItemsRO == null)
                {
                    _highlightLegendItemsRO = new ReadOnlyBulkObservableCollection<PlanogramHighlightResultGroup>(_highlightLegendItems);
                }
                return _highlightLegendItemsRO;
            }
        }

        #region Controller inherited properties

        /// <summary>
        /// Gets/Sets the current mouse tool type in use.
        /// </summary>
        public MouseToolType MouseToolType
        {
            get { return ParentController.SelectedMouseToolType; }
            set { ParentController.SelectedMouseToolType = value; }
        }

        /// <summary>
        /// Gets/Sets whether gridlines should be autosized.
        /// </summary>
        public Boolean AutosizeGridlines
        {
            get { return ParentController.AutosizeGridlines; }
            set { ParentController.AutosizeGridlines = value; }
        }

        /// <summary>
        /// Gets/Sets the height of fixed gridlines.
        /// </summary>
        public Double GridlineFixedHeight
        {
            get { return ParentController.GridlineFixedHeight; }
            set { ParentController.GridlineFixedHeight = value; }
        }

        /// <summary>
        /// Gets/Sets the width of fixed gridlines.
        /// </summary>
        public Double GridlineFixedWidth
        {
            get { return ParentController.GridlineFixedWidth; }
            set { ParentController.GridlineFixedWidth = value; }
        }

        /// <summary>
        /// Gets/Sets the depth of fixed gridlines
        /// </summary>
        public Double GridlineFixedDepth
        {
            get { return ParentController.GridlineFixedDepth; }
            set { ParentController.GridlineFixedDepth = value; }
        }

        /// <summary>
        /// Gets/Sets the major distance of gridlines
        /// </summary>
        public Double GridlineMajorDist
        {
            get { return ParentController.GridlineMajorDist; }
            set { ParentController.GridlineMajorDist = value; }
        }

        /// <summary>
        /// Gets/Sets the minor distance of gridlines
        /// </summary>
        public Double GridlineMinorDist
        {
            get { return ParentController.GridlineMinorDist; }
            set { ParentController.GridlineMinorDist = value; }
        }

        #endregion

        public Boolean IsDynamicTextScalingEnabled
        {
            get { return App.ViewState.Settings.Model.IsDynamicTextScalingEnabled; }
        }

        /// <summary>
        /// Gets/Sets whether thge planogram is empty or not
        /// </summary>
        /// <remarks>Move to binding..</remarks>
        public Boolean IsPlanogramEmpty
        {
            get { return _isPlanogramEmpty; }
            set
            {
                if (_isPlanogramEmpty != value)
                {
                    _isPlanogramEmpty = value;
                    OnPropertyChanged(IsPlanogramEmptyProperty);
                }
            }
        }

        /// <summary>
        /// The currently selected sequence group.
        /// </summary>
        public PlanogramSequenceGroupViewModel SelectedSequenceGroup
        {
            get { return _selectedSequenceGroup; }
            set
            {
                _selectedSequenceGroup = value;
                OnPropertyChanged(SelectedSequenceGroupProperty);
                SelectedSequenceSubGroup = null;
                if (_suppressSequenceGroupComponentSelection || _selectedSequenceGroup == null) return;
                SelectedPlanItems.Clear();
                SelectedPlanItems.AddRange(Planogram.EnumerateAllComponents().Where(c => c.FillColour.Equals(_selectedSequenceGroup.Colour)));
            }
        }

        /// <summary>
        /// The sequence group view models associated with this <see cref="Planogram"/>, only populated
        /// if this <see cref="Planogram"/> is a Sequence Template.
        /// </summary>
        public BulkObservableCollection<PlanogramSequenceGroupViewModel> SequenceGroups
        {
            get { return _sequenceGroups; }
        }

        /// <summary>
        /// The currently selected sequence group sub group.
        /// </summary>
        public PlanogramSequenceGroupSubGroup SelectedSequenceSubGroup
        {
            get { return _selectedSequenceSubGroup; }
            set
            {
                _selectedSequenceSubGroup = value;
                OnPropertyChanged(SelectedSequenceSubGroupProperty);
                if (_selectedSequenceSubGroup == null) return;
                IEnumerable<String> subGroupGtins = _selectedSequenceSubGroup.EnumerateProducts().Select(p => p.Gtin).ToList();
                SelectedPlanItems.Clear();
                SelectedPlanItems.AddRange(Planogram
                    .EnumerateAllPositions()
                    .Where(p =>
                        p.SequenceColour.HasValue &&
                        p.SequenceColour.Equals(_selectedSequenceSubGroup.Parent.Colour) &&
                        subGroupGtins.Contains(p.Product.Gtin)));
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="parentController"></param>
        /// <param name="viewType"></param>
        public PlanVisualDocument(PlanControllerViewModel parentController, CameraViewType viewType)
            : base(parentController)
        {
            //initialise document settings.
            this.ArePlanogramViewSettingsSupported = true;
            this.IsZoomingSupported = true;
            this.IsHighlightingSupported = true;

            _viewType = viewType;

            //update selected items.
            SelectedPlanItems_BulkCollectionChanged(this.SelectedPlanItems, new
            BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));

            Planogram.Model.Sequence.Groups.CollectionChanged += OnPlanogramSequenceGroupsCollectionChanged;
            if (Planogram.PlanogramType == PlanogramType.SequenceTemplate)
            {
                using (PlanogramMerchandisingGroupList merchandisingGroups = Planogram.Model.GetMerchandisingGroups())
                {
                    Planogram.Model.Sequence.ApplyFromMerchandisingGroups(merchandisingGroups);
                }
                RefreshSequenceGroups();
            }

            //attach to events
            SetPlanEventHandlers(true);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when the planogram's sequence group collection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnPlanogramSequenceGroupsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (Planogram.PlanogramType == PlanogramType.SequenceTemplate)
            {
                RefreshSequenceGroups();
            }
        }

        /// <summary>
        /// Called whenever a property of this viewmodel changes value.
        /// </summary>
        /// <param name="propertyName"></param>
        protected override void OnPropertyChanged(String propertyName)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName == HighlightProperty.Path)
            {
                OnUpdateHighlight();
            }
            else if (propertyName == ShowPositionsProperty.Path)
            {
                ShowPositionsVisibleChanged();
            }
            else if (propertyName == ProductLabelProperty.Path)
            {
                this.PositionLabelText =
                 (this.ProductLabel != null) ?
                 new PlanogramPositionLabelTexts(this.ProductLabel.Text, this.Planogram.Model)
                 : null;
            }
            else if (propertyName == FixtureLabelProperty.Path)
            {
                this.FixtureLabelText =
                 (this.FixtureLabel != null) ?
                 new PlanogramFixtureLabelTexts(this.FixtureLabel.Text, this.Planogram.Model)
                     : null;
            }
        }

        /// <summary>
        /// Called whenever a property changes on the parent controller
        /// </summary>
        /// <param name="propertyName"></param>
        protected override void OnParentControllerPropertyChanged(String propertyName)
        {
            base.OnParentControllerPropertyChanged(propertyName);

            /*if (propertyName == PlanControllerViewModel.CuttingPlaneTypeProperty.Path
                || propertyName == PlanControllerViewModel.CuttingPlanPositionProperty.Path)
            {
                //ApplyCuttingPlane();
            }
            else*/
            if (propertyName == PlanControllerViewModel.AutosizeGridlinesProperty.Path)
            {
                OnPropertyChanged(AutosizeGridlinesProperty);
            }
            else if (propertyName == PlanControllerViewModel.SelectedMouseToolTypeProperty.Path)
            {
                OnPropertyChanged(MouseToolTypeProperty);
            }
            else if (propertyName == PlanControllerViewModel.GridlineFixedHeightProperty.Path)
            {
                OnPropertyChanged(GridlineFixedHeightProperty);
            }
            else if (propertyName == PlanControllerViewModel.GridlineFixedWidthProperty.Path)
            {
                OnPropertyChanged(GridlineFixedWidthProperty);
            }
            else if (propertyName == PlanControllerViewModel.GridlineFixedDepthProperty.Path)
            {
                OnPropertyChanged(GridlineFixedDepthProperty);
            }
            else if (propertyName == PlanControllerViewModel.GridlineMajorDistProperty.Path)
            {
                OnPropertyChanged(GridlineMajorDistProperty);
            }
            else if (propertyName == PlanControllerViewModel.GridlineMinorDistProperty.Path)
            {
                OnPropertyChanged(GridlineMinorDistProperty);
            }
        }

        /// <summary>
        /// Called when the contents of the selected plan items collection chnages
        /// </summary>
        private void SelectedPlanItems_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            //don't bother updating if this is not visible.
            if (!this.IsVisible) return;

            List<IPlanSubComponentRenderable> subs = new List<IPlanSubComponentRenderable>();
            List<IPlanAnnotationRenderable> annos = new List<IPlanAnnotationRenderable>();
            List<IPlanPositionRenderable> positions = new List<IPlanPositionRenderable>();

            foreach (IPlanItem item in this.SelectedPlanItems)
            {
                switch (item.PlanItemType)
                {
                    case PlanItemType.Fixture:
                    case PlanItemType.Assembly:
                    case PlanItemType.Component:
                    case PlanItemType.SubComponent:
                        foreach (IPlanItem subComponent in PlanItemHelper.GetAllChildItemsOfType(item, PlanItemType.SubComponent))
                        {
                            subs.Add((IPlanSubComponentRenderable)subComponent);
                        }
                        break;

                    case PlanItemType.Annotation:
                        annos.Add((IPlanAnnotationRenderable)item);
                        break;

                    case PlanItemType.Position:
                        positions.Add((IPlanPositionRenderable)item);
                        break;

                }
            }

            //cycle through updating the selection
            foreach (IModelConstruct3DData modelData in this.PlanogramModelData.EnumerateChildModels())
            {
                PlanSubComponent3DData subData = modelData as PlanSubComponent3DData;
                if (subData != null)
                {
                    subData.IsSelected = subs.Contains(subData.SubComponent);
                    continue;
                }

                PlanPosition3DData posData = modelData as PlanPosition3DData;
                if (posData != null)
                {
                    posData.IsSelected = positions.Contains(posData.PlanPosition);
                    continue;
                }

                PlanAnnotation3DData annoData = modelData as PlanAnnotation3DData;
                if (annoData != null)
                {
                    annoData.IsSelected = annos.Contains(annoData.Annotation);
                }
            }

            if (Planogram.PlanogramType == PlanogramType.SequenceTemplate)
            {
                // Ensure that the selected sequence group is flagged as changed, because this is linked to 
                // plan item selection.
                IEnumerable<PlanogramComponentView> selectedComponents = SelectedPlanItems
                    .Where(p => p.PlanItemType == PlanItemType.Component && p.Component != null)
                    .Select(p => p.Component)
                    .ToList();
                if (selectedComponents.Count() == 1)
                {
                    PlanogramComponentView selectedComponent = selectedComponents.First();
                    _suppressSequenceGroupComponentSelection = true;
                    SelectedSequenceGroup = SequenceGroups.FirstOrDefault(s => s.Colour.Equals(selectedComponent.FillColour));
                    _suppressSequenceGroupComponentSelection = false;
                }
            }
        }

        /// <summary>
        /// Responds to the settings being changed (options). 
        /// </summary>
        /// <remarks>Used within this class to monitor changes in whether the selection should blink.</remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Settings_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<UserEditorSettings> e)
        {
            //When the Settings is changed we should check to see if the blink on selection is also changed.
            if (e.NewModel.IsShowSelectionAsBlinkingEnabled == true && !_isSelectionBlinking)
            {
                BeginSelectionBlinking();
            }
            else if (e.NewModel.IsShowSelectionAsBlinkingEnabled == false && _isSelectionBlinking)
            {
                StopSelectionBlinking();
            }

            //notify of the property change.
            OnPropertyChanged(IsDynamicTextScalingEnabledProperty);
        }

        /// <summary>
        /// Carries out post process actions.
        /// </summary>
        public override void OnPlanProcessCompleting()
        {
            if (this.IsVisible)
            {
                //highlight
                OnUpdateHighlight();

                //Product Labels
                this.PositionLabelText =
                    (this.ProductLabel != null) ?
                    new PlanogramPositionLabelTexts(this.ProductLabel.Text, this.Planogram.Model)
                    : null;

                //Fixture Labels
                this.FixtureLabelText =
                 (this.FixtureLabel != null) ?
                 new PlanogramFixtureLabelTexts(this.FixtureLabel.Text, this.Planogram.Model)
                     : null;

                //Is Planogram Empty
                this.IsPlanogramEmpty = this.Planogram.Model.FixtureItems.Count <= 0;


                _isRefreshRequired = false;
            }
            else
            {
                _isRefreshRequired = true;
            }
        }

        /// <summary>
        /// Called when the visibility of this document changes.
        /// </summary>
        protected override void OnIsVisibleChanged()
        {
            base.OnIsVisibleChanged();
            OnPropertyChanged("IsPlanogramModelVisible");

            //Stop blinking whilst this document is not visible.
            if (this.IsVisible)
            {
                //if a plan process complete was flagged
                // the refresh the data now.
                if (_isRefreshRequired) OnPlanProcessCompleting();

                if (_isHighlightUpdateRequired) OnUpdateHighlight();

                this.PlanogramModelData.UnpauseUpdates();
                BeginSelectionBlinking();

                //update selected items.
                SelectedPlanItems_BulkCollectionChanged(this.SelectedPlanItems, new
                BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));
            }
            else
            {
                this.PlanogramModelData.PauseUpdates();
                StopSelectionBlinking();
            }
        }

        #endregion

        #region Commands

        #region ReverseSequenceOrderCommand

        private RelayCommand _reverseSequenceOrderCommand = null;

        public RelayCommand ReverseSequenceOrderCommand
        {
            get
            {
                if (_reverseSequenceOrderCommand == null)
                {
                    _reverseSequenceOrderCommand = new RelayCommand(
                        p => ReverseSequenceOrderCommandExecuted(),
                        p => ReverseSequenceOrderCommandCanExecute())
                    {
                        FriendlyName = Message.PlanVisualDocument_ReverseSequenceOrder_Name,
                        FriendlyDescription = Message.PlanVisualDocument_ReverseSequenceOrder_Description,
                        Icon = ImageResources.SequenceTemplateRibbon_ReverseSequenceOrder,
                    };
                }
                return _reverseSequenceOrderCommand;
            }
        }

        private Boolean ReverseSequenceOrderCommandCanExecute()
        {
            if (!SelectedPlanItems.All(i => i.PlanItemType == PlanItemType.Position && i.Position != null))
            {
                ReverseSequenceOrderCommand.DisabledReason = Message.PlanVisualDocument_ReverseSequenceOrder_DisabledReason1;
                return false;
            }
            else if (SelectedPlanItems.Select(p => p.Position.SequenceColour).Distinct().Count() > 1)
            {
                ReverseSequenceOrderCommand.DisabledReason = Message.PlanVisualDocument_ReverseSequenceOrder_DisabledReason2;
                return false;
            }
            else if (!AreConsecutive(SelectedPlanItems.Select(p => p.Position.Sequence)))
            {
                ReverseSequenceOrderCommand.DisabledReason = Message.PlanVisualDocument_ReverseSequenceOrder_DisabledReason3;
                return false;
            }
            return true;
        }

        private void ReverseSequenceOrderCommandExecuted()
        {
            IEnumerable<PlanogramPositionView> positions = SelectedPlanItems
                .Where(p => p.PlanItemType == PlanItemType.Position && p.Position != null)
                .Select(p => p.Position);
            if (!positions.Any()) return;

            Planogram.BeginUpdate();
            Int16 sequenceX = positions.Min(p => p.SequenceX);
            Int16 sequence = positions.Min(p => p.Sequence);
            foreach (PlanogramPositionView position in positions.OrderByDescending(p => p.SequenceX).ToList())
            {
                position.SequenceX = sequenceX++;
                position.Sequence = sequence++;
            }
            Planogram.EndUpdate();
        }

        #endregion

        #region ShowSequenceGroupsCommand

        private RelayCommand _showSequenceGroupsCommand = null;

        public RelayCommand ShowSequenceGroupsCommand
        {
            get
            {
                if (_showSequenceGroupsCommand == null)
                {
                    _showSequenceGroupsCommand = new RelayCommand(
                        p => ShowSequenceGroupsCommandExecuted(),
                        p => ShowSequenceGroupsCommandCanExecute())
                    {
                        FriendlyName = Message.PlanVisualDocument_ShowSequenceGroups_Name,
                        FriendlyDescription = Message.PlanVisualDocument_ShowSequenceGroups_Description,
                        Icon = ImageResources.SequenceTemplateRibbon_ShowSequenceGroups,
                    };
                }
                return _showSequenceGroupsCommand;
            }
        }

        private Boolean ShowSequenceGroupsCommandCanExecute()
        {
            return true;
        }

        private void ShowSequenceGroupsCommandExecuted()
        {
            this.ParentController.IsSequenceGroupsPanelVisible = !this.ParentController.IsSequenceGroupsPanelVisible;
        }

        #endregion

        #region AddSequenceSubGroupCommand

        private RelayCommand _addSequenceSubGroupCommand = null;

        public RelayCommand AddSequenceSubGroupCommand
        {
            get
            {
                if (_addSequenceSubGroupCommand == null)
                {
                    _addSequenceSubGroupCommand = new RelayCommand(
                        p => AddSequenceSubGroupCommandExecuted(),
                        p => AddSequenceSubGroupCommandCanExecute())
                    {
                        FriendlyName = Message.PlanVisualDocument_AddSequenceSubGroup_Name,
                        FriendlyDescription = Message.PlanVisualDocument_AddSequenceSubGroup_Description,
                        Icon = ImageResources.SequenceTemplateRibbon_AddSequenceSubGroup,
                    };
                }
                return _addSequenceSubGroupCommand;
            }
        }

        private Boolean AddSequenceSubGroupCommandCanExecute()
        {
            if (!SelectedPlanItems.All(i => i.PlanItemType == PlanItemType.Position && i.Position != null))
            {
                AddSequenceSubGroupCommand.DisabledReason = Message.PlanVisualDocument_AddSequenceSubGroup_DisabledReason1;
                return false;
            }
            else if (SelectedPlanItems.Select(p => p.Position.SequenceColour).Distinct().Count() > 1)
            {
                AddSequenceSubGroupCommand.DisabledReason = Message.PlanVisualDocument_AddSequenceSubGroup_DisabledReason2;
                return false;
            }
            else if (!AreConsecutive(SelectedPlanItems.Select(p => p.Position.Sequence)))
            {
                AddSequenceSubGroupCommand.DisabledReason = Message.PlanVisualDocument_AddSequenceSubGroup_DisabledReason3;
                return false;
            }
            return true;
        }

        private Boolean AreConsecutive(IEnumerable<Int16> numbers)
        {
            if (!numbers.Any()) return false;
            IEnumerable<Int16> orderedNumbers = numbers.OrderBy(n => n).ToList();
            Int16 previousNumber = orderedNumbers.First();
            foreach (Int16 number in orderedNumbers.Skip(1))
            {
                if (Math.Abs(number - previousNumber) > 1) return false;
                previousNumber = number;
            }
            return true;
        }

        private void AddSequenceSubGroupCommandExecuted()
        {
            IEnumerable<PlanogramPositionView> positions = SelectedPlanItems
                .Where(p => p.PlanItemType == PlanItemType.Position)
                .Select(p => p.Position);
            if (!positions.Any()) return;
            PlanogramSequenceGroupViewModel sequenceGroupView = SequenceGroups
                .FirstOrDefault(g => g.Colour.Equals(positions.First().SequenceColour));
            if (sequenceGroupView == null) return;
            IEnumerable<String> positionGtins = positions.Select(p => p.Product.Gtin).ToList();
            IEnumerable<PlanogramSequenceGroupProduct> sequenceProducts =
                sequenceGroupView.Model.Products.Where(p => positionGtins.Contains(p.Gtin));
            sequenceGroupView.Model.SubGroups.Add(PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup(sequenceProducts));
            OnPropertyChanged(SequenceGroupsProperty);
        }

        #endregion

        #region RemoveSequenceSubGroupCommand

        private RelayCommand _removeSequenceSubGroupCommand = null;

        public RelayCommand RemoveSequenceSubGroupCommand
        {
            get
            {
                if (_removeSequenceSubGroupCommand == null)
                {
                    _removeSequenceSubGroupCommand = new RelayCommand(
                        p => RemoveSequenceSubGroupCommandExecuted(),
                        p => RemoveSequenceSubGroupCommandCanExecute())
                    {
                        FriendlyName = Message.PlanVisualDocument_RemoveSequenceSubGroup_Name,
                        FriendlyDescription = Message.PlanVisualDocument_RemoveSequenceSubGroup_Description,
                        Icon = ImageResources.SequenceTemplateRibbon_RemoveSequenceSubGroup,
                    };
                }
                return _removeSequenceSubGroupCommand;
            }
        }

        private Boolean RemoveSequenceSubGroupCommandCanExecute()
        {
            if (!SelectedPlanItems.Any(i => i.PlanItemType == PlanItemType.Position && i.Position != null))
            {
                RemoveSequenceSubGroupCommand.DisabledReason = Message.PlanVisualDocument_RemoveSequenceSubGroup_DisabledReason1;
                return false;
            }
            else if (!SelectedPlanItems.Where(p => p.PlanItemType == PlanItemType.Position).Select(p => p.Position).Any(p => p.GetSequenceSubGroup() != null))
            {
                RemoveSequenceSubGroupCommand.DisabledReason = Message.PlanVisualDocument_RemoveSequenceSubGroup_DisabledReason2;
                return false;
            }
            return true;
        }

        private void RemoveSequenceSubGroupCommandExecuted()
        {
            IEnumerable<PlanogramPositionView> positions = SelectedPlanItems
                .Where(p => p.PlanItemType == PlanItemType.Position && p.Position != null)
                .Select(p => p.Position);
            if (!positions.Any()) return;
            IEnumerable<PlanogramSequenceGroupSubGroup> sequenceSubGroups = positions
                .Select(p => p.GetSequenceSubGroup())
                .Where(s => s != null)
                .Distinct();
            foreach (PlanogramSequenceGroupSubGroup sequenceSubGroup in sequenceSubGroups)
            {
                PlanogramSequenceGroup sequenceGroup = sequenceSubGroup.Parent;
                if (sequenceGroup == null) continue;
                sequenceGroup.SubGroups.Remove(sequenceSubGroup);
            }
            OnPropertyChanged(SequenceGroupsProperty);
        }

        #endregion

        #region AddSequenceSubGroupProductCommand

        private RelayCommand _addSequenceSubGroupProductCommand = null;

        public RelayCommand AddSequenceSubGroupProductCommand
        {
            get
            {
                if (_addSequenceSubGroupProductCommand == null)
                {
                    _addSequenceSubGroupProductCommand = new RelayCommand(
                        p => AddSequenceSubGroupProductCommandExecuted(),
                        p => AddSequenceSubGroupProductCommandCanExecute())
                    {
                        FriendlyName = Message.PlanVisualDocument_AddProductToSubGroup_Name,
                        FriendlyDescription = Message.PlanVisualDocument_AddProductToSubGroup_Description,
                        Icon = ImageResources.Add_32,
                        SmallIcon = ImageResources.Add_16,
                    };
                }
                return _addSequenceSubGroupProductCommand;
            }
        }

        private Boolean AddSequenceSubGroupProductCommandCanExecute()
        {
            if (!SelectedPlanItems.All(i => i.PlanItemType == PlanItemType.Position && i.Position != null))
            {
                AddSequenceSubGroupProductCommand.DisabledReason = Message.PlanVisualDocument_AddSequenceSubGroupProduct_DisabledReason1;
                return false;
            }
            else if (SelectedPlanItems.Select(p => p.Position.SequenceColour).Distinct().Count() > 1)
            {
                AddSequenceSubGroupProductCommand.DisabledReason = Message.PlanVisualDocument_AddSequenceSubGroupProduct_DisabledReason2;
                return false;
            }
            return true;
        }

        private void AddSequenceSubGroupProductCommandExecuted()
        {
            IEnumerable<PlanogramPositionView> positions = SelectedPlanItems
                .Where(p => p.PlanItemType == PlanItemType.Position && p.Position != null)
                .Select(p => p.Position);
            ILookup<PlanogramComponentView, PlanogramPositionView> positionsByComponent = Planogram
                .EnumerateAllPositions()
                .ToLookup(p => p.Component);

            foreach (PlanogramPositionView position in positions.OrderBy(p => p.Sequence))
            {
                if (!positionsByComponent.Contains(position.Component)) continue;
                IEnumerable<PlanogramPositionView> componentPositions = positionsByComponent[position.Component];
                PlanogramPositionView positionBefore = componentPositions.FirstOrDefault(p => p.Sequence == (position.Sequence - 1));
                PlanogramPositionView positionAfter = componentPositions.FirstOrDefault(p => p.Sequence == (position.Sequence + 1));
                PlanogramSequenceGroupSubGroup subGroupBefore = positionBefore == null ? null : positionBefore.GetSequenceSubGroup();
                PlanogramSequenceGroupSubGroup subGroupAfter = positionAfter == null ? null : positionAfter.GetSequenceSubGroup();

                PlanogramSequenceGroupSubGroup subGroupToUse;
                if (subGroupBefore == null && subGroupAfter == null)
                {
                    AddSequenceSubGroupCommandExecuted();
                    continue;
                }
                else if (subGroupBefore != null && subGroupAfter != null)
                {
                    subGroupToUse = subGroupBefore;
                }
                else
                {
                    subGroupToUse = subGroupBefore ?? subGroupAfter;
                }

                PlanogramSequenceGroupProduct sequenceProduct = subGroupToUse.Parent.Products
                    .FirstOrDefault(p => p.Gtin.Equals(position.Product.Gtin));
                if (sequenceProduct == null) continue;
                sequenceProduct.PlanogramSequenceGroupSubGroupId = subGroupToUse.Id;
            }

            OnPropertyChanged(SequenceGroupsProperty);
        }

        #endregion

        #region RemoveSequenceSubGroupProductCommand

        private RelayCommand _removeSequenceSubGroupProductCommand = null;

        public RelayCommand RemoveSequenceSubGroupProductCommand
        {
            get
            {
                if (_removeSequenceSubGroupProductCommand == null)
                {
                    _removeSequenceSubGroupProductCommand = new RelayCommand(
                        p => RemoveSequenceSubGroupProductCommandExecuted(),
                        p => RemoveSequenceSubGroupProductCommandCanExecute())
                    {
                        FriendlyName = Message.PlanVisualDocument_RemoveProductFromSubGroup_Name,
                        FriendlyDescription = Message.PlanVisualDocument_RemoveProductFromSubGroup_Description,
                        Icon = ImageResources.Delete_32,
                        SmallIcon = ImageResources.Delete_16,
                    };
                }
                return _removeSequenceSubGroupProductCommand;
            }
        }

        private Boolean RemoveSequenceSubGroupProductCommandCanExecute()
        {
            if (!SelectedPlanItems.All(i => i.PlanItemType == PlanItemType.Position && i.Position != null))
            {
                RemoveSequenceSubGroupProductCommand.DisabledReason = Message.PlanVisualDocument_RemoveSequenceSubGroupProduct_DisabledReason1;
                return false;
            }
            return true;
        }

        private void RemoveSequenceSubGroupProductCommandExecuted()
        {
            IEnumerable<PlanogramPositionView> positions = SelectedPlanItems
                .Where(p => p.PlanItemType == PlanItemType.Position && p.Position != null)
                .Select(p => p.Position);

            foreach (PlanogramPositionView position in positions)
            {
                PlanogramSequenceGroupSubGroup subGroup = position.GetSequenceSubGroup();
                if (subGroup == null) continue;
                PlanogramSequenceGroupProduct sequenceProduct = subGroup.EnumerateProducts()
                    .FirstOrDefault(p => p.Gtin.Equals(position.Product.Gtin));
                if (sequenceProduct == null) continue;
                sequenceProduct.PlanogramSequenceGroupSubGroupId = null;
            }

            OnPropertyChanged(SequenceGroupsProperty);
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Re-builds the <see cref="SequenceGroups"/> collection.
        /// </summary>
        private void RefreshSequenceGroups()
        {
            List<PlanogramSequenceGroupViewModel> sequenceGroups = new List<PlanogramSequenceGroupViewModel>();
            ILookup<Int32, PlanogramComponentView> componentViewsByColour = Planogram.EnumerateAllComponents()
                .ToLookup(c => c.FillColour, c => c);

            foreach (PlanogramSequenceGroup sequenceGroup in Planogram.Model.Sequence.Groups)
            {
                IEnumerable<PlanogramComponentView> components = null;
                if (componentViewsByColour.Contains(sequenceGroup.Colour))
                {
                    components = componentViewsByColour[sequenceGroup.Colour];
                }
                if (components == null) continue;
                sequenceGroups.Add(new PlanogramSequenceGroupViewModel(sequenceGroup, components));
            }

            _sequenceGroups.Clear();
            _sequenceGroups.AddRange(sequenceGroups);
        }

        #region General

        /// <summary>
        /// Attaches and detaches events to the source planogramview
        /// </summary>
        /// <param name="isAttach"></param>
        private void SetPlanEventHandlers(Boolean isAttach)
        {
            if (isAttach)
            {
                BeginSelectionBlinking();
                App.ViewState.Settings.ModelChanged += Settings_ModelChanged;

                this.SelectedPlanItems.BulkCollectionChanged += SelectedPlanItems_BulkCollectionChanged;
            }
            else
            {
                StopSelectionBlinking();
                App.ViewState.Settings.ModelChanged -= Settings_ModelChanged;
                this.SelectedPlanItems.BulkCollectionChanged -= SelectedPlanItems_BulkCollectionChanged;
            }
        }

        /// <summary>
        /// Called whenever this document should
        /// stop updating
        /// </summary>
        protected override void OnFreeze()
        {
            //dettach event handlers
            SetPlanEventHandlers(false);

            //freeze the plan visual
            if (_planogramModelData != null)
            {
                _planogramModelData.Freeze();
            }
        }

        /// <summary>
        /// Called whenever this document should
        /// resume updating.
        /// </summary>
        protected override void OnUnfreeze()
        {
            //reset event handlers
            SetPlanEventHandlers(true);

            //refresh data
            if (_planogramModelData != null)
            {
                _planogramModelData.Dispose();
                _planogramModelData = null;
                OnPropertyChanged(PlanogramModelDataProperty.Path);
            }

            OnPlanProcessCompleting();
            //OnUpdateHighlight();
            //NotifyHighlightResultChanged();
        }

        /// <summary>
        /// Carries out document specific actions based on
        /// the given keyboard shortcut keys.
        /// </summary>
        /// <param name="modifiers"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public override Boolean ProcessKeyboardShortcut(ModifierKeys modifiers, Key key)
        {
            Boolean isProcessed = base.ProcessKeyboardShortcut(modifiers, key);
            if (!isProcessed)
            {
                isProcessed = true;

                const Single moveAmount = 1;
                const Single resizeAmount = 5F;


                #region [Esc] - Reset mouse pointer / Clear selection
                if (key == Key.Escape)
                {
                    if (this.MouseToolType != MouseToolType.Pointer)
                    {
                        this.MouseToolType = MouseToolType.Pointer;
                    }
                    else if (this.SelectedPlanItems.Count > 0)
                    {
                        this.SelectedPlanItems.Clear();
                    }
                }
                #endregion

                #region [Shift + +] / [+] - Add position facings
                else if ((modifiers == ModifierKeys.Shift && key == Key.OemPlus)
                    || (modifiers == ModifierKeys.None && key == Key.Add))
                {
                    foreach (IPlanItem item in this.SelectedPlanItems)
                    {
                        if (item.PlanItemType == PlanItemType.Position)
                        {
                            item.Position.IncreaseFacings();
                        }
                    }
                }
                #endregion

                #region [Shift + -] / [-] - Subtract position facings
                else if ((modifiers == ModifierKeys.Shift && key == Key.OemMinus)
                    || (modifiers == ModifierKeys.None && key == Key.Subtract))
                {
                    foreach (IPlanItem item in this.SelectedPlanItems)
                    {
                        if (item.PlanItemType == PlanItemType.Position)
                        {
                            item.Position.DecreaseFacings();
                        }
                    }
                }
                #endregion

                #region  [Ctrl + arrow] - Move selected items
                else if (modifiers == ModifierKeys.Control
                    && (key == Key.Up || key == Key.Down || key == Key.Left || key == Key.Right))
                {
                    //create new undo if req
                    if (!this.Planogram.IsRecordingUndoableAction)
                    {
                        this.Planogram.BeginUndoableAction();
                        this.Planogram.PlanProcessCompleting += OnPlanItemsRepositioned;

                        this.SuppressPerformanceIntensiveUpdates = true;
                    }

                    Double changeX = 0;
                    Double changeY = 0;

                    Vector3D moveVector = new Vector3D(1, 1, 0);
                    switch (key)
                    {
                        case Key.Up:
                            changeY = moveAmount;
                            moveVector = new Vector3D(0, 1, 0);
                            break;
                        case Key.Down:
                            changeY = -moveAmount;
                            moveVector = new Vector3D(0, -1, 0);
                            break;
                        case Key.Left:
                            changeX = -moveAmount;
                            moveVector = new Vector3D(-1, 0, 0);
                            break;
                        case Key.Right:
                            changeX = moveAmount;
                            moveVector = new Vector3D(1, 0, 0);
                            break;
                    }

                    if (changeX != 0 || changeY != 0)
                    {
                        if (this.AttachedControl != null && this.ViewType != CameraViewType.Perspective)
                        {
                            moveVector = ModelConstruct3DHelper.FindItemMoveVector(this.AttachedControl.xViewer.Camera, changeX, changeY);
                        }

                        PlanItemHelper.RepositionItems(PlanItemHelper.StripOutCommonChildren(this.SelectedPlanItems), moveVector.X, moveVector.Y, moveVector.Z);
                    }
                }
                #endregion

                #region [Shift + arrow] -Add XY caps to positions or  resize selected fixtures items
                else if (modifiers == ModifierKeys.Shift
                        && (key == Key.Up || key == Key.Down || key == Key.Left || key == Key.Right))
                {
                    //create new undo if req
                    if (!this.Planogram.IsRecordingUndoableAction)
                    {
                        this.Planogram.BeginUndoableAction();
                    }

                    //if we have any positions then interpret this action as adding caps.
                    if (this.SelectedPlanItems.Any(i => i.PlanItemType == PlanItemType.Position))
                    {
                        foreach (IPlanItem item in this.SelectedPlanItems)
                        {
                            if (item.PlanItemType != PlanItemType.Position) continue;

                            switch (key)
                            {
                                case Key.Up:
                                    item.Position.AdjustYCapHigh(/*isUp*/true);
                                    break;
                                case Key.Down:
                                    item.Position.AdjustYCapHigh(/*isUp*/false);
                                    break;
                                case Key.Left:
                                    item.Position.AdjustXCapWide(/*isRight*/false);
                                    break;
                                case Key.Right:
                                    item.Position.AdjustXCapWide(/*isRight*/true);
                                    break;
                            }
                        }

                    }
                    else
                    {
                        Double changeX = 0;
                        Double changeY = 0;
                        Vector3D moveVector = new Vector3D(1, 1, 0);

                        switch (key)
                        {
                            case Key.Up:
                                changeY = resizeAmount;
                                moveVector = new Vector3D(0, 1, 0);
                                break;
                            case Key.Down:
                                changeY = -resizeAmount;
                                moveVector = new Vector3D(0, -1, 0);
                                break;
                            case Key.Left:
                                changeX = -resizeAmount;
                                moveVector = new Vector3D(-1, 0, 0);
                                break;
                            case Key.Right:
                                changeX = resizeAmount;
                                moveVector = new Vector3D(1, 0, 0);
                                break;
                        }

                        if (changeX != 0 || changeY != 0)
                        {

                            if (this.AttachedControl != null && this.ViewType != CameraViewType.Perspective)
                            {
                                moveVector =
                                    ModelConstruct3DHelper.FindItemMoveVector(
                                    this.AttachedControl.xViewer.Camera, changeX, changeY);
                            }
                            PlanItemHelper.ResizeItems(this.SelectedPlanItems, moveVector.X, moveVector.Y, moveVector.Z);
                        }
                    }
                }
                #endregion

                #region [Ctrl + Shift + arrow] - Rotate selected items.
                else if (modifiers == (ModifierKeys.Control | ModifierKeys.Shift)
                        && (key == Key.Up || key == Key.Down || key == Key.Left || key == Key.Right))
                {

                    //create new undo if req
                    if (!this.Planogram.IsRecordingUndoableAction)
                    {
                        this.Planogram.BeginUndoableAction();
                    }

                    Single changeAmount = Convert.ToSingle(2 / (360f / (2 * Math.PI)));

                    Double rotateX = 0;
                    Double rotateY = 0;
                    Vector3D moveVector = new Vector3D(1, 1, 0);

                    switch (key)
                    {
                        case Key.Up:
                            rotateX = changeAmount;
                            moveVector = new Vector3D(0, 1, 0);
                            break;
                        case Key.Down:
                            rotateX = -changeAmount;
                            moveVector = new Vector3D(0, -1, 0);
                            break;
                        case Key.Left:
                            rotateY = changeAmount;
                            moveVector = new Vector3D(-1, 0, 0);
                            break;
                        case Key.Right:
                            rotateY = -changeAmount;
                            moveVector = new Vector3D(1, 0, 0);
                            break;
                    }

                    if (rotateX != 0 || rotateY != 0)
                    {
                        if (this.AttachedControl != null && this.ViewType != CameraViewType.Perspective)
                        {
                            moveVector =
                                ModelConstruct3DHelper.FindItemMoveVector(
                                this.AttachedControl.xViewer.Camera, rotateX, rotateY);
                        }
                        PlanItemHelper.RotateItems(this.SelectedPlanItems, moveVector.X, moveVector.Y, moveVector.Z);
                    }
                }
                #endregion

                #region [Ctrl] + [A] - Select all items
                else if (modifiers == ModifierKeys.Control && key == Key.A)
                {
                    this.ClearAndSelectAllPlanItemsBasedOnSelectionType();
                }
                #endregion

                #region [arrow] - Select next item
                else if (modifiers == ModifierKeys.None
                    && (key == Key.Up || key == Key.Down || key == Key.Left || key == Key.Right))
                {
                    if (this.SelectedPlanItems.Count == 1)
                    {
                        IPlanItem curSelected = this.SelectedPlanItems[0];
                        IPlanItem newSelected = null;

                        if (curSelected.PlanItemType == PlanItemType.Position)
                        {
                            IEnumerable<PlanogramPositionView> allPositions =
                                this.Planogram.EnumerateAllPositions();

                            switch (key)
                            {
                                case Key.Up:
                                case Key.Right:
                                    newSelected = allPositions.SkipWhile(p => p != curSelected).ElementAtOrDefault(1);
                                    if (newSelected == null)
                                    {
                                        newSelected = allPositions.FirstOrDefault();
                                    }
                                    break;

                                case Key.Down:
                                case Key.Left:
                                    newSelected = newSelected = allPositions.TakeWhile(p => p != curSelected).LastOrDefault();
                                    if (newSelected == null)
                                    {
                                        newSelected = allPositions.LastOrDefault();
                                    }
                                    break;
                            }
                        }
                        else
                        {
                            //TODO


                        }

                        if (newSelected != null)
                        {
                            this.SelectedPlanItems.Clear();
                            this.SelectedPlanItems.Add(newSelected);
                        }
                    }
                }
                #endregion

                else
                {
                    isProcessed = false;
                }
            }

            return isProcessed;
        }

        /// <summary>
        /// Based on the current selection type settings apply all available selections
        /// </summary>
        /// <remarks>Clear() is done after calculating for intentional reasons. See SelectAllPositionsForMultisitedProduct() logic.</remarks>
        public void ClearAndSelectAllPlanItemsBasedOnSelectionType()
        {
            PlanItemSelection newSelection = new PlanItemSelection();

            switch (App.ViewState.SelectionMode)
            {
                case PlanItemSelectionType.OnlyPositions:
                    newSelection.AddRange(this.Planogram.EnumerateAllPositions());
                    break;

                case PlanItemSelectionType.OnlyComponents:
                    newSelection.AddRange(this.Planogram.EnumerateAllFixtureComponents());
                    break;

                case PlanItemSelectionType.PositionsAndComponents:
                    newSelection.AddRange(this.Planogram.EnumerateAllPositions());
                    newSelection.AddRange(this.Planogram.EnumerateAllFixtureComponents());
                    break;

                case PlanItemSelectionType.AllProductPositions:
                    newSelection.AddRange(SelectAllPositionsForMultisitedProduct());
                    break;

                case PlanItemSelectionType.ProductsAndComponents:
                    newSelection.AddRange(this.SelectAllPositionsForMultisitedProduct());
                    newSelection.AddRange(this.Planogram.EnumerateAllFixtureComponents());
                    break;

                default:
                    break;
            }

            this.SelectedPlanItems.Clear();
            this.SelectedPlanItems.AddRange(newSelection);
        }

        /// <summary>
        /// Selects all other positions that are a multisited product from the currently selected positions.
        /// </summary>
        /// <returns>All positions that require selecting</returns>
        private IEnumerable<PlanogramPositionView> SelectAllPositionsForMultisitedProduct()
        {
            PlanControllerViewModel planController = this.ParentController;
            if (planController == null) return null;

            // Get a list of products currently selected
            List<Object> productIds =
            planController.SelectedPlanItems.Where(p => p.Product != null).Select(p => p.Product.Model.Id).ToList();

            return this.Planogram.EnumerateAllPositions().Where(p => productIds.Contains(p.Product.Model.Id));
        }

        private void OnPlanItemsRepositioned(object sender, EventArgs e)
        {
            this.Planogram.PlanProcessCompleting -= OnPlanItemsRepositioned;
            this.SuppressPerformanceIntensiveUpdates = false;
            PlanItemHelper.FinalizeItemPositions(this.SelectedPlanItems);
            this.SelectedPlanItems.ValidateCurrentSelection();
        }

        /// <summary>
        /// Updates the selected items collection based on an item clicked by the user.
        /// </summary>
        /// <param name="hitPlanItem"></param>
        /// <param name="modifiers"></param>
        /// <param name="isMouseUp"></param>
        public void ProcessUserClickSelection(IPlanItem hitPlanItem, ModifierKeys modifiers, Boolean isMouseUp)
        {
            PlanItemSelection selectedPlanItems = this.SelectedPlanItems;

            //  If processing a MouseUp event and set to ignore the next one,
            //  clear the flag and return.
            if (isMouseUp && _ignoreNextMouseUpSelection)
            {
                _ignoreNextMouseUpSelection = false;
                return;
            }


            //if no item was hit then just clear:
            if (hitPlanItem == null)
            {
                _ignoreNextMouseUpSelection = false;
                if (selectedPlanItems.Count > 0) selectedPlanItems.Clear();
                return;
            }


            _ignoreNextMouseUpSelection = false;

            IPlanItem selectionItem = PlanItemHelper.GetSelectableItem(hitPlanItem);

            //if we cannot select the item then just clear.
            if (!CanUserSelect(selectionItem))
            {
                if (selectedPlanItems.Count > 0) selectedPlanItems.Clear();
                return;
            }



            //LI - remove this as it doesnt make much sense??
            // V8-31424: We don't necessarily want to remove all products from the selected items collection 
            // but rather just the one being removed - when we have closed the products list tab (Keeps selection esp when arranged).
            //if (!this.ParentController.PlanDocuments.Any(pd => pd.DocumentType == Model.DocumentType.ProductList))
            //{
            //    if (hitPlanItem.Product != null)
            //    {
            //        // If we have no product list open we should remove the product positions that are to be unselected.
            //        selectedPlanItems.RemoveRange(selectedPlanItems.Where(item => item.PlanItemType == PlanItemType.Product
            //        && item.Product.Model.Id == hitPlanItem.Product.Model.Id).ToList());
            //    }
            //}

            // Even if the list is not open we need to ensure we remove any selections that are duplicate products. (Previously all product positions were being removed).
            //List<IPlanItem> distinctItems = new List<IPlanItem>();
            //distinctItems.AddRange(selectedPlanItems.Distinct());
            //if (distinctItems.Any())
            //{
            //    selectedPlanItems.Clear();
            //    selectedPlanItems.AddRange(distinctItems);
            //}

            Boolean oldIsSelected = selectedPlanItems.Contains(selectionItem);

            //+ Mouse Down -
            if (!isMouseUp)
            {
                if (modifiers == ModifierKeys.None)
                {
                    if (!oldIsSelected)
                    {
                        //mousedown, no mod, not selected - select single.
                        if (selectedPlanItems.Count > 0) selectedPlanItems.Clear();
                        selectedPlanItems.AddRange(GetSelectionModeLinkedItems(selectionItem));

                        _ignoreNextMouseUpSelection = true;
                    }
                    else
                    {
                        //mouse down, no mod, already selected - do nothing.
                    }
                }
                else if (modifiers == ModifierKeys.Control)
                {
                    if (!oldIsSelected)
                    {
                        //mousedown, ctrl, not selected - add to existing selection
                        foreach (IPlanItem item in GetSelectionModeLinkedItems(selectionItem))
                        {
                            if (!selectedPlanItems.Contains(item))
                                selectedPlanItems.Add(item);
                        }

                        _ignoreNextMouseUpSelection = true;
                    }
                    else
                    {
                        //mousedown, ctrl, already selected - do nothing
                    }

                }
                else if (modifiers == ModifierKeys.Shift)
                {
                    #region Shift
                    if (selectionItem.PlanItemType == PlanItemType.Position
                                            && this.SelectedPlanItems.Count > 0
                                            && this.SelectedPlanItems.Last().PlanItemType == PlanItemType.Position)
                    {
                        if (!oldIsSelected)
                        {
                            // Based upon selections get real world co-ordinates to work out selected area                                    
                            List<PlanogramPositionView> selectedPositionPlanItems = selectedPlanItems.Where(p => p.PlanItemType == PlanItemType.Position).Select(p => p.Position).ToList();
                            Single xMinPos = selectedPositionPlanItems.Select(p => p.MetaWorldX).Min();
                            Single xMaxPos = selectedPositionPlanItems.Select(p => p.MetaWorldX + p.Width).Max();

                            Single yMinPos = selectedPositionPlanItems.Select(p => p.MetaWorldY).Min();
                            Single yMaxPos = selectedPositionPlanItems.Select(p => p.MetaWorldY + p.Height).Max();

                            Single zMinPos = selectedPositionPlanItems.Select(p => p.MetaWorldZ).Min();
                            Single zMaxPos = selectedPositionPlanItems.Select(p => p.MetaWorldZ + p.Depth).Max();

                            PlanogramPositionView positionView = selectionItem.Position;
                            Single metaWorldX = positionView.MetaWorldX;
                            Single width = positionView.Width;
                            if (metaWorldX < xMinPos) xMinPos = metaWorldX;
                            if (metaWorldX + width > xMaxPos) xMaxPos = metaWorldX + width;

                            Single metaWorldY = positionView.MetaWorldY;
                            Single height = positionView.Height;
                            if (metaWorldY < yMinPos) yMinPos = metaWorldY;
                            if (metaWorldY + height > yMaxPos) yMaxPos = metaWorldY + height;

                            Single metaWorldZ = positionView.MetaWorldZ;
                            Single depth = positionView.Depth;
                            if (metaWorldZ < zMinPos) zMinPos = metaWorldZ;
                            if (metaWorldZ + depth > zMaxPos) zMaxPos = metaWorldZ + depth;

                            // Create an object that represents the volume that we will be selecting.
                            RectValue selectedPositionWorldBounds = new RectValue()
                            {
                                X = xMinPos,
                                Y = yMinPos,
                                Z = zMinPos,
                                Width = (xMaxPos - xMinPos),
                                Height = (yMaxPos - yMinPos),
                                Depth = (zMaxPos - zMinPos)
                            };

                            // Now that we've got our search area, we need to enumerate through all the positions
                            // on the plan adding them to the selection as appropriate. We do this by sequencing
                            // them using default directions (L-R, B-T, F-B) and then adding them to the selection
                            // (which is the order they will be pasted in) in the order the are found in.
                            selectedPlanItems.Clear(); // Ensure that we add them in sequence order.

                            IEnumerable<PlanogramPositionView> sequenceSnake =
                                PlanogramSequenceGroup.EnumerateSequenceSnake<PlanogramPositionView>(
                                    PlanogramBlockingGroup.NewPlanogramBlockingGroup(), Planogram.EnumerateAllPositions());

                            foreach (PlanogramPositionView position in sequenceSnake)
                            {
                                // Create an object that represents this position
                                RectValue positionWorldBounds = new RectValue()
                                {
                                    X = position.MetaWorldX,
                                    Y = position.MetaWorldY,
                                    Z = position.MetaWorldZ,
                                    Width = position.Width,
                                    Height = position.Height,
                                    Depth = position.Depth
                                };

                                // is it within or selection area
                                if (positionWorldBounds.CollidesWith(selectedPositionWorldBounds) &&
                                    !selectedPlanItems.Contains(position))
                                {

                                    foreach (IPlanItem item in GetSelectionModeLinkedItems(position))
                                    {
                                        if (!selectedPlanItems.Contains(item))
                                            selectedPlanItems.Add(item);
                                    }
                                }
                            }
                        }
                        else
                        {
                            //mousedown, shft, already selected - do nothing.
                        }
                    }
                    #endregion
                }

            }

            //+ Mouse Up
            else
            {
                if (modifiers == ModifierKeys.None)
                {
                    if (!oldIsSelected)
                    {
                        //mouseup, no mod, not selected - do nothing
                        //should have been ignored during mouse down..
                    }
                    else
                    {
                        //mouseup, no mod, already selected - ensure single select
                        if (selectedPlanItems.Count > 1)
                        {
                            selectedPlanItems.Clear();
                            selectedPlanItems.AddRange(GetSelectionModeLinkedItems(selectionItem));
                        }
                    }
                }
                else if (modifiers == ModifierKeys.Control)
                {
                    if (!oldIsSelected)
                    {
                        //mouseup, ctrl, not selected - do nothing
                        //should have been ignored during mouse down..
                    }
                    else
                    {
                        //mouseup, ctrl, already selected - remove from selection
                        selectedPlanItems.RemoveRange(GetSelectionModeLinkedItems(selectionItem));
                    }
                }
                //else if (modifiers == ModifierKeys.Shift)
                //{
                //    if (selectionItem.PlanItemType == PlanItemType.Position
                //        && this.SelectedPlanItems.Count > 1)
                //    {
                //        //TODO:
                //        if (!oldIsSelected)
                //        {
                //            //mouseup, shft, not selected - ignore
                //        }
                //        //else
                //        //{
                //        //    //mouseup, shft, already selected - remove all between by seq no except selection item.
                //        //    Int32 selectionIdx = this.Planogram.GetSequenceIndex(selectionItem.Position);

                //        //    // V8-28765:
                //        //    // Removed this line, because it was deselecting items selected to the left of the 
                //        //    // first item selected.
                //        //    //this.SelectedPlanItems.RemoveRange(
                //        //    //    this.SelectedPlanItems.Where(p => p.PlanItemType == PlanItemType.Position
                //        //    //        && (this.Planogram.GetSequenceIndex(p.Position) > selectionIdx)).ToArray());
                //        //}
                //    }
                //}

            }
        }

        /// <summary>
        /// Notifies Positions that it should be unselected if no visible.
        /// </summary>
        public void ShowPositionsVisibleChanged()
        {
            if (!ShowPositions)
            {
                if (SelectedPlanItems.PositionView != null)
                {
                    var positions = SelectedPlanItems.PositionView.Items;
                    SelectedPlanItems.RemoveRange(positions);
                }
            }
        }

        /// <summary>
        /// Updates the highlight for this document.
        /// </summary>
        private void OnUpdateHighlight()
        {
            //if this document is not actually visible
            // then just flag the need for an update
            if (!IsVisible)
            {
                _isHighlightUpdateRequired = true;
                return;
            }

            _isHighlightUpdateRequired = false;

            //clear out legend items.
            if (_highlightLegendItems.Count > 0) _highlightLegendItems.Clear();

            if (this.Highlight == null)
            {
                this.PositionHighlights = null;
                return;
            }

            //get the highlight result.
            PlanogramHighlightResult result =
                this.ParentController.GetHighlightResult(this.Highlight);
            if (result == null) return;
            Debug.Assert(result.Highlight == this.Highlight, "Incorrect highlight result given to document");

            if (_highlightLegendItems.Count > 0) _highlightLegendItems.Clear();
            _highlightLegendItems.AddRange(result.Groups);

            //Apply it.
            this.PositionHighlights = new PlanogramPositionHighlightColours(result);
        }

        #endregion

        #region Fixture Related

        /// <summary>
        /// Adds a new component based on the given tooltype and position information
        /// </summary>
        /// <param name="toolType"></param>
        /// <param name="worldPosition"></param>
        /// <param name="worldRotation"></param>
        /// <param name="size"></param>
        /// <param name="nearestFixture"></param>
        /// <returns></returns>
        public IPlanItem AddNewComponent(MouseToolType toolType,
            PointValue worldPosition, RotationValue worldRotation, WidthHeightDepthValue size,
            PlanogramFixtureView nearestFixture)
        {
            if (toolType == Common.MouseToolType.CreateCustom)
            {
                return AddNewCustomComponent(worldPosition, worldRotation, size, nearestFixture);
            }
            else
            {

                IPlanItem addedItem = null;

                PlanogramView plan = this.Planogram;

                //set the plan as updating as we are going to change
                // multiple values so we don't want the ui to update until we are done.
                plan.BeginUpdate();

                //start a new plan edit to record changes.
                plan.BeginUndoableAction();


                //determine the parent fixture
                PlanogramFixtureView parentFixture = nearestFixture;
                if (parentFixture == null)
                {
                    parentFixture = (plan.Fixtures.Count > 0) ? plan.Fixtures.FirstOrDefault() : plan.AddFixture();
                }


                if (toolType == Common.MouseToolType.CreateText)
                {
                    //Create annotation.
                    PlanogramAnnotationView annotation = parentFixture.AddAnnotation();
                    annotation.IsSelectable = true;
                    PlanItemHelper.SetSize(annotation, size);

                    addedItem = annotation;
                }
                else
                {
                    //Create the component itself
                    PlanogramComponentType? componentType = LocalHelper.MouseToolTypeToPlanogramComponentType(toolType);
                    PlanogramComponentView component = parentFixture.AddComponent(componentType.Value);
                    component.IsSelectable = true;

                    PlanItemHelper.SetSize(component, size);

                    addedItem = component;
                }

                //Determine the local position and rotation of the new item relative to the fixture.
                PlanFixture3DData fixtureModelData = null;
                foreach (PlanFixture3DData fx in this.PlanogramModelData.EnumerateFixtureModels())
                {
                    IPlanItem item = fx.Fixture as IPlanItem;
                    if (item.Fixture == parentFixture)
                    {
                        fixtureModelData = fx;
                        break;
                    }
                }


                PointValue fixturePosition = fixtureModelData.Position;
                addedItem.X = Convert.ToSingle(Math.Round(worldPosition.X - fixturePosition.X));
                addedItem.Y = Convert.ToSingle(Math.Round(worldPosition.Y - fixturePosition.Y));
                addedItem.Z = Convert.ToSingle(Math.Round(worldPosition.Z - fixturePosition.Z));

                RotationValue fixtureRotation = fixtureModelData.Rotation;
                addedItem.Angle = worldRotation.Angle - fixtureRotation.Angle;
                addedItem.Slope = worldRotation.Slope - fixtureRotation.Slope;
                addedItem.Roll = worldRotation.Roll - fixtureRotation.Roll;


                //TODO: Do we need this?
                //PlanDocumentHelper.ShiftByCollisions(addedItem, this.ViewType);

                //complete the edit action.
                plan.EndUndoableAction();

                //end the update so that all affected ui gets refreshed.
                plan.EndUpdate();

                //select it
                this.SelectedPlanItems.SetSelection(addedItem);

                return addedItem;
            }
        }

        /// <summary>
        /// Creates a new custom component and launches the editor window.
        /// </summary>
        private IPlanItem AddNewCustomComponent(PointValue worldPosition, RotationValue worldRotation, WidthHeightDepthValue size,
            PlanogramFixtureView nearestFixture)
        {
            IPlanItem addedItem = null;

            PlanogramView plan = this.Planogram;

            //set the plan as updating as we are going to change
            // multiple values so we don't want the ui to update until we are done.
            plan.BeginUpdate();

            //start a new plan edit to record changes.
            plan.BeginUndoableAction();

            //determine the parent fixture
            PlanogramFixtureView parentFixture = nearestFixture;
            if (parentFixture == null)
            {
                parentFixture = (plan.Fixtures.Count > 0) ? plan.Fixtures.FirstOrDefault() : plan.AddFixture();
            }

            //Create the empty component
            PlanogramComponentView component = parentFixture.AddEmptyCustomComponent(size.Height, size.Width, size.Depth);

            //Determine the local position and rotation of the new item relative to the fixture.
            PlanFixture3DData fixtureModelData = null;
            foreach (PlanFixture3DData fx in this.PlanogramModelData.EnumerateFixtureModels())
            {
                IPlanItem item = fx.Fixture as IPlanItem;
                if (item.Fixture == parentFixture)
                {
                    fixtureModelData = fx;
                    break;
                }
            }


            PointValue fixturePosition = fixtureModelData.Position;
            component.X = Convert.ToSingle(Math.Round(worldPosition.X - fixturePosition.X));
            component.Y = Convert.ToSingle(Math.Round(worldPosition.Y - fixturePosition.Y));
            component.Z = Convert.ToSingle(Math.Round(worldPosition.Z - fixturePosition.Z));

            RotationValue fixtureRotation = fixtureModelData.Rotation;
            component.Angle = worldRotation.Angle - fixtureRotation.Angle;
            component.Slope = worldRotation.Slope - fixtureRotation.Slope;
            component.Roll = worldRotation.Roll - fixtureRotation.Roll;


            plan.EndUndoableAction();

            //select it show the component editor
            this.SelectedPlanItems.SetSelection(component);

            Boolean editApplied = this.ParentController.EditComponentParts();
            if (!editApplied)
            {
                //undo the component add action
                plan.Undo();
            }
            else
            {
                addedItem = component;
            }

            //end the update so that all affected ui gets refreshed.
            plan.EndUpdate();

            return addedItem;
        }

        /// <summary>
        /// Adds the fixture info to this planogram based on the given hit result.
        /// </summary>
        /// <param name="fixturePackgeInfo"></param>
        /// <param name="nearestSubHitResult"></param>
        public List<IPlanItem> AddFixtureLibraryItem(FixturePackageInfo fixturePackgeInfo, PlanHitTestResult nearestSubHitResult)
        {
            base.ShowWaitCursor(true);

            //fetch the fixture library item.
            FixturePackageViewModel packageView = new FixturePackageViewModel();
            try
            {
                packageView.Fetch(fixturePackgeInfo);
            }
            catch (DataPortalException ex)
            {
                base.ShowWaitCursor(false);
                LocalHelper.RecordException(ex.GetBaseException());
                Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(fixturePackgeInfo.Name, Galleria.Framework.Controls.Wpf.OperationType.Open);

                packageView.Dispose();
                return null;
            }


            //begin a new plan edit
            PlanogramView plan = this.Planogram;
            plan.BeginUndoableAction();

            //determine the parent fixture to add to.
            PlanogramFixtureView nearestFixture = nearestSubHitResult.GetNearestFixtureView();
            if (nearestFixture == null)
            {
                nearestFixture = plan.Fixtures.LastOrDefault();
            }

            //add the item to the plan.
            List<IPlanItem> addedItems = packageView.AddToPlanogram(plan, nearestFixture);

            //dispose of the package view so that the fixture gets released.
            packageView.Dispose();
            packageView = null;

            if (addedItems.Count == 1 && addedItems[0].PlanItemType != PlanItemType.Fixture)
            {
                IPlanItem addedItem = addedItems[0];
                addedItem.IsSelectable = true;

                //use the hit test result to determine the world position and rotation of the new fixture.
                IModelConstruct3DData addedItemModelData = RenderControlsHelper.FindItemModelData(this.PlanogramModelData, addedItem);

                RotationValue curWorldRotation = ModelConstruct3DHelper.GetWorldRotation(addedItemModelData);

                //Snap to the nearest subcomponent
                if (nearestSubHitResult.NearestSubComponent != null)
                {
                    PlanSubComponent3DData subHitData = nearestSubHitResult.NearestSubComponent;
                    PointValue subHitLocalPos = nearestSubHitResult.GetSubComponentLocalHitPoint();
                    PointValue relativeHitPoint = new PointValue()
                    {
                        X = subHitData.Position.X + (subHitLocalPos.X - (fixturePackgeInfo.Width / 2F)),
                        Y = subHitData.Position.Y + (subHitLocalPos.Y - (fixturePackgeInfo.Height / 2F)),
                        Z = subHitData.Position.Z + subHitLocalPos.Z
                    };

                    PointValue snappedWorldPos;
                    RotationValue snappedWorldRotation;

                    SnapAndRotateToWorld(subHitData, addedItemModelData.Size,
                        relativeHitPoint, new RotationValue(), nearestSubHitResult.HitFace,
                        out snappedWorldPos, out snappedWorldRotation);

                    //Convert to local
                    PointValue snappedLocalPos = ModelConstruct3DHelper.ToLocal(snappedWorldPos, addedItemModelData);
                    RotationValue snappedLocalRotation = ModelConstruct3DHelper.ToLocal(snappedWorldRotation, addedItemModelData);

                    //apply to the added item
                    PlanItemHelper.SetPosition(addedItem, snappedLocalPos);
                    PlanItemHelper.SetRotation(addedItem, snappedLocalRotation);
                }
                else
                {

                    PointValue subWorldHitPoint = nearestSubHitResult.GetSubComponentWorldHitPoint();

                    PointValue itemWorldPosition =
                    new PointValue(subWorldHitPoint.X - (fixturePackgeInfo.Width / 2F),
                        subWorldHitPoint.Y - (fixturePackgeInfo.Height / 2F),
                        subWorldHitPoint.Z);

                    //apply to the added item
                    PlanItemHelper.SetPosition(addedItem, ModelConstruct3DHelper.ToLocal(itemWorldPosition, addedItemModelData));
                    PlanItemHelper.SetRotation(addedItem, new RotationValue());
                }
            }

            plan.EndUndoableAction();

            base.ShowWaitCursor(false);

            //select it
            if (this.SelectedPlanItems.Count > 0) this.SelectedPlanItems.Clear();
            this.SelectedPlanItems.AddRange(addedItems);

            return addedItems;
        }

        /// <summary>
        /// Outputs the world position and world rotation of the given item 
        /// if it were snapped based on the hit subdata.
        /// </summary>
        /// <param name="hitSubData"></param>
        /// <param name="itemSize"></param>
        /// <param name="relativeHitPoint"></param>
        /// <param name="proposedWorldRotation"></param>
        /// <param name="hitFace"></param>
        /// <param name="snappedWorldPosition"></param>
        /// <param name="snappedWorldRotation"></param>
        public static void SnapAndRotateToWorld(
            PlanSubComponent3DData hitSubData,
            WidthHeightDepthValue itemSize,
            PointValue relativeHitPoint,
            RotationValue proposedWorldRotation,
            ModelConstruct3DHelper.HitFace hitFace,
            out PointValue snappedWorldPosition,
            out RotationValue snappedWorldRotation)
        {
            PointValue newLocalPosition = new PointValue(relativeHitPoint.X, relativeHitPoint.Y, relativeHitPoint.Z);
            RotationValue newRotation = new RotationValue(proposedWorldRotation.Angle, proposedWorldRotation.Slope, proposedWorldRotation.Roll);

            switch (hitFace)
            {
                case ModelConstruct3DHelper.HitFace.Front:
                    {
                        //if the widths are the same then snap x
                        if (itemSize.Width == hitSubData.Size.Width)
                        {
                            newLocalPosition.X = hitSubData.Position.X;
                        }
                    }
                    break;

                case ModelConstruct3DHelper.HitFace.Back:
                    {
                        //if the widths are the same then snap x
                        if (itemSize.Width == hitSubData.Size.Width)
                        {
                            newLocalPosition.X = hitSubData.Position.X + itemSize.Width;
                        }

                        //rotate the component to match the sub back.
                        newRotation.Angle += Convert.ToSingle(CommonHelper.ToRadians(-180));
                    }
                    break;

                case ModelConstruct3DHelper.HitFace.Top:
                    {
                        //if the widths are the same then snap x
                        if (itemSize.Width == hitSubData.Size.Width)
                        {
                            newLocalPosition.X = hitSubData.Position.X;
                        }

                        //snap the z
                        newLocalPosition.Z = hitSubData.Position.Z;
                    }
                    break;

                case ModelConstruct3DHelper.HitFace.Bottom:
                    {
                        //if the widths are the same then snap x
                        if (itemSize.Width == hitSubData.Size.Width)
                        {
                            newLocalPosition.X = hitSubData.Position.X;
                        }

                        //snap the z
                        newLocalPosition.Z = hitSubData.Position.Z;
                    }
                    break;

                case ModelConstruct3DHelper.HitFace.Left:
                    {
                        //snap the z
                        newLocalPosition.Z = hitSubData.Position.Z;
                        //rotate to place against left face
                        newRotation.Angle += Convert.ToSingle(CommonHelper.ToRadians(-90));
                    }
                    break;

                case ModelConstruct3DHelper.HitFace.Right:
                    {
                        //snap the z
                        newLocalPosition.Z = hitSubData.Position.Z + itemSize.Width;
                        //rotate to place against the right face
                        newRotation.Angle += Convert.ToSingle(CommonHelper.ToRadians(90));
                    }
                    break;
            }


            //convert the position back to world space.
            snappedWorldPosition = ModelConstruct3DHelper.ToWorld(newLocalPosition, hitSubData);
            snappedWorldRotation = newRotation;

        }

        #endregion

        #region Position Related

        /// <summary>
        /// Adds new product positions to the subcomponent based on the given hit point.
        /// </summary>
        public void AddNewPositions(PlanogramSubComponentView subComponent, PointValue localHitPoint, IEnumerable<PlanogramProductView> products)
        {
            base.ShowWaitCursor(true);

            this.Planogram.BeginUndoableAction();

            //add the positions
            List<PlanogramPositionView> addedPositions =
                subComponent.AddPositions(products, localHitPoint.X, localHitPoint.Y, localHitPoint.Z);

            this.Planogram.EndUndoableAction();

            //select the new positions
            this.SelectedPlanItems.SetSelection(addedPositions);

            base.ShowWaitCursor(false);

        }

        /// <summary>
        /// Adds new product positions to the subcomponent based on the given anchor
        /// </summary>
        public void AddNewPositions(PlanogramSubComponentView subComponent, IEnumerable<PlanogramProductView> products,
            PlanogramPositionView anchor, PlanogramPositionAnchorDirection direction)
        {
            base.ShowWaitCursor(true);

            this.Planogram.BeginUndoableAction();

            //add the positions
            List<PlanogramPositionView> addedPositions = subComponent.AddPositions(products, anchor, direction);

            this.Planogram.EndUndoableAction();

            //select the new positions
            this.SelectedPlanItems.SetSelection(addedPositions);

            base.ShowWaitCursor(false);

        }

        /// <summary>
        /// Adds new product positions to the subcomponent based on the given hit point.
        /// </summary>
        public void AddNewPositions(PlanogramSubComponentView subComponent, PointValue localHitPoint, IEnumerable<Product> products)
        {
            base.ShowWaitCursor(true);

            this.Planogram.BeginUndoableAction();

            //Add the positions
            List<PlanogramPositionView> addedPositions = subComponent.AddPositions(products, localHitPoint);

            this.Planogram.EndUndoableAction();

            //select the new positions
            this.SelectedPlanItems.SetSelection(addedPositions);

            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Adds new product positions to the subcomponent based on the given hit point.
        /// </summary>
        public void AddNewPositions(PlanogramSubComponentView subComponent, IEnumerable<Product> products,
            PlanogramPositionView anchor, PlanogramPositionAnchorDirection direction)
        {
            base.ShowWaitCursor(true);

            this.Planogram.BeginUndoableAction();

            //Add the positions
            List<PlanogramPositionView> addedPositions = subComponent.AddPositions(products, anchor, direction);

            this.Planogram.EndUndoableAction();

            //select the new positions
            this.SelectedPlanItems.SetSelection(addedPositions);

            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Adds new product positions to the subcomponent based on the given hit point.
        /// </summary>
        public void AddNewPositions(PlanogramSubComponentView subComponent, PointValue localHitPoint, IEnumerable<ProductLibraryProduct> products)
        {
            base.ShowWaitCursor(true);

            this.Planogram.BeginUndoableAction();

            //Add the positions
            List<PlanogramPositionView> addedPositions = subComponent.AddPositions(products.Select(p => p.Product), localHitPoint);

            //Add performance for positions from library products
            this.AddNewPositionsPerformance(subComponent, addedPositions, products);

            this.Planogram.EndUndoableAction();

            //select the new positions
            this.SelectedPlanItems.SetSelection(addedPositions);

            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Adds new product positions to the subcomponent based on the given hit point.
        /// </summary>
        public void AddNewPositions(PlanogramSubComponentView subComponent, IEnumerable<ProductLibraryProduct> products,
            PlanogramPositionView anchor, PlanogramPositionAnchorDirection direction)
        {
            base.ShowWaitCursor(true);

            this.Planogram.BeginUndoableAction();

            //Add the positions
            List<PlanogramPositionView> addedPositions = subComponent.AddPositions(products.Select(p => p.Product), anchor, direction);

            //Add performance for positions from library products
            this.AddNewPositionsPerformance(subComponent, addedPositions, products);

            this.Planogram.EndUndoableAction();

            //select the new positions
            this.SelectedPlanItems.SetSelection(addedPositions);

            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Method to update the performance data based on the product library products added
        /// </summary>
        /// <param name="subComponent"></param>
        /// <param name="products"></param>
        public void AddNewPositionsPerformance(PlanogramSubComponentView subComponent, IEnumerable<PlanogramPositionView> addedPositions, IEnumerable<ProductLibraryProduct> libraryProducts)
        {
            if (subComponent != null)
            {
                Dictionary<String, PlanogramPositionView> positionsLookup = addedPositions.ToDictionary(p => p.Product.Gtin);

                //Enumerate through products and add performance data
                foreach (ProductLibraryProduct libraryProduct in libraryProducts)
                {
                    //Locate performance data created when product was added
                    if (positionsLookup.ContainsKey(libraryProduct.Gtin))
                    {
                        PlanogramPositionView newPosition = positionsLookup[libraryProduct.Gtin];
                        ParentController.CopyPerformanceData(libraryProduct, newPosition.Product.Model);
                    }
                }
            }
        }

        /// <summary>
        /// Returns the nearest position for position drag snapping.
        /// </summary>
        /// <param name="hitResult"></param>
        /// <param name="nearestMerchSubData"></param>
        /// <param name="ignorePositions"></param>
        /// <param name="posData"></param>
        /// <param name="direction"></param>
        public void GetPositionAnchor(
            PlanHitTestResult hitResult, PlanSubComponent3DData nearestMerchSubData,
            IEnumerable<PlanogramPositionView> ignorePositions,
            out PlanPosition3DData posData, out PlanogramPositionAnchorDirection? direction)
        {
            PlanPosition3DData anchorData = null;
            PlanogramPositionAnchorDirection? anchorDirection = null;

            if (hitResult != null && nearestMerchSubData != null)
            {
                PlanogramSubComponentView sub = (PlanogramSubComponentView)nearestMerchSubData.SubComponent;

                #region Direct Position Hit
                if (hitResult.ActualHitModelData is PlanPosition3DData)
                {
                    anchorData = (PlanPosition3DData)hitResult.ActualHitModelData;
                    IPlanPositionRenderable anchorPos = anchorData.PlanPosition;

                    #region Perspective
                    if (this.ViewType == CameraViewType.Perspective)
                    {
                        //Must hit the face directly.
                        switch (hitResult.HitFace)
                        {
                            case ModelConstruct3DHelper.HitFace.Front:
                                if (sub.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.BackStacked
                                    || sub.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.Even)
                                {
                                    anchorDirection = PlanogramPositionAnchorDirection.InFront;
                                }
                                break;

                            case ModelConstruct3DHelper.HitFace.Back:
                                if (sub.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.FrontStacked
                                    || sub.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.Even)
                                {
                                    anchorDirection = PlanogramPositionAnchorDirection.Behind;
                                }
                                break;

                            case ModelConstruct3DHelper.HitFace.Top:
                                if (sub.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.BottomStacked
                                    || sub.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.Even)
                                {
                                    anchorDirection = PlanogramPositionAnchorDirection.Above;
                                }
                                break;

                            case ModelConstruct3DHelper.HitFace.Bottom:
                                if (sub.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.TopStacked
                                    || sub.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.Even)
                                {
                                    anchorDirection = PlanogramPositionAnchorDirection.Below;
                                }
                                break;

                            case ModelConstruct3DHelper.HitFace.Left:
                                if (sub.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.RightStacked
                                    || sub.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.Even)
                                {
                                    anchorDirection = PlanogramPositionAnchorDirection.ToLeft;
                                }
                                break;

                            case ModelConstruct3DHelper.HitFace.Right:
                                if (sub.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.LeftStacked
                                    || sub.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.Even)
                                {
                                    anchorDirection = PlanogramPositionAnchorDirection.ToRight;
                                }
                                break;
                        }
                    }
                    #endregion

                    //Orthographic
                    else
                    {
                        AxisType[] axisOrder = sub.GetAxisPriorityOrder();

                        switch (hitResult.HitFace)
                        {
                            #region Front/Back
                            case ModelConstruct3DHelper.HitFace.Front:
                            case ModelConstruct3DHelper.HitFace.Back:
                                foreach (AxisType axis in axisOrder)
                                {
                                    switch (axis)
                                    {
                                        case AxisType.X:
                                            if (sub.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.LeftStacked
                                            || sub.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.RightStacked
                                            || sub.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.Even)
                                            {
                                                anchorDirection = (hitResult.ModelLocalHitPoint.X < anchorPos.Width / 2) ?
                                                    PlanogramPositionAnchorDirection.ToLeft : PlanogramPositionAnchorDirection.ToRight;
                                            }
                                            break;

                                        case AxisType.Y:
                                            if (sub.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.TopStacked
                                                || sub.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.BottomStacked
                                                || sub.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.Even)
                                            {
                                                anchorDirection = (hitResult.ModelLocalHitPoint.Y < anchorPos.Height / 2) ?
                                                    PlanogramPositionAnchorDirection.Below : PlanogramPositionAnchorDirection.Above;
                                            }
                                            break;

                                        case AxisType.Z:
                                            if (sub.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.FrontStacked
                                                || sub.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.BackStacked
                                                || sub.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.Even)
                                            {
                                                anchorDirection = (hitResult.HitFace == ModelConstruct3DHelper.HitFace.Front) ?
                                                    PlanogramPositionAnchorDirection.InFront : PlanogramPositionAnchorDirection.Behind;
                                            }
                                            break;
                                    }

                                    if (anchorDirection.HasValue) break;
                                }
                                break;
                            #endregion

                            #region Left / Right
                            case ModelConstruct3DHelper.HitFace.Left:
                            case ModelConstruct3DHelper.HitFace.Right:
                                foreach (AxisType axis in axisOrder)
                                {
                                    switch (axis)
                                    {
                                        case AxisType.X:
                                            if (sub.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.LeftStacked
                                            || sub.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.RightStacked
                                            || sub.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.Even)
                                            {
                                                anchorDirection = (hitResult.HitFace == ModelConstruct3DHelper.HitFace.Left) ?
                                                    PlanogramPositionAnchorDirection.ToLeft : PlanogramPositionAnchorDirection.ToRight;
                                            }
                                            break;

                                        case AxisType.Y:
                                            if (sub.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.TopStacked
                                               || sub.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.BottomStacked
                                               || sub.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.Even)
                                            {
                                                anchorDirection = (hitResult.ModelLocalHitPoint.Y < anchorPos.Height / 2) ?
                                                    PlanogramPositionAnchorDirection.Below : PlanogramPositionAnchorDirection.Above;
                                            }
                                            break;

                                        case AxisType.Z:
                                            if (sub.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.FrontStacked
                                                || sub.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.BackStacked
                                                || sub.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.Even)
                                            {
                                                anchorDirection = (hitResult.ModelLocalHitPoint.Z < anchorPos.Depth / 2) ?
                                                    PlanogramPositionAnchorDirection.Behind : PlanogramPositionAnchorDirection.InFront;
                                            }
                                            break;
                                    }
                                    if (anchorDirection.HasValue) break;
                                }
                                break;
                            #endregion

                            #region Top/Bottom
                            case ModelConstruct3DHelper.HitFace.Top:
                            case ModelConstruct3DHelper.HitFace.Bottom:
                                foreach (AxisType axis in axisOrder)
                                {
                                    switch (axis)
                                    {
                                        case AxisType.X:
                                            if (sub.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.LeftStacked
                                            || sub.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.RightStacked
                                            || sub.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.Even)
                                            {
                                                anchorDirection = (hitResult.ModelLocalHitPoint.X < anchorPos.Width / 2) ?
                                                    PlanogramPositionAnchorDirection.ToLeft : PlanogramPositionAnchorDirection.ToRight;
                                            }
                                            break;

                                        case AxisType.Y:
                                            if (sub.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.TopStacked
                                                || sub.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.BottomStacked
                                                || sub.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.Even)
                                            {
                                                anchorDirection = (hitResult.HitFace == ModelConstruct3DHelper.HitFace.Top) ?
                                                    PlanogramPositionAnchorDirection.Below : PlanogramPositionAnchorDirection.Above;
                                            }
                                            break;

                                        case AxisType.Z:
                                            if (sub.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.FrontStacked
                                                || sub.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.BackStacked
                                                || sub.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.Even)
                                            {
                                                anchorDirection = (hitResult.ModelLocalHitPoint.Z < anchorPos.Depth / 2) ?
                                                    PlanogramPositionAnchorDirection.Behind : PlanogramPositionAnchorDirection.InFront;
                                            }
                                            break;

                                    }
                                    if (anchorDirection.HasValue) break;
                                }
                                break;
                                #endregion
                        }
                    }

                }
                #endregion

                #region Find Nearest Position
                else
                {
                    PointValue subHitPoint = ModelConstruct3DHelper.ToLocal(hitResult.GetSubComponentWorldHitPoint(), nearestMerchSubData);

                    //compile a list of the directions in which we are going to check for positions in priority order
                    List<PlanogramPositionAnchorDirection> checkDirections = new List<PlanogramPositionAnchorDirection>();

                    AxisType[] axisOrder;
                    switch (this.ViewType)
                    {
                        default:
                            if (sub.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack)
                            {
                                axisOrder = new AxisType[] { AxisType.Z, AxisType.X, AxisType.Y };
                            }
                            else
                            {
                                axisOrder = new AxisType[] { AxisType.Y, AxisType.Z, AxisType.X };
                            }
                            break;

                        case CameraViewType.Top:
                        case CameraViewType.Bottom:
                            axisOrder = new AxisType[] { AxisType.Z, AxisType.X, AxisType.Y };
                            break;


                    }

                    foreach (AxisType axis in axisOrder)
                    {
                        switch (axis)
                        {
                            case AxisType.X:
                                {
                                    if (sub.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.LeftStacked
                                    || sub.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.Even)
                                    {
                                        checkDirections.Add(PlanogramPositionAnchorDirection.ToRight);
                                    }
                                    else if (sub.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.RightStacked)
                                    {
                                        checkDirections.Add(PlanogramPositionAnchorDirection.ToLeft);
                                    }
                                }
                                break;

                            case AxisType.Y:
                                {
                                    if (sub.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.TopStacked)
                                    {
                                        checkDirections.Add(PlanogramPositionAnchorDirection.Below);
                                    }
                                    else if (sub.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.BottomStacked
                                        || sub.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.Even)
                                    {
                                        checkDirections.Add(PlanogramPositionAnchorDirection.Above);
                                    }
                                }
                                break;

                            case AxisType.Z:
                                {
                                    if (sub.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.FrontStacked
                                    || sub.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.Even)
                                    {
                                        checkDirections.Add(PlanogramPositionAnchorDirection.Behind);
                                    }
                                    else if (sub.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.BackStacked)
                                    {
                                        checkDirections.Add(PlanogramPositionAnchorDirection.InFront);
                                    }
                                }
                                break;
                        }

                    }


                    //remove any directions that are not available according to the merch type.
                    if (sub.MerchandisingType == PlanogramSubComponentMerchandisingType.HangFromBottom) checkDirections.Remove(PlanogramPositionAnchorDirection.Above);
                    else if (sub.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack) checkDirections.Remove(PlanogramPositionAnchorDirection.Below);


                    //cycle through checking the suggested directions.
                    foreach (PlanogramPositionAnchorDirection checkDirection in checkDirections)
                    {
                        switch (checkDirection)
                        {
                            #region Above
                            case PlanogramPositionAnchorDirection.Above:
                                {
                                    //check if we are above a position
                                    PlanogramPositionView posBelow = null;
                                    foreach (PlanogramPositionView pos in sub.Positions)
                                    {
                                        if (!ignorePositions.Contains(pos))
                                        {
                                            if (subHitPoint.X >= pos.X && subHitPoint.X <= (pos.X + pos.Width))
                                            {
                                                if (posBelow == null || pos.SequenceY > posBelow.SequenceY)
                                                {
                                                    posBelow = pos;
                                                }
                                            }
                                        }
                                    }

                                    if (posBelow != null)
                                    {
                                        anchorDirection = PlanogramPositionAnchorDirection.Above;
                                        anchorData = RenderControlsHelper.FindItemModelData(this.PlanogramModelData, posBelow) as PlanPosition3DData;
                                    }
                                }
                                break;
                            #endregion

                            #region Below
                            case PlanogramPositionAnchorDirection.Below:
                                {
                                    //get the position above
                                    PlanogramPositionView posAbove = null;
                                    foreach (PlanogramPositionView pos in sub.Positions)
                                    {
                                        if (!ignorePositions.Contains(pos))
                                        {
                                            //if within x bounds
                                            if (subHitPoint.X >= pos.X && subHitPoint.X <= (pos.X + pos.Width))
                                            {
                                                //if the position is  above the hit point
                                                if (pos.Y > subHitPoint.Y)
                                                {
                                                    if (posAbove == null || pos.Y < posAbove.Y)
                                                    {
                                                        posAbove = pos;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (posAbove != null)
                                    {
                                        anchorDirection = PlanogramPositionAnchorDirection.Below;
                                        anchorData = RenderControlsHelper.FindItemModelData(this.PlanogramModelData, posAbove) as PlanPosition3DData;
                                    }
                                }
                                break;
                            #endregion

                            #region InFront
                            case PlanogramPositionAnchorDirection.InFront:
                                {
                                    PlanogramPositionView posBehind = null;
                                    foreach (PlanogramPositionView pos in sub.Positions)
                                    {
                                        if (!ignorePositions.Contains(pos))
                                        {
                                            //if within x bounds
                                            if (subHitPoint.X >= pos.X && subHitPoint.X <= (pos.X + pos.Width))
                                            {
                                                //if the position is  behind of the hit point
                                                if (pos.Z < subHitPoint.Z)
                                                {
                                                    if (posBehind == null || pos.Z > posBehind.Z)
                                                    {
                                                        posBehind = pos;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (posBehind != null)
                                    {
                                        anchorDirection = PlanogramPositionAnchorDirection.InFront;
                                        anchorData = RenderControlsHelper.FindItemModelData(this.PlanogramModelData, posBehind) as PlanPosition3DData;
                                    }
                                }
                                break;
                            #endregion

                            #region Behind
                            case PlanogramPositionAnchorDirection.Behind:
                                {
                                    PlanogramPositionView posInFront = null;
                                    foreach (PlanogramPositionView pos in sub.Positions)
                                    {
                                        if (!ignorePositions.Contains(pos))
                                        {
                                            //if within x bounds
                                            if (subHitPoint.X >= pos.X && subHitPoint.X <= (pos.X + pos.Width))
                                            {
                                                //if the position is  in front of the hit point
                                                if (pos.Z > subHitPoint.Z)
                                                {
                                                    if (posInFront == null || pos.Z < posInFront.Z)
                                                    {
                                                        posInFront = pos;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (posInFront != null)
                                    {
                                        anchorDirection = PlanogramPositionAnchorDirection.Behind;
                                        anchorData = RenderControlsHelper.FindItemModelData(this.PlanogramModelData, posInFront) as PlanPosition3DData;
                                    }
                                }
                                break;
                            #endregion

                            #region Left
                            case PlanogramPositionAnchorDirection.ToLeft:
                                {
                                    //get the position to right
                                    PlanogramPositionView posRight = null;
                                    foreach (PlanogramPositionView pos in sub.Positions)
                                    {
                                        if (!ignorePositions.Contains(pos))
                                        {
                                            //if within y bounds
                                            if (subHitPoint.Y >= pos.Y && subHitPoint.Y <= (pos.Y + pos.Height))
                                            {
                                                //if the position is to the right of the hit point
                                                if (pos.X > subHitPoint.X)
                                                {
                                                    if (posRight == null || pos.X < posRight.X)
                                                    {
                                                        posRight = pos;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (posRight != null)
                                    {
                                        anchorDirection = PlanogramPositionAnchorDirection.ToLeft;
                                        anchorData = RenderControlsHelper.FindItemModelData(this.PlanogramModelData, posRight) as PlanPosition3DData;
                                    }
                                }
                                break;
                            #endregion

                            #region Right
                            case PlanogramPositionAnchorDirection.ToRight:
                                {
                                    //get the position to left
                                    PlanogramPositionView posLeft = null;
                                    foreach (PlanogramPositionView pos in sub.Positions)
                                    {
                                        if (!ignorePositions.Contains(pos))
                                        {
                                            //if within y bounds
                                            if (subHitPoint.Y >= pos.Y && subHitPoint.Y <= (pos.Y + pos.Height))
                                            {
                                                //if the position is to the left of the hit point
                                                if (pos.X < subHitPoint.X)
                                                {
                                                    if (posLeft == null || pos.X > posLeft.X)
                                                    {
                                                        posLeft = pos;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (posLeft != null)
                                    {
                                        anchorDirection = PlanogramPositionAnchorDirection.ToRight;
                                        anchorData = RenderControlsHelper.FindItemModelData(this.PlanogramModelData, posLeft) as PlanPosition3DData;
                                    }
                                }
                                break;
                                #endregion
                        }

                        if (anchorDirection.HasValue) break;
                    }
                }
                #endregion
            }

            posData = anchorData;
            direction = anchorDirection;
        }

        /// <summary>
        /// Sets the plan item selection to the positions for the given highlight result group.
        /// </summary>
        public void SelectHighlightGroupPositions(PlanogramHighlightResultGroup group)
        {
            List<Object> positionIds = group.Items.Cast<PlanogramPosition>().Select(p => p.Id).ToList();

            List<PlanogramPositionView> positionsToSelect =
                this.Planogram.EnumerateAllPositions().Where(p => positionIds.Contains(p.Model.Id))
                .ToList();

            this.SelectedPlanItems.SetSelection(positionsToSelect);
        }

        #endregion

        #region Selection Blinking

        private CancellationTokenSource _selectionBlinkCancellation;

        private void StopSelectionBlinking()
        {
            _isSelectionBlinking = false;

            if (_selectionBlinkCancellation != null)
            {
                _selectionBlinkCancellation.Cancel();
                _selectionBlinkCancellation.Dispose();
                _selectionBlinkCancellation = null;
            }
        }

        private void BeginSelectionBlinking()
        {
            if (App.Current == null) return;
            if (!App.ViewState.Settings.Model.IsShowSelectionAsBlinkingEnabled) return;
            if (!this.IsVisible || this.AttachedControl == null) return;
            if (_isSelectionBlinking) return;

            if (_isSelectionBlinking
                && _selectionBlinkCancellation != null
                && !_selectionBlinkCancellation.IsCancellationRequested)
            {
                //already blinking so dont bother
                return;
            }


            _isSelectionBlinking = true;

            if (_selectionBlinkCancellation != null && !_selectionBlinkCancellation.IsCancellationRequested)
            {
                _selectionBlinkCancellation.Cancel();
                _selectionBlinkCancellation.Dispose();
                _selectionBlinkCancellation = null;
            }

            _selectionBlinkCancellation = new CancellationTokenSource();
            CancellationToken ct = _selectionBlinkCancellation.Token;

            var ui = System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext();

            System.Threading.Tasks.Task.Factory.StartNew(
                ((arg) =>
                    {
                        System.Threading.Tasks.TaskScheduler dp = (System.Threading.Tasks.TaskScheduler)arg;

                        while (!ct.IsCancellationRequested)
                        {
                            Thread.Sleep(App.ViewState.Settings.Model.BlinkSpeed);

                            if (!ct.IsCancellationRequested)
                            {
                                System.Threading.Tasks.Task.Factory.StartNew(
                                    (() =>
                                    {
                                        if (this.IsVisible)
                                        {
                                            //Stop when instructed to do so AND selection colour has been returned to original colour (amber). 
                                            if (!_isSelectionBlinking && !_isSelectionAlternateOn) return;

                                            _isSelectionAlternateOn = !_isSelectionAlternateOn;
                                            OnPropertyChanged(PlanRenderSettings.SelectionColourPropertyName);
                                        }

                                    }), ct, System.Threading.Tasks.TaskCreationOptions.None, dp);
                            }
                        }
                    }),
                    ui,
                    _selectionBlinkCancellation.Token,
                    System.Threading.Tasks.TaskCreationOptions.None,
                    System.Threading.Tasks.TaskScheduler.Default);

        }

        #endregion

        #endregion

        #region IPlanRenderSettings

        IPlanogramLabel IPlanRenderSettings.ProductLabel
        {
            get { return this.ProductLabel; }
            set { this.ProductLabel = value as LabelItem; }
        }

        IPlanogramLabel IPlanRenderSettings.FixtureLabel
        {
            get { return this.FixtureLabel; }
            set { this.FixtureLabel = value as LabelItem; }
        }

        Double IPlanRenderSettings.SelectionLineThickness
        {
            get
            {
                switch (this.Planogram.LengthUnitsOfMeasure)
                {
                    default:
                        return 0.5D;

                    case PlanogramLengthUnitOfMeasureType.Centimeters:
                        return 0.5D;

                    case PlanogramLengthUnitOfMeasureType.Inches:
                        return 0.25D;
                }
            }
        }

        ModelColour IPlanRenderSettings.SelectionColour
        {
            get
            {
                if (_isSelectionAlternateOn)
                {
                    return ModelColour.Black;
                }
                else
                {
                    return ModelColour.NewColour(255, 165, 0, 255);
                }
            }
        }

        public ModelMaterial3D.FillPatternType SelectionFillPattern
        {
            get { return ModelMaterial3D.FillPatternType.Solid; }
        }

        Boolean IPlanRenderSettings.CanRenderAsync
        {
            get { return true; }
        }

        Boolean IPlanRenderSettings.GenerateNormals
        {
            //for performance
            get { return false; }
        }

        Boolean IPlanRenderSettings.GenerateTextureCoordinates
        {
            get { return true; }
        }

        PlanogramPositionHighlightColours IPlanRenderSettings.PositionHighlights
        {
            get { return _positionHighlights; }
        }

        private PlanogramPositionHighlightColours PositionHighlights
        {
            get
            {
                return _positionHighlights;
            }
            set
            {
                if (_positionHighlights != value)
                {
                    _positionHighlights = value;
                    OnPropertyChanged(PlanRenderSettings.PositionHighlightsPropertyName);
                }
            }
        }

        PlanogramPositionLabelTexts IPlanRenderSettings.PositionLabelText
        {
            get { return _positionLabelText; }
        }

        private PlanogramPositionLabelTexts PositionLabelText
        {
            get
            {
                return _positionLabelText;
            }
            set
            {
                if (_positionLabelText != value)
                {
                    _positionLabelText = value;
                    OnPropertyChanged(PlanRenderSettings.PositionLabelTextPropertyName);
                }
            }
        }

        PlanogramFixtureLabelTexts IPlanRenderSettings.FixtureLabelText
        {
            get { return _fixtureLabelText; }
        }

        private PlanogramFixtureLabelTexts FixtureLabelText
        {
            get
            {
                return _fixtureLabelText;
            }
            set
            {
                if (_fixtureLabelText != value)
                {
                    _fixtureLabelText = value;
                    OnPropertyChanged(PlanRenderSettings.FixtureLabelTextPropertyName);
                }
            }
        }

        Boolean IPlanRenderSettings.ShowTopDownProductLabelsAsFront
        {
            get { return (this.ViewType != CameraViewType.Perspective); }
        }

        Boolean IPlanRenderSettings.ShowProductFillColours
        {
            get { return true; }
            set { }
        }

        CameraViewType IPlanRenderSettings.CameraType
        {
            get { return this.ViewType; }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                base.Dispose(disposing);

                SetPlanEventHandlers(false);
                Planogram.Model.Sequence.Groups.CollectionChanged -= OnPlanogramSequenceGroupsCollectionChanged;

                if (disposing)
                {
                    if (_planogramModelData != null)
                    {
                        _planogramModelData.Dispose();
                        _planogramModelData = null;
                    }

                }
                base.IsDisposed = true;
            }
        }

        #endregion

    }
}