﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-25042 : A.Silva ~ Created.
// V8-24700 : A.Silva ~ MenutItem_OnClick now stores the info object not the model itself.
// V8-26143 : A.Silva ~ Removed MenuItem_OnClick, as there is no longer a button raising the event on the UI.

#endregion

#endregion

using System.Windows;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanRepository
{
    /// <summary>
    ///     Interaction logic for PlanRepositoryHomeTab.xaml
    /// </summary>
    public sealed partial class PlanRepositoryHomeTab
    {
        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof (PlanRepositoryViewModel), typeof (PlanRepositoryHomeTab),
            new PropertyMetadata(default(PlanRepositoryViewModel)));

        public PlanRepositoryViewModel ViewModel
        {
            get { return (PlanRepositoryViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region AddToFavouriesCommandProperty

        public static readonly DependencyProperty AddToFavouriesCommandProperty =
            DependencyProperty.Register("AddToFavouritesCommand", typeof(RelayCommand), typeof(PlanRepositoryHomeTab),
            new PropertyMetadata(null));

        public RelayCommand AddToFavouritesCommand
        {
            get { return (RelayCommand)GetValue(AddToFavouriesCommandProperty); }
            set { SetValue(AddToFavouriesCommandProperty, value); }
        }

        #endregion

        #region RemoveFromFavouriesCommandProperty

        public static readonly DependencyProperty RemoveFromFavouriesCommandProperty =
            DependencyProperty.Register("RemoveFromFavouritesCommand", typeof(RelayCommand), typeof(PlanRepositoryHomeTab),
            new PropertyMetadata(null));

        public RelayCommand RemoveFromFavouritesCommand
        {
            get { return (RelayCommand)GetValue(RemoveFromFavouriesCommandProperty); }
            set { SetValue(RemoveFromFavouriesCommandProperty, value); }
        }

        #endregion

        #endregion


        #region Constructor

        public PlanRepositoryHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}