﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-25724 : A.Silva ~ Created.
// V8-26284 : A.Kuszyk ~ Amended to use Common.Wpf selector.
#endregion

#region Version History : CCM 802
// V8-27880 : A.Kuszyk
//  Changed binding of selected group to use view model.
#endregion

#region Version History : CCM830
// V8-32191 : A.Probyn
//  Updated to override OnKeyDown for copy and paste.
#endregion

#endregion

using System.ComponentModel;
using System.Windows;
using Galleria.Ccm.Editor.Client.Wpf.Common.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using System.Windows.Input;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanRepository
{
    /// <summary>
    ///     Interaction logic for PlanRepositorySidePanel.xaml
    /// </summary>
    public partial class PlanRepositorySidePanel
    {
        #region PlanogramHierarchyViewProperty

        public static readonly DependencyProperty PlanogramHierarchyViewProperty =
                    DependencyProperty.Register("PlanogramHierarchyView", typeof(PlanogramHierarchyViewModel),
                        typeof(PlanRepositorySidePanel), new PropertyMetadata(null, OnPlanogramHierarchyViewPropertyChanged));

        private static void OnPlanogramHierarchyViewPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = (PlanRepositorySidePanel)obj;

            if (senderControl.ViewModel == null) return;
            var newValue = e.NewValue as PlanogramHierarchyViewModel;

            //if (newValue != null)
            //{
            //    if (senderControl.ViewModel.PlanogramHierarchyView == newValue) return;
            //    senderControl.ViewModel.PlanogramHierarchyView = newValue;
            //}
            //else
            //{
            //    senderControl.ViewModel.PlanogramHierarchyView= null;
            //}
        }

        public PlanogramHierarchyViewModel PlanogramHierarchyView
        {
            get { return (PlanogramHierarchyViewModel)GetValue(PlanogramHierarchyViewProperty); }
            set { SetValue(PlanogramHierarchyViewProperty, value); }
        }

        #endregion

        #region SelectedGroupView Property

        /// <summary>
        ///     Currently selected <see cref="PlanogramGroupSelectorView"/>.
        /// </summary>
        public static readonly DependencyProperty SelectedGroupViewProperty =
            DependencyProperty.Register("SelectedGroupView", typeof (PlanogramGroupViewModel),
                typeof (PlanRepositorySidePanel), new PropertyMetadata(null, OnSelectedGroupViewPropertyChanged));

        private static void OnSelectedGroupViewPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = (PlanRepositorySidePanel) obj;

            if (senderControl.ViewModel == null) return;
            var newValue = e.NewValue as PlanogramGroupViewModel;

            if (newValue != null)
            {
                if (senderControl.ViewModel.SelectedPlanogramGroup.PlanogramGroup.Id == newValue.PlanogramGroup.Id &&
                    senderControl.ViewModel.SelectedPlanogramGroup.IsRecentWorkGroup == newValue.IsRecentWorkGroup) return;
                senderControl.ViewModel.SelectedPlanogramGroup = newValue;
            }
            else
            {
                senderControl.ViewModel.SelectedPlanogramGroup = null;
            }
        }

        /// <summary>
        ///     Gets or sets the currently selected <see cref="PlanogramGroupViewModel"/>.
        /// </summary>
        public PlanogramGroupViewModel SelectedGroupView
        {
            get { return (PlanogramGroupViewModel) GetValue(SelectedGroupViewProperty); }
            set { SetValue(SelectedGroupViewProperty, value); }
        }

        #endregion

        #region ViewModel Property

        /// <summary>
        ///     Viewmodel controller for <see cref="PlanRepositorySidePanel"/>.
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register(
            "ViewModel", typeof (PlanRepositoryViewModel), typeof (PlanRepositorySidePanel),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = (PlanRepositorySidePanel) obj;

            if (e.OldValue != null)
            {
                var oldValue = (PlanRepositoryViewModel) e.OldValue;
                oldValue.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue == null) return;
            var newValue = (PlanRepositoryViewModel) e.NewValue;
            newValue.PropertyChanged += senderControl.ViewModel_PropertyChanged;
        }

        /// <summary>
        ///     Gets and sets the viewmodel controller for the <see cref="PlanRepositorySidePanel"/>.
        /// </summary>
        public PlanRepositoryViewModel ViewModel
        {
            get { return (PlanRepositoryViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Event Handlers

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            //If Ctrl+C selected, copy planogram
            if (e.Key == Key.C && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                if (this.ViewModel.CopyPlanogramCommand.CanExecute())
                {
                    this.ViewModel.CopyPlanogramCommand.Execute();
                }
            }
            else if (e.Key == Key.X && Keyboard.Modifiers == ModifierKeys.Control)
            {
                if (this.ViewModel.CutPlanogramCommand.CanExecute())
                {
                    this.ViewModel.CutPlanogramCommand.Execute();
                }
            }
            else if (e.Key == Key.V && Keyboard.Modifiers == ModifierKeys.Control)
            {
                if (this.ViewModel.PastePlanogramCommand.CanExecute())
                {
                    this.ViewModel.PastePlanogramCommand.Execute();
                }
            }
        }

        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != PlanRepositoryViewModel.SelectedPlanogramGroupProperty.Path) return;

            // V8-26284: Commented out, because this was interferrering with the Common
            // control's item selection.
            //if (ViewModel.PlanogramGroupSelection != null)
            //{
            //    xSelector.SetSelectedGroup(ViewModel.PlanogramGroupSelection);
            //}
            //else
            //{
            //    xSelector.SelectedGroup = null;
            //}
        }

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of the <see cref="PlanRepositorySidePanel"/> class.
        /// </summary>
        public PlanRepositorySidePanel()
        {
            InitializeComponent();
        }

        #endregion
    }
}