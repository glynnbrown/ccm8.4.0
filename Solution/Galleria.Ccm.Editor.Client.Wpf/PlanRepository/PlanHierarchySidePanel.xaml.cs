﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25724 : A.Silva ~ Created.
#endregion

#endregion

namespace Galleria.Ccm.Editor.Client.Wpf.PlanRepository
{
    /// <summary>
    /// Interaction logic for PlanHierarchySidePanel.xaml
    /// </summary>
    public partial class PlanHierarchySidePanel
    {
        public PlanHierarchySidePanel()
        {
            InitializeComponent();
        }
    }
}
