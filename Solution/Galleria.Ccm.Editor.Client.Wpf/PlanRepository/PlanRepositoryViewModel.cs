﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-25042 : A.Silva ~ Created.
// V8-25645 : A.Silva ~ ConnectToRepository command now opens the DbConnectionWizard window.
// V8-25724 : A.Silva ~ Refactored code to account for the code separation of the side Planogram Group Hierarchy panel.
// V8-25722 : A.Silva ~ Removed Group Filtering as it is now done within the Planogram Hierarchy Selector.
// V8-25718 : A.Silva ~ Implemented PlanogramDetails.
// V8-24700 : A.Silva ~ Modified to use Custom Column Layouts.
// V8-25908 : A.Silva ~ Removed PlanogramDetailsView, 
//                    ~ Added code to display the thumbnails.
// V8-25937 : A.Silva ~ Corrected an issue where the temporary column collection was being cleared, and never reconstructed.
// V8-25664 : A.Kuszyk ~ Added My Categories commands.
// V8-25907 : A.Silva ~ Added some localisation.
// V8-26143 : A.Silva ~ Changed to support ICustomColumLayoutView, and access to custom column layout editor from the context menu.
// V8-26171 : A.Silva ~ Added CustomColumnManager and removed ICustomColumnLayoutView.
// V8-26329 : A.Silva ~ Added _columnLayoutManager_CurrentColumnLayoutEdited eventhandler.
// V8-26370 : A.Silva ~ Corrected disposing of _columnLayoutManager.
// V8-26284 : A.Kuszyk ~ Amended to use Common.Wpf selector.
// V8-25986 : L.Inson 
//  Improvements to searching.
// V8-26944 : A.Silva ~ Added CustomColumnCreatorDelegate.
// V8-26472 : A.Probyn ~ Adjusted CustomColumnCreator temporarily based on custom column layout changes
//                     ~ Updated reference to ColumnLayoutManager Constructor
// V8-26944 : A.Silva ~ Added PlanogramRowView type to wrap the Planogram Info and the Validation Info.
//                    ~ Changed old references to PlanogramInfo with PlanogramRepositoryRow.
//                    ~ Added the Data Template Creator for custom template columns.
// V8-27045 : A.Silva ~ Added PlanogramValidation data when populating the PlanogramRepositoryRow.
// V8-26941 : A.Silva ~ Minor correction.
// V8-27058 : A.Probyn ~ Changed over to use PlanogramMetadataImageViewModel for thumbnail
// V8-27393 : L.Ineson
//  Refactored to stop memory leaks.
// V8-27966 : A.Silva
//      Added polling for the Planogram Hierarchy.
//      Added context menu options to add, edit and delete planogram groups.
//      Added context menu options to cut, copy and paste Planograms and Planogram Groups and the code to do it.
// V8-28003 : A.Silva
//      Added Delete Planogram Command.
//      Amended SelectorViewModel Property to have Binding Property Path.
// V8-28240 : A.Silva
//      Amended UpdateAvailablePlanogramInfos so that the root group only displays its own plans.
// V8-28271 : A.Silva
//      Amended AvailablePlanograms_ModelChanged so that any previous planogram selection is kept if possible.
// V8-28286 : A.Silva
//      Amended UpdateAvailablePlanogramInfos so that searching fetches from all planogram groups if a string is searched and the root planogram group is selected,
//          or if the root recent work planogram group is selected.
// V8-28334 : A.Silva
//      Paused polling while a new, delete or edit operation is in process.
// V8-28374 : A.Silva
//      Corrected RestoreSelection so that any previous selections are always deleted.
// CCM-27599 : J.Pickup
//  Added Entity to FetchBySearchCriteriaAsync
// CCM-28330 : L.Ineson
//  Rows now update when polling instead of changing outright.
// V8-28533 : A.Kuszyk
//  Added lock check to Delete Planogram Command CanExecute.
// V8-28432 : A.Kuszyk
//  Changed PastePlanogram_CanExecute to check for SelectedPlanogramGroup directly.

#endregion

#region Version History: (CCM 8.01)

//V8-28251 : L.Ineson
//  Amended Planogram and Planogram Group delete commands.

#endregion

#region Version History : CCM 802

// V8-27880 : A.Kuszyk
//  Changed selected group to use view model and added re-selecting from session selection to OnRepositoryConnectionChanged().
// V8-28927 : D.Pleasance
//  Amended EditGroup command so that showMerchGroupSelector is dependant on user having DomainPermission.SyncWithMerchandisingHierarchy role

#endregion

#region Version History: (CCM 8.1.0)

// V8-28242 : M.Shelley
//  Added an editor window for the planogram properties
// V8-29860 : M.Shelley
//  Added the PlanogramType property
// V8-30059 : L.Ineson
//  Amended edit properties to make setting individual values optional.
// V8-30074 : L.Ineson
//  AvailablePlanograms_ModelChanged no longer falls over 
// if multiple infos for the same plan are returned. 

#endregion

#region Version History: (CCM 8.1.1)

// V8-30058 : M.Shelley
//  Added a tooltip for the context menu "Properties" and "Delete" items
// V8-30491
//  Alter check for "selectedGroup.Id == 1" in the UpdateAvailablePlanogramInfos method to check for a null parent group
// V8-30583 : A.Silva
//  Amended RestoreSelection to not select the first row if there is no previous selection.

#endregion

#region Version History : CCM820
// V8-30527 : A.Kuszyk
//  Added permissions check to DeleteGroup_CanExecute().
#endregion
#region Version History : CCM830
//V8-32525 : L.Ineson
//  Added delete permission check to the cut command
// V8-32810 : L.Ineson
//  Added Export to File
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Editor.Client.Wpf.Startup;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.Model;
using Csla;
using Galleria.Ccm.Common.Wpf.Misc;
using CommonLanguage = Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Framework.Planograms.Helpers;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanRepository
{
    //TODO: Remove the SelectorViewModel reference. 
    //It looks like the planogram group commands here that reference it should actually be placed against the SelectorViewModel directly and not here.


    /// <summary>
    ///     Viewmodel controller for the PlanRepositoryOrganiser.
    /// </summary>
    public sealed class PlanRepositoryViewModel : ViewModelAttachedControlObject<PlanRepositoryOrganiser>
    {
        #region Constants

        internal const String ScreenKey = "PlanRepository";
        internal const CustomColumnLayoutType ColumnLayoutType = CustomColumnLayoutType.Planogram;

        #endregion

        #region Fields

        private String _currentEntityName;

        /// <summary>
        ///     Whether the user has permission to delete planograms or not.
        /// </summary>
        private Boolean _userHasPlanogramDeletePermission;
        private Predicate<String> _isUniqueGroupPredicate;

        private readonly PlanogramHierarchyViewModel _masterPlanogramHierarchyView = new PlanogramHierarchyViewModel();
        private PlanogramGroupViewModel _selectedPlanogramGroup;

        private readonly UserPlanogramGroupListViewModel _userFavouritePlanGroupsView = new UserPlanogramGroupListViewModel();

        private String _planogramSearchCriteria;

        private readonly BulkObservableCollection<PlanogramRepositoryRow> _availablePlanogramRows = new BulkObservableCollection<PlanogramRepositoryRow>();
        private ReadOnlyBulkObservableCollection<PlanogramRepositoryRow> _availablePlanogramRowsRO;
        private readonly ObservableCollection<PlanogramRepositoryRow> _selectedPlanogramRows = new ObservableCollection<PlanogramRepositoryRow>();

        private readonly PlanogramInfoListViewModel _planogramInfoListViewModel = new PlanogramInfoListViewModel();
        private PlanogramInfo _activePlanogramInfo;

        private Object _copyPasteSource; // Stores the current source of a copy/paste operation. Null if no copy/paste operation is ongoing.
        private Boolean _isMove;//Whether the operation is cut or just copy.

        private PlanogramHierarchySelectorViewModel _selectorViewModel;
        private static Predicate<String> _isUniqueCheck;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath CurrentEntityNameProperty = WpfHelper.GetPropertyPath<PlanRepositoryViewModel>(p => p.CurrentEntityName);
        public static readonly PropertyPath PlanogramHierarchyViewProperty = WpfHelper.GetPropertyPath<PlanRepositoryViewModel>(p => p.PlanogramHierarchyView);
        public static readonly PropertyPath SelectedPlanogramGroupProperty = WpfHelper.GetPropertyPath<PlanRepositoryViewModel>(p => p.SelectedPlanogramGroup);
        public static readonly PropertyPath UserFavouriteGroupsViewProperty = WpfHelper.GetPropertyPath<PlanRepositoryViewModel>(p => p.UserFavouriteGroupsView);
        public static readonly PropertyPath PlanogramSearchCriteriaProperty = WpfHelper.GetPropertyPath<PlanRepositoryViewModel>(p => p.PlanogramSearchCriteria);
        public static readonly PropertyPath ActivePlanogramInfoProperty = WpfHelper.GetPropertyPath<PlanRepositoryViewModel>(p => p.ActivePlanogramInfo);
        public static readonly PropertyPath PlanogramRowsProperty = WpfHelper.GetPropertyPath<PlanRepositoryViewModel>(p => p.PlanogramRows);
        public static readonly PropertyPath SelectedPlanogramRowsProperty = WpfHelper.GetPropertyPath<PlanRepositoryViewModel>(p => p.SelectedPlanogramRows);
        public static readonly PropertyPath SelectorViewModelProperty = WpfHelper.GetPropertyPath<PlanRepositoryViewModel>(p => p.SelectorViewModel);
        public static readonly PropertyPath CanMovePlanogramProperty = WpfHelper.GetPropertyPath<PlanRepositoryViewModel>(p => p.CanMovePlanogram);
        //Commands
        public static readonly PropertyPath ConnectToRepositoryCommandProperty = WpfHelper.GetPropertyPath<PlanRepositoryViewModel>(p => p.ConnectToRepositoryCommand);
        public static readonly PropertyPath OpenPlanogramCommandProperty = WpfHelper.GetPropertyPath<PlanRepositoryViewModel>(p => p.OpenPlanogramCommand);
        public static readonly PropertyPath DeletePlanogramCommandProperty = WpfHelper.GetPropertyPath<PlanRepositoryViewModel>(p => p.DeletePlanogramCommand);
        public static readonly PropertyPath CutPlanogramCommandProperty = WpfHelper.GetPropertyPath<PlanRepositoryViewModel>(p => p.CutPlanogramCommand);
        public static readonly PropertyPath CopyPlanogramCommandProperty = WpfHelper.GetPropertyPath<PlanRepositoryViewModel>(p => p.CopyPlanogramCommand);
        public static readonly PropertyPath PastePlanogramCommandProperty = WpfHelper.GetPropertyPath<PlanRepositoryViewModel>(p => p.PastePlanogramCommand);
        public static readonly PropertyPath NewGroupCommandProperty = WpfHelper.GetPropertyPath<PlanRepositoryViewModel>(p => p.NewGroupCommand);
        public static readonly PropertyPath DeleteGroupCommandProperty = WpfHelper.GetPropertyPath<PlanRepositoryViewModel>(p => p.DeleteGroupCommand);
        public static readonly PropertyPath EditGroupCommandProperty = WpfHelper.GetPropertyPath<PlanRepositoryViewModel>(p => p.EditGroupCommand);
        public static readonly PropertyPath CutPlanogramGroupCommandProperty = WpfHelper.GetPropertyPath<PlanRepositoryViewModel>(p => p.CutPlanogramGroupCommand);
        public static readonly PropertyPath CopyPlanogramGroupCommandProperty = WpfHelper.GetPropertyPath<PlanRepositoryViewModel>(p => p.CopyPlanogramGroupCommand);
        public static readonly PropertyPath PastePlanogramGroupCommandProperty = WpfHelper.GetPropertyPath<PlanRepositoryViewModel>(p => p.PastePlanogramGroupCommand);
        public static readonly PropertyPath ExportToFileCommandProperty = WpfHelper.GetPropertyPath<PlanRepositoryViewModel>(p => p.ExportToFileCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the name of the currently connected entity.
        /// </summary>
        public String CurrentEntityName
        {
            get { return _currentEntityName; }
            private set
            {
                _currentEntityName = value;
                OnPropertyChanged(CurrentEntityNameProperty);
            }
        }

        /// <summary>
        /// Returns the master planogram hierarchy view.
        /// </summary>
        public PlanogramHierarchyViewModel PlanogramHierarchyView
        {
            get { return _masterPlanogramHierarchyView; }
        }

        /// <summary>
        /// Gets/Sets the selected planogram group.
        /// </summary>
        public PlanogramGroupViewModel SelectedPlanogramGroup
        {
            get { return _selectedPlanogramGroup; }
            set
            {
                _selectedPlanogramGroup = value;
                OnPropertyChanged(SelectedPlanogramGroupProperty);
                OnSelectedPlanogramGroupChanged();
            }
        }

        /// <summary>
        /// Returns the view of the current user favourite groups.
        /// </summary>
        public UserPlanogramGroupListViewModel UserFavouriteGroupsView
        {
            get { return _userFavouritePlanGroupsView; }
        }

        /// <summary>
        /// Gets/Sets the search criteria to use when fetching planograms.
        /// </summary>
        public String PlanogramSearchCriteria
        {
            get { return _planogramSearchCriteria; }
            set
            {
                _planogramSearchCriteria = value;
                OnPropertyChanged(PlanogramSearchCriteriaProperty);

                OnPlanogramSearchCriteriaChanged();
            }
        }

        /// <summary>
        ///     Gets the read only collection of <see cref="PlanogramRepositoryRow"/> items that should be displayed.
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramRepositoryRow> PlanogramRows
        {
            get
            {
                return _availablePlanogramRowsRO ??
                       (_availablePlanogramRowsRO =
                       new ReadOnlyBulkObservableCollection<PlanogramRepositoryRow>(_availablePlanogramRows));
            }
        }

        /// <summary>
        ///  Returns the editable collection of currently selected <see cref="PlanogramRepositoryRow"/>.
        /// </summary>
        public ObservableCollection<PlanogramRepositoryRow> SelectedPlanogramRows
        {
            get { return _selectedPlanogramRows; }
        }

        /// <summary>
        /// Returns the active planogram info
        /// for which property information should be displayed.
        /// </summary>
        public PlanogramInfo ActivePlanogramInfo
        {
            get { return _activePlanogramInfo; }
            private set
            {
                if (_activePlanogramInfo != value)
                {
                    _activePlanogramInfo = value;
                    OnPropertyChanged(ActivePlanogramInfoProperty);
                }
            }
        }

        //TODO: REMOVE THIS. 
        public PlanogramHierarchySelectorViewModel SelectorViewModel
        {
            private get { return _selectorViewModel; }
            set
            {
                _selectorViewModel = value;
                OnPropertyChanged(SelectorViewModelProperty);
            }
        }

        /// <summary>
        /// Determines if the user is allowed to move Planograms
        /// </summary>
        public Boolean CanMovePlanogram
        {
            get
            {
                return ApplicationContext.User.IsInRole(DomainPermission.PlanogramDelete.ToString());
            }
        }
        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of the <see cref="PlanRepositoryViewModel" /> class.
        /// </summary>
        public PlanRepositoryViewModel()
        {
            UpdatePermissionFlags();
            App.ViewState.EntityIdChanged += ViewState_EntityIdChanged;

            _planogramInfoListViewModel.ModelChanged += AvailablePlanograms_ModelChanged;

            _selectedPlanogramRows.CollectionChanged += SelectedPlanogramValidationRows_CollectionChanged;

            OnRepositoryConnectionChanged();
        }

        #endregion

        #region Commands

        #region ConnectToRepository

        private RelayCommand _connectToRepositoryCommand;

        /// <summary>
        ///     Get (and lazy set) the <see cref="_connectToRepositoryCommand" />.
        /// </summary>
        public RelayCommand ConnectToRepositoryCommand
        {
            get
            {
                if (_connectToRepositoryCommand == null)
                {
                    _connectToRepositoryCommand = new RelayCommand(
                        o => ConnectToRepository_Executed())
                    {
                        FriendlyName = Message.PlanRepository_Ribbon_ConnectRepository,
                        FriendlyDescription = Message.PlanRepository_Ribbon_ConnectRepository_Desc,
                        Icon = ImageResources.PlanRepository_Ribbon_ConnectRepository_32
                    };
                    base.ViewModelCommands.Add(_connectToRepositoryCommand);
                }

                return _connectToRepositoryCommand;
            }
        }

        /// <summary>
        ///     Show sthe <see cref="DbConnectionWizardWindow" />, then fetches the <see cref="PlanogramHierarchy" /> model for the
        ///     current entity.
        /// </summary>
        private void ConnectToRepository_Executed()
        {
            App.ShowWindow(new DbConnectionWizardWindow(), true);
        }

        #endregion

        #region Planogram Commands

        #region OpenPlanogram

        private RelayCommand _openPlanogramCommand;

        /// <summary>
        /// Opens the selected planograms
        /// </summary>
        public RelayCommand OpenPlanogramCommand
        {
            get
            {
                if (_openPlanogramCommand == null)
                {
                    _openPlanogramCommand = new RelayCommand(
                        o => OpenPlanogram_Executed(), o => OpenPlanogram_CanExecute())
                    {
                        FriendlyName = Message.Generic_Open,
                        FriendlyDescription = Message.PlanRepository_OpenPlanogram_Desc,
                        Icon = ImageResources.PlanRepository_Ribbon_OpenPlanogram_32,
                        DisabledReason = Message.PlanRepository_OpenPlanogram_DisabledReason_NoSelection
                    };
                    base.ViewModelCommands.Add(_openPlanogramCommand);
                }
                return _openPlanogramCommand;
            }
        }

        private Boolean OpenPlanogram_CanExecute()
        {
            if (SelectedPlanogramRows.Count == 0)
            {
                OpenPlanogramCommand.DisabledReason = Message.PlanogramRepository_OpenPlanogram_DisabledNoSelection;
            }
            else
            {
                return true;
            }

            return false;
        }

        private void OpenPlanogram_Executed()
        {
            var selection = SelectedPlanogramRows.Select(row => row.Info).ToList();

            if (AttachedControl != null) AttachedControl.Close();

            App.MainPageViewModel.OpenPlanogramsFromRepository(selection);
        }

        #endregion

        #region DeletePlanogram

        private RelayCommand _deletePlanogramCommand;

        /// <summary>
        /// Deletes the currently selected planograms.
        /// </summary>
        public RelayCommand DeletePlanogramCommand
        {
            get
            {
                if (_deletePlanogramCommand == null)
                {
                    _deletePlanogramCommand = new RelayCommand(
                        p => DeletePlanogram_Executed(),
                        p => DeletePlanogram_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        FriendlyDescription = Message.PlanRepository_PlanogramDelete_Description,
                        Icon = ImageResources.Delete_32,
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_deletePlanogramCommand);
                }

                return _deletePlanogramCommand;
            }
        }

        private Boolean DeletePlanogram_CanExecute()
        {
            DeletePlanogramCommand.DisabledReason = String.Empty;

            //  Must have delete permission.
            if (!_userHasPlanogramDeletePermission)
            {
                DeletePlanogramCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            // Must have selected planograms.
            if (SelectedPlanogramRows.Count == 0)
            {
                DeletePlanogramCommand.DisabledReason = Message.PlanRepository_PlanogramDelete_DisabledReason_NoSelection;
                return false;
            }

            // Selected plan must not be locked.
            if (SelectedPlanogramRows.Any(r => r.Info.IsLocked))
            {
                DeletePlanogramCommand.DisabledReason = Message.PlanRepository_PlanogramDelete_DisabledReason_LockedPlans;
                return false;
            }

            return true;
        }

        private void DeletePlanogram_Executed()
        {
            StopPolling();

            //delete
            PlanogramRepositoryUIHelper.DeletePlanograms(this.SelectedPlanogramRows.Select(p => p.Info).ToList(), OnPlanogramDeleted);

            StartPolling();
        }

        private void OnPlanogramDeleted(Object sender, EventArgs<PlanogramInfo> args)
        {
            //remove the last deleted planogram from the main list.
            if (args.ReturnValue != null)
            {
                _availablePlanogramRows.Remove(_availablePlanogramRows.FirstOrDefault(p => Object.Equals(p.Info.Id, args.ReturnValue.Id)));
            }
        }


        #endregion

        #region CutPlanogram

        private RelayCommand _cutPlanogramCommand;

        /// <summary>
        /// Cuts the currently selected planograms.
        /// </summary>
        public RelayCommand CutPlanogramCommand
        {
            get
            {
                if (_cutPlanogramCommand != null) return _cutPlanogramCommand;

                _cutPlanogramCommand = new RelayCommand(o => CutPlanogram_Executed(), o => CutPlanogram_CanExecute())
                {
                    FriendlyName = Message.Generic_Cut,
                    FriendlyDescription = Message.PlanRepository_PlanogramCut_Description,
                    Icon = ImageResources.Cut_16,
                    SmallIcon = ImageResources.Cut_16,
                    DisabledReason = Message.PlanRepository_DisabledReason_NoPlanogramSelected
                };
                ViewModelCommands.Add(_cutPlanogramCommand);
                return _cutPlanogramCommand;
            }
        }


        private Boolean CutPlanogram_CanExecute()
        {
            //  Must have delete permission.
            if (!_userHasPlanogramDeletePermission)
            {
                DeletePlanogramCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }


            if (SelectedPlanogramRows.Count == 0)
            {
                CutPlanogramCommand.DisabledReason = Message.PlanRepository_DisabledReason_NoPlanogramSelected;
                return false;
            }

            return true;
        }

        private void CutPlanogram_Executed()
        {
            _isMove = true;
            _copyPasteSource = SelectedPlanogramRows.Select(o => o.Info).ToList();
        }

        #endregion

        #region CopyPlanogram

        private RelayCommand _copyPlanogramCommand;

        /// <summary>
        /// Copies the currently selected planograms to the clipboard.
        /// </summary>
        public RelayCommand CopyPlanogramCommand
        {
            get
            {
                if (_copyPlanogramCommand != null) return _copyPlanogramCommand;

                _copyPlanogramCommand = new RelayCommand(o => CopyPlanogram_Executed(), o => CopyPlanogram_CanExecute())
                {
                    FriendlyName = Message.Generic_Copy,
                    FriendlyDescription = Message.PlanRepository_PlanogramCopy_Description,
                    Icon = ImageResources.Copy_16,
                    SmallIcon = ImageResources.Copy_16,
                    DisabledReason = Message.PlanRepository_DisabledReason_NoPlanogramSelected
                };
                ViewModelCommands.Add(_copyPlanogramCommand);
                return _copyPlanogramCommand;
            }
        }


        private Boolean CopyPlanogram_CanExecute()
        {
            if (SelectedPlanogramRows.Count == 0)
            {
                CopyPlanogramCommand.DisabledReason = Message.PlanRepository_DisabledReason_NoPlanogramSelected;
                return false;
            }

            return true;
        }

        private void CopyPlanogram_Executed()
        {
            _isMove = false;
            _copyPasteSource = SelectedPlanogramRows.Select(o => o.Info).ToList();
        }

        #endregion

        #region PastePlanograms

        private RelayCommand _pastePlanogramCommand;

        /// <summary>
        /// Pastes planograms from the clipboard
        /// to the selected planogram group.
        /// </summary>
        public RelayCommand PastePlanogramCommand
        {
            get
            {
                if (_pastePlanogramCommand != null) return _pastePlanogramCommand;

                _pastePlanogramCommand = new RelayCommand(o => PastePlanogram_Executed(), o => PastePlanogram_CanExecute())
                {
                    FriendlyName = Message.Generic_Paste,
                    FriendlyDescription = Message.PlanRepository_PlanogramPaste_Description,
                    Icon = ImageResources.Paste_16,
                    SmallIcon = ImageResources.Paste_16,
                    DisabledReason = Message.PlanRepository_Paste_DisabledReason_NoSource
                };
                ViewModelCommands.Add(_pastePlanogramCommand);
                return _pastePlanogramCommand;
            }
        }

        private Boolean PastePlanogram_CanExecute()
        {
            if (this.SelectedPlanogramGroup == null)
            {
                CopyPlanogramCommand.DisabledReason = Message.PlanRepository_DisabledReason_NoPlanogramGroupSelected;
                return false;
            }

            if (!(_copyPasteSource is ICollection<PlanogramInfo>))
            {
                CopyPlanogramCommand.DisabledReason = Message.PlanRepository_Paste_DisabledReason_NoSource;
                return false;
            }

            return true;
        }

        private void PastePlanogram_Executed()
        {
            StopPolling();
            if (_isMove)
            {
                PlanogramRepositoryUIHelper.MovePlanogramsToGroup((ICollection<PlanogramInfo>)_copyPasteSource, this.SelectedPlanogramGroup.PlanogramGroup);
                _copyPasteSource = null;
            }
            else
            {
                PlanogramRepositoryUIHelper.CopyPlanogramsToGroup((ICollection<PlanogramInfo>)_copyPasteSource, this.SelectedPlanogramGroup.PlanogramGroup);
            }
            StartPolling();
        }

        #endregion

        #region PlanogramPropertiesCommand

        private RelayCommand _planogramPropertiesCommand;

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand PlanogramPropertiesCommand
        {
            get
            {
                if (_planogramPropertiesCommand == null)
                {
                    _planogramPropertiesCommand = new RelayCommand(
                        p => PlanogramPropertiesCommand_Executed(),
                        p => PlanogramPropertiesCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Properties,
                        Icon = ImageResources.Properties_14,
                    };
                    base.ViewModelCommands.Add(_planogramPropertiesCommand);
                }
                return _planogramPropertiesCommand;
            }
        }

        private bool PlanogramPropertiesCommand_CanExecute()
        {
            bool enableCommand = true;
            String disabledReason = String.Empty;

            if (!this.SelectedPlanogramRows.Any())
            {
                disabledReason = CommonLanguage.Message.PlanRepository_EditPlanogramProperties_NoneSelected;
                enableCommand = false;
            }
            else if (this.SelectedPlanogramRows.Where(x => x.Info.IsLocked).Any())
            {
                disabledReason = CommonLanguage.Message.PlanRepository_EditPlanogramProperties_PlansLockedWarning;
                enableCommand = false;
            }

            _planogramPropertiesCommand.DisabledReason = disabledReason;

            return enableCommand;
        }

        private void PlanogramPropertiesCommand_Executed()
        {
            var planPropEditorVm = new PlanPropertiesEditorViewModel(App.ViewState.EntityId, this.SelectedPlanogramRows);

            CommonHelper.GetWindowService().ShowDialog<PlanPropertiesEditorWindow>(planPropEditorVm);

            if (planPropEditorVm.DialogResult == true)
            {
                //perform the update:
                ShowWaitCursor(true);

                var updateResult =
                     Package.TryUpdatePlanogramAttributes(
                     planPropEditorVm.CreatePlanogramAttributes(
                     this.SelectedPlanogramRows.Select(p => (Object)p.Info.Id)));

                ShowWaitCursor(false);

                // Refresh the data grid
                _masterPlanogramHierarchyView.FetchForCurrentEntity();

            }
        }

        #endregion

        #region Unlock Selected Planograms

        private RelayCommand _unlockSelectedPlanogramCommand;

        /// <summary>
        ///    Attempts to unlock the selected planogram(s) if they are locked out by a user but not SystemProcess.
        /// </summary>
        public RelayCommand UnlockSelectedPlanogramCommand
        {
            get
            {
                if (_unlockSelectedPlanogramCommand == null)
                {
                    _unlockSelectedPlanogramCommand = new RelayCommand(
                        o => UnlockSelectedPlanogramsCommand_Executed(),
                        o => UnlockSelectedPlanogramsCommand_CanExecute())
                    {
                        FriendlyName = Message.PlanRepository_UnlockSelectedPlanogramCommand,
                        FriendlyDescription = Message.PlanRepository_UnlockSelectedPlanogramCommand_Description,
                        Icon = ImageResources.PlanogramRepository_UnlockPlanogram
                    };
                    ViewModelCommands.Add(_unlockSelectedPlanogramCommand);
                }
                return _unlockSelectedPlanogramCommand;
            }
        }

        private Boolean UnlockSelectedPlanogramsCommand_CanExecute()
        {
            //Must have permission
            if (!ApplicationContext.User.IsInRole(DomainPermission.UnlockPlanogram.ToString()))
            {
                UnlockSelectedPlanogramCommand.DisabledReason = Message.PlanRepository_UnlockSelectedPlanogramCommand_DisabledReason;
                return false;
            }

            //At least one of the selected planograms must be locked.
            if (!SelectedPlanogramRows.Select(row => row.Info).Where(c => c.IsLocked).ToList().Any())
            {
                UnlockSelectedPlanogramCommand.DisabledReason = Message.PlanRepository_UnlockSelectedPlanogramCommand_DisabledReasonNotLocked;
                return false;
            }

            return true;
        }

        private void UnlockSelectedPlanogramsCommand_Executed()
        {
            //Only continue if we have some selected plans.
            if (SelectedPlanogramRows == null) return;

            List<PlanogramInfo> selection = SelectedPlanogramRows.Select(row => row.Info).Where(c => c.IsLocked).ToList();

            //If selection contains at least one locked plano then continue.
            if (!selection.Any()) return;

            //Confirm with user.
            ModalMessageResult continueResult =
                CommonHelper.GetWindowService().ShowMessage(
                Ccm.Common.Wpf.Services.MessageWindowType.Warning,
                Message.PlanRepository_ContinueWithPlanogramUnlock_Header,
                (selection.Count() != 1) ?
                    Message.PlanRepository_ContinueWithPlanogramUnlock_DescriptionPlural :
                    Message.PlanRepository_ContinueWithPlanogramUnlock_DescriptionSingular,
                 Message.Generic_Yes,
                 Message.Generic_No);
            if (continueResult != ModalMessageResult.Button1) return;


            //Run an unlock on every selected item and keep count of total number of unlocked plans.
            Int32 noPlansSuccessfullyUnlocked =
                this.SelectedPlanogramRows.Count(currentInfoToUnlock =>
                    Package.TryUnlockPackageById(currentInfoToUnlock.Info.Id, DomainPrincipal.CurrentUserId, PackageLockType.User));

            //Dialog to notify the user of outcome
            String notifyDesc = String.Empty;
            if (noPlansSuccessfullyUnlocked == selection.Count())
            {
                //Success all
                notifyDesc = Message.UnlockSelectedPlanogramsCommand_SuccessUnlockAll;
            }
            else if (noPlansSuccessfullyUnlocked == 0)
            {
                //Failed All
                notifyDesc = Message.UnlockSelectedPlanogramsCommand_FailedUnlockAll;
            }
            else if (noPlansSuccessfullyUnlocked > 0 && noPlansSuccessfullyUnlocked < selection.Count())
            {
                //We failed to unlock x amount.
                Int32 failedCount = selection.Count() - noPlansSuccessfullyUnlocked;
                notifyDesc = String.Format(Message.UnlockSelectedPlanogramsCommand_FailedUnlockXAmount, failedCount);
            }

            //Notify user of the outcome
            CommonHelper.GetWindowService().ShowMessage(
                Ccm.Common.Wpf.Services.MessageWindowType.Info,
                 Message.PlanRepository_PlanogramUnlock_Header,
                notifyDesc,
                1, Message.Generic_OK, null, null);


            //Request an imediate refresh on the locking?
            UpdateAvailablePlanogramInfos();
        }

        #endregion

        #region RenamePlanogram

        private RelayCommand _renamePlanogramCommand;

        public RelayCommand RenamePlanogramCommand
        {
            get
            {
                if (_renamePlanogramCommand == null)
                {
                    _renamePlanogramCommand = new RelayCommand(
                        p => RenamePlanogram_Executed(),
                        p => RenamePlanogram_CanExecute())
                    {
                        FriendlyName = CommonLanguage.Message.RenamePlanogram_Title,
                        SmallIcon = Galleria.Ccm.Common.Wpf.Resources.ImageResources.PlanRepositoryRenamePlanogram_16,
                    };
                }
                return _renamePlanogramCommand;
            }
        }

        private Boolean RenamePlanogram_CanExecute()
        {
            // Must have some rows selected
            if (!this.SelectedPlanogramRows.Any())
            {
                this.RenamePlanogramCommand.DisabledReason = CommonLanguage.Message.PlanRepository_EditPlanogramProperties_NoneSelected;
                return false;
            }

            // All the selected plans must be unlocked.
            if (this.SelectedPlanogramRows.Where(x => x.Info.IsLocked).Any())
            {
                this.RenamePlanogramCommand.DisabledReason = CommonLanguage.Message.PlanRepository_EditPlanogramProperties_PlansLockedWarning;
                return false;
            }

            return true;
        }

        private void RenamePlanogram_Executed()
        {
            StopPolling();
            PlanogramRepositoryUIHelper.RenamePlanogram(this.SelectedPlanogramRows.First().Info);
            StartPolling();
        }

        #endregion

        #endregion

        #region Group Commands

        #region NewGroupCommand

        private RelayCommand _newGroupCommand;

        /// <summary>
        /// Adds a new PlanogramGroup to the Hierarchy.
        /// </summary>
        public RelayCommand NewGroupCommand
        {
            get
            {
                if (_newGroupCommand == null)
                {
                    _newGroupCommand = new RelayCommand(
                        p => NewGroup_Executed(),
                        p => NewGroup_CanExecute())
                    {
                        FriendlyName = Message.Generic_New,
                        FriendlyDescription = Message.PlanRepository_NewPlanogramGroup_Description,
                        Icon = ImageResources.PlanogramRepository_NewGroup_32,
                        SmallIcon = ImageResources.PlanogramRepository_NewGroup_16
                    };
                    base.ViewModelCommands.Add(_newGroupCommand);
                }
                return _newGroupCommand;
            }
        }

        public Boolean NewGroup_CanExecute()
        {
            return this.SelectedPlanogramGroup != null;
        }

        public void NewGroup_Executed()
        {
            StopPolling();

            Int32? groupIdToSelect = PlanogramRepositoryUIHelper.CreateNewPlanogramGroup(this.SelectedPlanogramGroup.PlanogramGroup);

            StartPolling();

            //try to select the new group
            if (groupIdToSelect.HasValue)
            {
                PlanogramGroupViewModel newGroup = _masterPlanogramHierarchyView.EnumerateAllGroupViews().FirstOrDefault(i => i.Id == groupIdToSelect.Value);
                if (newGroup != null) this.SelectedPlanogramGroup = newGroup;
            }
        }

        #endregion

        #region DeleteGroupCommand

        private RelayCommand _deleteGroupCommand;

        /// <summary>
        /// Deletes a new PlanogramGroup from the Hierarchy.
        /// </summary>
        public RelayCommand DeleteGroupCommand
        {
            get
            {
                if (_deleteGroupCommand == null)
                {
                    _deleteGroupCommand = new RelayCommand(
                        p => DeleteGroup_Executed(),
                        p => DeleteGroup_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        FriendlyDescription = Message.PlanRepository_PlanogramGroupDelete_Description,
                        Icon = ImageResources.PlanogramRepository_DeleteGroup_32,
                        SmallIcon = ImageResources.PlanogramRepository_DeleteGroup_16
                    };
                    base.ViewModelCommands.Add(_deleteGroupCommand);
                }
                return _deleteGroupCommand;
            }
        }

        public Boolean DeleteGroup_CanExecute()
        {
            //  Must have planogram delete permission.
            if (!ApplicationContext.User.IsInRole(DomainPermission.PlanogramHierarchyGroupDelete.ToString()))
            {
                DeleteGroupCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            return PlanogramRepositoryUIHelper.CanDeletePlanogramGroup(this.SelectedPlanogramGroup.PlanogramGroup);
        }

        public void DeleteGroup_Executed()
        {
            PlanogramGroup groupToDelete = this.SelectedPlanogramGroup.PlanogramGroup;

            //Stop polling while the delete is carried out
            StopPolling();

            //Perform the delete.
            PlanogramRepositoryUIHelper.DeletePlanogramGroup(groupToDelete, OnPlanogramGroupItemDeleted);

            //start polling again
            StartPolling();
        }

        /// <summary>
        /// Called whenever a planogram group or planogram delete
        /// is completed during the process of deleteing a planogram
        /// group and its child items.
        /// </summary>
        private void OnPlanogramGroupItemDeleted(Object sender, EventArgs<Object> args)
        {
            PlanogramInfo deletedPlan = args.ReturnValue as PlanogramInfo;
            if (deletedPlan != null)
            {
                _availablePlanogramRows.Remove(_availablePlanogramRows.FirstOrDefault(p => Object.Equals(p.Info.Id, deletedPlan.Id)));
            }
            else
            {
                PlanogramGroup deletedGroup = args.ReturnValue as PlanogramGroup;
                if (deletedGroup != null)
                {
                    //remove the group from its parent group view.
                    foreach (PlanogramGroupViewModel parentGroup in _masterPlanogramHierarchyView.RootGroup.EnumerateAllChildGroups())
                    {
                        PlanogramGroupViewModel groupView = parentGroup.ChildViews.FirstOrDefault(g => g.PlanogramGroup.Id == deletedGroup.Id);
                        if (groupView != null)
                        {
                            parentGroup.RemoveGroup(groupView);
                            break;
                        }
                    }
                }
            }
        }


        #endregion

        #region EditGroupCommand

        private RelayCommand _editGroupCommand;

        /// <summary>
        /// Edits a PlanogramGroup in the Hierarchy.
        /// </summary>
        public RelayCommand EditGroupCommand
        {
            get
            {
                if (_editGroupCommand == null)
                {
                    _editGroupCommand = new RelayCommand(
                        p => EditGroup_Executed(),
                        p => EditGroup_CanExecute())
                    {
                        FriendlyName = Message.Generic_Edit,
                        FriendlyDescription = Message.PlanRepository_PlanogramGroupEdit_Description,
                        Icon = ImageResources.PlanogramRepository_EditGroup_32,
                        SmallIcon = ImageResources.PlanogramRepository_EditGroup_16,
                        DisabledReason = Message.PlanRepository_DisabledReason_NoPlanogramGroupSelected
                    };
                    base.ViewModelCommands.Add(_editGroupCommand);
                }
                return _editGroupCommand;
            }
        }

        public Boolean EditGroup_CanExecute()
        {
            return this.SelectedPlanogramGroup != null;
        }

        public void EditGroup_Executed()
        {
            StopPolling();
            Boolean showMerchGroupSelector = ApplicationContext.User.IsInRole(DomainPermission.SyncWithMerchandisingHierarchy.ToString());
            PlanogramRepositoryUIHelper.ShowPlanogramGroupEditWindow(this.SelectedPlanogramGroup.PlanogramGroup, showMerchGroupSelector);
            StartPolling();
        }

        #endregion

        #region CutPlanogramGroup

        /// <summary>
        ///     Reference to the current instance for <see cref="CutPlanogramGroupCommand"/>.
        /// </summary>
        private RelayCommand _cutPlanogramGroupCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="CutPlanogramGroupCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand CutPlanogramGroupCommand
        {
            get
            {
                if (_cutPlanogramGroupCommand != null) return _cutPlanogramGroupCommand;

                _cutPlanogramGroupCommand = new RelayCommand(o => CutPlanogramGroup_Executed(), o => CutPlanogramGroup_CanExecute())
                {
                    FriendlyName = Message.Generic_Cut,
                    FriendlyDescription = Message.PlanRepository_PlanogramGroupCut_Description,
                    Icon = ImageResources.Cut_16,
                    SmallIcon = ImageResources.Cut_16
                };
                ViewModelCommands.Add(_cutPlanogramGroupCommand);
                return _cutPlanogramGroupCommand;
            }
        }

        /// <summary>
        ///		Invoked whenever the UI needs to assess the availability of the <see cref="CutPlanogramGroupCommand"/>.
        /// </summary>
        private Boolean CutPlanogramGroup_CanExecute()
        {
            if (SelectorViewModel == null)
            {
                return false;
            }

            if (SelectorViewModel.SelectedGroup.PlanogramGroup == null)
            {
                CutPlanogramGroupCommand.DisabledReason = Message.PlanRepository_DisabledReason_NoPlanogramGroupSelected;
                return false;
            }

            //  Must have group delete permission.
            if (!ApplicationContext.User.IsInRole(DomainPermission.PlanogramHierarchyGroupDelete.ToString()))
            {
                CutPlanogramGroupCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            return true;
        }

        /// <summary>
        ///		Invoked whenever the <see cref="CutPlanogramGroupCommand"/> is executed.
        /// </summary>
        private void CutPlanogramGroup_Executed()
        {
            _isMove = true;
            _copyPasteSource = SelectorViewModel.SelectedGroup;
        }

        #endregion

        #region CopyPlanogramGroup

        /// <summary>
        ///     Reference to the current instance for <see cref="CopyPlanogramGroupCommand"/>.
        /// </summary>
        private RelayCommand _copyPlanogramGroupCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="CopyPlanogramGroupCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand CopyPlanogramGroupCommand
        {
            get
            {
                if (_copyPlanogramGroupCommand != null) return _copyPlanogramGroupCommand;

                _copyPlanogramGroupCommand = new RelayCommand(o => CopyPlanogramGroup_Executed(), o => CopyPlanogramGroup_CanExecute())
                {
                    FriendlyName = Message.Generic_Copy,
                    FriendlyDescription = Message.PlanRepository_PlanogramGroupCopy_Description,
                    Icon = ImageResources.Copy_16,
                    SmallIcon = ImageResources.Copy_16
                };
                ViewModelCommands.Add(_copyPlanogramGroupCommand);
                return _copyPlanogramGroupCommand;
            }
        }

        /// <summary>
        ///		Invoked whenever the UI needs to assess the availability of the <see cref="CopyPlanogramGroupCommand"/>.
        /// </summary>
        private Boolean CopyPlanogramGroup_CanExecute()
        {
            if (SelectorViewModel == null)
            {
                return false;
            }

            if (SelectorViewModel.SelectedGroup.PlanogramGroup == null)
            {
                CopyPlanogramGroupCommand.DisabledReason = Message.PlanRepository_DisabledReason_NoPlanogramGroupSelected;
                return false;
            }

            return true;
        }

        /// <summary>
        ///		Invoked whenever the <see cref="CopyPlanogramGroupCommand"/> is executed.
        /// </summary>
        private void CopyPlanogramGroup_Executed()
        {
            _isMove = false;
            _copyPasteSource = SelectorViewModel.SelectedGroup;
        }

        #endregion

        #region PastePlanogramGroup

        /// <summary>
        ///     Reference to the current instance for <see cref="PastePlanogramGroupCommand"/>.
        /// </summary>
        private RelayCommand _pastePlanogramGroupCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="PastePlanogramGroupCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand PastePlanogramGroupCommand
        {
            get
            {
                if (_pastePlanogramGroupCommand != null) return _pastePlanogramGroupCommand;

                _pastePlanogramGroupCommand = new RelayCommand(o => PastePlanogramGroup_Executed(), o => PastePlanogramGroup_CanExecute())
                {
                    FriendlyName = Message.Generic_Paste,
                    FriendlyDescription = Message.PlanRepository_PlanogramGroupPaste_Description,
                    Icon = ImageResources.Paste_16,
                    SmallIcon = ImageResources.Paste_16
                };
                ViewModelCommands.Add(_pastePlanogramGroupCommand);
                return _pastePlanogramGroupCommand;
            }
        }

        /// <summary>
        ///		Invoked whenever the UI needs to assess the availability of the <see cref="PastePlanogramGroupCommand"/>.
        /// </summary>
        private Boolean PastePlanogramGroup_CanExecute()
        {
            if (SelectorViewModel == null)
            {
                return false;
            }

            if (_copyPasteSource == null)
            {
                PastePlanogramGroupCommand.DisabledReason = Message.PlanRepository_Paste_DisabledReason_NoSource;
                return false;
            }

            if (SelectorViewModel.SelectedGroup.PlanogramGroup == null)
            {
                PastePlanogramGroupCommand.DisabledReason = Message.PlanRepository_DisabledReason_NoPlanogramGroupSelected;
                return false;
            }

            return true;
        }

        /// <summary>
        ///		Invoked whenever the <see cref="PastePlanogramGroupCommand"/> is executed.
        /// </summary>
        private void PastePlanogramGroup_Executed()
        {
            StopPolling();

            PlanogramGroupViewModel groupToPaste = _copyPasteSource as PlanogramGroupViewModel;

            if (_isMove)
            {
                PlanogramRepositoryUIHelper.MovePlanogramGroup(groupToPaste.PlanogramGroup, this.SelectedPlanogramGroup.PlanogramGroup);
            }
            else
            {
                if (SelectorViewModel != null) SelectorViewModel.PasteGroup(groupToPaste);
            }
            _copyPasteSource = null;

            StartPolling();
        }

        #endregion

        #endregion

        #region ExportToFile

        private RelayCommand _exportToFileCommand;

        /// <summary>
        /// Shows the export planograms window
        /// </summary>
        public RelayCommand ExportToFileCommand
        {
            get
            {
                if (_exportToFileCommand == null)
                {
                    _exportToFileCommand =
                        PlanogramExportHelper.CreateShowExportWindowCommand(
                        this.SelectedPlanogramRows.Select(p => p.Info));
                    base.ViewModelCommands.Add(_exportToFileCommand);
                }
                return _exportToFileCommand;
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the entity id changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewState_EntityIdChanged(Object sender, EventArgs e)
        {
            OnRepositoryConnectionChanged();
        }

        /// <summary>
        /// Called whenever the list of available planograms changes.
        /// </summary>
        private void AvailablePlanograms_ModelChanged(Object sender, ViewStateObjectModelChangedEventArgs<PlanogramInfoList> e)
        {
            // Store current planogram row selection.
            List<PlanogramRepositoryRow> currentlySelectedPlanogramRows = SelectedPlanogramRows.ToList();

            if (e.NewModel != null)
            {
                Dictionary<Int32, PlanogramValidationInfo> planIdToValidationDict
                   = new Dictionary<int, PlanogramValidationInfo>();
                try
                {
                    planIdToValidationDict =
                        PlanogramValidationInfoList.FetchByPlanogramIds(e.NewModel.Select(i => i.Id))
                        .ToLookupDictionary(v => v.PlanogramId);
                }
                catch (Csla.DataPortalException ex)
                {
                    CommonHelper.RecordException(ex);
                }

                //remove all old rows
                _availablePlanogramRows.RemoveRange(
                    _availablePlanogramRows.Where(p => !e.NewModel.Select(i => i.Id).Contains(p.Info.Id))
                    .ToList());


                var rowToId = _availablePlanogramRows.ToLookupDictionary(p => p.Info.Id);

                //update or add new ones
                List<PlanogramRepositoryRow> newRows = new List<PlanogramRepositoryRow>();
                foreach (PlanogramInfo planInfo in e.NewModel)
                {
                    PlanogramValidationInfo validationInfo = null;
                    planIdToValidationDict.TryGetValue(planInfo.Id, out validationInfo);

                    PlanogramRepositoryRow row;
                    if (rowToId.TryGetValue(planInfo.Id, out row))
                    {
                        row.UpdateInfo(planInfo, validationInfo);
                    }
                    else
                    {
                        newRows.Add(new PlanogramRepositoryRow(planInfo, validationInfo));
                    }
                }
                if (newRows.Count > 0) _availablePlanogramRows.AddRange(newRows);
            }
            else
            {
                _availablePlanogramRows.Clear();
            }


            RestoreSelection(currentlySelectedPlanogramRows);
        }

        /// <summary>
        /// Called whenever the items fetched fromt the repository should be updated.
        /// </summary>
        private void OnRepositoryConnectionChanged()
        {
            if (!App.ViewState.IsConnectedToRepository)
            {
                OnRepositoryConnectionLost();
                return;
            }

            StartPolling();
            _userFavouritePlanGroupsView.FetchForCurrentUserAndEntity();
            CurrentEntityName =
                EntityInfoList.FetchAllEntityInfos().FirstOrDefault(e => e.Id == App.ViewState.EntityId)?.Name;

            if (CurrentEntityName == null)
            {
                App.ViewState.IsConnectedToRepository = false;
                OnRepositoryConnectionLost();
                return;
            }

            // If the model has not yet loaded, fetch the current planogram hierarchy now.
            if (_masterPlanogramHierarchyView.Model == null) _masterPlanogramHierarchyView.FetchForCurrentEntity();

            // Try to get the last selected planogram group from the session selection.
            PlanogramGroupViewModel lastPlanogramGroupViewModel = null;
            if (App.ViewState.SessionPlanogramGroupSelection != null)
            {
                lastPlanogramGroupViewModel = _masterPlanogramHierarchyView.
                    EnumerateAllGroupViews().
                    Union(_masterPlanogramHierarchyView.GetRecentWork()).
                    FirstOrDefault(p =>
                        p.PlanogramGroup.Id == App.ViewState.SessionPlanogramGroupSelection.PlanogramGroupId &&
                        p.IsRecentWorkGroup == App.ViewState.SessionPlanogramGroupSelection.IsRecentWorkGroup);
            }

            SelectedPlanogramGroup = lastPlanogramGroupViewModel ?? _masterPlanogramHierarchyView.RootGroup;
        }

        private void OnRepositoryConnectionLost()
        {
            StopPolling();
            _masterPlanogramHierarchyView.Model = null;
            _userFavouritePlanGroupsView.Model = null;
            CurrentEntityName = null;

            SelectedPlanogramGroup = null;
        }

        /// <summary>
        /// Called whenever the planogram 
        /// </summary>
        private void OnPlanogramSearchCriteriaChanged()
        {
            UpdateAvailablePlanogramInfos();
        }

        /// <summary>
        /// Called whenever the value of the SelectedPlanogramGroup changes
        /// </summary>
        private void OnSelectedPlanogramGroupChanged()
        {
            UpdateAvailablePlanogramInfos();
        }

        /// <summary>
        ///     Invoked whenever the collection of selected planogram validation rows changes.
        /// </summary>
        private void SelectedPlanogramValidationRows_CollectionChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            var lastSelectedPlanogramValidationRow = this.SelectedPlanogramRows.LastOrDefault();
            this.ActivePlanogramInfo = lastSelectedPlanogramValidationRow != null ?
                lastSelectedPlanogramValidationRow.Info :
                null;
        }


        #endregion

        #region Methods

        private void UpdatePermissionFlags()
        {
            _userHasPlanogramDeletePermission =
                ApplicationContext.User.IsInRole(DomainPermission.PlanogramDelete.ToString());
        }

        /// <summary>
        /// Updates the collection of available planograms.
        ///     If the root planogram group is selected, use the search filter and look recursively down 
        ///     the planogram group hierarchy for the specified search term
        ///     
        ///     If the root planogram group is selected and no search term is specified, only show plans from 
        ///     the root planogram group
        ///     
        ///     If the user has a sub group selected, do not search down the planogram group hierarchy even if 
        ///     there is a search term specified
        /// </summary>
        private void UpdateAvailablePlanogramInfos()
        {
            PlanogramGroup selectedGroup = SelectedPlanogramGroup == null ? null : SelectedPlanogramGroup.PlanogramGroup;

            if (App.ViewState.IsConnectedToRepository
                && selectedGroup != null)
            {
                // Fetch from any planogram group IF
                if ((selectedGroup.ParentGroup == null && !String.IsNullOrEmpty(PlanogramSearchCriteria)) // a search string was provided when the root Planogram Group is selected,
                    || selectedGroup.Id == 0) // OR the root Recent Work Planogram Group is selected.
                {
                    _planogramInfoListViewModel.FetchBySearchCriteriaAsync(PlanogramSearchCriteria, null, App.ViewState.EntityId);
                }
                else
                {
                    _planogramInfoListViewModel.FetchBySearchCriteriaAsync(PlanogramSearchCriteria, selectedGroup.Id, App.ViewState.EntityId);
                }
            }
            else
            {
                _planogramInfoListViewModel.Model = null;
            }
        }

        /// <summary>
        ///     Tries to restore the selected items, if not possible it will select the first available item, or null.
        /// </summary>
        /// <param name="previousSelection">The previously selected <see cref="PlanogramRepositoryRow"/> items to be restored.</param>
        private void RestoreSelection(IEnumerable<PlanogramRepositoryRow> previousSelection)
        {
            //  Clear any previous selection.
            SelectedPlanogramRows.Clear();
            if (PlanogramRows.Count == 0) return;

            // Find matches for the previously selected rows.
            IList<PlanogramRepositoryRow> matchingPlanogramRows = PlanogramRows
                .Where(row =>
                    previousSelection.Any(previousRow =>
                        previousRow.Info.Id == row.Info.Id)).ToList();

            // Select the matches.
            foreach (PlanogramRepositoryRow row in matchingPlanogramRows)
            {
                SelectedPlanogramRows.Add(row);
            }
            // V8-30583 Adding the first available row causes issues 
            //  when the rows have been filtered and that first row is not necessarily listed.
            
            //if (SelectedPlanogramRows.Any()) return;
            // Try to select the first planogram as there were no matches.
            //PlanogramRepositoryRow firstRow = PlanogramRows.FirstOrDefault();
            //if (firstRow != null) SelectedPlanogramRows.Add(firstRow);
        }

        /// <summary>
        ///     Starts the data polling.
        /// </summary>
        private void StartPolling()
        {
            if (_masterPlanogramHierarchyView != null && !_masterPlanogramHierarchyView.IsPolling)
            {
                _masterPlanogramHierarchyView.BeginPollingFetchForCurrentEntity();
            }
        }

        /// <summary>
        ///     Stops the data polling
        /// </summary>
        private void StopPolling()
        {
            if (_masterPlanogramHierarchyView != null) _masterPlanogramHierarchyView.StopPollingAsync();
        }

        #endregion

        #region IDisposable override

        protected override void Dispose(Boolean disposing)
        {
            if (IsDisposed) return;

            if (disposing)
            {
                StopPolling();
                App.ViewState.EntityIdChanged -= ViewState_EntityIdChanged;
                _planogramInfoListViewModel.ModelChanged -= AvailablePlanograms_ModelChanged;

                _selectedPlanogramRows.CollectionChanged -= SelectedPlanogramValidationRows_CollectionChanged;

                AttachedControl = null;
            }

            IsDisposed = true;
        }

        #endregion
    }
}