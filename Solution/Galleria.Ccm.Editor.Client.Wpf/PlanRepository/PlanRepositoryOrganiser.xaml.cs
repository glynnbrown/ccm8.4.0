﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM 8.0.0

// V8-25042 : A.Silva ~ Created.
// V8-25724 : A.Silva ~ Separated code for the plan repository side panel.
// v8-25722 : A.Silva ~ Removed support for filtering planogram groups as it is now done in the independent side panel.
// V8-25718 : A.Silva ~ Changed to use PlanogramDetail.
// V8-25908 : A.Silva ~ Removed references to PlanogramDetail. The Planogram property items are created locally.
// V8-25871 : A.Kuszyk ~ Amended for drag and drop.
// V8-25664 : A.Kuszyk ~ Added event handlers for My Categories.
// V8-26143 : A.Silva ~ Added event handler XPlanogramGrid_OnContextMenuShowing.
// V8-26171 : A.Silva ~ Removed references to the old Custom Column Layout Editor.
// V8-26284 : A.Kuszyk ~ Amended to use Common.Wpf selector and moved out PlanogramGroup drag and drop events.
// V8-26822 : L.Ineson ~ Changed how my categories groups are displayed.
// V8-26944 : A.Silva ~ Replaced SelectedPlanogramInfos with SelectedPlanogramValidationRows.
// V8-27045 : A.Silva ~ Implemented Column Layouts properly.
// V8-26941 : A.Silva ~ Minor corrections.
// V8-27146 : A.Silva ~ Added Group Validation column to the end of the Column Set collection.
// V8-27058 : A.Probyn ~ Removed reference to MetaTotalPositionCount
// V8-27196 : A.Silva ~ Amended Field registration with Custom Column Layout MAnager so that only numeric fields are registered.
// V8-27506 : A.Probyn ~ Added SortMemberPath to PlanRepositoryGroupValidationColumnDataTemplate
// V8-27411 : M.Pettit ~ Added Plan Status cols, renamed ValidationGroups column
// V8-27621 : A.Silva
//      Amended column totals not showing.
// V8-27966 : A.Silva
//      Removed context menu option to refresh.
// V8-28040 : A.Probyn
//  ~ Added code to call ApplyFiltersIntoGrid after column set is applied to the grid.
// V8-27900 : L.ineson
//  Added export to excel to column context menu.
// V8-28083  : A.Probyn
//  Added a SortMemberPath to the template columns on creation. Without this, a filter on another column + sort on a template column
//  causes it to go horribly wrong and fall over.
// V8-28333 : A.Silva
//      Moved the datagrid context menu from showing only for rows, to showing also over white space.
//      Corrected issue when right clicking would not select the row, but only open the context menu.
//  V8-28273 : A.Probyn
//    Corrected so columns are all in visible/frozen options on grid context menu.

#endregion

#region Version History: CCM 8.0.1
//V8:28716 : L.Ineson
//  Copied over unlock command.
#endregion

#region Version History : CCM 8.0.2
// V8-27880 : A.Kuszyk
//  Changed binding of selected group to use view model and added saving of group selection
//  to OnClosed().
#endregion

#region Version History: CCM 8.1.0
// V8-28242 : M.Shelley
//  Added an editor window for the planogram properties
#endregion

#region Version History: CCM 8.1.1
// V8-30225 : M.Shelley
//  Added the Planogram UCR to the plan repository grid
// V8-30278 : L.Ineson
//  Amended Plan status column so that it can be sorted & filtered
// V8-30058 : M.Shelley
//  Added a tooltip for the context menu "Properties" and "Delete" items
#endregion

#region Version History : CCM820
// V8-30527 : A.Kuszyk
//  Added permission to Planogram Group delete menu item.
//V8-30958 : L.Ineson
//  Updated after column manager changes.
#endregion

#region Version History : CCM830
// V8-31832 : L.Ineson
//  Changes to support calculated columns.
// V8-32191 : A.Probyn
//  Updated to override OnPreviewKeyDown for copy and paste.
#endregion
#endregion

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Windows.Threading;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using ContextMenu = Fluent.ContextMenu;
using Image = System.Windows.Controls.Image;
using MenuItem = Fluent.MenuItem;
using Galleria.Framework.Model;
using System.Collections.Generic;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanRepository
{
    /// <summary>
    ///     Interaction logic for PlanRepositoryOrganiser.xaml
    /// </summary>
    public sealed partial class PlanRepositoryOrganiser
    {
        #region Fields

        private GridLength _startingHeightBottomPanel = new GridLength(300.0);//Starting height for the bottom panel (planogram information panel).
        private GridLength _startingWidthSidePanel = new GridLength(210.0);//Starting width for the side panel (planogram group hierarchy).

        private ColumnLayoutManager _columnLayoutManager;

        #endregion

        #region Properties

        #region ViewModel Property

        /// <summary>
        ///     The <see cref="DependencyProperty" /> for the <see cref="ViewModel" /> property.
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanRepositoryViewModel), typeof(PlanRepositoryOrganiser),
                new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        ///     Handles the change of viemodels for the view.
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = (PlanRepositoryOrganiser)sender;

            // Detach previous viemodel if there was one.
            if (e.OldValue != null)
            {
                PlanRepositoryViewModel oldModel = e.OldValue as PlanRepositoryViewModel;
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                PlanRepositoryViewModel newModel = e.NewValue as PlanRepositoryViewModel;
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;


            }
            senderControl.UpdateBreadcrumb();
        }

        /// <summary>
        ///     Gets or sets the viewmodel context for this view.
        /// </summary>
        public PlanRepositoryViewModel ViewModel
        {
            get { return (PlanRepositoryViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        /// <summary>
        ///     ViewModel for the Planogram Hierarchy Selector.
        /// </summary>
        private PlanogramHierarchySelectorViewModel SelectorViewModel
        {
            get { return xPlanHierarchySelector.ViewModel; }
        }

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of the <see cref="PlanRepositoryOrganiser" /> class.
        /// </summary>
        public PlanRepositoryOrganiser()
        {
            // Show the busy cursor while loading the screen.
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            ViewModel = new PlanRepositoryViewModel();

            Loaded += PlanRepositoryOrganiser_Loaded;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Carries out actions after the window is Loaded.
        /// </summary>
        private void PlanRepositoryOrganiser_Loaded(Object sender, RoutedEventArgs e)
        {
            Loaded -= PlanRepositoryOrganiser_Loaded;

            // Initialize the custom column manager
            const String pathMask = "Info.{0}";// The actual PlanogramInfo is wrapped in the PlanogramRepositoryRow, and accessed through PlanogramRepositoryRow.Info.

            PlanogramInfoColumnLayoutFactory layoutFactory =
                new PlanogramInfoColumnLayoutFactory(typeof(PlanogramInfo),
                    new List<IModelPropertyInfo>
                    {
                        PlanogramInfo.NameProperty,
                        PlanogramInfo.UniqueContentReferenceProperty,
                        PlanogramInfo.StatusProperty
                    });

            _columnLayoutManager = new ColumnLayoutManager(
                layoutFactory,
                DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.EntityId),
                PlanRepositoryViewModel.ScreenKey,
                pathMask);

            _columnLayoutManager.CalculatedColumnPath = "CalculatedValueResolver";
            _columnLayoutManager.ColumnSetChanging += ColumnLayoutManager_ColumnSetChanging;
            _columnLayoutManager.RegisterDataTemplate<PlanogramRepositoryRow>(PlanogramRepositoryRow.CreateDataTemplate);
            _columnLayoutManager.AttachDataGrid(xPlanogramGrid);

            
            // Stop showing the busy cursor as loading the screen has finished.
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; })
                , DispatcherPriority.Background, null);

            // Try to connect to the repository if we do not have a current connection yet.
            if (!App.ViewState.IsConnectedToRepository)
            {
                ViewModel.ConnectToRepositoryCommand.Execute();
            }

        }

         /// <summary>
        /// Called whenever the column layout manager columns are about to change.
        /// </summary>
        private void ColumnLayoutManager_ColumnSetChanging(object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            //Prefix with addition columns:

            Int32 curIdx = 0;

            //Info column group
            columnSet.Insert(curIdx++, new DataGridExtendedTemplateColumn
            {
                Header = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_InformationColumnHeader,
                CellTemplate = Resources["PlanRepositoryInfoColumnDataTemplate"] as DataTemplate,
                CanUserFilter = false,
                CanUserSort = false,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                MinWidth = 50,
                IsReadOnly = true,
                ColumnGroupName = "",
                SortMemberPath = "LockedToolTip",
                ColumnCellAlignment = HorizontalAlignment.Stretch
            });

            //Planogram Name column
            DataGridExtendedTextColumn nameGridCol = new DataGridExtendedTextColumn
            {
                Header = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_PlanogramNameColumnHeader,
                Binding = new Binding("Info.Name"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = "",
                MinWidth = 50,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(curIdx++, nameGridCol);

            // Planogram UCR column
            DataGridExtendedTextColumn ucrGridCol = new DataGridExtendedTextColumn
            {
                Header = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_PlanogramUCRColumnHeader,
                Binding = new Binding("Info.UniqueContentReference"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = "",
                MinWidth = 60,
                Width = 100,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(curIdx++, ucrGridCol);

            //Create the header group
            DataGridHeaderGroup statusHeaderGroup = new DataGridHeaderGroup();
            statusHeaderGroup.HeaderName = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_StatusGroupHeader;

            //Plan Status column
            DataGridExtendedTextColumn planStatusCol = new DataGridExtendedTextColumn()
            {
                Header = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_PlanStatusColumnHeader,
                Binding = new Binding("Info.Status")
                {
                    Mode = BindingMode.OneWay,
                    ConverterParameter = PlanogramStatusTypeHelper.FriendlyNames,
                    Converter = Application.Current.Resources[Framework.Controls.Wpf.ResourceKeys.ConverterDictionaryKeyToValue] as IValueConverter
                },
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                IsReadOnly = true,
                MinWidth = 50,
                ColumnGroupName = statusHeaderGroup.HeaderName,
            };
            columnSet.Insert(curIdx++, planStatusCol);

            //Validation column
            DataGridExtendedTemplateColumn validationStatusCol = new DataGridExtendedTemplateColumn
            {
                Header = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_ValidationColumnHeader,
                CellTemplate = Resources["PlanRepositoryValidationColumnDataTemplate"] as DataTemplate,
                CanUserFilter = false,
                CanUserSort = false,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                MinWidth = 50,
                IsReadOnly = true,
                SortMemberPath = "ValidationProcessingVisibility",
                ColumnGroupName = statusHeaderGroup.HeaderName,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(curIdx++, validationStatusCol);

        }

        /// <summary>
        ///     Handles the change of selection for the current <see cref="PlanogramInfo" />.
        /// </summary>
        private void ViewModel_PropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == PlanRepositoryViewModel.SelectedPlanogramGroupProperty.Path)
            {
                UpdateBreadcrumb();
            }
        }

        #region XPlanogramGrid handlers

        /// <summary>
        /// Called when a planogram is double clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void XPlanogramGrid_OnRowItemMouseDoubleClick(Object sender, ExtendedDataGridItemEventArgs e)
        {
            ViewModel.OpenPlanogramCommand.Execute();
        }

        /// <summary>
        /// Called when the enter key is pressed on the grid
        /// </summary>
        private void XPlanogramGrid_OnKeyDown(Object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && ViewModel.SelectedPlanogramRows.Count > 0)
            {
                ViewModel.OpenPlanogramCommand.Execute();
                e.Handled = true;
            }

            else if (e.Key == Key.Space)
            {
                Keyboard.Focus(xBottomExpander);
            }

            else
            {
                return;
            }
        }

        /// <summary>
        ///     Invoked whenever the context menu on the planogram is displaying.
        /// </summary>
        private void XPlanogramGrid_OnContextMenuOpening(Object sender, ContextMenuEventArgs e)
        {
            if (ViewModel == null) { return; }

            //show the row context menu
            var contextMenu = new ContextMenu();

            contextMenu.Items.Add(NewMenuItem(ViewModel.OpenPlanogramCommand));
            contextMenu.Items.Add(new Separator());

            // Check if a planogram can be unlocked
            bool canUnlockPlans = ViewModel.UnlockSelectedPlanogramCommand.CanExecute();

            // Check if there are any locked plans selected
            // If so, add the "Unlock" context menu item
            contextMenu.Items.Add(new Separator());
            if (canUnlockPlans)
            {
                contextMenu.Items.Add(NewMenuItem(ViewModel.UnlockSelectedPlanogramCommand));
            }

            // Always add the "Properties" context menu item but if any of the selected plans 
            // are locked disable the menu item (done in the CanExecute method)
            var propMenuitem = NewMenuItem(ViewModel.PlanogramPropertiesCommand);
            if (!String.IsNullOrEmpty(ViewModel.PlanogramPropertiesCommand.DisabledReason))
            {
                propMenuitem.ToolTip = ViewModel.PlanogramPropertiesCommand.DisabledReason;
            }
            contextMenu.Items.Add(propMenuitem);

            contextMenu.Items.Add(NewMenuItem(ViewModel.RenamePlanogramCommand));

            contextMenu.Items.Add(new Separator());
            contextMenu.Items.Add(NewMenuItem(ViewModel.CutPlanogramCommand));
            contextMenu.Items.Add(NewMenuItem(ViewModel.CopyPlanogramCommand));
            contextMenu.Items.Add(NewMenuItem(ViewModel.PastePlanogramCommand));
            contextMenu.Items.Add(new Separator());

            var deleteMenuItem = NewMenuItem(ViewModel.DeletePlanogramCommand);
            if (!String.IsNullOrEmpty(ViewModel.DeletePlanogramCommand.DisabledReason))
            {
                deleteMenuItem.ToolTip = ViewModel.DeletePlanogramCommand.DisabledReason;
            }
            contextMenu.Items.Add(deleteMenuItem);

            contextMenu.Placement = PlacementMode.Mouse;
            contextMenu.IsOpen = true;
        }

        #endregion

        #region Panel expanders support

        private void XBottomPanelExpander_OnCollapsed(Object sender, RoutedEventArgs e)
        {
            var index = Grid.GetRow((Expander)sender);
            var row = xBaseGrid.RowDefinitions[index];
            _startingHeightBottomPanel = row.Height; // Store the height of the panel BEFORE it was collapsed.
            row.Height = GridLength.Auto; // Collapse the panel.
        }

        private void XBottomPanelExpander_OnExpanded(Object sender, RoutedEventArgs e)
        {
            var index = Grid.GetRow((Expander)sender);
            var row = xBaseGrid.RowDefinitions[index];
            row.Height = _startingHeightBottomPanel; // Restore the panel to the size it had before being collapsed.
        }

        private void XSidePanelExpander_OnCollapsed(Object sender, RoutedEventArgs e)
        {
            var index = Grid.GetColumn((Expander)sender);
            var column = xBaseGrid.ColumnDefinitions[index];
            _startingWidthSidePanel = column.Width; // Store the width of the the panel BEFORE it was collapsed.
            column.Width = GridLength.Auto; // Collapse the panel.
        }

        private void XSidePanelExpander_OnExpanded(Object sender, RoutedEventArgs e)
        {
            var index = Grid.GetColumn((Expander)sender);
            var column = xBaseGrid.ColumnDefinitions[index];
            column.Width = _startingWidthSidePanel; // Restore the panel to the size it had before being collapsed.
        }

        #endregion

        /// <summary>
        /// Called whenever a breadcrumb is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Breadcrumb_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (ViewModel == null) return;

            TextBlock breadcrumb = (TextBlock)sender;
            PlanogramGroupViewModel group = breadcrumb.Tag as PlanogramGroupViewModel;
            if (group != null)
            {
                ViewModel.SelectedPlanogramGroup = group;
            }
        }

        /// <summary>
        ///     Invoked when the <see cref="PlanogramHierarchySelector"/> control has finished loading.
        /// </summary>
        private void XPlanHierarchySelector_OnLoaded(Object sender, RoutedEventArgs e)
        {
            this.xPlanHierarchySelector.Loaded -= XPlanHierarchySelector_OnLoaded;
            this.ViewModel.SelectorViewModel = this.SelectorViewModel;
        }

        /// <summary>
        ///     Invoked whenever the context menu for the planogram hierarchy selector is needed.
        /// </summary>
        private void XPlanHierarchySelector_OnNodeContextMenuShowing(Object sender, PlanogramHierarchyContextEventArgs e)
        {
            var contextMenu = e.ContextMenu;

            if (ViewModel.NewGroupCommand.CanExecute()) contextMenu.Items.Add(NewMenuItem(ViewModel.NewGroupCommand));
            if (ViewModel.EditGroupCommand.CanExecute()) contextMenu.Items.Add(NewMenuItem(ViewModel.EditGroupCommand));
            
            // Always add the delete menu item, because it might not be executable due to permissions.
            MenuItem deleteMenuItem = NewMenuItem(ViewModel.DeleteGroupCommand);
            if (!String.IsNullOrEmpty(ViewModel.DeleteGroupCommand.DisabledReason))
            {
                deleteMenuItem.ToolTip = ViewModel.DeleteGroupCommand.DisabledReason;
            }
            contextMenu.Items.Add(deleteMenuItem);

            contextMenu.Items.Add(new Separator());

            if (ViewModel.CutPlanogramGroupCommand.CanExecute()) contextMenu.Items.Add(NewMenuItem(ViewModel.CutPlanogramGroupCommand));
            if (ViewModel.CopyPlanogramGroupCommand.CanExecute()) contextMenu.Items.Add(NewMenuItem(ViewModel.CopyPlanogramGroupCommand));
            if (ViewModel.PastePlanogramGroupCommand.CanExecute()) contextMenu.Items.Add(NewMenuItem(ViewModel.PastePlanogramGroupCommand));
        }

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            base.OnPreviewKeyDown(e);

            //If Ctrl+C selected, copy planogram
            if (e.Key == Key.C && Keyboard.Modifiers == ModifierKeys.Control)
            {
                if (this.ViewModel.CopyPlanogramCommand.CanExecute())
                {
                    this.ViewModel.CopyPlanogramCommand.Execute();
                }
            }
            else if (e.Key == Key.X && Keyboard.Modifiers == ModifierKeys.Control)
            {
                if (this.ViewModel.CutPlanogramCommand.CanExecute())
                {
                    this.ViewModel.CutPlanogramCommand.Execute();
                }
            }
            else if (e.Key == Key.V && Keyboard.Modifiers == ModifierKeys.Control)
            {
                if (this.ViewModel.PastePlanogramCommand.CanExecute())
                {
                    this.ViewModel.PastePlanogramCommand.Execute();
                }
            }
        }

        private void xBottomExpander_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (xBottomExpander.IsExpanded)
                {
                    xBottomExpander.IsExpanded = false;
                }
                else
                {
                    xBottomExpander.IsExpanded = true;
                }
            }
        }
        private void xSideExpander_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (xSideExpander.IsExpanded)
                {
                    xSideExpander.IsExpanded = false;
                }
                else
                {
                    xSideExpander.IsExpanded = true;
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the breadcurb area
        /// </summary>
        private void UpdateBreadcrumb()
        {
            if (BreadcrumbStackPanel == null) return;

            //clear out any existing items.
            foreach (TextBlock tb in BreadcrumbStackPanel.Children.OfType<TextBlock>())
            {
                tb.MouseLeftButtonDown -= Breadcrumb_MouseLeftButtonDown;
            }
            BreadcrumbStackPanel.Children.Clear();

            //load in the path for the selected plan group
            if (ViewModel != null && ViewModel.SelectedPlanogramGroup != null)
            {
                //add in the icon
                var icon = new Image()
                {
                    Height = 15,
                    Width = 15,
                    Source = ImageResources.TreeViewItem_OpenFolder,
                    Margin = new Thickness(1, 1, 4, 1),
                    VerticalAlignment = VerticalAlignment.Center
                };
                BreadcrumbStackPanel.Children.Add(icon);

                Style itemStyle = (Style)Resources["PlanRepository_StyBreadcrumbItem"];
                Style sepStyle = (Style)Resources["PlanRepository_StyBreadcrumbSeparator"];

                //load in the breadcrumb parts
                foreach (var group in ViewModel.SelectedPlanogramGroup.PlanogramGroup.FetchFullPath())
                {
                    TextBlock breadcrumb = new TextBlock();
                    breadcrumb.Style = itemStyle;
                    breadcrumb.Text = group.Name;
                    breadcrumb.Tag = group;
                    breadcrumb.MouseLeftButtonDown += Breadcrumb_MouseLeftButtonDown;
                    BreadcrumbStackPanel.Children.Add(breadcrumb);

                    Path separator = new Path();
                    separator.Style = sepStyle;
                    BreadcrumbStackPanel.Children.Add(separator);
                }
            }

        }

        #endregion

        #region Static Helper Methods


        /// <summary>
        ///     Creates a new instance of <see cref="MenuItem"/> from the provided <see cref="IRelayCommand"/> object.
        /// </summary>
        /// <param name="command">The command from which to generate the menu item.</param>
        /// <returns>A new instance of <see cref="MenuItem"/>.</returns>
        private static MenuItem NewMenuItem(IRelayCommand command)
        {
            return new MenuItem
            {
                Command = command,
                Header = command.FriendlyName,
                Icon = command.SmallIcon ?? command.Icon
            };
        }

        private static Boolean IsValidIndex(ICollection items, Int32 index)
        {
            return 0 <= index && index < items.Count;
        }

        #endregion

        #region Window close

        /// <summary>
        ///     Dispose of anything Disposable when closing the window.
        /// </summary>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            if (ViewModel != null)
            {
                // Store the selected group's Id in the ViewState for next time.
                App.ViewState.SessionPlanogramGroupSelection = 
                    new SessionPlanogramGroupSelection(this.xPlanHierarchySelector.ViewModel.SelectedGroup);

                Dispatcher.Invoke((Action)(() =>
                {
                    var disposableViewModel = ViewModel as IDisposable;
                    if (disposableViewModel != null) disposableViewModel.Dispose();
                    
                    ViewModel = null;

                    if (_columnLayoutManager != null)
                    {
                        _columnLayoutManager.ColumnSetChanging -= ColumnLayoutManager_ColumnSetChanging;
                        _columnLayoutManager.Dispose();
                    }

                    //also dispose of the hierarchy selector
                    if (xPlanHierarchySelector != null)
                    {
                        xPlanHierarchySelector.Dispose();
                    }

                }), DispatcherPriority.Background);
            }
        }

        #endregion

    }
}
