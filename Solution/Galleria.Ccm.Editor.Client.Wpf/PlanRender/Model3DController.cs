﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2013

//#region Version History: (CCM 8.0)
//// L.Hodson ~ Created.
//#endregion
//#endregion

//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Diagnostics.CodeAnalysis;
//using System.Linq;
//using System.Windows;
//using System.Windows.Controls;
//using System.Windows.Controls.Primitives;
//using System.Windows.Media.Media3D;
//using Galleria.Ccm.Editor.Client.Wpf.Common;
//using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow.PlanControls;
//using Galleria.Framework2.DataStructures;

//namespace Galleria.Ccm.Editor.Client.Wpf.PlanRender
//{
//    //TODO: Move into plan controls

//    /// <summary>
//    /// A control which aids in manipulating a 3D models on a 2D plane.
//    /// </summary>
//    public sealed class Model3DController : Control, IDisposable
//    {
//        #region Fields
//        private ModelConstruct3D _model;
//        private Viewport3D _viewport;
//        #endregion

//        #region Properties

//        #region BoundingHeight

//        public static readonly DependencyProperty BoundingHeightProperty =
//            DependencyProperty.Register("BoundingHeight", typeof(Double), typeof(Model3DController),
//            new PropertyMetadata(0D));

//        /// <summary>
//        /// Gets/Sets the height of the bounding box
//        /// </summary>
//        public Double BoundingHeight
//        {
//            get { return (Double)GetValue(BoundingHeightProperty); }
//            set { SetValue(BoundingHeightProperty, value); }
//        }

//        #endregion

//        #region BoundingWidth

//        public static readonly DependencyProperty BoundingWidthProperty =
//            DependencyProperty.Register("BoundingWidth", typeof(Double), typeof(Model3DController),
//            new PropertyMetadata(0D));

//        /// <summary>
//        /// Gets/Sets the width of the bounding box
//        /// </summary>
//        public Double BoundingWidth
//        {
//            get { return (Double)GetValue(BoundingWidthProperty); }
//            set { SetValue(BoundingWidthProperty, value); }
//        }

//        #endregion

//        public ModelConstruct3D Model
//        {
//            get { return _model; }
//        }

//        public Vector3D TransformVector { get; set; }

//        /// <summary>
//        /// The plan item we are controlling 
//        /// TODO: Consider moving this out so that the controller could be
//        /// put into the framework..
//        /// </summary>
//        public IPlanItem PlanItem
//        {
//            get { return ((IPlanPart3DData)_model.ModelData).PlanPart as IPlanItem; }
//        }

//        #endregion

//        #region Constructor

//        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline",
//            Justification = "Dependency properties are initialized in-line.")]
//        static Model3DController()
//        {
//            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
//                typeof(Model3DController), new FrameworkPropertyMetadata(typeof(Model3DController)));
//        }

//        public Model3DController(ModelConstruct3D controlModel, Viewport3D viewport)
//        {
//            this.TransformVector = new Vector3D(1, 1, 0);
//            _model = controlModel;
//            _viewport = viewport;

//            UpdatePosition();

//            controlModel.Updated += new EventHandler(visual_Updated);
//        }

//        #endregion

//        #region Event Handlers

//        private void visual_Updated(object sender, EventArgs e)
//        {
//            UpdatePosition();
//        }

//        protected override void OnMouseDoubleClick(System.Windows.Input.MouseButtonEventArgs e)
//        {
//            base.OnMouseDoubleClick(e);

//            PlanItemPropertiesWindow win = new PlanItemPropertiesWindow(this.PlanItem);
//            App.ShowWindow(win, /*isModal*/true);
//        }

//        #endregion

//        #region Methods

//        /// <summary>
//        /// Updates the position of this controller.
//        /// </summary>
//        public void UpdatePosition()
//        {
//            Viewport3D parentViewport = _viewport;
//            if (parentViewport != null)
//            {
//                ModelConstruct3D model = this.Model;

//                Boolean ignorePositions = (this.PlanItem.PlanItemType != PlanItemType.Position);

//                Rect bounds = GetModelBounds(parentViewport, model, Rect.Empty, ignorePositions);
//                if (!bounds.IsEmpty)
//                {
//                    this.BoundingHeight = bounds.Height;
//                    this.BoundingWidth = bounds.Width;
//                    Canvas.SetLeft(this, bounds.X);
//                    Canvas.SetTop(this, bounds.Y);
//                }
//            }
//            else
//            {
//                this.BoundingHeight = 0;
//                this.BoundingWidth = 0;
//            }
//        }

//        #region Move Action

//        private object _actionPropertyResetValue;

//        internal void BeginMoveAction()
//        {
//            this.PlanItem.Planogram.BeginEdit();

//            //take a copy of the raw position of the item.
//            _actionPropertyResetValue =
//                new ObjectPoint3D()
//                {
//                    X = this.PlanItem.X,
//                    Y = this.PlanItem.Y,
//                    Z = this.PlanItem.Z,
//                };
//        }

//        internal void MoveVisual(Double horizDelta, Double vertDelta)
//        {
//            //get the current position
//            Point currPos = new Point(Canvas.GetLeft(this), Canvas.GetTop(this));

//            //get the new position
//            Point newPos = new Point(currPos.X + horizDelta, currPos.Y + vertDelta);

//            //apply the change to this.
//            Canvas.SetLeft(this, newPos.X);
//            Canvas.SetTop(this, newPos.Y);

//            //Translate and move the visual
//            IPlanItem planItem = this.PlanItem;

//            Viewport3D parentViewport = _viewport;
//            if (parentViewport != null)
//            {
//                Point3D pointNear;
//                Point3D pointFar;

//                if (HelixToolkit.Wpf.Viewport3DHelper.Point2DtoPoint3D(parentViewport, newPos, out pointNear, out pointFar))
//                {
//                    Boolean ignorePositions = (planItem.PlanItemType != PlanItemType.Position);

//                    Rect3D modelsBounds = GetBoundingRect(this.Model, Rect3D.Empty, ignorePositions); //nb - position is not correct in this.
//                    Point3D cummulatedPos = GetParentPositionCummulation(planItem);

//                    ObjectPoint3D rawPosition =
//                            new ObjectPoint3D()
//                            {
//                                X = planItem.X,
//                                Y = planItem.Y,
//                                Z = planItem.Z,
//                            };

//                    ObjectPoint3D newPosition =
//                            new ObjectPoint3D()
//                            {
//                                X = pointNear.X - cummulatedPos.X,
//                                Y = pointNear.Y - cummulatedPos.Y,
//                                Z = pointNear.Z - cummulatedPos.Z,
//                            };


//                    if (this.TransformVector.X == 0)
//                    {
//                        newPosition.X = rawPosition.X;
//                    }
//                    else if (this.TransformVector.X == -1)
//                    {
//                        newPosition.X -= modelsBounds.Size.X;
//                    }


//                    if (this.TransformVector.Y == 0)
//                    {
//                        newPosition.Y = rawPosition.Y;
//                    }
//                    else if (this.TransformVector.Y == -1)
//                    {
//                        newPosition.Y -= modelsBounds.Size.Y;
//                    }

//                    if (this.TransformVector.Z == 0)
//                    {
//                        newPosition.Z = rawPosition.Z;
//                    }
//                    else if (this.TransformVector.Z == -1)
//                    {
//                        newPosition.Z += modelsBounds.Size.Z;
//                    }


//                    //Set the new item position.
//                    planItem.X = (Single)newPosition.X;
//                    planItem.Y = (Single)newPosition.Y;
//                    planItem.Z = (Single)newPosition.Z;
//                }
//            }

//        }

//        internal void EndMoveAction()
//        {
//            ObjectPoint3D origValue = (ObjectPoint3D)_actionPropertyResetValue;

//            //get the raw item position
//            ObjectPoint3D newValue =
//                new ObjectPoint3D()
//                {
//                    X = this.PlanItem.X,
//                    Y = this.PlanItem.Y,
//                    Z = this.PlanItem.Z,
//                };


//            PlanogramView parentPlan = this.PlanItem.Planogram;

//            ModelUndoAction newUndoAction = new ModelUndoAction();
//            newUndoAction.AddSetPropertyAction(this.PlanItem, PlanItemHelper.XProperty.Path, origValue.X, newValue.X);
//            newUndoAction.AddSetPropertyAction(this.PlanItem, PlanItemHelper.YProperty.Path, origValue.Y, newValue.Y);
//            newUndoAction.AddSetPropertyAction(this.PlanItem, PlanItemHelper.ZProperty.Path, origValue.Z, newValue.Z);
//            parentPlan.AddUndoAction(newUndoAction);
                
//            _actionPropertyResetValue = null;

//            this.PlanItem.Planogram.EndEdit();
//        }

//        #endregion

//        #endregion

//        #region IDisposable Members

//        private Boolean _isDisposed;

//        public void Dispose()
//        {
//            if (!_isDisposed)
//            {
//                _model.Updated -= visual_Updated;

//                GC.SuppressFinalize(this);
//                _isDisposed = true;
//            }
//        }

//        #endregion

//        #region Static Helpers

//        private static Point3D GetParentPositionCummulation(IPlanItem item)
//        {
//            Point3D position = new Point3D();

//            Transform3DGroup fullPosTransform = new Transform3DGroup();

//            switch (item.PlanItemType)
//            {
//                case PlanItemType.SubComponent:
//                    {
//                        fullPosTransform.Children.Add(new TranslateTransform3D(item.Fixture.X, item.Fixture.Y, item.Fixture.Z));
//                        fullPosTransform.Children.Add(new TranslateTransform3D(item.Assembly.X, item.Assembly.Y, item.Assembly.Z));
//                        fullPosTransform.Children.Add(new TranslateTransform3D(item.Component.X, item.Component.Y, item.Component.Z));

//                        position = fullPosTransform.Transform(new Point3D());
//                    }
//                    break;

//                case PlanItemType.Component:
//                    {
//                        fullPosTransform.Children.Add(new TranslateTransform3D(item.Fixture.X, item.Fixture.Y, item.Fixture.Z));
//                        fullPosTransform.Children.Add(new TranslateTransform3D(item.Assembly.X, item.Assembly.Y, item.Assembly.Z));

//                        position = fullPosTransform.Transform(new Point3D());
//                    }
//                    break;


//                case PlanItemType.Assembly:
//                    {
//                        fullPosTransform.Children.Add(new TranslateTransform3D(item.Fixture.X, item.Fixture.Y, item.Fixture.Z));

//                        position = fullPosTransform.Transform(new Point3D());
//                    }
//                    break;

//                case PlanItemType.Fixture:
//                    //do nothing
//                    break;

//                case PlanItemType.Position:
//                    {
//                        fullPosTransform.Children.Add(new TranslateTransform3D(item.Fixture.X, item.Fixture.Y, item.Fixture.Z));
//                        fullPosTransform.Children.Add(new TranslateTransform3D(item.Assembly.X, item.Assembly.Y, item.Assembly.Z));
//                        fullPosTransform.Children.Add(new TranslateTransform3D(item.Component.X, item.Component.Y, item.Component.Z));
//                        fullPosTransform.Children.Add(new TranslateTransform3D(item.SubComponent.X, item.SubComponent.Y, item.SubComponent.Z));

//                        position = fullPosTransform.Transform(new Point3D());
//                    }
//                    break;

//                case PlanItemType.Annotation:
//                    {
//                        if (item.Fixture != null)
//                        {
//                            fullPosTransform.Children.Add(new TranslateTransform3D(item.Fixture.X, item.Fixture.Y, item.Fixture.Z));

//                            if (item.Assembly != null)
//                            {
//                                fullPosTransform.Children.Add(new TranslateTransform3D(item.Assembly.X, item.Assembly.Y, item.Assembly.Z));

//                                if (item.Component != null)
//                                {
//                                    fullPosTransform.Children.Add(new TranslateTransform3D(item.Component.X, item.Component.Y, item.Component.Z));

//                                    if (item.SubComponent != null)
//                                    {
//                                        fullPosTransform.Children.Add(new TranslateTransform3D(item.SubComponent.X, item.SubComponent.Y, item.SubComponent.Z));
//                                    }
//                                }
//                            }
//                        }

//                        position = fullPosTransform.Transform(new Point3D());
//                    }
//                    break;
//            }

//            return position;
//        }

//        /// <summary>
//        /// Returns a rectangle that contains the model in screen space.
//        /// </summary>
//        /// <param name="model3D">the Model3D to get the bounding info from</param>
//        /// <param name="includeChildModels">includes any other models attached to the supplied model</param>
//        /// <returns>the bounding rectangle, or Rect.Empty if no model is found.</returns>
//        public static Rect GetModelBounds(Viewport3D viewport, ModelConstruct3D model3D, Rect bounds, Boolean ignorePositions)
//        {
//            Rect output = bounds;
//            if (model3D != null)
//            {
//                //get the max bounding area from the model parts
//                foreach (var part in model3D.ModelParts)
//                {
//                    //create the bounding rect for the part
//                    Rect partBounds = Model3DHelper.GetModelPartBounds(viewport, part, model3D);

//                    //If we have not found any bounds yet then set the output to
//                    //the current child bound
//                    if (output.Equals(Rect.Empty))
//                    {
//                        output = partBounds;
//                    }
//                    else
//                    {
//                        //Make sure we get the maximum area.
//                        output = new Rect(
//                            new Point(
//                                Math.Min(partBounds.TopLeft.X, output.TopLeft.X),
//                                Math.Min(partBounds.TopLeft.Y, output.TopLeft.Y)
//                                ),
//                            new Point(
//                                Math.Max(partBounds.BottomRight.X, output.BottomRight.X),
//                                Math.Max(partBounds.BottomRight.Y, output.BottomRight.Y)
//                                )
//                            );
//                    }
//                }


//                //Check the children for drawn objects
//                foreach (var child in model3D.ModelChildren)
//                {
//                    if (!ignorePositions
//                        || (ignorePositions && !(child.ModelData is PlanPosition3DData)))
//                    {
//                        //Get the model bounds for the child
//                        Rect childBounds = GetModelBounds(viewport, child, output, ignorePositions);

//                        //If we have not found any bounds yet then set the output to
//                        //the current child bound
//                        if (output.Equals(Rect.Empty))
//                        {
//                            output = childBounds;
//                        }
//                        else
//                        {
//                            //Make sure we get the maximum area.
//                            output = new Rect(
//                                new Point(
//                                    Math.Min(childBounds.TopLeft.X, output.TopLeft.X),
//                                    Math.Min(childBounds.TopLeft.Y, output.TopLeft.Y)
//                                    ),
//                                new Point(
//                                    Math.Max(childBounds.BottomRight.X, output.BottomRight.X),
//                                    Math.Max(childBounds.BottomRight.Y, output.BottomRight.Y)
//                                    )
//                                );
//                        }
//                    }
//                }

//            }

//            return output;
//        }

//        public static Rect3D GetBoundingRect(ModelConstruct3D model3D, Rect3D bounds, Boolean ignorePositions)
//        {
//            foreach (ModelConstructPart3D part in model3D.ModelParts)
//            {
//                bounds.Union(part.GetBounds());
//            }

//            foreach (ModelConstruct3D child in model3D.ModelChildren)
//            {
//                if (!ignorePositions
//                        || (ignorePositions && !(child.ModelData is PlanPosition3DData)))
//                {
//                    bounds.Union(GetBoundingRect(child, bounds, ignorePositions));
//                }
//            }

//            return bounds;
//        }

//        #endregion
//    }

//    #region Supporting Classes

//    public sealed class Model3DMoveThumb : Thumb
//    {
//        #region Fields
//        private Model3DController _parentController;
//        #endregion

//        #region Constructor

//        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Dependency properties are initialized in-line.")]
//        static Model3DMoveThumb()
//        {
//            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
//                typeof(Model3DMoveThumb), new FrameworkPropertyMetadata(typeof(Model3DMoveThumb)));
//        }

//        public Model3DMoveThumb()
//        {
//            this.DragStarted += new DragStartedEventHandler(OnDragStarted);
//            this.DragDelta += new DragDeltaEventHandler(OnDragDelta);
//            this.DragCompleted += new DragCompletedEventHandler(OnDragCompleted);
//        }

//        #endregion

//        #region Event Handlers

//        private void OnDragStarted(object sender, DragStartedEventArgs e)
//        {
//            if (_parentController == null)
//            {
//                _parentController = Framework.Controls.Wpf.Helpers.FindVisualAncestor<Model3DController>(this);
//            }

//            if (_parentController != null)
//            {
//                //lock on
//                CaptureMouse();

//                //indicate that a move action has begun
//                _parentController.BeginMoveAction();
//            }
//        }

//        private void OnDragDelta(object sender, DragDeltaEventArgs e)
//        {
//            _parentController.MoveVisual(e.HorizontalChange, e.VerticalChange);
//        }

//        private void OnDragCompleted(object sender, DragCompletedEventArgs e)
//        {
//            //release the mouse
//            ReleaseMouseCapture();

//            if (_parentController != null)
//            {
//                //indicate that the move action has completed
//                _parentController.EndMoveAction();

//                _parentController = null;
//            }
//        }

//        #endregion
//    }

//    #endregion

//    #region OLD Version (Multi-Model)

//    ///// <summary>
//    ///// A control which aids in manipulating a 3D models on a 2D plane.
//    ///// </summary>
//    //public sealed class Model3DController : Control, IDisposable
//    //{
//    //    #region Fields

//    //    private ReadOnlyCollection<ModelConstruct3D> _models;
//    //    private Viewport3D _viewport;

//    //    #endregion

//    //    #region Properties

//    //    #region BoundingHeight

//    //    public static readonly DependencyProperty BoundingHeightProperty =
//    //        DependencyProperty.Register("BoundingHeight", typeof(Double), typeof(Model3DController),
//    //        new PropertyMetadata(0D));

//    //    /// <summary>
//    //    /// Gets/Sets the height of the bounding box
//    //    /// </summary>
//    //    public Double BoundingHeight
//    //    {
//    //        get { return (Double)GetValue(BoundingHeightProperty); }
//    //        set { SetValue(BoundingHeightProperty, value); }
//    //    }

//    //    #endregion

//    //    #region BoundingWidth

//    //    public static readonly DependencyProperty BoundingWidthProperty =
//    //        DependencyProperty.Register("BoundingWidth", typeof(Double), typeof(Model3DController),
//    //        new PropertyMetadata(0D));

//    //    /// <summary>
//    //    /// Gets/Sets the width of the bounding box
//    //    /// </summary>
//    //    public Double BoundingWidth
//    //    {
//    //        get { return (Double)GetValue(BoundingWidthProperty); }
//    //        set { SetValue(BoundingWidthProperty, value); }
//    //    }

//    //    #endregion

//    //    /// <summary>
//    //    /// The models being handled by this controller
//    //    /// </summary>
//    //    public ReadOnlyCollection<ModelConstruct3D> Models
//    //    {
//    //        get { return _models; }
//    //    }

//    //    public Vector3D TransformVector { get; set; }

//    //    /// <summary>
//    //    /// The plan item we are controlling 
//    //    /// TODO: Consider moving this out so that the controller could be
//    //    /// put into the framework..
//    //    /// </summary>
//    //    public IPlanItem PlanItem { get; set; }

//    //    #endregion

//    //    #region Constructor

//    //    [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline",
//    //        Justification = "Dependency properties are initialized in-line.")]
//    //    static Model3DController()
//    //    {
//    //        FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
//    //            typeof(Model3DController), new FrameworkPropertyMetadata(typeof(Model3DController)));
//    //    }

//    //    public Model3DController(IEnumerable<ModelConstruct3D> controlModels, Viewport3D viewport, IPlanItem planItem)
//    //    {
//    //        this.TransformVector = new Vector3D(1, 1, 0);
//    //        _models = controlModels.ToList().AsReadOnly();
//    //        _viewport = viewport;
//    //        this.PlanItem = planItem;

//    //        UpdatePosition();

//    //        foreach (ModelConstruct3D model in controlModels)
//    //        {
//    //            model.Updated += new EventHandler(visual_Updated);
//    //        }
//    //    }

//    //    #endregion

//    //    #region Event Handlers

//    //    private void visual_Updated(object sender, EventArgs e)
//    //    {
//    //        UpdatePosition();
//    //    }

//    //    #endregion

//    //    #region Methods

//    //    /// <summary>
//    //    /// Updates the position of this controller.
//    //    /// </summary>
//    //    public void UpdatePosition()
//    //    {
//    //        Viewport3D parentViewport = _viewport;
//    //        if (parentViewport != null)
//    //        {
//    //            Rect bounds = Rect.Empty;

//    //            foreach (ModelConstruct3D model in this.Models)
//    //            {
//    //                bounds.Union(Model3DHelper.GetModelBounds(parentViewport, model, Rect.Empty));
//    //            }


//    //            this.BoundingHeight = bounds.Height;
//    //            this.BoundingWidth = bounds.Width;
//    //            Canvas.SetLeft(this, bounds.X);
//    //            Canvas.SetTop(this, bounds.Y);
//    //        }
//    //        else
//    //        {
//    //            this.BoundingHeight = 0;
//    //            this.BoundingWidth = 0;
//    //        }
//    //    }

//    //    #region Move Action

//    //    protected object _actionPropertyResetValue;

//    //    internal void BeginMoveAction()
//    //    {
//    //        //take a copy of the raw position of the item.
//    //        _actionPropertyResetValue =
//    //            new ObjectPoint3D()
//    //            {
//    //                X = this.PlanItem.X,
//    //                Y = this.PlanItem.Y,
//    //                Z = this.PlanItem.Z,
//    //            };
//    //    }

//    //    internal void MoveVisual(Double horizDelta, Double vertDelta)
//    //    {
//    //        //get the current position
//    //        Point currPos = new Point(Canvas.GetLeft(this), Canvas.GetTop(this));

//    //        //get the new position
//    //        Point newPos = new Point(currPos.X + horizDelta, currPos.Y + vertDelta);

//    //        //apply the change to this.
//    //        Canvas.SetLeft(this, newPos.X);
//    //        Canvas.SetTop(this, newPos.Y);

//    //        //Translate and move the visual
//    //        IPlanItem planItem = this.PlanItem;

//    //        Viewport3D parentViewport = _viewport;
//    //        if (parentViewport != null)
//    //        {
//    //            Point3D pointNear;
//    //            Point3D pointFar;

//    //            if (HelixToolkit.Wpf.Viewport3DHelper.Point2DtoPoint3D(parentViewport, newPos, out pointNear, out pointFar))
//    //            {

//    //                Rect3D modelsBounds = GetModelBounds(); //nb - position is not correct in this.
//    //                Point3D cummulatedPos = GetParentPositionCummulation(planItem);

//    //                ObjectPoint3D rawPosition =
//    //                        new ObjectPoint3D()
//    //                        {
//    //                            X = planItem.X,
//    //                            Y = planItem.Y,
//    //                            Z = planItem.Z,
//    //                        };

//    //                ObjectPoint3D newPosition =
//    //                        new ObjectPoint3D()
//    //                        {
//    //                            X = pointNear.X - cummulatedPos.X,
//    //                            Y = pointNear.Y - cummulatedPos.Y,
//    //                            Z = pointNear.Z - cummulatedPos.Z,
//    //                        };





//    //                if (this.TransformVector.X == 0)
//    //                {
//    //                    newPosition.X = rawPosition.X;
//    //                }
//    //                else if (this.TransformVector.X == -1)
//    //                {
//    //                    newPosition.X -= modelsBounds.Size.X;
//    //                }


//    //                if (this.TransformVector.Y == 0)
//    //                {
//    //                    newPosition.Y = rawPosition.Y;
//    //                }
//    //                else if (this.TransformVector.Y == -1)
//    //                {
//    //                    newPosition.Y -= modelsBounds.Size.Y;
//    //                }


//    //                if (this.TransformVector.Z == 0)
//    //                {
//    //                    newPosition.Z = rawPosition.Z;
//    //                }
//    //                else if (this.TransformVector.Z == -1)
//    //                {
//    //                    newPosition.Z += modelsBounds.Size.Z;
//    //                }


//    //                //Set the new item position.
//    //                planItem.X = (Single)newPosition.X;
//    //                planItem.Y = (Single)newPosition.Y;
//    //                planItem.Z = (Single)newPosition.Z;
//    //            }
//    //        }

//    //    }

//    //    internal void EndMoveAction()
//    //    {
//    //        ObjectPoint3D origValue = (ObjectPoint3D)_actionPropertyResetValue;

//    //        //get the raw item position
//    //        ObjectPoint3D newValue =
//    //            new ObjectPoint3D()
//    //            {
//    //                X = this.PlanItem.X,
//    //                Y = this.PlanItem.Y,
//    //                Z = this.PlanItem.Z,
//    //            };


//    //        PlanogramView parentPlan = this.PlanItem.Planogram;
//    //        parentPlan.AddUndoAction(
//    //            new Galleria.Ccm.Editor.Client.Wpf.Common.UndoAction()
//    //            {
//    //                ActionType = UndoActionType.Move,
//    //                Target = this.PlanItem,
//    //                PropertyPaths =
//    //                    new String[]
//    //                                    { 
//    //                                        PlanogramSubComponentView.XProperty.Path, 
//    //                                        PlanogramSubComponentView.YProperty.Path, 
//    //                                        PlanogramSubComponentView.ZProperty.Path
//    //                                    },
//    //                OriginalValues = new Object[] { origValue.X, origValue.Y, origValue.Z },
//    //                AppliedValues = new Object[] { newValue.X, newValue.Y, newValue.Z }
//    //            });


//    //        _actionPropertyResetValue = null;

//    //    }

//    //    //internal void CancelMoveAction()
//    //    //{
//    //    //    SetValue(_actionProperty, _actionPropertyResetValue);
//    //    //    _actionProperty = null;
//    //    //    _actionPropertyResetValue = null;
//    //    //}


//    //    /// <summary>
//    //    /// Gets the bounding rect for all models 
//    //    /// specified by this controller
//    //    /// </summary>
//    //    /// <returns></returns>
//    //    private Rect3D GetModelBounds()
//    //    {
//    //        Rect3D bounds = Rect3D.Empty;

//    //        foreach (ModelConstruct3D model in this.Models)
//    //        {
//    //            bounds.Union(model.GetBounds());
//    //        }

//    //        return bounds;
//    //    }

//    //    #endregion

//    //    #endregion

//    //    #region IDisposable Members

//    //    private Boolean _isDisposed;

//    //    public void Dispose()
//    //    {
//    //        if (!_isDisposed)
//    //        {
//    //            foreach (ModelConstruct3D model in this.Models)
//    //            {
//    //                model.Updated -= visual_Updated;
//    //            }

//    //            GC.SuppressFinalize(this);
//    //            _isDisposed = true;
//    //        }
//    //    }

//    //    #endregion

//    //    #region Static Helpers

//    //    private static Point3D GetParentPositionCummulation(IPlanItem item)
//    //    {
//    //        Point3D position = new Point3D();

//    //        Transform3DGroup fullPosTransform = new Transform3DGroup();

//    //        switch (item.PlanItemType)
//    //        {
//    //            case PlanItemType.SubComponent:
//    //                {
//    //                    fullPosTransform.Children.Add(new TranslateTransform3D(item.Fixture.X, item.Fixture.Y, item.Fixture.Z));
//    //                    fullPosTransform.Children.Add(new TranslateTransform3D(item.Assembly.X, item.Assembly.Y, item.Assembly.Z));
//    //                    fullPosTransform.Children.Add(new TranslateTransform3D(item.Component.X, item.Component.Y, item.Component.Z));

//    //                    position = fullPosTransform.Transform(new Point3D());
//    //                }
//    //                break;

//    //            case PlanItemType.Component:
//    //                {
//    //                    fullPosTransform.Children.Add(new TranslateTransform3D(item.Fixture.X, item.Fixture.Y, item.Fixture.Z));
//    //                    fullPosTransform.Children.Add(new TranslateTransform3D(item.Assembly.X, item.Assembly.Y, item.Assembly.Z));

//    //                    position = fullPosTransform.Transform(new Point3D());
//    //                }
//    //                break;


//    //            case PlanItemType.Assembly:
//    //                {
//    //                    fullPosTransform.Children.Add(new TranslateTransform3D(item.Fixture.X, item.Fixture.Y, item.Fixture.Z));

//    //                    position = fullPosTransform.Transform(new Point3D());
//    //                }
//    //                break;

//    //            case PlanItemType.Fixture:
//    //                //do nothing
//    //                break;

//    //            case PlanItemType.Position:
//    //                {
//    //                    fullPosTransform.Children.Add(new TranslateTransform3D(item.Fixture.X, item.Fixture.Y, item.Fixture.Z));
//    //                    fullPosTransform.Children.Add(new TranslateTransform3D(item.Assembly.X, item.Assembly.Y, item.Assembly.Z));
//    //                    fullPosTransform.Children.Add(new TranslateTransform3D(item.Component.X, item.Component.Y, item.Component.Z));
//    //                    fullPosTransform.Children.Add(new TranslateTransform3D(item.SubComponent.X, item.SubComponent.Y, item.SubComponent.Z));

//    //                    position = fullPosTransform.Transform(new Point3D());
//    //                }
//    //                break;

//    //            case PlanItemType.Annotation:
//    //                {
//    //                    if (item.Fixture != null)
//    //                    {
//    //                        fullPosTransform.Children.Add(new TranslateTransform3D(item.Fixture.X, item.Fixture.Y, item.Fixture.Z));

//    //                        if (item.Assembly != null)
//    //                        {
//    //                            fullPosTransform.Children.Add(new TranslateTransform3D(item.Assembly.X, item.Assembly.Y, item.Assembly.Z));

//    //                            if (item.Component != null)
//    //                            {
//    //                                fullPosTransform.Children.Add(new TranslateTransform3D(item.Component.X, item.Component.Y, item.Component.Z));

//    //                                if (item.SubComponent != null)
//    //                                {
//    //                                    fullPosTransform.Children.Add(new TranslateTransform3D(item.SubComponent.X, item.SubComponent.Y, item.SubComponent.Z));
//    //                                }
//    //                            }
//    //                        }
//    //                    }

//    //                    position = fullPosTransform.Transform(new Point3D());
//    //                }
//    //                break;
//    //        }

//    //        return position;
//    //    }

//    //    #endregion
//    //}

//    #endregion

//    #region EVEN OLDER VERSION
//    //public sealed class Model3DController : Control
//    //{
//    //    #region Fields
//    //    private readonly Boolean _isModelPart3D;
//    //    #endregion

//    //    #region Properties

//    //    #region BoundingHeight

//    //    public static readonly DependencyProperty BoundingHeightProperty =
//    //        DependencyProperty.Register("BoundingHeight", typeof(Double), typeof(Model3DController),
//    //        new PropertyMetadata(0D));

//    //    /// <summary>
//    //    /// Gets/Sets the height of the bounding box
//    //    /// </summary>
//    //    public Double BoundingHeight
//    //    {
//    //        get { return (Double)GetValue(BoundingHeightProperty); }
//    //        set { SetValue(BoundingHeightProperty, value); }
//    //    }

//    //    #endregion

//    //    #region BoundingWidth

//    //    public static readonly DependencyProperty BoundingWidthProperty =
//    //        DependencyProperty.Register("BoundingWidth", typeof(Double), typeof(Model3DController),
//    //        new PropertyMetadata(0D));

//    //    /// <summary>
//    //    /// Gets/Sets the width of the bounding box
//    //    /// </summary>
//    //    public Double BoundingWidth
//    //    {
//    //        get { return (Double)GetValue(BoundingWidthProperty); }
//    //        set { SetValue(BoundingWidthProperty, value); }
//    //    }

//    //    #endregion

//    //    public ModelVisual3D Visual { get; private set; }

//    //    public Vector3D TransformVector { get; set; }

//    //    #endregion

//    //    #region Constructor

//    //    [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Dependency properties are initialized in-line.")]
//    //    static Model3DController()
//    //    {
//    //        FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
//    //            typeof(Model3DController), new FrameworkPropertyMetadata(typeof(Model3DController)));
//    //    }

//    //    public Model3DController()
//    //    {
//    //        this.TransformVector = new Vector3D(1, 1, 0);
//    //    }

//    //    public Model3DController(ModelConstruct3D visual)
//    //    {
//    //        this.TransformVector = new Vector3D(1, 1, 0);
//    //        this.Visual = visual;
//    //        UpdatePosition();

//    //        visual.Updated += new EventHandler(visual_Updated);
//    //    }

//    //    public Model3DController(ModelConstructPart3D visual)
//    //    {
//    //        _isModelPart3D = true;

//    //        this.TransformVector = new Vector3D(1, 1, 0);
//    //        this.Visual = visual;

//    //        UpdatePosition();

//    //        visual.Updated += new EventHandler(visual_Updated);
//    //    }



//    //    #endregion

//    //    #region Event Handlers

//    //    private void visual_Updated(object sender, EventArgs e)
//    //    {
//    //        UpdatePosition();
//    //    }

//    //    #endregion

//    //    #region Methods

//    //    public void UpdatePosition()
//    //    {
//    //        Viewport3D parentViewport = Framework.Controls.Wpf.Helpers.FindVisualAncestor<Viewport3D>(this.Visual);
//    //        if (parentViewport != null)
//    //        {
//    //            Rect bounds = Rect.Empty;

//    //            if (_isModelPart3D)
//    //            {
//    //                //bounds = GetVisual2DBounds(parentViewport, this.Visual);
//    //                bounds = Model3DHelper.GetModelPartBounds(parentViewport, (ModelConstructPart3D)this.Visual);
//    //            }
//    //            else
//    //            {
//    //                bounds = Model3DHelper.GetModelBounds(parentViewport, (ModelConstruct3D)this.Visual, Rect.Empty);
//    //            }


//    //            this.BoundingHeight = bounds.Height;
//    //            this.BoundingWidth = bounds.Width;
//    //            Canvas.SetLeft(this, bounds.X);
//    //            Canvas.SetTop(this, bounds.Y);
//    //        }
//    //        else
//    //        {
//    //            this.BoundingHeight = 0;
//    //            this.BoundingWidth = 0;
//    //        }
//    //    }


//    //    #region Move Action

//    //    internal void BeginMoveAction()
//    //    {
//    //        ModelConstructPart3D modelPart3D = this.Visual as ModelConstructPart3D;
//    //        if (modelPart3D != null)
//    //        {
//    //            modelPart3D.BeginAction(ModelConstructPart3D.PositionProperty);
//    //        }
//    //    }

//    //    internal void MoveVisual(Double horizDelta, Double vertDelta)
//    //    {
//    //        //get the current position
//    //        Point currPos = new Point(Canvas.GetLeft(this), Canvas.GetTop(this));

//    //        //get the new position
//    //        Point newPos = new Point(currPos.X + horizDelta, currPos.Y + vertDelta);

//    //        //apply the change to this.
//    //        Canvas.SetLeft(this, newPos.X);
//    //        Canvas.SetTop(this, newPos.Y);

//    //        //Translate and move the visual

//    //        //TODO: Need to be cleverer about which points we transform..

//    //        Viewport3D parentViewport = Framework.Controls.Wpf.Helpers.FindVisualAncestor<Viewport3D>(this.Visual);
//    //        if (parentViewport != null)
//    //        {
//    //            Point3D pointNear;
//    //            Point3D pointFar;

//    //            if (HelixToolkit.Wpf.Viewport3DHelper.Point2DtoPoint3D(parentViewport, newPos, out pointNear, out pointFar))
//    //            {
//    //                ModelConstructPart3D modelPart3D = this.Visual as ModelConstructPart3D;
//    //                if (modelPart3D != null)
//    //                {
//    //                    Point3D cummulatedPos = GetModelCummulatedPosition(VisualTreeHelper.GetParent(modelPart3D) as ModelConstruct3D);

//    //                    ObjectPoint3D newPosition =
//    //                        new ObjectPoint3D()
//    //                        {
//    //                            X = pointNear.X - cummulatedPos.X,
//    //                            Y = pointNear.Y - cummulatedPos.Y,
//    //                            Z = pointNear.Z - cummulatedPos.Z,
//    //                        };

//    //                    if (this.TransformVector.X == 0)
//    //                    {
//    //                        newPosition.X = modelPart3D.Position.X;
//    //                    }
//    //                    else if (this.TransformVector.X == -1)
//    //                    {
//    //                        newPosition.X -= modelPart3D.Size.X;
//    //                    }
//    //                    if (this.TransformVector.Y == 0)
//    //                    {
//    //                        newPosition.Y = modelPart3D.Position.Y;
//    //                    }
//    //                    else if (this.TransformVector.Y == -1)
//    //                    {
//    //                        newPosition.Y -= modelPart3D.Size.Y;
//    //                    }
//    //                    if (this.TransformVector.Z == 0)
//    //                    {
//    //                        newPosition.Z = modelPart3D.Position.Z;
//    //                    }
//    //                    else if (this.TransformVector.Z == -1)
//    //                    {
//    //                        newPosition.Z += modelPart3D.Size.Z;
//    //                    }


//    //                    modelPart3D.Position = newPosition;
//    //                }
//    //                else
//    //                {
//    //                    ModelConstruct3D construct3D = this.Visual as ModelConstruct3D;
//    //                    if (construct3D != null)
//    //                    {
//    //                        Point3D cummulatedPos = GetModelCummulatedPosition(VisualTreeHelper.GetParent(construct3D) as ModelConstruct3D);

//    //                        construct3D.Position =
//    //                            new ObjectPoint3D()
//    //                            {
//    //                                X = pointFar.X - cummulatedPos.X,
//    //                                Y = pointFar.Y - construct3D.Size.Y - cummulatedPos.Y,
//    //                                Z = construct3D.Position.Z
//    //                            };
//    //                    }

//    //                }
//    //            }
//    //        }

//    //    }

//    //    internal void EndMoveAction()
//    //    {
//    //        ModelConstructPart3D modelPart3D = this.Visual as ModelConstructPart3D;
//    //        if (modelPart3D != null)
//    //        {
//    //            modelPart3D.EndAction();
//    //        }
//    //    }

//    //    #endregion

//    //    #endregion

//    //    #region Static Helpers

//    //    private static Point3D GetModelCummulatedPosition(ModelConstruct3D model)
//    //    {
//    //        Point3D position = new Point3D();

//    //        //cycle upwards through parents
//    //        ModelConstruct3D curParent = model;
//    //        while (curParent != null)
//    //        {
//    //            position.X += curParent.Position.X;
//    //            position.Y += curParent.Position.Y;
//    //            position.Z += curParent.Position.Z;


//    //            curParent = VisualTreeHelper.GetParent(curParent) as ModelConstruct3D;
//    //        }

//    //        return position;
//    //    }



//    //    ///// <summary>
//    //    ///// Creates a bounding rectangle in screen space for the
//    //    ///// supplied visual. This will return an empty Rect if the
//    //    ///// visual has no content.
//    //    ///// </summary>
//    //    ///// <param name="visual3D"></param>
//    //    ///// <returns></returns>
//    //    //private static Rect GetVisual2DBounds(Viewport3D viewport, ModelVisual3D visual3D)
//    //    //{
//    //    //    Rect3D bounds = Rect3D.Empty;

//    //    //    //check to see if the visual has content
//    //    //    if (visual3D.Content != null)
//    //    //    {
//    //    //        bounds = visual3D.Content.Bounds;
//    //    //    }

//    //    //    Point3D point3D = bounds.Location;

//    //    //    //Create the transform into screen space
//    //    //    GeneralTransform3DTo2D transform = visual3D.TransformToAncestor(viewport);

//    //    //    //if we have been graced with a tranform by our ancestors
//    //    //    //then use it to tansform our bounding points
//    //    //    if (transform != null)
//    //    //    {
//    //    //        Point point1 = transform.Transform(point3D);

//    //    //        point3D.X += bounds.Size.X;
//    //    //        point3D.Y += bounds.Size.Y;
//    //    //        point3D.Z += bounds.Size.Z;

//    //    //        Point point2 = transform.Transform(point3D);

//    //    //        return new Rect(point1, point2);
//    //    //    }

//    //    //    return Rect.Empty;
//    //    //}

//    //    #endregion
//    //}

//    #endregion
//}
