﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2013

//#region Version History: (CCM 8.0)
//// L.Hodson
////  Created
//#endregion
//#endregion

//using System;
//using System.Linq;
//using System.Diagnostics;
//using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;
//using Galleria.Framework.Planograms.Model;
//using System.Collections.Generic;

//namespace Galleria.Ccm.Editor.Client.Wpf.PlanRender.Interfaces
//{

    

//    #region OLD

//    //public enum CapType
//    //{
//    //    None,
//    //    Right,
//    //    Top,
//    //    RightTop
//    //}

//    //public sealed class PlanPositionOrientationInfo
//    //{
//    //    #region Properties

//    //    public CapType DataCapType { get; private set; }
//    //    public ObjectPoint3D Start { get; private set; }
//    //    public ObjectPoint3D UnitDiff { get; private set; }
//    //    public ObjectRotation3D UnitRotation { get; private set; }
//    //    public ObjectPoint3D TrayDiff { get; set; }
//    //    public ObjectRotation3D TrayRotation { get; private set; }
//    //    public ObjectPoint3D BreakTopOffset { get; set; }
//    //    public ObjectPoint3D BreakUpOffset { get; set; }
//    //    public ObjectPoint3D BreakBackOffset { get; set; }
//    //    public ObjectPoint3D TopCapOffset { get; set; }
//    //    public ObjectRotation3D TopCapRotation { get; set; }

//    //    #endregion

//    //    #region Constructor

//    //    public PlanPositionOrientationInfo(IPlanPositionRenderable position, CapType orientationCapType)
//    //    {
//    //        this.DataCapType = orientationCapType;

//    //        if (position.IsMerchandisedAsTrays)
//    //        {
//    //            if (orientationCapType == CapType.Right)
//    //            {
//    //                AssignRightTrayModelOrientationValues(position);
//    //            }
//    //            else if (orientationCapType == CapType.None)
//    //            {
//    //                AssignFrontTrayModelOrientationValues(position);
//    //            }
//    //            else
//    //            {
//    //                throw new NotSupportedException();
//    //            }
//    //        }
//    //        else
//    //        {
//    //            AssignUnitOrientationValues(position, orientationCapType);

//    //        }

//    //    }

//    //    #endregion

//    //    #region Methods

//    //    /// <summary>
//    //    /// Assigns values based on the given postion and cap type
//    //    /// </summary>
//    //    /// <param name="pos"></param>
//    //    /// <param name="capType"></param>
//    //    private void AssignUnitOrientationValues(IPlanPositionRenderable pos, CapType capType)
//    //    {
//    //        ObjectSize3D finalPosSize = GetFinalPositionSize(pos);

//    //        PlanogramProductOrientationType orientationType;
//    //        ObjectPoint3D units;
//    //        ObjectPoint3D capOffset;

//    //        switch (capType)
//    //        {
//    //            default:
//    //            case CapType.None:
//    //                {
//    //                    orientationType = pos.OrientationType;
//    //                    units = new ObjectPoint3D(pos.FrontWide, pos.FrontHigh, pos.FrontDeep);
//    //                    capOffset = new ObjectPoint3D();
//    //                }
//    //                break;

//    //            case CapType.Top:
//    //                {
//    //                    PlanogramProductOrientationType mainOrientationType = pos.OrientationType;
//    //                    ObjectPoint3D mainUnitDiff = GetOrientatedSingleItemSize(pos, mainOrientationType, false);

//    //                    orientationType = GetTopCapOrientation(mainOrientationType);
//    //                    units = new ObjectPoint3D(pos.FrontTopCapWide, pos.FrontTopCapHigh, pos.FrontTopCapDeep);

//    //                    //offset this block by the height of the main front block
//    //                    capOffset =
//    //                        new ObjectPoint3D()
//    //                        {
//    //                            Y = (mainUnitDiff.Y * pos.FrontHigh)
//    //                        };
//    //                }
//    //                break;

//    //            case CapType.Right:
//    //                {
//    //                    PlanogramProductOrientationType mainOrientationType = pos.OrientationType;
//    //                    ObjectPoint3D mainUnitDiff = GetOrientatedSingleItemSize(pos, mainOrientationType, false);

//    //                    orientationType = GetRightCapOrientation(mainOrientationType);
//    //                    units = new ObjectPoint3D(pos.FrontRightWide, pos.FrontRightHigh, pos.FrontRightDeep);

//    //                    //offset this block by the width of the main front block
//    //                    capOffset =
//    //                    new ObjectPoint3D()
//    //                    {
//    //                        X = ((mainUnitDiff.X + pos.FacingSpaceX) * pos.FrontWide)
//    //                    };
//    //                }
//    //                break;

//    //            case CapType.RightTop:
//    //                {
//    //                    PlanogramProductOrientationType mainOrientationType = pos.OrientationType;
//    //                    ObjectPoint3D mainUnitDiff = GetOrientatedSingleItemSize(pos, mainOrientationType, false);

//    //                    PlanogramProductOrientationType rightOrientationType = GetRightCapOrientation(mainOrientationType);
//    //                    ObjectPoint3D rightUnitDiff = GetOrientatedSingleItemSize(pos, rightOrientationType, false);

//    //                    orientationType = GetTopCapOrientation(rightOrientationType);
//    //                    units = new ObjectPoint3D(pos.FrontTopCapRightWide, pos.FrontTopCapRightHigh, pos.FrontTopCapRightDeep);

//    //                    //offset this block by the width of the main front block
//    //                    // and the height of the right block
//    //                    capOffset =
//    //                        new ObjectPoint3D()
//    //                        {
//    //                            X = ((mainUnitDiff.X + pos.FacingSpaceX) * pos.FrontWide),

//    //                            Y = (rightUnitDiff.Y * pos.FrontRightHigh),
//    //                        };

//    //                }
//    //                break;


//    //        }


//    //        this.UnitRotation = GetRotation(orientationType);
//    //        this.UnitDiff = GetOrientatedSingleItemSize(pos, orientationType, false);

//    //        this.Start = new ObjectPoint3D()
//    //        {
//    //            X = capOffset.X,
//    //            Y = capOffset.Y,

//    //            //offset z from the front of the position.
//    //            Z = finalPosSize.Z - (capOffset.Z + (UnitDiff.Z * units.Z))
//    //        };
//    //    }

//    //    private void AssignFrontTrayModelOrientationValues(IPlanPositionRenderable pos)
//    //    {
//    //        ObjectSize3D finalPosSize = GetFinalPositionSize(pos);

//    //        Single trayThickHeight = pos.Product.TrayThickHeight;
//    //        if (trayThickHeight == 0) trayThickHeight = 0.02F;

//    //        Single trayThickWidth = pos.Product.TrayThickWidth;
//    //        if (trayThickWidth == 0) trayThickWidth = 0.02F;

//    //        Single trayThickDepth = pos.Product.TrayThickDepth;
//    //        if (trayThickDepth == 0) trayThickDepth = 0.02F;


//    //        PlanogramProductOrientationType orientationType = pos.OrientationType;
//    //        ObjectPoint3D trays = new ObjectPoint3D(pos.TraysWide, pos.TraysHigh, pos.TraysDeep);

//    //        this.UnitDiff = GetOrientatedSingleItemSize(pos, orientationType, false);
//    //        this.TrayDiff = GetOrientatedSingleItemSize(pos, orientationType, true);

//    //        this.TrayRotation = GetRotation(orientationType);
//    //        this.UnitRotation = GetRotation(orientationType);

//    //        this.Start = new ObjectPoint3D()
//    //        {
//    //            //offset z from the front of the position.
//    //            Z = finalPosSize.Z - (TrayDiff.Z * trays.Z)
//    //        };


//    //        this.BreakUpOffset = new ObjectPoint3D()
//    //        {
//    //            //offset x by the width of the main trays
//    //            X = (TrayDiff.X * trays.X),

//    //            // line up z with the front of the position
//    //            Z = finalPosSize.Z - ((UnitDiff.Z * pos.TrayBreakUpDeep))
//    //        };


//    //        this.BreakTopOffset = new ObjectPoint3D()
//    //        {
//    //            X = trayThickWidth,
//    //            Y = (TrayDiff.Y * trays.Y),

//    //            //line up z with the front of the position
//    //            Z = finalPosSize.Z - ((UnitDiff.Z * pos.TrayBreakTopDeep))
//    //        };

//    //        this.BreakBackOffset = new ObjectPoint3D()
//    //        {
//    //            X = trayThickWidth,

//    //            //line up z with the back of the front trays
//    //            Z = this.Start.Z - ((UnitDiff.Z * pos.TrayBreakBackDeep))
//    //        };

//    //        this.TopCapRotation = GetRotation(GetTopCapOrientation(orientationType));

//    //        this.TopCapOffset = new ObjectPoint3D()
//    //        {
//    //            X = trayThickWidth,
//    //            Y = (TrayDiff.Y * trays.Y) + (pos.TrayBreakTopHigh * UnitDiff.Y),
//    //            Z = finalPosSize.Z - ((UnitDiff.Y * pos.FrontTopCapDeep))
//    //        };

//    //    }

//    //    private void AssignRightTrayModelOrientationValues(IPlanPositionRenderable pos)
//    //    {
//    //        ObjectSize3D finalPosSize = GetFinalPositionSize(pos);

//    //        Single trayThickHeight = pos.Product.TrayThickHeight;
//    //        if (trayThickHeight == 0) trayThickHeight = 0.02F;

//    //        Single trayThickWidth = pos.Product.TrayThickWidth;
//    //        if (trayThickWidth == 0) trayThickWidth = 0.02F;

//    //        Single trayThickDepth = pos.Product.TrayThickDepth;
//    //        if (trayThickDepth == 0) trayThickDepth = 0.02F;

//    //        PlanogramProductOrientationType mainOrientationType = pos.OrientationType;
//    //        ObjectPoint3D mainTrays = new ObjectPoint3D(pos.TraysWide, pos.TraysHigh, pos.TraysDeep);
//    //        ObjectPoint3D mainTrayDiff = GetOrientatedSingleItemSize(pos, mainOrientationType, true);

//    //        PlanogramProductOrientationType orientationType = GetRightCapOrientation(mainOrientationType);
//    //        ObjectPoint3D trays = new ObjectPoint3D(pos.TraysRightWide, pos.TraysRightHigh, pos.TraysRightDeep);

//    //        this.UnitDiff = GetOrientatedSingleItemSize(pos, orientationType, false);
//    //        this.TrayDiff = GetOrientatedSingleItemSize(pos, orientationType, true);

//    //        this.TrayRotation = GetRotation(orientationType);
//    //        this.UnitRotation = GetRotation(orientationType);

//    //        ObjectPoint3D capOffset = new ObjectPoint3D()
//    //        {
//    //            X = (mainTrayDiff.X * mainTrays.X)
//    //        };

//    //        this.Start = new ObjectPoint3D()
//    //        {
//    //            X = capOffset.X ,
//    //            Y = capOffset.Y,

//    //            //offset z from the front of the position.
//    //            Z = finalPosSize.Z - (capOffset.Z + (TrayDiff.Z * trays.Z))
//    //        };


//    //        this.BreakUpOffset = new ObjectPoint3D()
//    //        {
//    //            //offset x by the width of the main trays
//    //            X = capOffset.X + (mainTrayDiff.X * trays.X),

//    //            // line up z with the front of the position
//    //            Z = finalPosSize.Z - ((UnitDiff.Z * pos.TrayBreakUpDeep))
//    //        };

//    //        this.BreakTopOffset = new ObjectPoint3D()
//    //        {
//    //            X = capOffset.X + trayThickWidth,
//    //            Y = (TrayDiff.Y * trays.Y),
//    //            Z = finalPosSize.Z - ((UnitDiff.Z * pos.TrayBreakTopRightDeep))
//    //        };


//    //        this.BreakBackOffset = new ObjectPoint3D()
//    //        {
//    //            //offset x by the width of the main trays
//    //            X = capOffset.X + (mainTrayDiff.X * trays.X),

//    //            //line up z with the back of the right trays
//    //            Z = this.Start.Z - ((UnitDiff.Z * pos.TrayBreakBackDeep))
//    //        };


//    //        this.TopCapRotation = GetRotation(GetTopCapOrientation(orientationType));


//    //        this.TopCapOffset = new ObjectPoint3D()
//    //        {
//    //            X = capOffset.X + trayThickWidth,
//    //            Y = (TrayDiff.Y * trays.Y) + (pos.TrayBreakTopHigh * UnitDiff.Y),
//    //            Z = finalPosSize.Z - ((UnitDiff.Y * pos.FrontTopCapRightDeep))
//    //        };

//    //    }

//    //    #endregion

//    //    #region Static helpers

//    //    /// <summary>
//    //    /// Returns the total size of the position bounding block.
//    //    /// </summary>
//    //    /// <param name="pos"></param>
//    //    /// <returns></returns>
//    //    public static ObjectSize3D GetFinalPositionSize(IPlanPositionRenderable pos)
//    //    {
//    //        Single height = 0;
//    //        Single width = 0;
//    //        Single depth = 0;

//    //        PlanogramProductOrientationType orientationType = pos.OrientationType;

//    //        if (pos.IsMerchandisedAsTrays)
//    //        {
//    //            Single trayHeight = pos.Product.TrayHeight;
//    //            Single trayWidth = pos.Product.TrayWidth;
//    //            Single trayDepth = pos.Product.TrayDepth;


//    //            ObjectPoint3D trayDiff = GetOrientatedSingleItemSize(pos, orientationType, true);

//    //            //Width
//    //            Single frontWidth = (pos.TraysWide) * trayDiff.X;
//    //            frontWidth += /*space between facings*/((pos.TraysWide - 1) * pos.FacingSpaceX);
//    //            Single frontRightWidth = 0;
//    //            if (pos.TraysRightWide > 0 && pos.TraysRightHigh > 0 && pos.TraysRightDeep > 0)
//    //            {
//    //                if (frontWidth > 0)
//    //                {
//    //                    frontWidth += pos.FacingSpaceX; //add space after last front facing.
//    //                }

//    //                frontRightWidth = pos.TraysRightWide * trayDiff.Z;
//    //                frontRightWidth += /*space between facings*/((pos.TraysRightWide - 1) * pos.FacingSpaceX);
//    //            }
//    //            width = frontWidth + frontRightWidth;

//    //            //Height
//    //            Single frontHeight = pos.TraysHigh * trayDiff.Y;
//    //            if (frontHeight > 0)
//    //            {
//    //                frontHeight += ((pos.TraysHigh - 1) * pos.FacingSpaceY);
//    //            }

//    //            Single rightHeight = pos.TraysRightHigh * trayDiff.Y;
//    //            if (rightHeight > 0)
//    //            {
//    //                rightHeight += ((pos.TraysRightHigh - 1) * pos.FacingSpaceY);
//    //            }

//    //            height = Math.Max(frontHeight, rightHeight);

//    //            //Depth
//    //            depth = Math.Max((pos.TraysDeep) * trayDiff.Z, (pos.TraysRightDeep) * trayDiff.X);
//    //        }
//    //        else
//    //        {
//    //            ObjectPoint3D unitDiff = GetOrientatedSingleItemSize(pos, orientationType, false);

//    //            ObjectPoint3D nestedUnitDiff =
//    //                GetOrientatedSize(orientationType,
//    //                pos.Product.NestingWidth,
//    //                pos.Product.NestingHeight,
//    //                pos.Product.NestingDepth);


//    //            //Front
//    //            Single frontHeight = pos.FrontHigh * unitDiff.Y;
//    //            if (frontHeight > 0)
//    //            {
//    //                if (pos.FacingSpaceY == 0 && nestedUnitDiff.Y > 0)
//    //                {
//    //                    //set front height to consider nesting value.
//    //                    frontHeight = unitDiff.Y + ((pos.FrontHigh - 1) * nestedUnitDiff.Y);
//    //                }

//    //                //add on space between facings.
//    //                frontHeight += ((pos.FrontHigh - 1) * pos.FacingSpaceY);

//    //            }

//    //            Single frontWidth = pos.FrontWide * unitDiff.X;
//    //            if (frontWidth > 0)
//    //            {
//    //                if (pos.FacingSpaceX == 0 && nestedUnitDiff.X > 0)
//    //                {
//    //                    //set front width to consider nesting value.
//    //                    frontWidth = unitDiff.X + ((pos.FrontWide - 1) * nestedUnitDiff.X);
//    //                }

//    //                //add on space between facings.
//    //                frontWidth += ((pos.FrontWide - 1) * pos.FacingSpaceX);
//    //            }


//    //            Single frontDepth = pos.FrontDeep * unitDiff.Z;
//    //            if (frontDepth > 0)
//    //            {
//    //                if (pos.FacingSpaceZ == 0 && nestedUnitDiff.Z > 0)
//    //                {
//    //                    //set front width to consider nesting value.
//    //                    frontDepth = unitDiff.Z + ((pos.FrontDeep - 1) * nestedUnitDiff.Z);
//    //                }

//    //                //add on space between facings.
//    //                frontDepth += ((pos.FrontDeep - 1) * pos.FacingSpaceZ);
//    //            }


//    //            //Front Top Cap
//    //            Single topCapHeight = 0;
//    //            if (pos.FrontTopCapHigh > 0 && pos.FrontTopCapWide > 0 && pos.FrontTopCapDeep > 0)
//    //            {
//    //                topCapHeight = pos.FrontTopCapHigh * unitDiff.Z;
//    //            }



//    //            //Right
//    //            Single frontRightHeight = 0;
//    //            if (pos.FrontRightHigh > 0 && pos.FrontRightWide > 0 && pos.FrontRightDeep > 0)
//    //            {
//    //                frontRightHeight = (pos.FrontRightHigh * unitDiff.Y);

//    //                if (frontRightHeight > 0)
//    //                {
//    //                    if (pos.FacingSpaceY == 0 && nestedUnitDiff.Y > 0)
//    //                    {
//    //                        //set right height to consider nesting value.
//    //                        frontRightHeight = unitDiff.Y + ((pos.FrontRightHigh - 1) * nestedUnitDiff.Y);
//    //                    }

//    //                    //add on space between facings.
//    //                    frontRightHeight += ((pos.FrontRightHigh - 1) * pos.FacingSpaceY);
//    //                }
//    //            }

//    //            Single frontRightWidth = 0;
//    //            if (pos.FrontRightWide > 0 && pos.FrontRightHigh > 0 && pos.FrontRightDeep > 0)
//    //            {
//    //                if (frontWidth > 0)
//    //                {
//    //                    frontWidth += pos.FacingSpaceX; //add space after last front facing.
//    //                }

//    //                frontRightWidth = pos.FrontRightWide * unitDiff.Z;
//    //                if (frontRightWidth > 0)
//    //                {
//    //                    if (pos.FacingSpaceX == 0 && nestedUnitDiff.Z > 0)
//    //                    {
//    //                        //set right width to consider nesting value.
//    //                        frontRightWidth = unitDiff.Z + ((pos.FrontRightWide - 1) * nestedUnitDiff.Z);
//    //                    }


//    //                    //add on space between facings.
//    //                    frontRightWidth += ((pos.FrontRightWide - 1) * pos.FacingSpaceX);
//    //                }

//    //            }

//    //            Single frontRightDepth = 0;
//    //            if (pos.FrontRightDeep > 0 && pos.FrontRightHigh > 0 && pos.FrontRightWide > 0)
//    //            {
//    //                frontRightDepth = (pos.FrontRightDeep * unitDiff.X);

//    //                if (frontRightDepth > 0)
//    //                {
//    //                    if (pos.FacingSpaceZ == 0 && nestedUnitDiff.X > 0)
//    //                    {
//    //                        frontRightDepth = unitDiff.X + ((pos.FrontRightDeep - 1) * nestedUnitDiff.X);
//    //                    }

//    //                    //add on space between facings.
//    //                    frontRightDepth += ((pos.FrontRightDeep - 1) * pos.FacingSpaceZ);
//    //                }
//    //            }



//    //            //Set final size values
//    //            height = Math.Max(frontHeight, frontRightHeight) + topCapHeight;

//    //            width = frontWidth + frontRightWidth;

//    //            depth = Math.Max(frontDepth, frontRightDepth);
//    //        }

//    //        return new ObjectSize3D(width, height, depth);
//    //    }

//    //    /// <summary>
//    //    /// Returns the orientated size of a single item.
//    //    /// </summary>
//    //    /// <param name="pos"></param>
//    //    /// <param name="orientationType"></param>
//    //    /// <param name="isTrayFacing"></param>
//    //    /// <returns></returns>
//    //    private static ObjectPoint3D GetOrientatedSingleItemSize(
//    //        IPlanPositionRenderable pos, PlanogramProductOrientationType orientationType, Boolean isTrayFacing)
//    //    {
//    //        if (isTrayFacing)
//    //        {
//    //            return GetOrientatedSize(orientationType,
//    //                pos.Product.TrayWidth,
//    //                pos.Product.TrayHeight,
//    //                pos.Product.TrayDepth);
//    //        }
//    //        else
//    //        {
//    //            return GetOrientatedSize(orientationType,
//    //                pos.UnitWidth,
//    //                pos.UnitHeight,
//    //                pos.UnitDepth);
//    //        }

//    //    }


//    //    public static ObjectPoint3D GetOrientatedSize(PlanogramProductOrientationType orientationType,
//    //        Single width, Single height, Single depth)
//    //    {
//    //        Single prodHeight = height;
//    //        Single prodWidth = width;
//    //        Single prodDepth = depth;

//    //        switch (orientationType)
//    //        {
//    //            default:
//    //            case PlanogramProductOrientationType.Front0:
//    //            case PlanogramProductOrientationType.Back0:
//    //            case PlanogramProductOrientationType.Front180:
//    //            case PlanogramProductOrientationType.Back180:
//    //                return new ObjectPoint3D(prodWidth, prodHeight, prodDepth);

//    //            case PlanogramProductOrientationType.Front90:
//    //            case PlanogramProductOrientationType.Back90:
//    //            case PlanogramProductOrientationType.Front270:
//    //            case PlanogramProductOrientationType.Back270:
//    //                return new ObjectPoint3D(prodHeight, prodWidth, prodDepth);


//    //            case PlanogramProductOrientationType.Right0:
//    //            case PlanogramProductOrientationType.Left0:
//    //            case PlanogramProductOrientationType.Right180:
//    //            case PlanogramProductOrientationType.Left180:
//    //                return new ObjectPoint3D(prodDepth, prodHeight, prodWidth);


//    //            case PlanogramProductOrientationType.Right90:
//    //            case PlanogramProductOrientationType.Left90:
//    //            case PlanogramProductOrientationType.Right270:
//    //            case PlanogramProductOrientationType.Left270:
//    //                return new ObjectPoint3D(prodHeight, prodDepth, prodWidth);

//    //            case PlanogramProductOrientationType.Top0:
//    //            case PlanogramProductOrientationType.Bottom0:
//    //            case PlanogramProductOrientationType.Top180:
//    //            case PlanogramProductOrientationType.Bottom180:
//    //                return new ObjectPoint3D(prodWidth, prodDepth, prodHeight);


//    //            case PlanogramProductOrientationType.Top90:
//    //            case PlanogramProductOrientationType.Bottom90:
//    //            case PlanogramProductOrientationType.Top270:
//    //            case PlanogramProductOrientationType.Bottom270:
//    //                return new ObjectPoint3D(prodDepth, prodWidth, prodHeight);

//    //        }
//    //    }

//    //    /// <summary>
//    //    /// Converts the given radian value to degrees
//    //    /// </summary>
//    //    /// <param name="radians"></param>
//    //    /// <returns></returns>
//    //    private static Single ToDegrees(Single radians)
//    //    {
//    //        return Convert.ToSingle(Math.Round(radians * (180f / Math.PI), 1));
//    //    }

//    //    /// <summary>
//    //    /// Converts the given degree value to radians.
//    //    /// </summary>
//    //    /// <param name="degrees"></param>
//    //    /// <returns></returns>
//    //    private static Single ToRadians(Single degrees)
//    //    {
//    //        return Convert.ToSingle(degrees * (Math.PI / 180f));
//    //    }


//    //    private static ObjectRotation3D GetRotation(PlanogramProductOrientationType type)
//    //    {
//    //        switch (type)
//    //        {

//    //            case PlanogramProductOrientationType.Front0:
//    //                return new ObjectRotation3D();


//    //            case PlanogramProductOrientationType.Front90:
//    //                return new ObjectRotation3D()
//    //                {
//    //                    Angle = 0,
//    //                    Slope = 0,
//    //                    Roll = ToRadians(90)
//    //                };

//    //            case PlanogramProductOrientationType.Front180:
//    //                return new ObjectRotation3D()
//    //                {
//    //                    Angle = 0,
//    //                    Slope = 0,
//    //                    Roll = ToRadians(180),
//    //                };

//    //            case PlanogramProductOrientationType.Front270:
//    //                return new ObjectRotation3D()
//    //                {
//    //                    Angle = 0,
//    //                    Slope = 0,
//    //                    Roll = ToRadians(270)
//    //                };


//    //            case PlanogramProductOrientationType.Top0:
//    //                return new ObjectRotation3D()
//    //                {
//    //                    Angle = 0,
//    //                    Slope = ToRadians(270),
//    //                    Roll = 0,
//    //                };


//    //            case PlanogramProductOrientationType.Top90:
//    //                return new ObjectRotation3D()
//    //                {
//    //                    Angle = ToRadians(90),
//    //                    Slope = 0,
//    //                    Roll = ToRadians(90),
//    //                };

//    //            case PlanogramProductOrientationType.Top180:
//    //                return new ObjectRotation3D()
//    //                {
//    //                    Angle = ToRadians(180),
//    //                    Slope = ToRadians(90),
//    //                    Roll = 0
//    //                };

//    //            case PlanogramProductOrientationType.Top270:
//    //                return new ObjectRotation3D()
//    //                {
//    //                    Angle = ToRadians(90),
//    //                    Slope = ToRadians(180),
//    //                    Roll = ToRadians(90),
//    //                };

//    //            case PlanogramProductOrientationType.Right0:
//    //                return new ObjectRotation3D()
//    //                {
//    //                    Angle = ToRadians(90),
//    //                    Slope = 0,
//    //                    Roll = 0,
//    //                };

//    //            case PlanogramProductOrientationType.Right90:
//    //                return new ObjectRotation3D()
//    //                {
//    //                    Angle = ToRadians(90),
//    //                    Slope = ToRadians(90),
//    //                    Roll = 0,
//    //                };

//    //            case PlanogramProductOrientationType.Right180:
//    //                return new ObjectRotation3D()
//    //                {
//    //                    Angle = ToRadians(270),
//    //                    Slope = 0,
//    //                    Roll = ToRadians(180),
//    //                };

//    //            case PlanogramProductOrientationType.Right270:
//    //                return new ObjectRotation3D()
//    //                {
//    //                    Angle = 0,
//    //                    Slope = ToRadians(270),
//    //                    Roll = ToRadians(270),
//    //                };

//    //            case PlanogramProductOrientationType.Left0:
//    //                return new ObjectRotation3D()
//    //                {
//    //                    Angle = ToRadians(270),
//    //                    Slope = 0,
//    //                    Roll = 0,
//    //                };

//    //            case PlanogramProductOrientationType.Left90:
//    //                return new ObjectRotation3D()
//    //                {
//    //                    Angle = ToRadians(270),
//    //                    Slope = ToRadians(270),
//    //                    Roll = 0,
//    //                };

//    //            case PlanogramProductOrientationType.Left180:
//    //                return new ObjectRotation3D()
//    //                {
//    //                    Angle = ToRadians(270),
//    //                    Slope = ToRadians(180),
//    //                    Roll = 0,
//    //                };

//    //            case PlanogramProductOrientationType.Left270:
//    //                return new ObjectRotation3D()
//    //                {
//    //                    Angle = ToRadians(270),
//    //                    Slope = ToRadians(90),
//    //                    Roll = 0,
//    //                };

//    //            case PlanogramProductOrientationType.Back0:
//    //                return new ObjectRotation3D()
//    //                {
//    //                    Angle = ToRadians(180),
//    //                    Slope = 0,
//    //                    Roll = 0,
//    //                };

//    //            case PlanogramProductOrientationType.Back90:
//    //                return new ObjectRotation3D()
//    //                {
//    //                    Angle = ToRadians(180),
//    //                    Slope = 0,
//    //                    Roll = ToRadians(270),
//    //                };

//    //            case PlanogramProductOrientationType.Back180:
//    //                return new ObjectRotation3D()
//    //                {
//    //                    Angle = ToRadians(180),
//    //                    Slope = 0,
//    //                    Roll = ToRadians(180),
//    //                };

//    //            case PlanogramProductOrientationType.Back270:
//    //                return new ObjectRotation3D()
//    //                {
//    //                    Angle = ToRadians(180),
//    //                    Slope = 0,
//    //                    Roll = ToRadians(90),
//    //                };

//    //            case PlanogramProductOrientationType.Bottom0:
//    //                return new ObjectRotation3D()
//    //                {
//    //                    Angle = 0,
//    //                    Slope = ToRadians(90),
//    //                    Roll = 0,
//    //                };

//    //            case PlanogramProductOrientationType.Bottom90:
//    //                return new ObjectRotation3D()
//    //                {
//    //                    Angle = ToRadians(270),
//    //                    Slope = 0,
//    //                    Roll = ToRadians(90),
//    //                };

//    //            case PlanogramProductOrientationType.Bottom180:
//    //                return new ObjectRotation3D()
//    //                {
//    //                    Angle = 0,
//    //                    Slope = ToRadians(270),
//    //                    Roll = ToRadians(180)
//    //                };

//    //            case PlanogramProductOrientationType.Bottom270:
//    //                return new ObjectRotation3D()
//    //                {
//    //                    Angle = ToRadians(90),
//    //                    Slope = 0,
//    //                    Roll = ToRadians(270),
//    //                };


//    //            default: throw new NotImplementedException();
//    //        }

//    //    }


//    //    public static PlanogramProductOrientationType GetRightCapOrientation(PlanogramProductOrientationType mainOrientation)
//    //    {

//    //        switch (mainOrientation)
//    //        {
//    //            case PlanogramProductOrientationType.Front0: return PlanogramProductOrientationType.Right0;
//    //            case PlanogramProductOrientationType.Front90: return PlanogramProductOrientationType.Top90;
//    //            case PlanogramProductOrientationType.Front180: return PlanogramProductOrientationType.Right180;
//    //            case PlanogramProductOrientationType.Front270: return PlanogramProductOrientationType.Bottom270;
//    //            case PlanogramProductOrientationType.Top0: return PlanogramProductOrientationType.Right270;
//    //            case PlanogramProductOrientationType.Top90: return PlanogramProductOrientationType.Back270;
//    //            case PlanogramProductOrientationType.Top180: return PlanogramProductOrientationType.Left270;
//    //            case PlanogramProductOrientationType.Top270: return PlanogramProductOrientationType.Front270;
//    //            case PlanogramProductOrientationType.Right0: return PlanogramProductOrientationType.Back0;
//    //            case PlanogramProductOrientationType.Right90: return PlanogramProductOrientationType.Top180;
//    //            case PlanogramProductOrientationType.Right180: return PlanogramProductOrientationType.Front180;
//    //            case PlanogramProductOrientationType.Right270: return PlanogramProductOrientationType.Back180;
//    //            case PlanogramProductOrientationType.Back0: return PlanogramProductOrientationType.Left0;
//    //            case PlanogramProductOrientationType.Back90: return PlanogramProductOrientationType.Top270;
//    //            case PlanogramProductOrientationType.Back180: return PlanogramProductOrientationType.Left180;
//    //            case PlanogramProductOrientationType.Back270: return PlanogramProductOrientationType.Bottom90;
//    //            case PlanogramProductOrientationType.Bottom0: return PlanogramProductOrientationType.Right90;
//    //            case PlanogramProductOrientationType.Bottom90: return PlanogramProductOrientationType.Front90;
//    //            case PlanogramProductOrientationType.Bottom180: return PlanogramProductOrientationType.Right270;
//    //            case PlanogramProductOrientationType.Bottom270: return PlanogramProductOrientationType.Back90;
//    //            case PlanogramProductOrientationType.Left0: return PlanogramProductOrientationType.Front0;
//    //            case PlanogramProductOrientationType.Left90: return PlanogramProductOrientationType.Top0;
//    //            case PlanogramProductOrientationType.Left180: return PlanogramProductOrientationType.Front180;
//    //            case PlanogramProductOrientationType.Left270: return PlanogramProductOrientationType.Bottom0;

//    //            default:
//    //                Debug.Fail("TODO");
//    //                return PlanogramProductOrientationType.Right0;

//    //        }

//    //    }

//    //    public static PlanogramProductOrientationType GetTopCapOrientation(PlanogramProductOrientationType mainOrientation)
//    //    {
//    //        switch (mainOrientation)
//    //        {
//    //            case PlanogramProductOrientationType.Front0: return PlanogramProductOrientationType.Top0;
//    //            case PlanogramProductOrientationType.Front90: return PlanogramProductOrientationType.Left90;
//    //            case PlanogramProductOrientationType.Front180: return PlanogramProductOrientationType.Top180;
//    //            case PlanogramProductOrientationType.Front270: return PlanogramProductOrientationType.Right270;
//    //            case PlanogramProductOrientationType.Top0: return PlanogramProductOrientationType.Front0;
//    //            case PlanogramProductOrientationType.Top90: return PlanogramProductOrientationType.Left180;
//    //            case PlanogramProductOrientationType.Top180: return PlanogramProductOrientationType.Front180;
//    //            case PlanogramProductOrientationType.Top270: return PlanogramProductOrientationType.Right180;
//    //            case PlanogramProductOrientationType.Right0: return PlanogramProductOrientationType.Top90;
//    //            case PlanogramProductOrientationType.Right90: return PlanogramProductOrientationType.Front90;
//    //            case PlanogramProductOrientationType.Right180: return PlanogramProductOrientationType.Bottom90;
//    //            case PlanogramProductOrientationType.Right270: return PlanogramProductOrientationType.Front270;
//    //            case PlanogramProductOrientationType.Back0: return PlanogramProductOrientationType.Bottom180;
//    //            case PlanogramProductOrientationType.Back90: return PlanogramProductOrientationType.Right90;
//    //            case PlanogramProductOrientationType.Back180: return PlanogramProductOrientationType.Bottom0;
//    //            case PlanogramProductOrientationType.Back270: return PlanogramProductOrientationType.Right270;
//    //            case PlanogramProductOrientationType.Bottom0: return PlanogramProductOrientationType.Front0;
//    //            case PlanogramProductOrientationType.Bottom90: return PlanogramProductOrientationType.Left0;
//    //            case PlanogramProductOrientationType.Bottom180: return PlanogramProductOrientationType.Back0;
//    //            case PlanogramProductOrientationType.Bottom270: return PlanogramProductOrientationType.Right0;
//    //            case PlanogramProductOrientationType.Left0: return PlanogramProductOrientationType.Top270;
//    //            case PlanogramProductOrientationType.Left90: return PlanogramProductOrientationType.Back90;
//    //            case PlanogramProductOrientationType.Left180: return PlanogramProductOrientationType.Bottom270;
//    //            case PlanogramProductOrientationType.Left270: return PlanogramProductOrientationType.Front270;


//    //            default:
//    //                Debug.Fail("TODO");
//    //                return PlanogramProductOrientationType.Top0;

//    //        }
//    //    }


//    //    /// <summary>
//    //    /// Returns a tuple per position facing. 
//    //    /// Val1 = x start, Val2 = facing width.
//    //    /// </summary>
//    //    /// <param name="pos"></param>
//    //    /// <returns></returns>
//    //    public static List<Tuple<Single, Single>> GetFacingsXAndWidth(IPlanPositionRenderable pos)
//    //    {
//    //        List<Tuple<Single, Single>> returnList = new List<Tuple<Single, Single>>();

//    //        if (pos.IsMerchandisedAsTrays)
//    //        {
//    //            Single curX = 0;

//    //            Boolean hasFront = (pos.TraysWide > 0 && pos.TraysHigh > 0 && pos.TraysDeep > 0);
//    //            Boolean hasRightCap = (pos.TraysRightWide > 0 && pos.TraysRightHigh > 0 && pos.TraysRightDeep > 0);
//    //            Boolean hasBreakUp = (pos.TrayBreakUpWide > 0 && pos.TrayBreakUpHigh > 0 && pos.TrayBreakUpDeep > 0);

//    //            //front trays
//    //            if (hasFront)
//    //            {
//    //                ObjectPoint3D frontUnitSize = GetOrientatedSingleItemSize(pos, pos.OrientationType, true);
//    //                Single frontUnitSizeX = Convert.ToSingle(frontUnitSize.X);

//    //                for (Int32 i = 0; i < pos.TraysWide; i++)
//    //                {
//    //                    returnList.Add(new Tuple<Single, Single>(curX, frontUnitSizeX));
//    //                    curX += frontUnitSizeX;

//    //                    //add spacing.
//    //                    if (i < pos.TraysWide - 1)
//    //                    {
//    //                        curX += pos.FacingSpaceX;
//    //                    }
//    //                }

//    //                if (hasRightCap || hasBreakUp)
//    //                {
//    //                    //add last spacing
//    //                    curX += pos.FacingSpaceX;
//    //                }
//    //            }


//    //            //right trays
//    //            if (hasRightCap)
//    //            {
//    //                ObjectPoint3D rgtUnitSize = GetOrientatedSingleItemSize(pos, GetRightCapOrientation(pos.OrientationType), true);
//    //                Single rgtUnitSizeX = Convert.ToSingle(rgtUnitSize.X);

//    //                for (Int32 i = 0; i < pos.FrontRightWide; i++)
//    //                {
//    //                    returnList.Add(new Tuple<Single, Single>(curX, rgtUnitSizeX));
//    //                    curX += rgtUnitSizeX;

//    //                    //add spacing.
//    //                    if (i < pos.FrontRightWide - 1)
//    //                    {
//    //                        curX += pos.FacingSpaceX;
//    //                    }
//    //                }

//    //                if (hasBreakUp)
//    //                {
//    //                    //add last spacing
//    //                    curX += pos.FacingSpaceX;
//    //                }
//    //            }


//    //            //break up
//    //            if (hasBreakUp)
//    //            {
//    //                ObjectPoint3D frontUnitSize = GetOrientatedSingleItemSize(pos, pos.OrientationType, false);
//    //                Single frontUnitSizeX = Convert.ToSingle(frontUnitSize.X);

//    //                for (Int32 i = 0; i < pos.TrayBreakUpWide; i++)
//    //                {
//    //                    returnList.Add(new Tuple<Single, Single>(curX, frontUnitSizeX));
//    //                    curX += frontUnitSizeX;

//    //                    //add spacing.
//    //                    if (i < pos.TrayBreakUpWide - 1)
//    //                    {
//    //                        curX += pos.FacingSpaceX;
//    //                    }
//    //                }
//    //            }
//    //        }
//    //        else
//    //        {
//    //            Boolean hasFront = (pos.FrontWide > 0 && pos.FrontHigh > 0 && pos.FrontDeep > 0);
//    //            Boolean hasRightCap = (pos.FrontRightWide > 0 && pos.FrontRightHigh > 0 && pos.FrontRightDeep > 0);

//    //            Single curX = 0;

//    //            //front facings
//    //            if (hasFront)
//    //            {
//    //                ObjectPoint3D frontUnitSize = GetOrientatedSingleItemSize(pos, pos.OrientationType, false);
//    //                Single frontUnitSizeX = Convert.ToSingle(frontUnitSize.X);

//    //                for (Int32 i = 0; i < pos.FrontWide; i++)
//    //                {
//    //                    returnList.Add(new Tuple<Single, Single>(curX, frontUnitSizeX));
//    //                    curX += frontUnitSizeX;

//    //                    //add spacing.
//    //                    if (i < pos.FrontWide - 1)
//    //                    {
//    //                        curX += pos.FacingSpaceX;
//    //                    }
//    //                }

//    //                if (hasRightCap)
//    //                {
//    //                    //add last spacing
//    //                    curX += pos.FacingSpaceX;
//    //                }
//    //            }

//    //            //right facings.
//    //            if (hasRightCap)
//    //            {
//    //                ObjectPoint3D rgtUnitSize = GetOrientatedSingleItemSize(pos, GetRightCapOrientation(pos.OrientationType), false);
//    //                Single rgtUnitSizeX = Convert.ToSingle(rgtUnitSize.X);

//    //                for (Int32 i = 0; i < pos.FrontRightWide; i++)
//    //                {
//    //                    returnList.Add(new Tuple<Single, Single>(curX, rgtUnitSizeX));
//    //                    curX += rgtUnitSizeX;

//    //                    //add spacing.
//    //                    if (i < pos.FrontRightWide - 1)
//    //                    {
//    //                        curX += pos.FacingSpaceX;
//    //                    }
//    //                }
//    //            }

//    //        }


//    //        return returnList;
//    //    }

//    //    #endregion
//    //}

//    #endregion
//}
