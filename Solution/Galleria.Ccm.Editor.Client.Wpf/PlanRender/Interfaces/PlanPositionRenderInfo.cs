﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: (CCM 8.0)
//// V8-24971 L.Hodson
////  Created
//#endregion
//#endregion

//using System;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.Linq;
//using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;
//using Galleria.Framework.Planograms.Model;

//namespace Galleria.Ccm.Editor.Client.Wpf.PlanRender.Interfaces
//{
//    /// <summary>
//    /// Calculates info needed to render a specific IPlanPositionRenderable
//    /// </summary>
//    public sealed class PlanPositionRenderInfo
//    {
//        #region Properties

//        public IPlanPositionRenderable PositionData { get; private set; }

//        public ObjectPoint3D MainStart { get; private set; }
//        public ObjectRotation3D MainRotation { get; private set; }
//        public ObjectSize3D MainRotatedUnitSize { get; private set; }
//        public ObjectSize3D MainSize { get; private set; }

//        public Boolean HasXBlock { get; private set; }
//        public ObjectPoint3D XStart { get; private set; }
//        public ObjectRotation3D XRotation { get; private set; }
//        public ObjectSize3D XRotatedUnitSize { get; private set; }
//        public ObjectSize3D XSize { get; private set; }

//        public Boolean HasYBlock { get; private set; }
//        public ObjectPoint3D YStart { get; private set; }
//        public ObjectRotation3D YRotation { get; private set; }
//        public ObjectSize3D YRotatedUnitSize { get; private set; }
//        public ObjectSize3D YSize { get; private set; }

//        public Boolean HasZBlock { get; private set; }
//        public ObjectPoint3D ZStart { get; private set; }
//        public ObjectRotation3D ZRotation { get; private set; }
//        public ObjectSize3D ZRotatedUnitSize { get; private set; }
//        public ObjectSize3D ZSize { get; private set; }

//        public ObjectSize3D TotalSize { get; private set; }

//        #endregion

//        #region Constructor

//        private PlanPositionRenderInfo() { }
//        private PlanPositionRenderInfo(IPlanPositionRenderable pos)
//        {
//            PositionData = pos;
//            CalculateValues();
//        }

//        #endregion

//        #region FactoryMethods

//        /// <summary>
//        /// Calculates information required to render the given position.
//        /// </summary>
//        /// <param name="pos"></param>
//        /// <returns></returns>
//        public static PlanPositionRenderInfo Calculate(IPlanPositionRenderable pos)
//        {
//            return new PlanPositionRenderInfo(pos);
//        }

//        #endregion

//        #region Methods

//        private void CalculateValues()
//        {
//            IPlanPositionRenderable pos = this.PositionData;
//            ObjectPoint3D units;

//            #region Main Block

//            units = new ObjectPoint3D(pos.FacingsWide, pos.FacingsHigh, pos.FacingsDeep);

//            this.MainRotation = GetRotation(pos.OrientationType);

//            if (pos.IsMerchandisedAsTrays)
//            {
//                this.MainRotatedUnitSize = GetOrientatedSize(pos.OrientationType, pos.Product.TrayWidth, pos.Product.TrayHeight, pos.Product.TrayDepth);
//            }
//            else
//            {
//                this.MainRotatedUnitSize = GetOrientatedSize(pos.OrientationType, pos.UnitWidth, pos.UnitHeight, pos.UnitDepth);
//            }

//            this.MainSize =
//                new ObjectSize3D()
//                {
//                    X = (units.X * this.MainRotatedUnitSize.X) + ((units.X-1) * pos.FacingSpaceX),
//                    Y = (units.Y * this.MainRotatedUnitSize.Y) + ((units.Y - 1) * pos.FacingSpaceY),
//                    Z = (units.Z * this.MainRotatedUnitSize.Z) + ((units.Z - 1) * pos.FacingSpaceZ)
//                };

//            #endregion

//            #region X Block
//            units = new ObjectPoint3D(pos.FacingsXWide, pos.FacingsXHigh, pos.FacingsXDeep);

//            if (units.X > 0 && units.Y > 0 && units.Z > 0)
//            {
//                this.HasXBlock = true;
//                this.XRotation = GetRotation(pos.OrientationTypeX);

//                if (pos.IsXMerchandisedAsTrays)
//                {
//                    this.XRotatedUnitSize = GetOrientatedSize(pos.OrientationTypeX, pos.Product.TrayWidth, pos.Product.TrayHeight, pos.Product.TrayDepth);
//                }
//                else
//                {
//                    this.XRotatedUnitSize = GetOrientatedSize(pos.OrientationTypeX, pos.UnitXWidth, pos.UnitXHeight, pos.UnitXDepth);
//                }

//                this.XSize =
//                    new ObjectSize3D()
//                    {
//                        X = (units.X * this.XRotatedUnitSize.X) + ((units.X - 1) * pos.FacingSpaceX),
//                        Y = (units.Y * this.XRotatedUnitSize.Y) + ((units.Y - 1) * pos.FacingSpaceY),
//                        Z = (units.Z * this.XRotatedUnitSize.Z) + ((units.Z - 1) * pos.FacingSpaceZ)
//                    };
//            }
//            else
//            {
//                this.XRotation = new ObjectRotation3D();
//                this.XRotatedUnitSize = new ObjectSize3D();
//                this.XSize = new ObjectSize3D();
//            }
//            #endregion

//            #region Y Block

//            units = new ObjectPoint3D(pos.FacingsYWide, pos.FacingsYHigh, pos.FacingsYDeep);
//            if (units.X > 0 && units.Y > 0 && units.Z > 0)
//            {
//                this.HasYBlock = true;
//                this.YRotation = GetRotation(pos.OrientationTypeY);

//                if (pos.IsYMerchandisedAsTrays)
//                {
//                    this.YRotatedUnitSize = GetOrientatedSize(pos.OrientationTypeY, pos.Product.TrayWidth, pos.Product.TrayHeight, pos.Product.TrayDepth);
//                }
//                else
//                {
//                    this.YRotatedUnitSize = GetOrientatedSize(pos.OrientationTypeY, pos.UnitYWidth, pos.UnitYHeight, pos.UnitYDepth);
//                }


//                this.YSize =
//                    new ObjectSize3D()
//                    {
//                        X = (units.X * this.YRotatedUnitSize.X) + ((units.X - 1) * pos.FacingSpaceX),
//                        Y = (units.Y * this.YRotatedUnitSize.Y) + ((units.Y - 1) * pos.FacingSpaceY),
//                        Z = (units.Z * this.YRotatedUnitSize.Z) + ((units.Z - 1) * pos.FacingSpaceZ)
//                    };
//            }
//            else
//            {
//                this.YRotation = new ObjectRotation3D();
//                this.YRotatedUnitSize = new ObjectSize3D();
//                this.YSize = new ObjectSize3D();
//            }
//            #endregion

//            #region Z Block

//            units = new ObjectPoint3D(pos.FacingsZWide, pos.FacingsZHigh, pos.FacingsZDeep);

//            if (units.X > 0 && units.Y > 0 && units.Z > 0)
//            {
//                this.HasZBlock = true;
//                this.ZRotation = GetRotation(pos.OrientationTypeZ);

//                if (pos.IsZMerchandisedAsTrays)
//                {
//                    this.ZRotatedUnitSize = GetOrientatedSize(pos.OrientationTypeZ, pos.Product.TrayWidth, pos.Product.TrayHeight, pos.Product.TrayDepth);
//                }
//                else
//                {
//                    this.ZRotatedUnitSize = GetOrientatedSize(pos.OrientationTypeZ, pos.UnitZWidth, pos.UnitZHeight, pos.UnitZDepth);
//                }


//                this.ZSize =
//                    new ObjectSize3D()
//                    {
//                        X = (units.X * this.ZRotatedUnitSize.X) + ((units.X - 1) * pos.FacingSpaceX),
//                        Y = (units.Y * this.ZRotatedUnitSize.Y) + ((units.Y - 1) * pos.FacingSpaceY),
//                        Z = (units.Z * this.ZRotatedUnitSize.Z) + ((units.Z - 1) * pos.FacingSpaceZ)
//                    };
//            }
//            else
//            {
//                this.ZRotation = new ObjectRotation3D();
//                this.ZRotatedUnitSize = new ObjectSize3D();
//                this.ZSize = new ObjectSize3D();
//            }
//            #endregion

//            #region TotalSize

//            ObjectSize3D finalSize = 
//                new ObjectSize3D()
//                {
//                    X = new Single[] { MainSize.X, YSize.X, ZSize.X }.Max(),
//                    Y = new Single[] { MainSize.Y, XSize.Y + ZSize.Y }.Max(),
//                    Z = new Single[] { MainSize.Z, XSize.Z, YSize.Z }.Max()
//                };
//            if (HasXBlock)
//            {
//                finalSize.X += XSize.X + pos.FacingSpaceX;
//            }
//            if (HasYBlock)
//            {
//                finalSize.Y += YSize.Y + pos.FacingSpaceY;
//            }
//            if (HasZBlock)
//            {
//                finalSize.Z += ZSize.Z + pos.FacingSpaceZ;
//            }
//            this.TotalSize = finalSize;

//            #endregion

//            #region Block start positions

//            //set the start position of each block
//            ObjectPoint3D mainStart = new ObjectPoint3D(0, 0, TotalSize.Z - MainSize.Z);
//            if (this.HasXBlock && pos.IsXPlacedLeft)
//            {
//                mainStart.X += XSize.X;
//            }
//            if (HasYBlock && pos.IsYPlacedBottom)
//            {
//                mainStart.Y += YSize.Y;
//            }
//            if (HasZBlock && pos.IsZPlacedFront)
//            {
//                mainStart.Z -= ZSize.Z;
//            }
//            this.MainStart = mainStart;

//            if (HasXBlock)
//            {
//                ObjectPoint3D xStart =
//                    new ObjectPoint3D()
//                    {
//                        X = 0,
//                        Y = mainStart.Y,
//                        Z = (mainStart.Z + MainSize.Z) - XSize.Z //line up with front.
//                    };

//                if (!pos.IsXPlacedLeft)
//                {
//                    xStart.X = mainStart.X + MainSize.X;
//                }

//                this.XStart = xStart;
//            }

//            if (HasYBlock)
//            {
//                ObjectPoint3D yStart =
//                    new ObjectPoint3D()
//                    {
//                        X = mainStart.X,
//                        Y = 0,
//                        Z = (mainStart.Z + MainSize.Z) - YSize.Z
//                    };

//                if (!pos.IsYPlacedBottom)
//                {
//                    yStart.Y = mainStart.Y + MainSize.Y;
//                }

//                this.YStart = yStart;
//            }

//            if (HasZBlock)
//            {
//                ObjectPoint3D zStart =
//                    new ObjectPoint3D()
//                    {
//                        X = mainStart.X,
//                        Y = mainStart.Y,
//                        Z = TotalSize.Z -ZSize.Z
//                    };

//                if (!pos.IsZPlacedFront)
//                {
//                    zStart.Z = TotalSize.Z - MainSize.Z - ZSize.Z;
//                }
//                this.ZStart = zStart;
//            }

//            #endregion
//        }

//        #endregion

//        #region Static Helpers

//        /// <summary>
//        /// Returns the actual angle slope roll rotation for the given orientation type.
//        /// </summary>
//        /// <param name="type"></param>
//        /// <returns></returns>
//        public static ObjectRotation3D GetRotation(PlanogramProductOrientationType type)
//        {
//            switch (type)
//            {

//                case PlanogramProductOrientationType.Front0:
//                    return new ObjectRotation3D();


//                case PlanogramProductOrientationType.Front90:
//                    return new ObjectRotation3D()
//                    {
//                        Angle = 0,
//                        Slope = 0,
//                        Roll = ToRadians(90)
//                    };

//                case PlanogramProductOrientationType.Front180:
//                    return new ObjectRotation3D()
//                    {
//                        Angle = 0,
//                        Slope = 0,
//                        Roll = ToRadians(180),
//                    };

//                case PlanogramProductOrientationType.Front270:
//                    return new ObjectRotation3D()
//                    {
//                        Angle = 0,
//                        Slope = 0,
//                        Roll = ToRadians(270)
//                    };


//                case PlanogramProductOrientationType.Top0:
//                    return new ObjectRotation3D()
//                    {
//                        Angle = 0,
//                        Slope = ToRadians(270),
//                        Roll = 0,
//                    };


//                case PlanogramProductOrientationType.Top90:
//                    return new ObjectRotation3D()
//                    {
//                        Angle = ToRadians(90),
//                        Slope = 0,
//                        Roll = ToRadians(90),
//                    };

//                case PlanogramProductOrientationType.Top180:
//                    return new ObjectRotation3D()
//                    {
//                        Angle = ToRadians(180),
//                        Slope = ToRadians(90),
//                        Roll = 0
//                    };

//                case PlanogramProductOrientationType.Top270:
//                    return new ObjectRotation3D()
//                    {
//                        Angle = ToRadians(90),
//                        Slope = ToRadians(180),
//                        Roll = ToRadians(90),
//                    };

//                case PlanogramProductOrientationType.Right0:
//                    return new ObjectRotation3D()
//                    {
//                        Angle = ToRadians(90),
//                        Slope = 0,
//                        Roll = 0,
//                    };

//                case PlanogramProductOrientationType.Right90:
//                    return new ObjectRotation3D()
//                    {
//                        Angle = ToRadians(90),
//                        Slope = ToRadians(90),
//                        Roll = 0,
//                    };

//                case PlanogramProductOrientationType.Right180:
//                    return new ObjectRotation3D()
//                    {
//                        Angle = ToRadians(270),
//                        Slope = 0,
//                        Roll = ToRadians(180),
//                    };

//                case PlanogramProductOrientationType.Right270:
//                    return new ObjectRotation3D()
//                    {
//                        Angle = 0,
//                        Slope = ToRadians(270),
//                        Roll = ToRadians(270),
//                    };

//                case PlanogramProductOrientationType.Left0:
//                    return new ObjectRotation3D()
//                    {
//                        Angle = ToRadians(270),
//                        Slope = 0,
//                        Roll = 0,
//                    };

//                case PlanogramProductOrientationType.Left90:
//                    return new ObjectRotation3D()
//                    {
//                        Angle = ToRadians(270),
//                        Slope = ToRadians(270),
//                        Roll = 0,
//                    };

//                case PlanogramProductOrientationType.Left180:
//                    return new ObjectRotation3D()
//                    {
//                        Angle = ToRadians(270),
//                        Slope = ToRadians(180),
//                        Roll = 0,
//                    };

//                case PlanogramProductOrientationType.Left270:
//                    return new ObjectRotation3D()
//                    {
//                        Angle = ToRadians(270),
//                        Slope = ToRadians(90),
//                        Roll = 0,
//                    };

//                case PlanogramProductOrientationType.Back0:
//                    return new ObjectRotation3D()
//                    {
//                        Angle = ToRadians(180),
//                        Slope = 0,
//                        Roll = 0,
//                    };

//                case PlanogramProductOrientationType.Back90:
//                    return new ObjectRotation3D()
//                    {
//                        Angle = ToRadians(180),
//                        Slope = 0,
//                        Roll = ToRadians(270),
//                    };

//                case PlanogramProductOrientationType.Back180:
//                    return new ObjectRotation3D()
//                    {
//                        Angle = ToRadians(180),
//                        Slope = 0,
//                        Roll = ToRadians(180),
//                    };

//                case PlanogramProductOrientationType.Back270:
//                    return new ObjectRotation3D()
//                    {
//                        Angle = ToRadians(180),
//                        Slope = 0,
//                        Roll = ToRadians(90),
//                    };

//                case PlanogramProductOrientationType.Bottom0:
//                    return new ObjectRotation3D()
//                    {
//                        Angle = 0,
//                        Slope = ToRadians(90),
//                        Roll = 0,
//                    };

//                case PlanogramProductOrientationType.Bottom90:
//                    return new ObjectRotation3D()
//                    {
//                        Angle = ToRadians(270),
//                        Slope = 0,
//                        Roll = ToRadians(90),
//                    };

//                case PlanogramProductOrientationType.Bottom180:
//                    return new ObjectRotation3D()
//                    {
//                        Angle = 0,
//                        Slope = ToRadians(270),
//                        Roll = ToRadians(180)
//                    };

//                case PlanogramProductOrientationType.Bottom270:
//                    return new ObjectRotation3D()
//                    {
//                        Angle = ToRadians(90),
//                        Slope = 0,
//                        Roll = ToRadians(270),
//                    };


//                default: throw new NotImplementedException();
//            }

//        }

//        /// <summary>
//        /// Converts the given degree value to radians.
//        /// </summary>
//        /// <param name="degrees"></param>
//        /// <returns></returns>
//        private static Single ToRadians(Single degrees)
//        {
//            return Convert.ToSingle(degrees * (Math.PI / 180f));
//        }

//        /// <summary>
//        /// Returns the item size orientated to the given orientation type.
//        /// </summary>
//        public static ObjectSize3D GetOrientatedSize(PlanogramProductOrientationType orientationType,
//            Single width, Single height, Single depth)
//        {
//            Single prodHeight = height;
//            Single prodWidth = width;
//            Single prodDepth = depth;

//            switch (orientationType)
//            {
//                default:
//                case PlanogramProductOrientationType.Front0:
//                case PlanogramProductOrientationType.Back0:
//                case PlanogramProductOrientationType.Front180:
//                case PlanogramProductOrientationType.Back180:
//                    return new ObjectSize3D(prodWidth, prodHeight, prodDepth);

//                case PlanogramProductOrientationType.Front90:
//                case PlanogramProductOrientationType.Back90:
//                case PlanogramProductOrientationType.Front270:
//                case PlanogramProductOrientationType.Back270:
//                    return new ObjectSize3D(prodHeight, prodWidth, prodDepth);


//                case PlanogramProductOrientationType.Right0:
//                case PlanogramProductOrientationType.Left0:
//                case PlanogramProductOrientationType.Right180:
//                case PlanogramProductOrientationType.Left180:
//                    return new ObjectSize3D(prodDepth, prodHeight, prodWidth);


//                case PlanogramProductOrientationType.Right90:
//                case PlanogramProductOrientationType.Left90:
//                case PlanogramProductOrientationType.Right270:
//                case PlanogramProductOrientationType.Left270:
//                    return new ObjectSize3D(prodHeight, prodDepth, prodWidth);

//                case PlanogramProductOrientationType.Top0:
//                case PlanogramProductOrientationType.Bottom0:
//                case PlanogramProductOrientationType.Top180:
//                case PlanogramProductOrientationType.Bottom180:
//                    return new ObjectSize3D(prodWidth, prodDepth, prodHeight);


//                case PlanogramProductOrientationType.Top90:
//                case PlanogramProductOrientationType.Bottom90:
//                case PlanogramProductOrientationType.Top270:
//                case PlanogramProductOrientationType.Bottom270:
//                    return new ObjectSize3D(prodDepth, prodWidth, prodHeight);

//            }
//        }

//        /// <summary>
//        /// Gets the suggested right cap orientation for the given main 
//        /// </summary>
//        /// <param name="mainOrientation"></param>
//        /// <returns></returns>
//        public static PlanogramProductOrientationType SuggestRightCapOrientation(PlanogramProductOrientationType mainOrientation)
//        {

//            switch (mainOrientation)
//            {
//                case PlanogramProductOrientationType.Front0: return PlanogramProductOrientationType.Right0;
//                case PlanogramProductOrientationType.Front90: return PlanogramProductOrientationType.Top90;
//                case PlanogramProductOrientationType.Front180: return PlanogramProductOrientationType.Right180;
//                case PlanogramProductOrientationType.Front270: return PlanogramProductOrientationType.Bottom270;
//                case PlanogramProductOrientationType.Top0: return PlanogramProductOrientationType.Right270;
//                case PlanogramProductOrientationType.Top90: return PlanogramProductOrientationType.Back270;
//                case PlanogramProductOrientationType.Top180: return PlanogramProductOrientationType.Left270;
//                case PlanogramProductOrientationType.Top270: return PlanogramProductOrientationType.Front270;
//                case PlanogramProductOrientationType.Right0: return PlanogramProductOrientationType.Back0;
//                case PlanogramProductOrientationType.Right90: return PlanogramProductOrientationType.Top180;
//                case PlanogramProductOrientationType.Right180: return PlanogramProductOrientationType.Front180;
//                case PlanogramProductOrientationType.Right270: return PlanogramProductOrientationType.Back180;
//                case PlanogramProductOrientationType.Back0: return PlanogramProductOrientationType.Left0;
//                case PlanogramProductOrientationType.Back90: return PlanogramProductOrientationType.Top270;
//                case PlanogramProductOrientationType.Back180: return PlanogramProductOrientationType.Left180;
//                case PlanogramProductOrientationType.Back270: return PlanogramProductOrientationType.Bottom90;
//                case PlanogramProductOrientationType.Bottom0: return PlanogramProductOrientationType.Right90;
//                case PlanogramProductOrientationType.Bottom90: return PlanogramProductOrientationType.Front90;
//                case PlanogramProductOrientationType.Bottom180: return PlanogramProductOrientationType.Right270;
//                case PlanogramProductOrientationType.Bottom270: return PlanogramProductOrientationType.Back90;
//                case PlanogramProductOrientationType.Left0: return PlanogramProductOrientationType.Front0;
//                case PlanogramProductOrientationType.Left90: return PlanogramProductOrientationType.Top0;
//                case PlanogramProductOrientationType.Left180: return PlanogramProductOrientationType.Front180;
//                case PlanogramProductOrientationType.Left270: return PlanogramProductOrientationType.Bottom0;

//                default:
//                    Debug.Fail("TODO");
//                    return PlanogramProductOrientationType.Right0;

//            }

//        }

//        /// <summary>
//        /// Gets the suggested top cap orientation for the given main.
//        /// </summary>
//        /// <param name="mainOrientation"></param>
//        /// <returns></returns>
//        public static PlanogramProductOrientationType SuggestTopCapOrientation(PlanogramProductOrientationType mainOrientation)
//        {
//            switch (mainOrientation)
//            {
//                case PlanogramProductOrientationType.Front0: return PlanogramProductOrientationType.Top0;
//                case PlanogramProductOrientationType.Front90: return PlanogramProductOrientationType.Left90;
//                case PlanogramProductOrientationType.Front180: return PlanogramProductOrientationType.Top180;
//                case PlanogramProductOrientationType.Front270: return PlanogramProductOrientationType.Right270;
//                case PlanogramProductOrientationType.Top0: return PlanogramProductOrientationType.Front0;
//                case PlanogramProductOrientationType.Top90: return PlanogramProductOrientationType.Left180;
//                case PlanogramProductOrientationType.Top180: return PlanogramProductOrientationType.Front180;
//                case PlanogramProductOrientationType.Top270: return PlanogramProductOrientationType.Right180;
//                case PlanogramProductOrientationType.Right0: return PlanogramProductOrientationType.Top90;
//                case PlanogramProductOrientationType.Right90: return PlanogramProductOrientationType.Front90;
//                case PlanogramProductOrientationType.Right180: return PlanogramProductOrientationType.Bottom90;
//                case PlanogramProductOrientationType.Right270: return PlanogramProductOrientationType.Front270;
//                case PlanogramProductOrientationType.Back0: return PlanogramProductOrientationType.Bottom180;
//                case PlanogramProductOrientationType.Back90: return PlanogramProductOrientationType.Right90;
//                case PlanogramProductOrientationType.Back180: return PlanogramProductOrientationType.Bottom0;
//                case PlanogramProductOrientationType.Back270: return PlanogramProductOrientationType.Right270;
//                case PlanogramProductOrientationType.Bottom0: return PlanogramProductOrientationType.Front0;
//                case PlanogramProductOrientationType.Bottom90: return PlanogramProductOrientationType.Left0;
//                case PlanogramProductOrientationType.Bottom180: return PlanogramProductOrientationType.Back0;
//                case PlanogramProductOrientationType.Bottom270: return PlanogramProductOrientationType.Right0;
//                case PlanogramProductOrientationType.Left0: return PlanogramProductOrientationType.Top270;
//                case PlanogramProductOrientationType.Left90: return PlanogramProductOrientationType.Back90;
//                case PlanogramProductOrientationType.Left180: return PlanogramProductOrientationType.Bottom270;
//                case PlanogramProductOrientationType.Left270: return PlanogramProductOrientationType.Front270;


//                default:
//                    Debug.Fail("TODO");
//                    return PlanogramProductOrientationType.Top0;

//            }
//        }

//        /// <summary>
//        /// Returns a tuple per position facing. 
//        /// Val1 = x start, Val2 = facing width.
//        /// </summary>
//        /// <param name="pos"></param>
//        /// <returns></returns>
//        public static List<Tuple<Single, Single>> GetFacingsXAndWidth(IPlanPositionRenderable pos)
//        {
//            List<Tuple<Single, Single>> returnList = new List<Tuple<Single, Single>>();

//            PlanPositionRenderInfo renderInfo = PlanPositionRenderInfo.Calculate(pos);
//            Single unitSizeX;
//            Single curX = 0;

//            //left x block.
//            if (renderInfo.HasXBlock && pos.IsXPlacedLeft)
//            {
//                unitSizeX = renderInfo.XRotatedUnitSize.X;
//                for (Int32 i = 0; i < pos.FacingsXWide; i++)
//                {
//                    returnList.Add(new Tuple<Single, Single>(curX, unitSizeX));
//                    curX += unitSizeX;

//                    //add spacing.
//                    if (i <= pos.FacingsXWide - 1)
//                    {
//                        curX += pos.FacingSpaceX;
//                    }
//                }
//            }

//            //main
//            unitSizeX = renderInfo.MainRotatedUnitSize.X;
//            for (Int32 i = 0; i < pos.FacingsWide; i++)
//            {
//                returnList.Add(new Tuple<Single, Single>(curX, unitSizeX));
//                curX += unitSizeX;

//                //add spacing.
//                if (i < pos.FacingsWide - 1)
//                {
//                    curX += pos.FacingSpaceX;
//                }
//            }

//            //right x block
//            if (renderInfo.HasXBlock && !pos.IsXPlacedLeft)
//            {
//                //add another spacing
//                curX += pos.FacingSpaceX;

//                unitSizeX = renderInfo.XRotatedUnitSize.X;
//                for (Int32 i = 0; i < pos.FacingsXWide; i++)
//                {
//                    returnList.Add(new Tuple<Single, Single>(curX, unitSizeX));
//                    curX += unitSizeX;

//                    //add spacing.
//                    if (i < pos.FacingsXWide - 1)
//                    {
//                        curX += pos.FacingSpaceX;
//                    }
//                }
//            }

//            return returnList;
//        }

//        #endregion
//    }
//}
