﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Windows.Controls;
//using System.Windows;
//using System.Windows.Media.Media3D;
//using System.Diagnostics.CodeAnalysis;
//using System.Windows.Controls.Primitives;
//using HelixToolkit.Wpf;
//using System.Windows.Input;

//namespace Galleria.Ccm.Editor.Client.Wpf.PlanRender
//{
//    /// <summary>
//    ///  Represents a control used to change the position of a camera
//    /// </summary>
//    [TemplatePart(Name=_partLeftArrow, Type=typeof(RepeatButton))]
//    [TemplatePart(Name = _partUpArrow, Type = typeof(RepeatButton))]
//    [TemplatePart(Name = _partUpArrow, Type = typeof(RepeatButton))]
//    [TemplatePart(Name = _partDownArrow, Type = typeof(RepeatButton))]
//    public sealed class CameraPositionController: Control
//    {
//        #region Constants
//        const String _partLeftArrow = "PART_LeftArrow";
//        const String _partRightArrow = "PART_RightArrow";
//        const String _partUpArrow = "PART_UpArrow";
//        const String _partDownArrow = "PART_DownArrow";
//        #endregion

//        #region Fields
//        private RepeatButton _leftArrow;
//        private RepeatButton _rightArrow;
//        private RepeatButton _upArrow;
//        private RepeatButton _downArrow;
//        #endregion

//        #region Properties

//        #region Viewport

//        public static readonly DependencyProperty ViewportProperty =
//            DependencyProperty.RegisterAttached("Viewport", typeof(HelixViewport3D), typeof(CameraPositionController),
//            new PropertyMetadata(null, OnViewportPropertyChanged));

//        /// <summary>
//        /// Gets/Sets the viewport this controller is attached to.
//        /// </summary>
//        public HelixViewport3D Viewport
//        {
//            get { return (HelixViewport3D)GetValue(ViewportProperty); }
//            set { SetValue(ViewportProperty, value); }
//        }

//        private static void OnViewportPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
//        {
//            CameraPositionController senderControl = (CameraPositionController)obj;
//        }

//        #endregion

//        #endregion

//        #region Constructor

//        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline",
//            Justification = "Dependency properties are initialized in-line.")]
//        static CameraPositionController()
//        {
//            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
//                typeof(CameraPositionController), new FrameworkPropertyMetadata(typeof(CameraPositionController)));
//        }

//        public CameraPositionController() { }

//        #endregion

//        #region Overrides

//        public override void OnApplyTemplate()
//        {
//            base.OnApplyTemplate();

//            _leftArrow = this.GetTemplateChild(_partLeftArrow) as RepeatButton;
//            _leftArrow.Click += LeftArrow_Click;

//            _rightArrow = this.GetTemplateChild(_partRightArrow) as RepeatButton;
//            _rightArrow.Click += RightArrow_Click;

//            _upArrow = this.GetTemplateChild(_partUpArrow) as RepeatButton;
//            _upArrow.Click += UpArrow_Click;

//            _downArrow = this.GetTemplateChild(_partDownArrow) as RepeatButton;
//            _downArrow.Click += DownArrow_Click;

//        }

//        private void LeftArrow_Click(object sender, RoutedEventArgs e)
//        {
//            //       case Key.Right:
//            //           this.AddPanForce(5 * f * this.LeftRightPanSensitivity, 0);
//            //           e.Handled = true;
//            //           break;

//            if (this.Viewport != null)
//            {
//                if (this.Viewport.CameraController != null)
//                {
//                    Boolean control = Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl);
//                    Double f = 0.5;//control ? 0.25 : 1;

//                    Double panSensitivity = this.Viewport.CameraController.LeftRightPanSensitivity;

//                    this.Viewport.CameraController.AddPanForce(5 * f * panSensitivity, 0);
//                }
//            }
//        }

//        private void RightArrow_Click(object sender, RoutedEventArgs e)
//        {
//            //case Key.Left:
//            //           this.AddPanForce(-5 * f * this.LeftRightPanSensitivity, 0);
//            //           e.Handled = true;
//            //           break;

//            if (this.Viewport != null)
//            {
//                if (this.Viewport.CameraController != null)
//                {
//                    Boolean control = Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl);
//                    Double f = 0.5;//control ? 0.25 : 1;

//                    Double panSensitivity = this.Viewport.CameraController.LeftRightPanSensitivity;

//                    this.Viewport.CameraController.AddPanForce(-5 * f * panSensitivity, 0);
//                }
//            }
//        }

//        private void UpArrow_Click(object sender, RoutedEventArgs e)
//        {
//            //       case Key.Down:
//            //           this.AddPanForce(0, 5 * f * this.UpDownPanSensitivity);
//            //           e.Handled = true;
//            //           break;

//            if (this.Viewport != null)
//            {
//                if (this.Viewport.CameraController != null)
//                {
//                    Boolean control = Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl);
//                    Double f = 0.5;//control ? 0.25 : 1;

//                    Double panSensitivity = this.Viewport.CameraController.UpDownPanSensitivity;

//                    this.Viewport.CameraController.AddPanForce(0, 5 * f * panSensitivity);
//                }
//            }

//        }

//        private void DownArrow_Click(object sender, RoutedEventArgs e)
//        {

//            //       case Key.Up:
//            //           this.AddPanForce(0, -5 * f * this.UpDownPanSensitivity);
//            //           e.Handled = true;
//            //           break;


//            if (this.Viewport != null)
//            {
//                if (this.Viewport.CameraController != null)
//                {
//                    Boolean control = Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl);
//                    Double f = 0.5;//control ? 0.25 : 1;

//                    Double panSensitivity = this.Viewport.CameraController.UpDownPanSensitivity;

//                    this.Viewport.CameraController.AddPanForce(0, -5 * f * panSensitivity);
//                }
//            }
//        }


//        #endregion

//    }
//}
