﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (V8 1.0)
// V8-25395 : A.Probyn
//  Created
#endregion
#endregion

using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Model;
using System.Collections.Generic;
using System.IO;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.ProductLibraryMaintenance
{
    /// <summary>
    /// Interaction logic for ProductLibraryMaintenanceBackstageOpen.xaml
    /// </summary>
    public partial class ProductLibraryMaintenanceBackstageOpen : UserControl
    {

        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(ProductLibraryMaintenanceViewModel), typeof(ProductLibraryMaintenanceBackstageOpen),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public ProductLibraryMaintenanceViewModel ViewModel
        {
            get { return (ProductLibraryMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ProductLibraryMaintenanceBackstageOpen senderControl = (ProductLibraryMaintenanceBackstageOpen)obj;

            if (e.OldValue != null)
            {
                ProductLibraryMaintenanceViewModel oldModel = (ProductLibraryMaintenanceViewModel)e.OldValue;

            }

            if (e.NewValue != null)
            {
                ProductLibraryMaintenanceViewModel newModel = (ProductLibraryMaintenanceViewModel)e.NewValue;

            }
            senderControl.UpdateSearchResults();
        }


        #endregion

        #region SearchTextProperty

        public static readonly DependencyProperty SearchTextProperty =
            DependencyProperty.Register("SearchText", typeof(String), typeof(ProductLibraryMaintenanceBackstageOpen),
            new PropertyMetadata(String.Empty, OnSearchTextPropertyChanged));

        /// <summary>
        /// Gets/Sets the criteria to search product libraries for
        /// </summary>
        public String SearchText
        {
            get { return (String)GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }

        private static void OnSearchTextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ProductLibraryMaintenanceBackstageOpen senderControl = (ProductLibraryMaintenanceBackstageOpen)obj;
            senderControl.UpdateSearchResults();
        }

        #endregion

        #endregion

        #region Constructor

        public ProductLibraryMaintenanceBackstageOpen()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Finds results matching the given criteria from the viewmodel and updates the search collection
        /// </summary>
        private void UpdateSearchResults()
        {
            //_searchResults.Clear();

            if (this.ViewModel != null)
            {
                //IEnumerable<ProductLibraryInfo> results = this.ViewModel.GetMatchingProductLibraries(this.SearchText.ToUpperInvariant());

                //foreach (ProductLibraryInfo productLibraryInfo in results)
                //{
                //    _searchResults.Add(productLibraryInfo);
                //}
            }
        }

        #endregion

        #region Event Handler

        private void ViewModel_AvailableProductLibrariesBulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            UpdateSearchResults();
        }

        private void searchResultsListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //ListBox senderControl = (ListBox)sender;
            //ListBoxItem clickedItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
            //if (clickedItem != null)
            //{
            //    ProductLibraryInfo productLibraryToOpen = (ProductLibraryInfo)senderControl.SelectedItem;

            //    if (productLibraryToOpen != null)
            //    {
            //        //send the product library to be opened
            //        this.ViewModel.OpenCommand.Execute(productLibraryToOpen.Id);
            //    }
            //}
        }

        #endregion
    }
}
