﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (V8 1.0)
// V8-25395 : A.Probyn
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Fluent;

namespace Galleria.Ccm.Editor.Client.Wpf.ProductLibraryMaintenance
{
    /// <summary>
    /// Interaction logic for ProductLibraryMaintenanceHomeTab.xaml
    /// </summary>
    public partial class ProductLibraryMaintenanceHomeTab : RibbonTabItem
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ProductLibraryMaintenanceViewModel), typeof(ProductLibraryMaintenanceHomeTab),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public ProductLibraryMaintenanceViewModel ViewModel
        {
            get { return (ProductLibraryMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ProductLibraryMaintenanceHomeTab senderControl = (ProductLibraryMaintenanceHomeTab)obj;

            if (e.OldValue != null)
            {
                ProductLibraryMaintenanceViewModel oldModel = (ProductLibraryMaintenanceViewModel)e.OldValue;
            }
            if (e.NewValue != null)
            {
                ProductLibraryMaintenanceViewModel newModel = (ProductLibraryMaintenanceViewModel)e.NewValue;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public ProductLibraryMaintenanceHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
