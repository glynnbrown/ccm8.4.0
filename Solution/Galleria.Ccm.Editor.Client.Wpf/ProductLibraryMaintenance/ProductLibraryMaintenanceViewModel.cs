﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (V8 1.0)
// V8-25395 : A.Probyn
//  Created. Based on GFS but overhauled to resolve many bugs in fixed width.
// V8-26252 : I.George
//  Removed the if/else statement that was used to set the filter when opening a file dialogue window so it shows .txt and .csv
// V8-27648 : A.Probyn
//  Updated to include active planogram metrics where necessary.
// V8-27648 : A.Probyn ~ Updated to support ProductLibraryProduct
// V8-27986 : A.Probyn 
//      ~ Updated so that changing worksheet works and corrected the code so that the start row at is now working.
//      ~ Updated code surrouding switching between excel & text and fixed width logic.
//      ~ Disabled IsFirstRowHeaders for fixed width as it doesn't make sense.  
//      ~ StartRowAt changes now updates the preview data
//      ~ IsFirstRowHeaders no longer affects the FileData. It is now only consumed in the preview data to stop loss of data.   
//      ~ Updated FixedWidth behaviour when clearing mappings   
// V8-28389 : A.Probyn ~ Corrected issues when switching between excel and text
// V8-27931 : D.Pleasance ~ Amended to prevent duplication of columns whe type Excel selected
#endregion

#region Version History: CCM 801
// V8-27897 : A.Kuszyk
//  Moved Populate method into model.
#endregion

#region Version History: (CCM 8.1.0)
// V8-30130 : A.Probyn
//  Extended file data load process to handle any unhandled exceptions correctly.
// V8-29550 : A.Probyn
//  Added busy cursor to the changing of the first row is headers to improve UI for large column set loading.
#endregion
#region Version History: (CCM 8.1.1)
// V8-27887 : A.Probyn
//  Corrected bug UpdatePreviewDataCollection in which stops high start row numbers resulting in 0 rows.
// V8-29406 : N.Haywood
//  Added file type validation for OpenFileDialog
#endregion
#region Version History: (CCM 8.2.0)
//V8-28861 : L.Ineson
//  Amended to use session directory path.
#endregion
#region Version History: (CCM 8.3.0)
// V8-32361 : L.Ineson
//  Made changes to allow for default templates to be created.
// V8-32473 : L.Ineson
//  Corrected the save button to automatically save to file for a new item.
//  Editing a default template from the repository will now try to keep the last selected sample excel file. 
// V8-32568 : L.Ineson
//  Fixed issue where the template was being made readonly before it could be deleted.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Windows;

using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;

using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Imports;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using System.Windows.Input;
using Microsoft.Win32;
using Csla;
using System.Collections.ObjectModel;
using System.IO;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Controls;
using System.Collections.Specialized;
using Galleria.Framework.Dal;
using System.Text;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Common.Wpf;
using Galleria.Ccm.Common.Wpf.Selectors;


namespace Galleria.Ccm.Editor.Client.Wpf.ProductLibraryMaintenance
{
    //TODO: Tidy up.

    /// <summary>
    /// ViewModel controller for ProductLibraryMaintenanceOrganiser
    /// </summary>
    public class ProductLibraryMaintenanceViewModel : WindowViewModelBase, IDataErrorInfo
    {
        #region Constants
        const String _exCategory = "ProductLibraryMaintenance";
        #endregion

        #region Fields

        private ProductLibraryViewModel _selectedProductLibrary;
        private PlanogramViewObjectView _activePlanView;
        private readonly ModelPermission<Model.ProductLibrary> _itemRepositoryPerms = new ModelPermission<Model.ProductLibrary>();

        private Dictionary<ImportDefinitionFileType, String> _availableFileTypes;
        private ObservableCollection<String> _availableWorksheets = new ObservableCollection<String>();
        private String _selectedWorksheet;

        private String _filePath;
        private ImportFileData _fileData = ImportFileData.NewImportFileData();

        public const Int32 numPreviewRows = 50;
        private ObservableCollection<ImportFileDataRow> _previewData = new ObservableCollection<ImportFileDataRow>(); //used for excel & delimited text
        private ObservableCollection<String> _rawPreviewData = new ObservableCollection<String>(); //used for fixeed width text
        private ReadOnlyObservableCollection<String> _rawPreviewDataRO;
        private DataGridColumnCollection _previewColumnSet = new DataGridColumnCollection();

        private ObservableCollection<ImportMapping> _selectMappingFromGrid = new ObservableCollection<ImportMapping>();
        private ImportFileDataRow _headerRow;
        private Boolean _suppressMappingChangedHandler;

        private Boolean _hasOutOfMemoryException;
        private Boolean _hasIOException;
        private Boolean _hasFileMissingException;

       
        private ObservableCollection<ImportFixedLineDef> _fixedWidthLines = new ObservableCollection<ImportFixedLineDef>();
        private List<Int32> _fixedWidthLineValueList = new List<Int32>();

        private ObservableCollection<String> _availableColumns = new ObservableCollection<String>();
        private ReadOnlyObservableCollection<String> _availableColumnsRO;


        private Boolean _isFilePathValid;
        private Boolean _isFilePathSet;
        private Boolean _isDelimitBoxVisible;
        private Boolean _isDelimiterTypeBoxVisible;
        private Boolean _isExcelWorkbookBoxVisible;
        private Boolean _isExcelWorkbookTextboxVisible;
        private Boolean _isFixedWidth;

        private Boolean _isImportInProgress = false;


        #region ImportProcess related

        private Stopwatch _timer = new Stopwatch();
        private ModalBusy _busyDialog;

        #endregion

        #endregion

        #region Property Paths

        //Properties
        public static readonly PropertyPath SelectedProductLibraryProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.SelectedProductLibrary);
        public static readonly PropertyPath AvailableColumnsProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.AvailableColumns);
        public static readonly PropertyPath AvailableFileTypesProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.AvailableFileTypes);
        public static readonly PropertyPath AvailableWorksheetsProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.AvailableWorksheets);
        public static readonly PropertyPath SelectedWorksheetProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.SelectedWorksheet);
        public static readonly PropertyPath SelectedProductLibraryFileNameProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.SelectedProductLibraryFileName);
        public static readonly PropertyPath FilePathProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.FilePath);
        public static readonly PropertyPath FileDataProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.FileData);
        public static readonly PropertyPath RawPreviewDataProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.RawPreviewData);
        public static readonly PropertyPath PreviewDataProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.PreviewData);
        public static readonly PropertyPath PreviewDataColumnSetProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.PreviewDataColumnSet);
        public static readonly PropertyPath SelectedMappingFromGridProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.SelectedMappingFromGrid);
        public static readonly PropertyPath FixedWidthLinesProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.FixedWidthLines);
        public static readonly PropertyPath FixedWidthLineValueListProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.FixedWidthLineValueList);
        public static readonly PropertyPath AreMappingsAllValidProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.AreMappingsAllValid);
        public static readonly PropertyPath IsFilePathValidProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.IsFilePathValid);
        public static readonly PropertyPath IsFilePathSetProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.IsFilePathSet);
        public static readonly PropertyPath IsStandardFilePreviewVisibleProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.IsStandardFilePreviewVisible);
        public static readonly PropertyPath IsFixedWidthTextFilePreviewVisibleProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.IsFixedWidthTextFilePreviewVisible);
        public static readonly PropertyPath IsDelimitBoxVisibleProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.IsDelimitBoxVisible);
        public static readonly PropertyPath IsDelimiterTypeBoxVisibleProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.IsDelimiterTypeBoxVisible);
        public static readonly PropertyPath IsExcelWorkbookBoxVisibleProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.IsExcelWorkbookBoxVisible);
        public static readonly PropertyPath IsExcelWorkbookTextboxVisibleProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.IsExcelWorkbookTextboxVisible);
        public static readonly PropertyPath IsFixedWidthProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.IsFixedWidth);

        //Commands
        public static readonly PropertyPath NewCommandProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.NewCommand);
        public static readonly PropertyPath OpenFromFileCommandProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.OpenFromFileCommand);
        public static readonly PropertyPath OpenFromRepositoryCommandProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.OpenFromRepositoryCommand);
        public static readonly PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.SaveCommand);
        public static readonly PropertyPath SaveAsRepositoryCommandProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.SaveAsToRepositoryCommand);
        public static readonly PropertyPath SaveAsFileCommandProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.SaveAsToFileCommand);
        public static readonly PropertyPath SaveAndNewCommandProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.SaveAndNewCommand);
        public static readonly PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.SaveAndCloseCommand);
        public static readonly PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<ProductLibraryMaintenanceViewModel>(p => p.CloseCommand);

        #endregion

        #region Properties

        [Obsolete("Temporary, code should be refactored to not require this")]
        private ProductLibraryMaintenanceOrganiser AttachedControl
        {
            get { return GetAttachedControl() as ProductLibraryMaintenanceOrganiser; }
        }

        public Model.ProductLibrary CurrentProductLibrary
        {
            get { return _selectedProductLibrary.Model; }
            private set
            {
                _selectedProductLibrary.Model = value;
            }
        }

        /// <summary>
        /// Gets/Sets the import mapping template
        /// </summary>
        public ProductLibraryViewModel SelectedProductLibrary
        {
            get { return _selectedProductLibrary; }
            set
            {
                _selectedProductLibrary = value;
                OnPropertyChanged(SelectedProductLibraryProperty);
                OnPropertyChanged(SelectedProductLibraryFileNameProperty);
            }
        }

        /// <summary>
        /// Returns friendly name for the selected product librarys file
        /// </summary>
        public String SelectedProductLibraryFileName
        {
            get
            {
                if (this.FilePath != null && !String.IsNullOrEmpty(this.FilePath))
                {
                    return Path.GetFileNameWithoutExtension(this.FilePath);
                }
                return String.Empty;
            }
        }

        /// <summary>
        /// Returns a source containing all file types available for selection
        /// </summary>
        public Dictionary<ImportDefinitionFileType, String> AvailableFileTypes
        {
            get
            {
                if (_availableFileTypes == null)
                {
                    _availableFileTypes = new Dictionary<ImportDefinitionFileType, String>()
                    {
                        {ImportDefinitionFileType.Excel, ImportDefinitionFileTypeHelper.FriendlyNames[ImportDefinitionFileType.Excel]},
                        {ImportDefinitionFileType.Text, ImportDefinitionFileTypeHelper.FriendlyNames[ImportDefinitionFileType.Text]},
                    };
                }
                return _availableFileTypes;
            }
        }

        /// <summary>
        /// Returns the collection of available excel worksheets
        /// </summary>
        public ObservableCollection<String> AvailableWorksheets
        {
            get
            {
                return _availableWorksheets;
            }
            set
            {
                _availableWorksheets = value;
                OnPropertyChanged(AvailableWorksheetsProperty);
                if (this.AvailableWorksheets != null && this.AvailableWorksheets.Count > 0
                    && String.IsNullOrEmpty(this.SelectedWorksheet))
                {
                    _selectedWorksheet = AvailableWorksheets.First();
                }
            }
        }

        /// <summary>
        /// Gets/Sets the selected selectedWorksheet
        /// </summary>
        public String SelectedWorksheet
        {
            get { return this.SelectedProductLibrary.Model.ExcelWorkbookSheet; }
            set
            {
                this.SelectedProductLibrary.Model.ExcelWorkbookSheet = value;
                OnPropertyChanged(SelectedWorksheetProperty);
                OnSelectedWorksheetChanged();
            }
        }

        /// <summary>
        /// Returns the active plan view
        /// </summary>
        private PlanogramViewObjectView ActivePlanView
        {
            get { return _activePlanView; }
        }

        /// <summary>
        /// Gets/Sets the selected file path
        /// </summary>
        public String FilePath
        {
            get { return _filePath; }
            set
            {
                _filePath = value;

                _selectedWorksheet = String.Empty; //invalidate selected worksheet

                this.SelectedProductLibrary.Model.IsFirstRowHeaders = false; //reset

                OnPropertyChanged(FilePathProperty);
                OnPropertyChanged(SelectedProductLibraryFileNameProperty);
                OnFilePathChanged();

                OnPropertyChanged(IsExcelWorkbookBoxVisibleProperty);
                OnPropertyChanged(IsExcelWorkbookTextboxVisibleProperty);

                //if file path is indeed valid, try to retreive the data
                if (IsFilePathValid)
                {
                    DataRetrieval();
                    if (this.SelectedProductLibrary.Model.FileType == ImportDefinitionFileType.Text && this.FileData != null && !this.FileData.Columns.Any())
                    {
                        UpdateFileDataFromTextFile();
                    }
                }
            }
        }

        /// <summary>
        /// Returns column separated file data
        /// </summary>
        public ImportFileData FileData
        {
            get { return _fileData; }
            set
            {
                ImportFileData oldValue = _fileData;
                _fileData = value;

                OnFileDataChanged(oldValue, value);
                OnPropertyChanged(FileDataProperty);
            }
        }

        /// <summary>
        /// Returns the collection of raw data for review
        /// </summary>
        public ReadOnlyObservableCollection<String> RawPreviewData
        {
            get
            {
                if (_rawPreviewDataRO == null)
                {
                    _rawPreviewDataRO = new ReadOnlyObservableCollection<String>(_rawPreviewData);
                }
                return _rawPreviewDataRO;
            }
        }

        /// <summary>
        /// Returns the collection of columns used to preview the data
        /// </summary>
        public DataGridColumnCollection PreviewDataColumnSet
        {
            get { return _previewColumnSet; }
        }

        /// <summary>
        /// Gets/sets the selected files imported data
        /// </summary>
        public ObservableCollection<ImportFileDataRow> PreviewData
        {
            get { return _previewData; }
            set
            {
                _previewData = value;
                OnPropertyChanged(PreviewDataProperty);
            }
        }

        public ObservableCollection<ImportMapping> SelectedMappingFromGrid
        {
            get { return _selectMappingFromGrid; }
            set
            {
                _selectMappingFromGrid = value;
                OnPropertyChanged(SelectedMappingFromGridProperty);
            }
        }


        /// <summary>
        /// Returns the collection containing the defined fixed width lines
        /// </summary>
        public ObservableCollection<ImportFixedLineDef> FixedWidthLines
        {
            get { return _fixedWidthLines; }
        }

        internal List<Int32> FixedWidthLineValueList
        {
            get
            {
                return _fixedWidthLineValueList;
            }
            set
            {
                _fixedWidthLineValueList = value;
                OnPropertyChanged(FixedWidthLineValueListProperty);
            }

        }

        /// <summary>
        /// Returns the collection containing columns available for mapping
        /// </summary>
        public ReadOnlyObservableCollection<String> AvailableColumns
        {
            get
            {
                if (_availableColumnsRO == null)
                {
                    _availableColumnsRO = new ReadOnlyObservableCollection<String>(_availableColumns);
                }
                return _availableColumnsRO;
            }
        }


        /// <summary>
        /// Returns true if all columns are mapped correctly
        /// </summary>
        public Boolean AreMappingsAllValid
        {
            get
            {
                if (this.FileData != null)
                {
                    return
                        _fileData.MappingList
                                 .Where(m => m.PropertyIsMandatory)
                                 .All(m => !String.IsNullOrEmpty(m.ColumnReference));
                }
                return true;
            }
        }

        /// <summary>
        /// Flag to indicate if the file path is valid.
        /// </summary>
        public Boolean IsFilePathValid
        {
            get { return _isFilePathValid; }
            private set
            {
                _isFilePathValid = value;
                OnPropertyChanged(IsFilePathValidProperty);
            }
        }

        /// <summary>
        /// Returns true if a sample file was included 
        /// </summary>
        public Boolean IsFilePathSet
        {
            get { return _isFilePathSet; }
            set
            {
                _isFilePathSet = value;
                OnPropertyChanged(IsFilePathSetProperty);
            }
        }

        /// <summary>
        /// Returns true if to display a fixed with thext file preview
        /// </summary>
        public Boolean IsStandardFilePreviewVisible
        {
            get { return _isFilePathSet && !_isFixedWidth; }
        }

        /// <summary>
        /// Returns true if to display a fixed with thext file preview
        /// </summary>
        public Boolean IsFixedWidthTextFilePreviewVisible
        {
            get { return _isFilePathSet && _isFixedWidth; }
        }

        public Boolean IsDelimitBoxVisible
        {
            get { return _isDelimitBoxVisible; }
            set
            {
                _isDelimitBoxVisible = value;
                OnPropertyChanged(IsDelimitBoxVisibleProperty);
                OnPropertyChanged(IsDelimiterTypeBoxVisibleProperty);
                OnPropertyChanged(IsExcelWorkbookBoxVisibleProperty);
                OnPropertyChanged(IsExcelWorkbookTextboxVisibleProperty);
            }
        }

        public Boolean IsDelimiterTypeBoxVisible
        {
            get { return (IsDelimitBoxVisible && !IsFixedWidth); }
            private set
            {
                _isDelimiterTypeBoxVisible = value;
                OnPropertyChanged(IsDelimiterTypeBoxVisibleProperty);
            }
        }

        public Boolean IsExcelWorkbookBoxVisible
        {
            get { return !String.IsNullOrEmpty(this.FilePath) && !IsDelimitBoxVisible; }
            private set
            {
                _isExcelWorkbookBoxVisible = value;
                OnPropertyChanged(IsExcelWorkbookBoxVisibleProperty);
                OnPropertyChanged(IsExcelWorkbookTextboxVisibleProperty);
            }
        }

        public Boolean IsExcelWorkbookTextboxVisible
        {
            get { return _isExcelWorkbookTextboxVisible = !IsExcelWorkbookBoxVisible && !IsDelimitBoxVisible; }
            private set
            {
                _isExcelWorkbookTextboxVisible = value;
                OnPropertyChanged(IsExcelWorkbookTextboxVisibleProperty);
            }
        }

        /// <summary>
        /// Gets/Sets if fixed width is selected.
        /// </summary>
        public Boolean IsFixedWidth
        {
            get { return _isFixedWidth; }
            set
            {
                _isFixedWidth = value;
                OnPropertyChanged(IsFixedWidthProperty);
                OnIsFixedWidthChanged(value);

                OnPropertyChanged(IsDelimiterTypeBoxVisibleProperty);
                OnPropertyChanged(IsExcelWorkbookBoxVisibleProperty);
                OnPropertyChanged(IsStandardFilePreviewVisibleProperty);
                OnPropertyChanged(IsFixedWidthTextFilePreviewVisibleProperty);
            }
        }

        #endregion

        #region Constructor

        public ProductLibraryMaintenanceViewModel()
            : this(null) { }
        

        /// <summary>
        /// Default constructor
        /// </summary>
        public ProductLibraryMaintenanceViewModel(PlanogramViewObjectView activePlanView)
        {
            //set perms
            _itemRepositoryPerms = (App.ViewState.IsConnectedToRepository) ?
                new ModelPermission<Model.ProductLibrary>(Model.ProductLibrary.GetUserPermissions())
                : ModelPermission<Model.ProductLibrary>.DenyAll();


            //Setup selected product library view model and active planogram
            _selectedProductLibrary = new ProductLibraryViewModel();
            _activePlanView = activePlanView;

            //attach to model changed on selected product library view model
            this.SelectedProductLibrary.ModelChanged += SelectedProductLibrary_ModelChanged;


            //Create new product library
            this.NewCommand.Execute();

        }

        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _newCommand;

        /// <summary>
        /// Creates a new product library template.
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand = new RelayCommand(
                        p => NewCommand_Executed(),
                        p => NewCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_New,
                        FriendlyDescription = Message.Generic_New_Tooltip,
                        Icon = ImageResources.NewDocument_32,
                        SmallIcon = ImageResources.NewDocument_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.N
                    };
                    RegisterCommand(_newCommand);
                }
                return _newCommand;
            }
        }

        private Boolean NewCommand_CanExecute()
        {
            //user must have create permission
            //if (!Csla.Rules.BusinessRules.HasPermission(
            //    Csla.Rules.AuthorizationActions.CreateObject, typeof(Galleria.Ccm.Model.ProductLibrary)))
            //{
            //    _newCommand.DisabledReason = Message.Generic_NoCreatePermission;
            //    return false;
            //}

            return true;
        }

        private void NewCommand_Executed()
        {
            // Create product library
            Ccm.Model.ProductLibrary newProductLibrary = Ccm.Model.ProductLibrary.NewProductLibrary();

            //Set model
            this.SelectedProductLibrary.Model = newProductLibrary;

            this.IsFilePathValid = false;
            
            _filePath = String.Empty;
            OnPropertyChanged(FilePathProperty);


            this.IsFilePathSet = false;
            OnPropertyChanged(IsStandardFilePreviewVisibleProperty);
            OnPropertyChanged(IsFixedWidthTextFilePreviewVisibleProperty);

            this.IsFixedWidth = false;
            this.IsDelimitBoxVisible = false;
            this.IsExcelWorkbookBoxVisible = false;
            this.IsExcelWorkbookTextboxVisible = true;

            _selectedWorksheet = this.SelectedProductLibrary.Model.ExcelWorkbookSheet;
            OnPropertyChanged(SelectedWorksheetProperty);

            //Preload mappings
            this.GetMappingList();

            //Ensure friendly name is correct
            OnPropertyChanged(SelectedProductLibraryFileNameProperty);
        }

        #endregion

        #region OpenFromFileCommand

        private RelayCommand _openFromFileCommand;

        /// <summary>
        /// Open product library from file
        /// </summary>
        public RelayCommand OpenFromFileCommand
        {
            get
            {
                if (_openFromFileCommand == null)
                {
                    _openFromFileCommand = new RelayCommand(p => OpenFromFile_Executed(p))
                    {
                        FriendlyName = Message.ProductLibraryMaintenance_OpenFromFile,
                        SmallIcon = ImageResources.Open_16,
                    };
                    RegisterCommand(_openFromFileCommand);
                }
                return _openFromFileCommand;
            }
        }

        private void OpenFromFile_Executed(Object arg)
        {
            String fileName = null;

            //check the param
            if (arg != null && arg is String)
            {
                String pathString = (String)arg;
                if (pathString.EndsWith(Model.ProductLibrary.FileExtension)
                    && System.IO.File.Exists(pathString))
                {
                    fileName = pathString;
                }
            }


            //show the file open dialog:
            if (String.IsNullOrEmpty(fileName))
            {
                Boolean ofdFResult =
                 GetWindowService().ShowOpenFileDialog(
                    App.ViewState.GetSessionDirectory(SessionDirectory.ProductUniverse),
                    Message.ImportDefinition_ImportFileTypeFilter_POGPL,
                    out fileName);

                if (!ofdFResult) return;
            }

            //validate the selected file name:
            List<String> extensions = new List<String>(new String[] { ".xlsx", ".xls", ".txt", ".csv", ".pogpl" });
            if (!extensions.Any(s => String.Equals(Path.GetExtension(fileName), s, StringComparison.OrdinalIgnoreCase)))
            {
                GetWindowService()
                    .ShowOkMessage(Ccm.Common.Wpf.Services.MessageWindowType.Error,
                    Message.General_WrongFileType_Title,
                    Message.General_WrongFileType_Description);
                return;
            }


            OpenFromFile(fileName);

            CloseRibbonBackstage();
        }

        /// <summary>
        /// Loads the mapping template from the given file
        /// </summary>
        /// <param name="fileName"></param>
        public void OpenFromFile(String fileName)
        {
            //load:
            base.ShowWaitCursor(true);

            try
            {
                //Resolve file path to ensure its a POGPL
                Object productLibraryId = ProductLibraryHelper.ResolveProductLibraryIdFromFilePath(fileName);

                if (System.IO.File.Exists(productLibraryId.ToString()))
                {
                    //Fetch
                    this.SelectedProductLibrary.FetchById(productLibraryId, /*asReadonly*/false);
                }
                else
                {
                    base.ShowWaitCursor(false);

                    //Show warning as no template exists
                    //Missing template error display
                    GetWindowService().ShowOkMessage(Ccm.Common.Wpf.Services.MessageWindowType.Warning,
                        Message.ProductLibraryMaintenance_MissingTemplate,
                        String.Format(Message.ProductLibraryMaintenance_MissingTemplate_AutoCreateDescription, this.SelectedProductLibrary));


                    base.ShowWaitCursor(true);

                    //Create new and select file as source file path
                    this.NewCommand.Execute();

                    //Set file type
                    this.SelectedProductLibrary.Model.FileType = FileHelper.GetFileTypeFromExtension(fileName);

                    //Set source file
                    this.FilePath = fileName;

                    //update the session directory
                    App.ViewState.SetSessionDirectory(SessionDirectory.ProductUniverse, Path.GetDirectoryName(fileName));
                }
            }

            catch (Exception ex)
            {
                base.ShowWaitCursor(false);
                RecordException(ex, _exCategory);
                GetWindowService().ShowErrorOccurredMessage(fileName, OperationType.Open);
                return;
            }

            base.ShowWaitCursor(false);

            
        }

        #endregion

        #region OpenFromRepositoryCommand

        private RelayCommand _openFromRepositoryCommand;

        /// <summary>
        /// Loads the requested item from the repository
        /// parameter = the id of the item to open
        /// </summary>
        public RelayCommand OpenFromRepositoryCommand
        {
            get
            {
                if (_openFromRepositoryCommand == null)
                {
                    _openFromRepositoryCommand = new RelayCommand(
                        p => OpenFromRepository_Executed(p),
                        p => OpenFromRepository_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_OpenFromRepository,
                        SmallIcon = ImageResources.Open_16,
                    };
                    RegisterCommand(_openFromRepositoryCommand);
                }
                return _openFromRepositoryCommand;
            }
        }

        private Boolean OpenFromRepository_CanExecute(Object arg)
        {
            //must be connected
            if (!App.ViewState.IsConnectedToRepository)
            {
                this.OpenFromRepositoryCommand.DisabledReason = Message.DisabledReason_NoRepositoryConnected;
                return false;
            }

            //must have open permission
            if (!_itemRepositoryPerms.CanFetch)
            {
                this.OpenFromRepositoryCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            //must have a value
            if (arg == null)
            {
                this.OpenFromRepositoryCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void OpenFromRepository_Executed(Object arg)
        {
            //Get the id of the item to open.
            Int32? itemId = arg as Int32?;
            if (arg is ProductLibraryInfo)
            {
                itemId = ((ProductLibraryInfo)arg).Id as Int32?;
            }
            else if (arg is Int32)
            {
                itemId = (Int32)arg;
            }
            Debug.Assert(itemId.HasValue, "No id provided");
            if (!itemId.HasValue) return;


            //if (!itemId.HasValue)
            //{
            //    //show the selection dialog
            //    ProductLibraryInfoList productUniverseInfos;
            //    try
            //    {
            //        productUniverseInfos = ProductLibraryInfoList.FetchByEntityId(App.ViewState.EntityId);
            //    }
            //    catch (Exception ex)
            //    {
            //        RecordException(ex);
            //        return;
            //    }


            //    GenericSingleItemSelectorWindow win = new GenericSingleItemSelectorWindow();
            //    win.SelectionMode = DataGridSelectionMode.Single;
            //    win.ItemSource = productUniverseInfos;

            //    GetWindowService().ShowDialog<GenericSingleItemSelectorWindow>(win);
            //    if (win.DialogResult != true) return;

            //    itemId = (Int32)win.SelectedItems.Cast<ProductLibraryInfo>().First().Id;
            //}


            OpenFromRepository(itemId.Value);

            CloseRibbonBackstage();
            
        }

        public void OpenFromRepository(Int32 itemId)
        {
            //Confirm change with user.
            if (!ContinueWithItemChange()) return;


            ShowWaitCursor(true);

            try
            {
                //fetch the requested item
                this.CurrentProductLibrary = Model.ProductLibrary.FetchById(itemId);
                this.CurrentProductLibrary.MarkGraphAsInitialized();
            }
            catch (Exception ex)
            {
                base.ShowWaitCursor(false);
                RecordException(ex);
                GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                return;
            }


            //close the backstage
            CloseRibbonBackstage();

            ShowWaitCursor(false);
        }

        #endregion

        #region SaveCommand

        private RelayCommand _saveCommand;

        /// <summary>
        /// Saves the mapping template
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        p => Save_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_Save,
                        FriendlyDescription = Message.Generic_Save_Tooltip,
                        Icon = ImageResources.Save_32,
                        SmallIcon = ImageResources.Save_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.S
                    };
                    RegisterCommand(_saveCommand);
                }
                return _saveCommand;
            }
        }

        private Boolean Save_CanExecute()
        {
            //may not be null
            if (this.SelectedProductLibrary == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                this.SaveAndNewCommand.DisabledReason = String.Empty;
                this.SaveAndCloseCommand.DisabledReason = String.Empty;
                this.SaveCommand.DisabledReason = String.Empty;
                return false;
            }

            if (String.IsNullOrEmpty(this.SelectedProductLibrary.Model.ExcelWorkbookSheet))
            {
                this.SelectedProductLibrary.Model.PropertyChanged -= SelectedProductLibrary_PropertyChanged;
                this.SelectedProductLibrary.Model.ExcelWorkbookSheet = this.SelectedWorksheet;
                this.SelectedProductLibrary.Model.PropertyChanged += SelectedProductLibrary_PropertyChanged;
            }

            //must be valid
            if (!this.SelectedProductLibrary.Model.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;

                return false;
            }

            //must not be readonly.
            if (this.CurrentProductLibrary.IsReadOnly)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            //user must have edit permission
            //if (!Csla.Rules.BusinessRules.HasPermission
            //    (Csla.Rules.AuthorizationActions.EditObject, typeof(Galleria.Ccm.Model.ProductLibrary)))
            //{
            //    this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
            //    this.SaveAndNewCommand.DisabledReason = Message.Generic_NoEditPermission;
            //    this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoEditPermission;

            //    return false;
            //}

            if (_fileData != null && _fileData.MappingList != null && _fileData.MappingList.Count > 0)
            {
                ImportMapping unmachedMapping = _fileData.MappingList.FirstOrDefault(m => m.PropertyIsMandatory && !m.IsColumnReferenceSet);
                if (unmachedMapping == null)
                {
                    // check for duplicate mappings, prevent from saving if duplicates detected
                    ImportMapping duplicateMapping = _fileData.MappingList.FirstOrDefault(m => m.IsDuplicateMapping);
                    if (duplicateMapping != null)
                    {
                        this.SaveCommand.DisabledReason = Message.ProductLibraryMaintenance_SaveDisabledReason_DuplicateMappings;
                        this.SaveAndNewCommand.DisabledReason = Message.ProductLibraryMaintenance_SaveDisabledReason_DuplicateMappings;
                        this.SaveAndCloseCommand.DisabledReason = Message.ProductLibraryMaintenance_SaveDisabledReason_DuplicateMappings;
                        this.SaveCommand.DisabledReason = Message.ProductLibraryMaintenance_SaveDisabledReason_DuplicateMappings;

                        return false;
                    }

                    if (this.IsFixedWidth)
                    {
                        foreach (ImportMapping mapping in _fileData.MappingList)
                        {
                            //one of the column ends defined but not the other
                            if (
                                (mapping.FixedWidthColumnStart == null && mapping.FixedWidthColumnEnd != null)
                                || (mapping.FixedWidthColumnStart != null && mapping.FixedWidthColumnEnd == null)
                                //no boundaries defined, but reference column is set or a default is set
                                || (mapping.FixedWidthColumnStart == null
                                        && mapping.FixedWidthColumnEnd == null
                                        && (mapping.IsColumnReferenceSet))
                                //boundaries defined, but not valid
                                || (mapping.IsColumnReferenceSet && mapping.FixedWidthColumnStart != null && mapping.FixedWidthColumnEnd != null && mapping.FixedWidthColumnStart >= mapping.FixedWidthColumnEnd)
                                )
                            {
                                this.SaveCommand.DisabledReason = Message.ProductLibraryMaintenance_SaveDisabledReason_FixedWidthStartEndValueMissing;
                                this.SaveAndNewCommand.DisabledReason = Message.ProductLibraryMaintenance_SaveDisabledReason_FixedWidthStartEndValueMissing;
                                this.SaveAndCloseCommand.DisabledReason = Message.ProductLibraryMaintenance_SaveDisabledReason_FixedWidthStartEndValueMissing;
                                this.SaveCommand.DisabledReason = Message.ProductLibraryMaintenance_SaveDisabledReason_FixedWidthStartEndValueMissing;

                                return false;
                            }
                        }
                    }
                    return true;
                }
                else
                {
                    this.SaveCommand.DisabledReason = Message.ProductLibraryMaintenance_SaveDisabledReason_MandatoryMappingsMissing;
                    this.SaveAndNewCommand.DisabledReason = Message.ProductLibraryMaintenance_SaveDisabledReason_MandatoryMappingsMissing;
                    this.SaveAndCloseCommand.DisabledReason = Message.ProductLibraryMaintenance_SaveDisabledReason_MandatoryMappingsMissing;
                    this.SaveCommand.DisabledReason = Message.ProductLibraryMaintenance_SaveDisabledReason_MandatoryMappingsMissing;

                    return false;
                }
            }
            if (this.SelectedProductLibrary.Model.FileType == ImportDefinitionFileType.Excel &&
                String.IsNullOrEmpty(this.SelectedProductLibrary.Model.ExcelWorkbookSheet) || String.IsNullOrWhiteSpace(this.SelectedProductLibrary.Model.ExcelWorkbookSheet))
            {
                this.SaveCommand.DisabledReason = Message.ProductLibraryMaintenance_SaveDisabledReason_ExcelNoWorkbookSelected;
                this.SaveAndNewCommand.DisabledReason = Message.ProductLibraryMaintenance_SaveDisabledReason_ExcelNoWorkbookSelected;
                this.SaveAndCloseCommand.DisabledReason = Message.ProductLibraryMaintenance_SaveDisabledReason_ExcelNoWorkbookSelected;
                this.SaveCommand.DisabledReason = Message.ProductLibraryMaintenance_SaveDisabledReason_ExcelNoWorkbookSelected;

                return false;
            }

            return true;
        }

        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        private Boolean SaveCurrentItem()
        {
            if (this.CurrentProductLibrary.IsNew)
            {
                //always save new items to file.
                String newFilePath = null;
                if (this.IsFilePathSet)
                {
                    newFilePath =
                        Path.ChangeExtension(this.FilePath, Model.ProductLibrary.FileExtension);
                }
                return SaveAsToFile(newFilePath);
            }
            else if (this.CurrentProductLibrary.IsFile)
            {
                return SaveExistingFileItem();
            }
            else
            {
                return SaveExistingRepositoryItem();
            }
        }

        /// <summary>
        /// Saves the current item.
        /// </summary>
        /// <returns>Returns true if the save was sucessful. False if cancelled.</returns>
        private Boolean SaveExistingFileItem()
        {
            if (!this.CurrentProductLibrary.IsFile) return false;

            Boolean canContinue = true;
            try
            {
                Boolean exists = !this.SelectedProductLibrary.Model.IsNew;
                this.PopulateProductLibrary(exists /*new definition*/);
            }
            catch
            {
                return false; //can't save if item is not populated
            }

            if (this.SelectedProductLibrary.Model.IsValid)
            {
                //Calculate Id for POGPL file
                Object productLibraryId = ProductLibraryHelper.ResolveProductLibraryIdFromFilePath(this.FilePath);

                //If can continue
                if (canContinue)
                {
                    base.ShowWaitCursor(true);

                    try
                    {
                        //save the item
                        this.SelectedProductLibrary.SaveAsFile(productLibraryId.ToString()); //save the item         
                    }
                    catch (Exception ex)
                    {
                        base.ShowWaitCursor(false);

                        canContinue = false;

                        LocalHelper.RecordException(ex, _exCategory);
                        Exception rootException = ex.GetBaseException();

                        //if it is a concurrency exception check if the user wants to reload
                        if (rootException is ConcurrencyException)
                        {
                            Boolean itemReloadRequired = Galleria.Framework.Controls.Wpf.Helpers.ShowConcurrencyReloadPrompt(this.SelectedProductLibraryFileName);
                            if (itemReloadRequired)
                            {
                                //Reload model
                                this.SelectedProductLibrary.FetchById(Convert.ToString(this.SelectedProductLibrary.Model.Id), /*asReadOnly*/false);
                            }
                        }
                        else
                        {
                            //otherwise just show the user an error has occurred.
                            GetWindowService().ShowErrorOccurredMessage(
                                this.SelectedProductLibrary.Model.Id.ToString(), OperationType.Save);
                        }

                        base.ShowWaitCursor(true);
                    }
                }

                base.ShowWaitCursor(false);
            }
            else
            {
                this.SaveCommand.DisabledReason = Message.ProductLibraryMaintenance_SaveDisabledReason_ProductLibraryInvalid;
                return false;
            }

            return canContinue;
        }

        /// <summary>
        /// Saves the current item.
        /// </summary>
        /// <returns>Returns true if the save was sucessful. False if cancelled.</returns>
        private Boolean SaveExistingRepositoryItem()
        {
            if (this.CurrentProductLibrary.IsFile) return false;

            this.PopulateProductLibrary(false /*new definition*/);

            Object itemId = this.CurrentProductLibrary.Id;

            //** check the item unique value
            String newName;


            Predicate<String> isUniqueCheck =
                   (s) =>
                   {
                       Boolean returnValue = true;

                       ShowWaitCursor(true);

                       foreach (ProductLibraryInfo info in ProductLibraryInfoList.FetchByEntityId(App.ViewState.EntityId))
                       {
                           if (!Object.Equals(info.Id, itemId)
                               && info.Name.ToLowerInvariant() == s.ToLowerInvariant())
                           {
                               returnValue = false;
                               break;
                           }
                       }

                       ShowWaitCursor(false);

                       return returnValue;
                   };


            Boolean nameAccepted = GetWindowService().PromptIfNameIsNotUnique(isUniqueCheck, this.CurrentProductLibrary.Name, out newName);
            if (!nameAccepted) return false;

            //set the name
            if (this.CurrentProductLibrary.Name != newName) this.CurrentProductLibrary.Name = newName;
            this.CurrentProductLibrary.EntityId = App.ViewState.EntityId;


            //** save the item
            ShowWaitCursor(true);

            try
            {
                this.CurrentProductLibrary = this.CurrentProductLibrary.Save();
            }
            catch (Exception ex)
            {
                ShowWaitCursor(false);

                Exception rootException = ex.GetBaseException();
                RecordException(rootException);

                //if it is a concurrency exception check if the user wants to reload
                if (rootException is ConcurrencyException)
                {
                    Boolean itemReloadRequired = GetWindowService().ShowConcurrencyReloadPrompt(this.CurrentProductLibrary.Name);
                    if (itemReloadRequired)
                    {
                        this.CurrentProductLibrary = Model.ProductLibrary.FetchById(this.CurrentProductLibrary.Id);
                    }
                }
                else
                {
                    //otherwise just show the user an error has occurred.
                    GetWindowService().ShowErrorOccurredMessage(this.CurrentProductLibrary.Name, OperationType.Save);
                }

                return false;
            }

            ShowWaitCursor(false);

            return true;
        }


        /// <summary>
        /// Create an import product library from available user provided values
        /// </summary>
        private void PopulateProductLibrary(Boolean exists)
        {
            this.SelectedProductLibrary.Model.PropertyChanged -= SelectedProductLibrary_PropertyChanged;

            //populate product library
            this.SelectedProductLibrary.Model.Populate(exists, SelectedWorksheet, FileData, IsFixedWidth);

            this.SelectedProductLibrary.Model.PropertyChanged += SelectedProductLibrary_PropertyChanged;

        }


        #endregion

        #region SaveAndNew

        private RelayCommand _saveAndNewCommand;

        /// <summary>
        /// Saves the current item and creates a new one
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(
                        p => SaveAndNew_Executed(), p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                        SmallIcon = ImageResources.SaveAndNew_16
                    };
                    RegisterCommand(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            //request a save of the current item
            Boolean itemSaved = SaveCurrentItem();

            if (itemSaved)
            {
                //load a new item
                NewCommand.Execute();
            }

        }

        #endregion

        #region SaveAndCloseCommand

        private RelayCommand _saveAndCloseCommand;

        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    RegisterCommand(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean itemSaved = SaveCurrentItem();

            //if save completed
            if (itemSaved)
            {
                //close the attached window
                CloseWindow();
            }
        }

        #endregion

        #region SaveAsToRepository

        private RelayCommand _saveAsToRepositoryCommand;

        /// <summary>
        /// Saves the currrent template as a new template in the repository.
        /// </summary>
        public RelayCommand SaveAsToRepositoryCommand
        {
            get
            {
                if (_saveAsToRepositoryCommand == null)
                {
                    _saveAsToRepositoryCommand = new RelayCommand(
                            o => SaveAsToRepository_Executed(),
                        o => SaveAsToRepository_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAsToRepository,
                        SmallIcon = ImageResources.SaveAs_16,
                    };
                    RegisterCommand(_saveAsToRepositoryCommand);
                }
                return _saveAsToRepositoryCommand;
            }
        }

        private Boolean SaveAsToRepository_CanExecute()
        {
            //must have a repository connection.
            if (!App.ViewState.IsConnectedToRepository)
            {
                this.SaveAsToRepositoryCommand.DisabledReason = Message.DisabledReason_NoRepositoryConnected;
                return false;
            }

            //must have permission
            if (!_itemRepositoryPerms.CanCreate)
            {
                this.SaveAsToRepositoryCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //may not be null
            if (this.CurrentProductLibrary == null)
            {
                this.SaveAsToRepositoryCommand.DisabledReason = String.Empty;
                return false;
            }

            //if (!String.IsNullOrEmpty(_saveAsItemError))
            //{
            //    this.SaveAsToRepositoryCommand.DisabledReason = _saveAsItemError;
            //    return false;
            //}

            //must be valid
            if (!this.CurrentProductLibrary.IsValid)
            {
                this.SaveAsToRepositoryCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }


            return true;
        }

        private void SaveAsToRepository_Executed()
        {
            this.PopulateProductLibrary(true /*new definition*/);

            //** confirm the name to save as
            String copyName = null;

            Predicate<String> isUniqueCheck =
                   (s) =>
                   {
                       Boolean returnValue = true;

                       base.ShowWaitCursor(true);
                       foreach (ProductLibraryInfo info in ProductLibraryInfoList.FetchByEntityId(App.ViewState.EntityId))
                       {
                           if (info.Name.ToLowerInvariant() == s.ToLowerInvariant())
                           {
                               //ask the user if they want to overwrite
                               ModalMessageResult result =
                               GetWindowService().ShowMessage(Ccm.Common.Wpf.Services.MessageWindowType.Warning,
                                   "Item Already Exists",
                                   "An item with this name already exists, do you want to overwrite?",
                                   "Yes", "No");

                               if (result == ModalMessageResult.Button1)
                               {
                                   returnValue = true;

                                   //delete the existing info
                                   try
                                   {
                                       Model.ProductLibrary item = Model.ProductLibrary.FetchById(info.Id);
                                       item.Delete();
                                       item.Save();
                                       item.Dispose();
                                   }
                                   catch (Exception ex)
                                   {
                                       RecordException(ex);
                                       GetWindowService().ShowErrorMessage("Delete Failed",
                                           "The existing item could not be deleted, please provide a new unique name");
                                       returnValue = false;
                                   }
                               }
                               else returnValue = false;

                               break;
                           }
                       }
                       base.ShowWaitCursor(false);

                       return returnValue;
                   };


            Boolean nameAccepted = GetWindowService().PromptForSaveAsName(isUniqueCheck, String.Empty, out copyName);
            if (!nameAccepted) return;

            // update the name.
            this.CurrentProductLibrary.Name = copyName;


            //** save the item
            ShowWaitCursor(true);
            try
            {
                this.CurrentProductLibrary = this.CurrentProductLibrary.SaveAsToRepository(App.ViewState.EntityId);
            }
            catch (Exception ex)
            {
                ShowWaitCursor(false);
                RecordException(ex.GetBaseException());
                GetWindowService().ShowErrorOccurredMessage(this.CurrentProductLibrary.Name, OperationType.Save);
                return;
            }


            ShowWaitCursor(false);
        }

        #endregion

        #region SaveToFile

        private RelayCommand _saveAsToFileCommand;

        /// <summary>
        /// Saves the current template as a new file template.
        /// </summary>
        public RelayCommand SaveAsToFileCommand
        {
            get
            {
                if (_saveAsToFileCommand == null)
                {
                    _saveAsToFileCommand = new RelayCommand(
                        p => SaveAsToFile_Executed(p),
                        p => SaveAsToFile_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAsToFile,
                        SmallIcon = ImageResources.SaveAs_16,
                    };
                    RegisterCommand(_saveAsToFileCommand);
                }
                return _saveAsToFileCommand;
            }
        }

        private Boolean SaveAsToFile_CanExecute()
        {
            //may not be null
            if (this.CurrentProductLibrary == null)
            {
                this.SaveAsToFileCommand.DisabledReason = String.Empty;
                return false;
            }

            //must be valid
            if (!this.CurrentProductLibrary.IsValid)
            {
                this.SaveAsToFileCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }


            return true;
        }

        private void SaveAsToFile_Executed(Object args)
        {
            SaveAsToFile(args as String);
        }

        private Boolean SaveAsToFile(String filePath = null)
        {
            this.PopulateProductLibrary(true /*new definition*/);


            Model.ProductLibrary itemToSave = this.CurrentProductLibrary;

            if (String.IsNullOrEmpty(filePath))
            {
                //show dialog to get path
                Boolean result =
                GetWindowService().ShowSaveFileDialog(
                    this.CurrentProductLibrary.Name,
                    CCMClient.ViewState.GetSessionDirectory(SessionDirectory.ProductLibrary),
                    String.Format(CultureInfo.InvariantCulture, Message.ProductLibraryMaintenance_ImportFilter, Model.ProductLibrary.FileExtension),
                    out filePath);

                if (!result) return false;

                //update the session directory.
                CCMClient.ViewState.SetSessionDirectory(SessionDirectory.ProductLibrary, Path.GetDirectoryName(filePath));
            }

            base.ShowWaitCursor(true);

            //save
            try
            {
                //update the item name to the chosen file one.
                itemToSave.Name = Path.GetFileNameWithoutExtension(filePath);

                this.CurrentProductLibrary = itemToSave.SaveAsFile(filePath);
            }
            catch (Exception ex)
            {
                base.ShowWaitCursor(false);
                RecordException(ex);
                GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Save);
                return false;
            }
            base.ShowWaitCursor(false);

            return true;
        }

        #endregion

        #region BrowseForFileCommand

        private RelayCommand _browseForFileCommand;

        /// <summary>
        /// Show the file open dialog to allow the user to select a sample file.
        /// </summary>
        public RelayCommand BrowseForFileCommand
        {
            get
            {
                if (_browseForFileCommand == null)
                {
                    _browseForFileCommand = new RelayCommand(
                        p => BrowseForFile_Executed())
                    {
                        FriendlyName = Message.Generic_Browse
                    };
                    RegisterCommand(_browseForFileCommand);
                }
                return _browseForFileCommand;
            }
        }

        private void BrowseForFile_Executed()
        {
             String importFilter =
                 Message.ImportDefinition_ImportFileTypeFilter_Excel2007 + "|" 
                 + Message.ImportDefinition_ImportFileTypeFilter_Excel2003 + "|" 
                 + Message.ImportDefinition_ImportFileTypeFilter_Text + "|" 
                 + Message.ImportDefinition_ImportFileTypeFilter_Csv; // (GFS-16729) replaced with 'double' Excel filter

            String fileName;

            Boolean ofdResult = 
                GetWindowService().ShowOpenFileDialog(null,importFilter, out fileName);

            if(!ofdResult) return;


            LoadSampleFile(fileName);
        }

        public void LoadSampleFile(String fileName)
        {
            if (this.SelectedProductLibrary.Model.FileType == ImportDefinitionFileType.Excel)
            {
                //Set file
                this.FilePath = fileName;
            }
            else if (this.SelectedProductLibrary.Model.FileType == ImportDefinitionFileType.Text)
            {
                //check if textfile is valid - does it have content?
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(fileName);

                if (fileInfo.Length > 0)
                {
                    //Set file
                    this.FilePath = fileName;
                }
                else
                {
                    //Display message to user that file is not valid
                    GetWindowService().ShowOkMessage(
                        Ccm.Common.Wpf.Services.MessageWindowType.Warning,
                        Message.ImportDefinition_FileEmpty_Header,
                        String.Format(Message.ImportDefinition_FileEmpty_Description, fileName));

                }
            }
                
        }

        #endregion

        #region ClearCommand

        private RelayCommand _clearCommand;

        /// <summary>
        /// Clear Mappings
        /// </summary>
        public RelayCommand ClearCommand
        {
            get
            {
                if (_clearCommand == null)
                {
                    _clearCommand = new RelayCommand(
                        p => Clear_Executed())
                    {
                        FriendlyName = Message.ProductLibraryMaintenance_Clear,
                        FriendlyDescription = Message.ProductLibraryMaintenance_Clear_Description,
                        Icon = ImageResources.ProductLibraryMaintenance_Clear
                    };
                    RegisterCommand(_clearCommand);
                }
                return _clearCommand;
            }
        }

        private void Clear_Executed()
        {
            // Menu button
        }

        #endregion

        #region ClearSelectedMappingsCommand

        private RelayCommand<ImportMapping> _clearSelectedMappingsCommand;

        /// <summary>
        /// Clears all the selected column mappings
        /// </summary>
        public RelayCommand<ImportMapping> ClearSelectedMappingsCommand
        {
            get
            {
                if (_clearSelectedMappingsCommand == null)
                {
                    _clearSelectedMappingsCommand = new RelayCommand<ImportMapping>(
                        p => ClearSelectedMappingsCommand_Executed(),
                        p => ClearSelectedMappingsCommand_CanExecute())
                    {
                        FriendlyName = Message.ProductLibraryMaintenance_ClearSelectedMapping,
                        FriendlyDescription = Message.ProductLibraryMaintenance_ClearSelectedMapping_Description,
                        Icon = ImageResources.ProductLibraryMaintenance_ClearSelection
                    };
                    RegisterCommand(_clearSelectedMappingsCommand);
                }
                return _clearSelectedMappingsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ClearSelectedMappingsCommand_CanExecute()
        {
            //Do not assume that mappings have been set
            if (_selectMappingFromGrid != null)
            {
                foreach (ImportMapping mapping in _selectMappingFromGrid)
                {
                    if (!String.IsNullOrEmpty(mapping.ColumnReference))
                    {
                        this.ClearSelectedMappingsCommand.DisabledReason = String.Empty;
                        return true;
                    }
                }
            }
            this.ClearSelectedMappingsCommand.DisabledReason = Message.ProductLibraryMaintenance_ClearSelectedMappingsDisabledReason_NoMappingSelected;
            return false;
        }

        private void ClearSelectedMappingsCommand_Executed()
        {
            foreach (ImportMapping mapping in _selectMappingFromGrid)
            {
                mapping.ColumnReference = ImportMapping.EmptyColumnReference;

                if (this.IsFixedWidth)
                {
                    if (mapping.FixedWidthColumnEnd != null)
                    {
                        this.FixedWidthLines.Remove(this.FixedWidthLines.Where(p => p.LineValue == mapping.FixedWidthColumnEnd).First());
                    }
                    mapping.FixedWidthColumnStart = null;
                    mapping.FixedWidthColumnEnd = null;

                    this.UpdateFixedWidthPositionsToMappings();
                }
            }
        }

        #endregion

        #region ClearSelectedMappingCommand

        private RelayCommand<ImportMapping> _clearSelectedMappingCommand;

        /// <summary>
        /// Clears all the selected column Mapping
        /// </summary>
        public RelayCommand<ImportMapping> ClearSelectedMappingCommand
        {
            get
            {
                if (_clearSelectedMappingCommand == null)
                {
                    _clearSelectedMappingCommand = new RelayCommand<ImportMapping>(
                        p => ClearSelectedMappingCommand_Executed(p),
                        p => ClearSelectedMappingCommand_CanExecute(p))
                    {
                        FriendlyName = Message.ProductLibraryMaintenance_ClearSelectedMapping,
                        FriendlyDescription = Message.ProductLibraryMaintenance_ClearSelectedMapping_Description,
                        Icon = ImageResources.ProductLibraryMaintenance_ClearSelection
                    };
                    RegisterCommand(_clearSelectedMappingCommand);
                }
                return _clearSelectedMappingCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ClearSelectedMappingCommand_CanExecute(ImportMapping mapping)
        {
            if (mapping == null)
            {
                this.ClearSelectedMappingCommand.DisabledReason = Message.ProductLibraryMaintenance_ClearSelectedMappingsDisabledReason_NoMappingSelected;
                return false;
            }
            return true;
        }

        private void ClearSelectedMappingCommand_Executed(ImportMapping mapping)
        {
            mapping.ColumnReference = ImportMapping.EmptyColumnReference;

            if (this.IsFixedWidth)
            {
                //Remove mapping fixed width line
                if (mapping.FixedWidthColumnEnd != null)
                {
                    this.FixedWidthLines.Remove(this.FixedWidthLines.Where(p => p.LineValue == mapping.FixedWidthColumnEnd).First());
                }
                mapping.FixedWidthColumnStart = null;
                mapping.FixedWidthColumnEnd = null;

                //Update positions to mapping relations
                this.UpdateFixedWidthPositionsToMappings();
            }
        }

        /// <summary>
        /// Method to rematch fixed width positions to column mappings after mapping column update
        /// </summary>
        private void UpdateFixedWidthPositionsToMappings()
        {
            if (this.IsFixedWidth)
            {
                foreach (ImportMapping mappingItem in this.FileData.MappingList)
                {
                    //If mapping is set
                    if (mappingItem.IsColumnReferenceSet)
                    {
                        mappingItem.FixedWidthColumnEnd = null;
                        mappingItem.FixedWidthColumnStart = null;

                        //Find fixed width line that matches
                        try
                        {
                            Int32 columnNumber = this.FileData.GetMappedColNumber(mappingItem);
                            if (columnNumber > 0)
                            {
                                mappingItem.FixedWidthColumnEnd = FixedWidthLineValueList[columnNumber];
                                mappingItem.FixedWidthColumnStart = FixedWidthLineValueList[columnNumber - 1] + 1;
                            }
                        }
                        catch
                        {
                            //parse failed; cannot autopopulate start / end
                        }
                    }
                    else
                    {
                        //Check if there any more fixed lines, than mappings set
                        if (this.FixedWidthLines.Count <= this.FileData.MappingList.Count(p => p.IsColumnReferenceSet))
                        {
                            //No more available lines, break.
                            break;
                        }
                    }
                }
            }
        }

        #endregion

        #region ClearAllMappingsCommand

        private RelayCommand _clearAllMappingsCommand;

        /// <summary>
        /// Clears all the column mappings
        /// </summary>
        public RelayCommand ClearAllMappingsCommand
        {
            get
            {
                if (_clearAllMappingsCommand == null)
                {
                    _clearAllMappingsCommand = new RelayCommand(
                        p => ClearAllMappings_Executed(),
                        p => ClearAllMappings_CanExecute())
                    {
                        FriendlyName = Message.ProductLibraryMaintenance_ClearAllMappings,
                        FriendlyDescription = Message.ProductLibraryMaintenance_ClearAllMappings_Description,
                        Icon = ImageResources.ProductLibraryMaintenance_ClearAll
                    };
                    RegisterCommand(_clearAllMappingsCommand);
                }
                return _clearAllMappingsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ClearAllMappings_CanExecute()
        {
            if (this.FileData != null && this.FileData.MappingList != null)
            {
                foreach (ImportMapping mapping in this.FileData.MappingList)
                {
                    if (!String.IsNullOrEmpty(mapping.ColumnReference))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void ClearAllMappings_Executed()
        {
            _suppressMappingChangedHandler = true;

            if (this.FileData != null && this.FileData.MappingList != null)
            {
                //Clear all column mapping source column references
                foreach (ImportMapping item in this.FileData.MappingList)
                {
                    item.ColumnReference = ImportMapping.EmptyColumnReference;

                    if (this.IsFixedWidth)
                    {
                        item.FixedWidthColumnStart = null;
                        item.FixedWidthColumnEnd = null;
                    }
                }

                //Only reset if fixed width lines have been set
                if (this.FixedWidthLines.Count > 0)
                {
                    this.FixedWidthLines.Clear();
                }
            }

            _suppressMappingChangedHandler = false;
            OnPropertyChanged(AreMappingsAllValidProperty);
        }

        #endregion

        #region AutoMapCommand

        private RelayCommand _autoMapCommand;

        /// <summary>
        /// Trys to auto map columns
        /// </summary>
        public RelayCommand AutoMapCommand
        {
            get
            {
                if (_autoMapCommand == null)
                {
                    _autoMapCommand = new RelayCommand(
                        p => AutoMap_Executed(),
                        p => AutoMap_CanExecute())
                    {
                        FriendlyName = Message.ProductLibraryMaintenance_AutoMap,
                        FriendlyDescription = Message.ProductLibraryMaintenance_AutoMap_Description,
                        Icon = ImageResources.ProductLibraryMaintenance_AutoMatch
                    };
                    RegisterCommand(_autoMapCommand);
                }
                return _autoMapCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean AutoMap_CanExecute()
        {
            //sample file should be included to attempt auto-map
            if (String.IsNullOrEmpty(this.FilePath))
            {
                this.AutoMapCommand.DisabledReason = Message.ProductLibraryMaintenance_AutoMapDisabledReason_NoFileSelected;
                return false;
            }

            //data should not be null
            if (this.FileData == null)
            {
                this.AutoMapCommand.DisabledReason = Message.ProductLibraryMaintenance_AutoMapDisabledReason_NoFileData;
                return false;

            }
            //data should have columns.
            if (!this.IsFixedWidth && this.FileData.Columns.Count == 0)
            {
                this.AutoMapCommand.DisabledReason = Message.ProductLibraryMaintenance_AutoMapDisabledReason_NoColumns;
                return false;
            }
            return true;
        }

        private void AutoMap_Executed()
        {
            //Suppress column mapping update
            Boolean isLoading = _suppressMappingChangedHandler;

            //Trigger auto mapping
            AutoMappingColumns(isLoading);
        }

        /// <summary>
        /// Method to handle the auto mapping of columns
        /// </summary>
        /// <param name="loadingProductLibrary"></param>
        private void AutoMappingColumns(Boolean loadingProductLibrary)
        {
            if (!loadingProductLibrary)
            {
                if (!this.IsFixedWidth)
                {
                    //if the first row is headers try to use these to create the match
                    if (this.SelectedProductLibrary.Model.IsFirstRowHeaders)
                    {
                        foreach (ImportMapping item in this.FileData.MappingList)
                        {
                            String itemColName = item.PropertyName.Replace(" ", "").ToLowerInvariant();

                            //try to find a matching header
                            ImportFileDataColumn col =
                                this.FileData.Columns.FirstOrDefault(c => c.Header.Replace(" ", "").ToLowerInvariant() == itemColName);
                            if (col != null)
                            {
                                item.ColumnReference = col.Header;
                            }
                        }
                    }
                    else
                    {
                        Int32 availableColumnsCount = this.FileData.Columns.Count;
                        Int32 currentColIndex = 1;

                        foreach (ImportMapping item in this.FileData.MappingList)
                        {
                            //Check index value is valid
                            if (currentColIndex <= availableColumnsCount)
                            {
                                //Update column reference to match available column
                                item.ColumnReference = this.FileData.Columns[currentColIndex - 1].Header;
                            }
                            //Increase index number
                            currentColIndex += 1;
                        }
                    }
                }
                else
                {
                    //Auto map fixed width

                    //Get number of fixed width line setup
                    Int32 numberFixedwidthLines = this.FixedWidthLines.Count;
                    Int32 mappingIndex = 1;
                    foreach (ImportMapping item in this.FileData.MappingList)
                    {
                        //Check index value is valid
                        if (mappingIndex <= numberFixedwidthLines)
                        {
                            //Ensure index is valid
                            if (mappingIndex <= this.FileData.Columns.Count)
                            {
                                //Update column reference to match available column
                                item.ColumnReference = this.FileData.Columns[mappingIndex - 1].Header;
                            }
                        }
                        else { break; }
                        //Increase index number
                        mappingIndex += 1;
                    }
                }

                OnPropertyChanged(AreMappingsAllValidProperty);

                if (this.IsFixedWidth)
                {
                    MapFixedColumnWidthPositions();
                }
            }
        }

        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;
        /// <summary>
        /// Deletes the passed store
        /// Parameter = store to delete
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(p => Delete_Executed(), p => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        SmallIcon = ImageResources.Delete_16
                    };
                    RegisterCommand(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        [DebuggerStepThrough]
        private bool Delete_CanExecute()
        {
            //user must have delete permission
            if (!Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.DeleteObject, typeof(Galleria.Ccm.Model.ProductLibrary)))
            {
                _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must not be null
            if (this.SelectedProductLibrary == null)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }

            //must not be new
            if (this.SelectedProductLibrary.Model.IsNew)
            {
                _deleteCommand.DisabledReason = Message.Generic_Delete_DisabledIsNew;
                return false;
            }

            return true;
        }

        private void Delete_Executed()
        {
            Boolean deleteConfirmed = true;
            if (this.HasAttachedControl())
            {
                ModalMessage msg = new ModalMessage()
                {
                    Title = Message.ProductLibraryMaintenance_DeleteWarning,
                    Header = this.SelectedProductLibraryFileName,
                    MessageIcon = ImageResources.Warning_32,
                    Description = Message.ProductLibraryMaintenance_DeleteIWarningMessage,
                    ButtonCount = 2,
                    Button1Content = Message.Generic_Delete,
                    Button2Content = Message.Generic_Cancel,
                    DefaultButton = ModalMessageButton.Button1,
                    CancelButton = ModalMessageButton.Button2
                };
                App.ShowWindow(msg, true);
                deleteConfirmed = (msg.Result == ModalMessageResult.Button1);
                //deleteConfirmed = Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(this.SelectedProductLibraryFileName);
            }

            //confirm with user
            if (deleteConfirmed)
            {
                base.ShowWaitCursor(true);

                //mark the item as deleted so item changed does not show.
                Ccm.Model.ProductLibrary itemToDelete = this.SelectedProductLibrary.Model;
                itemToDelete.Delete();

                //commit the delete
                try
                {
                    itemToDelete.Save();
                }
                catch (Exception ex)
                {
                    base.ShowWaitCursor(false);

                    RecordException(ex, _exCategory);
                    GetWindowService().ShowErrorOccurredMessage(itemToDelete.Id.ToString(), OperationType.Delete);

                    base.ShowWaitCursor(true);
                }

                //load a new item
                NewCommand.Execute();

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;

        /// <summary>
        /// Closes the current maintenance form
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(
                        p => Close_Executed())
                    {
                        FriendlyName = Message.Generic_Close,
                        SmallIcon = ImageResources.Close_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    RegisterCommand(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private void Close_Executed()
        {
            CloseWindow();
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Event handler for the selected product library model changing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedProductLibrary_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<Model.ProductLibrary> e)
        {
            //Trigger library changed event
            OnSelectedProductLibraryChanged(e.OldModel, e.NewModel);

            OnPropertyChanged(SelectedProductLibraryFileNameProperty);
        }

        /// <summary>
        /// Method to handle the selected product library model changing
        /// </summary>
        /// <param name="oldLibrary"></param>
        /// <param name="newLibrary"></param>
        private void OnSelectedProductLibraryChanged(Model.ProductLibrary oldLibrary, Model.ProductLibrary newLibrary)
        {
            if (oldLibrary != null)
            {
                //Dispose old object if it not the same as the new one.
                if (newLibrary == null || newLibrary.Id != oldLibrary.Id)
                {
                    oldLibrary.Dispose();
                }

                //Detch from old models events
                oldLibrary.PropertyChanged -= SelectedProductLibrary_PropertyChanged;
            }


            if (newLibrary != null)
            {
                String curFilePath = this.FilePath;

                //Trigger properties to update
                if (newLibrary.FileType == ImportDefinitionFileType.Text)
                {
                    this.IsDelimitBoxVisible = true;
                    this.IsExcelWorkbookBoxVisible = this.IsExcelWorkbookTextboxVisible = false;
                }
                else
                {
                    this.IsDelimitBoxVisible = false;
                    this.IsExcelWorkbookBoxVisible = false;
                    this.IsExcelWorkbookTextboxVisible = true;
                }

                //Load data
                DataRetrieval();

                //If text type
                if (newLibrary.FileType == ImportDefinitionFileType.Text)
                {
                    this.IsFixedWidth = newLibrary.TextDataFormat == ImportDefinitionTextDataFormatType.Fixed;
                }

                OnPropertyChanged(IsStandardFilePreviewVisibleProperty);
                OnPropertyChanged(IsFixedWidthTextFilePreviewVisibleProperty);

                if (this.SelectedProductLibrary.Model.IsFirstRowHeaders)
                {
                    OnIsFirstRowColumnHeadersChanged(true);
                    UpdateAvailableColumns();
                }


                //Retreive mapped columns
                foreach (ProductLibraryColumnMapping columnMapping in newLibrary.ColumnMappings)
                {
                    ImportMapping mapping
                        = this.FileData.MappingList.FirstOrDefault(
                            m => String.Compare(m.PropertyName, columnMapping.Destination, /*ignorecase*/true) == 0); // no source but our destination should be defaulted

                    if (mapping == null) continue;

                    //Populate column mappings
                    mapping.PropertyDefault = columnMapping.Default;
                    mapping.PropertyName = columnMapping.Destination; // destination = model object property
                    mapping.PropertyMax = columnMapping.Max;
                    mapping.PropertyMin = columnMapping.Min;
                    mapping.ColumnReference = columnMapping.Source; //source = customer data column

                    //Only set fixed width column start if the mapping is mapped
                    if (mapping.IsColumnReferenceSet)
                    {
                        mapping.FixedWidthColumnStart = columnMapping.TextFixedWidthColumnStart;
                        mapping.FixedWidthColumnEnd = columnMapping.TextFixedWidthColumnEnd;
                    }
                }


                if (this.AttachedControl != null)
                {
                    this.AttachedControl.TriggerUIUpdate();
                }



                //if the template has no filepath
                // then try to reload the previous one.
                if (!this.IsFilePathSet && !String.IsNullOrEmpty(curFilePath))
                {
                    this.FilePath = curFilePath;
                }

                //Attach to new models events
                newLibrary.PropertyChanged += new PropertyChangedEventHandler(SelectedProductLibrary_PropertyChanged);

            }
        }

        /// <summary>
        /// Responds to a change in file path. Checks that it is valid.
        /// </summary>
        private void OnFilePathChanged()
        {
            bool isValid = false;

            //set IsSampleFileIncluded property
            this.IsFilePathSet = !String.IsNullOrEmpty(this.FilePath);

            //update the template name if none is set
            if (String.IsNullOrEmpty(this.CurrentProductLibrary.Name))
            {
                this.CurrentProductLibrary.Name = Path.GetFileNameWithoutExtension(this.FilePath);
            }

            //Ensure fixed width is defaulted
            if (this.IsFixedWidth)
            {
                this.IsFixedWidth = false;
            }

            OnPropertyChanged(IsStandardFilePreviewVisibleProperty);
            OnPropertyChanged(IsFixedWidthTextFilePreviewVisibleProperty);//IsSampleFileIncluded && this.IsFixedWidth;

            if (this.IsFilePathSet)
            {
                //check if the file exists and is of the correct extension
                if (System.IO.File.Exists(this.FilePath))
                {
                    isValid = 
                        ProductLibraryHelper.GetValidDataSourceExtensions(this.SelectedProductLibrary.Model.FileType)
                        .Contains(Path.GetExtension(this.FilePath));
                }
            }
            this.IsFilePathValid = isValid;
        }

        /// <summary>
        /// Responds to a change of the loaded file data
        /// </summary>
        private void OnFileDataChanged(ImportFileData oldValue, ImportFileData newValue)
        {
            if (oldValue != null)
            {
                oldValue.Columns.BulkCollectionChanged -= FileColumnSetDefinitions_BulkCollectionChanged;
                if (oldValue.MappingList != null)
                    _previewColumnSet.Clear();
                _availableColumns.Clear();
            }

            if (newValue != null)
            {
                //Clear available columns
                _availableColumns.Clear();

                //Clear header row
                _headerRow = null;

                newValue.Columns.BulkCollectionChanged += FileColumnSetDefinitions_BulkCollectionChanged;

                //add in row number col
                DataGridColumn rowNumCol = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    Message.ImportDefinition_RowNumberColHeader, ImportFileDataRow.RowNumberProperty.Name, HorizontalAlignment.Center);
                _previewColumnSet.Add(rowNumCol);

                //With preview data columns
                if (this.SelectedProductLibrary.Model.FileType == ImportDefinitionFileType.Text)
                {
                    UpdateFileDataFromTextFile();
                }
                //27931 FIX START
                else
                {
                    //add in all the existing columns
                    foreach (ImportFileDataColumn colDef in _fileData.Columns)
                    {
                        DataGridColumn col = ExtendedDataGrid.CreateReadOnlyTextColumn(colDef.Header,
                                       colDef.GetBindingPath(), FileHelper.ConvertAlignment(colDef.CellAlignment));
                        BindingOperations.SetBinding(col, DataGridColumn.HeaderProperty,
                            new Binding(ImportFileDataColumn.HeaderProperty.Name) { Source = colDef });
                        _previewColumnSet.Add(col);
                    }
                }

                //automap if we suspect this can be done
                GuessMapping();

                //if first row is headers, but no header row set, force load
                if (_headerRow == null && this.SelectedProductLibrary.Model.IsFirstRowHeaders)
                {
                    UpdateHeaderRow();
                }

                //update available columns + add an empty ref
                UpdateAvailableColumns();

                //build up the collection for the preview grid
                UpdatePreviewDataCollection();

                //Try and auto map if new and nothing is mapped
                if (this.SelectedProductLibrary.Model.IsNew && newValue.MappingList.All(p => !p.IsColumnReferenceSet))
                {
                    this.AutoMapCommand.Execute();
                }
            }

            //only update this if the source file changed - may have just changed worksheets
            if ((oldValue != null && newValue != null) ? (oldValue.SourceFilePath != newValue.SourceFilePath) : true)
            {
                UpdateAvailableWorksheets();
            }

            //Ensure no sort/group
            if (this.AttachedControl != null)
            {
                this.AttachedControl.mappingsGrid.SortByDescriptions.Clear();
                this.AttachedControl.mappingsGrid.GroupByDescriptions.Clear();
            }
        }

        /// <summary>
        /// Responds to a change of the is fixed width flag and handles load/unload of related data
        /// </summary>
        /// <param name="newValue"></param>
        private void OnIsFixedWidthChanged(Boolean newValue)
        {
            if (this.SelectedProductLibrary.Model.FileType == ImportDefinitionFileType.Text)
            {
                if (newValue)
                {
                    //Ensure first row contains headers is off as it is disabled for fixed width
                    if (this.SelectedProductLibrary.Model != null)
                    {
                        this.SelectedProductLibrary.Model.IsFirstRowHeaders = false;
                    }

                    //Clear existing columns
                    this._availableColumns.Clear();
                    if (this.FileData.Columns.Count > 0)
                    {
                        this.FileData.Columns.Clear();
                    }

                    //Clear any existing mappings
                    this.ClearAllMappingsCommand.Execute();

                    //Update available columns
                    this.UpdateFileDataFromTextFile();
                    this.UpdateAvailableColumns();

                    //NB - Add any existing mappings handled by other code that triggers this method to begin with.
                }
                else
                {
                    //Reload data
                    OnFileDataChanged(this.FileData, this.FileData);

                    //try to assess what format the data is in
                    GetColumnSeparationType(this.RawPreviewData);

                    //Ensure delimited is popoulated
                    //UpdateFileDataFromTextFile();
                }
            }
        }


        /// <summary>
        /// Responds to changes in the column definitions set
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FileColumnSetDefinitions_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (ImportFileDataColumn colDef in e.ChangedItems)
                    {
                        DataGridColumn col = ExtendedDataGrid.CreateReadOnlyTextColumn(colDef.Header,
                            colDef.GetBindingPath(), FileHelper.ConvertAlignment(colDef.CellAlignment));
                        BindingOperations.SetBinding(col, DataGridColumn.HeaderProperty,
                            new Binding(ImportFileDataColumn.HeaderProperty.Name) { Source = colDef });
                        _previewColumnSet.Add(col);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (ImportFileDataColumn colDef in e.ChangedItems)
                    {
                        DataGridColumn col = _previewColumnSet.FirstOrDefault(c => (c.Header as String) == colDef.Header);
                        _previewColumnSet.Remove(col);
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    _previewColumnSet.Clear();

                    //add the row number column back in 
                    DataGridColumn rowNumCol = ExtendedDataGrid.CreateReadOnlyTextColumn(
                       Message.ImportDefinition_RowNumberColHeader, ImportFileDataRow.RowNumberProperty.Name, HorizontalAlignment.Center);
                    _previewColumnSet.Add(rowNumCol);

                    break;
            }
        }

        /// <summary>
        /// Property changed event handler for the selected product library
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedProductLibrary_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Ccm.Model.ProductLibrary.FileTypeProperty.Name)
            {
                this.IsDelimitBoxVisible = this.SelectedProductLibrary.Model.FileType == ImportDefinitionFileType.Text;

                if (IsDelimitBoxVisible && this.SelectedProductLibrary.Model.TextDelimiterType == null)
                {
                    this.SelectedProductLibrary.Model.TextDelimiterType = ImportDefinitionTextDelimiterType.Comma;
                }
                //reset the file path
                this.FilePath = null;
                this.SelectedWorksheet = null;
            }
            else if (e.PropertyName == Ccm.Model.ProductLibrary.IsFirstRowHeadersProperty.Name)
            {
                //Ensure header row is correct
                OnIsFirstRowColumnHeadersChanged(false);

                //Update available columns
                UpdateAvailableColumns();
            }
            else if (e.PropertyName == Ccm.Model.ProductLibrary.StartAtRowProperty.Name)
            {
                //Refresh preview data
                UpdatePreviewDataCollection();
            }
            else if (e.PropertyName == Ccm.Model.ProductLibrary.TextDataFormatProperty.Name)
            {
                this.ClearAllMappingsCommand.Execute();
                this.IsFixedWidth = this.SelectedProductLibrary.Model.TextDataFormat == ImportDefinitionTextDataFormatType.Fixed;
                this.UpdateFileDataFromTextFile();
            }
            else if (e.PropertyName == Ccm.Model.ProductLibrary.TextDelimiterTypeProperty.Name)
            {
                this.SelectedProductLibrary.Model.IsFirstRowHeaders = false;
                OnFileDataChanged(this.FileData, this.FileData);
            }
        }

        /// <summary>
        /// Responds to the change of the IsFirstRowColumnHeaders
        /// </summary>
        private void OnIsFirstRowColumnHeadersChanged(Boolean loadingProductLibrary)
        {
            base.ShowWaitCursor(true);

            //Logic only applies to excel and delimited text
            if (!this.IsFixedWidth)
            {
                //Reset all column mapping prior to update
                this.ClearAllMappingsCommand.Execute();

                //Only check if a sample file has been included as sample file is optional
                if (this.IsFilePathSet)
                {
                    //Update header row
                    UpdateHeaderRow();

                    //Refresh preview data
                    UpdatePreviewDataCollection();

                    OnPropertyChanged(AreMappingsAllValidProperty);

                    //Only automap if not loading from product library
                    if (!loadingProductLibrary)
                    {
                        if (AutoMap_CanExecute())
                        {
                            AutoMapCommand.Execute();
                        }
                    }
                }
            }

            base.ShowWaitCursor(false);
        }

        private void UpdateHeaderRow()
        {
            //Ensure that if only 1 row but headerrow is populated, that we can still return headerrow to filedata
            //when user unchecks 'First Row is Headers' checkbox
            if ((this.FileData.Rows.Count > 0) || ((this.FileData.Rows.Count == 0) & (_headerRow != null)))
            {
                if (this.SelectedProductLibrary.Model.IsFirstRowHeaders)
                {
                    //Get first row containing headers
                    _headerRow = this.FileData.Rows[0];

                    //Manually update column headers
                    foreach (ImportFileDataColumn colDef in this.FileData.Columns)
                    {
                        if (_headerRow[colDef.ColumnNumber] != null)
                        {
                            colDef.Header = _headerRow[colDef.ColumnNumber].Value.ToString();
                        }
                    }
                }
                else
                {
                    //Manually update column headers
                    Int32 columnIndex = 0;
                    foreach (ImportFileDataColumn colDef in this.FileData.Columns)
                    {
                        colDef.Header = String.Format(CultureInfo.CurrentUICulture, "Column {0}", (columnIndex + 1));
                        columnIndex++;
                    }

                    ///Clear header row save back in
                    if (_headerRow != null)//[SA-14587]
                    {
                        _headerRow = null;
                    }
                }
            }
        }

        private void OnSelectedWorksheetChanged()
        {
            //worksheet changed so that the data needs to be updated
            if (!_isImportInProgress && !this.IsExcelWorkbookTextboxVisible) //only import when file and available worksheets are in place
            {
                //Take copy of whether it was first row headers prior to data load so we know whether to 
                //force a refresh of column headers
                Boolean isFirstRowHeaders = this.SelectedProductLibrary.Model.IsFirstRowHeaders;

                //Load worksheet data
                ImportFromExcelFile(this.FilePath, FileHelper.ResolveWorksheetIndex(this.FilePath, this.SelectedWorksheet));

                //Only trigger load of headers if they were set before load, otherwise no need as import
                //will deal with everything.
                if (isFirstRowHeaders)
                {
                    //Ensure first row data is correct
                    OnIsFirstRowColumnHeadersChanged(false);
                }

                //Ensure available columns are correct
                UpdateAvailableColumns();

                if (String.IsNullOrEmpty(this.SelectedWorksheet) && this.AvailableWorksheets != null && this.AvailableWorksheets.Any())
                {
                    _selectedWorksheet = AvailableWorksheets.First();
                }
            }

        }

        private void OnFixedWidthLinesChanged()
        {
            _fixedWidthLineValueList.Clear();

            _fixedWidthLineValueList.Add(-1); //add start value of the very first column; will become 0 below
            foreach (ImportFixedLineDef line in this.FixedWidthLines.ToList())
            {
                _fixedWidthLineValueList.Add(line.LineValue);
            }

            _fixedWidthLineValueList.Sort();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Method to construct an object containing the preview data for the grid
        /// </summary>
        /// <param name="data"></param>
        private void UpdatePreviewDataCollection()
        {
            if (this.PreviewData != null)
            {
                //Clear down the collection
                this.PreviewData.Clear();
            }

            //Keep track of row start index
            Int32 rowStartIndex = 0;

            //If first row contains headers skip first row
            if (this.SelectedProductLibrary.Model.IsFirstRowHeaders)
            {
                //Skip first row
                rowStartIndex += 1;
            }

            //Row start index
            rowStartIndex = rowStartIndex + (this.SelectedProductLibrary.Model.StartAtRow - 1);

            //Calculate how many rows to preview
            int numRowsToFetch;
            numRowsToFetch = ((FileData.Rows.Count - rowStartIndex) > numPreviewRows) ? numPreviewRows : (FileData.Rows.Count - rowStartIndex);

            //Keep track of row index
            int rowIndex = 0;

            //Loop through each row and populate the collection
            foreach (ImportFileDataRow row in FileData.Rows)
            {
                if (rowIndex >= rowStartIndex
                    && this.PreviewData.Count <= numRowsToFetch)
                {
                    //Construct objects containing this data
                    this.PreviewData.Add(row);
                }
                else if (this.PreviewData.Count >= numRowsToFetch)
                { break; }

                rowIndex += 1;
            }
        }

        /// <summary>
        /// Populates import file data object using source data file
        /// </summary>
        /// <param name="dataContext">The ImportDataContext associated with the current run of the app</param>
        private void DataRetrieval()
        {
            if (this.IsFilePathValid && !String.IsNullOrEmpty(this.FilePath))
            {
                //Load import data from the import source file (Excel or Text)
                if (this.SelectedProductLibrary.Model.FileType == ImportDefinitionFileType.Excel)
                {
                    ImportFromExcelFile(this.FilePath, FileHelper.ResolveWorksheetIndex(this.FilePath, this.SelectedProductLibrary.Model.ExcelWorkbookSheet)); //Run process to import source data from an excel file
                }
                else if (this.SelectedProductLibrary.Model.FileType == ImportDefinitionFileType.Text)
                {
                    _rawPreviewData.Clear();

                    //Start reading the file line by line adding the data rows, using the current character encoding
                    if (System.IO.File.Exists(this.FilePath))
                    {
                        //attempt to detect file encoding
                        Encoding currentEncoding = FileHelper.DetectFileEncoding(this.FilePath);

                        StreamReader reader = new StreamReader(this.FilePath, currentEncoding);
                        while (!reader.EndOfStream)
                        {
                            if (!reader.EndOfStream)
                            {
                                _rawPreviewData.Add(reader.ReadLine());
                            }
                            else
                            {
                                break;
                            }
                        }
                        reader.Close();

                        //try to assess what format the data is in
                        GetColumnSeparationType(this.RawPreviewData);

                        //Load fixed width points
                        LoadFixedWidthLines();
                    }

                    _fileData.MappingList = GetMappingList();
                    _fileData.SourceFilePath = this.FilePath;
                    this.FileData = _fileData;
                }
            }
            //If loading existing and file path cannot be resolved
            else if (!this.IsFilePathValid && String.IsNullOrEmpty(this.FilePath)
                && this.SelectedProductLibrary != null && !this.SelectedProductLibrary.Model.IsNew)
            {
                //Clear file path
                _filePath = String.Empty;
                OnPropertyChanged(FilePathProperty);
                OnPropertyChanged(SelectedProductLibraryFileNameProperty);

                //Clear file data
                ImportFileData fileData = ImportFileData.NewImportFileData();
                fileData.MappingList = GetMappingList();
                this.FileData = fileData;

                //Hide panels
                OnPropertyChanged(IsStandardFilePreviewVisibleProperty);
                OnPropertyChanged(IsFixedWidthTextFilePreviewVisibleProperty);

                this.IsFilePathSet = false;
                
                

                //Show warning
                base.ShowWaitCursor(false);

                //GetWindowService().ShowOkMessage(Ccm.Common.Wpf.Services.MessageWindowType.Warning,
                //    Message.ImportDefinition_ImportTitle_FileMissing,
                //    Message.ProductLibraryMaintenance_SourceFileMissing);

            }
            else
            {
                //Clear file data
                ImportFileData fileData = ImportFileData.NewImportFileData();
                fileData.MappingList = GetMappingList();
                this.FileData = fileData;
            }
        }


        /// <summary>
        /// Returns the mapping list for the given type
        /// </summary>
        /// <param name="dataType"></param>
        /// <returns></returns>
        public IImportMappingList GetMappingList()
        {
            //Return product library mappings
            return ProductLibraryImportMappingList.NewProductLibraryImportMappingList();
        }

        /// <summary>
        /// Updates the column seperation of the data from a text file
        /// </summary>
        public void UpdateFileDataFromTextFile()
        {
            if (this.IsFilePathSet)
            {
                if (this.SelectedProductLibrary.Model.FileType == ImportDefinitionFileType.Text)
                {
                    //clear down the existing structure
                    this.FileData.ClearAll();

                    //get the delimiter if required 
                    Char[] delimiter = null;
                    if (this.SelectedProductLibrary.Model.TextDelimiterType != null)
                    {
                        delimiter = (!this.IsFixedWidth) ? FileHelper.GetDelimiterString((ImportDefinitionTextDelimiterType)this.SelectedProductLibrary.Model.TextDelimiterType).ToCharArray() : null;
                    }

                    //cycle through the rows
                    for (Int32 i = 0; i < this.RawPreviewData.Count; i++)
                    {
                        String row = this.RawPreviewData[i];

                        if (String.IsNullOrEmpty(row))
                        {
                            //skip this item if the row is null or empty.
                            continue;
                        }

                        //split the string into the defined columns
                        List<String> splitLine = new List<String>();
                        if (this.IsFixedWidth)
                        {
                            Int32 lastPoint = 0;
                            foreach (ImportFixedLineDef lineDef in this.FixedWidthLines.OrderBy(l => l.LineValue))
                            {
                                //The start and end of the substring cannot be greater than the string length
                                Int32 startIndex = Math.Min(row.Length, lastPoint);
                                Int32 length = Math.Min(Math.Max(row.Length - lastPoint, 0), lineDef.LineValue - lastPoint);

                                String colString = row.Substring(startIndex, length);
                                splitLine.Add(colString);
                                lastPoint = lineDef.LineValue;
                            }
                        }
                        else
                        {
                            if (delimiter != null)
                            {
                                splitLine.AddRange(row.Split(delimiter));
                            }
                        }

                        //create a new import data row
                        ImportFileDataRow dataRow = ImportFileDataRow.NewImportFileDataRow(i + 1);

                        Int32 colNumber = 1;
                        foreach (String colString in splitLine)
                        {
                            String cellValue = colString.TrimEnd(' ');

                            //check if we have a column, if not then add
                            if (this.FileData.Columns.Count < colNumber)
                            {
                                ImportFileDataColumn column = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                                column.Header = String.Format(Message.ImportDefinition_ImportColumnName, colNumber);
                                column.CellAlignment = ImportFileDataHorizontalAlignment.Left;

                                this.FileData.Columns.Add(column);
                            }

                            //add the cell data
                            dataRow.Cells.Add(ImportFileDataCell.NewImportFileDataCell(colNumber, cellValue));

                            colNumber++;
                        }

                        this.FileData.Rows.Add(dataRow);
                    }
                }
            }
        }

        private void UpdateAvailableWorksheets()
        {
            //populate available worksheets
            _availableWorksheets.Clear();

            if (this.FileData != null)
            {
                String fileExt = Path.GetExtension(_fileData.SourceFilePath);
                if (fileExt == ".xlsx" || fileExt == ".xls")
                {
                    foreach (String str in FileHelper.LoadSelectedFileWorksheets(_fileData.SourceFilePath))
                    {
                        _availableWorksheets.Add(str);
                    }
                }
            }

            if (_availableWorksheets.Contains(this.SelectedProductLibrary.Model.ExcelWorkbookSheet))
            {
                this.SelectedWorksheet = _availableWorksheets.FirstOrDefault(p => p.Equals(this.SelectedProductLibrary.Model.ExcelWorkbookSheet));
            }
            else
            {
                this.SelectedWorksheet = _availableWorksheets.FirstOrDefault();
            }
        }

        /// <summary>
        /// Method to update the available columns collection from the file data columns
        /// </summary>
        public void UpdateAvailableColumns()
        {
            if (_fileData == null) return;

            base.ShowWaitCursor(true);

            //update available columns + add an empty ref
            if (_fileData.Columns.Any())
            {
                //Clear existing
                _availableColumns.Clear();
                //Add empty ref
                _availableColumns.Add(ImportMapping.EmptyColumnReference);
                //Add column for each file data column
                foreach (ImportFileDataColumn col in _fileData.Columns)
                {
                    _availableColumns.Add(col.Header);
                }
            }
            else if (!this.IsFilePathSet)
            {
                //we are loading without an excel spreadsheet
                //so load the mappings directly from the file
                foreach (ProductLibraryColumnMapping mapping in
                     this.SelectedProductLibrary.Model.ColumnMappings)
                {
                    _availableColumns.Add(mapping.Source);
                }
            }

            base.ShowWaitCursor(false);

        }

        /// <summary>
        /// Method to handle updating the fixed width line points from the column mappings
        /// </summary>
        private void LoadFixedWidthLines()
        {
            //Load existing fixed width lines for existing library
            if (this.SelectedProductLibrary.Model.TextDataFormat == ImportDefinitionTextDataFormatType.Fixed
                    && this.SelectedProductLibrary.Model.ColumnMappings.Count > 0)
            {
                //clear existing
                this.FixedWidthLines.Clear();

                //enumerate through and add new lines
                foreach (ProductLibraryColumnMapping mapping in this.SelectedProductLibrary.Model.ColumnMappings)
                {
                    //ensure source is set
                    if (!String.IsNullOrEmpty(mapping.Source))
                    {
                        this.FixedWidthLines.Add(new ImportFixedLineDef(mapping.TextFixedWidthColumnEnd));
                    }
                }
            }
        }

        /// <summary>
        /// Translates user defined line positions into column start end values
        /// </summary>
        internal void MapFixedColumnWidthPositions()
        {
            _fixedWidthLineValueList.Clear();
            OnFixedWidthLinesChanged();

            foreach (ImportMapping mapping in this.FileData.MappingList)
            {
                mapping.FixedWidthColumnEnd = null;
                mapping.FixedWidthColumnStart = null;

                try
                {
                    Int32 columnNumber = this.FileData.GetMappedColNumber(mapping);
                    if (columnNumber > 0)
                    {
                        mapping.FixedWidthColumnEnd = FixedWidthLineValueList[columnNumber];
                        mapping.FixedWidthColumnStart = FixedWidthLineValueList[columnNumber - 1] + 1;
                    }
                }
                catch
                {
                    //parse failed; cannot autopopulate start / end
                }
            }
        }


        /// <summary>
        /// Shows a warning requesting user ok to continue if current item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            return GetWindowService().ContinueWithItemChange(this.SelectedProductLibrary.Model, this.SaveCommand);
        }


        //internal Int32 SilentResolveWorksheetIndex()
        //{
        //    Int32 workbookIndex = 0;

        //    if (!String.IsNullOrEmpty(this.FilePath))
        //    {
        //        //Load available worksheets
        //        this._availableWorksheets = FileHelper.LoadSelectedFileWorksheets(this.FilePath);

        //        if (this.SelectedWorksheet == null && _availableWorksheets.Count > 0)
        //        {
        //            this._selectedWorksheet = _availableWorksheets.FirstOrDefault();
        //        }


        //        //find index of the worksheet containing data
        //        String workbookSheetName = this.SelectedWorksheet;
        //        if (!String.IsNullOrEmpty(workbookSheetName))
        //        {

        //            if (!String.IsNullOrEmpty(workbookSheetName))
        //            {
        //                if (this._availableWorksheets.Contains(workbookSheetName))
        //                {
        //                    workbookIndex = this._availableWorksheets.IndexOf(workbookSheetName);
        //                }
        //            }
        //        }
        //    }
        //    return workbookIndex;
        //}

        /// <summary>
        /// Makes an attempt to guess if the file is fixed width or not
        /// </summary>
        private void GetColumnSeparationType(IEnumerable<String> dataLines)
        {
            if (this.SelectedProductLibrary.Model.IsNew)
            {
                this.SelectedProductLibrary.Model.PropertyChanged -= SelectedProductLibrary_PropertyChanged;

                //get the first 2 lines
                String[] dataLinesCut = dataLines.Take(2).ToArray();
                Int32 numLines = dataLinesCut.Length;

                //check through each delimiter to see if it repeats in each line
                Char delimiter;
                Int32 line1Count = 0;
                Int32 line2Count = 0;

                //check through the delimiters
                foreach (ImportDefinitionTextDelimiterType type in Enum.GetValues(typeof(ImportDefinitionTextDelimiterType)))
                {
                    //dont check for tab
                    if (type != ImportDefinitionTextDelimiterType.Tab)
                    {
                        if (dataLinesCut.Any())
                        {
                            delimiter = FileHelper.GetDelimiterString(type)[0];
                            line1Count = dataLinesCut[0].Count(c => c == delimiter);
                            if (numLines > 1)
                            {
                                //We have 2 lines, so check the number of delimiters are equal
                                line2Count = dataLinesCut[1].Count(c => c == delimiter);
                                if ((line1Count == line2Count) && (line1Count != 0))
                                {
                                    this.SelectedProductLibrary.Model.TextDataFormat = ImportDefinitionTextDataFormatType.Delimited;
                                    this.SelectedProductLibrary.Model.TextDelimiterType = type;
                                    return;
                                }
                            }
                            else
                            {
                                //we have only one line, so make a best guess
                                if (line1Count > 1)
                                {
                                    //this delimiter character exists more than once in this line, so assume it is valid
                                    this.SelectedProductLibrary.Model.TextDataFormat = ImportDefinitionTextDataFormatType.Delimited;
                                    this.SelectedProductLibrary.Model.TextDelimiterType = type;
                                    return;
                                }
                            }
                        }
                    }
                }

                //if we still havent returned out then mark the file as fixed width
                this.SelectedProductLibrary.Model.TextDataFormat = ImportDefinitionTextDataFormatType.Fixed;

                this.SelectedProductLibrary.Model.PropertyChanged += SelectedProductLibrary_PropertyChanged;
            }

            //Trigger UI update
            if (this.AttachedControl != null)
            {
                this.AttachedControl.OnViewBoundryChanged();
            }
        }


        /// <summary>
        /// Performs a quick check on the data to see if the first row is headers
        /// if so then sets the cheack and automaps
        /// </summary>
        private void GuessMapping()
        {
            if (this.FileData.Rows.Count > 0)
            {
                //Check if the first row is possibly headers
                ImportFileDataRow row1 = this.FileData.Rows[0];
                Int32 colsCount = row1.Cells.Count;
                Int32 matchCount = 0;

                foreach (ImportFileDataCell cell in row1.Cells)
                {
                    //remove all spaces and convert to lowercase
                    String cellValue = cell.Value.ToString().Replace(" ", "").ToLowerInvariant();

                    ImportMapping matchedMapping =
                        this.FileData.MappingList.FirstOrDefault(m => m.PropertyName.Replace(" ", "").ToLowerInvariant() == cellValue);

                    if (matchedMapping != null)
                    {
                        matchCount++;
                    }
                }

                //if over half match then automap
                if (matchCount > (colsCount / 2))
                {
                    if (!this.SelectedProductLibrary.Model.IsFirstRowHeaders)
                    {
                        this.SelectedProductLibrary.Model.IsFirstRowHeaders = true; //this should in attempt AutoMap OnIsFirstRowHeadersChanged
                    }
                }
            }
        }

        #region Import

        private void ImportFromExcelFile(String filePath, Int32 worksheetIndex)
        {
            _timer.Restart();

            //start the import worker
            Csla.Threading.BackgroundWorker importWorker = new Csla.Threading.BackgroundWorker();
            importWorker.DoWork += ImportWorker_DoWork;
            importWorker.RunWorkerCompleted += ImportWorker_RunWorkerCompleted;
            importWorker.WorkerReportsProgress = false;
            importWorker.WorkerSupportsCancellation = false;

            importWorker.RunWorkerAsync(new object[] { filePath, worksheetIndex });

            //show busy
            if (this.AttachedControl != null)
            {
                _busyDialog = new ModalBusy();
                _busyDialog.Header = Message.ImportDefinition_Validation_BusyImportingHeader;
                _busyDialog.Description = Message.ImportDefinition_Validation_BusyImportingDescription;
                _busyDialog.IsDeterminate = false;
                _busyDialog.Owner = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<ExtendedRibbonWindow>(this.AttachedControl);
                App.ShowWindow(_busyDialog, /*isModal*/true);
            }
        }

        private void ImportWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            object[] args = (object[])e.Argument;
            String filePath = (String)args[0];
            Int32 worksheetIndex = (Int32)args[1];

            _hasOutOfMemoryException = false;
            _hasIOException = false;
            _hasFileMissingException = false;

            _isImportInProgress = true;

            //create this as a new object so that we don't get threading issues
            ImportFileData fileData = ImportFileData.NewImportFileData();

            fileData.SourceFilePath = filePath;

            if (!String.IsNullOrEmpty(filePath))
            {
                //Check file still exists
                if (!System.IO.File.Exists(filePath))
                {
                    //File doesn't exist, flag for exception to be displayed
                    _hasFileMissingException = true;
                }
                else
                {
                    DataTable tableData = FileHelper.LoadSpecificWorksheet(filePath, worksheetIndex, /*isFirstRowHeaders*/false,
                        ref _hasOutOfMemoryException, ref _hasIOException);

                    if (tableData != null)
                    {
                        //setup columns
                        Int32 colNumber = 1;
                        foreach (DataColumn col in tableData.Columns)
                        {
                            ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                            importColumn.Header = col.ColumnName;
                            importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                            fileData.Columns.Add(importColumn);

                            colNumber++;
                        }

                        //setup rows
                        Int32 rowNumber = 1;
                        foreach (DataRow row in tableData.Rows)
                        {
                            ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);
                            Int32 rowItemArrayCount = row.ItemArray.Count();

                            //cycle through adding cells
                            for (Int32 cellIndex = 0; cellIndex < rowItemArrayCount; cellIndex++)
                            {
                                ImportFileDataCell importCell =
                                    ImportFileDataCell.NewImportFileDataCell(cellIndex + 1, row.ItemArray[cellIndex]);
                                importRow.Cells.Add(importCell);
                            }

                            // through adding rows
                            fileData.Rows.Add(importRow);

                            rowNumber++;
                        }
                    }
                }
            }
            e.Result = fileData;
        }

        private void ImportWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Csla.Threading.BackgroundWorker importWorker = (Csla.Threading.BackgroundWorker)sender;
            importWorker.DoWork -= ImportWorker_DoWork;
            importWorker.RunWorkerCompleted -= ImportWorker_RunWorkerCompleted;

            if (_hasFileMissingException)
            {
                //Hide busy
                if (_busyDialog != null)
                {
                    _busyDialog.Close();
                    _busyDialog = null;
                }

                if (this.AttachedControl != null)
                {
                    ModalMessage errorMessageBox = new ModalMessage();
                    errorMessageBox.Header = Message.ImportDefinition_ImportTitle_FileMissing;
                    errorMessageBox.Description = Message.ImportDefinition_FileMissing;
                    errorMessageBox.ButtonCount = 1;
                    errorMessageBox.Button1Content = Message.Generic_OK;
                    errorMessageBox.Owner = this.AttachedControl;
                    errorMessageBox.ShowInTaskbar = true;
                    App.ShowWindow(errorMessageBox, this.AttachedControl, true);
                }
            }
            else if (_hasOutOfMemoryException)
            {
                //Hide busy
                if (_busyDialog != null)
                {
                    _busyDialog.Close();
                    _busyDialog = null;
                }

                if (this.AttachedControl != null)
                {
                    ModalMessage errorMessageBox = new ModalMessage();
                    errorMessageBox.Header = Message.ImportDefinition_ImportTitle_FileTooLarge;
                    errorMessageBox.Description = Message.ImportDefinition_FileTooLarge;
                    errorMessageBox.ButtonCount = 1;
                    errorMessageBox.Button1Content = Message.Generic_OK;
                    errorMessageBox.Owner = this.AttachedControl;
                    errorMessageBox.ShowInTaskbar = true;
                    App.ShowWindow(errorMessageBox, this.AttachedControl, true);
                }
            }
            else if (_hasIOException)
            {
                //Hide busy 
                if (_busyDialog != null)
                {
                    _busyDialog.Close();
                    _busyDialog = null;
                }

                if (this.AttachedControl != null)
                {
                    ModalMessage errorMessageBox = new ModalMessage();
                    errorMessageBox.Header = Message.ImportDefinition_ImportTitle;
                    errorMessageBox.Description = Message.ImportDefinition_FileInUse;
                    errorMessageBox.ButtonCount = 1;
                    errorMessageBox.Button1Content = Message.Generic_OK;
                    errorMessageBox.Owner = this.AttachedControl;
                    errorMessageBox.ShowInTaskbar = true;
                    App.ShowWindow(errorMessageBox, this.AttachedControl, true);
                }
            }
            else if (e.Error != null)
            {
                //Hide busy 
                if (_busyDialog != null)
                {
                    _busyDialog.Close();
                    _busyDialog = null;
                }

                //If any other exception is detected which is not handled
                if (AttachedControl != null)
                {
                    var errorMessageBox = new ModalMessage();
                    errorMessageBox.Header = Message.ImportDefinition_ImportTitle_ImportFailed;
                    errorMessageBox.Description = Message.ImportDefinition_ImportTitle_ImportFailedDescription;
                    errorMessageBox.ButtonCount = 1;
                    errorMessageBox.Button1Content = Message.Generic_OK;
                    errorMessageBox.ShowInTaskbar = true;
                    App.ShowWindow(errorMessageBox, true);
                }
            }
            else
            {
                //The file data was imported so we can continue
                ImportFileData fileData = ImportFileData.NewImportFileData();

                fileData = (ImportFileData)e.Result;

                fileData.MappingList = GetMappingList();
                fileData.SourceFilePath = this.FilePath;
                this.FileData = fileData;

                _isImportInProgress = false;
            }

            if (_busyDialog != null)
            {
                _busyDialog.Close();
                _busyDialog = null;
            }

            _timer.Stop();
            Debug.WriteLine(String.Format("Import from Excel: {0} secs", _timer.Elapsed.TotalSeconds));
        }

        #endregion

        #endregion

        #region IDisposable

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    //Set model to null to clear attached events
                    this.SelectedProductLibrary.Model = null;
                    //Clear file data
                    _fileData.Columns.BulkCollectionChanged -= FileColumnSetDefinitions_BulkCollectionChanged;
                    _fileData = null;
                    _rawPreviewData.Clear();
                    _rawPreviewData = null;
                    _previewData.Clear();
                    _previewData = null;
                    _previewColumnSet.Clear();
                    _previewColumnSet = null;
                    _fixedWidthLines.Clear();
                    _availableColumns.Clear();
                    _fixedWidthLineValueList.Clear();
                    _headerRow = null;

                    base.DisposeBase();
                }
                base.IsDisposed = true;
            }
        }

        #endregion

        #region IDataErrorInfo

        string IDataErrorInfo.Error
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Implementation of the IDataErrorInfo interface
        /// </summary>
        /// <param name="columnName">Property name</param>
        /// <returns>String containing error</returns>
        string IDataErrorInfo.this[String columnName]
        {
            get
            {
                String result = null;

                if (columnName == SelectedWorksheetProperty.Path)
                {
                    if (this.SelectedProductLibrary.Model.FileType == ImportDefinitionFileType.Excel
                        && String.IsNullOrEmpty(this.SelectedWorksheet)
                        && String.IsNullOrWhiteSpace(this.SelectedWorksheet))
                    {
                        result = Message.ProductLibraryMaintenance_SaveDisabledReason_ExcelNoWorkbookSelected;
                    }
                }

                return result;
            }
        }
        #endregion
    }

    /// <summary>
    /// Class for help manage the import file fixed line definition
    /// </summary>
    public class ImportFixedLineDef
    {
        #region Fields

        private Int32 _lineValue;

        #endregion

        #region Properties

        public Int32 LineValue
        {
            get { return _lineValue; }
            set { _lineValue = value; }
        }

        #endregion

        #region Constructor

        public ImportFixedLineDef(Int32 lineValue)
        {
            _lineValue = lineValue;
        }

        #endregion
    }
}
