﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (V8 1.0)
// V8-25395 : A.Probyn
//  Created
// V8-27648 : A.Probyn ~ Updated to support ProductLibraryProduct
// V8-27986 : A.Probyn ~ Updated logic when adding fixed width lines
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using System.Collections.ObjectModel;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using System.Collections.Specialized;
using System.Windows.Controls.Primitives;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.Helpers;
using System.ComponentModel;
using Galleria.Ccm.Common.Wpf.Misc;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Editor.Client.Wpf.ProductLibraryMaintenance
{
    /// <summary>
    /// Interaction logic for ProductLibraryMaintenanceOrganiser.xaml
    /// </summary>
    public partial class ProductLibraryMaintenanceOrganiser : ExtendedRibbonWindow
    {
        #region Constants

        const Int32 charSize = 7;
        const Int32 rulerHeight = 35;

        const String AvailableColumnsCollectionKey = "ImportDefinition_AvailableColumnsCollection";

        #endregion

        #region Fields

        //private String _preloadFilePath;
        private readonly ProductLibraryInfoListViewModel _infoListView = new ProductLibraryInfoListViewModel();
        #endregion

        #region Properties

        #region ViewModel

        /// <summary>
        /// ViewModel dependency property definition
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ProductLibraryMaintenanceViewModel), typeof(ProductLibraryMaintenanceOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets the viewmodel controller.
        /// </summary>
        public ProductLibraryMaintenanceViewModel ViewModel
        {
            get { return (ProductLibraryMaintenanceViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        /// <summary>
        /// Called whenver the viewmodel controller changes
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ProductLibraryMaintenanceOrganiser senderControl = (ProductLibraryMaintenanceOrganiser)obj;

            String clearSelectedMappingsCommandKey = "ClearSelectedMappingsCommand";
            String clearSelectedMappingCommandKey = "ClearSelectedMappingCommand";

            if (e.OldValue != null)
            {
                ProductLibraryMaintenanceViewModel oldModel = (ProductLibraryMaintenanceViewModel)e.OldValue;
                oldModel.UnregisterWindowControl();
                oldModel.FixedWidthLines.CollectionChanged -= senderControl.FixedWidthLines_CollectionChanged;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
                senderControl.Resources.Remove(clearSelectedMappingsCommandKey);
                senderControl.Resources.Remove(clearSelectedMappingCommandKey);

                ((INotifyCollectionChanged)oldModel.AvailableColumns).CollectionChanged -= senderControl.ViewModelAvailableColumns_CollectionChanged;


            }
            if (e.NewValue != null)
            {
                ProductLibraryMaintenanceViewModel newModel = (ProductLibraryMaintenanceViewModel)e.NewValue;
                newModel.RegisterWindowControl(senderControl);
                newModel.FixedWidthLines.CollectionChanged += senderControl.FixedWidthLines_CollectionChanged;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
                senderControl.Resources.Add(clearSelectedMappingsCommandKey, newModel.ClearSelectedMappingsCommand);
                senderControl.Resources.Add(clearSelectedMappingCommandKey, newModel.ClearSelectedMappingCommand);

                ((INotifyCollectionChanged)newModel.AvailableColumns).CollectionChanged += senderControl.ViewModelAvailableColumns_CollectionChanged;
                senderControl.ResetAvailableColumnsCollection(newModel.AvailableColumns);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="viewModel">the viewmodel controller.</param>
        public ProductLibraryMaintenanceOrganiser(ProductLibraryMaintenanceViewModel viewModel)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            //start loading
            InitializeComponent();

            this.ViewModel = viewModel;

            this.Loaded += ProductLibraryMaintenanceOrganiser_Loaded;

            this.Loaded += new RoutedEventHandler(ProductLibraryMaintenanceOrganiser_FirstTimeLoaded);

            this.KeyDown += ProductLibraryMaintenanceOrganiser_KeyDown;
        }

        #endregion

        #region Event Handler

        /// <summary>
        /// Called whenever the backstage open tab needs to refresh repository items.
        /// </summary>
        private void BackstageOpenTabItem_RefreshRepositoryItems(object sender, EventArgs e)
        {
            BackstageOpenTabItem tab = (BackstageOpenTabItem)sender;

            if (tab.RepositoryItems == null) tab.RepositoryItems = _infoListView.BindableCollection;

            if (App.ViewState.IsConnectedToRepository)
            {
                _infoListView.FetchAllForEntityAsync();
            }
        }

        /// <summary>
        /// Loaded event handler for the product library maintenance window
        /// </summary>
        private void ProductLibraryMaintenanceOrganiser_FirstTimeLoaded(object sender, RoutedEventArgs e)
        {
            ProductLibraryMaintenanceOrganiser win = (ProductLibraryMaintenanceOrganiser)sender;
            win.Loaded -= ProductLibraryMaintenanceOrganiser_FirstTimeLoaded;


            //ResetAvailableColumnsCollection(this.ViewModel.AvailableColumns);

            //Preload file details
            //if (!String.IsNullOrEmpty(_preloadFilePath))
            //{
            //    //Set file type
            //    this.ViewModel.SelectedProductLibrary.Model.FileType = FileHelper.GetFileTypeFromExtension(_preloadFilePath);

            //    //Set file path
            //    this.ViewModel.FilePath = _preloadFilePath;
            //}
            //else
            //{
            //    this.ViewModel.SelectedProductLibrary.Model.FileType = ImportDefinitionFileType.Excel;
            //    this.ViewModel.FilePath = String.Empty;
            //}
        }

        /// <summary>
        /// Loaded event handler for the product library maintenance window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductLibraryMaintenanceOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel != null)
            {
                if (this.ViewModel.IsFixedWidth == true)
                {
                    this.xFixedWidthColumStart.Visibility = System.Windows.Visibility.Visible;
                    this.xFixedWidthColumEnd.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    this.xFixedWidthColumStart.Visibility = System.Windows.Visibility.Hidden;
                    this.xFixedWidthColumEnd.Visibility = System.Windows.Visibility.Hidden;
                }


                if (this.ViewModel.IsFilePathSet == true)
                {
                    xSourceColumnNoFile.Visibility = System.Windows.Visibility.Hidden;
                    xSourceColumn.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    xSourceColumnNoFile.Visibility = System.Windows.Visibility.Visible;
                    xSourceColumn.Visibility = System.Windows.Visibility.Hidden;
                }

            }

            Galleria.Framework.Controls.Wpf.Helpers.QueueAction((Action)(() => OnViewBoundryChanged()));

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        /// <summary>
        /// Called whenever a property changes on the view model controller.
        /// </summary>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ProductLibraryMaintenanceViewModel.IsFixedWidthProperty.Path)
            {
                if (this.ViewModel.IsFixedWidth == true)
                {
                    xFixedWidthColumStart.Visibility = System.Windows.Visibility.Visible;
                    xFixedWidthColumEnd.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    xFixedWidthColumStart.Visibility = System.Windows.Visibility.Hidden;
                    xFixedWidthColumEnd.Visibility = System.Windows.Visibility.Hidden;
                }

            }
            else if (e.PropertyName == ProductLibraryMaintenanceViewModel.IsStandardFilePreviewVisibleProperty.Path)
            {
                filePreviewExpander.IsExpanded = (this.ViewModel.IsStandardFilePreviewVisible);
            }
            else if (e.PropertyName == ProductLibraryMaintenanceViewModel.IsFixedWidthTextFilePreviewVisibleProperty.Path
                && (!this.ViewModel.IsStandardFilePreviewVisible))
            {
                //only use this if the standard preview isn't used
                fileFixedWidthTextPreviewExpander.IsExpanded = (this.ViewModel.IsFixedWidthTextFilePreviewVisible);
            }
            if (e.PropertyName == ProductLibraryMaintenanceViewModel.IsFilePathSetProperty.Path)
            {
                if (this.ViewModel.IsFilePathSet == true)
                {
                    xSourceColumnNoFile.Visibility = System.Windows.Visibility.Hidden;
                    xSourceColumn.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    xSourceColumnNoFile.Visibility = System.Windows.Visibility.Visible;
                    xSourceColumn.Visibility = System.Windows.Visibility.Hidden;
                }

            }
        }

        /// <summary>
        /// Event handler for the key down event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductLibraryMaintenanceOrganiser_KeyDown(object sender, KeyEventArgs e)
        {
            base.OnKeyDown(e);

            //If Ctrl+O selected, set backstage to true and select open tab
            if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
            {
                this.RibbonBackstage.IsOpen = true;
                this.BackstageOpenTab.IsSelected = true;
            }
        }

        /// <summary>
        /// Responds to changes in the view model available columns collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModelAvailableColumns_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            ColumnHeaderCollection resCollection = this.Resources[AvailableColumnsCollectionKey] as ColumnHeaderCollection;

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (String s in e.NewItems)
                    {
                        resCollection.Add(s);
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (String s in e.OldItems)
                    {
                        resCollection.Remove(s);
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    ResetAvailableColumnsCollection((IEnumerable<String>)sender);
                    break;
            }
        }

        /// <summary>
        /// Collection changed handler for the fixed width lines
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FixedWidthLines_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (ImportFixedLineDef lineDef in e.NewItems)
                    {
                        AddLine(lineDef, this.rulerCanvas);
                        //Add new column
                        this.ViewModel.UpdateFileDataFromTextFile();
                        //End

                        //Ensure lines are mapped
                        this.ViewModel.MapFixedColumnWidthPositions();
                        //Ensure available columns reflects change
                        this.ViewModel.UpdateAvailableColumns();
                        this.ViewModel.AutoMapCommand.Execute();
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (ImportFixedLineDef lineDef in e.OldItems)
                    {
                        ImportDataFixedWidthLine line =
                            this.rulerCanvas.Children.OfType<ImportDataFixedWidthLine>()
                            .FirstOrDefault(l => l.LineDef == lineDef);

                        this.rulerCanvas.Children.Remove(line);

                        //Update columns
                        this.ViewModel.UpdateFileDataFromTextFile();
                        this.ViewModel.UpdateAvailableColumns();
                        //End

                        //Ensure lines are mapped
                        this.ViewModel.MapFixedColumnWidthPositions();
                        this.ViewModel.AutoMapCommand.Execute();

                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    OnViewBoundryChanged();
                    this.ViewModel.MapFixedColumnWidthPositions();
                    this.ViewModel.AutoMapCommand.Execute();
                    break;
            }
        }

        /// <summary>
        /// Mouse down event handler for the ruler canvas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RulerCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Point hitPoint = e.GetPosition(this.rulerCanvas);
            Int32 horizPoint = Convert.ToInt32(hitPoint.X / charSize);

            //if the line point does not already exist then add it
            ImportFixedLineDef existingDef = this.ViewModel.FixedWidthLines.FirstOrDefault(l => l.LineValue == horizPoint);
            if (existingDef == null)
            {
                this.ViewModel.FixedWidthLines.Add(new ImportFixedLineDef(horizPoint));
            }
        }

        /// <summary>
        /// Method to handle the view boundary changings
        /// </summary>
        internal void OnViewBoundryChanged()
        {
            //remove all children from the canvas
            List<ImportDataFixedWidthLine> childLines =
                this.rulerCanvas.Children.OfType<ImportDataFixedWidthLine>().ToList();
            foreach (ImportDataFixedWidthLine line in childLines)
            {
                this.rulerCanvas.Children.Remove(line);
            }

            //add in the new lines
            if (this.ViewModel != null)
            {
                DrawRuler();

                foreach (ImportFixedLineDef linePoint in this.ViewModel.FixedWidthLines)
                {
                    AddLine(linePoint, this.rulerCanvas);
                }
            }
        }

        /// <summary>
        /// Double click event for a line
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Line_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ImportDataFixedWidthLine line = (ImportDataFixedWidthLine)sender;
            line.MouseDoubleClick += new MouseButtonEventHandler(Line_MouseDoubleClick);

            //remove the line
            this.ViewModel.FixedWidthLines.Remove(line.LineDef);
            this.rulerCanvas.Children.Remove(line);

        }

        /// <summary>
        /// Scroll changed event handler for the raw data grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RawDataGrid_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            //update the canvas position
            Canvas.SetLeft(rulerCanvas, -e.HorizontalOffset);
        }

        /// <summary>
        /// Handler for the file preview expanding
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FilePreviewExpander_Expanded(object sender, RoutedEventArgs e)
        {
            mappingsGridRow.Height = new GridLength(0.1, GridUnitType.Star);
            filePreviewRow.Height = new GridLength(0.1, GridUnitType.Star);
        }

        /// <summary>
        /// Handler for the file preview collapsing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FilePreviewExpander_Collapsed(object sender, RoutedEventArgs e)
        {
            mappingsGridRow.Height = new GridLength(0.1, GridUnitType.Star);
            filePreviewRow.Height = new GridLength(0, GridUnitType.Auto);
        }

        /// <summary>
        /// Handler for a width line drag completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Line_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            ImportDataFixedWidthLine line = (ImportDataFixedWidthLine)sender;

            this.ViewModel.UpdateFileDataFromTextFile();
            this.ViewModel.MapFixedColumnWidthPositions();
            this.ViewModel.AutoMapCommand.Execute();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Reset the available columns collection
        /// </summary>
        /// <param name="sourceCollection"></param>
        private void ResetAvailableColumnsCollection(IEnumerable<String> sourceCollection)
        {
            ColumnHeaderCollection resCollection = this.Resources[AvailableColumnsCollectionKey] as ColumnHeaderCollection;
            if (resCollection != null)
            {
                resCollection.Clear();

                if (sourceCollection != null)
                {
                    foreach (String columnName in sourceCollection.OrderBy(s => s))
                    {
                        resCollection.Add(columnName);
                    }
                }
            }
        }

        /// <summary>
        /// Draws the ruler object for fixed width lines
        /// </summary>
        private void DrawRuler()
        {
            if (this.ViewModel.IsFilePathSet)
            {
                if (this.ViewModel.SelectedProductLibrary.Model.FileType == Model.ImportDefinitionFileType.Text
                    && this.ViewModel.RawPreviewData.Count > 0)
                {
                    //get the max width of the raw lines
                    Int32 maxLineLength = this.ViewModel.RawPreviewData.Max(s => s.Length);

                    //round this up to 10.
                    Int32 roundedMaxLineLength = maxLineLength;
                    while (!(roundedMaxLineLength % 10 == 0))
                    {
                        roundedMaxLineLength++;
                    }

                    Int32 drawWidth = roundedMaxLineLength * charSize;
                    this.rulerCanvas.Width = drawWidth;

                    //for each point draw a line
                    //if divisable by 10 draw a major line
                    for (Int32 i = 1; i <= roundedMaxLineLength; i++)
                    {
                        Int32 left = i * charSize;

                        //major line
                        if (i % 10 == 0)
                        {
                            //create a marker textblock
                            TextBlock nbr = new TextBlock();
                            nbr.Text = String.Format("{0}", i);
                            nbr.FontSize = 11;
                            nbr.Width = nbr.Text.Length * charSize;
                            Canvas.SetLeft(nbr, left - (nbr.Width / 2)); //set to center
                            Canvas.SetTop(nbr, 5);
                            this.rulerCanvas.Children.Add(nbr);

                            //add the marker line
                            Rectangle markerLine = new Rectangle();
                            Canvas.SetLeft(markerLine, i * charSize); //set to center
                            Canvas.SetTop(markerLine, 25);
                            markerLine.Height = 10;
                            markerLine.Width = 1;
                            markerLine.Stroke = Brushes.Black;
                            this.rulerCanvas.Children.Add(markerLine);
                        }
                        else
                        {
                            //add the marker line
                            Rectangle markerLine = new Rectangle();
                            Canvas.SetLeft(markerLine, i * charSize); //set to center
                            Canvas.SetTop(markerLine, 30);
                            markerLine.Height = 5;
                            markerLine.Width = 1;
                            markerLine.Stroke = Brushes.Black;
                            this.rulerCanvas.Children.Add(markerLine);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Returns the boundry in which lines should be displayed
        /// </summary>
        /// <returns></returns>
        private Rect GetViewBoundary()
        {
            Rect boundry = new Rect();
            boundry.Height = this.rulerCanvas.ActualHeight;
            boundry.Width = this.rulerCanvas.Width;

            return boundry;
        }

        /// <summary>
        /// Adds a new line to the given canvas
        /// </summary>
        /// <param name="linePoint"></param>
        /// <param name="cnvas"></param>
        private void AddLine(ImportFixedLineDef linePoint, Canvas cnvas)
        {
            ImportDataFixedWidthLine line = new ImportDataFixedWidthLine(linePoint);
            line.Height = cnvas.ActualHeight;
            Canvas.SetTop(line, rulerHeight);
            cnvas.Children.Add(line);

            //subscribe to the child dragged and double clicked
            line.MouseDoubleClick += new MouseButtonEventHandler(Line_MouseDoubleClick);
            line.DragCompleted += new DragCompletedEventHandler(Line_DragCompleted);
        }

        /// <summary>
        /// Method to triger ui update from the controller
        /// </summary>
        internal void TriggerUIUpdate()
        {
            //Set delayed update of boundary changed
            Galleria.Framework.Controls.Wpf.Helpers.QueueAction((Action)(() => this.OnViewBoundryChanged()));
        }

        #endregion

        #region Window Close

        /// <summary>
        /// On the window being closed, checked if changes may require saving
        /// </summary>
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                if (!this.ViewModel.ContinueWithItemChange())
                {
                    e.Cancel = true;
                }

            }
        }

        /// <summary>
        /// Calls the cancel command on cross clicked
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            this.KeyDown -= ProductLibraryMaintenanceOrganiser_KeyDown;
            this.Loaded -= ProductLibraryMaintenanceOrganiser_Loaded;
            _infoListView.Dispose();

            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    if (this.ViewModel != null)
                    {
                        IDisposable oldModel = this.ViewModel;
                        this.ViewModel = null;
                        if (oldModel != null) oldModel.Dispose();
                    }
                }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion


    }

    /// <summary>
    /// Control representing a line used to indicated a fixed width break point
    /// </summary>
    public class ImportDataFixedWidthLine : Thumb
    {
        /* Line actions - 
         *  Drag to move the line point
         *  double click to delete the line
        */

        #region Constants

        const Int32 charWidth = 7;
        #endregion

        #region Properties

        #region LineDef Property

        public static readonly DependencyProperty LineDefProperty =
            DependencyProperty.Register("LineDef", typeof(ImportFixedLineDef),
            typeof(ImportDataFixedWidthLine));

        public ImportFixedLineDef LineDef
        {
            get { return (ImportFixedLineDef)GetValue(LineDefProperty); }
            set { SetValue(LineDefProperty, value); }
        }

        #endregion



        #endregion


        #region Constructor

        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Style key override")]
        static ImportDataFixedWidthLine()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
               typeof(ImportDataFixedWidthLine), new FrameworkPropertyMetadata(typeof(ImportDataFixedWidthLine)));
        }

        public ImportDataFixedWidthLine(ImportFixedLineDef def)
        {
            this.LineDef = def;
            this.DragDelta += new DragDeltaEventHandler(ImportDataFixedWidthLine_DragDelta);
            this.DragCompleted += new DragCompletedEventHandler(ImportDataFixedWidthLine_DragCompleted);

            Canvas.SetLeft(this, def.LineValue * charWidth);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to the attempt to drag the line
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportDataFixedWidthLine_DragDelta(object sender, DragDeltaEventArgs e)
        {
            //only allow a horizontal drag
            Int32 newHorizPostion = Convert.ToInt32(Canvas.GetLeft(this) + e.HorizontalChange);
            if (newHorizPostion > 0)
            {
                //correct to the nearest point
                Int32 mod = newHorizPostion % charWidth;
                if (mod != 0)
                {
                    newHorizPostion = newHorizPostion - mod;
                }

                Canvas.SetLeft(this, newHorizPostion);
            }
        }

        private void ImportDataFixedWidthLine_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            Double left = Canvas.GetLeft(this);
            this.LineDef.LineValue = Convert.ToInt32(left / charWidth);
        }

        #endregion
    }


    /// <summary>
    /// Custom string collection for the available columns drop down list
    /// </summary>
    public class ColumnHeaderCollection : ObservableCollection<String>
    {
        public ColumnHeaderCollection() { }
    }
}
