﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// L.Hodson ~ Created.
// V8-24265 : J.Pickup
//  Added labels filepath
// V8-25395 : A.Probyn
//  Added ProductLibraryFolderPath
//  Added event so other windows can respond to the entity id changing as its only set on authentication complete.
// CCM-25653 : L.Ineson
//  Added IsConnectedToRepository and connection methods.
// V8-25645 : A.Silva
//  Connection methods now check that the connection is valid before adding it to the repository, throwing an exception otherwise.
// V8-24700 : A.Silva 
//  Added property CustomColumnLayoutFolderPath to get/set the path to the folder storing the settings files.
// V8-26815 : A.Silva 
//  Added property ValidationTemplateFolderPath to Get/Set the path to the folder storing the user's validation templates.
// V8-27087 : L.Ineson
//  Added GetCurrentEntityDisplayUnits()
// V8-24264 : A.Probyn
//  Added DisplayUnits based on GetCurrentEntityDisplayUnits
// V8-27938 : N.Haywood
//  Added ReportColumnLayoutFolderPath
// V8-27957 : N.Foster
//  Implement CCM security
// V8-28018 : L.ineson
// SetRepositoryConnection now returns a true or false rather than deliberately throwing an exception.
// V8-28001 : A.Kuszyk
//  Added event for repository connection change.
// V8-27938 : N.Haywood
//  Added Data Sheets
// V8-27805 : L.ineson
//  Added RepositoryConnectionChanging event
// V8-27950 : J.Pickup
//  Set Repository Connection now validated database.
#endregion

#region Version History : CCM 801
// V8-28760 : A.Kuszyk
//  Added SessionImageLocation and SessionPlanogramLocation properties.
#endregion

#region Version History : CCM 802
// V8-27880 : A.Kuszyk
//  Added SessionPlanogramGroupSelection and supporting type.
#endregion

#region Version History : CCM 811
// V8-28861 : L.Luong
//  Disconnecting from database should now reorder so that the recent connection is last in the list
#endregion

#region Version History : CCM 820
// V8-30791 : D.Pleasance
//  Added recent colors
//V8-28861 : L.Ineson
//  Added session directory methods.
// V8-31140 : L.Ineson
//  Added properties for current servername and databasename
#endregion 

#region Version History : CCM 830
// V8-32823  : J.Pickup
//  Export visbiltity work (New properties).
#endregion


#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using Csla;
using Galleria.Ccm.Common.Wpf;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Controls.Wpf.HelperClasses;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;


namespace Galleria.Ccm.Editor.Client.Wpf
{
    public sealed class ViewState : IViewState
    {
        #region Constants

        private const String HelpFileName = "CCM_Space_Planning.chm";
        private const String RepositoryDalName = "Mssql";
        private const int MaximumNumberOfSavedColours = 30;
        #endregion

        #region Fields

        private String _helpFilePath;
        private readonly Dictionary<SessionDirectory, String> _sessionDirectories = new Dictionary<SessionDirectory, String>();

        private readonly PlanogramViewObjectView _activePlanogramView = new PlanogramViewObjectView();
        private UserEditorSettingsViewModel _settingsView;
        private RecentPlanogramListView _recentPlanogramsView;
        private Int32 _entityId;
        private String _serverName;
        private String _databaseName;

        private readonly ProductLibraryViewModel _productLibraryView = new ProductLibraryViewModel();

        private BulkObservableCollection<ColorItem> _recentColors = new BulkObservableCollection<ColorItem>();
        private ReadOnlyBulkObservableCollection<ColorItem> _recentColorsRO;

        private PlanItemSelectionType _selectionMode = PlanItemSelectionType.PositionsAndComponents;


        Boolean _isExportSpacemanAvailable = false; 
        Boolean _isExportJDAAvailable  = false;
        Boolean _isExportApolloAvailable = false;

        #endregion

        #region Binding Property Paths
        public static readonly PropertyPath RecentColorsProperty = WpfHelper.GetPropertyPath<ViewState>(p => p.RecentColors);
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the current entity that the application is connected to.
        /// </summary>
        public Int32 EntityId 
        {
            get { return _entityId; } 
            set
            {
                _entityId = value;

                if (App.IsUnitTesting)
                {
                    //if we are unit testing then allow then also
                    // update the repository flag.
                    IsConnectedToRepository = (value != 0);
                }

                RaiseEntityIdChanged();
            }
        }

        /// <summary>
        /// Returns the name of the connected repository server.
        /// </summary>
        public String ServerName
        {
            get { return _serverName; }
            internal set { _serverName = value; }
        }

        /// <summary>
        /// Returns the name of the connected repository database.
        /// </summary>
        public String DatabaseName
        {
            get { return _databaseName; }
            internal set { _databaseName = value; }
        }

        /// <summary>
        /// Returns true if we are currently connected to a repository.
        /// </summary>
        public Boolean IsConnectedToRepository { get; internal set; }

        /// <summary>
        /// Returns an object view containing the currently active planogram.
        /// </summary>
        public PlanogramViewObjectView ActivePlanogramView
        {
            get { return _activePlanogramView; }
        }

        /// <summary>
        /// Returns the application settings.
        /// </summary>
        public UserEditorSettingsViewModel Settings
        {
            get
            {
                if (_settingsView == null)
                {
                    _settingsView = new UserEditorSettingsViewModel();
                    _settingsView.ModelChanged += new EventHandler<Framework.ViewModel.ViewStateObjectModelChangedEventArgs<UserEditorSettings>>(SettingsView_ModelChanged);
                    _settingsView.Fetch();
                }
                return _settingsView;
            }
        }

        /// <summary>
        /// Returns the view of the planogram mru list
        /// </summary>
        public RecentPlanogramListView RecentPlanogramsView
        {
            get
            {
                if (_recentPlanogramsView == null)
                {
                    _recentPlanogramsView = new RecentPlanogramListView();

                    try
                    {
                        _recentPlanogramsView.FetchAll();
                    }
                    catch (DataPortalException)
                    {
                        //fail if in debug but otherwise don't do anything.
                        // We don't want to stop the user loading the app just because this wont work.
                        Debug.WriteLine("Failed to load recent planograms");
                    }
                }
                return _recentPlanogramsView;
            }
        }

        /// <summary>
        /// Returns the view of the current product library.
        /// </summary>
        public ProductLibraryViewModel ProductLibraryView
        {
            get { return _productLibraryView; }
        }

        /// <summary>
        /// Returns the display unit of measure collection
        /// for the current entity.
        /// </summary>
        public DisplayUnitOfMeasureCollection DisplayUnits
        {
            get
            {
                return GetCurrentEntityDisplayUnits();
            }
        }

        /// <summary>
        /// Returns the collection of recent colors for the user.
        /// </summary>
        public ReadOnlyBulkObservableCollection<ColorItem> RecentColors
        {
            get { return _recentColorsRO; }
        }

        /// <summary>
        /// Gets/Sets the current selection type.
        /// </summary>
        public PlanItemSelectionType SelectionMode
        {
            get { return _selectionMode; }
            set
            {
                _selectionMode = value;
                RaiseSelectionModeChanged();
            }
        }

        /// <summary>
        /// The current user selected planogram group.
        /// </summary>
        public SessionPlanogramGroupSelection SessionPlanogramGroupSelection { get; set; }


        /// <summary>
        /// Returns the path for the chm
        /// </summary>
        public String HelpFilePath
        {
            get
            {
                if (String.IsNullOrEmpty(_helpFilePath))
                {
                    String loc = System.Reflection.Assembly.GetExecutingAssembly().Location;
                    String path = System.IO.Path.GetDirectoryName(loc);
                    _helpFilePath = System.IO.Path.Combine(path, HelpFileName);

                    if (!System.IO.File.Exists(_helpFilePath))
                    {
                        //we are debugging
                        _helpFilePath = "Resources\\HelpFiles\\" + HelpFileName;
                    }
                }
                return _helpFilePath;
            }
        }


        /// <summary>
        /// The current user selected image location or, if none has been chosen, the default
        /// image location.
        /// TODO: Get rid.
        /// </summary>
        public String SessionImageLocation
        {
            get {return GetSessionDirectory(SessionDirectory.Images);}
            set {SetSessionDirectory(SessionDirectory.Images, value);}
        }


        /// <summary>
        ///     Gets the folder <c>CustomColumnLayout</c> settings should be saved to.
        ///     TODO: Get Rid
        /// </summary>
        public String CustomColumnLayoutFolderPath
        {
            get {return GetSessionDirectory(SessionDirectory.CustomColumnLayout);}
            set {SetSessionDirectory(SessionDirectory.CustomColumnLayout, value);}
        }

        /// <summary>
        /// Returns true if ExportSpacemanVisible true in config
        /// </summary>
        public Boolean IsExportSpacemanAvailable { get { return _isExportSpacemanAvailable; } }

        /// <summary>
        /// Returns true if ExportJDAVisible true in config
        /// </summary>
        public Boolean IsExportJDAAvailable { get { return _isExportJDAAvailable; } }

        /// <summary>
        /// Returns true if ExportApolloVisible true in config
        /// </summary>
        public Boolean IsExportApolloAvailable { get { return _isExportApolloAvailable; } }

        #endregion

        #region Events

        #region FixtureLibraryChanged

        /// <summary>
        /// Notifies that the fixture library has changed.
        /// </summary>
        public event EventHandler FixtureLibraryChanged;

        /// <summary>
        /// Raises the FixtureLibraryChanged event.
        /// </summary>
        public void RaiseFixtureLibraryChanged()
        {
            if (FixtureLibraryChanged != null)
            {
                FixtureLibraryChanged(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Settings model changed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SettingsView_ModelChanged(object sender, Framework.ViewModel.ViewStateObjectModelChangedEventArgs<UserEditorSettings> e)
        {
            if (e.OldModel != null)
            {
                e.OldModel.Colors.BulkCollectionChanged -= Colors_BulkCollectionChanged;
            }

            if (e.NewModel != null)
            {
                Colors_BulkCollectionChanged(e.NewModel, new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));
                e.NewModel.Colors.BulkCollectionChanged += Colors_BulkCollectionChanged;
            }
            else
            {
                _recentColors.Clear();
            }
        }

        /// <summary>
        /// User colors collection changed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Colors_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (UserEditorSettingsColor color in e.ChangedItems)
                    {
                        _recentColors.Add(new ColorItem(ColorHelper.IntToColor(color.Color), null));
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (UserEditorSettingsColor color in e.ChangedItems)
                    {
                        ColorItem colorToRemove = _recentColors.FirstOrDefault(p => ColorHelper.ColorToInt(p.Colour) == color.Color);
                        if (colorToRemove != null)
                        {
                            _recentColors.Remove(colorToRemove);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        _recentColors.Clear();
                        _recentColors.AddRange(_settingsView.Model.Colors.Select(c => new ColorItem(ColorHelper.IntToColor(c.Color), null)));
                    }
                    break;
            }
        }

        #endregion

        #region EntityIdChanged

        /// <summary>
        /// Notifies that the entity id has changed.
        /// </summary>
        public event EventHandler EntityIdChanged;

        /// <summary>
        /// Raises the EntityIdChanged event.
        /// </summary>
        public void RaiseEntityIdChanged()
        {
            if (EntityIdChanged != null)
            {
                EntityIdChanged(this, EventArgs.Empty);
            }
        }

        #endregion

        #region RepositoryConnectionChanging

        /// <summary>
        /// Fired when the repository connection is about to change.
        /// </summary>
        public event EventHandler<RepositoryConnectionChangingEventArgs> RepositoryConnectionChanging;

        /// <summary>
        /// Raises the RepositoryConnectionChanging event
        /// </summary>
        /// <returns>returns true if the action can continue</returns>
        private Boolean RaiseRepositoryConnectionChanging(String server, String database)
        {
            RepositoryConnectionChangingEventArgs args = new RepositoryConnectionChangingEventArgs(server, database);

            if (RepositoryConnectionChanging != null)
            {
                RepositoryConnectionChanging(this, args);
            }

            return !args.Cancel;
        }

        public sealed class RepositoryConnectionChangingEventArgs : EventArgs
        {
            public Boolean Cancel { get; set; }
            public String ServerName { get; private set; }
            public String DatabaseName { get; private set; }

            public RepositoryConnectionChangingEventArgs(String server, String database)
            {
                ServerName = server;
                DatabaseName = database;
            }
        }

        #endregion

        #region RepositoryConnectionChanged

        /// <summary>
        /// Fired when the repository connection has been changed or disconnected.
        /// </summary>
        public event EventHandler RepositoryConnectionChanged;

        /// <summary>
        /// Raises the RepositoryConnectionChanged event.
        /// </summary>
        private void RaiseRepositoryConnectionChanged()
        {
            if (RepositoryConnectionChanged != null)
            {
                RepositoryConnectionChanged(this, EventArgs.Empty);
            }
        }
        #endregion

        #region SelectionModeChanged

        /// <summary>
        /// Notifies that the SelectionModel property value has changed.
        /// </summary>
        public event EventHandler SelectionModeChanged;

        /// <summary>
        /// Called to raise the SelectionModeChanged event
        /// </summary>
        private void RaiseSelectionModeChanged()
        {
            if (SelectionModeChanged != null)
            {
                SelectionModeChanged(this, EventArgs.Empty);
            }
        }

        #endregion

        #endregion

        #region Constructor

        public ViewState()
        {
            IsConnectedToRepository = false;
            _recentColorsRO = new ReadOnlyBulkObservableCollection<ColorItem>(_recentColors);

            this.LoadAppSettingConfigIntoViewState();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Sets the connection to the repository.
        /// </summary>
        /// <param name="server">Name of the server hosting the database.</param>
        /// <param name="dbName">Name of the database.</param>
        public Boolean SetRepositoryConnection(String server, String dbName, out Boolean upgradeRequired, out String databaseInvalidReason)
        {
            databaseInvalidReason = String.Empty;
            upgradeRequired = false;

            //raise the event to check if we can continue
            if (!RaiseRepositoryConnectionChanging(server, dbName)) { return false; }

            // create a dal factor config
            var dalFactoryConfig = new DalFactoryConfigElement(RepositoryDalName);
            dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Server", server));
            dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Database", dbName));

            // create the dal factory
            IDalFactory dalFactory = new Dal.Mssql.DalFactory(dalFactoryConfig);

            // verify that the database is available
            if (dalFactory.IsDatabaseAvailable())
            {
                //Lets check if the repo is a valid schema:
                String databaseVersion;
                String latestDatabaseVersion;
                IncompatibilityErrorType incompatibilityErrorType;

                upgradeRequired = dalFactory.UpgradeRequired(out databaseVersion, out latestDatabaseVersion, out incompatibilityErrorType);

                if (!upgradeRequired)
                {
                    // register the dal factory within the dal container
                    DalContainer.RegisterFactory(RepositoryDalName, dalFactory);
                    DalContainer.DalName = RepositoryDalName;

                    // update the user settings
                    UserSystemSetting settings = UserSystemSetting.FetchUserSettings();
                    Connection currentConnection = settings.Connections.FirstOrDefault(connection => (connection.ServerName == server && connection.DatabaseName == dbName));
                    if (currentConnection != null)
                    {
                        currentConnection.IsCurrentConnection = true;
                        settings = settings.Save();
                    }

                    // indicate that we are connected to the repository
                    this.IsConnectedToRepository = true;
                    this.ServerName = server;
                    this.DatabaseName = dbName;
                    RaiseRepositoryConnectionChanged();
                    return true;
                }
                else
                { 
                    //Invalid DB.
                    if (Convert.ToDouble(latestDatabaseVersion) > Convert.ToDouble(databaseVersion))
                    {
                        //Too early
                        databaseInvalidReason = Message.ViewState_DbInvalid_Older;
                    }
                    else if (Convert.ToDouble(latestDatabaseVersion) < Convert.ToDouble(databaseVersion))
                    {
                        //Too late
                        databaseInvalidReason = Message.ViewState_DbInvalid_Newer;
                    }

                    ClearRepositoryConnection();
                    return false;
                } 
            }
            else
            {
                ClearRepositoryConnection();
                upgradeRequired = false;
                return false;
            }
        }

        /// <summary>
        /// Clears the connection to the current repository.
        /// </summary>
        public Boolean ClearRepositoryConnection()
        {
            //raise the event to check if we can continue
            if (!RaiseRepositoryConnectionChanging(null, null)) return false;

            // ensure the user is no longer authenticated
            DomainPrincipal.Logout();

            // create an empty dal factory config
            var dalFactoryConfig = new DalFactoryConfigElement(RepositoryDalName);
            IDalFactory dalFactory = new Dal.Mssql.DalFactory(dalFactoryConfig);

            // register within the container which replaces the old dal factory
            DalContainer.RegisterFactory(RepositoryDalName, dalFactory);
            DalContainer.DalName = RepositoryDalName;

            // update the user settings
            UserSystemSetting settings = UserSystemSetting.FetchUserSettings();
            Connection currentConnection = settings.Connections.FirstOrDefault(connection => connection.IsCurrentConnection);
            Connection temp = Connection.NewConnection();
            Connection lastConnection = settings.Connections.LastOrDefault();
            if (currentConnection != null)
            {
                currentConnection.IsCurrentConnection = false;

                temp.Id = currentConnection.Id;
                currentConnection.Id = lastConnection.Id;
                lastConnection.Id = temp.Id;

                settings = settings.Save();
            }

            // indicates that we are no longer connected to a repository
            this.IsConnectedToRepository = false;
            // Clear the product library
            RaiseRepositoryConnectionChanged();
            this.EntityId = 0;
            this.ServerName = null;
            this.DatabaseName = null;

            return true;
        }

        /// <summary>
        /// Returns the display units of measure for the current entity
        /// or an empty collection if we are not connected to a repository.
        /// </summary>
        /// <returns></returns>
        public DisplayUnitOfMeasureCollection GetCurrentEntityDisplayUnits()
        {
            DisplayUnitOfMeasureCollection displayUnits = DisplayUnitOfMeasureCollection.Empty;
            if (App.ViewState.IsConnectedToRepository)
            {
                try
                {
                    displayUnits = DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(this.EntityId);
                }
                catch (Exception ex)
                {
                    LocalHelper.RecordException(ex);
                }
            }
            return displayUnits;
        }

        /// <summary>
        /// Applies the color as user recent color if it doesnt already exist (Max of 30 colors)
        /// </summary>
        /// <param name="color"></param>
        internal void ApplyRecentColor(System.Windows.Media.Color? color)
        {
            if (!color.HasValue) return;

            var colorValue = ColorHelper.ColorToInt(color.Value);
            if (Settings.Model.Colors.Any(p => p.Color == colorValue)) return;

            while (Settings.Model.Colors.Count >= MaximumNumberOfSavedColours)
            {
                Settings.Model.Colors.RemoveAt(0);
            }

            Settings.Model.Colors.Add(colorValue);
        }

        /// <summary>
        /// Gets the current session directory path to use for the given type.
        /// </summary>
        public String GetSessionDirectory(SessionDirectory type)
        {
            //return the session directory if it exists
            String sessionDir = null;
            if(_sessionDirectories.TryGetValue(type, out sessionDir)
                && !String.IsNullOrWhiteSpace(sessionDir) 
                && Directory.Exists(sessionDir))
            {
                return sessionDir;
            }

            //otherwise return the settings directory
            return SessionDirectoryHelper.GetOrCreateSettingsDirectory(type, this.Settings.Model);
        }

        /// <summary>
        /// Sets the current session directory path to use for the given type.
        /// </summary>
        public void SetSessionDirectory(SessionDirectory type, String directory)
        {
            if (!String.IsNullOrWhiteSpace(directory) && Directory.Exists(directory))
            {
                _sessionDirectories[type] = directory;
            }
        }

        /// <summary>
        /// Loads relevant properties into the current viewstate instance from the app settings within the configuration file. ( 
        /// </summary>
        private void LoadAppSettingConfigIntoViewState()
        {
            if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains(Constants.exportSpacemanAvailable))
            {
                _isExportSpacemanAvailable = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings[Constants.exportSpacemanAvailable]);
            }
            else
            {
                _isExportSpacemanAvailable = false;
            }

            if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains(Constants.exportJDAAvailable))
            {
                _isExportJDAAvailable = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings[Constants.exportJDAAvailable]);
            }
            else
            {
                _isExportJDAAvailable = false;
            }

            if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains(Constants.exportApolloAvailable))
            {
                _isExportApolloAvailable = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings[Constants.exportApolloAvailable]);
            }
            else
            {
                _isExportApolloAvailable = false;
            }
        }

        #endregion
    }

    #region Supporting Classes

    /// <summary>
    /// Represents the selection of a Planogram Group based on it's Id and whether or not it
    /// is a member of the Recent Work group.
    /// </summary>
    public sealed class SessionPlanogramGroupSelection
    {
        /// <summary>
        /// True if the Planogram Group is a member of Recent Work.
        /// </summary>
        public Boolean IsRecentWorkGroup { get; set; }
        
        /// <summary>
        /// The Id of the Planogram Group.
        /// </summary>
        public Int32 PlanogramGroupId { get; set; }

        /// <summary>
        /// Instantiates a selection using values from the given Planogram Group view model.
        /// </summary>
        /// <param name="planogramGroupViewModel"></param>
        public SessionPlanogramGroupSelection(PlanogramGroupViewModel planogramGroupViewModel)
        {
            Debug.Assert((planogramGroupViewModel != null), "planogramGroupViewModel is null");
            if (planogramGroupViewModel == null) return;

            IsRecentWorkGroup = planogramGroupViewModel.IsRecentWorkGroup;
            PlanogramGroupId = planogramGroupViewModel.PlanogramGroup.Id;
        }
    }

    #endregion
}
