﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25873 : A.Probyn
//      Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using System.Collections.ObjectModel;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;
using System.Windows.Threading;

namespace Galleria.Ccm.Editor.Client.Wpf.FixtureLibrary
{
    /// <summary>
    /// Interaction logic for FolderSelector.xaml
    /// </summary>
    public partial class FolderSelector : ExtendedRibbonWindow
    {
        #region Fields
        private String _preselectedPath = String.Empty;
        private ObservableCollection<TreeViewItem> _treeViewItems = new ObservableCollection<TreeViewItem>();
        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
           DependencyProperty.Register("ViewModel", typeof(FolderSelectorViewModel), typeof(FolderSelector),
           new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            FolderSelector senderControl = (FolderSelector)obj;

            if (e.OldValue != null)
            {
                FolderSelectorViewModel oldModel = e.OldValue as FolderSelectorViewModel;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                FolderSelectorViewModel newModel = e.NewValue as FolderSelectorViewModel;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
                newModel.AttachedControl = senderControl;
            }

            senderControl.UpdateTreeviewItems();
        }

        /// <summary>
        /// Property changed event for the view model
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == FolderSelectorViewModel.RootFolderProperty.Path)
            {
                UpdateTreeviewItems();
            }
        }

        /// <summary>
        /// Viewmodel Controller
        /// </summary>
        public FolderSelectorViewModel ViewModel
        {
            get { return (FolderSelectorViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public FolderSelector(String preselectedPath)
        {
            //Store preselected path
            this._preselectedPath = preselectedPath;

            this.AddHandler(Treeview.MouseRightButtonDownEvent, (MouseButtonEventHandler)TreeView_MouseRightButtonDown);
            this.AddHandler(TreeViewItem.MouseRightButtonDownEvent, (MouseButtonEventHandler)TreeViewItem_MouseRightButtonDown);

            InitializeComponent();

            this.xTreeView.ItemsSource = _treeViewItems;

            this.ViewModel = new FolderSelectorViewModel();
        }

        #endregion

        #region Event Handlers
        /// <summary>
        /// Called on right click of a tree view item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeView_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            //only process if we have not right clicked on an actual tree view item.
            TreeViewItem item = ((DependencyObject)e.OriginalSource).FindVisualAncestor<TreeViewItem>();
            if (item == null)
            {
                ShowContextMenu(this.ViewModel.RootFolder);
                e.Handled = true;
            }
        }

        /// <summary>
        /// Called whenever the treeview selected item changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (this.ViewModel != null)
            {
                if (e.NewValue != null)
                {
                    TreeViewItem selectedItem = e.NewValue as TreeViewItem;
                    if (selectedItem != null)
                    {
                        Folder folder = selectedItem.Header as Folder;
                        this.ViewModel.SelectedFolder = folder;
                    }
                }
            }
        }

        /// <summary>
        /// Called on right click of a tree view item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeViewItem_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            DependencyObject obj = e.OriginalSource as DependencyObject;
            if (obj != null)
            {
                TreeViewItem item = obj.FindVisualAncestor<TreeViewItem>();
                if (item != null)
                {
                    item.IsSelected = true;


                    //show the context menu for the item
                    if (item.Header is Folder)
                    {
                        ShowContextMenu((Folder)item.Header);
                    }

                    e.Handled = true;
                }
            }
        }

        #region Event Handlers

        /// <summary>
        /// Returns a false result on cancel click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Updates the treeview
        /// </summary>
        private void UpdateTreeviewItems()
        {
            _treeViewItems.Clear();

            if (this.ViewModel != null)
            {
                //create folders
                Folder rootFolder = this.ViewModel.RootFolder;

                //Add root folder
                DataTemplate folderTemplate = this.Resources["FixtureLibrary_DTFolderTreeItem"] as DataTemplate;

                if (rootFolder != null)
                {
                    TreeViewItem rootFolderItem = new TreeViewItem();
                    rootFolderItem.Header = rootFolder;
                    rootFolderItem.HeaderTemplate = folderTemplate;
                    rootFolderItem.IsExpanded = true;
                    rootFolderItem.IsSelected = ((this.ViewModel.SelectedFolder == null && rootFolder.FileSystemPath == this._preselectedPath) || this.ViewModel.SelectedFolder == rootFolder) ? true : false;
                    _treeViewItems.Add(rootFolderItem);

                    foreach (Folder childFolder in rootFolder.ChildFolders)
                    {
                        AddTreeviewFolder(rootFolderItem, childFolder);
                    }
                }
            }
        }

        /// <summary>
        /// Adds a new folder item to the structure
        /// </summary>
        /// <param name="parentFolderItem"></param>
        /// <param name="folder"></param>
        /// <param name="fixturePackageInfos"></param>
        private void AddTreeviewFolder(TreeViewItem parentFolderItem, Folder folder)
        {
            DataTemplate folderTemplate = this.Resources["FixtureLibrary_DTFolderTreeItem"] as DataTemplate;

            TreeViewItem folderItem = new TreeViewItem();
            folderItem.Header = folder;
            folderItem.HeaderTemplate = folderTemplate;
            folderItem.IsExpanded = true;
            folderItem.IsSelected = ((this.ViewModel.SelectedFolder == null && folder.FileSystemPath == this._preselectedPath) || this.ViewModel.SelectedFolder == folder) ? true : false;

            //cycle through child folders
            foreach (Folder childFolder in folder.ChildFolders.OrderBy(f => f.Name))
            {
                AddTreeviewFolder(folderItem, childFolder);
            }

            //we must show the folder even if it has no items
            // otherwise how will we add new ones
            if (parentFolderItem != null)
            {
                parentFolderItem.Items.Add(folderItem);
            }
            else
            {
                _treeViewItems.Add(folderItem);
            }


        }

        /// <summary>
        /// Shows the context menu for the given folder.
        /// </summary>
        /// <param name="folder"></param>
        private void ShowContextMenu(Folder folder)
        {
            Fluent.ContextMenu contextMenu = new Fluent.ContextMenu();
            RelayCommand cmd;

            //add folder.
            cmd = this.ViewModel.NewFolderCommand;
            contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, CommandParameter = folder, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });

            //rename folder
            if (this.ViewModel.RenameFolderCommand.CanExecute(folder))
            {
                cmd = this.ViewModel.RenameFolderCommand;
                contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, CommandParameter = folder, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });
            }

            //delete folder
            if (this.ViewModel.DeleteFolderCommand.CanExecute(folder))
            {
                contextMenu.Items.Add(new Separator());

                cmd = this.ViewModel.DeleteFolderCommand;
                contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, CommandParameter = folder, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });
            }


            contextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.MousePoint;
            contextMenu.IsOpen = true;
        }

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            if (!_isDisposed)
            {
                IDisposable disView = this.ViewModel;

                this.ViewModel = null;

                if (disView != null)
                {
                    disView.Dispose();
                }

                GC.SuppressFinalize(this);

                _isDisposed = true;
            }
        }

        #endregion

        #region Window Close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion
    }
}
