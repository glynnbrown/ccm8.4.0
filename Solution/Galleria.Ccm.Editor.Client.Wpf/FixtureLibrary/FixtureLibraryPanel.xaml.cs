﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
// L.Hodson ~ Created.
// V8-25863 : A.Probyn
//      Set default placement to be top - reverted back
// V8-25373  : A.Probyn
//  Added code to fix Drag/Drop of plan item on fixture library panel
#endregion

#region Version History: (CCM 8.0.0)
// V8-28912 : M.Shelley
//  Fixed an issue whereby the ItemType column was not grouping correctly.
#endregion

#endregion

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Model;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Model;
using System.ComponentModel;

namespace Galleria.Ccm.Editor.Client.Wpf.FixtureLibrary
{
    /// <summary>
    /// Interaction logic for FixtureLibraryPanel.xaml
    /// </summary>
    public partial class FixtureLibraryPanel : DockingPanelControl, IDisposable
    {
        #region Fields
        private CursorAdorner _dragOverAdorner;
        private ObservableCollection<FixtureLibraryPanelViewModel.FixturePackageInfoView> _fixtureLibraryItems = new ObservableCollection<FixtureLibraryPanelViewModel.FixturePackageInfoView>();
        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
           DependencyProperty.Register("ViewModel", typeof(FixtureLibraryPanelViewModel), typeof(FixtureLibraryPanel),
           new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            FixtureLibraryPanel senderControl = (FixtureLibraryPanel)obj;

            if (e.OldValue != null)
            {
                FixtureLibraryPanelViewModel oldModel = e.OldValue as FixtureLibraryPanelViewModel;
                oldModel.AvailableFixturePackageInfos.BulkCollectionChanged -= senderControl.ViewModel_AvailableFixturePackageInfosBulkCollectionChanged;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                FixtureLibraryPanelViewModel newModel = e.NewValue as FixtureLibraryPanelViewModel;
                newModel.AvailableFixturePackageInfos.BulkCollectionChanged += senderControl.ViewModel_AvailableFixturePackageInfosBulkCollectionChanged;
                newModel.AttachedControl = senderControl;
            }

            senderControl.UpdateFixtureLibraryItems();
        }



        /// <summary>
        /// Viewmodel Controller
        /// </summary>
        public FixtureLibraryPanelViewModel ViewModel
        {
            get { return (FixtureLibraryPanelViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public FixtureLibraryPanel()
        {
            this.Title = Message.FixtureLibrary_PanelTitle;

            InitializeComponent();

            this.ViewModel = new FixtureLibraryPanelViewModel();

            //Load column set on intial load
            LoadColumnSet();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the available fixture package infos collection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_AvailableFixturePackageInfosBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            UpdateFixtureLibraryItems();
        }

        protected override void OnDragEnter(DragEventArgs e)
        {
            base.OnDragEnter(e);

            if (DragDropBehaviour.IsUnpackedType<PlanItemDragDropArgs>(e.Data))
            {
                AdornerLayer aLayer = AdornerLayer.GetAdornerLayer(this);
                Adorner test = aLayer.FindVisualDescendent<Adorner>();
                if(test != null)
                {
                    aLayer.Remove(test);
                }

                if (aLayer != null)
                {
                    //Create new adorner
                    if (_dragOverAdorner == null)
                    {
                        _dragOverAdorner = new CursorAdorner(this, ImageResources.Ribbon_AddToFixtureLibrary_16);
                    }

                    //Check for existing
                    CursorAdorner existingAdorner = aLayer.FindVisualDescendent<CursorAdorner>();
                    //If not one
                    if (existingAdorner == null)
                    {
                        //Set position
                        Point pos = e.GetPosition(_dragOverAdorner.AdornedElement);
                        _dragOverAdorner.LeftOffset = pos.X;
                        _dragOverAdorner.TopOffset = pos.Y;
                        //Add to layer
                        aLayer.Add(_dragOverAdorner);
                    }
                }
            }
        }

        protected override void OnDragOver(DragEventArgs e)
        {
            base.OnDragOver(e);

            CursorAdorner adorner = _dragOverAdorner;
            if (adorner != null)
            {
                Point pos = e.GetPosition(adorner.AdornedElement);
                adorner.LeftOffset = pos.X;
                adorner.TopOffset = pos.Y;
            }
        }

        protected override void OnDragLeave(DragEventArgs e)
        {
            base.OnDragLeave(e);

            _dragOverAdorner = null;

            //If plan item
            if (DragDropBehaviour.IsUnpackedType<PlanItemDragDropArgs>(e.Data))
            {
                //Ensure adorner is removed
                AdornerLayer aLayer = AdornerLayer.GetAdornerLayer(this);
                Adorner existingAdorner = aLayer.FindVisualDescendent<Adorner>();
                if (existingAdorner != null)
                {
                    aLayer.Remove(existingAdorner);
                }
            }
        }
        
        protected override void OnDrop(DragEventArgs e)
        {
            if (DragDropBehaviour.IsUnpackedType<PlanItemDragDropArgs>(e.Data))
            {
                //a plan item was dropped
                PlanItemDragDropArgs args = DragDropBehaviour.UnpackData<PlanItemDragDropArgs>(e.Data);
                MainPageCommands.AddToFixtureLibrary.Execute();
                e.Handled = true;

                //Ensure adorner is removed
                AdornerLayer aLayer = AdornerLayer.GetAdornerLayer(this);
                Adorner existingAdorner = aLayer.FindVisualDescendent<Adorner>();
                if (existingAdorner != null)
                {
                    aLayer.Remove(existingAdorner);
                }
            }
            
            base.OnDrop(e);
        }

        /// <summary>
        /// Called when a treeview item is dragged.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataGrid_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if ((e.LeftButton == MouseButtonState.Pressed) && !(DragDropBehaviour.IsDragging))
            {
                TextBlock item = ((DependencyObject)e.OriginalSource).FindVisualAncestor<TextBlock>();
                if (item != null)
                {
                    var fixtureInfo = item.DataContext as FixtureLibraryPanelViewModel.FixturePackageInfoView;
                    if (fixtureInfo != null)
                    {
                        FixtureLibraryItemDropEventArgs libraryItemArgs = new FixtureLibraryItemDropEventArgs(fixtureInfo.Model);

                        FrameworkElement dragScope = this as FrameworkElement;
                        Window dragScopeWin = this.FindVisualAncestor<Window>();
                        if (dragScopeWin != null)
                        {
                            dragScope = dragScopeWin.Content as FrameworkElement;
                        }

                        e.Handled = true;

                        DragDropBehaviour dragArgs = new DragDropBehaviour(libraryItemArgs);
                        dragArgs.DragArea = dragScope;
                        dragArgs.SetAdornerFromImageSource(ImageResources.Ribbon_AddToFixtureLibrary_16);
                        dragArgs.BeginDrag();
                    }
                }
            }

        }

        /// <summary>
        /// Called whenever a tree view item is double clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DependencyObject obj = e.OriginalSource as DependencyObject;
            if (obj != null)
            {
                DataGridRow row = obj.FindVisualAncestor<DataGridRow>();
                if (row != null && this.ViewModel != null)
                {
                    var info = row.Item as FixtureLibraryPanelViewModel.FixturePackageInfoView;

                    if (this.ViewModel.AddToPlanogramCommand.CanExecute(info))
                    {
                        this.ViewModel.AddToPlanogramCommand.Execute(info);
                    }
                }
            }
        }

        /// <summary>
        /// Called on right click of a tree view item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataGrid_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            DependencyObject obj = e.OriginalSource as DependencyObject;
            if (obj != null)
            {
                DataGridRow row = obj.FindVisualAncestor<DataGridRow>();
                if (row != null)
                {
                    row.IsSelected = true;

                    if (row.Item is FixtureLibraryPanelViewModel.FixturePackageInfoView)
                    {
                        ShowContextMenu((FixtureLibraryPanelViewModel.FixturePackageInfoView)row.Item);
                    }

                    e.Handled = true;
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Method to load the column set
        /// </summary>
        private void LoadColumnSet()
        {
            //Clear existing columns
            this.xDataGrid.Columns.Clear();

            var uomValues = App.ViewState.DisplayUnits;

            //Add columns
            this.xDataGrid.Columns.Add(CommonHelper.CreateReadOnlyColumn(FixturePackage.NameProperty, uomValues, Message.FixtureLibrary_ColumnGroup));
            this.xDataGrid.Columns.Add(CommonHelper.CreateReadOnlyColumn("Folder.Name", typeof(String), Message.FixtureLibrary_Folder, Framework.Model.ModelPropertyDisplayType.None, uomValues, Message.FixtureLibrary_ColumnGroup));

            // V8-28912 - changed the type of extended data grid column to allow the grouping to work correctly.
            //this.xDataGrid.Columns.Add(CommonHelper.CreateReadOnlyColumn("ItemType", typeof(FixtureLibraryPanelViewModel.FixturePackageInfoType), Message.FixtureLibrary_ItemType, Framework.Model.ModelPropertyDisplayType.None, uomValues, Message.FixtureLibrary_ColumnGroup));
            this.xDataGrid.Columns.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(Message.FixtureLibrary_ItemType, "ItemType", HorizontalAlignment.Center));

            this.xDataGrid.Columns.Add(CommonHelper.CreateReadOnlyColumn(FixturePackage.HeightProperty, uomValues, Message.FixtureLibrary_ColumnGroup));
            this.xDataGrid.Columns.Add(CommonHelper.CreateReadOnlyColumn(FixturePackage.WidthProperty, uomValues, Message.FixtureLibrary_ColumnGroup));
            this.xDataGrid.Columns.Add(CommonHelper.CreateReadOnlyColumn(FixturePackage.DepthProperty, uomValues, Message.FixtureLibrary_ColumnGroup));

            this.xDataGrid.Columns.Add(CommonHelper.CreateReadOnlyColumn("FixtureCount", typeof(Int32), FixturePackageInfo.FixtureCountProperty.FriendlyName, Framework.Model.ModelPropertyDisplayType.None, uomValues, Message.FixtureLibrary_ColumnGroup));
            this.xDataGrid.Columns.Add(CommonHelper.CreateReadOnlyColumn("ComponentCount", typeof(Int32), FixturePackageInfo.ComponentCountProperty.FriendlyName, Framework.Model.ModelPropertyDisplayType.None, uomValues, Message.FixtureLibrary_ColumnGroup));
            this.xDataGrid.Columns.Add(CommonHelper.CreateReadOnlyColumn("AssemblyCount", typeof(Int32), FixturePackageInfo.AssemblyCountProperty.FriendlyName, Framework.Model.ModelPropertyDisplayType.None, uomValues, Message.FixtureLibrary_ColumnGroup));
            this.xDataGrid.Columns.Add(CommonHelper.CreateReadOnlyColumn("CustomAttribute1", typeof(String), FixturePackageInfo.CustomAttribute1Property.FriendlyName, Framework.Model.ModelPropertyDisplayType.None, uomValues, Message.FixtureLibrary_ColumnGroup));
            this.xDataGrid.Columns.Add(CommonHelper.CreateReadOnlyColumn("CustomAttribute2", typeof(String), FixturePackageInfo.CustomAttribute2Property.FriendlyName, Framework.Model.ModelPropertyDisplayType.None, uomValues, Message.FixtureLibrary_ColumnGroup));
            this.xDataGrid.Columns.Add(CommonHelper.CreateReadOnlyColumn("CustomAttribute3", typeof(String), FixturePackageInfo.CustomAttribute3Property.FriendlyName, Framework.Model.ModelPropertyDisplayType.None, uomValues, Message.FixtureLibrary_ColumnGroup));
            this.xDataGrid.Columns.Add(CommonHelper.CreateReadOnlyColumn("CustomAttribute4", typeof(String), FixturePackageInfo.CustomAttribute4Property.FriendlyName, Framework.Model.ModelPropertyDisplayType.None, uomValues, Message.FixtureLibrary_ColumnGroup));
            this.xDataGrid.Columns.Add(CommonHelper.CreateReadOnlyColumn("CustomAttribute5", typeof(String), FixturePackageInfo.CustomAttribute5Property.FriendlyName, Framework.Model.ModelPropertyDisplayType.None, uomValues, Message.FixtureLibrary_ColumnGroup));

            //Force sort
            this.xDataGrid.SortByDescriptions.Add(new SortDescription(FixturePackage.NameProperty.Name, ListSortDirection.Ascending));
        }

        /// <summary>
        /// Updates the treeview
        /// </summary>
        private void UpdateFixtureLibraryItems()
        {
            this._fixtureLibraryItems.Clear();

            if (this.ViewModel != null)
            {
                foreach (FixtureLibraryPanelViewModel.FixturePackageInfoView info in this.ViewModel.AvailableFixturePackageInfos.ToList())
                {
                    if (!_fixtureLibraryItems.Contains(info))
                    {
                        _fixtureLibraryItems.Add(info);
                    }
                }
            }
        }

        /// <summary>
        /// Shows the context menu for the given fixture package view
        /// </summary>
        /// <param name="fixturePackageView"></param>
        private void ShowContextMenu(FixtureLibraryPanelViewModel.FixturePackageInfoView fixturePackageView)
        {
            Fluent.ContextMenu contextMenu = new Fluent.ContextMenu();
            RelayCommand cmd;

            cmd = this.ViewModel.AddToPlanogramCommand;
            contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });

            cmd = this.ViewModel.EditFixturePackageCommand;
            contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });

            contextMenu.Items.Add(new Separator());

            cmd = this.ViewModel.DeleteFixturePackageCommand;
            contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });

            contextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.MousePoint;
            contextMenu.IsOpen = true;
        }
        
        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            if (!_isDisposed)
            {
                IDisposable disView = this.ViewModel;

                this.ViewModel = null;

                if (disView != null)
                {
                    disView.Dispose();
                }

                GC.SuppressFinalize(this);

                _isDisposed = true;
            }
        }

        #endregion
    }
}
