﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
// V8-25573 : A.Probyn
//      Added title to DeleteFixturePackage_Executed dialog
// V8-26184 : L.Ineson
//  Removed the delete folder command as it is no longer applicable.
// V8-27654 : L.Ineson
//  Made sure this refreshes when the system settings change.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Csla;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Editor.Client.Wpf.FixtureLibrary
{
    /// <summary>
    /// Viewmodel controller for the fixture library panel.
    /// </summary>
    public sealed class FixtureLibraryPanelViewModel : ViewModelAttachedControlObject<FixtureLibraryPanel>
    {
        #region Nested Classes

        public enum FixturePackageInfoType
        {
            Unknown,
            MultiFixture,
            Fixture,
            Assembly,
            Component
        }

        public static class FixturePackageInfoTypeHelper
        {
            public static readonly Dictionary<FixturePackageInfoType, String> FriendlyNames =
                new Dictionary<FixturePackageInfoType, String>()
            {
                {FixturePackageInfoType.Unknown, Message.Enum_FixturePackageInfoType_Unknown},
                {FixturePackageInfoType.MultiFixture, Message.Enum_FixturePackageInfoType_MultiFixture},
                {FixturePackageInfoType.Fixture, Message.Enum_FixturePackageInfoType_Fixture},
                {FixturePackageInfoType.Assembly, Message.Enum_FixturePackageInfoType_Assembly},
                {FixturePackageInfoType.Component, Message.Enum_FixturePackageInfoType_Component}
            };
        }

        /// <summary>
        /// View of a FixturePackageInfo.
        /// </summary>
        public sealed class FixturePackageInfoView : ViewModelObjectView<FixturePackageInfo>
        {
            #region Fields
            private FixturePackageInfoType _itemType;
            private Folder _folder;
            #endregion

            #region Properties

            public Folder Folder
            { 
                get { return _folder; }
            }

            public FixturePackageInfoType ItemType
            {
                get { return _itemType; }
            }

            public Object Id
            {
                get { return Model.Id; }
            }

            public String Name
            {
                get { return Model.Name; }
            }

            public Single Height
            {
                get { return Model.Height; }
            }

            public Single Width
            {
                get { return Model.Width; }
            }

            public Single Depth
            {
                get { return Model.Depth; }
            }

            /// <summary>
            /// Returns the icons for the package type.
            /// </summary>
            public ImageSource Icon
            {
                get
                {
                    switch (_itemType)
                    {
                        default:
                            return null;

                        case FixturePackageInfoType.MultiFixture:
                            return ImageResources.FixtureLibrary_MultiFixtureTypeIcon;

                        case FixturePackageInfoType.Fixture:
                            return ImageResources.FixtureLibrary_FixtureTypeIcon;

                        case FixturePackageInfoType.Assembly:
                            return ImageResources.FixtureLibrary_AssemblyTypeIcon;

                        case FixturePackageInfoType.Component:
                            return ImageResources.FixtureLibrary_ComponentTypeIcon;
                    }
                }
            }


            public Byte[] ThumbnailImageData
            {
                get { return Model.ThumbnailImageData; }
            }

            public String CustomAttribute1
            {
                get { return Model.CustomAttribute1; }
            }

            public String CustomAttribute2
            {
                get { return Model.CustomAttribute2; }
            }

            public String CustomAttribute3
            {
                get { return Model.CustomAttribute3; }
            }

            public String CustomAttribute4
            {
                get { return Model.CustomAttribute4; }
            }

            public String CustomAttribute5
            {
                get { return Model.CustomAttribute5; }
            }

            #region Helper Properties

            public Int32 AssemblyCount
            {
                get { return Model.AssemblyCount; }
            }

            public Int32 ComponentCount
            {
                get { return Model.ComponentCount; }
            }

            public Int32 FixtureCount
            {
                get { return Model.FixtureCount; }
            }

            #endregion

            #endregion

            #region Constructor

            /// <summary>
            /// Creates a new instance of this type.
            /// </summary>
            /// <param name="model"></param>
            public FixturePackageInfoView(FixturePackageInfo model, Folder folder)
            {
                //Set model
                this.Model = model;

                if (model.FixtureCount > 1)
                {
                    _itemType = FixturePackageInfoType.MultiFixture;
                }
                else if (model.FixtureCount == 1)
                {
                    _itemType = FixturePackageInfoType.Fixture;
                }
                else if (model.AssemblyCount > 0)
                {
                    _itemType = FixturePackageInfoType.Assembly;
                }
                else if (model.ComponentCount > 0)
                {
                    _itemType = FixturePackageInfoType.Component;
                }

                _folder = folder;
            }

            #endregion

        }

        #endregion

        #region Fields

        private readonly UserEditorSettingsViewModel _systemSettingsView;
        private readonly PlanogramViewObjectView _activePlanView;

        private readonly FixturePackageInfoListViewModel _fixturePackageInfoListView = new FixturePackageInfoListViewModel();
        private readonly BulkObservableCollection<FixturePackageInfoView> _fixturePackageInfos = new BulkObservableCollection<FixturePackageInfoView>();
        private FixturePackageInfoView _selectedFixturePackageInfo;

        private readonly FolderViewModel _folderView = new FolderViewModel();

        #endregion

        #region Binding Property Path

        //Properties
        public static readonly PropertyPath AvailableFixturePackageInfosProperty = WpfHelper.GetPropertyPath<FixtureLibraryPanelViewModel>(p => p.AvailableFixturePackageInfos);
        public static readonly PropertyPath SelectedFixturePackageInfoProperty = WpfHelper.GetPropertyPath<FixtureLibraryPanelViewModel>(p => p.SelectedFixturePackageInfo);
        public static readonly PropertyPath RootFolderProperty = WpfHelper.GetPropertyPath<FixtureLibraryPanelViewModel>(p => p.RootFolder);

        //Commands
        public static readonly PropertyPath AddToPlanogramCommandProperty = WpfHelper.GetPropertyPath<FixtureLibraryPanelViewModel>(p => p.AddToPlanogramCommand);
        public static readonly PropertyPath EditFixturePackageCommandProperty = WpfHelper.GetPropertyPath<FixtureLibraryPanelViewModel>(p => p.EditFixturePackageCommand);
        public static readonly PropertyPath DeleteFixturePackageCommandProperty = WpfHelper.GetPropertyPath<FixtureLibraryPanelViewModel>(p => p.DeleteFixturePackageCommand);
        //public static readonly PropertyPath DeleteFolderCommandProperty = WpfHelper.GetPropertyPath<FixtureLibraryPanelViewModel>(p => p.DeleteFolderCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the collection of available fixture packages
        /// </summary>
        public BulkObservableCollection<FixturePackageInfoView> AvailableFixturePackageInfos
        {
            get
            {
                if (_fixturePackageInfoListView.Model == null)
                {
                    Refresh();
                }
                return _fixturePackageInfos;
            }
        }

        /// <summary>
        /// Gets/Sets the currently selected fixture
        /// </summary>
        public FixturePackageInfoView SelectedFixturePackageInfo
        {
            get { return _selectedFixturePackageInfo; }
            set
            {
                _selectedFixturePackageInfo = value;
                OnPropertyChanged(SelectedFixturePackageInfoProperty);
            }
        }

        /// <summary>
        /// Returns the root folder.
        /// </summary>
        public Folder RootFolder
        {
            get { return _folderView.Model; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        public FixtureLibraryPanelViewModel()
            : this(App.ViewState.Settings, App.ViewState.ActivePlanogramView)
        { }

        /// <summary>
        /// Testing Constructor
        /// </summary>
        /// <param name="settingsView"></param>
        /// <param name="activePlanView"></param>
        public FixtureLibraryPanelViewModel(UserEditorSettingsViewModel settingsView, PlanogramViewObjectView activePlanView)
        {
            _systemSettingsView = settingsView;
            _systemSettingsView.ModelChanged += SystemSettingsView_ModelChanged;

            _activePlanView = activePlanView;
            _folderView.ModelChanged += FolderView_ModelChanged;
            _fixturePackageInfoListView.ModelChanged += FixturePackageInfoListView_ModelChanged;

            App.ViewState.FixtureLibraryChanged += ViewState_FixtureLibraryChanged;
        }


        #endregion

        #region Commands

        #region AddToPlanogram Command

        private RelayCommand _addToPlanogramCommand;

        /// <summary>
        /// Adds the given fixture to the selected planogram
        /// </summary>
        public RelayCommand AddToPlanogramCommand
        {
            get
            {
                if (_addToPlanogramCommand == null)
                {
                    _addToPlanogramCommand = new RelayCommand(
                        p => AddToPlanogram_Executed(p),
                        p => AddToPlanogram_CanExecute(p))
                    {
                        FriendlyName = Message.FixtureLibrary_AddToPlanogram,
                        SmallIcon = ImageResources.Add_16
                    };
                    base.ViewModelCommands.Add(_addToPlanogramCommand);
                }
                return _addToPlanogramCommand;
            }
        }

        private Boolean AddToPlanogram_CanExecute(Object arg)
        {
            if (arg == null && this.SelectedFixturePackageInfo == null)
            {
                return false;
            }

            if (_activePlanView.Model == null)
            {
                return false;
            }

            return true;
        }

        private void AddToPlanogram_Executed(Object arg)
        {
            base.ShowWaitCursor(true);

            FixturePackageInfoView info = arg as FixturePackageInfoView;
            if (info == null)
            {
                info = this.SelectedFixturePackageInfo;
            }

            PlanogramView plan = _activePlanView.Model;

            if (info != null && plan != null)
            {
                //fetch the fixture library item.
                using (FixturePackageViewModel packageView = new FixturePackageViewModel())
                {
                    try
                    {
                        packageView.Fetch(info.Model);
                    }
                    catch (DataPortalException ex)
                    {
                        base.ShowWaitCursor(false);

                        LocalHelper.RecordException(ex.GetBaseException());
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(info.Name, Galleria.Framework.Controls.Wpf.OperationType.Open);

                        return;
                    }

                    using (var undoableAction = plan.NewUndoableAction())
                    {
                        packageView.AddToPlanogram(plan, null);
                    }
                }


                //zoom all to fit - 
                MainPageCommands.ZoomToFit.Execute();
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region EditFixturePackageCommand

        private RelayCommand _editFixturePackageCommand;

        /// <summary>
        /// Allows the user to rename the given fixture, change folder etc.
        /// </summary>
        public RelayCommand EditFixturePackageCommand
        {
            get
            {
                if (_editFixturePackageCommand == null)
                {
                    _editFixturePackageCommand = new RelayCommand(
                        p => EditFixturePackage_Executed(p),
                        p => EditFixturePackage_CanExecute(p))
                    {
                        FriendlyName = Message.FixtureLibrary_EditFixturePackage,
                        SmallIcon = ImageResources.FixtureLibrary_EditFixturePackage
                    };
                    base.ViewModelCommands.Add(_editFixturePackageCommand);
                }
                return _editFixturePackageCommand;
            }
        }

        private Boolean EditFixturePackage_CanExecute(Object args)
        {
            FixturePackageInfoView info = args as FixturePackageInfoView;
            if (info == null && this.SelectedFixturePackageInfo == null)
            {
                return false;
            }

            return true;
        }

        private void EditFixturePackage_Executed(Object args)
        {
            FixturePackageInfoView info = args as FixturePackageInfoView;
            if (info == null)
            {
                info = this.SelectedFixturePackageInfo;
            }


            if (info != null)
            {

                String newFolderPath = info.Folder.FileSystemPath;

                using (FixturePackageViewModel packageView = new FixturePackageViewModel())
                {
                    FixturePackage tempModel = FixturePackage.NewFixturePackage();
                    CopyFixtureInfoValues(info.Model, tempModel);

                    String[] takenNames = _fixturePackageInfos.Except(new FixturePackageInfoView[] { info }).Select(f => f.Name).ToArray();
                    String newName = info.Name;

                    Boolean continueWithAction = true;

                    if (this.AttachedControl != null)
                    {
                        continueWithAction = FixtureLibraryHelper.AddFixtureLibraryPromptUserForValues(
                             Message.FixtureLibrary_EditFixturePackageDialogTitle,
                             Message.FixtureLibrary_EditFixturePackageDialogText,
                             Message.FixtureLibrary_EditFixturePackageDialogNotUniqueText,
                             true,
                             s => !takenNames.Contains(s),
                             info.Name,
                             info.Folder.FileSystemPath,
                             out newName,
                             out newFolderPath,
                             tempModel);


                    }
                    else
                    {
                        //we are unit testing
                        newName = Guid.NewGuid().ToString();
                    }

                    if (continueWithAction)
                    {
                        base.ShowWaitCursor(true);

                        try
                        {
                            //Get full object once everything is updated. Cant fetch before as it looks the folder structure
                            //so it cant be editted in the folder selector.
                            packageView.Fetch(info.Model);

                            //Copy values from temp model
                            CopyTempFixtureValues(packageView, tempModel);

                            //Update with new name
                            packageView.Model.Name = newName;

                            //Save any property changes
                            packageView.Model.SaveAs(packageView.Model.FileName);
                        }
                        catch (Exception ex)
                        {
                            base.ShowWaitCursor(false);

                            Exception rootEx = ex.GetBaseException();

                            LocalHelper.RecordException(rootEx);

                            if (this.AttachedControl != null)
                            {
                                Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                                    info.Name, OperationType.Save);
                            }
                        }
                    }
                }

                //Move the file to a different folder if neccesary
                if (Path.GetDirectoryName((String)info.Id) != Path.Combine(newFolderPath))
                {
                    try
                    {
                        //Move fixture package
                        FixturePackageInfo.MoveFixturePackage((String)info.Id, newFolderPath);
                    }
                    catch (Exception ex)
                    {
                        base.ShowWaitCursor(false);

                        Exception rootEx = ex.GetBaseException();

                        LocalHelper.RecordException(rootEx);

                        if (this.AttachedControl != null)
                        {
                            Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                                info.Name, OperationType.Save);
                        }
                    }
                }
                          
                //Refresh folders and fixture libraries
                Refresh();

                base.ShowWaitCursor(false);
            }
        }

        private void CopyFixtureInfoValues(FixturePackageInfo info, FixturePackage model)
        {
            //Copy values that could be editted to 
            model.CustomAttribute1 = info.CustomAttribute1;
            model.CustomAttribute2 = info.CustomAttribute2;
            model.CustomAttribute3 = info.CustomAttribute3;
            model.CustomAttribute4 = info.CustomAttribute4;
            model.CustomAttribute5 = info.CustomAttribute5;
        }

        private void CopyTempFixtureValues(FixturePackageViewModel view, FixturePackage tempModel)
        {
            //Copy values that could have been editted back 
            if (view.Model.CustomAttribute1 != tempModel.CustomAttribute1)
            {
                view.Model.CustomAttribute1 = tempModel.CustomAttribute1;
            }
            if (view.Model.CustomAttribute2 != tempModel.CustomAttribute2)
            {
                view.Model.CustomAttribute2 = tempModel.CustomAttribute2;
            }
            if (view.Model.CustomAttribute3 != tempModel.CustomAttribute3)
            {
                view.Model.CustomAttribute3 = tempModel.CustomAttribute3;
            }
            if (view.Model.CustomAttribute4 != tempModel.CustomAttribute4)
            {
                view.Model.CustomAttribute4 = tempModel.CustomAttribute4;
            }
            if (view.Model.CustomAttribute5 != tempModel.CustomAttribute5)
            {
                view.Model.CustomAttribute5 = tempModel.CustomAttribute5;
            }
        }

        #endregion

        #region DeleteFixturePackage Command

        private RelayCommand _deleteFixturePackageCommand;

        /// <summary>
        /// Deletes the given fixture
        /// </summary>
        public RelayCommand DeleteFixturePackageCommand
        {
            get
            {
                if (_deleteFixturePackageCommand == null)
                {
                    _deleteFixturePackageCommand = new RelayCommand(
                        p => DeleteFixturePackage_Executed(p))
                    {
                        FriendlyName = Message.FixtureLibrary_DeleteFixturePackage,
                        SmallIcon = ImageResources.Delete_16,
                        InputGestureKey = Key.Delete
                    };
                    base.ViewModelCommands.Add(_deleteFixturePackageCommand);
                }
                return _deleteFixturePackageCommand;
            }
        }

        private void DeleteFixturePackage_Executed(Object arg)
        {
            FixturePackageInfoView info = arg as FixturePackageInfoView;
            if (info == null)
            {
                info = this.SelectedFixturePackageInfo;
            }

            if (info != null)
            {
                Boolean continueWithDelete = false;

                if (this.AttachedControl != null)
                {
                    ModalMessage warning =
                        new ModalMessage()
                        {
                            Header = info.Name,
                            Title = Message.FixtureLibrary_DeleteTitle,
                            Description = Message.FixtureLibrary_DeleteWarning,
                            MessageIcon = ImageResources.Warning_32,
                            ButtonCount = 2,
                            Button1Content = Message.Generic_Delete,
                            Button2Content = Message.Generic_Cancel
                        };
                    App.ShowWindow(warning, /*isModal*/true);
                    if (warning.Result == ModalMessageResult.Button1)
                    {
                        continueWithDelete = true;
                    }
                }
                else
                {
                    //unit testing
                    continueWithDelete = true;
                }

                if (continueWithDelete)
                {
                    base.ShowWaitCursor(true);

                    FixtureLibraryHelper.DeleteFixturePackage(info.Model);
                    Refresh();

                    base.ShowWaitCursor(false);
                }
            }
        }

        #endregion

        #region DeleteFolderCommand

        //private RelayCommand _deleteFolderCommand;

        ///// <summary>
        ///// Deletes the given folder.
        ///// </summary>
        //public RelayCommand DeleteFolderCommand
        //{
        //    get
        //    {
        //        if (_deleteFolderCommand == null)
        //        {
        //            _deleteFolderCommand = new RelayCommand(
        //               p => DeleteFolder_Executed(p),
        //               p => DeleteFolder_CanExecute(p))
        //               {
        //                   FriendlyName = Message.FixtureLibrary_DeleteFolder,
        //                   SmallIcon = ImageResources.FixtureLibrary_DeleteFolder_16,
        //               };
        //            base.ViewModelCommands.Add(_deleteFolderCommand);
        //        }
        //        return _deleteFolderCommand;
        //    }
        //}

        //private Boolean DeleteFolder_CanExecute(Object args)
        //{
        //    //must have item selectd
        //    FixturePackageInfoView info = args as FixturePackageInfoView;
        //    if(info == null)
        //    {
        //        return false;
        //    }

        //    //cannot delete root folder
        //    Folder folder = info.Folder;
        //    if (info.Folder.Parent == null)
        //    {
        //        return false;
        //    }

        //    return true;
        //}

        //private void DeleteFolder_Executed(Object args)
        //{
        //    FixturePackageInfoView info = args as FixturePackageInfoView;
        //    if (info == null)
        //    {
        //        info = this.SelectedFixturePackageInfo;
        //    }
        //    if (info != null)
        //    {
        //        Folder folder = info.Folder;

        //        if (folder != null)
        //        {
        //            //confirm the delete with the user.
        //            Boolean deleteConfirmed = true;
        //            if (this.AttachedControl != null)
        //            {
        //                deleteConfirmed = false;

        //                ModalMessage msg = new ModalMessage()
        //                {
        //                    ButtonCount = 2,
        //                    Button1Content = Message.Generic_Delete,
        //                    Button2Content = Message.Generic_Cancel,
        //                    Title = Message.FixtureLibrary_DeleteFolder_WarningTitle,
        //                    Description = Message.FixtureLibrary_DeleteFolder_WarningText,
        //                    Header = folder.Name,
        //                    MessageIcon = ImageResources.Warning_32
        //                };
        //                App.ShowWindow(msg, /*isModal*/true);

        //                if (msg.Result == ModalMessageResult.Button1)
        //                {
        //                    deleteConfirmed = true;
        //                }
        //            }


        //            if (deleteConfirmed)
        //            {
        //                base.ShowWaitCursor(true);

        //                //remove from the parent list.
        //                folder.Parent.ChildFolders.Remove(folder);

        //                try
        //                {
        //                    _folderView.Save();
        //                }
        //                catch (DataPortalException ex)
        //                {
        //                    base.ShowWaitCursor(false);

        //                    Exception rootEx = ex.GetBaseException();

        //                    LocalHelper.RecordException(rootEx);

        //                    if (this.AttachedControl != null)
        //                    {
        //                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
        //                            folder.Name, OperationType.Delete);
        //                    }

        //                }

        //                base.ShowWaitCursor(false);
        //            }
        //        }
        //    }
        //}

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the viewstate raises the FixtureLibraryChanged event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewState_FixtureLibraryChanged(object sender, EventArgs e)
        {
            Refresh();
        }

        /// <summary>
        /// Called whenever the system settings change
        /// </summary>
        private void SystemSettingsView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<UserEditorSettings> e)
        {
            //refresh the fixture infos just in case the default directory has changed.
            Refresh();
        }

        /// <summary>
        /// Called whenever the folder view model changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FolderView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<Folder> e)
        {
            OnPropertyChanged(RootFolderProperty);
            _fixturePackageInfoListView.FetchAll();
        }

        /// <summary>
        /// Called when the fixture package info list updates.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FixturePackageInfoListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<FixturePackageInfoList> e)
        {
            Object selectedPackageId =
                (this.SelectedFixturePackageInfo != null) ? this.SelectedFixturePackageInfo.Id : null;

            if (e.OldModel != null)
            {
                _fixturePackageInfos.Clear();
            }

            if (e.NewModel != null)
            {
                //Get all folders that exist
                IEnumerable<Folder> availableFolders = this.RootFolder.GetAllChildFolders();

                foreach(FixturePackageInfo modelItem in e.NewModel)
                {
                    //Find folder
                    Folder folder = availableFolders.FirstOrDefault(p => p.Id.Equals(modelItem.FolderId));
                    if (folder == null)
                    {
                        folder = (this.RootFolder.Id.Equals(modelItem.FolderId)) ? this.RootFolder : null;
                    }

                    //Create fixture package
                    _fixturePackageInfos.Add(new FixturePackageInfoView(modelItem, folder));
                }
                this.SelectedFixturePackageInfo = _fixturePackageInfos.FirstOrDefault(f => Object.Equals(f.Id, selectedPackageId));
            }
            else
            {
                this.SelectedFixturePackageInfo = null;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Refreshes the available fixture packages collection.
        /// </summary>
        private void Refresh()
        {
            _folderView.FetchByDirectoryName(App.ViewState.GetSessionDirectory(SessionDirectory.FixtureLibrary));
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                App.ViewState.FixtureLibraryChanged -= ViewState_FixtureLibraryChanged;
                _folderView.ModelChanged -= FolderView_ModelChanged;
                _fixturePackageInfoListView.ModelChanged -= FixturePackageInfoListView_ModelChanged;
                _systemSettingsView.ModelChanged -= SystemSettingsView_ModelChanged;

                base.IsDisposed = true;
            }
        }

        #endregion

    }

    /// <summary>
    /// Event args for dragging a fixture library item.
    /// </summary>
    public sealed class FixtureLibraryItemDropEventArgs : EventArgs
    {
        public FixturePackageInfo Fixture { get; private set; }

        public FixtureLibraryItemDropEventArgs(FixturePackageInfo fixture)
        {
            this.Fixture = fixture;
        }
    }
}
