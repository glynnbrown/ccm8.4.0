﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25873 : A.Probyn
//      Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using System.ComponentModel;
using System.Windows.Threading;

namespace Galleria.Ccm.Editor.Client.Wpf.FixtureLibrary
{
    /// <summary>
    /// Dialog window to provide a method by which a name for save as operations may be provided
    /// </summary>
    public partial class FixtureLibraryAddEditWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
           DependencyProperty.Register("ViewModel", typeof(FixtureLibraryAddEditWindowViewModel), typeof(FixtureLibraryAddEditWindow),
           new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            FixtureLibraryAddEditWindow senderControl = (FixtureLibraryAddEditWindow)obj;

            if (e.OldValue != null)
            {
                FixtureLibraryAddEditWindowViewModel oldModel = e.OldValue as FixtureLibraryAddEditWindowViewModel;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                FixtureLibraryAddEditWindowViewModel newModel = e.NewValue as FixtureLibraryAddEditWindowViewModel;
                newModel.AttachedControl = senderControl;
            }
        }



        /// <summary>
        /// Viewmodel Controller
        /// </summary>
        public FixtureLibraryAddEditWindowViewModel ViewModel
        {
            get { return (FixtureLibraryAddEditWindowViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region DescriptionProperty

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(String), typeof(FixtureLibraryAddEditWindow),
            new PropertyMetadata(String.Empty));

        /// <summary>
        /// Gets/Sets the Description text
        /// </summary>
        public String Description
        {
            get { return (String)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public FixtureLibraryAddEditWindow(FixturePackage fixturePackage, 
                                           Boolean isNew,
                                           String folderPath)
        {
            this.ViewModel = new FixtureLibraryAddEditWindowViewModel(fixturePackage, isNew, folderPath);

            InitializeComponent();

            this.Loaded += new RoutedEventHandler(FixtureLibraryAddEditWindow_Loaded);
        }

        private void FixtureLibraryAddEditWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= FixtureLibraryAddEditWindow_Loaded;

            //focus on the text and select all,
            //had to do this on a delay otherwise it loses it.
            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    this.xName.Focus();
                    this.xName.SelectAll();

                }), System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Returns a true dialog result on save click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        /// <summary>
        /// Returns a false result on cancel click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        #endregion
        
        #region Window Close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion
    }
}
