﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25873 : A.Probyn
//      Created
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Model;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Framework.ViewModel;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using System.Globalization;
using Csla;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Editor.Client.Wpf.FixtureLibrary
{
    /// <summary>
    /// Viewmodel controller for the FolderSelector.
    /// </summary>
    public sealed class FolderSelectorViewModel : ViewModelAttachedControlObject<FolderSelector>
    {
        #region Fields
          
        private FolderViewModel _folderView;
        private Folder _selectedFolder;

        #endregion

        #region Binding Property Path

        public static readonly PropertyPath RootFolderProperty = WpfHelper.GetPropertyPath<FolderSelectorViewModel>(p => p.RootFolder);
        public static readonly PropertyPath SelectedFolderProperty = WpfHelper.GetPropertyPath<FolderSelectorViewModel>(p => p.SelectedFolder);

        public static readonly PropertyPath SelectFolderCommandProperty = WpfHelper.GetPropertyPath<FolderSelectorViewModel>(p => p.SelectFolderCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the root folder.
        /// </summary>
        public Folder RootFolder
        {
            get { return _folderView.Model; }
        }

        /// <summary>
        /// Gets/sets the selected folder
        /// </summary>
        public Folder SelectedFolder
        {
            get { return _selectedFolder; }
            set
            {
                _selectedFolder = value;
                OnPropertyChanged(SelectedFolderProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        public FolderSelectorViewModel()
        {
            _folderView = new FolderViewModel();
            _folderView.ModelChanged += FolderView_ModelChanged;

            //Trigger load of folders
            LoadFolders();
        }


        #endregion

        #region Commands

        #region SelectFolderCommand

        private RelayCommand _selectFolderCommand;

        /// <summary>
        /// Selects a new folder
        /// </summary>
        public RelayCommand SelectFolderCommand
        {
            get
            {
                if (_selectFolderCommand == null)
                {
                    _selectFolderCommand = new RelayCommand(
                        p => SelectFolder_Executed(),
                        p => SelectFolder_CanExecute())
                        {
                            FriendlyName = Message.Generic_Select,
                            DisabledReason = Message.FolderSelector_Select_Disabled
                        };
                    base.ViewModelCommands.Add(_selectFolderCommand);
                }
                return _selectFolderCommand;
            }
        }

        private Boolean SelectFolder_CanExecute()
        {
            if (this.SelectedFolder == null)
            { 
                return false;
            }
            return true;
        }

        private void SelectFolder_Executed()
        {
            this.AttachedControl.DialogResult = true;
        }

        #endregion

        #region NewFolderCommand

        private RelayCommand _newFolderCommand;

        /// <summary>
        /// Creates a new folder
        /// </summary>
        public RelayCommand NewFolderCommand
        {
            get
            {
                if (_newFolderCommand == null)
                {
                    _newFolderCommand = new RelayCommand(
                        p => NewFolder_Executed(p))
                        {
                            FriendlyName = Message.FixtureLibrary_NewFolder,
                            SmallIcon = ImageResources.FixtureLibrary_NewFolder_16
                        };
                    base.ViewModelCommands.Add(_newFolderCommand);
                }
                return _newFolderCommand;
            }
        }

        private void NewFolder_Executed(Object args)
        {
            //Determine which folder the new one should be added to.
            Folder parentFolder = args as Folder;
            if (parentFolder == null)
            {
                parentFolder = this.RootFolder;
            }


            if (parentFolder != null)
            {
                Folder newFolder = Folder.NewFolder();
                parentFolder.ChildFolders.Add(newFolder);

                //set the new folder initial name
                String newFolderName = Message.FixtureLibrary_NewFolderName;
                Int16 folderCount = 2;
                while (parentFolder.ChildFolders.Select(s => s.Name).Contains(newFolderName))
                {
                    newFolderName = String.Format(CultureInfo.CurrentCulture, "{0} ({1})",
                        Message.FixtureLibrary_NewFolderName, folderCount);

                    folderCount++;
                }
                newFolder.Name = newFolderName;

                //request a folder name from the user.
                Boolean continueWithAction = SetFolderName(newFolder);
                if (continueWithAction)
                {
                    base.ShowWaitCursor(true);

                    try
                    {
                        //save immediately as this may be in a file structure
                        _folderView.Save();
                    }
                    catch (DataPortalException ex)
                    {
                        base.ShowWaitCursor(false);

                        Exception rootEx = ex.GetBaseException();
                        LocalHelper.RecordException(rootEx);

                        if (this.AttachedControl != null)
                        {
                            Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                                newFolder.Name, OperationType.Save);
                        }

                    }

                }
                else
                {
                    //remove the folder again
                    parentFolder.ChildFolders.Remove(newFolder);
                }

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region RenameFolderCommand

        private RelayCommand _renameFolderCommand;

        /// <summary>
        /// Allows the user to rename the given folder.
        /// </summary>
        public RelayCommand RenameFolderCommand
        {
            get
            {
                if (_renameFolderCommand == null)
                {
                    _renameFolderCommand = new RelayCommand(
                        p => RenameFolder_Executed(p),
                        p => RenameFolder_CanExecute(p))
                        {
                            FriendlyName = Message.FixtureLibrary_RenameFolder,
                            SmallIcon = ImageResources.FixtureLibrary_RenameFolder
                        };
                    base.ViewModelCommands.Add(_renameFolderCommand);
                }
                return _renameFolderCommand;
            }
        }

        private Boolean RenameFolder_CanExecute(Object args)
        {
            Folder folder = args as Folder;

            if (folder == null || folder == this.RootFolder)
            {
                return false;
            }

            return true;
        }

        private void RenameFolder_Executed(Object args)
        {
            Folder folder = args as Folder;
            if (folder != null)
            {
                Boolean continueWithAction = SetFolderName(folder);

                if (continueWithAction)
                {
                    base.ShowWaitCursor(true);

                    try
                    {
                        _folderView.Save();
                    }
                    catch (DataPortalException ex)
                    {
                        base.ShowWaitCursor(false);

                        Exception rootEx = ex.GetBaseException();

                        LocalHelper.RecordException(rootEx);

                        if (this.AttachedControl != null)
                        {
                            Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                                folder.Name, OperationType.Save);
                        }
                    }

                    base.ShowWaitCursor(false);
                }
            }
        }

        private Boolean SetFolderName(Folder folder)
        {
            Boolean nameSet = false;

            if (folder != null)
            {
                String[] takenNames = folder.Parent.ChildFolders.Except(new Folder[] { folder }).Select(c => c.Name).ToArray();
                String newName = folder.Name;

                nameSet = true;

                if (this.AttachedControl != null)
                {
                    nameSet =
                    Galleria.Framework.Controls.Wpf.Helpers.PromptUserForValue(
                        Message.FixtureLibrary_RenameFolderDialogTitle,
                        Message.FixtureLibrary_RenameFolderDialogText,
                        Message.FixtureLibrary_RenameFolderDialogNotUniqueText,
                        Message.Generic_Name,
                        /*forceFirstShow*/true,
                        s => !takenNames.Contains(s),
                        folder.Name, out newName);

                    if (nameSet)
                    {
                        folder.Name = newName;
                    }
                }
                else
                {
                    //we are unit testing
                    folder.Name = "TEST";
                }
            }

            return nameSet;
        }

        #endregion

        #region DeleteFolderCommand

        private RelayCommand _deleteFolderCommand;

        /// <summary>
        /// Deletes the given folder.
        /// </summary>
        public RelayCommand DeleteFolderCommand
        {
            get
            {
                if (_deleteFolderCommand == null)
                {
                    _deleteFolderCommand = new RelayCommand(
                       p => DeleteFolder_Executed(p),
                       p => DeleteFolder_CanExecute(p))
                       {
                           FriendlyName = Message.FixtureLibrary_DeleteFolder,
                           SmallIcon = ImageResources.FixtureLibrary_DeleteFolder_16,
                       };
                    base.ViewModelCommands.Add(_deleteFolderCommand);
                }
                return _deleteFolderCommand;
            }
        }

        private Boolean DeleteFolder_CanExecute(Object args)
        {
            Folder folderToDelete = args as Folder;

            //must have a folder specified.
            if (folderToDelete == null)
            {
                return false;
            }

            //cannot delete the root folder.
            if (folderToDelete == this.RootFolder)
            {
                return false;
            }









            return true;
        }










        private void DeleteFolder_Executed(Object args)
        {
            Folder folder = args as Folder;
            if (folder != null)
            {
                //confirm the delete with the user.
                Boolean deleteConfirmed = true;
                if (this.AttachedControl != null)
                {
                    deleteConfirmed = false;

                    ModalMessage msg = new ModalMessage()
                    {
                        ButtonCount = 2,
                        Button1Content = Message.Generic_Delete,
                        Button2Content = Message.Generic_Cancel,
                        Title = Message.FixtureLibrary_DeleteFolder_WarningTitle,
                        Description = Message.FixtureLibrary_DeleteFolder_WarningText,
                        Header = folder.Name,
                        MessageIcon = ImageResources.Warning_32
                    };
                    App.ShowWindow(msg, /*isModal*/true);

                    if (msg.Result == ModalMessageResult.Button1)
                    {
                        deleteConfirmed = true;
                    }
                }


                if (deleteConfirmed)
                {
                    base.ShowWaitCursor(true);

                    //remove from the parent list.
                    folder.Parent.ChildFolders.Remove(folder);






                    try
                    {
                        _folderView.Save();
                    }
                    catch (DataPortalException ex)
                    {
                        base.ShowWaitCursor(false);

                        Exception rootEx = ex.GetBaseException();

                        LocalHelper.RecordException(rootEx);





                        if (this.AttachedControl != null)
                        {
                            Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                                folder.Name, OperationType.Delete);
                        }


                    }

                    base.ShowWaitCursor(false);
                }
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the folder view model changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FolderView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<Folder> e)
        {
            OnPropertyChanged(RootFolderProperty);

            //Load into control
        }

        #endregion

        #region Methods

        /// <summary>
        /// Refreshes the available fixture packages collection.
        /// </summary>
        private void LoadFolders()
        {
            _folderView.FetchByDirectoryName(App.ViewState.GetSessionDirectory(SessionDirectory.FixtureLibrary));
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                _folderView.ModelChanged -= FolderView_ModelChanged;
                _selectedFolder = null;

                base.IsDisposed = true;
            }
        }

        #endregion

    }
}
