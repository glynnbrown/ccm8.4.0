﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25573 : A.Probyn
//      Created
// V8-28358 : D.Pleasance
//  Added validation to check for invalid file characters
#endregion
#region Version history: (CCM 8.1.0)
// V8-31057 : A.Probyn
//  Added validation to ensure the source fixture package is valid itself (old versions may not be).
#endregion
#endregion

using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Csla;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Collections;
using System.Collections.Generic;
using System.IO;
using System.ComponentModel;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Editor.Client.Wpf.FixtureLibrary
{
    /// <summary>
    /// Viewmodel controller for the FixtureLibraryAddEditWindow.
    /// </summary>
    public sealed class FixtureLibraryAddEditWindowViewModel : ViewModelAttachedControlObject<FixtureLibraryAddEditWindow>, IDataErrorInfo
    {
        #region Fields

        private FixturePackage _fixturePackage;
        private String _folderPath;
        private String _description;

        #endregion

        #region Binding Property Path

        //Properties
        public static readonly PropertyPath DescriptionProperty = WpfHelper.GetPropertyPath<FixtureLibraryAddEditWindowViewModel>(p => p.Description);
        public static readonly PropertyPath FixtureNameProperty = WpfHelper.GetPropertyPath<FixtureLibraryAddEditWindowViewModel>(p => p.FixtureName);
        public static readonly PropertyPath FolderPathProperty = WpfHelper.GetPropertyPath<FixtureLibraryAddEditWindowViewModel>(p => p.FolderPath);
        public static readonly PropertyPath FixturePackageProperty = WpfHelper.GetPropertyPath<FixtureLibraryAddEditWindowViewModel>(p => p.FixturePackage);

        //Commands
        public static readonly PropertyPath SelectFolderCommandProperty = WpfHelper.GetPropertyPath<FixtureLibraryAddEditWindowViewModel>(p => p.SelectFolderCommand);
        public static readonly PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<FixtureLibraryAddEditWindowViewModel>(p => p.SaveCommand);
 
        #endregion

        #region Properties

        /// <summary>
        /// Gets/sets the window description
        /// </summary>
        public String Description
        {
            get { return _description; }
            set
            {
                _description = value;
                OnPropertyChanged(DescriptionProperty);
            }
        }

        /// <summary>
        /// Gets/sets the FixtureName
        /// </summary>
        public String FixtureName
        {
            get { return _fixturePackage.Name; }
            set
            {
                _fixturePackage.Name = value;
                OnPropertyChanged(FixtureNameProperty);
            }
        }

        /// <summary>
        /// Gets/sets the FolderPath
        /// </summary>
        public String FolderPath
        {
            get { return _folderPath; }
            set
            {
                _folderPath = value;
                OnPropertyChanged(FolderPathProperty);
            }
        }

        /// <summary>
        /// Get/Sets the fixture package
        /// </summary>
        public FixturePackage FixturePackage
        {
            get { return _fixturePackage; }
            set
            {
                _fixturePackage = value;
                OnPropertyChanged(FixturePackageProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Testing Constructor
        /// </summary>
        /// <param name="settingsView"></param>
        /// <param name="activePlanView"></param>
        public FixtureLibraryAddEditWindowViewModel(FixturePackage fixturePackage,
                                           Boolean isNew,
                                           String folderPath)
        {
            //Set  fixture
            this._fixturePackage = fixturePackage;
            
            this._folderPath = folderPath;
        }


        #endregion

        #region Commands

        #region SelectFolder Command

        private RelayCommand _selectFolderCommand;

        /// <summary>
        /// Opens folder selector dialog
        /// </summary>
        public RelayCommand SelectFolderCommand
        {
            get
            {
                if (_selectFolderCommand == null)
                {
                    _selectFolderCommand = new RelayCommand(
                        p => SelectFolder_Executed(),
                        p => SelectFolder_CanExecute())
                    {
                        FriendlyName = Message.FixtureLibraryAddEdit_SelectFolder,
                        FriendlyDescription = Message.FixtureLibraryAddEdit_SelectFolder_Desc,
                        SmallIcon = ImageResources.FixtureLibraryAddEdit_SelectFolder_16
                    };
                    base.ViewModelCommands.Add(_selectFolderCommand);
                }
                return _selectFolderCommand;
            }
        }

        private Boolean SelectFolder_CanExecute()
        {
            return true;
        }

        private void SelectFolder_Executed()
        {
            String outputDir = null;

            //Get folder path
            String folderPath;
            if (!String.IsNullOrEmpty(this.FolderPath))
            {
                folderPath = this.FolderPath;
            }
            else
            {
                folderPath = App.ViewState.GetSessionDirectory(SessionDirectory.FixtureLibrary);
            }

            if (this.AttachedControl != null)
            {
                //Setup folder selector
                FolderSelector dialog = new FolderSelector(folderPath);
                dialog.Owner = this.AttachedControl;
                dialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                //Show dialog
                dialog.ShowDialog();

                //If window has result
                if (dialog.DialogResult.HasValue && dialog.DialogResult.Value)
                {
                    //Set output directory
                    outputDir = dialog.ViewModel.SelectedFolder.FileSystemPath;
                }
            }
            else
            {
                //Default to original pre set folder
                outputDir = folderPath;
            }

            this.FolderPath = outputDir;
        }

        #endregion
        
        #region Save Command

        private RelayCommand _saveCommand;

        /// <summary>
        /// Save the given fixture package details
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        p => Save_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_Save,
                        FriendlyDescription = Message.Generic_Save_Tooltip,
                        DisabledReason = Message.Generic_Save_DisabledReasonInvalidData
                    };
                    base.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        private Boolean Save_CanExecute()
        {
            //if source fixture is not valid to begin with
            if (!FixturePackage.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.FixtureLibraryAddEdit_SaveDisabled_InvalidFixture;
                return false;
            }

            return !String.IsNullOrEmpty(this.FixtureName)
                && !String.IsNullOrEmpty(this.FolderPath);
        }

        private void Save_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.DialogResult = true;
            }
        }

        #endregion

        #endregion
        
        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                this._fixturePackage = null;
                
                base.IsDisposed = true;
            }
        }

        #endregion
        
        #region IDataErrorInfo

        string IDataErrorInfo.Error
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Implementation of the IDataErrorInfo interface
        /// </summary>
        /// <param name="columnName">Property name</param>
        /// <returns>String containing error</returns>
        string IDataErrorInfo.this[String columnName]
        {
            get
            {
                String result = null;

                if (columnName == FixtureNameProperty.Path)
                {
                    if (String.IsNullOrEmpty(this.FixtureName)
                        || String.IsNullOrWhiteSpace(this.FixtureName))
                    {
                        result = Message.FixtureLibraryAddEdit_SaveDisabled_NoFixtureName;
                    }
                    else
                    {
                        if (this.FixtureName.IndexOfAny(Path.GetInvalidFileNameChars()) >= 0)
                        {
                            result = Message.FixtureLibraryAddEdit_SaveDisabled_InvalidCharacters;
                        }
                    }
                }

                if (columnName == FolderPathProperty.Path)
                {
                    if (String.IsNullOrEmpty(this.FolderPath)
                        || String.IsNullOrWhiteSpace(this.FolderPath))
                    {
                        result = Message.FixtureLibraryAddEdit_SaveDisabled_NoFolderPath;
                    }
                }

                return result;
            }
        }

        #endregion
    }
}