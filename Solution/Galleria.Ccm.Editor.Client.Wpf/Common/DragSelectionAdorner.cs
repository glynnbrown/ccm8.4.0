﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion
#endregion


using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Globalization;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Facilitates drag selections.
    /// </summary>
    public sealed class DragSelectionAdorner: Adorner
    {
        #region Fields

        private Point? _startPoint; //the starting point of the rubberband
        private Point? _endPoint; //the end point of the rubber band
        private Pen _rubberBandPen; //the pen to draw with
        private Canvas _adornedCanvas;
        private String _displayText;

        #endregion

        #region Properties

        /// <summary>
        /// The starting point of the rubber band
        /// </summary>
        public Point? StartPoint
        {
            get { return _startPoint; }
            set { _startPoint = value; }
        }

        public Brush PenColour
        {
            get { return _rubberBandPen.Brush; }
            set { _rubberBandPen.Brush = value; }
        }

        public DashStyle PenDashStyle
        {
            get { return _rubberBandPen.DashStyle; }
            set { _rubberBandPen.DashStyle = value; }
        }

        public String DisplayText
        {
            get { return _displayText; }
            set { _displayText = value; }
        }


        #endregion

        #region Events

        #region DragSelectionCompleted

        public static readonly RoutedEvent DragSelectionCompletedEvent =
            EventManager.RegisterRoutedEvent("DragSelectionCompleted", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(DragSelectionAdorner));

        public event RoutedEventHandler DragSelectionCompleted
        {
            add { AddHandler(DragSelectionCompletedEvent, value); }
            remove { RemoveHandler(DragSelectionCompletedEvent, value); }
        }

        private void OnDragSelectionCompleted()
        {
            //raise out the selected event
            RaiseEvent(new RoutedEventArgs(DragSelectionAdorner.DragSelectionCompletedEvent));
        }

        #endregion

        #region DragSizeChanged

        public static readonly RoutedEvent DragSizeChangedEvent =
            EventManager.RegisterRoutedEvent("DragSizeChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(DragSelectionAdorner));

        public event RoutedEventHandler DragSizeChanged
        {
            add { AddHandler(DragSizeChangedEvent, value); }
            remove { RemoveHandler(DragSizeChangedEvent, value); }
        }

        private void OnDragSizeChanged()
        {
            //raise out the selected event
            RaiseEvent(new RoutedEventArgs(DragSelectionAdorner.DragSizeChangedEvent));
        }

        #endregion

        #endregion

        #region Constructor

        public DragSelectionAdorner(Canvas canvas, Point dragStartPoint)
            : base(canvas)
        {
            _adornedCanvas = canvas;
            _startPoint = dragStartPoint;

            _rubberBandPen = new Pen(Brushes.LightSlateGray, 1);
            _rubberBandPen.DashStyle = new DashStyle(new Double[] { 2 }, 1);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Updates the visual & selected items on mouse move
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseMove(System.Windows.Input.MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {

                if (!this.IsMouseCaptured)
                {
                    CaptureMouse();
                }

                _endPoint = e.GetPosition(this);

                OnDragSizeChanged();
                this.InvalidateVisual();
            }
            else
            {
                if (this.IsMouseCaptured)
                {
                    ReleaseMouseCapture();
                }
            }

            e.Handled = true;
        }

        /// <summary>
        /// Finalises the selected items on mouse up
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseUp(System.Windows.Input.MouseButtonEventArgs e)
        {
            ReleaseMouseCapture();

            // remove this adorner from adorner layer
            AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(_adornedCanvas);
            if (adornerLayer != null)
            {
                adornerLayer.Remove(this);
            }

            e.Handled = false;


            OnDragSelectionCompleted();
        }

        /// <summary>
        /// Draws a fake background on render otherwise the OnMouseMove event would not be fired.
        /// Alternative: implement a Canvas as a child of this adorner, like
        /// the ConnectionAdorner does.
        /// </summary>
        /// <param name="drawingContext"></param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            drawingContext.DrawRectangle(Brushes.Transparent, null, new Rect(RenderSize));

            if (_startPoint.HasValue && _endPoint.HasValue)
            {
                Rect dragRect = new Rect(_startPoint.Value, _endPoint.Value);

                drawingContext.DrawRectangle(Brushes.Transparent,
                    _rubberBandPen, dragRect);

                if(!String.IsNullOrEmpty(this.DisplayText))
                {
                    FormattedText fTxt = new FormattedText(
                        this.DisplayText,
                        CultureInfo.CurrentCulture,
                        System.Windows.FlowDirection.LeftToRight,
                        new Typeface("Arial"),
                        12,
                        this.PenColour);
                    

                    drawingContext.DrawText(fTxt,
                        new Point(dragRect.BottomLeft.X + (dragRect.Width / 2) - (fTxt.Width/2),
                            dragRect.BottomLeft.Y - (dragRect.Height / 2) - (fTxt.Height / 2)));
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the selection rectangle
        /// </summary>
        /// <returns></returns>
        public Rect GetSelectionRect()
        {
            Rect returnRect = Rect.Empty;

            if (_startPoint.HasValue && _endPoint.HasValue)
            {
                returnRect =  new Rect(_startPoint.Value, _endPoint.Value);
            }

            return returnRect;
        }

        #endregion
    }
}
