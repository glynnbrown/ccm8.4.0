﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26337 : A.Kuszyk
//  Added Performance Data properties.
// CCM-26149 : L.Ineson
//  Split away from PlanogramProductMultiView
// V8-27647 : L.Luong
//  Added Inventory properties
#endregion

#region Version History: (CCM 8.03)
// V8-29427 : L.Luong
//  Added Meta Data properties
#endregion

#region Version History: (CCM 8.10)
// V8-29683 : L.Luong
//  Added method to convert to 2 decimal places
#endregion
#region Version History: (CCM 8.20)
// V8-29998 : I.George
// Added Performance Rank Properties
// V8-30992 : A.Probyn
//  Added MinimumInventoryUnits
#endregion
#region Version History: (CCM 830)
// CCM-18956 : M.Pettit
// Added Calculated Metrics
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Galleria.Framework.Attributes;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Presents a merged view of multiple PlanogramPerformanceData model objects
    /// </summary>
    public sealed class PlanogramPerformanceDataMultiView : ViewModelObject, IDataErrorInfo
    {
        #region Fields

        private IEnumerable<PlanogramPerformanceData> _items;

        private PlanogramView _plan;
        private Boolean _isLoggingUndoableActions = true;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath P1Property = GetPropertyPath(p => p.P1);
        public static readonly PropertyPath P1NameProperty = GetPropertyPath(p => p.P1Name);
        public static readonly PropertyPath P1VisibilityProperty = GetPropertyPath(p => p.P1Visibility);
        public static readonly PropertyPath P2NameProperty = GetPropertyPath(p => p.P2Name);
        public static readonly PropertyPath P2VisibilityProperty = GetPropertyPath(p => p.P2Visibility);
        public static readonly PropertyPath P2Property = GetPropertyPath(p => p.P2);
        public static readonly PropertyPath P3NameProperty = GetPropertyPath(p => p.P3Name);
        public static readonly PropertyPath P3VisibilityProperty = GetPropertyPath(p => p.P3Visibility);
        public static readonly PropertyPath P3Property = GetPropertyPath(p => p.P3);
        public static readonly PropertyPath P4NameProperty = GetPropertyPath(p => p.P4Name);
        public static readonly PropertyPath P4VisibilityProperty = GetPropertyPath(p => p.P4Visibility);
        public static readonly PropertyPath P4Property = GetPropertyPath(p => p.P4);
        public static readonly PropertyPath P5NameProperty = GetPropertyPath(p => p.P5Name);
        public static readonly PropertyPath P5VisibilityProperty = GetPropertyPath(p => p.P5Visibility);
        public static readonly PropertyPath P5Property = GetPropertyPath(p => p.P5);
        public static readonly PropertyPath P6NameProperty = GetPropertyPath(p => p.P6Name);
        public static readonly PropertyPath P6VisibilityProperty = GetPropertyPath(p => p.P6Visibility);
        public static readonly PropertyPath P6Property = GetPropertyPath(p => p.P6);
        public static readonly PropertyPath P7NameProperty = GetPropertyPath(p => p.P7Name);
        public static readonly PropertyPath P7VisibilityProperty = GetPropertyPath(p => p.P7Visibility);
        public static readonly PropertyPath P7Property = GetPropertyPath(p => p.P7);
        public static readonly PropertyPath P8NameProperty = GetPropertyPath(p => p.P8Name);
        public static readonly PropertyPath P8VisibilityProperty = GetPropertyPath(p => p.P8Visibility);
        public static readonly PropertyPath P8Property = GetPropertyPath(p => p.P8);
        public static readonly PropertyPath P9NameProperty = GetPropertyPath(p => p.P9Name);
        public static readonly PropertyPath P9VisibilityProperty = GetPropertyPath(p => p.P9Visibility);
        public static readonly PropertyPath P9Property = GetPropertyPath(p => p.P9);
        public static readonly PropertyPath P10NameProperty = GetPropertyPath(p => p.P10Name);
        public static readonly PropertyPath P10VisibilityProperty = GetPropertyPath(p => p.P10Visibility);
        public static readonly PropertyPath P10Property = GetPropertyPath(p => p.P10);
        public static readonly PropertyPath P11NameProperty = GetPropertyPath(p => p.P11Name);
        public static readonly PropertyPath P11VisibilityProperty = GetPropertyPath(p => p.P11Visibility);
        public static readonly PropertyPath P11Property = GetPropertyPath(p => p.P11);
        public static readonly PropertyPath P12NameProperty = GetPropertyPath(p => p.P12Name);
        public static readonly PropertyPath P12VisibilityProperty = GetPropertyPath(p => p.P12Visibility);
        public static readonly PropertyPath P12Property = GetPropertyPath(p => p.P12);
        public static readonly PropertyPath P13NameProperty = GetPropertyPath(p => p.P13Name);
        public static readonly PropertyPath P13VisibilityProperty = GetPropertyPath(p => p.P13Visibility);
        public static readonly PropertyPath P13Property = GetPropertyPath(p => p.P13);
        public static readonly PropertyPath P14NameProperty = GetPropertyPath(p => p.P14Name);
        public static readonly PropertyPath P14VisibilityProperty = GetPropertyPath(p => p.P14Visibility);
        public static readonly PropertyPath P14Property = GetPropertyPath(p => p.P14);
        public static readonly PropertyPath P15NameProperty = GetPropertyPath(p => p.P15Name);
        public static readonly PropertyPath P15VisibilityProperty = GetPropertyPath(p => p.P15Visibility);
        public static readonly PropertyPath P15Property = GetPropertyPath(p => p.P15);
        public static readonly PropertyPath P16NameProperty = GetPropertyPath(p => p.P16Name);
        public static readonly PropertyPath P16VisibilityProperty = GetPropertyPath(p => p.P16Visibility);
        public static readonly PropertyPath P16Property = GetPropertyPath(p => p.P16);
        public static readonly PropertyPath P17NameProperty = GetPropertyPath(p => p.P17Name);
        public static readonly PropertyPath P17VisibilityProperty = GetPropertyPath(p => p.P17Visibility);
        public static readonly PropertyPath P17Property = GetPropertyPath(p => p.P17);
        public static readonly PropertyPath P18NameProperty = GetPropertyPath(p => p.P18Name);
        public static readonly PropertyPath P18VisibilityProperty = GetPropertyPath(p => p.P18Visibility);
        public static readonly PropertyPath P18Property = GetPropertyPath(p => p.P18);
        public static readonly PropertyPath P19NameProperty = GetPropertyPath(p => p.P19Name);
        public static readonly PropertyPath P19VisibilityProperty = GetPropertyPath(p => p.P19Visibility);
        public static readonly PropertyPath P19Property = GetPropertyPath(p => p.P19);
        public static readonly PropertyPath P20NameProperty = GetPropertyPath(p => p.P20Name);
        public static readonly PropertyPath P20VisibilityProperty = GetPropertyPath(p => p.P20Visibility);
        public static readonly PropertyPath P20Property = GetPropertyPath(p => p.P20);
        
        public static readonly PropertyPath CP1NameProperty = GetPropertyPath(p => p.CP1Name);
        public static readonly PropertyPath CP1VisibilityProperty = GetPropertyPath(p => p.CP1Visibility);
        public static readonly PropertyPath CP1Property = GetPropertyPath(p => p.CP1);
        public static readonly PropertyPath CP2NameProperty = GetPropertyPath(p => p.CP2Name);
        public static readonly PropertyPath CP2VisibilityProperty = GetPropertyPath(p => p.CP2Visibility);
        public static readonly PropertyPath CP2Property = GetPropertyPath(p => p.CP2);
        public static readonly PropertyPath CP3NameProperty = GetPropertyPath(p => p.CP3Name);
        public static readonly PropertyPath CP3VisibilityProperty = GetPropertyPath(p => p.CP3Visibility);
        public static readonly PropertyPath CP3Property = GetPropertyPath(p => p.CP3);
        public static readonly PropertyPath CP4NameProperty = GetPropertyPath(p => p.CP4Name);
        public static readonly PropertyPath CP4VisibilityProperty = GetPropertyPath(p => p.CP4Visibility);
        public static readonly PropertyPath CP4Property = GetPropertyPath(p => p.CP4);
        public static readonly PropertyPath CP5NameProperty = GetPropertyPath(p => p.CP5Name);
        public static readonly PropertyPath CP5VisibilityProperty = GetPropertyPath(p => p.CP5Visibility);
        public static readonly PropertyPath CP5Property = GetPropertyPath(p => p.CP5);
        public static readonly PropertyPath CP6NameProperty = GetPropertyPath(p => p.CP6Name);
        public static readonly PropertyPath CP6VisibilityProperty = GetPropertyPath(p => p.CP6Visibility);
        public static readonly PropertyPath CP6Property = GetPropertyPath(p => p.CP6);
        public static readonly PropertyPath CP7NameProperty = GetPropertyPath(p => p.CP7Name);
        public static readonly PropertyPath CP7VisibilityProperty = GetPropertyPath(p => p.CP7Visibility);
        public static readonly PropertyPath CP7Property = GetPropertyPath(p => p.CP7);
        public static readonly PropertyPath CP8NameProperty = GetPropertyPath(p => p.CP8Name);
        public static readonly PropertyPath CP8VisibilityProperty = GetPropertyPath(p => p.CP8Visibility);
        public static readonly PropertyPath CP8Property = GetPropertyPath(p => p.CP8);
        public static readonly PropertyPath CP9NameProperty = GetPropertyPath(p => p.CP9Name);
        public static readonly PropertyPath CP9VisibilityProperty = GetPropertyPath(p => p.CP9Visibility);
        public static readonly PropertyPath CP9Property = GetPropertyPath(p => p.CP9);
        public static readonly PropertyPath CP10NameProperty = GetPropertyPath(p => p.CP10Name);
        public static readonly PropertyPath CP10VisibilityProperty = GetPropertyPath(p => p.CP10Visibility);
        public static readonly PropertyPath CP10Property = GetPropertyPath(p => p.CP10);

        public static readonly PropertyPath UnitsSoldPerDayProperty = GetPropertyPath(p => p.UnitsSoldPerDay);
        public static readonly PropertyPath AchievedCasePacksProperty = GetPropertyPath(p => p.AchievedCasePacks);
        public static readonly PropertyPath AchievedDosProperty = GetPropertyPath(p => p.AchievedDos);
        public static readonly PropertyPath AchievedShelfLifeProperty = GetPropertyPath(p => p.AchievedShelfLife);
        public static readonly PropertyPath AchievedDeliveriesProperty = GetPropertyPath(p => p.AchievedDeliveries);

        #region MetaData

        public static readonly PropertyPath MetaP1PercentageProperty = GetPropertyPath(p => p.MetaP1Percentage);
        public static readonly PropertyPath MetaP2PercentageProperty = GetPropertyPath(p => p.MetaP2Percentage);
        public static readonly PropertyPath MetaP3PercentageProperty = GetPropertyPath(p => p.MetaP3Percentage);
        public static readonly PropertyPath MetaP4PercentageProperty = GetPropertyPath(p => p.MetaP4Percentage);
        public static readonly PropertyPath MetaP5PercentageProperty = GetPropertyPath(p => p.MetaP5Percentage);
        public static readonly PropertyPath MetaP6PercentageProperty = GetPropertyPath(p => p.MetaP6Percentage);
        public static readonly PropertyPath MetaP7PercentageProperty = GetPropertyPath(p => p.MetaP7Percentage);
        public static readonly PropertyPath MetaP8PercentageProperty = GetPropertyPath(p => p.MetaP8Percentage);
        public static readonly PropertyPath MetaP9PercentageProperty = GetPropertyPath(p => p.MetaP9Percentage);
        public static readonly PropertyPath MetaP10PercentageProperty = GetPropertyPath(p => p.MetaP10Percentage);
        public static readonly PropertyPath MetaP11PercentageProperty = GetPropertyPath(p => p.MetaP11Percentage);
        public static readonly PropertyPath MetaP12PercentageProperty = GetPropertyPath(p => p.MetaP12Percentage);
        public static readonly PropertyPath MetaP13PercentageProperty = GetPropertyPath(p => p.MetaP13Percentage);
        public static readonly PropertyPath MetaP14PercentageProperty = GetPropertyPath(p => p.MetaP14Percentage);
        public static readonly PropertyPath MetaP15PercentageProperty = GetPropertyPath(p => p.MetaP15Percentage);
        public static readonly PropertyPath MetaP16PercentageProperty = GetPropertyPath(p => p.MetaP16Percentage);
        public static readonly PropertyPath MetaP17PercentageProperty = GetPropertyPath(p => p.MetaP17Percentage);
        public static readonly PropertyPath MetaP18PercentageProperty = GetPropertyPath(p => p.MetaP18Percentage);
        public static readonly PropertyPath MetaP19PercentageProperty = GetPropertyPath(p => p.MetaP19Percentage);
        public static readonly PropertyPath MetaP20PercentageProperty = GetPropertyPath(p => p.MetaP20Percentage);
        public static readonly PropertyPath MetaCP1PercentageProperty = GetPropertyPath(p => p.MetaCP1Percentage);
        public static readonly PropertyPath MetaCP2PercentageProperty = GetPropertyPath(p => p.MetaCP2Percentage);
        public static readonly PropertyPath MetaCP3PercentageProperty = GetPropertyPath(p => p.MetaCP3Percentage);
        public static readonly PropertyPath MetaCP4PercentageProperty = GetPropertyPath(p => p.MetaCP4Percentage);
        public static readonly PropertyPath MetaCP5PercentageProperty = GetPropertyPath(p => p.MetaCP5Percentage);
        public static readonly PropertyPath MetaCP6PercentageProperty = GetPropertyPath(p => p.MetaCP6Percentage);
        public static readonly PropertyPath MetaCP7PercentageProperty = GetPropertyPath(p => p.MetaCP7Percentage);
        public static readonly PropertyPath MetaCP8PercentageProperty = GetPropertyPath(p => p.MetaCP8Percentage);
        public static readonly PropertyPath MetaCP9PercentageProperty = GetPropertyPath(p => p.MetaCP9Percentage);
        public static readonly PropertyPath MetaCP10PercentageProperty = GetPropertyPath(p => p.MetaCP10Percentage);
        public static readonly PropertyPath MetaP1RankProperty = GetPropertyPath(p => p.MetaP1Rank);
        public static readonly PropertyPath MetaP2RankProperty = GetPropertyPath(p => p.MetaP2Rank);
        public static readonly PropertyPath MetaP3RankProperty = GetPropertyPath(p => p.MetaP3Rank);
        public static readonly PropertyPath MetaP4RankProperty = GetPropertyPath(p => p.MetaP4Rank);
        public static readonly PropertyPath MetaP5RankProperty = GetPropertyPath(p => p.MetaP5Rank);
        public static readonly PropertyPath MetaP6RankProperty = GetPropertyPath(p => p.MetaP6Rank);
        public static readonly PropertyPath MetaP7RankProperty = GetPropertyPath(p => p.MetaP7Rank);
        public static readonly PropertyPath MetaP8RankProperty = GetPropertyPath(p => p.MetaP8Rank);
        public static readonly PropertyPath MetaP9RankProperty = GetPropertyPath(p => p.MetaP9Rank);
        public static readonly PropertyPath MetaP10RankProperty = GetPropertyPath(p => p.MetaP10Rank);
        public static readonly PropertyPath MetaP11RankProperty = GetPropertyPath(p => p.MetaP11Rank);
        public static readonly PropertyPath MetaP12RankProperty = GetPropertyPath(p => p.MetaP12Rank);
        public static readonly PropertyPath MetaP13RankProperty = GetPropertyPath(p => p.MetaP13Rank);
        public static readonly PropertyPath MetaP14RankProperty = GetPropertyPath(p => p.MetaP14Rank);
        public static readonly PropertyPath MetaP15RankProperty = GetPropertyPath(p => p.MetaP15Rank);
        public static readonly PropertyPath MetaP16RankProperty = GetPropertyPath(p => p.MetaP16Rank);
        public static readonly PropertyPath MetaP17RankProperty = GetPropertyPath(p => p.MetaP17Rank);
        public static readonly PropertyPath MetaP18RankProperty = GetPropertyPath(p => p.MetaP18Rank);
        public static readonly PropertyPath MetaP19RankProperty = GetPropertyPath(p => p.MetaP19Rank);
        public static readonly PropertyPath MetaP20RankProperty = GetPropertyPath(p => p.MetaP20Rank);
        public static readonly PropertyPath MetaCP1RankProperty = GetPropertyPath(p => p.MetaCP1Rank);
        public static readonly PropertyPath MetaCP2RankProperty = GetPropertyPath(p => p.MetaCP2Rank);
        public static readonly PropertyPath MetaCP3RankProperty = GetPropertyPath(p => p.MetaCP3Rank);
        public static readonly PropertyPath MetaCP4RankProperty = GetPropertyPath(p => p.MetaCP4Rank);
        public static readonly PropertyPath MetaCP5RankProperty = GetPropertyPath(p => p.MetaCP5Rank);
        public static readonly PropertyPath MetaCP6RankProperty = GetPropertyPath(p => p.MetaCP6Rank);
        public static readonly PropertyPath MetaCP7RankProperty = GetPropertyPath(p => p.MetaCP7Rank);
        public static readonly PropertyPath MetaCP8RankProperty = GetPropertyPath(p => p.MetaCP8Rank);
        public static readonly PropertyPath MetaCP9RankProperty = GetPropertyPath(p => p.MetaCP9Rank);
        public static readonly PropertyPath MetaCP10RankProperty = GetPropertyPath(p => p.MetaCP10Rank);

        #endregion

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets whether changes should be logged as undoable actions.
        /// Note - this requires that a plan was specified on create.
        /// </summary>
        public Boolean IsLoggingUndoableActions
        {
            get { return _plan != null && _isLoggingUndoableActions; }
            set { _isLoggingUndoableActions = value; }
        }

        public IEnumerable<PlanogramPerformanceData> Items
        {
            get
            {
                return _items;
            }
        }

        public Int32 Count
        {
            get { return Items.Count(); }
        }

        #region Performance Properties

        #region P1

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "P1Property")]
        public Single? P1
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.P1Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.P1Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility P1Visibility
        {
            get { return GetMetricVisibility(1); }
        }

        public String P1Name
        {
            get { return GetMetricName(1); }
        }
        #endregion

        #region P2
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "P2Property")]
        public Single? P2
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.P2Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.P2Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility P2Visibility
        {
            get { return GetMetricVisibility(2); }
        }

        public String P2Name
        {
            get { return GetMetricName(2); }
        }
        #endregion

        #region P3
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "P3Property")]
        public Single? P3
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.P3Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.P3Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility P3Visibility
        {
            get { return GetMetricVisibility(3); }
        }

        public String P3Name
        {
            get { return GetMetricName(3); }
        }
        #endregion

        #region P4
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "P4Property")]
        public Single? P4
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.P4Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.P4Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility P4Visibility
        {
            get { return GetMetricVisibility(4); }
        }

        public String P4Name
        {
            get { return GetMetricName(4); }
        }
        #endregion

        #region P5
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "P5Property")]
        public Single? P5
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.P5Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.P5Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility P5Visibility
        {
            get { return GetMetricVisibility(5); }
        }

        public String P5Name
        {
            get { return GetMetricName(5); }
        }
        #endregion

        #region P6
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "P6Property")]
        public Single? P6
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.P6Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.P6Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility P6Visibility
        {
            get { return GetMetricVisibility(6); }
        }

        public String P6Name
        {
            get { return GetMetricName(6); }
        }
        #endregion

        #region P7
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "P7Property")]
        public Single? P7
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.P7Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.P7Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility P7Visibility
        {
            get { return GetMetricVisibility(7); }
        }

        public String P7Name
        {
            get { return GetMetricName(7); }
        }
        #endregion

        #region P8
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "P8Property")]
        public Single? P8
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.P8Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.P8Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility P8Visibility
        {
            get { return GetMetricVisibility(8); }
        }

        public String P8Name
        {
            get { return GetMetricName(8); }
        }
        #endregion

        #region P9
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "P9Property")]
        public Single? P9
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.P9Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.P9Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility P9Visibility
        {
            get { return GetMetricVisibility(9); }
        }

        public String P9Name
        {
            get { return GetMetricName(9); }
        }
        #endregion

        #region P10
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "P10Property")]
        public Single? P10
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.P10Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.P10Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility P10Visibility
        {
            get { return GetMetricVisibility(10); }
        }

        public String P10Name
        {
            get { return GetMetricName(10); }
        }
        #endregion

        #region P11
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "P11Property")]
        public Single? P11
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.P11Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.P11Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility P11Visibility
        {
            get { return GetMetricVisibility(11); }
        }

        public String P11Name
        {
            get { return GetMetricName(11); }
        }
        #endregion

        #region P12
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "P12Property")]
        public Single? P12
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.P12Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.P12Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility P12Visibility
        {
            get { return GetMetricVisibility(12); }
        }

        public String P12Name
        {
            get { return GetMetricName(12); }
        }
        #endregion

        #region P13
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "P13Property")]
        public Single? P13
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.P13Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.P13Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility P13Visibility
        {
            get { return GetMetricVisibility(13); }
        }

        public String P13Name
        {
            get { return GetMetricName(13); }
        }
        #endregion

        #region P14
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "P14Property")]
        public Single? P14
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.P14Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.P14Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility P14Visibility
        {
            get { return GetMetricVisibility(14); }
        }

        public String P14Name
        {
            get { return GetMetricName(14); }
        }
        #endregion

        #region P15
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "P15Property")]
        public Single? P15
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.P15Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.P15Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility P15Visibility
        {
            get { return GetMetricVisibility(15); }
        }

        public String P15Name
        {
            get { return GetMetricName(15); }
        }
        #endregion

        #region P16
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "P16Property")]
        public Single? P16
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.P16Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.P16Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility P16Visibility
        {
            get { return GetMetricVisibility(16); }
        }

        public String P16Name
        {
            get { return GetMetricName(16); }
        }
        #endregion

        #region P17
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "P17Property")]
        public Single? P17
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.P17Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.P17Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility P17Visibility
        {
            get { return GetMetricVisibility(17); }
        }

        public String P17Name
        {
            get { return GetMetricName(17); }
        }
        #endregion

        #region P18
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "P18Property")]
        public Single? P18
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.P18Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.P18Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility P18Visibility
        {
            get { return GetMetricVisibility(18); }
        }

        public String P18Name
        {
            get { return GetMetricName(18); }
        }
        #endregion

        #region P19
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "P19Property")]
        public Single? P19
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.P19Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.P19Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility P19Visibility
        {
            get { return GetMetricVisibility(19); }
        }

        public String P19Name
        {
            get { return GetMetricName(19); }
        }
        #endregion

        #region P20
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "P20Property")]
        public Single? P20
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.P20Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.P20Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility P20Visibility
        {
            get { return GetMetricVisibility(20); }
        }

        public String P20Name
        {
            get { return GetMetricName(20); }
        }
        #endregion

        #region CP1
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "CP1Property")]
        public Single? CP1
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.CP1Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.CP1Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility CP1Visibility
        {
            get { return GetMetricVisibility(21); }
        }

        public String CP1Name
        {
            get { return GetMetricName(21); }
        }
        #endregion

        #region CP2
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "CP2Property")]
        public Single? CP2
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.CP2Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.CP2Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility CP2Visibility
        {
            get { return GetMetricVisibility(22); }
        }

        public String CP2Name
        {
            get { return GetMetricName(22); }
        }
        #endregion

        #region CP3
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "CP3Property")]
        public Single? CP3
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.CP3Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.CP3Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility CP3Visibility
        {
            get { return GetMetricVisibility(23); }
        }

        public String CP3Name
        {
            get { return GetMetricName(23); }
        }
        #endregion

        #region CP4
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "CP4Property")]
        public Single? CP4
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.CP4Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.CP4Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility CP4Visibility
        {
            get { return GetMetricVisibility(24); }
        }

        public String CP4Name
        {
            get { return GetMetricName(24); }
        }
        #endregion

        #region CP5
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "CP5Property")]
        public Single? CP5
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.CP5Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.CP5Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility CP5Visibility
        {
            get { return GetMetricVisibility(25); }
        }

        public String CP5Name
        {
            get { return GetMetricName(25); }
        }
        #endregion

        #region CP6
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "CP6Property")]
        public Single? CP6
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.CP6Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.CP6Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility CP6Visibility
        {
            get { return GetMetricVisibility(26); }
        }

        public String CP6Name
        {
            get { return GetMetricName(26); }
        }
        #endregion

        #region CP7
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "CP7Property")]
        public Single? CP7
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.CP7Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.CP7Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility CP7Visibility
        {
            get { return GetMetricVisibility(27); }
        }

        public String CP7Name
        {
            get { return GetMetricName(27); }
        }
        #endregion

        #region CP8
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "CP8Property")]
        public Single? CP8
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.CP8Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.CP8Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility CP8Visibility
        {
            get { return GetMetricVisibility(28); }
        }

        public String CP8Name
        {
            get { return GetMetricName(28); }
        }
        #endregion

        #region CP9
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "CP9Property")]
        public Single? CP9
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.CP9Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.CP9Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility CP9Visibility
        {
            get { return GetMetricVisibility(29); }
        }

        public String CP9Name
        {
            get { return GetMetricName(29); }
        }
        #endregion

        #region CP10
        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "CP10Property")]
        public Single? CP10
        {
            get
            {
                return RoundValue(GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.CP10Property.Name));
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.CP10Property.Name, RoundValue(value));
                }
            }
        }

        public Visibility CP10Visibility
        {
            get { return GetMetricVisibility(30); }
        }

        public String CP10Name
        {
            get { return GetMetricName(30); }
        }
        #endregion

        #region UnitsSoldPerDay

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "UnitsSoldPerDayProperty")]
        public Single? UnitsSoldPerDay
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.UnitsSoldPerDayProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.UnitsSoldPerDayProperty.Name, value);
                }
            }
        }

        #endregion

        #region AchievedCasePacks

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "AchievedCasePacksProperty")]
        public Single? AchievedCasePacks
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.AchievedCasePacksProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.AchievedCasePacksProperty.Name, value);
                }
            }
        }

        #endregion

        #region AchievedDos

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "AchievedDosProperty")]
        public Single? AchievedDos
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.AchievedDosProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.AchievedDosProperty.Name, value);
                }
            }
        }

        #endregion

        #region AchievedShelfLife

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "AchievedShelfLifeProperty")]
        public Single? AchievedShelfLife
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.AchievedShelfLifeProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.AchievedShelfLifeProperty.Name, value);
                }
            }
        }

        #endregion

        #region AchievedDeliveries

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "AchievedDeliveriesProperty")]
        public Single? AchievedDeliveries
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.AchievedDeliveriesProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.AchievedDeliveriesProperty.Name, value);
                }
            }
        }

        #endregion

        #region MinimumInventoryUnits

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MinimumInventoryUnitsProperty")]
        public Single? MinimumInventoryUnits
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MinimumInventoryUnitsProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramPerformanceData.MinimumInventoryUnitsProperty.Name, value);
                }
            }
        }

        #endregion

        #endregion

        #region Meta Data Properties

        #region MetaP1Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP1PercentageProperty")]
        public Single? MetaP1Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaP1PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaP2Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP2PercentageProperty")]
        public Single? MetaP2Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaP2PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaP3Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP3PercentageProperty")]
        public Single? MetaP3Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaP3PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaP4Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP4PercentageProperty")]
        public Single? MetaP4Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaP4PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaP5Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP5PercentageProperty")]
        public Single? MetaP5Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaP5PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaP6Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP6PercentageProperty")]
        public Single? MetaP6Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaP6PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaP7Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP7PercentageProperty")]
        public Single? MetaP7Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaP7PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaP8Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP8PercentageProperty")]
        public Single? MetaP8Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaP8PercentageProperty.Name);
            }
        }
        #endregion

        #region MetaP9Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP9PercentageProperty")]
        public Single? MetaP9Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaP9PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaP10Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP10PercentageProperty")]
        public Single? MetaP10Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaP10PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaP11Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP11PercentageProperty")]
        public Single? MetaP11Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaP11PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaP12Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP12PercentageProperty")]
        public Single? MetaP12Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaP12PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaP13Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP13PercentageProperty")]
        public Single? MetaP13Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaP8PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaP14Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP14PercentageProperty")]
        public Single? MetaP14Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaP14PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaP15Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP15PercentageProperty")]
        public Single? MetaP15Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaP15PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaP16Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP16PercentageProperty")]
        public Single? MetaP16Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaP16PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaP17Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP17PercentageProperty")]
        public Single? MetaP17Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaP17PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaP18Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP18PercentageProperty")]
        public Single? MetaP18Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaP18PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaP19Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP19PercentageProperty")]
        public Single? MetaP19Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaP19PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaP20Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP20PercentageProperty")]
        public Single? MetaP20Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaP20PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaCP1Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaCP1PercentageProperty")]
        public Single? MetaCP1Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaCP1PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaCP2Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaCP2PercentageProperty")]
        public Single? MetaCP2Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaCP2PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaCP3Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaCP3PercentageProperty")]
        public Single? MetaCP3Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaCP3PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaCP4Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaCP4PercentageProperty")]
        public Single? MetaCP4Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaCP4PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaCP5Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaCP5PercentageProperty")]
        public Single? MetaCP5Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaCP5PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaCP6Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaCP6PercentageProperty")]
        public Single? MetaCP6Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaCP6PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaCP7Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaCP7PercentageProperty")]
        public Single? MetaCP7Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaCP7PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaCP8Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaCP8PercentageProperty")]
        public Single? MetaCP8Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaCP8PercentageProperty.Name);
            }
        }
        #endregion

        #region MetaCP9Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaCP9PercentageProperty")]
        public Single? MetaCP9Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaCP9PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaCP10Percentage

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaCP10PercentageProperty")]
        public Single? MetaCP10Percentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPerformanceData.MetaCP10PercentageProperty.Name);
            }
        }

        #endregion

        #region MetaP1Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP1RankProperty")]
        public Int32? MetaP1Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaP1RankProperty.Name);
            }
        }

        #endregion

        #region MetaP2Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP2RankProperty")]
        public Int32? MetaP2Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaP2RankProperty.Name);
            }
        }

        #endregion

        #region MetaP3Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP3RankProperty")]
        public Int32? MetaP3Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaP3RankProperty.Name);
            }
        }

        #endregion

        #region MetaP4Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP4RankProperty")]
        public Int32? MetaP4Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaP4RankProperty.Name);
            }
        }

        #endregion

        #region MetaP5Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP5RankProperty")]
        public Int32? MetaP5Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaP5RankProperty.Name);
            }
        }

        #endregion

        #region MetaP6Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP6RankProperty")]
        public Int32? MetaP6Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaP6RankProperty.Name);
            }
        }

        #endregion

        #region MetaP7Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP7RankProperty")]
        public Int32? MetaP7Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaP7RankProperty.Name);
            }
        }

        #endregion

        #region MetaP8Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP8RankProperty")]
        public Int32? MetaP8Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaP8RankProperty.Name);
            }
        }

        #endregion

        #region MetaP9Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP9RankProperty")]
        public Int32? MetaP9Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaP9RankProperty.Name);
            }
        }

        #endregion

        #region MetaP10Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP10RankProperty")]
        public Int32? MetaP10Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaP10RankProperty.Name);
            }
        }

        #endregion

        #region MetaP11Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP11RankProperty")]
        public Int32? MetaP11Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaP11RankProperty.Name);
            }
        }

        #endregion

        #region MetaP12Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP12RankProperty")]
        public Int32? MetaP12Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaP12RankProperty.Name);
            }
        }

        #endregion

        #region MetaP13Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP13RankProperty")]
        public Int32? MetaP13Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaP13RankProperty.Name);
            }
        }

        #endregion

        #region MetaP14Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP14RankProperty")]
        public Int32? MetaP14Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaP14RankProperty.Name);
            }
        }

        #endregion

        #region MetaP15Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP15RankProperty")]
        public Int32? MetaP15Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaP15RankProperty.Name);
            }
        }

        #endregion

        #region MetaP16Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP16RankProperty")]
        public Int32? MetaP16Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaP16RankProperty.Name);
            }
        }

        #endregion

        #region MetaP17Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP17RankProperty")]
        public Int32? MetaP17Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaP17RankProperty.Name);
            }
        }

        #endregion

        #region MetaP18Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP18RankProperty")]
        public Int32? MetaP18Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaP18RankProperty.Name);
            }
        }

        #endregion

        #region MetaP19Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP19RankProperty")]
        public Int32? MetaP19Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaP19RankProperty.Name);
            }
        }

        #endregion

        #region MetaP20Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaP20RankProperty")]
        public Int32? MetaP20Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaP20RankProperty.Name);
            }
        }

        #endregion

        #region MetaCP1Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaCP1RankProperty")]
        public Int32? MetaCP1Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaCP1RankProperty.Name);
            }
        }

        #endregion

        #region MetaCP2Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaCP2RankProperty")]
        public Int32? MetaCP2Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaCP2RankProperty.Name);
            }
        }

        #endregion

        #region MetaCP3Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaCP3RankProperty")]
        public Int32? MetaCP3Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaCP3RankProperty.Name);
            }
        }

        #endregion

        #region MetaCP4Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaCP4RankProperty")]
        public Int32? MetaCP4Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaCP4RankProperty.Name);
            }
        }

        #endregion

        #region MetaCP5Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaCP5RankProperty")]
        public Int32? MetaCP5Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaCP5RankProperty.Name);
            }
        }

        #endregion

        #region MetaCP6Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaCP6RankProperty")]
        public Int32? MetaCP6Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaCP6RankProperty.Name);
            }
        }

        #endregion

        #region MetaCP7Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaCP7RankProperty")]
        public Int32? MetaCP7Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaCP7RankProperty.Name);
            }
        }

        #endregion

        #region MetaCP8Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaCP8RankProperty")]
        public Int32? MetaCP8Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaCP8RankProperty.Name);
            }
        }

        #endregion

        #region MetaCP9Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaCP9RankProperty")]
        public Int32? MetaCP9Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaCP9RankProperty.Name);
            }
        }

        #endregion

        #region MetaCP10Rank

        [ModelObjectDisplayName(typeof(PlanogramPerformanceData), "MetaCP10RankProperty")]
        public Int32? MetaCP10Rank
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPerformanceData.MetaCP10RankProperty.Name);
            }
        }

        #endregion

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="items"></param>
        public PlanogramPerformanceDataMultiView(IEnumerable<PlanogramPerformanceData> items, PlanogramView planView)
        {
            _items = items.ToList();
            _plan = planView;
            foreach (var i in items)
            {
                i.PropertyChanged += PlanItem_PropertyChanged;
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on any linked plan item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(e.PropertyName);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the common value of the given property for all the items
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        private static T GetCommonPropertyValue<T>(IEnumerable<Object> items, String propertyName)
        {
            return PlanItemHelper.GetCommonPropertyValue<T>(items, propertyName);
        }

        /// <summary>
        /// Sets the given value against
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        private void SetPropertyValue<T>(IEnumerable<Object> items, String propertyName, T value)
        {
            PlanItemHelper.SetPropertyValue<T>(items, propertyName, value, IsLoggingUndoableActions, _plan);
        }

        /// <summary>
        /// Returns the name of the given metric.
        /// </summary>
        /// <param name="metricId"></param>
        /// <returns></returns>
        private String GetMetricName(Int32 metricId)
        {
            if (!Items.Any())
            {
                return String.Empty;
            }
            else
            {
                return Items.First().GetMetricName(metricId);
            }
        }

        /// <summary>
        /// Returns the visibility of the given metric id.
        /// </summary>
        /// <param name="metricId"></param>
        /// <returns></returns>
        private Visibility GetMetricVisibility(Int32 metricId)
        {
            if (!Items.Any()) return Visibility.Collapsed;

            if (Items.First().IsMetricInUse(metricId))
            {

                return Visibility.Visible;
            }
            else
            {
                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Convert value to correct format
        /// </summary>
        /// <param name="metricId"></param>
        /// <returns></returns>
        private Single? RoundValue(Single? value)
        {
            if (value != null)
            {
                return (Single?)Math.Round((double)value.Value, 2, MidpointRounding.AwayFromZero);
            }
            return null;
        }

        #endregion

        #region IDataErrorInfo Members

        public String Error
        {
            get { throw new NotImplementedException(); }
        }

        public String this[String columnName]
        {
            get
            {
                return ((IDataErrorInfo)_items.First())[columnName];
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                foreach (var i in _items)
                {
                    i.PropertyChanged -= PlanItem_PropertyChanged;
                }

                base.IsDisposed = true;
            }
        }

        /// <summary>
        /// Finalizer, just in case
        /// </summary>
        ~PlanogramPerformanceDataMultiView()
        {
            Dispose(false);
        }

        #endregion

        #region Static Helpers

        private static PropertyPath GetPropertyPath(System.Linq.Expressions.Expression<Func<PlanogramPerformanceDataMultiView, Object>> expression)
        {
            return WpfHelper.GetPropertyPath<PlanogramPerformanceDataMultiView>(expression);
        }

        #endregion
    }
}
