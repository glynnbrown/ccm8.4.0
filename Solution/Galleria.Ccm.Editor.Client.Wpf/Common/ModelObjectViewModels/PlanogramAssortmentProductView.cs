﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.3)
//V8-32718 : A.Probyn
// Created
// V8-32718 : D.Pleasance
//  Added overload constructor to allow planItems to be passed through to enable displaying of product / position attributes.
#endregion

#endregion

using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using System;
using System.Windows;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// A view of a planogram assortment product.
    /// </summary>
    public sealed class PlanogramAssortmentProductView
    {
        #region Fields
        private PlanogramProduct _planogramProduct;
        private PlanogramAssortmentProduct _product;
        private IPlanItem _planItem;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath ProductProperty = WpfHelper.GetPropertyPath<PlanogramAssortmentProductView>(p => p.Product);
        public static readonly PropertyPath PlanogramProductProperty = WpfHelper.GetPropertyPath<PlanogramAssortmentProductView>(p => p.PlanogramProduct);
        public static readonly PropertyPath ProductGtinProperty = WpfHelper.GetPropertyPath<PlanogramAssortmentProductView>(p => p.ProductGtin);
        public static readonly PropertyPath ProductNameProperty = WpfHelper.GetPropertyPath<PlanogramAssortmentProductView>(p => p.ProductName);
        
        #endregion

        #region Properties

        /// <summary>
        /// Gets the product this row relates to
        /// </summary>
        public PlanogramAssortmentProduct Product
        {
            get { return _product; }
        }

        /// <summary>
        /// Returns the products matching planogram product
        /// </summary>
        public PlanogramProduct PlanogramProduct
        {
            get
            {
                if (_planogramProduct == null)
                {
                    _planogramProduct = _product.GetPlanogramProduct();
                }
                return _planogramProduct;
            }
        }

        public IPlanItem PlanItem
        {
            get
            {
                return _planItem;
            }
        }

        /// <summary>
        /// Gets the id of the product this row relates to
        /// </summary>
        public Object Id
        {
            get { return _product.Id; }
        }

        /// <summary>
        /// Gets the Gtin of the product this row relates to
        /// </summary>
        public String ProductGtin
        {
            get { return  _product.Gtin; }
        }

        /// <summary>
        /// Gets the Name of the product this row relates to
        /// </summary>
        public String ProductName
        {
            get { return _product.Name; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="parentPlanogramView">the parent planogram view</param>
        /// <param name="product">the planogram product model</param>
        public PlanogramAssortmentProductView(PlanogramAssortmentProduct product)
        {
            _product = product;
        }

        public PlanogramAssortmentProductView(PlanogramAssortmentProduct product, IPlanItem planItem)
        {
            _product = product;
            _planItem = planItem;
        }

        #endregion
    }
}