﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
#endregion
#endregion

using Csla.Core;
using Galleria.Framework.Planograms.ViewModel;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    //TODO: Swap over to ViewModelBase instead.

    /// <summary>
    /// An extended version of the ViewModelObject base which provides advanced event capabilities.
    /// </summary>
    public abstract class ExtendedViewModelObject : ViewModelObject,
        INotifyPropertyChanging, INotifyViewModelChildChanging, INotifyChildChanged, IUpdateable
    {
        #region Fields
        private List<Object> _hookedChildObjects = new List<Object>();
        private Boolean _isUpdating; //flag to indicate that this is updating.
        private Int32 _updateLevelCount = 0; //the update level count.
        private ExtendedViewModelObject _parent;
        #endregion

        #region Properties

        /// <summary>
        /// Returns the parent object that this is currently hooked into.
        /// </summary>
        protected ExtendedViewModelObject Parent
        {
            get { return _parent; }
            private set { _parent = value; }
        }

        #endregion

        #region INotifyPropertyChanging

        public event PropertyChangingEventHandler PropertyChanging;

        private void RaisePropertyChanging(PropertyChangingEventArgs e)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, e);
            }
        }

        protected virtual void OnPropertyChanging(PropertyChangingEventArgs e)
        {
            RaisePropertyChanging(e);
        }

        protected void OnPropertyChanging(String propertyName)
        {
            OnPropertyChanging(new PropertyChangingEventArgs(propertyName));
        }

        protected void OnPropertyChanging(PropertyPath property)
        {
            OnPropertyChanging(new PropertyChangingEventArgs(property.Path));
        }

        #endregion

        #region INotifyChildChanged

        private event EventHandler<ChildChangedEventArgs> _childChangedHandlers;

        /// <summary>
        /// Raised whenever a child is changed.
        /// </summary>
        public event EventHandler<ChildChangedEventArgs> ChildChanged
        {
            add
            {
                _childChangedHandlers = (EventHandler<ChildChangedEventArgs>)
                  System.Delegate.Combine(_childChangedHandlers, value);
            }
            remove
            {
                _childChangedHandlers = (EventHandler<ChildChangedEventArgs>)
                  System.Delegate.Remove(_childChangedHandlers, value);
            }
        }

        private void RaiseChildChanged(ChildChangedEventArgs args)
        {
            if (_childChangedHandlers != null)
            {
                _childChangedHandlers.Invoke(this, args);
            }
        }

        protected virtual void OnChildChanged(ChildChangedEventArgs e)
        {
            RaiseChildChanged(e);
        }

        private void OnChildChanged(Object child, PropertyChangedEventArgs propertyChangedArgs)
        {
            ChildChangedEventArgs args = new ChildChangedEventArgs(child, propertyChangedArgs);
            OnChildChanged(args);
        }

        private void OnChildChanged(Object child, NotifyCollectionChangedEventArgs collectionChangedArgs)
        {
            ChildChangedEventArgs args = new ChildChangedEventArgs(child, null, collectionChangedArgs);
            OnChildChanged(args);
        }

        #endregion

        #region INotifyChildChanging

        private event EventHandler<ViewModelChildChangingEventArgs> _childChangingHandlers;

        /// <summary>
        /// Raised whenever a child is about to change.
        /// </summary>
        public event EventHandler<ViewModelChildChangingEventArgs> ChildChanging
        {
            add
            {
                _childChangingHandlers = (EventHandler<ViewModelChildChangingEventArgs>)
                  System.Delegate.Combine(_childChangingHandlers, value);
            }
            remove
            {
                _childChangingHandlers = (EventHandler<ViewModelChildChangingEventArgs>)
                  System.Delegate.Remove(_childChangingHandlers, value);
            }
        }

        private void RaiseChildChanging(ViewModelChildChangingEventArgs args)
        {
            if (_childChangingHandlers != null)
            {
                _childChangingHandlers.Invoke(this, args);
            }
        }

        protected virtual void OnChildChanging(ViewModelChildChangingEventArgs e)
        {
            RaiseChildChanging(e);
        }

        private void OnChildChanging(Object child, PropertyChangingEventArgs propertyChangingArgs)
        {
            ViewModelChildChangingEventArgs args = new ViewModelChildChangingEventArgs(child, propertyChangingArgs);
            OnChildChanging(args);
        }

        #endregion

        #region IUpdatable

        /// <summary>
        /// Returns true if this view is currently updating.
        /// </summary>
        public Boolean IsUpdating
        {
            get { return _isUpdating; }
            private set
            {
                if (_isUpdating != value)
                {
                    _isUpdating = value;
                    OnPropertyChanged("IsUpdating");
                }
            }
        }

        /// <summary>
        /// Notifies this object that multiple properties are about to be updated
        /// UI should not be updated until EndUpdate has been called.
        /// </summary>
        /// <returns>true if the call started a new update, or false if
        /// this was already updating</returns>
        public virtual void BeginUpdate()
        {
            //increase the update level count.
            _updateLevelCount++;

            //flag this viewmodel as in the process of updating.
            IsUpdating = true;

            //ripple down.
            foreach (Object child in _hookedChildObjects)
            {
                IUpdateable updateableChild = child as IUpdateable;
                if (updateableChild != null)
                {
                    updateableChild.BeginUpdate();
                }
            }
        }

        /// <summary>
        /// Ends the ongoing update state.
        /// </summary>
        public virtual void EndUpdate()
        {
            if (IsUpdating)
            {
                //ripple down.
                foreach (Object child in _hookedChildObjects)
                {
                    IUpdateable updateableChild = child as IUpdateable;
                    if (updateableChild != null)
                    {
                        updateableChild.EndUpdate();
                    }
                }

                //decrease the update level count.
                _updateLevelCount--;

                if (_updateLevelCount == 0)
                {
                    IsUpdating = false;
                }
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on a child object.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Child_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(e.PropertyName))
            {
                OnChildChanged(sender, e);
            }
        }

        /// <summary>
        /// Called whenever a child collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Child_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            OnChildChanged(this/*sender*/, e);
        }

        /// <summary>
        /// Called whenever a child raises an INotifyChildChanged event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Child_ChildChanged(object sender, ChildChangedEventArgs e)
        {
            OnChildChanged(e);
        }

        /// <summary>
        /// Called whenever a child raises an INotifyChildChanging event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Child_ChildChanging(object sender, ViewModelChildChangingEventArgs e)
        {
            OnChildChanging(e);
        }

        /// <summary>
        /// Called whenever a property is about to change on a child object
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Child_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            if (!String.IsNullOrEmpty(e.PropertyName))
            {
                OnChildChanging(sender, e);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds event hooks to the given child
        /// </summary>
        /// <param name="child"></param>
        protected void AddChildEventHooks(Object child)
        {
            if (!_hookedChildObjects.Contains(child))
            {
                ExtendedViewModelObject childExtendedViewModelObject = child as ExtendedViewModelObject;
                if (childExtendedViewModelObject != null)
                {
                    childExtendedViewModelObject.Parent = this;
                }

                INotifyPropertyChanged propertyChangedChild = child as INotifyPropertyChanged;
                if (propertyChangedChild != null)
                {
                    propertyChangedChild.PropertyChanged += Child_PropertyChanged;
                }

                INotifyCollectionChanged collectionChangedChild = child as INotifyCollectionChanged;
                if (collectionChangedChild != null)
                {
                    collectionChangedChild.CollectionChanged += Child_CollectionChanged;
                }

                INotifyChildChanged childChangeNotifyChild = child as INotifyChildChanged;
                if (childChangeNotifyChild != null)
                {
                    childChangeNotifyChild.ChildChanged += Child_ChildChanged;
                }

                INotifyPropertyChanging propertyChangingChild = child as INotifyPropertyChanging;
                if (propertyChangingChild != null)
                {
                    propertyChangingChild.PropertyChanging += Child_PropertyChanging;
                }

                INotifyViewModelChildChanging childChangingNotifyChild = child as INotifyViewModelChildChanging;
                if (childChangingNotifyChild != null)
                {
                    childChangingNotifyChild.ChildChanging += Child_ChildChanging;
                }

                //set the child to updating state if required.
                if (IsUpdating)
                {
                    IUpdateable updateableChild = child as IUpdateable;
                    if (updateableChild != null)
                    {
                        updateableChild.BeginUpdate();
                    }
                }


                _hookedChildObjects.Add(child);
            }
        }

        /// <summary>
        /// Removes event hooks from the given child.
        /// </summary>
        /// <param name="child"></param>
        protected void RemoveChildEventHooks(Object child, Boolean disposeChild = true)
        {
            ExtendedViewModelObject childExtendedViewModelObject = child as ExtendedViewModelObject;
            if (childExtendedViewModelObject != null)
            {
                childExtendedViewModelObject.Parent = null;
            }

            INotifyPropertyChanged propertyChangedChild = child as INotifyPropertyChanged;
            if (propertyChangedChild != null)
            {
                propertyChangedChild.PropertyChanged -= Child_PropertyChanged;
            }

            INotifyCollectionChanged collectionChangedChild = child as INotifyCollectionChanged;
            if (collectionChangedChild != null)
            {
                collectionChangedChild.CollectionChanged -= Child_CollectionChanged;
            }

            INotifyChildChanged childChangeNotifyChild = child as INotifyChildChanged;
            if (childChangeNotifyChild != null)
            {
                childChangeNotifyChild.ChildChanged -= Child_ChildChanged;
            }

            INotifyPropertyChanging propertyChangingChild = child as INotifyPropertyChanging;
            if (propertyChangingChild != null)
            {
                propertyChangingChild.PropertyChanging -= Child_PropertyChanging;
            }

            INotifyViewModelChildChanging childChangingNotifyChild = child as INotifyViewModelChildChanging;
            if (childChangingNotifyChild != null)
            {
                childChangingNotifyChild.ChildChanging -= Child_ChildChanging;
            }

            IUpdateable updateableChild = child as IUpdateable;
            if (updateableChild != null && updateableChild.IsUpdating)
            {
                updateableChild.EndUpdate();
            }

            if (disposeChild)
            {
                IDisposable disposableChild = child as IDisposable;
                if (disposableChild != null)
                {
                    disposableChild.Dispose();
                }
            }

            _hookedChildObjects.Remove(child);
        }

        /// <summary>
        /// Removes hooks from all child objects of a specific type.
        /// </summary>
        protected void RemoveAllChildEventHooks(Type childType, Boolean disposeChildren = true)
        {
            foreach (Object child in _hookedChildObjects.ToList())
            {
                if (child.GetType() == childType)
                {
                    RemoveChildEventHooks(child, disposeChildren);
                }
            }
        }

        /// <summary>
        /// Removes hooks from all child objects.
        /// </summary>
        protected void RemoveAllChildEventHooks()
        {
            foreach (Object child in _hookedChildObjects.ToList())
            {
                RemoveChildEventHooks(child);
            }
        }

        #endregion

    }

}
