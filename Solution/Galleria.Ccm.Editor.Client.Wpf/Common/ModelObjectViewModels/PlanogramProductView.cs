﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
// V8-24124 : A.Silva ~ Changed the explicit implementation of IPlanItem to an implicit one.
//                    ~ Added support for drag and drop operations.
// V8-26337 : A.Kuszyk
//  Added Performance Data properties.
// V8-26499 : A.Kuszyk
//  Moved Performance properties into base class.
//V8-26787 : L.Ineson
//  Changes to planitemfield
// V8-26377 : L.Ineson
//  Added custom attribute and performance properties to plan item fields enumeratror.
// V8-25922 : L.Ineson
//  Removed PlanogramProductDropEventArgs
#endregion

#region Version History: CCM801

// V8-28676 : A.Silva
//      Added EnumerateImageIds, EnumerateExternalImageFetchActions, ClearImages and StoreExternalImages.
// V8-28887 : A.Silva
//      Amended FetchExternalImage() so that it stores a copy of the cached image and not the image itself.
// V8-27164 : A.Silva
//      Amended FetchExternalImage() so that it correctly creates callbacks when needed.

#endregion

#region Version History: CCM802

// V8-29039 \ V8-29018 : D.Pleasance
//      Amended StoreExternalImages so that realImageProvider is Paused during the task, rather than stopped as previous, 
//      also checks if the provider was running, if so restarts if required.
#endregion

#region Version History: CCM820
// V8-31164 : D.Pleasance
//      Removed BlockingColour \ SequenceNumber from Planogram Product
#endregion

#region Version History: (CCM 8.3)
//V8-31832 : L.Ineson
// Implemented ICalculatedValueResolver
#endregion

#endregion

using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// A view of a planogram product.
    /// </summary>
    public sealed class PlanogramProductView : PlanogramProductViewModelBase, IPlanItem
    {
        #region Fields
        private CalculatedValueResolver _calculatedValueResolver;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath IsLoadingImagesProperty = WpfHelper.GetPropertyPath<PlanogramProductView>(p => p.IsLoadingImages);
        
        #endregion

        #region Proeprties

        /// <summary>
        /// Returns the resolver used for binding to calculated values.
        /// </summary>
        public CalculatedValueResolver CalculatedValueResolver
        {
            get
            {
                if (_calculatedValueResolver == null)
                {
                    _calculatedValueResolver =
                        new CalculatedValueResolver(GetCalculatedValue, /*isCachingEnabled*/true);
                }
                return _calculatedValueResolver;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="parentPlanogramView">the parent planogram view</param>
        /// <param name="product">the planogram product model</param>
        public PlanogramProductView(PlanogramView parentPlanogramView, PlanogramProduct product)
            : base(product,parentPlanogramView)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Called by the parent planogram to notify that processing has completed
        /// </summary>
        internal void NotifyPlanProcessCompleting()
        {
            //if we have calculated values cached then update them now.
            if (_calculatedValueResolver != null) _calculatedValueResolver.RefreshValues();
        }


        /// <summary>
        /// Returns the list of default field infos for this item.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDefaultPlanItemFields()
        {
            String[] defaults = new String[]
            {
                PlanogramProduct.GtinProperty.Name,
                PlanogramProduct.NameProperty.Name,
                PlanogramProduct.BrandProperty.Name,
                PlanogramProduct.HeightProperty.Name,
                PlanogramProduct.WidthProperty.Name,
                PlanogramProduct.DepthProperty.Name,
            };

            foreach (var field in EnumerateDisplayableFields())
            {
                if (defaults.Contains(field.PropertyName))
                {
                    yield return field;
                }
            }
        }

        /// <summary>
        /// Resolves the given expression for this planogram product.
        /// </summary>
        public Object GetCalculatedValue(String expressionText)
        {
            PlanogramProduct product = this.Model;
            if (product == null) return null;

            Planogram plan = this.Model.Parent;
            if (plan == null) return null;

            return PlanogramFieldHelper.ResolveFieldExpression(expressionText,
                PlanogramItemPlacement.NewPlanogramItemPlacement(product: product, planogram: plan));
        }

        /// <summary>
        /// Enumerates through all position views related to this product.
        /// </summary>
        /// <returns>An enumerable of position views.</returns>
        public IEnumerable<PlanogramPositionView> EnumeratePositionViews()
        {
            if (this.Planogram == null) yield break;

            foreach(PlanogramPositionView p in this.Planogram.EnumerateAllPositions())
            {
                if (p.Product == this) yield return p;
            }
        }

        #endregion

        #region IPlanItem Members

        public PlanogramView Planogram
        {
            get { return base.ParentPlangoram as PlanogramView; }
        }

        PlanogramFixtureView IPlanItem.Fixture
        {
            get { return null; }
        }

        PlanogramAssemblyView IPlanItem.Assembly
        {
            get { return null; }
        }

        PlanogramComponentView IPlanItem.Component
        {
            get { return null; }
        }

        PlanogramSubComponentView IPlanItem.SubComponent
        {
            get { return null; }
        }

        PlanogramPositionView IPlanItem.Position
        {
            get { return null; }
        }

        public PlanogramProductView Product
        {
            get { return this; }
        }

        PlanogramAnnotationView IPlanItem.Annotation
        {
            get { return null; }
        }

        PlanItemType IPlanItem.PlanItemType
        {
            get { return PlanItemType.Product; }
        }

        public Boolean IsSelectable { get; set; }


        public Single X
        {
            get { return 0; }
            set { }
        }

        public Single Y
        {
            get { return 0; }
            set { }
        }

        public Single Z
        {
            get { return 0; }
            set { }
        }

        public Single Angle { get { return 0; } set { } }
        public Single Slope { get { return 0; } set { } }
        public Single Roll { get { return 0; } set { } }

        #endregion

        #region IDisposable

        protected override void OnDisposing(bool disposing)
        {
            if (_calculatedValueResolver != null) _calculatedValueResolver.Dispose();
            base.OnDisposing(disposing);
        }

        #endregion

        
    }
}
