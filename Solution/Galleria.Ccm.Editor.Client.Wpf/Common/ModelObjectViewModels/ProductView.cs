﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// A.Probyn
//      Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;

using Galleria.Ccm.Model;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;

using Galleria.Framework.Helpers;


namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Provides a view of a product - not currently used, no need but may be scope in the future
    /// </summary>
    //public sealed class ProductView : ExtendedViewModelObject
    //{
    //    #region Fields

    //    private readonly Product _model;

    //    private String _gtin;
    //    private String _name;

    //    private Single _height;
    //    private Single _width;
    //    private Single _depth;

    //    #endregion

    //    #region Binding Property Paths

    //    public static readonly PropertyPath ModelProperty = WpfHelper.GetPropertyPath<ProductView>(p => p.Model);
    //    public static readonly PropertyPath GtinProperty = WpfHelper.GetPropertyPath<ProductView>(p => p.Gtin);
    //    public static readonly PropertyPath NameProperty = WpfHelper.GetPropertyPath<ProductView>(p => p.Name);
    //    public static readonly PropertyPath HeightProperty = WpfHelper.GetPropertyPath<ProductView>(p => p.Height);
    //    public static readonly PropertyPath WidthProperty = WpfHelper.GetPropertyPath<ProductView>(p => p.Width);
    //    public static readonly PropertyPath DepthProperty = WpfHelper.GetPropertyPath<ProductView>(p => p.Depth);

    //    #endregion

    //    #region Properties

    //    /// <summary>
    //    /// Gets the product model object
    //    /// </summary>
    //    public Product Model
    //    {
    //        get { return _model; }
    //    }

    //    #region Model Properties

    //    public String Gtin
    //    {
    //        get { return _model.Gtin; }
    //        set
    //        {
    //            if (value != _model.Gtin)
    //            {
    //                _model.Gtin = value;
    //            }
    //        }
    //    }
        
    //    public String Name
    //    {
    //        get { return _model.Name; }
    //        set
    //        {
    //            if (value != _model.Name)
    //            {
    //                _model.Name = value;
    //            }
    //        }
    //    }

    //    public Single Height
    //    {
    //        get { return _model.Height; }
    //        set
    //        {
    //            if (value != _model.Height)
    //            {
    //                _model.Height = value;
    //            }
    //        }
    //    }

    //    public Single Width
    //    {
    //        get { return _model.Width; }
    //        set
    //        {
    //            if (value != _model.Width)
    //            {
    //                _model.Width = value;
    //            }
    //        }
    //    }

    //    public Single Depth
    //    {
    //        get { return _model.Depth; }
    //        set
    //        {
    //            if (value != _model.Depth)
    //            {
    //                _model.Depth = value;
    //            }
    //        }
    //    }

    //    #endregion

    //    #endregion

    //    #region Constructor

    //    /// <summary>
    //    /// Creates a new instance of this type.
    //    /// </summary>
    //    /// <param name="parentSubComponent"></param>
    //    /// <param name="position"></param>
    //    /// <param name="productView"></param>
    //    public ProductView(Product product)
    //    {
            
    //        _model = product;
    //        _model.PropertyChanging += Model_PropertyChanging;
    //        _model.PropertyChanged += Model_PropertyChanged;

    //    }

    //    #endregion

    //    #region Event Handlers

    //    /// <summary>
    //    /// Called whenever a property is changing on the base model objects.
    //    /// </summary>
    //    /// <param name="sender"></param>
    //    /// <param name="e"></param>
    //    private void Model_PropertyChanging(object sender, PropertyChangingEventArgs e)
    //    {
    //        if (typeof(ProductView).GetProperty(e.PropertyName) != null)
    //        {
    //            OnPropertyChanging(e.PropertyName);
    //        }
    //    }

    //    /// <summary>
    //    /// Called whenever a property changes on the base model objects.
    //    /// </summary>
    //    /// <param name="sender"></param>
    //    /// <param name="e"></param>
    //    private void Model_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    //    {
    //        if (typeof(ProductView).GetProperty(e.PropertyName) != null)
    //        {
    //            //relay out.
    //            OnPropertyChanged(e.PropertyName);
    //        }
    //    }

 
    //    #endregion

    //    #region Methods

    //    #endregion

    //    #region Static Helpers

    //    #endregion

    //    #region IDisposable Members

    //    protected override void Dispose(Boolean disposing)
    //    {
    //        if (!base.IsDisposed)
    //        {

    //            _model.PropertyChanging -= Model_PropertyChanging;
    //            _model.PropertyChanged -= Model_PropertyChanged;

    //            if (disposing)
    //            {

    //            }

    //            base.IsDisposed = true;
    //        }

    //    }

    //    #endregion

    //}

}
