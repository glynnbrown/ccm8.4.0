﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26149 : L.Ineson
//  Created
// CCM-26091 : L.Ineson
//  Added more face thickness properties.
// CCM-27929 : L.Ineson
//  Combine type now shows for bars too.
// V8-27529 : L.Ineson
//  Merchconsrtaint row values are now available for all hang types.
#endregion
#region Version History: CCM802
// V8-29023 : M.Pettit
//  Added Merchandisable Depth property, IsMerchandisingHeightPropertyVisible, IsMerchandisingDepthPropertyVisible
#endregion
#region Version History: CCM803
// V8-29594 : L.Ineson
//  Removed riser transparency.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.Attributes;
using Galleria.Framework.Controls.Wpf.PropertyEditor;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Presents a merged view of multiple PlanogramSubComponent views
    /// </summary>
    public sealed class PlanogramSubComponentMultiView : ViewModelObject, IDataErrorInfo
    {
        #region Fields

        private IEnumerable<PlanogramSubComponentView> _items;

        private PlanogramView _plan;
        private Boolean _isLoggingUndoableActions = true;

        private PlanogramComponentType? _componentType;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath MerchandisableHeightProperty = GetPropertyPath(p => p.MerchandisableHeight);
        public static readonly PropertyPath MerchandisableDepthProperty = GetPropertyPath(p => p.MerchandisableDepth);
        public static readonly PropertyPath RiserThicknessProperty = GetPropertyPath(p => p.RiserThickness);
        public static readonly PropertyPath RiserHeightProperty = GetPropertyPath(p => p.RiserHeight);
        public static readonly PropertyPath RiserColourProperty = GetPropertyPath(p => p.RiserColour);
        public static readonly PropertyPath IsRiserPlacedOnFrontProperty = GetPropertyPath(p => p.IsRiserPlacedOnFront);
        public static readonly PropertyPath IsRiserPlacedOnBackProperty = GetPropertyPath(p => p.IsRiserPlacedOnBack);
        public static readonly PropertyPath IsRiserPlacedOnLeftProperty = GetPropertyPath(p => p.IsRiserPlacedOnLeft);
        public static readonly PropertyPath IsRiserPlacedOnRightProperty = GetPropertyPath(p => p.IsRiserPlacedOnRight);
        public static readonly PropertyPath NotchStartXProperty = GetPropertyPath(p => p.NotchStartX);
        public static readonly PropertyPath NotchSpacingXProperty = GetPropertyPath(p => p.NotchSpacingX);
        public static readonly PropertyPath NotchStartYProperty = GetPropertyPath(p => p.NotchStartY);
        public static readonly PropertyPath NotchSpacingYProperty = GetPropertyPath(p => p.NotchSpacingY);
        public static readonly PropertyPath NotchHeightProperty = GetPropertyPath(p => p.NotchHeight);
        public static readonly PropertyPath NotchWidthProperty = GetPropertyPath(p => p.NotchWidth);
        public static readonly PropertyPath IsNotchPlacedOnFrontProperty = GetPropertyPath(p => p.IsNotchPlacedOnFront);
        public static readonly PropertyPath NotchStyleTypeProperty = GetPropertyPath(p => p.NotchStyleType);
        public static readonly PropertyPath FaceThicknessFrontProperty = GetPropertyPath(p => p.FaceThicknessFront);
        public static readonly PropertyPath FaceThicknessBackProperty = GetPropertyPath(p => p.FaceThicknessBack);
        public static readonly PropertyPath FaceThicknessTopProperty = GetPropertyPath(p => p.FaceThicknessTop);
        public static readonly PropertyPath FaceThicknessBottomProperty = GetPropertyPath(p => p.FaceThicknessBottom);
        public static readonly PropertyPath FaceThicknessLeftProperty = GetPropertyPath(p => p.FaceThicknessLeft);
        public static readonly PropertyPath FaceThicknessRightProperty = GetPropertyPath(p => p.FaceThicknessRight);
        public static readonly PropertyPath DividerObstructionHeightProperty = GetPropertyPath(p => p.DividerObstructionHeight);
        public static readonly PropertyPath DividerObstructionWidthProperty = GetPropertyPath(p => p.DividerObstructionWidth);
        public static readonly PropertyPath DividerObstructionDepthProperty = GetPropertyPath(p => p.DividerObstructionDepth);
        public static readonly PropertyPath DividerObstructionStartXProperty = GetPropertyPath(p => p.DividerObstructionStartX);
        public static readonly PropertyPath DividerObstructionSpacingXProperty = GetPropertyPath(p => p.DividerObstructionSpacingX);
        public static readonly PropertyPath DividerObstructionStartYProperty = GetPropertyPath(p => p.DividerObstructionStartY);
        public static readonly PropertyPath DividerObstructionSpacingYProperty = GetPropertyPath(p => p.DividerObstructionSpacingY);
        public static readonly PropertyPath DividerObstructionStartZProperty = GetPropertyPath(p => p.DividerObstructionStartZ);
        public static readonly PropertyPath DividerObstructionSpacingZProperty = GetPropertyPath(p => p.DividerObstructionSpacingZ);
        public static readonly PropertyPath IsDividerObstructionAtStartProperty = GetPropertyPath(p => p.IsDividerObstructionAtStart);
        public static readonly PropertyPath IsDividerObstructionAtEndProperty = GetPropertyPath(p => p.IsDividerObstructionAtEnd);
        public static readonly PropertyPath IsDividerObstructionByFacingProperty = GetPropertyPath(p => p.IsDividerObstructionByFacing);
        public static readonly PropertyPath DividerObstructionFillColourProperty = GetPropertyPath(p => p.DividerObstructionFillColour);
        public static readonly PropertyPath DividerObstructionFillPatternProperty = GetPropertyPath(p => p.DividerObstructionFillPattern);
        public static readonly PropertyPath MerchConstraintRow1StartXProperty = GetPropertyPath(p => p.MerchConstraintRow1StartX);
        public static readonly PropertyPath MerchConstraintRow1SpacingXProperty = GetPropertyPath(p => p.MerchConstraintRow1SpacingX);
        public static readonly PropertyPath MerchConstraintRow1StartYProperty = GetPropertyPath(p => p.MerchConstraintRow1StartY);
        public static readonly PropertyPath MerchConstraintRow1SpacingYProperty = GetPropertyPath(p => p.MerchConstraintRow1SpacingY);
        public static readonly PropertyPath MerchConstraintRow1HeightProperty = GetPropertyPath(p => p.MerchConstraintRow1Height);
        public static readonly PropertyPath MerchConstraintRow1WidthProperty = GetPropertyPath(p => p.MerchConstraintRow1Width);
        public static readonly PropertyPath MerchConstraintRow2StartXProperty = GetPropertyPath(p => p.MerchConstraintRow2StartX);
        public static readonly PropertyPath MerchConstraintRow2SpacingXProperty = GetPropertyPath(p => p.MerchConstraintRow2SpacingX);
        public static readonly PropertyPath MerchConstraintRow2StartYProperty = GetPropertyPath(p => p.MerchConstraintRow2StartY);
        public static readonly PropertyPath MerchConstraintRow2SpacingYProperty = GetPropertyPath(p => p.MerchConstraintRow2SpacingY);
        public static readonly PropertyPath MerchConstraintRow2HeightProperty = GetPropertyPath(p => p.MerchConstraintRow2Height);
        public static readonly PropertyPath MerchConstraintRow2WidthProperty = GetPropertyPath(p => p.MerchConstraintRow2Width);
        public static readonly PropertyPath CombineTypeProperty = GetPropertyPath(p => p.CombineType);
        public static readonly PropertyPath MerchandisingStrategyXProperty = GetPropertyPath(p => p.MerchandisingStrategyX);
        public static readonly PropertyPath MerchandisingStrategyYProperty = GetPropertyPath(p => p.MerchandisingStrategyY);
        public static readonly PropertyPath MerchandisingStrategyZProperty = GetPropertyPath(p => p.MerchandisingStrategyZ);
        public static readonly PropertyPath LeftOverhangProperty = GetPropertyPath(p => p.LeftOverhang);
        public static readonly PropertyPath RightOverhangProperty = GetPropertyPath(p => p.RightOverhang);
        public static readonly PropertyPath FrontOverhangProperty = GetPropertyPath(p => p.FrontOverhang);
        public static readonly PropertyPath BackOverhangProperty = GetPropertyPath(p => p.BackOverhang);
        public static readonly PropertyPath TopOverhangProperty = GetPropertyPath(p => p.TopOverhang);
        public static readonly PropertyPath BottomOverhangProperty = GetPropertyPath(p => p.BottomOverhang);
        public static readonly PropertyPath IsProductOverlapAllowedProperty = GetPropertyPath(p => p.IsProductOverlapAllowed);
        public static readonly PropertyPath IsProductSqueezeAllowedProperty = GetPropertyPath(p => p.IsProductSqueezeAllowed);
        public static readonly PropertyPath AreNotchPropertiesVisibleProperty = GetPropertyPath(p => p.AreNotchPropertiesVisible);
        public static readonly PropertyPath AreAreRiserPropertiesVisibleProperty = GetPropertyPath(p => p.AreRiserPropertiesVisible);
        public static readonly PropertyPath AreMerchConstraintRowPropertiesVisibleProperty = GetPropertyPath(p => p.AreMerchConstraintRowPropertiesVisible);
        public static readonly PropertyPath AreRiserPropertiesVisibleProperty = GetPropertyPath(p => p.AreRiserPropertiesVisible);
        public static readonly PropertyPath AreFaceThicknessPropertiesVisibleProperty = GetPropertyPath(p => p.AreFaceThicknessPropertiesVisible);
        public static readonly PropertyPath AreMerchandisingPropertiesVisibleProperty = GetPropertyPath(p => p.AreMerchandisingPropertiesVisible);
        public static readonly PropertyPath AreDividerPropertiesVisibleProperty = GetPropertyPath(p => p.AreDividerPropertiesVisible);
        public static readonly PropertyPath IsCombineTypePropertyVisibleProperty = GetPropertyPath(p => p.IsCombineTypePropertyVisible);
        public static readonly PropertyPath AreXOverhangPropertiesVisibleProperty = GetPropertyPath(p => p.AreXOverhangPropertiesVisible);
        public static readonly PropertyPath AreYOverhangPropertiesVisibleProperty = GetPropertyPath(p => p.AreYOverhangPropertiesVisible);
        public static readonly PropertyPath AreZOverhangPropertiesVisibleProperty = GetPropertyPath(p => p.AreZOverhangPropertiesVisible);
        public static readonly PropertyPath AreXDividerPropertiesVisibleProperty = GetPropertyPath(p => p.AreXDividerPropertiesVisible);
        public static readonly PropertyPath AreYDividerPropertiesVisibleProperty = GetPropertyPath(p => p.AreYDividerPropertiesVisible);
        public static readonly PropertyPath AreZDividerPropertiesVisibleProperty = GetPropertyPath(p => p.AreZDividerPropertiesVisible);
        public static readonly PropertyPath IsMerchandisingDepthPropertyVisibleProperty = GetPropertyPath(p => p.IsMerchandisingDepthPropertyVisible);
        public static readonly PropertyPath IsMerchandisingHeightPropertyVisibleProperty = GetPropertyPath(p => p.IsMerchandisingHeightPropertyVisible);


        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets whether changes should be logged as undoable actions.
        /// Note - this requires that a plan was specified on create.
        /// </summary>
        public Boolean IsLoggingUndoableActions
        {
            get { return _plan != null && _isLoggingUndoableActions; }
            set { _isLoggingUndoableActions = value; }
        }

        public IEnumerable<PlanogramSubComponentView> Items
        {
            get
            {
                return _items;
            }
        }

        #region Riser Properties

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "RiserHeightProperty")]
        public Single? RiserHeight
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.RiserHeightProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.RiserHeightProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "RiserThicknessProperty")]
        public Single? RiserThickness
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.RiserThicknessProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.RiserThicknessProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "RiserColourProperty")]
        public Color RiserColour
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, PlanogramSubComponent.RiserColourProperty.Name);

                if (val.HasValue)
                {
                    return CommonHelper.IntToColor(val.Value);
                }
                else
                {
                    return Colors.White;
                }
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.RiserColourProperty.Name, CommonHelper.ColorToInt(value));
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "IsRiserPlacedOnFrontProperty")]
        public Boolean? IsRiserPlacedOnFront
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramSubComponent.IsRiserPlacedOnFrontProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.IsRiserPlacedOnFrontProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "IsRiserPlacedOnBackProperty")]
        public Boolean? IsRiserPlacedOnBack
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramSubComponent.IsRiserPlacedOnBackProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.IsRiserPlacedOnBackProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "IsRiserPlacedOnLeftProperty")]
        public Boolean? IsRiserPlacedOnLeft
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramSubComponent.IsRiserPlacedOnLeftProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.IsRiserPlacedOnLeftProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "IsRiserPlacedOnRightProperty")]
        public Boolean? IsRiserPlacedOnRight
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramSubComponent.IsRiserPlacedOnRightProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.IsRiserPlacedOnRightProperty.Name, value.Value);
                }
            }
        }

        #endregion

        #region Notch Properties

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "NotchStartXProperty")]
        public Single? NotchStartX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.NotchStartXProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.NotchStartXProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "NotchSpacingXProperty")]
        public Single? NotchSpacingX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.NotchSpacingXProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.NotchSpacingXProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "NotchStartYProperty")]
        public Single? NotchStartY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.NotchStartYProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.NotchStartYProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "NotchSpacingYProperty")]
        public Single? NotchSpacingY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.NotchSpacingYProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.NotchSpacingYProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "NotchHeightProperty")]
        public Single? NotchHeight
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.NotchHeightProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.NotchHeightProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "NotchWidthProperty")]
        public Single? NotchWidth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.NotchWidthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.NotchWidthProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "IsNotchPlacedOnFrontProperty")]
        public Boolean? IsNotchPlacedOnFront
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramSubComponent.IsNotchPlacedOnFrontProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.IsNotchPlacedOnFrontProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "NotchStyleTypeProperty")]
        [PropertyEditorItemsSource(typeof(PropertyEditorItemsSource<PlanogramSubComponentNotchStyleType>))]
        public PlanogramSubComponentNotchStyleType? NotchStyleType
        {
            get
            {
                return GetCommonPropertyValue<PlanogramSubComponentNotchStyleType?>(Items, PlanogramSubComponent.NotchStyleTypeProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.NotchStyleTypeProperty.Name, value.Value);
                }
            }
        }

        #endregion

        #region Face Thickness Properties

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "FaceThicknessFrontProperty")]
        public Single? FaceThicknessFront
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.FaceThicknessFrontProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.FaceThicknessFrontProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "FaceThicknessBackProperty")]
        public Single? FaceThicknessBack
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.FaceThicknessBackProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.FaceThicknessBackProperty.Name, value.Value);
                }
            }
        }

        //NB-Face Thickness front is not used as thse properties are really for chests and so 
        //top is always 0.
        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "FaceThicknessTopProperty")]
        public Single? FaceThicknessTop
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.FaceThicknessTopProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.FaceThicknessTopProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "FaceThicknessBottomProperty")]
        public Single? FaceThicknessBottom
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.FaceThicknessBottomProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.FaceThicknessBottomProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "FaceThicknessLeftProperty")]
        public Single? FaceThicknessLeft
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.FaceThicknessLeftProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.FaceThicknessLeftProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "FaceThicknessRightProperty")]
        public Single? FaceThicknessRight
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.FaceThicknessRightProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.FaceThicknessRightProperty.Name, value.Value);
                }
            }
        }

        #endregion

        #region Divider Properties

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "DividerObstructionHeightProperty")]
        public Single? DividerObstructionHeight
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.DividerObstructionHeightProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.DividerObstructionHeightProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "DividerObstructionWidthProperty")]
        public Single? DividerObstructionWidth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.DividerObstructionWidthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.DividerObstructionWidthProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "DividerObstructionDepthProperty")]
        public Single? DividerObstructionDepth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.DividerObstructionDepthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.DividerObstructionDepthProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "DividerObstructionStartXProperty")]
        public Single? DividerObstructionStartX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.DividerObstructionStartXProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.DividerObstructionStartXProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "DividerObstructionSpacingXProperty")]
        public Single? DividerObstructionSpacingX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.DividerObstructionSpacingXProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.DividerObstructionSpacingXProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "DividerObstructionStartYProperty")]
        public Single? DividerObstructionStartY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.DividerObstructionStartYProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.DividerObstructionStartYProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "DividerObstructionSpacingYProperty")]
        public Single? DividerObstructionSpacingY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.DividerObstructionSpacingYProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.DividerObstructionSpacingYProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "DividerObstructionStartZProperty")]
        public Single? DividerObstructionStartZ
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.DividerObstructionStartZProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.DividerObstructionStartZProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "DividerObstructionSpacingZProperty")]
        public Single? DividerObstructionSpacingZ
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.DividerObstructionSpacingZProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.DividerObstructionSpacingZProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "IsDividerObstructionAtStartProperty")]
        public Boolean? IsDividerObstructionAtStart
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramSubComponent.IsDividerObstructionAtStartProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.IsDividerObstructionAtStartProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "IsDividerObstructionAtEndProperty")]
        public Boolean? IsDividerObstructionAtEnd
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramSubComponent.IsDividerObstructionAtEndProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.IsDividerObstructionAtEndProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "IsDividerObstructionByFacingProperty")]
        public Boolean? IsDividerObstructionByFacing
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramSubComponent.IsDividerObstructionByFacingProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.IsDividerObstructionByFacingProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "DividerObstructionFillColourProperty")]
        public Color DividerObstructionFillColour
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, PlanogramSubComponent.DividerObstructionFillColourProperty.Name);
                return (val.HasValue) ? CommonHelper.IntToColor(val.Value) : Colors.White;
            }
            set
            {
                if (value != null && value != this.DividerObstructionFillColour)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.DividerObstructionFillColourProperty.Name, CommonHelper.ColorToInt(value));
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "DividerObstructionFillPatternProperty")]
        public PlanogramSubComponentFillPatternType? DividerObstructionFillPattern
        {
            get
            {
                return GetCommonPropertyValue<PlanogramSubComponentFillPatternType?>(Items, PlanogramSubComponent.DividerObstructionFillPatternProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.DividerObstructionFillPatternProperty.Name, value.Value);
                }
            }
        }

        #endregion

        #region MerchConstraintRow Properties

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "MerchConstraintRow1StartXProperty")]
        public Single? MerchConstraintRow1StartX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.MerchConstraintRow1StartXProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.MerchConstraintRow1StartXProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "MerchConstraintRow1SpacingXProperty")]
        public Single? MerchConstraintRow1SpacingX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.MerchConstraintRow1SpacingXProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.MerchConstraintRow1SpacingXProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "MerchConstraintRow1StartYProperty")]
        public Single? MerchConstraintRow1StartY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.MerchConstraintRow1StartYProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.MerchConstraintRow1StartYProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "MerchConstraintRow1SpacingYProperty")]
        public Single? MerchConstraintRow1SpacingY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.MerchConstraintRow1SpacingYProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.MerchConstraintRow1SpacingYProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "MerchConstraintRow1HeightProperty")]
        public Single? MerchConstraintRow1Height
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.MerchConstraintRow1HeightProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.MerchConstraintRow1HeightProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "MerchConstraintRow1WidthProperty")]
        public Single? MerchConstraintRow1Width
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.MerchConstraintRow1WidthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.MerchConstraintRow1WidthProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "MerchConstraintRow2StartXProperty")]
        public Single? MerchConstraintRow2StartX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.MerchConstraintRow2StartXProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.MerchConstraintRow2StartXProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "MerchConstraintRow2SpacingXProperty")]
        public Single? MerchConstraintRow2SpacingX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.MerchConstraintRow2SpacingXProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.MerchConstraintRow2SpacingXProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "MerchConstraintRow2StartYProperty")]
        public Single? MerchConstraintRow2StartY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.MerchConstraintRow2StartYProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.MerchConstraintRow2StartYProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "MerchConstraintRow2SpacingYProperty")]
        public Single? MerchConstraintRow2SpacingY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.MerchConstraintRow2SpacingYProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.MerchConstraintRow2SpacingYProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "MerchConstraintRow2HeightProperty")]
        public Single? MerchConstraintRow2Height
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.MerchConstraintRow2HeightProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.MerchConstraintRow2HeightProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "MerchConstraintRow2WidthProperty")]
        public Single? MerchConstraintRow2Width
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.MerchConstraintRow2WidthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.MerchConstraintRow2WidthProperty.Name, value.Value);
                }
            }
        }

        #endregion

        #region Merchandising Properties

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "CombineTypeProperty")]
        [PropertyEditorItemsSource(typeof(PropertyEditorItemsSource<PlanogramSubComponentCombineType>))]
        public PlanogramSubComponentCombineType? CombineType
        {
            get
            {
                return GetCommonPropertyValue<PlanogramSubComponentCombineType?>(Items, PlanogramSubComponent.CombineTypeProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.CombineTypeProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "MerchandisableHeightProperty")]
        public Single? MerchandisableHeight
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.MerchandisableHeightProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.MerchandisableHeightProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "MerchandisableDepthProperty")]
        public Single? MerchandisableDepth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.MerchandisableDepthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.MerchandisableDepthProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "MerchandisingStrategyXProperty")]
        [PropertyEditorItemsSource(typeof(PropertyEditorItemsSource<PlanogramSubComponentXMerchStrategyType>))]
        public PlanogramSubComponentXMerchStrategyType? MerchandisingStrategyX
        {
            get
            {
                return GetCommonPropertyValue<PlanogramSubComponentXMerchStrategyType?>(Items, PlanogramSubComponent.MerchandisingStrategyXProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.MerchandisingStrategyXProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "MerchandisingStrategyYProperty")]
        [PropertyEditorItemsSource(typeof(PropertyEditorItemsSource<PlanogramSubComponentYMerchStrategyType>))]
        public PlanogramSubComponentYMerchStrategyType? MerchandisingStrategyY
        {
            get
            {
                return GetCommonPropertyValue<PlanogramSubComponentYMerchStrategyType?>(Items, PlanogramSubComponent.MerchandisingStrategyYProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.MerchandisingStrategyYProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "MerchandisingStrategyZProperty")]
        [PropertyEditorItemsSource(typeof(PropertyEditorItemsSource<PlanogramSubComponentZMerchStrategyType>))]
        public PlanogramSubComponentZMerchStrategyType? MerchandisingStrategyZ
        {
            get
            {
                return GetCommonPropertyValue<PlanogramSubComponentZMerchStrategyType?>(Items, PlanogramSubComponent.MerchandisingStrategyZProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.MerchandisingStrategyZProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "IsProductOverlapAllowedProperty")]
        public Boolean? IsProductOverlapAllowed
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramSubComponent.IsProductOverlapAllowedProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.IsProductOverlapAllowedProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "IsProductSqueezeAllowedProperty")]
        public Boolean? IsProductSqueezeAllowed
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramSubComponent.IsProductSqueezeAllowedProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.IsProductSqueezeAllowedProperty.Name, value.Value);
                }
            }
        }



        private PlanogramSubComponentMerchandisingType? MerchandisingType
        {
            get
            {
                return GetCommonPropertyValue<PlanogramSubComponentMerchandisingType?>(
                    Items, PlanogramSubComponent.MerchandisingTypeProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.MerchandisingTypeProperty.Name, value.Value);
                }
            }
        }

        #endregion

        #region Overhang Properties

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "LeftOverhangProperty")]
        public Single? LeftOverhang
        {
            get { return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.LeftOverhangProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.LeftOverhangProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "RightOverhangProperty")]
        public Single? RightOverhang
        {
            get { return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.RightOverhangProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.RightOverhangProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "TopOverhangProperty")]
        public Single? TopOverhang
        {
            get { return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.TopOverhangProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.TopOverhangProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "BottomOverhangProperty")]
        public Single? BottomOverhang
        {
            get { return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.BottomOverhangProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.BottomOverhangProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "FrontOverhangProperty")]
        public Single? FrontOverhang
        {
            get { return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.FrontOverhangProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.FrontOverhangProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "BackOverhangProperty")]
        public Single? BackOverhang
        {
            get { return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.BackOverhangProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.BackOverhangProperty.Name, value);
                }
            }
        }

        #endregion

        #region Visibility Properties

        /// <summary>
        /// Returns true if notch properties should be displayed
        /// for this selection.
        /// </summary>
        public Boolean AreNotchPropertiesVisible
        {
            get
            {
                return _componentType == PlanogramComponentType.Backboard;
            }
        }

        /// <summary>
        /// Returns true if shelf properties should be
        /// displayed for this selection.
        /// </summary>
        public Boolean AreRiserPropertiesVisible
        {
            get { return _componentType == PlanogramComponentType.Shelf; }

        }

        /// <summary>
        /// Returns true if merch constraint row properties should
        /// be displayed for this selection.
        /// </summary>
        public Boolean AreMerchConstraintRowPropertiesVisible
        {
            get
            {
                return
                    this.Items.All(s => s.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang);
            }
        }

        /// <summary>
        /// Returns true if merch constraint row2 properties should
        /// be displayed for this selection.
        /// </summary>
        public Boolean AreMerchConstraintRow2PropertiesVisible
        {
            get
            {
                //must be a pegboard.
                return
                    this.Items.All(s => s.Component.ComponentType == PlanogramComponentType.Peg);
            }
        }

        public Boolean AreMerchConstraintRowXSpacingPropertiesVisible
        {
            get
            {
                return //SlotWall type is a special case for now
                    this.Items.All(s => s.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang &&
                    s.Component.ComponentType != PlanogramComponentType.SlotWall);
            }
        }

        /// <summary>
        /// Returns true if face thickness properties should be
        /// displayed for the current selection.
        /// </summary>
        public Boolean AreFaceThicknessPropertiesVisible
        {
            get { return _componentType == PlanogramComponentType.Chest; }
        }

        /// <summary>
        /// Returns true if merchanidsing properties should be displayed
        /// for this selection
        /// </summary>
        public Boolean AreMerchandisingPropertiesVisible
        {
            get { return this.Items.Any(i => i.MerchandisingType != PlanogramSubComponentMerchandisingType.None); }
        }

        /// <summary>
        /// Returns true if the combine type property
        /// should be displayed for this selection
        /// </summary>
        public Boolean IsCombineTypePropertyVisible
        {
            get
            {
                return _componentType == PlanogramComponentType.Shelf
                    || _componentType == PlanogramComponentType.Bar;
            }
        }

        public Boolean IsMerchandisingDepthPropertyVisible
        {
            get
            {
                return (_componentType == PlanogramComponentType.Bar
                    || _componentType == PlanogramComponentType.SlotWall
                    || _componentType == PlanogramComponentType.Peg
                    || _componentType == PlanogramComponentType.ClipStrip
                    || _componentType == PlanogramComponentType.Rod
                    || this.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang
                    || this.MerchandisingType == PlanogramSubComponentMerchandisingType.HangFromBottom);
            }
        }

        public Boolean IsMerchandisingHeightPropertyVisible
        {
            get { return !IsMerchandisingDepthPropertyVisible; }
        }

        /// <summary>
        /// Returns true if x overhang properties should be
        /// displayed for this selection
        /// </summary>
        public Boolean AreXOverhangPropertiesVisible
        {
            get
            {
                return AreMerchandisingPropertiesVisible
                    && Items.All(i => i.MerchandisingType != PlanogramSubComponentMerchandisingType.HangFromBottom);
            }
        }

        /// <summary>
        /// Returns true if y overhang properties should be
        /// displayed for this selection
        /// </summary>
        public Boolean AreYOverhangPropertiesVisible
        {
            get
            {
                return Items.All(i => i.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang);
            }
        }

        /// <summary>
        /// Returns true if z overhang properties should be displayed for
        /// this selection.
        /// </summary>
        public Boolean AreZOverhangPropertiesVisible
        {
            get
            {
                return Items.All(i => i.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack
                || i.MerchandisingType == PlanogramSubComponentMerchandisingType.HangFromBottom);
            }
        }



        /// <summary>
        /// Returns true if divider properties should be
        /// displayed for this selection.
        /// </summary>
        public Boolean AreDividerPropertiesVisible
        {
            get
            {
                //only show dividers if the components are merchandisable
                // and of the correct type.
                return
                    this.Items.All
                    (s => s.MerchandisingType != PlanogramSubComponentMerchandisingType.None
                        && (s.Component.ComponentType == PlanogramComponentType.Shelf
                        || s.Component.ComponentType == PlanogramComponentType.Chest
                        || s.Component.ComponentType == PlanogramComponentType.Custom));
            }
        }

        public Boolean IsDividerHeightVisible
        {
            get
            {
                return
                    //if x and stacked or hang blow
                    (AreXDividerPropertiesVisible
                    && this.Items.Any(s => s.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack ||
                    s.MerchandisingType == PlanogramSubComponentMerchandisingType.HangFromBottom))

                    //or if z and stacked or hang blow
                    || (AreZDividerPropertiesVisible
                    && this.Items.Any(s => s.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack ||
                    s.MerchandisingType == PlanogramSubComponentMerchandisingType.HangFromBottom));
            }
        }

        public Boolean IsDividerDepthVisible
        {
            get
            {
                return
                    //if x and hang
                    (AreXDividerPropertiesVisible
                    && this.Items.Any(s => s.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang))

                    //or if y and hang
                    || (AreYDividerPropertiesVisible
                    && this.Items.Any(s => s.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang));
            }
        }

        /// <summary>
        /// Returns true if the x divider properties
        /// should be displayed for this selection
        /// </summary>
        public Boolean AreXDividerPropertiesVisible
        {
            get
            {

                return

                    //dividers must be visible
                    AreDividerPropertiesVisible

                    //and all items must be a shelf, a chest or a custom type.
                    && this.Items.All(s =>
                    s.Component.ComponentType == PlanogramComponentType.Shelf
                    || s.Component.ComponentType == PlanogramComponentType.Chest
                    || s.Component.ComponentType == PlanogramComponentType.Custom);
            }
        }

        /// <summary>
        /// Returns true if the x divider properties
        /// should be displayed for this selection
        /// </summary>
        public Boolean AreYDividerPropertiesVisible
        {
            get
            {
                return

                    //dividers must be visible
                    AreDividerPropertiesVisible

                    //and all items must be a custom hang
                    && this.Items.All(s => s.Component.ComponentType == PlanogramComponentType.Custom);

            }
        }

        /// <summary>
        /// Returns true if the x divider properties
        /// should be displayed for this selection
        /// </summary>
        public Boolean AreZDividerPropertiesVisible
        {
            get
            {
                return

                    //dividers must be visible
                    AreDividerPropertiesVisible

                    //and all items must either be a chest or a custom.
                    && this.Items.All(s =>
                    s.Component.ComponentType == PlanogramComponentType.Chest
                    || s.Component.ComponentType == PlanogramComponentType.Custom);
            }
        }

        #endregion

        #region Styling Properties

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "FillColourFrontProperty")]
        public Color FillColourFront
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, PlanogramSubComponent.FillColourFrontProperty.Name);
                return (val.HasValue)? CommonHelper.IntToColor(val.Value) : Colors.White;
            }
            set
            {
                if (value != null && value != this.FillColourFront)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.FillColourFrontProperty.Name, CommonHelper.ColorToInt(value));
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "FillColourBackProperty")]
        public Color FillColourBack
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, PlanogramSubComponent.FillColourBackProperty.Name);
                return (val.HasValue) ? CommonHelper.IntToColor(val.Value) : Colors.White;
            }
            set
            {
                if (value != null && value != this.FillColourBack)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.FillColourBackProperty.Name, CommonHelper.ColorToInt(value));
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "FillColourLeftProperty")]
        public Color FillColourLeft
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, PlanogramSubComponent.FillColourLeftProperty.Name);
                return (val.HasValue) ? CommonHelper.IntToColor(val.Value) : Colors.White;
            }
            set
            {
                if (value != null && value != this.FillColourLeft)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.FillColourLeftProperty.Name, CommonHelper.ColorToInt(value));
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "FillColourRightProperty")]
        public Color FillColourRight
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, PlanogramSubComponent.FillColourRightProperty.Name);
                return (val.HasValue) ? CommonHelper.IntToColor(val.Value) : Colors.White;
            }
            set
            {
                if (value != null && value != this.FillColourRight)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.FillColourRightProperty.Name, CommonHelper.ColorToInt(value));
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "FillColourTopProperty")]
        public Color FillColourTop
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, PlanogramSubComponent.FillColourTopProperty.Name);
                return (val.HasValue) ? CommonHelper.IntToColor(val.Value) : Colors.White;
            }
            set
            {
                if (value != null && value != this.FillColourTop)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.FillColourTopProperty.Name, CommonHelper.ColorToInt(value));
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "FillColourBottomProperty")]
        public Color FillColourBottom
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, PlanogramSubComponent.FillColourBottomProperty.Name);
                return (val.HasValue) ? CommonHelper.IntToColor(val.Value) : Colors.White;
            }
            set
            {
                if (value != null && value != this.FillColourBottom)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.FillColourBottomProperty.Name, CommonHelper.ColorToInt(value));
                }
            }
        }
       
        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "FillPatternTypeFrontProperty")]
        public PlanogramSubComponentFillPatternType? FillPatternTypeFront
        {
            get
            {
                return GetCommonPropertyValue<PlanogramSubComponentFillPatternType?>(Items, PlanogramSubComponent.FillPatternTypeFrontProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.FillPatternTypeFrontProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "FillPatternTypeBackProperty")]
        public PlanogramSubComponentFillPatternType? FillPatternTypeBack
        {
            get
            {
                return GetCommonPropertyValue<PlanogramSubComponentFillPatternType?>(Items, PlanogramSubComponent.FillPatternTypeBackProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.FillPatternTypeBackProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "FillPatternTypeTopProperty")]
        public PlanogramSubComponentFillPatternType? FillPatternTypeTop
        {
            get
            {
                return GetCommonPropertyValue<PlanogramSubComponentFillPatternType?>(Items, PlanogramSubComponent.FillPatternTypeTopProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.FillPatternTypeTopProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "FillPatternTypeBottomProperty")]
        public PlanogramSubComponentFillPatternType? FillPatternTypeBottom
        {
            get
            {
                return GetCommonPropertyValue<PlanogramSubComponentFillPatternType?>(Items, PlanogramSubComponent.FillPatternTypeBottomProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.FillPatternTypeBottomProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "FillPatternTypeLeftProperty")]
        public PlanogramSubComponentFillPatternType? FillPatternTypeLeft
        {
            get
            {
                return GetCommonPropertyValue<PlanogramSubComponentFillPatternType?>(Items, PlanogramSubComponent.FillPatternTypeLeftProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.FillPatternTypeLeftProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "FillPatternTypeRightProperty")]
        public PlanogramSubComponentFillPatternType? FillPatternTypeRight
        {
            get
            {
                return GetCommonPropertyValue<PlanogramSubComponentFillPatternType?>(Items, PlanogramSubComponent.FillPatternTypeRightProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.FillPatternTypeRightProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "LineColourProperty")]
        public Color LineColour
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, PlanogramSubComponent.LineColourProperty.Name);
                return (val.HasValue) ? CommonHelper.IntToColor(val.Value) : Colors.White;
            }
            set
            {
                if (value != null && value != this.LineColour)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.LineColourProperty.Name, CommonHelper.ColorToInt(value));
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramSubComponent), "LineThicknessProperty")]
        public Single? LineThickness
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramSubComponent.LineThicknessProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramSubComponent.LineThicknessProperty.Name, value.Value);
                }
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="items"></param>
        public PlanogramSubComponentMultiView(IEnumerable<PlanogramSubComponentView> items)
        {
            _items = items.ToList();
            _plan = items.First().Planogram;

            _componentType = GetCommonPropertyValue<PlanogramComponentType?>(items.Select(s => s.Component), PlanogramComponent.ComponentTypeProperty.Name);

            foreach (var i in items)
            {
                i.PropertyChanged += PlanItem_PropertyChanged;
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on any linked plan item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(e.PropertyName);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the common value of the given property for all the items
        /// </summary>
        private static T GetCommonPropertyValue<T>(IEnumerable<Object> items, String propertyName)
        {
            T value = default(T);

            if (items.Any())
            {
                Type classType = items.First().GetType();
                System.Reflection.PropertyInfo pInfo = classType.GetProperty(propertyName);
                if (pInfo != null)
                {
                    Boolean isFirst = true;
                    foreach (Object item in items)
                    {
                        T itemValue = (T)pInfo.GetValue(item, null);
                        if (isFirst)
                        {
                            value = itemValue;
                        }
                        else if (!Object.Equals(itemValue, value))
                        {
                            return default(T);
                        }

                        isFirst = false;
                    }
                }
            }

            return value;

        }

        /// <summary>
        /// Sets the given value against
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        private void SetPropertyValue<T>(IEnumerable<Object> items, String propertyName, T value)
        {
            if (items.Any())
            {

                //Start logging the change
                Boolean isProcessingUndoable = false;
                if (IsLoggingUndoableActions)
                {
                    if (!_plan.IsRecordingUndoableAction)
                    {
                        isProcessingUndoable = true;
                        _plan.BeginUndoableAction(propertyName);
                    }
                }

                //make the property change
                Type classType = items.First().GetType();
                System.Reflection.PropertyInfo pInfo = classType.GetProperty(propertyName);

                if (pInfo != null)
                {
                    foreach (Object item in items)
                    {
                        pInfo.SetValue(item, value, null);
                    }
                }

                //Commit the log.
                if (isProcessingUndoable)
                {
                    _plan.EndUndoableAction();
                }
            }
        }

        /// <summary>
        /// Returns true if the given property should be displayed for this selection
        /// </summary>
        public Boolean ShowProperty(String propertyName)
        {
            // Notch properties
            if (propertyName == PlanogramSubComponent.IsNotchPlacedOnFrontProperty.Name
                    || propertyName == PlanogramSubComponent.NotchStyleTypeProperty.Name
                    || propertyName == PlanogramSubComponent.NotchStartXProperty.Name
                    || propertyName == PlanogramSubComponent.NotchSpacingXProperty.Name
                    || propertyName == PlanogramSubComponent.NotchStartYProperty.Name
                    || propertyName == PlanogramSubComponent.NotchSpacingYProperty.Name
                    || propertyName == PlanogramSubComponent.NotchHeightProperty.Name
                    || propertyName == PlanogramSubComponent.NotchWidthProperty.Name)
            {
                return this.AreNotchPropertiesVisible;
            }

            // Divider Properties
            else if (propertyName == PlanogramSubComponent.DividerObstructionHeightProperty.Name
                || propertyName == PlanogramSubComponent.DividerObstructionWidthProperty.Name
                || propertyName == PlanogramSubComponent.DividerObstructionDepthProperty.Name
                || propertyName == PlanogramSubComponent.IsDividerObstructionAtStartProperty.Name
                || propertyName == PlanogramSubComponent.IsDividerObstructionAtEndProperty.Name)
            {
                return this.AreDividerPropertiesVisible;
            }

            else if (propertyName == PlanogramSubComponent.DividerObstructionStartXProperty.Name
                || propertyName == PlanogramSubComponent.DividerObstructionSpacingXProperty.Name
                || propertyName == PlanogramSubComponent.IsDividerObstructionByFacingProperty.Name)
            {
                return this.AreXDividerPropertiesVisible;
            }

            else if (propertyName == PlanogramSubComponent.DividerObstructionStartYProperty.Name
                || propertyName == PlanogramSubComponent.DividerObstructionSpacingYProperty.Name)
            {
                return this.AreYDividerPropertiesVisible;
            }

            else if (propertyName == PlanogramSubComponent.DividerObstructionStartZProperty.Name
               || propertyName == PlanogramSubComponent.DividerObstructionSpacingZProperty.Name)
            {
                return this.AreZDividerPropertiesVisible;
            }


            // Riser Properties
            else if (propertyName == PlanogramSubComponent.RiserThicknessProperty.Name
                || propertyName == PlanogramSubComponent.RiserHeightProperty.Name
                || propertyName == PlanogramSubComponent.RiserColourProperty.Name
                || propertyName == PlanogramSubComponent.RiserTransparencyPercentProperty.Name
                || propertyName == PlanogramSubComponent.IsRiserPlacedOnFrontProperty.Name
                || propertyName == PlanogramSubComponent.IsRiserPlacedOnBackProperty.Name
                || propertyName == PlanogramSubComponent.IsRiserPlacedOnLeftProperty.Name
                || propertyName == PlanogramSubComponent.IsRiserPlacedOnRightProperty.Name
                || propertyName == PlanogramSubComponent.RiserFillPatternTypeProperty.Name)
            {
                return this.AreRiserPropertiesVisible;
            }


            // Peghole Properties
            else if (propertyName == PlanogramSubComponent.MerchConstraintRow1StartXProperty.Name
                || propertyName == PlanogramSubComponent.MerchConstraintRow1StartYProperty.Name
                || propertyName == PlanogramSubComponent.MerchConstraintRow1SpacingYProperty.Name
                || propertyName == PlanogramSubComponent.MerchConstraintRow1HeightProperty.Name
                || propertyName == PlanogramSubComponent.MerchConstraintRow2StartXProperty.Name
                || propertyName == PlanogramSubComponent.MerchConstraintRow2StartYProperty.Name
                || propertyName == PlanogramSubComponent.MerchConstraintRow2SpacingYProperty.Name
                || propertyName == PlanogramSubComponent.MerchConstraintRow2HeightProperty.Name)
            {
                return this.AreMerchConstraintRowPropertiesVisible;
            }

            else if (propertyName == PlanogramSubComponent.MerchConstraintRow1SpacingXProperty.Name
                || propertyName == PlanogramSubComponent.MerchConstraintRow1WidthProperty.Name
                || propertyName == PlanogramSubComponent.MerchConstraintRow2SpacingXProperty.Name
                || propertyName == PlanogramSubComponent.MerchConstraintRow2WidthProperty.Name)
            {
                return this.AreMerchConstraintRowXSpacingPropertiesVisible;
            }

            // Merchandising Properties

            else if (propertyName == PlanogramSubComponent.MerchandisingStrategyXProperty.Name
                || propertyName == PlanogramSubComponent.MerchandisingStrategyYProperty.Name
                || propertyName == PlanogramSubComponent.MerchandisingStrategyZProperty.Name
                || propertyName == PlanogramSubComponent.MerchandisableHeightProperty.Name
                || propertyName == PlanogramSubComponent.MerchandisableDepthProperty.Name)
            {
                return this.AreMerchandisingPropertiesVisible;
            }

            else if (propertyName == PlanogramSubComponent.CombineTypeProperty.Name)
            {
                return this.IsCombineTypePropertyVisible;
            }

            else if (propertyName == PlanogramSubComponent.LeftOverhangProperty.Name
            || propertyName == PlanogramSubComponent.RightOverhangProperty.Name)
            {
                return this.AreXOverhangPropertiesVisible;
            }

            else if (propertyName == PlanogramSubComponent.FrontOverhangProperty.Name
                || propertyName == PlanogramSubComponent.BackOverhangProperty.Name)
            {
                return this.AreYOverhangPropertiesVisible;
            }

            else if (propertyName == PlanogramSubComponent.TopOverhangProperty.Name
                || propertyName == PlanogramSubComponent.BottomOverhangProperty.Name)
            {
                return this.AreZOverhangPropertiesVisible;
            }

            //Face Thickness
            else if (propertyName == PlanogramSubComponent.FaceThicknessFrontProperty.Name
               || propertyName == PlanogramSubComponent.FaceThicknessBackProperty.Name
               || propertyName == PlanogramSubComponent.FaceThicknessTopProperty.Name
                || propertyName == PlanogramSubComponent.FaceThicknessBottomProperty.Name
                || propertyName == PlanogramSubComponent.FaceThicknessLeftProperty.Name
                || propertyName == PlanogramSubComponent.FaceThicknessRightProperty.Name)
            {
                return this.AreFaceThicknessPropertiesVisible;
            }


            return true;
        }



        #endregion

        #region IDataErrorInfo Members

        public String Error
        {
            get { throw new NotImplementedException(); }
        }

        public String this[String columnName]
        {
            get
            {
                return ((IDataErrorInfo)_items.First())[columnName];
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                foreach (var i in _items)
                {
                    i.PropertyChanged -= PlanItem_PropertyChanged;
                }
                base.IsDisposed = true;
            }
        }

        #endregion

        #region Static Helpers

        private static PropertyPath GetPropertyPath(System.Linq.Expressions.Expression<Func<PlanogramSubComponentMultiView, Object>> expression)
        {
            return WpfHelper.GetPropertyPath<PlanogramSubComponentMultiView>(expression);
        }

        #endregion

    }
}
