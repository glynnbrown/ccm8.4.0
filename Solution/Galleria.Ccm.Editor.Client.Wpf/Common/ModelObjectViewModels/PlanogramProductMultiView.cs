﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// CCM-26149 : L.Hodson
//  Created
// V8-26337 : A.Kuszyk
//  Added Performance Data properties.
// V8-26516 : L.Ineson
//  Added squeeze actual values.
// CCM-26149 : L.Ineson
//  Split out performance properties into PlanogramPerformanceDataMultiView
//  Split out custom attribute properties into CustomAttributeDataMultiView 
// V8-27571 : I.George 
//  Added IsTrayProduct
// V8-27606 : L.Ineson
//  Added FinancialGroupCode and Name

#endregion

#region Version History: (CCM 8.03)

// V8-29427 : L.Luong
//  Added Meta Data properties
// V8-28491 : L.Luong
//  Added MetaIsInMasterData

#endregion

#region Version History: (CCM 8.1.0)

// V8-29446 : M.Pettit
//  Manually entered values of 0 for Height,Width,Depth are ignored
// V8-29533 : D.Pleasance
//  Added data error validation to check for duplicate product Gtins

#endregion

#region Version History: CCM811

// V8-30328 : A.Silva
//  Amended the contructor so that it does not enumerate the items more than once, and does not initialize either CustomAttributesView or PerformanceDataView.
//  Amended CustomAttributesView and PerformanceDataView to be lazy loaded, and fetched only when required.

#endregion

#region Version History: CCM820
// V8-30665 : A.Probyn
//  Applied Alvaro's fix to the CustomAttributes property to stop the property changed event being spammed
//  when position properties are changed on a lot of products at once.
#endregion

#region Version History: CCM830
// V8-31531 : A.Heathcote
//  Removed IsTrayProduct
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.Attributes;
using Galleria.Framework.Controls.Wpf.PropertyEditor;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Presents a merged view of multiple planogramproductviews
    /// </summary>
    public sealed class PlanogramProductMultiView : ViewModelObject, IDataErrorInfo
    {
        #region Fields

        /// <summary>
        ///     Stores the collection of <see cref="PlanogramProductView"/> items that are part of this <c>Product Multi View</c>.
        /// </summary>
        private readonly ICollection<PlanogramProductView> _items;

        /// <summary>
        ///     Stores the <see cref="PlanogramView"/> that the items of this instance belong to.
        /// </summary>
        private readonly PlanogramView _plan;

        private Boolean _isLoggingUndoableActions = true;
        private Boolean _isValid = true;

        private CustomAttributeDataMultiView _customAttributesView;
        private PlanogramPerformanceDataMultiView _performanceDataView;


        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath GtinProperty = GetPropertyPath(p => p.Gtin);
        public static readonly PropertyPath NameProperty = GetPropertyPath(p => p.Name);
        public static readonly PropertyPath HeightProperty = GetPropertyPath(p => p.Height);
        public static readonly PropertyPath WidthProperty = GetPropertyPath(p => p.Width);
        public static readonly PropertyPath DepthProperty = GetPropertyPath(p => p.Depth);
        public static readonly PropertyPath TrayHighProperty = GetPropertyPath(p => p.TrayHigh);
        public static readonly PropertyPath TrayWideProperty = GetPropertyPath(p => p.TrayWide);
        public static readonly PropertyPath TrayDeepProperty = GetPropertyPath(p => p.TrayDeep);
        public static readonly PropertyPath TrayHeightProperty = GetPropertyPath(p => p.TrayHeight);
        public static readonly PropertyPath TrayWidthProperty = GetPropertyPath(p => p.TrayWidth);
        public static readonly PropertyPath TrayDepthProperty = GetPropertyPath(p => p.TrayDepth);
        public static readonly PropertyPath TrayThickHeightProperty = GetPropertyPath(p => p.TrayThickHeight);
        public static readonly PropertyPath TrayThickWidthProperty = GetPropertyPath(p => p.TrayThickWidth);
        public static readonly PropertyPath TrayThickDepthProperty = GetPropertyPath(p => p.TrayThickDepth);
        public static readonly PropertyPath TrayPackUnitsProperty = GetPropertyPath(p => p.TrayPackUnits);
        public static readonly PropertyPath BrandProperty = GetPropertyPath(p => p.Brand);
        public static readonly PropertyPath DisplayHeightProperty = GetPropertyPath(p => p.DisplayHeight);
        public static readonly PropertyPath DisplayWidthProperty = GetPropertyPath(p => p.DisplayWidth);
        public static readonly PropertyPath DisplayDepthProperty = GetPropertyPath(p => p.DisplayDepth);
        public static readonly PropertyPath AlternateHeightProperty = GetPropertyPath(p => p.AlternateHeight);
        public static readonly PropertyPath AlternateWidthProperty = GetPropertyPath(p => p.AlternateWidth);
        public static readonly PropertyPath AlternateDepthProperty = GetPropertyPath(p => p.AlternateDepth);
        public static readonly PropertyPath PointOfPurchaseHeightProperty = GetPropertyPath(p => p.PointOfPurchaseHeight);
        public static readonly PropertyPath PointOfPurchaseWidthProperty = GetPropertyPath(p => p.PointOfPurchaseWidth);
        public static readonly PropertyPath PointOfPurchaseDepthProperty = GetPropertyPath(p => p.PointOfPurchaseDepth);
        public static readonly PropertyPath SqueezeHeightProperty = GetPropertyPath(p => p.SqueezeHeight);
        public static readonly PropertyPath SqueezeWidthProperty = GetPropertyPath(p => p.SqueezeWidth);
        public static readonly PropertyPath SqueezeDepthProperty = GetPropertyPath(p => p.SqueezeDepth);
        public static readonly PropertyPath SqueezeHeightActualProperty = GetPropertyPath(p => p.SqueezeHeightActual);
        public static readonly PropertyPath SqueezeWidthActualProperty = GetPropertyPath(p => p.SqueezeWidthActual);
        public static readonly PropertyPath SqueezeDepthActualProperty = GetPropertyPath(p => p.SqueezeDepthActual);
        public static readonly PropertyPath CaseHeightProperty = GetPropertyPath(p => p.CaseHeight);
        public static readonly PropertyPath CaseWidthProperty = GetPropertyPath(p => p.CaseWidth);
        public static readonly PropertyPath CaseDepthProperty = GetPropertyPath(p => p.CaseDepth);
        public static readonly PropertyPath NumberOfPegHolesProperty = GetPropertyPath(p => p.NumberOfPegHoles);
        public static readonly PropertyPath PegXProperty = GetPropertyPath(p => p.PegX);
        public static readonly PropertyPath PegX2Property = GetPropertyPath(p => p.PegX2);
        public static readonly PropertyPath PegX3Property = GetPropertyPath(p => p.PegX3);
        public static readonly PropertyPath PegYProperty = GetPropertyPath(p => p.PegY);
        public static readonly PropertyPath PegY2Property = GetPropertyPath(p => p.PegY2);
        public static readonly PropertyPath PegY3Property = GetPropertyPath(p => p.PegY3);
        public static readonly PropertyPath PegProngOffsetXProperty = GetPropertyPath(p => p.PegProngOffsetX);
        public static readonly PropertyPath PegProngOffsetYProperty = GetPropertyPath(p => p.PegProngOffsetY);
        public static readonly PropertyPath PegDepthProperty = GetPropertyPath(p => p.PegDepth);
        public static readonly PropertyPath NestingHeightProperty = GetPropertyPath(p => p.NestingHeight);
        public static readonly PropertyPath NestingWidthProperty = GetPropertyPath(p => p.NestingWidth);
        public static readonly PropertyPath NestingDepthProperty = GetPropertyPath(p => p.NestingDepth);
        public static readonly PropertyPath CasePackUnitsProperty = GetPropertyPath(p => p.CasePackUnits);
        public static readonly PropertyPath CaseHighProperty = GetPropertyPath(p => p.CaseHigh);
        public static readonly PropertyPath CaseWideProperty = GetPropertyPath(p => p.CaseWide);
        public static readonly PropertyPath CaseDeepProperty = GetPropertyPath(p => p.CaseDeep);
        public static readonly PropertyPath MaxStackProperty = GetPropertyPath(p => p.MaxStack);
        public static readonly PropertyPath MaxTopCapProperty = GetPropertyPath(p => p.MaxTopCap);
        public static readonly PropertyPath MaxRightCapProperty = GetPropertyPath(p => p.MaxRightCap);
        public static readonly PropertyPath MinDeepProperty = GetPropertyPath(p => p.MinDeep);
        public static readonly PropertyPath MaxDeepProperty = GetPropertyPath(p => p.MaxDeep);
        public static readonly PropertyPath FrontOverhangProperty = GetPropertyPath(p => p.FrontOverhang);
        public static readonly PropertyPath FingerSpaceAboveProperty = GetPropertyPath(p => p.FingerSpaceAbove);
        public static readonly PropertyPath FingerSpaceToTheSideProperty = GetPropertyPath(p => p.FingerSpaceToTheSide);
        public static readonly PropertyPath StatusTypeProperty = GetPropertyPath(p => p.StatusType);
        public static readonly PropertyPath OrientationTypeProperty = GetPropertyPath(p => p.OrientationType);
        public static readonly PropertyPath MerchandisingStyleProperty = GetPropertyPath(p => p.MerchandisingStyle);
        public static readonly PropertyPath IsFrontOnlyProperty = GetPropertyPath(p => p.IsFrontOnly);
        public static readonly PropertyPath IsPlaceHolderProductProperty = GetPropertyPath(p => p.IsPlaceHolderProduct);
        public static readonly PropertyPath IsActiveProperty = GetPropertyPath(p => p.IsActive);
        public static readonly PropertyPath CanBreakTrayUpProperty = GetPropertyPath(p => p.CanBreakTrayUp);
        public static readonly PropertyPath CanBreakTrayDownProperty = GetPropertyPath(p => p.CanBreakTrayDown);
        public static readonly PropertyPath CanBreakTrayBackProperty = GetPropertyPath(p => p.CanBreakTrayBack);
        public static readonly PropertyPath CanBreakTrayTopProperty = GetPropertyPath(p => p.CanBreakTrayTop);
        public static readonly PropertyPath ForceMiddleCapProperty = GetPropertyPath(p => p.ForceMiddleCap);
        public static readonly PropertyPath ForceBottomCapProperty = GetPropertyPath(p => p.ForceBottomCap);
        public static readonly PropertyPath ShapeTypeProperty = GetPropertyPath(p => p.ShapeType);
        public static readonly PropertyPath FillPatternTypeProperty = GetPropertyPath(p => p.FillPatternType);
        public static readonly PropertyPath FillColourProperty = GetPropertyPath(p => p.FillColour);
        public static readonly PropertyPath ShapeProperty = GetPropertyPath(p => p.Shape);
        public static readonly PropertyPath PointOfPurchaseDescriptionProperty = GetPropertyPath(p => p.PointOfPurchaseDescription);
        public static readonly PropertyPath ShortDescriptionProperty = GetPropertyPath(p => p.ShortDescription);
        public static readonly PropertyPath SubcategoryProperty = GetPropertyPath(p => p.Subcategory);
        public static readonly PropertyPath CustomerStatusProperty = GetPropertyPath(p => p.CustomerStatus);
        public static readonly PropertyPath ColourProperty = GetPropertyPath(p => p.Colour);
        public static readonly PropertyPath FlavourProperty = GetPropertyPath(p => p.Flavour);
        public static readonly PropertyPath PackagingShapeProperty = GetPropertyPath(p => p.PackagingShape);
        public static readonly PropertyPath PackagingTypeProperty = GetPropertyPath(p => p.PackagingType);
        public static readonly PropertyPath CountryOfOriginProperty = GetPropertyPath(p => p.CountryOfOrigin);
        public static readonly PropertyPath CountryOfProcessingProperty = GetPropertyPath(p => p.CountryOfProcessing);
        public static readonly PropertyPath ShelfLifeProperty = GetPropertyPath(p => p.ShelfLife);
        public static readonly PropertyPath DeliveryFrequencyDaysProperty = GetPropertyPath(p => p.DeliveryFrequencyDays);
        public static readonly PropertyPath DeliveryMethodProperty = GetPropertyPath(p => p.DeliveryMethod);
        public static readonly PropertyPath VendorCodeProperty = GetPropertyPath(p => p.VendorCode);
        public static readonly PropertyPath VendorProperty = GetPropertyPath(p => p.Vendor);
        public static readonly PropertyPath ManufacturerCodeProperty = GetPropertyPath(p => p.ManufacturerCode);
        public static readonly PropertyPath ManufacturerProperty = GetPropertyPath(p => p.Manufacturer);
        public static readonly PropertyPath SizeProperty = GetPropertyPath(p => p.Size);
        public static readonly PropertyPath UnitOfMeasureProperty = GetPropertyPath(p => p.UnitOfMeasure);
        public static readonly PropertyPath DateIntroducedProperty = GetPropertyPath(p => p.DateIntroduced);
        public static readonly PropertyPath DateDiscontinuedProperty = GetPropertyPath(p => p.DateDiscontinued);
        public static readonly PropertyPath DateEffectiveProperty = GetPropertyPath(p => p.DateEffective);
        public static readonly PropertyPath HealthProperty = GetPropertyPath(p => p.Health);
        public static readonly PropertyPath CorporateCodeProperty = GetPropertyPath(p => p.CorporateCode);
        public static readonly PropertyPath BarcodeProperty = GetPropertyPath(p => p.Barcode);
        public static readonly PropertyPath SellPriceProperty = GetPropertyPath(p => p.SellPrice);
        public static readonly PropertyPath SellPackCountProperty = GetPropertyPath(p => p.SellPackCount);
        public static readonly PropertyPath SellPackDescriptionProperty = GetPropertyPath(p => p.SellPackDescription);
        public static readonly PropertyPath RecommendedRetailPriceProperty = GetPropertyPath(p => p.RecommendedRetailPrice);
        public static readonly PropertyPath ManufacturerRecommendedRetailPriceProperty = GetPropertyPath(p => p.ManufacturerRecommendedRetailPrice);
        public static readonly PropertyPath CostPriceProperty = GetPropertyPath(p => p.CostPrice);
        public static readonly PropertyPath CaseCostProperty = GetPropertyPath(p => p.CaseCost);
        public static readonly PropertyPath TaxRateProperty = GetPropertyPath(p => p.TaxRate);
        public static readonly PropertyPath ConsumerInformationProperty = GetPropertyPath(p => p.ConsumerInformation);
        public static readonly PropertyPath TextureProperty = GetPropertyPath(p => p.Texture);
        public static readonly PropertyPath StyleNumberProperty = GetPropertyPath(p => p.StyleNumber);
        public static readonly PropertyPath PatternProperty = GetPropertyPath(p => p.Pattern);
        public static readonly PropertyPath ModelProperty = GetPropertyPath(p => p.Model);
        public static readonly PropertyPath GarmentTypeProperty = GetPropertyPath(p => p.GarmentType);
        public static readonly PropertyPath IsPrivateLabelProperty = GetPropertyPath(p => p.IsPrivateLabel);
        public static readonly PropertyPath IsNewProductProperty = GetPropertyPath(p => p.IsNewProduct);
        public static readonly PropertyPath FinancialGroupCodeProperty = GetPropertyPath(p => p.FinancialGroupCode);
        public static readonly PropertyPath FinancialGroupNameProperty = GetPropertyPath(p => p.FinancialGroupName);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets whether changes should be logged as undoable actions.
        /// Note - this requires that a plan was specified on create.
        /// </summary>
        public Boolean IsLoggingUndoableActions
        {
            get { return _plan != null && _isLoggingUndoableActions; }
            set { _isLoggingUndoableActions = value; }
        }

        public IEnumerable<PlanogramProductView> Items
        {
            get
            {
                return _items;
            }
        }

        public Int32 Count
        {
            get { return Items.Count(); }
        }

        public PlanogramProductView this[Int32 i]
        {
            get { return _items.ElementAt(i); }
        }

        public Boolean IsValid
        {
            get { return _isValid; }            
        }


        #region Product Properties
        [ModelObjectDisplayName(typeof(PlanogramProduct), "GtinProperty")]
        public String Gtin
        {
            get
            {
                if (!this.Items.Any())
                {
                    return String.Empty;
                }
                else if (this.Items.Count() == 1)
                {
                    return this.Items.First().Gtin;
                }
                else
                {
                    return "<Multiple>";
                }
            }
            set
            {
                if (this.Items.Count() == 1)
                {
                    if (value != null)
                    {
                        SetPropertyValue(Items, PlanogramProduct.GtinProperty.Name, value);
                    }
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "NameProperty")]
        public String Name
        {
            get
            {
                if (!this.Items.Any())
                {
                    return String.Empty;
                }
                else if (this.Items.Count() == 1)
                {
                    return this.Items.First().Name;
                }
                else
                {
                    return "<Multiple>";
                }
            }
            set
            {
                if (this.Items.Count() == 1)
                {
                    if (value != null)
                    {
                        SetPropertyValue(Items, PlanogramProduct.NameProperty.Name, value);
                    }
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "MerchandisingStyleProperty")]
        public PlanogramProductMerchandisingStyle? MerchandisingStyle
        {
            get
            {
                return GetCommonPropertyValue<PlanogramProductMerchandisingStyle?>(Items, PlanogramProduct.MerchandisingStyleProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.MerchandisingStyleProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "HeightProperty")]
        public Single? Height
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.HeightProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    if (!value.Value.Equals(0F))
                    {
                        SetPropertyValue(Items, PlanogramProduct.HeightProperty.Name, value.Value);
                    }
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "WidthProperty")]
        public Single? Width
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.WidthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    if (!value.Value.Equals(0F))
                    {
                        SetPropertyValue(Items, PlanogramProduct.WidthProperty.Name, value.Value);
                    }
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "DepthProperty")]
        public Single? Depth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.DepthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    if (!value.Value.Equals(0F))
                    {
                        SetPropertyValue(Items, PlanogramProduct.DepthProperty.Name, value.Value);
                    }
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "SqueezeHeightProperty")]
        public Single? SqueezeHeight
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.SqueezeHeightProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.SqueezeHeightProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "SqueezeHeightActualProperty")]
        public Single? SqueezeHeightActual
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.SqueezeHeightActualProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.SqueezeHeightActualProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "SqueezeWidthProperty")]
        public Single? SqueezeWidth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.SqueezeWidthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.SqueezeWidthProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "SqueezeWidthActualProperty")]
        public Single? SqueezeWidthActual
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.SqueezeWidthActualProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.SqueezeWidthActualProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "SqueezeDepthProperty")]
        public Single? SqueezeDepth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.SqueezeDepthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.SqueezeDepthProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "SqueezeDepthActualProperty")]
        public Single? SqueezeDepthActual
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.SqueezeDepthActualProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.SqueezeDepthActualProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "OrientationTypeProperty")]
        [PropertyEditorItemsSource(typeof(PropertyEditorItemsSource<PlanogramProductOrientationType>))]
        public PlanogramProductOrientationType? OrientationType
        {
            get
            {
                return GetCommonPropertyValue<PlanogramProductOrientationType?>(Items, PlanogramProduct.OrientationTypeProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.OrientationTypeProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "IsFrontOnlyProperty")]
        public Boolean? IsFrontOnly
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramProduct.IsFrontOnlyProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.IsFrontOnlyProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "IsPlaceHolderProductProperty")]
        public Boolean? IsPlaceHolderProduct
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramProduct.IsPlaceHolderProductProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.IsPlaceHolderProductProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "IsActiveProperty")]
        public Boolean? IsActive
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramProduct.IsActiveProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.IsActiveProperty.Name, value.Value);
                }
            }
        }

      

        [ModelObjectDisplayName(typeof(PlanogramProduct), "TrayHighProperty")]
        public Byte? TrayHigh
        {
            get
            {
                return GetCommonPropertyValue<Byte?>(Items, PlanogramProduct.TrayHighProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.TrayHighProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "TrayWideProperty")]
        public Byte? TrayWide
        {
            get
            {
                return GetCommonPropertyValue<Byte?>(Items, PlanogramProduct.TrayWideProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.TrayWideProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "TrayDeepProperty")]
        public Byte? TrayDeep
        {
            get
            {
                return GetCommonPropertyValue<Byte?>(Items, PlanogramProduct.TrayDeepProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.TrayDeepProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "TrayHeightProperty")]
        public Single? TrayHeight
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.TrayHeightProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.TrayHeightProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "TrayWidthProperty")]
        public Single? TrayWidth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.TrayWidthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.TrayWidthProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "TrayDepthProperty")]
        public Single? TrayDepth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.TrayDepthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.TrayDepthProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "TrayThickHeightProperty")]
        public Single? TrayThickHeight
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.TrayThickHeightProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.TrayThickHeightProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "TrayThickWidthProperty")]
        public Single? TrayThickWidth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.TrayThickWidthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.TrayThickWidthProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "TrayThickDepthProperty")]
        public Single? TrayThickDepth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.TrayThickDepthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.TrayThickDepthProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "CanBreakTrayUpProperty")]
        public Boolean? CanBreakTrayUp
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramProduct.CanBreakTrayUpProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.CanBreakTrayUpProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "CanBreakTrayDownProperty")]
        public Boolean? CanBreakTrayDown
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramProduct.CanBreakTrayDownProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.CanBreakTrayDownProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "CanBreakTrayBackProperty")]
        public Boolean? CanBreakTrayBack
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramProduct.CanBreakTrayBackProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.CanBreakTrayBackProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "CanBreakTrayTopProperty")]
        public Boolean? CanBreakTrayTop
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramProduct.CanBreakTrayTopProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.CanBreakTrayTopProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "TrayPackUnitsProperty")]
        public Int16? TrayPackUnits
        {
            get
            {
                return GetCommonPropertyValue<Int16?>(Items, PlanogramProduct.TrayPackUnitsProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.TrayPackUnitsProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "ForceMiddleCapProperty")]
        public Boolean? ForceMiddleCap
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramProduct.ForceMiddleCapProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.ForceMiddleCapProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "ForceBottomCapProperty")]
        public Boolean? ForceBottomCap
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramProduct.ForceBottomCapProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.ForceBottomCapProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "CasePackUnitsProperty")]
        public Int16? CasePackUnits
        {
            get
            {
                return GetCommonPropertyValue<Int16?>(Items, PlanogramProduct.CasePackUnitsProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.CasePackUnitsProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "CaseHighProperty")]
        public Byte? CaseHigh
        {
            get
            {
                return GetCommonPropertyValue<Byte?>(Items, PlanogramProduct.CaseHighProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.CaseHighProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "CaseWideProperty")]
        public Byte? CaseWide
        {
            get
            {
                return GetCommonPropertyValue<Byte?>(Items, PlanogramProduct.CaseWideProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.CaseWideProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "CaseDeepProperty")]
        public Byte? CaseDeep
        {
            get
            {
                return GetCommonPropertyValue<Byte?>(Items, PlanogramProduct.CaseDeepProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.CaseDeepProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "CaseHeightProperty")]
        public Single? CaseHeight
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.CaseHeightProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.CaseHeightProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "CaseWidthProperty")]
        public Single? CaseWidth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.CaseWidthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.CaseWidthProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "CaseDepthProperty")]
        public Single? CaseDepth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.CaseDepthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.CaseDepthProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "MaxStackProperty")]
        public Byte? MaxStack
        {
            get
            {
                return GetCommonPropertyValue<Byte?>(Items, PlanogramProduct.MaxStackProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.MaxStackProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "MaxTopCapProperty")]
        public Byte? MaxTopCap
        {
            get
            {
                return GetCommonPropertyValue<Byte?>(Items, PlanogramProduct.MaxTopCapProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.MaxTopCapProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "MaxRightCapProperty")]
        public Byte? MaxRightCap
        {
            get
            {
                return GetCommonPropertyValue<Byte?>(Items, PlanogramProduct.MaxRightCapProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.MaxRightCapProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "MinDeepProperty")]
        public Byte? MinDeep
        {
            get
            {
                return GetCommonPropertyValue<Byte?>(Items, PlanogramProduct.MinDeepProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.MinDeepProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "MaxDeepProperty")]
        public Byte? MaxDeep
        {
            get
            {
                return GetCommonPropertyValue<Byte?>(Items, PlanogramProduct.MaxDeepProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.MaxDeepProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "NumberOfPegHolesProperty")]
        public Byte? NumberOfPegHoles
        {
            get
            {
                return GetCommonPropertyValue<Byte?>(Items, PlanogramProduct.NumberOfPegHolesProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.NumberOfPegHolesProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "PegXProperty")]
        public Single? PegX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.PegXProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.PegXProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "PegX2Property")]
        public Single? PegX2
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.PegX2Property.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.PegX2Property.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "PegX3Property")]
        public Single? PegX3
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.PegX3Property.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.PegX3Property.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "PegYProperty")]
        public Single? PegY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.PegYProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.PegYProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "PegY2Property")]
        public Single? PegY2
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.PegY2Property.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.PegY2Property.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "PegY3Property")]
        public Single? PegY3
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.PegY3Property.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.PegY3Property.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "PegProngOffsetXProperty")]
        public Single? PegProngOffsetX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.PegProngOffsetXProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.PegProngOffsetXProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "PegProngOffsetYProperty")]
        public Single? PegProngOffsetY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.PegProngOffsetYProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.PegProngOffsetYProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "PegDepthProperty")]
        public Single? PegDepth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.PegDepthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.PegDepthProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "NestingHeightProperty")]
        public Single? NestingHeight
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.NestingHeightProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.NestingHeightProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "NestingWidthProperty")]
        public Single? NestingWidth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.NestingWidthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.NestingWidthProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "NestingDepthProperty")]
        public Single? NestingDepth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.NestingDepthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.NestingDepthProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "FrontOverhangProperty")]
        public Single? FrontOverhang
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.FrontOverhangProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.FrontOverhangProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "FingerSpaceAboveProperty")]
        public Single? FingerSpaceAbove
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.FingerSpaceAboveProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.FingerSpaceAboveProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "FingerSpaceToTheSideProperty")]
        public Single? FingerSpaceToTheSide
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.FingerSpaceToTheSideProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.FingerSpaceToTheSideProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "DisplayHeightProperty")]
        public Single? DisplayHeight
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.DisplayHeightProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.DisplayHeightProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "DisplayWidthProperty")]
        public Single? DisplayWidth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.DisplayWidthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.DisplayWidthProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "DisplayDepthProperty")]
        public Single? DisplayDepth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.DisplayDepthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.DisplayDepthProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "AlternateHeightProperty")]
        public Single? AlternateHeight
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.AlternateHeightProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.AlternateHeightProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "AlternateWidthProperty")]
        public Single? AlternateWidth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.AlternateWidthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.AlternateWidthProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "AlternateDepthProperty")]
        public Single? AlternateDepth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.AlternateDepthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.AlternateDepthProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "PointOfPurchaseHeightProperty")]
        public Single? PointOfPurchaseHeight
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.PointOfPurchaseHeightProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.PointOfPurchaseHeightProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "PointOfPurchaseWidthProperty")]
        public Single? PointOfPurchaseWidth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.PointOfPurchaseWidthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.PointOfPurchaseWidthProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "PointOfPurchaseDepthProperty")]
        public Single? PointOfPurchaseDepth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.PointOfPurchaseDepthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.PointOfPurchaseDepthProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "StatusTypeProperty")]
        [PropertyEditorItemsSource(typeof(PropertyEditorItemsSource<PlanogramProductStatusType>))]
        public PlanogramProductStatusType? StatusType
        {
            get
            {
                return GetCommonPropertyValue<PlanogramProductStatusType?>(Items, PlanogramProduct.StatusTypeProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramProduct.StatusTypeProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "ShapeTypeProperty")]
        public PlanogramProductShapeType? ShapeType
        {
            get
            {
                return GetCommonPropertyValue<PlanogramProductShapeType?>(Items, PlanogramProduct.ShapeTypeProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.ShapeTypeProperty.Name, value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "FillPatternTypeProperty")]
        public PlanogramProductFillPatternType? FillPatternType
        {
            get { return GetCommonPropertyValue<PlanogramProductFillPatternType?>(Items, PlanogramProduct.FillPatternTypeProperty.Name); }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.FillPatternTypeProperty.Name, value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "FillColourProperty")]
        public Color FillColour
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, PlanogramProduct.FillColourProperty.Name);

                if (val.HasValue)
                {
                    return CommonHelper.IntToColor(val.Value);
                }
                else
                {
                    return Colors.White;
                }
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.FillColourProperty.Name, CommonHelper.ColorToInt(value));
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "BrandProperty")]
        public String Brand
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.BrandProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.BrandProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "ShapeProperty")]
        public String Shape
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.ShapeProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.ShapeProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "PointOfPurchaseDescriptionProperty")]
        public String PointOfPurchaseDescription
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.PointOfPurchaseDescriptionProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.PointOfPurchaseDescriptionProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "ShortDescriptionProperty")]
        public String ShortDescription
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.ShortDescriptionProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.ShortDescriptionProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "SubcategoryProperty")]
        public String Subcategory
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.SubcategoryProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.SubcategoryProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "CustomerStatusProperty")]
        public String CustomerStatus
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.CustomerStatusProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.CustomerStatusProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "ColourProperty")]
        public String Colour
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.ColourProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.ColourProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "FlavourProperty")]
        public String Flavour
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.FlavourProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.FlavourProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "PackagingShapeProperty")]
        public String PackagingShape
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.PackagingShapeProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.PackagingShapeProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "PackagingTypeProperty")]
        public String PackagingType
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.PackagingTypeProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.PackagingTypeProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "CountryOfOriginProperty")]
        public String CountryOfOrigin
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.CountryOfOriginProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.CountryOfOriginProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "CountryOfProcessingProperty")]
        public String CountryOfProcessing
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.CountryOfProcessingProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.CountryOfProcessingProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "ShelfLifeProperty")]
        public Int16? ShelfLife
        {
            get
            {
                return GetCommonPropertyValue<Int16?>(Items, PlanogramProduct.ShelfLifeProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.ShelfLifeProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "DeliveryFrequencyDaysProperty")]
        public Single? DeliveryFrequencyDays
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.DeliveryFrequencyDaysProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.DeliveryFrequencyDaysProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "DeliveryMethodProperty")]
        public String DeliveryMethod
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.DeliveryMethodProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.DeliveryMethodProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "VendorCodeProperty")]
        public String VendorCode
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.VendorCodeProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.VendorCodeProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "VendorProperty")]
        public String Vendor
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.VendorProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.VendorProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "ManufacturerCodeProperty")]
        public String ManufacturerCode
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.ManufacturerCodeProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.ManufacturerCodeProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "ManufacturerProperty")]
        public String Manufacturer
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.ManufacturerProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.ManufacturerProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "SizeProperty")]
        public String Size
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.SizeProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.SizeProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "UnitOfMeasureProperty")]
        public String UnitOfMeasure
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.UnitOfMeasureProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.UnitOfMeasureProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "DateIntroducedProperty")]
        public DateTime? DateIntroduced
        {
            get
            {
                return GetCommonPropertyValue<DateTime?>(Items, PlanogramProduct.DateIntroducedProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.DateIntroducedProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "DateDiscontinuedProperty")]
        public DateTime? DateDiscontinued
        {
            get
            {
                return GetCommonPropertyValue<DateTime?>(Items, PlanogramProduct.DateDiscontinuedProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.DateDiscontinuedProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "DateEffectiveProperty")]
        public DateTime? DateEffective
        {
            get
            {
                return GetCommonPropertyValue<DateTime?>(Items, PlanogramProduct.DateEffectiveProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.DateEffectiveProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "HealthProperty")]
        public String Health
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.HealthProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.HealthProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "CorporateCodeProperty")]
        public String CorporateCode
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.CorporateCodeProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.CorporateCodeProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "BarcodeProperty")]
        public String Barcode
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.BarcodeProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.BarcodeProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "SellPriceProperty")]
        public Single? SellPrice
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.SellPriceProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.SellPriceProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "SellPackCountProperty")]
        public Int16? SellPackCount
        {
            get
            {
                return GetCommonPropertyValue<Int16?>(Items, PlanogramProduct.SellPackCountProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.SellPackCountProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "SellPackDescriptionProperty")]
        public String SellPackDescription
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.SellPackDescriptionProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.SellPackDescriptionProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "RecommendedRetailPriceProperty")]
        public Single? RecommendedRetailPrice
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.RecommendedRetailPriceProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.RecommendedRetailPriceProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "ManufacturerRecommendedRetailPriceProperty")]
        public Single? ManufacturerRecommendedRetailPrice
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.ManufacturerRecommendedRetailPriceProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.ManufacturerRecommendedRetailPriceProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "CostPriceProperty")]
        public Single? CostPrice
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.CostPriceProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.CostPriceProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "CaseCostProperty")]
        public Single? CaseCost
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.CaseCostProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.CaseCostProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "TaxRateProperty")]
        public Single? TaxRate
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.TaxRateProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.TaxRateProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "ConsumerInformationProperty")]
        public String ConsumerInformation
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.ConsumerInformationProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.ConsumerInformationProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "TextureProperty")]
        public String Texture
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.TextureProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.TextureProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "StyleNumberProperty")]
        public Int16? StyleNumber
        {
            get
            {
                return GetCommonPropertyValue<Int16?>(Items, PlanogramProduct.StyleNumberProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.StyleNumberProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "PatternProperty")]
        public String Pattern
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.PatternProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.PatternProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "ModelProperty")]
        public String Model
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, "ModelString");
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, "ModelString", value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "GarmentTypeProperty")]
        public String GarmentType
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.GarmentTypeProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.GarmentTypeProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "IsPrivateLabelProperty")]
        public Boolean? IsPrivateLabel
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramProduct.IsPrivateLabelProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.IsPrivateLabelProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "IsNewProductProperty")]
        public Boolean? IsNewProduct
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramProduct.IsNewProductProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.IsNewProductProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "FinancialGroupCodeProperty")]
        public String FinancialGroupCode
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.FinancialGroupCodeProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.FinancialGroupCodeProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "FinancialGroupNameProperty")]
        public String FinancialGroupName
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramProduct.FinancialGroupNameProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramProduct.FinancialGroupNameProperty.Name, value);
                }
            }
        }

        #endregion

        #region Meta Data Properties

        [ModelObjectDisplayName(typeof(PlanogramProduct), "MetaNotAchievedInventoryProperty")]
        public Boolean? MetaNotAchievedInventory
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramProduct.MetaNotAchievedInventoryProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "MetaTotalUnitsProperty")]
        public Int32? MetaTotalUnits
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramProduct.MetaTotalUnitsProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "MetaPlanogramLinearSpacePercentageProperty")]
        public Single? MetaPlanogramLinearSpacePercentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.MetaPlanogramLinearSpacePercentageProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "MetaPlanogramAreaSpacePercentageProperty")]
        public Single? MetaPlanogramAreaSpacePercentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.MetaPlanogramAreaSpacePercentageProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "MetaPlanogramVolumetricSpacePercentageProperty")]
        public Single? MetaPlanogramVolumetricSpacePercentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramProduct.MetaPlanogramVolumetricSpacePercentageProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "MetaIsInMasterDataProperty")]
        public Boolean? MetaIsInMasterData
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramProduct.MetaIsInMasterDataProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "MetaTotalXFacingsProperty")]
        public Int32? MetaTotalXFacings
        {
            get { return GetCommonPropertyValue<Int32?>(Items, PlanogramProduct.MetaTotalXFacingsProperty.Name); }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "MetaTotalYFacingsProperty")]
        public Int32? MetaTotalYFacings
        {
            get { return GetCommonPropertyValue<Int32?>(Items, PlanogramProduct.MetaTotalYFacingsProperty.Name); }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "MetaTotalZFacingsProperty")]
        public Int32? MetaTotalZFacings
        {
            get { return GetCommonPropertyValue<Int32?>(Items, PlanogramProduct.MetaTotalZFacingsProperty.Name); }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "MetaIsRangedInAssortmentProperty")]
        public Boolean? MetaIsRangedInAssortment
        {
            get { return GetCommonPropertyValue<Boolean?>(Items, PlanogramProduct.MetaIsRangedInAssortmentProperty.Name); }
        }
            
        [ModelObjectDisplayName(typeof(PlanogramProduct), "MetaNotAchievedCasesProperty")]
        public Boolean? MetaNotAchievedCases
        {
            get { return GetCommonPropertyValue<Boolean?>(Items, PlanogramProduct.MetaNotAchievedCasesProperty.Name); }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "MetaNotAchievedDOSProperty")]
        public Boolean? MetaNotAchievedDOS
        {
            get { return GetCommonPropertyValue<Boolean?>(Items, PlanogramProduct.MetaNotAchievedDOSProperty.Name); }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "MetaIsOverShelfLifePercentProperty")]
        public Boolean? MetaIsOverShelfLifePercent
        {
            get { return GetCommonPropertyValue<Boolean?>(Items, PlanogramProduct.MetaIsOverShelfLifePercentProperty.Name); }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "MetaNotAchievedDeliveriesProperty")]
        public Boolean? MetaNotAchievedDeliveries
        {
            get { return GetCommonPropertyValue<Boolean?>(Items, PlanogramProduct.MetaNotAchievedDeliveriesProperty.Name); }
        }

        #endregion

        /// <summary>
        ///     Gets the <c>Custom Attribute Data Multi View</c> for the <c>Products</c> in this instance.
        /// </summary>
        /// <remarks>
        ///     [<c>LazyLoad</c>]The <c>Custom Attribute Data Multi View</c> is fetched the first time it is requested.
        /// </remarks>
        public CustomAttributeDataMultiView CustomAttributes
        {
            get
            {
                if (_customAttributesView != null) return _customAttributesView;
                SubscribeToPlanItemPropertyChanged(false);
                List<CustomAttributeData> customAttributes = _items.Select(i => i.Product.CustomAttributes).Where(i => i != null).ToList();
                SubscribeToPlanItemPropertyChanged();
                if (customAttributes.Count != 0)
                    _customAttributesView = new CustomAttributeDataMultiView(customAttributes, _plan);
                return _customAttributesView;
            }
        }

        /// <summary>
        ///     Gets the <c>Performance Data Multi View</c> for the <c>Products</c> in this instance.
        /// </summary>
        /// <remarks>
        ///     [<c>LazyLoad</c>]The <c>Performance Data Multi View</c> is fetched the first time it is requested.
        /// </remarks>
        public PlanogramPerformanceDataMultiView PerformanceData
        {
            get
            {
                if (_performanceDataView != null) return _performanceDataView;
                List<PlanogramPerformanceData> performanceItems = _items.Select(i => i.Product.PerformanceData).Where(i => i != null).ToList();
                if (performanceItems.Count != 0)
                    _performanceDataView = new PlanogramPerformanceDataMultiView(performanceItems, _plan);
                return _performanceDataView;
            }
        }
        
        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of <see cref="PlanogramProductMultiView"/>.
        /// </summary>
        /// <param name="items">The enumeration of <see cref="PlanogramProductView"/> to initialize this instance from.</param>
        /// <remarks>
        ///     After initializtion, the enumeration of <see cref="PlanogramProductView"/> is stored as a list internally.
        /// </remarks>
        public PlanogramProductMultiView(IEnumerable<PlanogramProductView> items)
        {
            _items = items.ToList();
            _plan = _items.First().Planogram;

            foreach (PlanogramProductView i in _items)
            {
                i.PropertyChanged += PlanItem_PropertyChanged;
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on any linked plan item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(e.PropertyName);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the common value of the given property for all the items
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        private static T GetCommonPropertyValue<T>(IEnumerable<Object> items, String propertyName)
        {
            return PlanItemHelper.GetCommonPropertyValue<T>(items, propertyName);
        }

        /// <summary>
        /// Sets the given value against
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        private void SetPropertyValue<T>(IEnumerable<Object> items, String propertyName, T value)
        {
            PlanItemHelper.SetPropertyValue<T>(items, propertyName, value, IsLoggingUndoableActions, _plan);
        }
        
        /// <summary>
        /// Method to handle subscribing to the property changed event
        /// </summary>
        /// <param name="attach"></param>
        private void SubscribeToPlanItemPropertyChanged(Boolean attach = true)
        {
            //Ensure any hooks are detached
            foreach (PlanogramProductView i in _items)
            {
                i.PropertyChanged -= PlanItem_PropertyChanged;
            }

            //Attach new hooks if required.
            if (attach)
            {
                foreach (PlanogramProductView i in _items)
                {
                    i.PropertyChanged += PlanItem_PropertyChanged;
                }
            }
        }

        #endregion

        #region IDataErrorInfo Members

        public String Error
        {
            get { return ""; }
        }

        public String this[String columnName]
        {
            get
            {
                if (columnName == GtinProperty.Path)
                {
                    if (_items.Count() == 1)
                    {
                        if (_plan.Products.Where(p => p.Gtin == _items.First().Gtin && !p.Model.Id.Equals(_items.First().Model.Id)).Any())
                        {
                            _isValid = false;
                            return Message.PlanogramProductMultiView_DuplicateGtin;
                        }
                        _isValid = true;
                    }
                }

                return ((IDataErrorInfo)_items.First())[columnName];
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                foreach (var i in _items)
                {
                    i.PropertyChanged -= PlanItem_PropertyChanged;
                }
                base.IsDisposed = true;
            }
        }

        #endregion

        #region Static Helpers

        private static PropertyPath GetPropertyPath(System.Linq.Expressions.Expression<Func<PlanogramProductMultiView, Object>> expression)
        {
            return WpfHelper.GetPropertyPath<PlanogramProductMultiView>(expression);
        }

        #endregion

    }
}