﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
// V8-25734 : L.Ineson
//  Moved out rendering controls to framework.
// V8-27058 : A.Probyn
//  Updated references to updated meta data properties on PlanogramFixture
//  Removed references to MetaNumberOfShelfEdgeLabels
// V8-26182 : L.Ineson
//  Moved new plan methods to model.
// V8-26384 : A.Probyn
//  Updated CreateFixtureThumbnail to have no rotation and show 2d front view.
#endregion
#region Version History: (CCM CCM820)
// V8-31280  : J.Pickup
//  Adding fixture library coponents now includes the image saved against the fixture Also Refactored some code to make things better. 
#endregion

#region Version History: (CCM 8.3.0)
//V8-32600 : L.Ineson
//  Moved static methods into new fixture library helper.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// ViewModel helper for the FixturePackage model object
    /// </summary>
    public sealed class FixturePackageViewModel : ViewStateObject<FixturePackage>, IDisposable
    {
        #region Constructor

        public FixturePackageViewModel()
        {
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the model changed event is to be raised.
        /// </summary>
        /// <param name="e"></param>
        protected override void RaiseModelChangedEvent(ViewStateObjectModelChangedEventArgs<FixturePackage> e)
        {
            base.RaiseModelChangedEvent(e);

            //dispose of the old model so that it gets unlocked
            if (e.OldModel != null)
            {
                e.OldModel.Dispose();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates a new fixture package from the given plan item.
        /// </summary>
        /// <param name="planItem"></param>
        public void CreateNew(IPlanItem planItem)
        {
            SetModel(ViewStateObjectModelChangeReason.Create,
                FixtureLibraryHelper.CreateFixturePackage(new List<IPlanItem> { planItem }), null);
        }

        /// <summary>
        /// Creates a new fixture package from the given plan items.
        /// </summary>
        /// <param name="planItems"></param>
        public void CreateNew(IEnumerable<IPlanItem> planItems)
        {
            SetModel(ViewStateObjectModelChangeReason.Create, FixtureLibraryHelper.CreateFixturePackage(planItems), null);
        }

        /// <summary>
        /// Fetches the fixture package for the given info.
        /// </summary>
        /// <param name="info"></param>
        public void Fetch(FixturePackageInfo info)
        {
            switch (info.PackageType)
            {
                case FixturePackageType.FileSystem:
                    FetchFromFile((String)info.Id);
                    break;

                default:
                case FixturePackageType.Unknown:
                    Fetch(new FixturePackage.FetchCriteria(FixturePackageType.Unknown, info.Id));
                    break;
            }
        }

        /// <summary>
        /// Fetches the fixture package for the given filepath.
        /// </summary>
        public void FetchFromFile(String filePath)
        {
            Fetch(new FixturePackage.FetchCriteria(FixturePackageType.FileSystem, filePath));
        }

        /// <summary>
        /// Saves the current model as a new file.
        /// </summary>
        /// <param name="filePath"></param>
        public void SaveAsFile(String filePath)
        {
            if (this.Model == null)
            {
                throw new ArgumentOutOfRangeException("Model");
            }

            SetModel(ViewStateObjectModelChangeReason.Update, this.Model.SaveAs(filePath), null);
        }

        /// <summary>
        /// Returns the default directory for the FileSystem package type.
        /// </summary>
        public String GetFileSystemDirectory()
        {
            return App.ViewState.GetSessionDirectory(SessionDirectory.FixtureLibrary);
        }

        /// <summary>
        /// Returns the file system path for the current model.
        /// </summary>
        /// <returns></returns>
        public String GetModelFileSystemPath()
        {
            return GetModelFileSystemPath(GetFileSystemDirectory());
        }

        /// <summary>
        /// Returns the file system path for the current model.
        /// </summary>
        /// <returns></returns>
        public String GetModelFileSystemPath(String folderPath)
        {
            if (String.IsNullOrEmpty(this.Model.Name))
            {
                throw new ArgumentNullException("Name");
            }

            if (this.Model != null)
            {
                if (this.Model.IsNew)
                {
                    String packagePath = Path.Combine(folderPath, this.Model.Name);
                    packagePath = Path.ChangeExtension(packagePath, FixturePackage.FileExtension);

                    return packagePath;
                }
                else
                {
                    return (String)this.Model.Id;
                }
            }
            return String.Empty;
        }

        /// <summary>
        /// Adds the current package model to the given planogram.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="nearestFixture"></param>
        public List<IPlanItem> AddToPlanogram(PlanogramView plan, PlanogramFixtureView nearestFixture)
        {
            return FixtureLibraryHelper.AddToPlanogram(this.Model, plan, nearestFixture);
        }      

        #endregion

        #region IDisposable

        public void Dispose()
        {
            if (this.Model != null)
            {
                FixturePackage m = this.Model;
                this.Model = null;
                m.Dispose();
            }
        }

        #endregion
    }

}
