﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History : CCM800

// L.Hodson
//  Created
// V8-24971 : L.Hodson
//  Changes to position structure
// V8-25734 : L.Ineson
//  Moved out rendering controls to framework.
// V8-26569 : L.Ineson
//  Added world space coordinates
//V8-26787 : L.Ineson
//  Changes to planitemfield
// V8-27570 : A.Silva
//      Added PositionSequenceNumber.
// V8-25231 : L.Ineson
//  Added methods for FillHigh, FillWide and FillDeep
// V8-28433 : A.Kuszyk
//  Re-factored FillHigh to take account of HangBelow subcomponents
// V8-28537 : L.Ineson
// Amended fill high, wide, deep to use collides with methods.

#endregion

#region Version History : CCM801

// V8-28837 : A.Kuszyk
//  Re-factored FillHigh/Wide/Deep methods into GetFillableSpace and added position collision to this method.
// V8-28801 : A.Kuszyk
//  Amended FillDeep to handle products with a peg depth of 0.

#endregion

#region Version History : CCM802

// CCM-28766 : J.Pickup
//  IsManuallyPlaced property introuced

#endregion

#region Version History : CCM810

// V8-29982 : A.Kuszyk
//  GetFillableSize adjusted to account for changes to Merchandising Space and Merchandising Group.
// V8-30103 : N.Foster
//  Autofill performance enhancements

#endregion

#region Version History : CCM 811

// V8-30334 : A.Silva
//  Updated usage of GetWorldMerchandisingSpaceBounds in GetFillableSpace 
//      to provide a prefeched collection of sub component placements for the planogram.
// V8-28856 : A.Silva
//  Removed obsolete code having to do with GetFillablespace.

#endregion

#region Version History : CCM 820
// V8-31368 : L.Ineson
//  Made sure that image properties do not cause the plan to be reprocessed.
#endregion

#region Version History: (CCM 8.3)
//V8-31832 : L.Ineson
// Implemented ICalculatedValueResolver
// V8-32327 : A.Silva
//  Added Debug Display info.

#endregion

#endregion

using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Planograms.ViewModel;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Provides a view of a planogram position.
    /// </summary>
    [DebuggerDisplay("{DebugDisplay()}")]
    public sealed class PlanogramPositionView : PlanogramPositionViewModelBase, IPlanItem, IPlanPositionRenderable
    {
        #region Fields
        private readonly PlanogramProductView _planogramProductView;
        private readonly PlanogramSubComponentView _parentSubComponent;
        private CalculatedValueResolver _calculatedValueResolver;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath PlanogramProductProperty = WpfHelper.GetPropertyPath<PlanogramPositionView>(p => p.Product);
        public static readonly PropertyPath HeightProperty = WpfHelper.GetPropertyPath<PlanogramPositionView>(p => p.Height);
        public static readonly PropertyPath WidthProperty = WpfHelper.GetPropertyPath<PlanogramPositionView>(p => p.Width);
        public static readonly PropertyPath DepthProperty = WpfHelper.GetPropertyPath<PlanogramPositionView>(p => p.Depth);
        //public static readonly PropertyPath WorldXProperty = WpfHelper.GetPropertyPath<PlanogramPositionView>(p => p.MetaWorldX);
        //public static readonly PropertyPath WorldYProperty = WpfHelper.GetPropertyPath<PlanogramPositionView>(p => p.MetaWorldY);
        //public static readonly PropertyPath WorldZProperty = WpfHelper.GetPropertyPath<PlanogramPositionView>(p => p.MetaWorldZ);

        public static readonly PropertyPath PositionSequenceNumberProperty = WpfHelper.GetPropertyPath<PlanogramPositionView>(p => p.PositionSequenceNumber);
        public static readonly PropertyPath IsManuallyPlacedProperty = WpfHelper.GetPropertyPath<PlanogramPositionView>(p => p.IsManuallyPlaced);

        #endregion

        #region Properties

        /// <summary>
        /// Gets the related product view.
        /// </summary>
        public PlanogramProductView Product
        {
            get { return _planogramProductView; }
        }

        /// <summary>
        /// Returns the resolver used for binding to calculated values.
        /// </summary>
        public CalculatedValueResolver CalculatedValueResolver
        {
            get
            {
                if (_calculatedValueResolver == null)
                {
                    _calculatedValueResolver = 
                        new CalculatedValueResolver(GetCalculatedValue, /*enableCaching*/true);
                }
                return _calculatedValueResolver;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="parentSubComponent"></param>
        /// <param name="position"></param>
        /// <param name="productView"></param>
        public PlanogramPositionView(PlanogramSubComponentView parentSubComponent,
            PlanogramPosition position, PlanogramProductView productView)
            : base(parentSubComponent, position)
        {
            _parentSubComponent = parentSubComponent;
            _planogramProductView = productView;

            this.IsSelectable = true;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Called by the parent planogram to notify that processing has completed
        /// </summary>
        internal void NotifyPlanProcessCompleting()
        {
            //if we have calculated values cached then update them now.
            if (_calculatedValueResolver != null) _calculatedValueResolver.RefreshValues();
        }

        /// <summary>
        /// Moves this position up to the next level of inventory
        /// </summary>
        /// <returns>True if successful, else false</returns>
        public Boolean IncreaseFacings()
        {
            return this.Model.IncreaseFacings(this.Product.Model, this.SubComponent.ModelPlacement);
        }

        /// <summary>
        /// Moves this position down to the next level of inventory
        /// </summary>
        public Boolean DecreaseFacings()
        {
            return this.Model.DecreaseFacings(this.Product.Model, this.SubComponent.ModelPlacement);
        }

        /// <summary>
        /// Adjusts the position X cap facing by 1 wide
        /// Used by keyboard shortcuts
        /// </summary>
        /// <param name="isAdjustRight">The adjust direction is right</param>
        public void AdjustXCapWide(Boolean isAdjustRight)
        {
            PlanogramPosition.AdjustXCapWide(
                this.Model,
                this.Model.GetPositionDetails(this.Product.Model, this.SubComponent.ModelPlacement), 
                isAdjustRight);
        }

        /// <summary>
        /// Adjusts the position Y cap facing by 1 high
        /// Used by keyboard shortcuts
        /// </summary>
        /// <param name="isAdjustUp">The adjust direction is up</param>
        public void AdjustYCapHigh(Boolean isAdjustUp)
        {
            PlanogramPosition.AdjustYCapHigh(
                this.Model, this.Model.GetPositionDetails(this.Product.Model, this.SubComponent.ModelPlacement),
                isAdjustUp);
        }

        /// <summary>
        /// Returns the text to be displayed by the position label based on
        /// the given format string.
        /// </summary>
        /// <param name="formatString"></param>
        /// <returns></returns>
        public override String GetLabelText(String formatString)
        {
            return Galleria.Framework.Planograms.Helpers.PlanogramFieldHelper.ResolvePositionLabel(
                formatString, this.Model, this.Product.Model, this.SubComponent.ModelPlacement);
        }

        /// <summary>
        /// Enumerates through the default fields to show to the user for this type.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDefaultPlanItemFields()
        {
            Type ownerType = typeof(PlanogramPosition);
            String ownerFriendlyName = PlanItemHelper.FriendlyNames[PlanItemType.Position];

            yield return ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName, PlanogramPosition.FacingsHighProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName, PlanogramPosition.FacingsWideProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName, PlanogramPosition.FacingsDeepProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName, PlanogramPosition.OrientationTypeProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName, PlanogramPosition.MerchandisingStyleProperty);

            yield return ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName, PlanogramPosition.UnitsHighProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName, PlanogramPosition.UnitsWideProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName, PlanogramPosition.UnitsDeepProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName, PlanogramPosition.TotalUnitsProperty);
        }


        /// <summary>
        /// Resolves the given expression for this item
        /// </summary>
        public Object GetCalculatedValue(String expressionText)
        {
            return PlanogramFieldHelper.ResolveFieldExpression(expressionText,
                PlanogramItemPlacement.NewPlanogramItemPlacement(
                position:this.Model,
                product: this.Product.Model,
                subComponentPlacement: this.SubComponent.ModelPlacement
                ));
        }

         /// <summary>
        /// Really shoddy thing to do, but force the property change to fire on the ui thread.
        /// </summary>
        /// <param name="propertyName"></param>
        protected override void FirePropertyChangedOnAppDispatcher(string propertyName)
        {
            Application.Current.Dispatcher.Invoke(new Action(() => OnPropertyChanged(propertyName)));
        }

        #endregion

        #region IPlanogramPosition Members

        IPlanProductRenderable IPlanPositionRenderable.Product
        {
            get { return _planogramProductView; }
        }

        IPlanSubComponentRenderable IPlanPositionRenderable.ParentSubComponent
        {
            get { return _parentSubComponent; }
        }

        #endregion

        #region IPlanItem Members

        public PlanogramView Planogram
        {
            get
            {
                if (_parentSubComponent == null) return null;
                return _parentSubComponent.Planogram;
            }
        }

        public PlanogramFixtureView Fixture
        {
            get { return _parentSubComponent.Fixture; }
        }

        public PlanogramAssemblyView Assembly
        {
            get { return _parentSubComponent.Assembly; }
        }

        public PlanogramComponentView Component
        {
            get { return _parentSubComponent.Component; }
        }

        public PlanogramSubComponentView SubComponent
        {
            get { return _parentSubComponent; }
        }

        PlanogramPositionView IPlanItem.Position
        {
            get { return this; }
        }

        PlanogramAnnotationView IPlanItem.Annotation
        {
            get { return null; }
        }

        PlanItemType IPlanItem.PlanItemType
        {
            get { return PlanItemType.Position; }
        }

        public Boolean IsSelectable { get; set; }


        Single IPlanItem.Angle
        {
            get { return 0; }
            set { }
        }

        Single IPlanItem.Slope
        {
            get { return 0; }
            set { }
        }

        Single IPlanItem.Roll
        {
            get { return 0; }
            set { }
        }

        #endregion

        #region IDisposable

        protected override void OnDisposing(bool disposing)
        {
            if (_calculatedValueResolver != null) _calculatedValueResolver.Dispose();
            base.OnDisposing(disposing);
        }

        #endregion

        #region DebuggerDisplay Methods

        internal String DebugDisplay()
        {
            var builder = new StringBuilder();
            builder.Append("{ Gtin = ");
            builder.Append(Product != null ? Product.Gtin : "No Product!");
            builder.Append(" - Seq(");
            builder.Append(Fixture.BaySequenceNumber);
            builder.Append(", ");
            builder.Append(Component.ComponentSequenceNumber);
            builder.Append(", ");
            builder.Append(SequenceX);
            builder.Append(", ");
            builder.Append(SequenceY);
            builder.Append(", ");
            builder.Append(SequenceZ);
            builder.Append(")");
            builder.Append(" }");
            return builder.ToString();
        }


        #endregion
    }
}