﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Ineson
//  CCM-25653 : Created
// V8-27411 : M.Pettit
//  Ensure package fetched as a locked writeable object
// V8-27411 : M.Pettit
//  Package fetch crtieria now requires userId and locktype
// V8-27734 : L.Ineson
//  Added onsaving and made sure that plan views get dettached.
// V8-27625 : A.Silva
//  Added Planogram Renumbering to OnSaving if the setting is enabled.
// V8-27919 : L.Ineson
//  Updated ImportByFilename to accept import template.
// V8-28147 : D.Pleasance
//  Amended ImportByFileName so that lockUserId is provided.
// V8-28444 : M.Shelley
//  MarkGraphAsInitialized after the model is set
// V8-28552 : L.Ineson
//  Made sure that plan usernames get updated when saving.
#endregion
#region Version History: (CCM 8.0.1)
// V8-28622 : D.Pleasance
//  Added metric mapping to ImportByFileName
// V8-28609 : L.Ineson
//  Plans are now marked initialized after a save has completed to stop
// the plan changed warning showing.
#endregion
#region Version History: (CCM 8.0.3)
//V8-29622 : L.Ineson
//  Added ability to load an existing model for unit testing purposes.
// V8-29726 : L.Ineson
//  Made sure that plan now gets cleaned up before save.
#endregion
#region Version History: (CCM 8.1.0)
//V8-30279 : D.Pleasance
//  Added PlanogramGroupId \ PlanogramGroupName
#endregion
#region Version History: (CCM 8.1.1)
// V8-30522 : L.Luong
//  Added RepositoryFolderPath
#endregion
#region Version History: (CCM 8.2.0)
//V8-30193 : L.Ineson
//  Removed renumber on save as renumbering is now applied automatically.
// V8-30892 : A.Probyn
//  Called PlanogramView OnAddEventHooks & OnRemoveEventHooks during AutoSave to eliminate UI threading issues during the copy.
// V8-31152 : M.Brumby
//  External file import uses a context now
// V8-31093 : A.Silva
//  Modified ImportByFileName so that now both a packageId (source file) and a filename (planogram name) can be given to import just one planogram from a multi-planogram Package File.
// V8-31175 : A.Silva
//  Amended ImportByFileName so that the Package ID is no longer passed into it (now the file name should already include the name of the planogram to import if any.
//  AND change undone as it is no longer necessary.

#endregion
#region Version History: (CCM 8.3.0)
//V8-31546 : M.Pettit
//  Added SaveAsFileType to PackageSaveSettings
//  Added SaveAsExternalFile
//  Refactored CCM.Model.PlanogramFileType to Framework.Plangoram.Model and renamed PlanogramExportFileType
// V8-32019 : A.Kuszyk
//  Added should calculate metadata flag to improve performance of unit tests in some circumstances (disabled by default).
// V8-31546 : M.Brumby
//  Made file type save options context sensitive
//V8-31547 : M.Pettit
//  Added Export options to PackageSaveSettings
//V8-32277 : L.Ineson
//  Added IsValid
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;

using System.Globalization;
using System.IO;
using System.Linq;
using Csla;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Collections;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Planograms.External;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Common.Wpf.Helpers;


namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Denotes the types of package connections
    /// supported by the PackageViewModel
    /// </summary>
    public enum PackageConnectionType
    {
        None = 0,
        FileSystem = 1,
        Repository = 2
    }

    /// <summary>
    /// All boolean save as options
    /// </summary>
    public enum SaveOptionType
    {
        SaveAssortment,
        SaveBlocking,
        SaveCtd,
        SaveEventLogs,
        SaveImages,
        SavePerformances,
        SaveValidationTemplate
    }

    /// <summary>
    /// Presents a view of a PlanogramPackage model.
    /// </summary>
    public sealed class PackageViewModel : ViewStateObject<Package>, IDisposable
    {
        #region Constants

        /// <summary>
        /// The file extension to use when saving to the filesystem.
        /// </summary>
        public static readonly String FileSystemExtension = ".pog";

        #endregion

        #region Fields

        /// <summary>
        /// Flags whether or not metadata should be calculated on plan changes. True by default. This will almost
        /// always be true, apart from in some unit testing scenarios.
        /// </summary>
        private Boolean _shouldCalculateMetadata = true;
        private PackageConnectionType _packageType = PackageConnectionType.None;
        private String _fileName;
        private String _repositoryFolderPath;

        private BulkObservableCollection<PlanogramView> _planViews = new BulkObservableCollection<PlanogramView>();
        private ReadOnlyBulkObservableCollection<PlanogramView> _planViewsRO;

        private PackageSaveSettings _saveSettings;
        private Int32? _planogramGroupId;
        private String _planogramGroupName;

        #endregion

        #region Properties

        /// <summary>
        /// Returns the collection of planogram views for this package.
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramView> PlanogramViews
        {
            get { return _planViewsRO; }
        }

        /// <summary>
        /// Returns the type of package currently being held.
        /// </summary>
        public PackageConnectionType PackageConnectionType
        {
            get { return _packageType; }
        }


        /// <summary>
        /// Returns the name of this file this package saves to.
        /// </summary>
        public String FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        public new Package Model
        {
            get { return base.Model; }
            private set
            {
                base.Model = value;
            }
        }

        /// <summary>
        /// Returns the save settings applied to this package.
        /// </summary>
        public PackageSaveSettings SaveSettings
        {
            get
            {
                if (_saveSettings == null)
                {
                    _saveSettings = new PackageSaveSettings();
                }
                return _saveSettings;
            }
        }

        /// <summary>
        /// Get \ Set the planograms current group.
        /// </summary>
        public Int32? PlanogramGroupId
        {
            get { return _planogramGroupId; }
            set { _planogramGroupId = value; }
        }

        /// <summary>
        /// Get \ Set the planograms current group name.
        /// </summary>
        public String PlanogramGroupName
        {
            get { return _planogramGroupName; }
            set { _planogramGroupName = value; }
        }

        /// <summary>
        /// gets the Repository Folder Path
        /// </summary>
        public String RepositoryFolderPath
        {
            get { return _repositoryFolderPath; }
        }

        #endregion

        #region Events

        public event EventHandler OnSaveBeginning;

        public event EventHandler OnSaveCompleted;

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        private PackageViewModel()
        {
            _planViews.BulkCollectionChanged += PlanViews_BulkCollectionChanged;
            _planViewsRO = new ReadOnlyBulkObservableCollection<PlanogramView>(_planViews);
            this.ModelChanged += OnModelChanged;
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a viewmodel populated with a new package.
        /// </summary>
        /// <param name="createInitialPlanogram"></param>
        /// <returns></returns>
        public static PackageViewModel CreateNewPackage(Boolean createInitialPlanogram)
        {
            PackageViewModel viewModel = new PackageViewModel();
            viewModel.NewPackage(createInitialPlanogram, App.ViewState.Settings.Model);
            return viewModel;
        }

        /// <summary>
        /// Returns the viewmodel for the package with the given filename.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static PackageViewModel FetchPackageByFileName(String fileName)
        {
            PackageViewModel viewModel = new PackageViewModel();
            viewModel.FetchByFileName(fileName);
            return viewModel;
        }

        /// <summary>
        /// Returns a viewmodel populated with the package for the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static PackageViewModel FetchPackageById(Object id)
        {
            PackageViewModel viewModel = new PackageViewModel();
            viewModel.FetchById(id);
            return viewModel;
        }

        /// <summary>
        /// Returns the viewmodel for a package imported from the given
        /// external type filename.
        /// </summary>
        public static PackageViewModel ImportPackageByFileName(String fileName, PlanogramImportTemplate importTemplate)
        {
            PackageViewModel viewModel = new PackageViewModel();
            viewModel.ImportByFileName(fileName, importTemplate);
            return viewModel;
        }

        /// <summary>
        /// Returns a viewmodel for the given package model.
        /// </summary>
        public static PackageViewModel NewPackageViewModel(Package model, Boolean shouldCalculateMetadata = true)
        {
            PackageViewModel viewModel = new PackageViewModel();
            viewModel._shouldCalculateMetadata = shouldCalculateMetadata;
            viewModel.FetchByModel(model);
            return viewModel;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the plan views collection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanViews_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Remove:
                case NotifyCollectionChangedAction.Reset:
                    {
                        if (e.ChangedItems != null)
                        {
                            foreach (PlanogramView oldView in e.ChangedItems)
                            {
                                oldView.Dispose();
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Called whenever this model changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnModelChanged(object sender, ViewStateObjectModelChangedEventArgs<Package> e)
        {
            if (e.OldModel != null)
            {
                e.OldModel.Planograms.BulkCollectionChanged -= Planograms_BulkCollectionChanged;
            }

            if (e.NewModel != null)
            {
                if (e.Reason == ViewStateObjectModelChangeReason.Update
                    && e.NewModel.Planograms.Count == this.PlanogramViews.Count)
                {
                    for (Int32 i = 0; i < e.NewModel.Planograms.Count; i++)
                    {
                        this.PlanogramViews[i].UpdateView(e.NewModel.Planograms[i]);
                    }
                }
                else
                {
                    Planograms_BulkCollectionChanged(e.NewModel, new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));
                }
                e.NewModel.Planograms.BulkCollectionChanged += Planograms_BulkCollectionChanged;
            }
            else
            {
                _planViews.Clear();
            }
        }

        /// <summary>
        /// Called whenever the package model planograms collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Planograms_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (Planogram planModel in e.ChangedItems)
                    {
                        _planViews.Add(new PlanogramView(planModel, this));
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (Planogram planModel in e.ChangedItems)
                    {
                        PlanogramView planView = _planViews.FirstOrDefault(p => p.Model == planModel);
                        _planViews.Remove(planView);
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        List<PlanogramView> updatedViews = new List<PlanogramView>();

                        foreach (Planogram planModel in Model.Planograms)
                        {
                            PlanogramView view = this.PlanogramViews.FirstOrDefault(p => Object.Equals(p.Model.Id, planModel.Id));
                            if (view != null)
                            {
                                view.UpdateView(planModel);
                                updatedViews.Add(view);
                            }
                            else
                            {
                                view = new PlanogramView(planModel, this,_shouldCalculateMetadata);
                                _planViews.Add(view);
                                updatedViews.Add(view);
                            }
                        }

                        //remove old views.
                        _planViews.RemoveRange(_planViews.Except(updatedViews).ToList());

                    }
                    break;
            }
        }

        /// <summary>
        /// Called when this package is about to be saved.
        /// </summary>
        private void OnSaving(Boolean isSaveAs)
        {
            //if we are saving to file then first apply the save settings
            if (_packageType == Common.PackageConnectionType.FileSystem)
            {
                ApplySaveSettings();
            }


            foreach (PlanogramView plan in this.PlanogramViews)
            {
                //[V8-28552] force the plan user name to update.
                if(isSaveAs || plan.Model.IsNew || String.IsNullOrEmpty(plan.Model.UserName))
                {
                    plan.Model.UserName = ApplicationContext.User.Identity.Name;
                }


                //freeze the planogram.
                // As part of this all orphaned parts will be cleaned
                // and the model will force a full process update.
                plan.FreezePlan();
            }
            
            //Raise out the event to notify the controllers
            if (OnSaveBeginning != null)
            {
                OnSaveBeginning(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Carries out post save actions.
        /// </summary>
        private void OnSaved(Exception saveError)
        {
            //raise the notification event.
            if (OnSaveCompleted != null)
            {
                OnSaveCompleted(this, EventArgs.Empty);
            }

            if (saveError == null)
            {
                //mark the just saved plan model as clean so that it does not prompt to save again.
                this.Model.MarkGraphAsInitialized();
            }
            else if (saveError != null)
            {
                //throw out any error we had
                throw saveError;
            }
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Creates a new package with an initial planogram.
        /// </summary>
        private void NewPackage(Boolean createInitialPlanogram, UserEditorSettings settings)
        {
            if (Model != null) { throw new ArgumentException("This model is already populated"); }

            _packageType = PackageConnectionType.None;

            //Create a new package
            Package newPackage = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            newPackage.Name = Message.Generic_New;

            if (createInitialPlanogram)
            {
                Stopwatch timer = new Stopwatch();
                timer.Start();

                //Add an initial planogram
                Planogram newPlan = Planogram.NewPlanogram();
                newPlan.Name = Message.Generic_New;

                //apply defaults specified in the settings.
                newPlan.LengthUnitsOfMeasure = settings.LengthUnitOfMeasure;
                switch (newPlan.LengthUnitsOfMeasure)
                {
                    default:
                    case PlanogramLengthUnitOfMeasureType.Centimeters:
                        newPlan.AreaUnitsOfMeasure = PlanogramAreaUnitOfMeasureType.SquareCentimeters;
                        break;

                    case PlanogramLengthUnitOfMeasureType.Inches:
                        newPlan.AreaUnitsOfMeasure = PlanogramAreaUnitOfMeasureType.SquareInches;
                        break;
                }

                newPlan.CurrencyUnitsOfMeasure = settings.CurrencyUnitOfMeasure;
                newPlan.VolumeUnitsOfMeasure = settings.VolumeUnitOfMeasure;
                newPlan.WeightUnitsOfMeasure = settings.WeightUnitOfMeasure;
                newPlan.ProductPlacementX = settings.ProductPlacementX;
                newPlan.ProductPlacementY = settings.ProductPlacementY;
                newPlan.ProductPlacementZ = settings.ProductPlacementZ;

                //add an initial fixture
                if (newPlan.Fixtures.Count == 0)
                {
                    PlanogramFixture fixture = newPlan.Fixtures.Add(
                        String.Format(CultureInfo.CurrentCulture, Message.NewPlanogramFixtureName, 1),
                            settings);

                    PlanogramFixtureItem fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem();
                    fixtureItem.PlanogramFixtureId = fixture.Id;
                    fixtureItem.BaySequenceNumber = (Int16)1;
                    newPlan.FixtureItems.Add(fixtureItem);

                    //set a backboard
                    PlanogramFixtureComponent backboardFc = null;
                    PlanogramComponent backboard = null;
                    if (settings.FixtureHasBackboard)
                    {
                        backboardFc = fixture.Components.Add(PlanogramComponentType.Backboard, settings);
                        backboard = newPlan.Components.FindById(backboardFc.PlanogramComponentId);
                        PlanogramView.AdjustDefaultsForUOM(settings.LengthUnitOfMeasure, backboard);
                        backboard.Width = fixture.Width;
                        backboard.Height = fixture.Height;
                        backboardFc.Z = -backboard.Depth;
                    }

                    //set a base
                    if (settings.FixtureHasBase)
                    {
                        PlanogramFixtureComponent basefc = fixture.Components.Add(PlanogramComponentType.Base, settings);
                        PlanogramComponent fixtureBase = newPlan.Components.FindById(basefc.PlanogramComponentId);
                        PlanogramView.AdjustDefaultsForUOM(settings.LengthUnitOfMeasure, fixtureBase);
                        fixtureBase.Width = fixture.Width;

                        if (backboardFc != null)
                        {
                            basefc.X = backboardFc.X;
                            basefc.Y = backboardFc.Y;
                            basefc.Z = backboardFc.Z + backboard.Depth;
                        }

                    }
                }

                timer.Stop();
                Debug.WriteLine("{0} - plan model created", timer.Elapsed);

                newPackage.Planograms.Add(newPlan);
            }

            //Load the model.
            SetModel(ViewStateObjectModelChangeReason.Create, newPackage, null);

            //remark as initialized.
            newPackage.MarkGraphAsInitialized();
        }
              

        #endregion

        #region Fetch

        /// <summary>
        /// Fetches the package from the given repository id.
        /// </summary>
        /// <param name="id"></param>
        private void FetchById(Object id)
        {
            if (Model != null) { throw new ArgumentException("This model is already populated"); }

            _packageType = PackageConnectionType.Repository;

            // Get the plan hierarchy path of the the plan's plan group.
            PlanogramInfo planInfo = PlanogramInfoList.FetchByIds(new[] { Convert.ToInt32(id) }).FirstOrDefault();
            if (planInfo != null)
            {
                _planogramGroupId = planInfo.PlanogramGroupId;
                _planogramGroupName = planInfo.PlanogramGroupName;
                if (_planogramGroupId.HasValue) 
                {
                    _repositoryFolderPath = PlanogramHierarchy.FetchByEntityId(App.ViewState.EntityId).EnumerateAllGroups().
                                FirstOrDefault(g => g.Id == planInfo.PlanogramGroupId.Value).GetFriendlyFullPath();
                }
            }

            Fetch(new Package.FetchCriteria(PackageType.Unknown, id, DomainPrincipal.CurrentUserId, PackageLockType.User, false));
        }

        /// <summary>
        /// Fetches the package from the given filename
        /// </summary>
        /// <param name="fileName"></param>
        private void FetchByFileName(String fileName)
        {
            if (Model != null) { throw new ArgumentException("This model is already populated"); }

            _packageType = PackageConnectionType.FileSystem;
            _fileName = fileName;

            //fetch
            Fetch(new Package.FetchCriteria(PackageType.FileSystem, fileName, DomainPrincipal.CurrentUserId, PackageLockType.User, false));
        }

        /// <summary>
        /// Imports a package from an external file type.
        /// </summary>
        /// <param name="fileName">The path of the file to import from</param>
        /// <param name="importTemplate">the import mappings to use.</param>
        private void ImportByFileName(String fileName, PlanogramImportTemplate importTemplate)
        {
            if (Model != null) { throw new ArgumentException("This model is already populated"); }

            _packageType = PackageConnectionType.FileSystem;
            _fileName = fileName;

            //if we have a template set, then create the fetch argument.
            Object[] fetchArg = null;
            if (importTemplate != null)
            {
                const Boolean isImport = true;
                var context = new ImportContext(App.ViewState.Settings.Model,
                                                importTemplate.GetPlanogramFieldMappings(isImport).Select(m => m.GetDataTransferObject()),
                                                importTemplate.GetPlanogramMetricMappings().Select(m => m.GetDataTransferObject()));
                fetchArg = new Object[] {context};
            }

            //fetch as readonly so that we cant edit the original file accidently.
            Fetch(new Package.FetchCriteria(PackageType.FileSystem, _fileName, DomainPrincipal.CurrentUserId, PackageLockType.Unknown, true, fetchArg));
        }

        /// <summary>
        /// Loads a new viewmodel for the given model.
        /// </summary>
        /// <param name="model"></param>
        private void FetchByModel(Package model)
        {
            //Load the model.
            SetModel(ViewStateObjectModelChangeReason.Fetch, model, null);

            //remark as initialized.
            model.MarkGraphAsInitialized();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Save changes to the current package.
        /// </summary>
        public void Save()
        {
            if (Model == null) return;

            if (_packageType == PackageConnectionType.None)
            {
                if (App.ViewState.IsConnectedToRepository)
                {
                    //we are connected to a repository so save there first.
                    SaveExisting();
                }
                else
                {
                    if (!String.IsNullOrEmpty(_fileName))
                    {
                        //we have a file name so save there.
                        SaveAsFile(_fileName);
                    }
                    else
                    {
                        throw new ArgumentNullException("No filename set");
                    }
                }
            }
            else
            {
                //just update, the model will do the reset.
                SaveExisting();
            }
        }

        /// <summary>
        /// Saves the current manner using the normal
        /// update method.
        /// </summary>
        private void SaveExisting()
        {
            if (Model == null) return;

            //carry out pre-save actions
            OnSaving(/*isSaveAs*/false);

            //save
            Exception error = null;
            try
            {
                //we are connected to a repository so save there first.
                base.Update();
            }
            catch (Exception ex)
            {
                error = ex;
            }

            //carry out post-save actions
            OnSaved(error);
        }

        /// <summary>
        /// Saves the current package to a new file.
        /// </summary>
        public void SaveAsFile(String newFileName)
        {
            if (Model == null) return;
            if (String.IsNullOrEmpty(newFileName)) throw new ArgumentNullException("No filename set");

            //set package type to file
            _packageType = PackageConnectionType.FileSystem;
            _fileName = newFileName;
            _repositoryFolderPath = null;

            //carry out pre-save actions
            OnSaving(/*isSaveAs*/true);
            this.Model.Name = Path.GetFileNameWithoutExtension(newFileName);

            //save
            Exception error = null;
            try
            {
                SetModel(ViewStateObjectModelChangeReason.Update, this.Model.SaveAs(DomainPrincipal.CurrentUserId, newFileName), null);
            }
            catch (Exception ex)
            {
                error = ex;
            }

            //carry out post-save actions
            OnSaved(error);
        }

        /// <summary>
        /// Saves the current package to a new record
        /// in the currently connected repository.
        /// </summary>
        /// <param name="newName"></param>
        public void SaveAsToRepository(Object entityId, PlanogramGroup group)
        {
            if (Model == null) return;

            //set the package type to repository
            _packageType = PackageConnectionType.Repository;
            _fileName = null;

            // set new repository folder path
            if (group != null)
            {
                _repositoryFolderPath = group.GetFriendlyFullPath();
            }

            //carry out pre-save actions
            OnSaving(/*isSaveAs*/true);

            //save
            Exception error = null;
            try
            {
                SetModel(ViewStateObjectModelChangeReason.Update, this.Model.SaveAs(DomainPrincipal.CurrentUserId, entityId), null);
            }
            catch (Exception ex)
            {
                error = ex;
            }

            //carry out post-save actions
            OnSaved(error);
        }

        /// <summary>
        /// Autosaves the current package to the given directory.
        /// </summary>
        /// <param name="autosaveDirectory"></param>
        public void AutoSave(String autosaveDirectory)
        {
            if (this.Model != null && this.Model.IsSavable)
            {
                String autosaveName = Path.GetFileNameWithoutExtension(this.Model.Name);
                String fileName = Path.Combine(autosaveDirectory, autosaveName);
                fileName = Path.ChangeExtension(fileName, ".tmp" + Galleria.Ccm.Constants.PlanogramFileExtension);

                String finalFileName = fileName.Replace(".tmp" + Galleria.Ccm.Constants.PlanogramFileExtension,
                    Galleria.Ccm.Constants.PlanogramAutosaveFileExtension);

                //perform a save as
                try
                {
                    if (System.IO.File.Exists(fileName)) System.IO.File.Delete(fileName);
                    if (System.IO.File.Exists(finalFileName)) System.IO.File.Delete(finalFileName);

                    //need to detch event hooks temporarily during copy
                    foreach (PlanogramView planView in this.PlanogramViews)
                    {
                        planView.OnRemoveEventHooks(planView.Model);
                    }

                    Package autosavePackage = this.Model.Copy();
                    autosavePackage = autosavePackage.SaveAs(DomainPrincipal.CurrentUserId, fileName);
                    autosavePackage.Dispose();

                    //need to atthac hooks again
                    foreach (PlanogramView planView in this.PlanogramViews)
                    {
                        planView.OnAddEventHooks(planView.Model);
                    }

                    //switch to the auto file extension.
                    if (System.IO.File.Exists(fileName))
                    {
                        System.IO.File.Move(fileName, finalFileName);
                    }
                }
                catch (DataPortalException ex)
                {
                    Exception rootEx = ex.GetBaseException();
                    LocalHelper.RecordException(ex);
                }
            }
        }

        /// <summary>
        /// Checks the save settings to clear sections of the planogram while saving
        /// </summary>
        private void ApplySaveSettings()
        {
            if (this.Model == null) return;

            foreach (Planogram planogram in this.Model.Planograms)
            {
                //Assortment
                if (!SaveSettings.SaveAssortment)
                {
                    planogram.Assortment.ClearAll();
                }

                //Blocking
                if (!SaveSettings.SaveBlocking)
                {
                    planogram.Blocking.Clear();
                }

                //Cdt
                if (!SaveSettings.SaveCtd)
                {
                    planogram.ConsumerDecisionTree.ClearAll();
                }

                //Event logs
                if (!SaveSettings.SaveEventLogs)
                {
                    planogram.EventLogs.Clear();
                }

                //images
                if (!SaveSettings.SaveImages)
                {
                    planogram.ClearImages();
                }

                //performance
                if (!SaveSettings.SavePerformances)
                {
                    planogram.Performance.ClearAll();
                }

                //validation template
                if (!SaveSettings.SaveValidationTemplate)
                {
                    planogram.ValidationTemplate.ClearAll();
                }
            }
        }

 
        /// <summary>
        /// Saves the current package to a new external file type (JDA, Spaceman)
        /// </summary>
        /// <param name="planView">The planogram in the package to save</param>
        /// <param name="filename">The name of the file to export to</param>
        /// <param name="fileType">The type of file to create from the current planogram</param>
        public Object SaveAsExternalFileType(PackageSaveSettings saveSettings, String filename, PlanogramExportFileType fileType, PlanogramExportTemplate exportTemplate)
        {
            Object output = null;
            //check package is populated
            if (Model == null) return output;

            //Check Save Settings is populated
            if (saveSettings == null) return output;       

            //Check filetype is an eternal type
            if (fileType == PlanogramExportFileType.POG) return output;

            ExportContext context = null;
            if (exportTemplate != null)
            {
                context = new ExportContext(App.ViewState.Settings.Model,
                                                exportTemplate.GetPlanogramFieldMappings().Select(m => m.GetDataTransferObject()),
                                                exportTemplate.GetPlanogramMetricMappings().Select(m => m.GetDataTransferObject()),
                                                saveSettings.ExportIncludePerformanceData,
                                                saveSettings.ExportRetainSourcePlanLookAndFeel,
                                                saveSettings.ExportAllowComplexPositions,
                                                saveSettings.ExportViewErrorLogsOnCompletion);
            }

            //check a filename has been set
            if (String.IsNullOrEmpty(filename)) throw new ArgumentNullException("No filename set");

            //save
            try
            {
                //and export the copy to the required file type
                this.Model.ExportToExternalFile(DomainPrincipal.CurrentUserId, fileType, filename, context);
            }
            catch
            {
                //rethrow the error.
                throw;
            }

            return context;

        }

        /// <summary>
        /// Returns the location path of this package according
        /// to its type.
        /// </summary>
        public String GetLocationPath()
        {
            if (this.Model == null) return String.Empty;

            if (this.Model.PackageType == PackageType.FileSystem)return this.FileName;

            else return this.RepositoryFolderPath;
        }

        /// <summary>
        /// Returns true if this package is valid and can be saved.
        /// </summary>
        public Boolean IsValid()
        {
            return this.PlanogramViews.All(p => p.IsPlanogramModelValid);
        }

        /// <summary>
        /// Returns the user name of the user to which this package is actually locked
        /// </summary>
        /// <returns>The display name of the user who has the plan locked
        /// or null if the package is not readonly/the name cannot be obtained.</returns>
        public String GetLockedByOtherUserName()
        {
            //return null if the model is not locked.
            if (this.Model == null || !this.Model.IsReadOnly) return null;

            if(this.Model.PackageType == PackageType.FileSystem)
            {
                //if this is a file then always return null for now.
                // we would need to interop to work out who actually has the file
                // open as there is no way of doing this in .net and this isnt worth the work right now.
            }
            else
            {
                //Repository plan:

                //because the package doesnt actually tell us which user has it locked 
                // (lockeduserid property only shows the current user and usernmame property contains creation user)
                // we need to get the info from the plan info.
                PlanogramInfo info = null;
                try
                {
                    info = PlanogramInfoList.FetchByIds(new Int32[] { (Int32)this.Model.Id }).FirstOrDefault();
                }
                catch (Exception) { }


                if (info != null)
                {
                    //return the name of the user who actually has it locked.
                    return info.LockUserDisplayName;
                }
            }


            return null;
        }

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean isDisposing)
        {
            if (!_isDisposed)
            {
                Package oldModel = this.Model;
                this.Model = null;

                _planViews.BulkCollectionChanged -= PlanViews_BulkCollectionChanged;

                if (oldModel != null)
                {
                    oldModel.Dispose();
                }

                _isDisposed = true;
            }

        }

        #endregion
    }

    /// <summary>
    /// Class containing save settings for a package.
    /// </summary>
    /// <remarks>TODO: Move properties to be directly against the package view?</remarks>
    public sealed class PackageSaveSettings : INotifyPropertyChanged
    {
        #region Fields
        private Boolean _saveAssortment = true;
        private Boolean _saveBlocking = true;
        private Boolean _saveCtd = true;
        private Boolean _saveEventLogs = true;
        private Boolean _saveImages = true;
        private Boolean _savePerformances = true;
        private Boolean _saveValidationTemplate = true;
        private PlanogramExportFileType _saveAsFileType = PlanogramExportFileType.POG;
        private Boolean _exportIncludePerformanceData = true;
        private Boolean _exportRetainSourcePlanLookAndFeel = true;
        private Boolean _exportAllowComplexPositions = true;
        private Boolean _exportViewErrorLogsOnCompletion = true;
        #endregion

        #region Properties

        public PlanogramExportFileType SaveAsFileType
        {
            get { return _saveAsFileType; }
            set
            {
                _saveAsFileType = value;
                UpdateAllProperties();
            }
        }

        public Boolean SaveAssortment
        {
            get { return _saveAssortment && SaveAssortmentEnabled; }
            set
            {
                _saveAssortment = value;
                OnPropertyChanged("SaveAssortment");
            }
        }

        public Boolean SaveBlocking
        {
            get { return _saveBlocking && SaveBlockingEnabled; }
            set
            {
                _saveBlocking = value;
                OnPropertyChanged("SaveBlocking");
            }
        }

        public Boolean SaveCtd
        {
            get { return _saveCtd && SaveCtdEnabled; }
            set
            {
                _saveCtd = value;
                OnPropertyChanged("SaveCtd");
            }
        }

        public Boolean SaveEventLogs
        {
            get { return _saveEventLogs && SaveEventLogsEnabled; }
            set
            {
                _saveEventLogs = value;
                OnPropertyChanged("SaveEventLogs");
            }
        }

        public Boolean SaveImages
        {
            get { return _saveImages && SaveImagesEnabled; }
            set
            {
                _saveImages = value;
                OnPropertyChanged("SaveImages");
            }
        }

        public Boolean SavePerformances
        {
            get { return _savePerformances && SavePerformancesEnabled; }
            set
            {
                _savePerformances = value;
                OnPropertyChanged("SavePerformances");
            }
        }

        public Boolean SaveValidationTemplate
        {
            get { return _saveValidationTemplate && SaveValidationTemplateEnabled; }
            set
            {
                _saveValidationTemplate = value;
                OnPropertyChanged("SaveValidationTemplate");
            }
        }

        public Boolean SaveAssortmentEnabled
        {
            get { return IsSaveOptionEnabled(SaveOptionType.SaveAssortment); }
        }

        public Boolean SaveBlockingEnabled
        {
            get { return IsSaveOptionEnabled(SaveOptionType.SaveBlocking); }
        }

        public Boolean SaveCtdEnabled
        {
            get { return IsSaveOptionEnabled(SaveOptionType.SaveCtd); }
        }

        public Boolean SaveEventLogsEnabled
        {
            get { return IsSaveOptionEnabled(SaveOptionType.SaveEventLogs); }
        }


        public Boolean SaveImagesEnabled
        {
            get { return IsSaveOptionEnabled(SaveOptionType.SaveImages); }
        }

        public Boolean SavePerformancesEnabled
        {
            get { return IsSaveOptionEnabled(SaveOptionType.SavePerformances); }
        }

        public Boolean SaveValidationTemplateEnabled
        {
            get { return IsSaveOptionEnabled(SaveOptionType.SaveValidationTemplate); }
        }

        public Boolean ExportIncludePerformanceData
        {
            get { return _exportIncludePerformanceData; }
            set
            {
                _exportIncludePerformanceData = value;
                OnPropertyChanged("ExportIncludePerformanceData");
            }
        }

        public Boolean ExportRetainSourcePlanLookAndFeel
        {
            get { return _exportRetainSourcePlanLookAndFeel; }
            set
            {
                _exportRetainSourcePlanLookAndFeel = value;
                OnPropertyChanged("ExportRetainSourcePlanLookAndFeel");
            }
        }

        public Boolean ExportAllowComplexPositions
        {
            get { return _exportAllowComplexPositions; }
            set
            {
                _exportAllowComplexPositions = value;
                OnPropertyChanged("ExportAllowComplexPositions");
            }
        }

        public Boolean ExportViewErrorLogsOnCompletion
        {
            get { return _exportViewErrorLogsOnCompletion; }
            set
            {
                _exportViewErrorLogsOnCompletion = value;
                OnPropertyChanged("ExportViewErrorLogsOnCompletion");
            }
        }

        #endregion

        #region Constructor

        public PackageSaveSettings()  { }

        #endregion

        #region Methods

        /// <summary>
        /// Determines if the supplied save option should be enabled for the currently active planogram
        /// </summary>
        /// <param name="saveOption"></param>
        /// <returns></returns>
        private Boolean IsSaveOptionEnabled(SaveOptionType saveOption)
        {
            switch (this.SaveAsFileType)
            {
                case PlanogramExportFileType.Apollo:
                    return IsApolloSaveOptionEnabled(saveOption);
                case PlanogramExportFileType.JDA:
                    return IsJDASaveOptionEnabled(saveOption);
                case PlanogramExportFileType.Spaceman:
                    return IsSpacemanSaveOptionEnabled(saveOption);
                case PlanogramExportFileType.POG:
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Determines if the save option is valid for apollo
        /// </summary>
        /// <param name="saveOption"></param>
        /// <returns></returns>
        private Boolean IsApolloSaveOptionEnabled(SaveOptionType saveOption)
        {
            switch (saveOption)
            {
                case SaveOptionType.SaveAssortment:
                    return false;
                case SaveOptionType.SaveBlocking:
                    return false;
                case SaveOptionType.SaveCtd:
                    return false;
                case SaveOptionType.SaveEventLogs:
                    return false;
                case SaveOptionType.SaveImages:
                    return false;
                case SaveOptionType.SavePerformances:
                    return true;
                case SaveOptionType.SaveValidationTemplate:
                    return false;
            }

            return false;
        }
        /// <summary>
        /// Determines if the save option is valid for JDA
        /// </summary>
        /// <param name="saveOption"></param>
        /// <returns></returns>
        private Boolean IsJDASaveOptionEnabled(SaveOptionType saveOption)
        {
            switch (saveOption)
            {
                case SaveOptionType.SaveAssortment:
                    return false;
                case SaveOptionType.SaveBlocking:
                    return false;
                case SaveOptionType.SaveCtd:
                    return false;
                case SaveOptionType.SaveEventLogs:
                    return false;
                case SaveOptionType.SaveImages:
                    return false;
                case SaveOptionType.SavePerformances:
                    return true;
                case SaveOptionType.SaveValidationTemplate:
                    return false;
            }

            return false;
        }
        /// <summary>
        /// Determines if the save option is valid for Spaceman
        /// </summary>
        /// <param name="saveOption"></param>
        /// <returns></returns>
        private Boolean IsSpacemanSaveOptionEnabled(SaveOptionType saveOption)
        {
            switch (saveOption)
            {
                case SaveOptionType.SaveAssortment:
                    return false;
                case SaveOptionType.SaveBlocking:
                    return false;
                case SaveOptionType.SaveCtd:
                    return false;
                case SaveOptionType.SaveEventLogs:
                    return false;
                case SaveOptionType.SaveImages:
                    return false;
                case SaveOptionType.SavePerformances:
                    return true;
                case SaveOptionType.SaveValidationTemplate:
                    return false;
            }

            return false;
        }

        private void UpdateAllProperties()
        {
            OnPropertyChanged("SaveAsFileType");
            OnPropertyChanged("SaveAssortment");
            OnPropertyChanged("SaveBlocking");
            OnPropertyChanged("SaveCtd");
            OnPropertyChanged("SaveEventLogs");
            OnPropertyChanged("SaveImages");
            OnPropertyChanged("SavePerformances");
            OnPropertyChanged("SaveValidationTemplate");
            OnPropertyChanged("SaveAssortmentEnabled");
            OnPropertyChanged("SaveBlockingEnabled");
            OnPropertyChanged("SaveCtdEnabled");
            OnPropertyChanged("SaveEventLogsEnabled");
            OnPropertyChanged("SaveImagesEnabled");
            OnPropertyChanged("SavePerformancesEnabled");
            OnPropertyChanged("SaveValidationTemplateEnabled");
        }
        #endregion
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}