﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-24265 N.Haywood
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// UI view of a Label
    /// </summary>
    public sealed class LabelInfoItem
    {
        #region Properties

        public Object Id { get; private set; }
        public String Name { get; private set; }
        public LabelType Type { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type from the given model.
        /// </summary>
        /// <param name="model"></param>
        public LabelInfoItem(LabelInfo model)
        {
            this.Id = model.Id;
            this.Name = model.Name;
            this.Type = model.Type;
        }

        #endregion

        #region Methods

        #endregion

        #region Static Helpers

        public static List<LabelInfoItem> CreateViews(IEnumerable<LabelInfo> settings)
        {
            List<LabelInfoItem> ItemList = new List<LabelInfoItem>();

            foreach (LabelInfo setting in settings)
            {
                ItemList.Add(new LabelInfoItem(setting));
            }

            return ItemList;
        }

        public static Boolean AreMatching(LabelInfoItem view, Label setting)
        {
            if (view == null && setting == null) { return true; }
            if (view == null || setting == null) { return false; }

            if (view.Name != setting.Name) { return false; }
            if (view.Id != setting.Id) { return false; }

            return true;
        }


        #endregion
    }
}
