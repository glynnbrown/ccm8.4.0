﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// L.Hodson
//  Created
//V8-26787 : L.Ineson
//  Changes to planitemfield
//V8-26442 : I.George
//  Added CustomAttributeData
// V8-27126 : L.Ineson
//  Corrected height, width and depth setters.
// V8-27570 : A.Silva
//      Added ComponentSequenceNumber.
// V8-27442 : L.ineson
//  Moved notch number into base.
#endregion

#region Version History: (CCM 8.01)
// V8-28344 : L.Luong
//  Added Line properties

#endregion

#region Version History: (CCM 8.03)
// V8-29427 : L.Luong
//  Added Meta Data properties
#endregion

#region Version History: (CCM 810)
// V8-29545 : M.Pettit
//  Added GetBoundingRectangle() method
#endregion

#region Version History: (CCM 811)
// V8-25081 : L.Ineson
//  Added child changed handler to allow for notification
//  of helper properties like fill color.
#endregion

#region Version History: (CCM 820)
// V8-31426 : M.Brumby
//  GetBoundingRectangle position detail calls now pass in the Component ModelPlacement so that
//  items that have been cut will not error when pasting them.
#endregion

#region Version History: (CCM 8.3)
//V8-31832 : L.Ineson
// Implemented ICalculatedValueResolver
// V8-32132 : A.Silva
//  Amended AddSubComponentCopy so that it uses the refactored version of AddPositionCopy.
// V8-32523 : L.Ineson
//  Added support for component level annotations.
// V8-32782 : L.Ineson
// Plan component now gets passed into constructor
#endregion

#endregion

using Csla.Core;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Represents a view of a PlanogramComponent object
    /// </summary>
    public sealed class PlanogramComponentView : PlanogramComponentViewModelBase, IPlanItem
    {
        #region Fields

        private readonly PlanogramFixtureView _parentFixtureView;
        private readonly PlanogramAssemblyView _parentAssemblyView;

        private CastedObservableCollection<PlanogramSubComponentView, PlanogramSubComponentViewModelBase> _subComponentsRO;
        private CastedObservableCollection<PlanogramAnnotationView, PlanogramAnnotationViewModelBase> _annotationsRO;

        private PlanogramSubComponentMultiView _mergedSubComponentView;
        private CalculatedValueResolver _calculatedValueResolver;


        #endregion

        #region Binding Property Paths

        //public static readonly PropertyPath NotchNumberProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.NotchNumber);
        public static readonly PropertyPath FillColourProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.FillColour);
        public static readonly PropertyPath LineColourProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.LineColour);
        public static readonly PropertyPath LineThicknessProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.LineThickness);
        public static readonly PropertyPath MergedSubComponentViewProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MergedSubComponentView);

        //public static readonly PropertyPath WorldXProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaWorldX);
        //public static readonly PropertyPath WorldYProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaWorldY);
        //public static readonly PropertyPath WorldZProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaWorldZ);
        //public static readonly PropertyPath WorldAngleProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaWorldAngle);
        //public static readonly PropertyPath WorldSlopeProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaWorldSlope);
        //public static readonly PropertyPath WorldRollProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaWorldRoll);

        //public static readonly PropertyPath ComponentSequenceNumberProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.ComponentSequenceNumber);

        //Meta Data
        //public static readonly PropertyPath MetaNumberOfSubComponentsProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaNumberOfSubComponents);
        //public static readonly PropertyPath MetaNumberOfMerchandisedSubComponentsProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaNumberOfMerchandisedSubComponents);
        //public static readonly PropertyPath MetaTotalMerchandisableLinearSpaceProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaTotalMerchandisableLinearSpace);
        //public static readonly PropertyPath MetaTotalMerchandisableAreaSpaceProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaTotalMerchandisableAreaSpace);
        //public static readonly PropertyPath MetaTotalMerchandisableVolumetricSpaceProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaTotalMerchandisableVolumetricSpace);
        //public static readonly PropertyPath MetaTotalLinearWhiteSpaceProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaTotalLinearWhiteSpace);
        //public static readonly PropertyPath MetaTotalAreaWhiteSpaceProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaTotalAreaWhiteSpace);
        //public static readonly PropertyPath MetaTotalVolumetricWhiteSpaceProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaTotalVolumetricWhiteSpace);
        //public static readonly PropertyPath MetaProductsPlacedProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaProductsPlaced);
        //public static readonly PropertyPath MetaNewProductsProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaNewProducts);
        //public static readonly PropertyPath MetaChangesFromPreviousCountProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaChangesFromPreviousCount);
        //public static readonly PropertyPath MetaChangeFromPreviousStarRatingProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaChangeFromPreviousStarRating);
        //public static readonly PropertyPath MetaBlocksDroppedProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaBlocksDropped);
        //public static readonly PropertyPath MetaNotAchievedInventoryProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaNotAchievedInventory);
        //public static readonly PropertyPath MetaTotalFacingsProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaTotalFacings);
        //public static readonly PropertyPath MetaAverageFacingsProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaAverageFacings);
        //public static readonly PropertyPath MetaTotalUnitsProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaTotalUnits);
        //public static readonly PropertyPath MetaAverageUnitsProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaAverageUnits);
        //public static readonly PropertyPath MetaMinDosProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaMinDos);
        //public static readonly PropertyPath MetaMaxDosProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaMaxDos);
        //public static readonly PropertyPath MetaAverageDosProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaAverageDos);
        //public static readonly PropertyPath MetaMinCasesProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaMinCases);
        //public static readonly PropertyPath MetaAverageCasesProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaAverageCases);
        //public static readonly PropertyPath MetaMaxCasesProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaMaxCases);
        //public static readonly PropertyPath MetaSpaceToUnitsIndexProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaSpaceToUnitsIndex);
        //public static readonly PropertyPath MetaIsComponentSlopeWithNoRiserProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaIsComponentSlopeWithNoRiser);
        //public static readonly PropertyPath MetaIsOverMerchandisedDepthProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaIsOverMerchandisedDepth);
        //public static readonly PropertyPath MetaIsOverMerchandisedHeightProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaIsOverMerchandisedHeight);
        //public static readonly PropertyPath MetaIsOverMerchandisedWidthProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaIsOverMerchandisedWidth);
        //public static readonly PropertyPath MetaTotalComponentCollisionsProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaTotalComponentCollisions);
        //public static readonly PropertyPath MetaTotalPositionCollisionsProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaTotalPositionCollisions);
        //public static readonly PropertyPath MetaTotalFrontFacingsProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaTotalFrontFacings);
        //public static readonly PropertyPath MetaAverageFrontFacingsProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaAverageFrontFacings);
        //public static readonly PropertyPath MetaPercentageLinearSpaceFilledProperty = WpfHelper.GetPropertyPath<PlanogramComponentView>(p => p.MetaPercentageLinearSpaceFilled);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the collection of child subcomponent views
        /// </summary>
        public CastedObservableCollection<PlanogramSubComponentView, PlanogramSubComponentViewModelBase> SubComponents
        {
            get
            {
                if (_subComponentsRO == null)
                {
                    _subComponentsRO =
                        new CastedObservableCollection<PlanogramSubComponentView, PlanogramSubComponentViewModelBase>(
                            base.SubComponentViews, true);
                }
                return _subComponentsRO;
            }
        }

        /// <summary>
        /// Returns the collection of annotations held by this component.
        /// </summary>
        public CastedObservableCollection<PlanogramAnnotationView, PlanogramAnnotationViewModelBase> Annotations
        {
            get
            {
                if (_annotationsRO == null)
                {
                    _annotationsRO = new CastedObservableCollection<PlanogramAnnotationView, PlanogramAnnotationViewModelBase>(base.AnnotationViews, true);
                }
                return _annotationsRO;
            }
        }

        /// <summary>
        /// Returns the main subcomponent for this component.
        /// This will either be the only sub or the first merchandisable.
        /// </summary>
        public PlanogramSubComponentMultiView MergedSubComponentView
        {
            get
            {
                //Lazy load in as required.
                if (_mergedSubComponentView == null)
                {
                    _mergedSubComponentView = new PlanogramSubComponentMultiView(this.SubComponents);
                }
                return _mergedSubComponentView;
            }
        }


        /// <summary>
        /// Gets/Sets a general colour for all subcomponents of this
        /// </summary>
        public Int32 FillColour
        {
            get
            {
                Int32 returnVal = 0;
                if (this.SubComponents.Count > 0)
                {
                    returnVal = this.SubComponents.First().FillColour;
                }
                return returnVal;
            }
            set
            {
                foreach (PlanogramSubComponentView subComponent in this.SubComponents)
                {
                    subComponent.FillColour = value;
                }
            }
        }

        /// <summary>
        /// Gets/Sets a Line colour for all subcomponents of this
        /// </summary>
        public Int32 LineColour
        {
            get
            {
                Int32 returnVal = 0;
                if (this.SubComponents.Count > 0)
                {
                    returnVal = this.SubComponents.First().LineColour;
                }
                return returnVal;
            }
            set
            {
                foreach (PlanogramSubComponentView subComponent in this.SubComponents)
                {
                    subComponent.LineColour = value;
                }
            }
        }

        /// <summary>
        /// Gets/Sets a Line colour for all subcomponents of this
        /// </summary>
        public Single LineThickness
        {
            get
            {
                Single returnVal = 0;
                if (this.SubComponents.Count > 0)
                {
                    returnVal = this.SubComponents.First().LineThickness;
                }
                return returnVal;
            }
            set
            {
                foreach (PlanogramSubComponentView subComponent in this.SubComponents)
                {
                    subComponent.LineThickness = value;
                }
            }
        }

        /// <summary>
        /// Gets/Sets the image used by all subcomponents
        /// </summary>
        public PlanogramImage SubComponentImage
        {
            get
            {
                PlanogramImage returnVal = null;
                if (this.SubComponents.Count > 0)
                {
                    returnVal = this.SubComponents.First().Image;
                }
                return returnVal;
            }
            set
            {
                foreach (PlanogramSubComponentView subComponent in this.SubComponents)
                {
                    subComponent.Image = value;
                }
            }
        }

        /// <summary>
        /// Gets/Sets the component height
        /// </summary>
        public override Single Height
        {
            get { return ComponentModel.Height; }
            set
            {
                //if we only have 1 subcomponent and it is exactly aligned then set its height
                if (this.SubComponents.Count == 1)
                {
                    var sub = this.SubComponents[0];
                    if (sub.Angle == 0 && sub.Slope == 0 && sub.Roll == 0)
                    {
                        this.SubComponents[0].Height = value;
                    }
                }

                //NB- if we have more than 1 sub do nothing - 
                //  the value will be calculated from the child subcomponents.

                this.ComponentModel.UpdateSizeFromSubComponents();
            }
        }

        /// <summary>
        /// Gets/Sets the component width
        /// </summary>
        public override Single Width
        {
            get { return ComponentModel.Width; }
            set
            {
                //if we only have 1 subcomponent and it is exactly aligned then set its width
                if (this.SubComponents.Count == 1)
                {
                    var sub = this.SubComponents[0];

                    if (sub.Angle == 0 && sub.Slope == 0 && sub.Roll == 0)
                    {
                        this.SubComponents[0].Width = value;
                    }
                }

                //NB- if we have more than 1 sub do nothing - 
                //  the value will be calculated from the child subcomponents.

                this.ComponentModel.UpdateSizeFromSubComponents();
            }
        }

        /// <summary>
        /// Gets/Sets the component depth
        /// </summary>
        public override Single Depth
        {
            get { return ComponentModel.Depth; }
            set
            {
                //if we only have 1 subcomponent and it is exactly aligned then set its width
                if (this.SubComponents.Count == 1)
                {
                    var sub = this.SubComponents[0];

                    if (sub.Angle == 0 && sub.Slope == 0 && sub.Roll == 0)
                    {
                        this.SubComponents[0].Depth = value;
                    }
                }

                //NB- if we have more than 1 sub do nothing - 
                //  the value will be calculated from the child subcomponents.

                this.ComponentModel.UpdateSizeFromSubComponents();
            }
        }

        /// <summary>
        /// Returns the resolver used for binding to calculated values.
        /// </summary>
        public CalculatedValueResolver CalculatedValueResolver
        {
            get
            {
                if (_calculatedValueResolver == null)
                {
                    _calculatedValueResolver =
                        new CalculatedValueResolver(GetCalculatedValue, /*enableCaching*/true);
                }
                return _calculatedValueResolver;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="parentFixtureView"></param>
        /// <param name="fixtureComponent"></param>
        public PlanogramComponentView(PlanogramFixtureView parentFixtureView, 
            PlanogramFixtureComponent fixtureComponent, PlanogramComponent component)
            : base(parentFixtureView, fixtureComponent, component)
        {
            _parentFixtureView = parentFixtureView;

            InitializeChildViews();

            this.ComponentModel.ChildChanged += Component_ChildChanged;
        }



        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="parentAssemblyView"></param>
        /// <param name="assemblyComponent"></param>
        public PlanogramComponentView(PlanogramAssemblyView parentAssemblyView, 
            PlanogramAssemblyComponent assemblyComponent, PlanogramComponent component)
            : base(parentAssemblyView, assemblyComponent, component)
        {
            _parentAssemblyView = parentAssemblyView;

            InitializeChildViews();

            this.ComponentModel.ChildChanged += Component_ChildChanged;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a the list if child subcomponents changes.
        /// </summary>
        protected override void OnComponentModelSubComponentsBulkCollectionChanged(Framework.Model.BulkCollectionChangedEventArgs e)
        {
            base.OnComponentModelSubComponentsBulkCollectionChanged(e);

            //reset the merged subcomponents view
            if (_mergedSubComponentView != null)
            {
                _mergedSubComponentView.Dispose();
                _mergedSubComponentView = null;
            }
            OnPropertyChanged(MergedSubComponentViewProperty.Path);
        }

        /// <summary>
        /// Called whenever child of this subcomponent changes.
        /// </summary>
        private void Component_ChildChanged(object sender, ChildChangedEventArgs e)
        {
            if (e.PropertyChangedArgs != null)
            {
                String propertyName = e.PropertyChangedArgs.PropertyName;

                //if the subcomponent fill colour front has chanegd then raise the fill colour 
                // property change.
                if (propertyName == PlanogramSubComponent.FillColourFrontProperty.Name)
                {
                    OnPropertyChanged(FillColourProperty.Path);
                }
            }

        }

        #endregion

        #region Methods

        /// <summary>
        /// Called by the parent planogram to notify that processing has completed
        /// </summary>
        internal void NotifyPlanProcessCompleting()
        {
            //if we have calculated values cached then update them now.
            if (_calculatedValueResolver != null) _calculatedValueResolver.RefreshValues();

            //notify all child views
            foreach (var sub in this.SubComponents)
            {
                sub.NotifyPlanProcessCompleting();
            }
        }

        #region SubComponent Child Methods

        /// <summary>
        /// Creates a new view of the given subcomponent
        /// </summary>
        /// <param name="subComponent"></param>
        /// <returns></returns>
        protected override PlanogramSubComponentViewModelBase CreateSubComponentView(PlanogramSubComponent subComponent)
        {
            return new PlanogramSubComponentView(this, subComponent);
        }

        /// <summary>
        /// Adds a new subcomponent
        /// </summary>
        /// <returns></returns>
        public PlanogramSubComponentView AddSubComponent()
        {
            PlanogramSubComponent subComponent = PlanogramSubComponent.NewPlanogramSubComponent();

            subComponent.Name = String.Format(CultureInfo.CurrentCulture,
                Message.NewPlanogramSubComponentName, this.SubComponents.Count);

            ComponentModel.SubComponents.Add(subComponent);

            PlanogramSubComponentView view =
                this.SubComponents.First(s => Object.Equals(s.Model.Id, subComponent.Id));

            return view;
        }

        /// <summary>
        /// Adds a copy of the given subcomponent
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        public PlanogramSubComponentView AddSubComponentCopy(PlanogramSubComponentView src, Boolean copyPositions = true)
        {
            PlanogramSubComponent subComponentModel = src.Model.Copy();
            subComponentModel.Name = String.Format(CultureInfo.CurrentCulture,
                Message.NewPlanogramSubComponentName, this.SubComponents.Count);
            ComponentModel.SubComponents.Add(subComponentModel);

            PlanogramSubComponentView view =
                this.SubComponents.First(s => Object.Equals(s.Model.Id, subComponentModel.Id));

            if (copyPositions)
            {
                //copy positions
                foreach (PlanogramPositionView pos in src.Positions.ToArray())
                {
                    view.AddPositionCopy(pos);
                }
            }

            //copy annotations
            foreach (PlanogramAnnotationView anno in src.Annotations.ToArray())
            {
                view.AddAnnotationCopy(anno);
            }


            return view;
        }

        /// <summary>
        /// Removes the given subcomponent
        /// </summary>
        /// <param name="subComponentView"></param>
        public void RemoveSubComponent(PlanogramSubComponentView subComponentView)
        {
            Debug.Assert(this.SubComponents.Contains(subComponentView), "Deleting subcomponent from wrong component");
            if (!this.SubComponents.Contains(subComponentView)) return;

            PlanogramSubComponent subComponent = subComponentView.Model;
            Object subId = subComponent.Id;

            Planogram planModel = subComponent.Parent.Parent;

            //remove the sub
            ComponentModel.SubComponents.Remove(subComponent);

            //remove any linked positions
            planModel.Positions.RemoveList(planModel.Positions.Where(p => Object.Equals(p.PlanogramSubComponentId, subId)).ToList());

            //remove linked annotations
            planModel.Annotations.RemoveList(planModel.Annotations.Where(p => Object.Equals(p.PlanogramSubComponentId, subId)).ToList());

        }

        /// <summary>
        /// Returns the world coordinate rectangle that describes the bounds of the component, 
        /// its child sub-components and their positions
        /// </summary>
        public RectValue GetBoundingRectangle()
        {
            RectValue componentBoundingRect = new RectValue(0, 0, 0, 0, 0, 0);

            foreach (PlanogramSubComponentView subComponentView in this.SubComponents.Where(s => s.IsMerchandisable == true))
            {
                PlanogramSubComponentPlacement subComponentPlacement = subComponentView.Model.GetPlanogramSubComponentPlacement();
                RectValue subComponentBoundingRect = subComponentPlacement.GetPlanogramRelativeBoundingBox();

                if (subComponentView.Positions.Count > 0)
                {
                    switch (subComponentView.MerchandisingType)
                    {
                        case PlanogramSubComponentMerchandisingType.None:
                            //non-merchandisable so ignore
                            break;

                        case PlanogramSubComponentMerchandisingType.Hang:
                            //Get the bottom of the lowest position, or the bottom of the subcomponent if lower
                            PlanogramPosition lowestPosition =
                                subComponentView.Positions.OrderBy(p => (p.Y)).First().Model;
                            RectValue lowestPositionRect =
                                lowestPosition.GetPlanogramRelativeBoundingBox(
                                    subComponentView.ModelPlacement, lowestPosition.GetPositionDetails(subComponentView.ModelPlacement));

                            //Get the top of the highest position, or the top of the subcomponent if higher
                            PlanogramPosition highestPosition =
                                subComponentView.Positions.OrderByDescending(p => (p.Y + p.Height)).First().Model;
                            RectValue highestPositionRect =
                                highestPosition.GetPlanogramRelativeBoundingBox(
                                    subComponentView.ModelPlacement, highestPosition.GetPositionDetails(subComponentView.ModelPlacement));

                            //Find the outer rect for both sub-component and positions
                            subComponentBoundingRect = subComponentBoundingRect.Union(lowestPositionRect);
                            subComponentBoundingRect = subComponentBoundingRect.Union(highestPositionRect);
                            break;

                        case PlanogramSubComponentMerchandisingType.HangFromBottom:
                            //Find the product whose bottom is the greatest distance from the sub-component
                            PlanogramPosition tallestRodPosition =
                                subComponentView.Positions.OrderBy(p => (p.Y)).First().Model;
                            RectValue tallestRodPositionRect =
                                tallestRodPosition.GetPlanogramRelativeBoundingBox(
                                    subComponentView.ModelPlacement, tallestRodPosition.GetPositionDetails(subComponentView.ModelPlacement));

                            //Find the outer rect for both sub-component and positions
                            subComponentBoundingRect = subComponentBoundingRect.Union(tallestRodPositionRect);
                            break;

                        case PlanogramSubComponentMerchandisingType.Stack:
                            //Find the product whose top is the greatest distance from the sub-component
                            PlanogramPosition tallestStackedPosition =
                                subComponentView.Positions.OrderByDescending(p => (p.Y + p.Height)).First().Model;
                            RectValue tallestStackedPositionRect =
                                tallestStackedPosition.GetPlanogramRelativeBoundingBox(
                                    subComponentView.ModelPlacement, tallestStackedPosition.GetPositionDetails(subComponentView.ModelPlacement));

                            //Find the outer rect for both sub-component and positions
                            subComponentBoundingRect = subComponentBoundingRect.Union(tallestStackedPositionRect);
                            break;
                    }
                }

                //Set the return value or union with previous rect to get outer bounding rectangle of all sub-components
                if (componentBoundingRect.Height == 0 && componentBoundingRect.Width == 0 && componentBoundingRect.Depth == 0)
                {
                    //we are on the first sub-component
                    componentBoundingRect = subComponentBoundingRect;
                }
                else
                {
                    componentBoundingRect = componentBoundingRect.Union(subComponentBoundingRect);
                }
            }

            return componentBoundingRect;
        }


        #endregion

        #region Annotation Child Methods

        /// <summary>
        /// Returns a view of the given annotation model.
        /// </summary>
        protected override PlanogramAnnotationViewModelBase CreateAnnotationView(PlanogramAnnotation annotation)
        {
            return new PlanogramAnnotationView(this, annotation);
        }

        /// <summary>
        /// Adds a new annotation to this subcomponent.
        /// </summary>
        public PlanogramAnnotationView AddAnnotation()
        {
            PlanogramAnnotation anno = PlanogramAnnotation.NewPlanogramAnnotation();

            //link to this
            AssociateAnnotation(anno);

            //add to the planogram
            this.Planogram.Model.Annotations.Add(anno);

            return Annotations.Last();
        }

        /// <summary>
        /// Adds a copy of this given annotation to this subcomponent.
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        public PlanogramAnnotationView AddAnnotationCopy(PlanogramAnnotationView src)
        {
            PlanogramAnnotation anno = src.Model.Copy();

            //link to this
            AssociateAnnotation(anno);

            //add to the planogram
            this.Planogram.Model.Annotations.Add(anno);

            return Annotations.Last();
        }

        /// <summary>
        /// Removes the given annotation from this subcomponent
        /// </summary>
        /// <param name="annoView"></param>
        public void RemoveAnnotation(PlanogramAnnotationView annoView)
        {
            this.Planogram.Model.Annotations.Remove(annoView.Model);
        }

        /// <summary>
        /// Moves the given annotation from its current fixture to this one.
        /// </summary>
        /// <param name="annoView"></param>
        /// <returns></returns>
        public PlanogramAnnotationView MoveAnnotation(PlanogramAnnotationView annoView)
        {
           //check that the anno is not already attached to this component.
            if (annoView.Component == this) return annoView;

            PlanogramAnnotation annotation = annoView.Model;

            //remove it from its original parent.
            PointValue worldPos = new PointValue(annoView.WorldX, annoView.WorldY, annoView.WorldZ);

            //relink the model to this.
            AssociateAnnotation(annotation);

            //correct its world position
            annoView.WorldX = worldPos.X;
            annoView.WorldY = worldPos.Y;
            annoView.WorldZ = worldPos.Z;

            //get the new view from this.
            PlanogramAnnotationView newView = this.Annotations.FirstOrDefault(f => f.Model == annotation);
            newView.IsSelectable = annoView.IsSelectable;

            return newView;
        }

        #endregion

        #region Notch Methods

        /// <summary>
        /// Snaps this subcomponent to the nearest available notch.
        /// </summary>
        public void SnapToNearestNotch()
        {
            Single? snapY =
                GetNearestNotchY(this.Fixture, this.SubComponents.FirstOrDefault(),
                PlanogramSubComponentFaceType.Front,
                this.Y);

            if (snapY.HasValue)
            {
                try
                {
                    this.Y = snapY.Value;
                }
                catch (Csla.PropertyLoadException ex)
                {
                    LocalHelper.RecordException(ex);
                }
            }

        }

        /// <summary>
        /// Returns the nearest notch snap.
        /// </summary>
        public static Single? GetNearestNotchY(
            PlanogramFixtureView snapToFixture, PlanogramSubComponentView subToSnap,
            PlanogramSubComponentFaceType snapToFace, Single unsnappedY)
        {
            if (snapToFixture == null || subToSnap == null) return null;

            //check if the fixture has a backboard
            PlanogramComponentView backboard = snapToFixture.Components.FirstOrDefault(c => c.ComponentType == PlanogramComponentType.Backboard);
            if (backboard == null) return null;

            PlanogramSubComponentView backboardSub = backboard.SubComponents[0];

            return GetNearestNotchY(backboardSub, subToSnap.Component.Height, snapToFace, unsnappedY);
        }

        /// <summary>
        /// Returns the nearest notch snap.
        /// </summary>
        public static Single? GetNearestNotchY(
            PlanogramSubComponentView backboardSub, Single componentHeight,
            PlanogramSubComponentFaceType snapToFace, Single unsnappedY)
        {
            if (backboardSub == null) return null;

            if (snapToFace == PlanogramSubComponentFaceType.Front && !backboardSub.IsNotchPlacedOnFront) return null;
            if (snapToFace == PlanogramSubComponentFaceType.Back && !backboardSub.IsNotchPlacedOnBack) return null;
            if (snapToFace == PlanogramSubComponentFaceType.Top) return null;
            if (snapToFace == PlanogramSubComponentFaceType.Bottom) return null;
            if (snapToFace == PlanogramSubComponentFaceType.Left && !backboardSub.IsNotchPlacedOnLeft) return null;
            if (snapToFace == PlanogramSubComponentFaceType.Right && !backboardSub.IsNotchPlacedOnRight) return null;

            Int32? notchNumber = backboardSub.Model.GetNearestNotchNumber(unsnappedY + componentHeight);
            if (notchNumber == null) return null;

            Single? actualNotchVal = backboardSub.Model.GetNotchLocalYPosition(notchNumber.Value);
            if (!actualNotchVal.HasValue) return null;


            return actualNotchVal.Value - componentHeight;
        }

        #endregion

        /// <summary>
        /// Resolves the given expression for this item
        /// </summary>
        public Object GetCalculatedValue(String expressionText)
        {
            return PlanogramFieldHelper.ResolveFieldExpression(expressionText,
                PlanogramItemPlacement.NewPlanogramItemPlacement(
                component: this.ComponentModel,
                fixtureComponent: this.FixtureComponentModel,
                assemblyComponent: this.AssemblyComponentModel,
                assembly: (this.Assembly != null) ? this.Assembly.AssemblyModel : null,
                fixtureAssembly: (this.Assembly != null) ? this.Assembly.FixtureAssemblyModel : null,
                fixture: this.Fixture.FixtureModel,
                fixtureItem: this.Fixture.FixtureItemModel,
                planogram: this.Planogram.Model
                ));
        }


        private void OnPropertyChanged(PropertyPath property)
        {
            OnPropertyChanged(property.Path);
        }

        #endregion

        #region Static Helpers

        public static IEnumerable<ObjectFieldInfo> EnumerateDefaultPlanItemFields()
        {
            String[] defaultFieldNames = 
            {
                PlanogramComponent.NameProperty.Name,
                PlanogramComponent.ComponentTypeProperty.Name,
                PlanogramComponent.HeightProperty.Name,
                PlanogramComponent.WidthProperty.Name,
                PlanogramComponent.DepthProperty.Name,
                PlanogramFixtureComponent.MetaWorldXProperty.Name,
                PlanogramFixtureComponent.MetaWorldYProperty.Name,
                PlanogramFixtureComponent.MetaWorldZProperty.Name
            };

            foreach (String fieldName in defaultFieldNames)
            {
                ObjectFieldInfo field = EnumerateDisplayableFields().FirstOrDefault(f => f.PropertyName == fieldName);
                if (field == null) continue;
                else yield return field;
            }
        }

        #endregion

        #region IPlanItem Members

        public PlanogramView Planogram
        {
            get
            {
                if (_parentAssemblyView != null)
                {
                    return _parentAssemblyView.Planogram;
                }
                else if (_parentFixtureView != null)
                {
                    return _parentFixtureView.Planogram;
                }
                return null;
            }
        }

        public PlanogramFixtureView Fixture
        {
            get
            {
                if (_parentAssemblyView != null)
                {
                    return _parentAssemblyView.Fixture;
                }
                else if (_parentFixtureView != null)
                {
                    return _parentFixtureView;
                }
                return null;
            }
        }

        public PlanogramAssemblyView Assembly
        {
            get { return _parentAssemblyView; }
        }

        public PlanogramComponentView Component
        {
            get { return this; }
        }

        PlanogramSubComponentView IPlanItem.SubComponent
        {
            get { return null; }
        }

        PlanogramPositionView IPlanItem.Position
        {
            get { return null; }
        }

        PlanogramProductView IPlanItem.Product
        {
            get { return null; }
        }

        PlanogramAnnotationView IPlanItem.Annotation
        {
            get { return null; }
        }

        PlanItemType IPlanItem.PlanItemType
        {
            get { return PlanItemType.Component; }
        }

        public Boolean IsSelectable { get; set; }

        #endregion

        #region Dispose

        protected override void OnDisposing(bool disposing)
        {
            this.ComponentModel.ChildChanged -= Component_ChildChanged;
            if (_calculatedValueResolver != null) _calculatedValueResolver.Dispose();

            base.OnDisposing(disposing);
        }

        #endregion
    }


}