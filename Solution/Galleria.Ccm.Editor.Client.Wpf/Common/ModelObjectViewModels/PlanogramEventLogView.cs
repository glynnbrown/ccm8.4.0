﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-28061 : A.Kuszyk
//  Created.
#endregion

#region Version History: (CCM CCM803)
// V8-29590 : A.Probyn
//	Extended for new Score property
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using System.Windows;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Editor.Client.Wpf.Common.ModelObjectViewModels
{
    /// <summary>
    /// Represents a PlanogramEventLog for the UI, with friendly name values instead of enums
    /// and an execution order property.
    /// </summary>
    public sealed class PlanogramEventLogView
    {
        #region Fields
        private String _affectedType;
        private String _entryType;
        private String _eventType;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath ExecutionOrderProperty = WpfHelper.GetPropertyPath<PlanogramEventLogView>(p => p.ExecutionOrder);
        public static readonly PropertyPath AffectedTypeProperty = WpfHelper.GetPropertyPath<PlanogramEventLogView>(p => p.AffectedType);
        public static readonly PropertyPath EntryTypeProperty = WpfHelper.GetPropertyPath<PlanogramEventLogView>(p => p.EntryType);
        public static readonly PropertyPath EventTypeProperty = WpfHelper.GetPropertyPath<PlanogramEventLogView>(p => p.EventType);
        public static readonly PropertyPath ContentProperty = WpfHelper.GetPropertyPath<PlanogramEventLogView>(p => p.Content);
        public static readonly PropertyPath DescriptionProperty = WpfHelper.GetPropertyPath<PlanogramEventLogView>(p => p.Description);
        public static readonly PropertyPath WorkpackageSourceProperty = WpfHelper.GetPropertyPath<PlanogramEventLogView>(p => p.WorkpackageSource);
        public static readonly PropertyPath TaskSourceProperty = WpfHelper.GetPropertyPath<PlanogramEventLogView>(p => p.TaskSource);
        public static readonly PropertyPath DateTimeProperty = WpfHelper.GetPropertyPath<PlanogramEventLogView>(p => p.DateTime);
        public static readonly PropertyPath EventIdProperty = WpfHelper.GetPropertyPath<PlanogramEventLogView>(p => p.EventId);
        public static readonly PropertyPath ScoreProperty = WpfHelper.GetPropertyPath<PlanogramEventLogView>(p => p.Score);

        #endregion

        #region Properties

        /// <summary>
        /// The order that this event log was executed in (passed into constructor, 
        /// but should be based on Id value).
        /// </summary>
        public Int32 ExecutionOrder { get; private set; }

        /// <summary>
        /// The friendly name of the model's AffectedType
        /// </summary>
        public String AffectedType 
        { 
            get
            {
                if (_affectedType == null)
                {
                    if (Model.AffectedType.HasValue)
                    {
                        _affectedType = PlanogramEventLogAffectedTypeHelper.FriendlyNames[Model.AffectedType.Value];
                    }
                    else
                    {
                        _affectedType = String.Empty;
                    }
                }
                return _affectedType;
            }
        }

        /// <summary>
        /// The friendly name of the model's EntryType
        /// </summary>
        public String EntryType
        {
            get
            {
                if (_entryType == null)
                {
                    _entryType = PlanogramEventLogEntryTypeHelper.FriendlyNames[Model.EntryType];
                }
                return _entryType;
            }
        }

        /// <summary>
        /// The friendly name of the model's EventType
        /// </summary>
        public String EventType
        {
            get
            {
                if (_eventType == null)
                {
                    _eventType = PlanogramEventLogEventTypeHelper.FriendlyNames[Model.EventType];
                }
                return _eventType;
            }
        }

        /// <summary>
        /// The content of the event log
        /// </summary>
        public String Content 
        { 
            get { return Model.Content; }
        }

        /// <summary>
        /// The description of the event log.
        /// </summary>
        public String Description
        {
            get { return Model.Description; }
        }

        /// <summary>
        /// The workpackage source of the event log.
        /// </summary>
        public String WorkpackageSource
        {
            get { return Model.WorkpackageSource; }
        }

        /// <summary>
        /// The task source of the event log.
        /// </summary>
        public String TaskSource
        {
            get { return Model.TaskSource; }
        }

        /// <summary>
        /// The date and time upon which the event log was recorded.
        /// </summary>
        public DateTime DateTime 
        {
            get { return Model.DateTime; }
        }

        /// <summary>
        /// The EventId of the event log, relating to values stored in Galleria.Ccm.Logging.EventLogEvent
        /// </summary>
        public Int32 EventId    
        {
            get { return Model.EventId; }
        }

        /// <summary>
        /// The Score of the event log, relating to values stored in Galleria.Ccm.Logging.EventLogEvent
        /// </summary>
        public Byte Score
        {
            get { return Model.Score; }
        }

        /// <summary>
        /// The model that this Model View represents.
        /// </summary>
        public PlanogramEventLog Model { get; private set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Instantiates a new Model View from the given model and execution order.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="executionOrder"></param>
        public PlanogramEventLogView(PlanogramEventLog model, Int32 executionOrder)
        {
            Model = model;
            ExecutionOrder = executionOrder;
        } 
        #endregion
    }
}
