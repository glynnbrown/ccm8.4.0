﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
// V8-25734 : L.Ineson
//  Moved out rendering controls to framework.
// V8-25867 : A.Probyn
//  Added defensive code to ResequencePositions when creating dictionary
//V8-26787 : L.Ineson
//  Changes to planitemfield
// V8-27154 : L.Luong
//  Added ProductPlacement X,Y,Z settings for the facings as new products get put on the planogram
// V8-25931 : A.Probyn
//  Added defensive code to AddPositionCopy.
// V8-28383 : A.Kuszyk
//  Fixed issue with left/behind/below anchor directions in MovePosition.
// V8-28616 : N.Haywood
//  Added a fix to AddPositions that fixed collision detection for fill top
#endregion

#region Version History: CCM801

// V8-28676 : A.Silva
//      Added Image Helper Methods

#endregion

#region Version History: CCM802
// V8-29023 : M.Pettit
//  Added Merchandisable Depth
// V8-28766 : J.Pickup
//  PlanogramProductPlacementXType single switched to 'Manual' (Refactor) inc y & x types.
#endregion

#region Version History: (CCM 803)
// V8-29622 : L.Ineson
//  Changes made to allow adding positions to work using merch group methods.
#endregion

#region Version History: (CCM 810)
// V8-29903 : L.Ineson
//  Move position methods now use new merch group insert existing methods
// V8-29716 : D.Pleasance
//  Added ValidatePlacementMerchandisingStyle, if the placement merchandising group is hang, then the placement position merchandising style must be unit.
#endregion

#region Version History : CCM811
// V8-30581 : A.Kuszyk
//  Overloaded UpdateDividers method.
#endregion

#region Version History: (CCM 8.2)
// V8-30936 : M.Brumby
//  Added slotwall
#endregion

#region Version History: (CCM 8.3)
//V8-31832 : L.Ineson
// Implemented ICalculatedValueResolver
// V8-32080 : A.Silva
//  Amended AddPosition from a collection of PlanogramProductViews so that if the product does not exist in the target plan it is added.
// V8-32132 : A.Silva
//  Refactored and amended AddPositionCopy so that it copies positions from other planograms correctly.
//V8-32296 : L.Ineson
//  Passed product copying to planogram methods so that perf data also gets copied.
// V8-32327 : A.Silva
//  Amended move and copy methods to respect visual order.
// V8-32503 : L.Ineson
//  Amended position move xyz to go through new merchgroup method.
// V8-32772 : L.Ineson
//  Made sure that caps are automatically removed for positions moved to a hanging component.
// V8-32327 : A.Silva
//  Added PastePositionViews to encapsulate common logic when pasting positions on the subcomponent.
// CCM-18555 : A.Silva
// Removed the "always move positions between planograms" behavior and brought it in line with the rest
//  (now CTRL and drag will copy not move the positions like any other plan item).
// CCM-18593 : A.Silva
//  Amended AddPositionCopy to avoid crashes when the positions change components 
//  (this is happenening currently when dragging two components with one selected position on each, 
//  the positions get placed in one of the shelves but the merch group is not updated in time and tries to move one of them in the wrong component.
// CCM-18613 : A.Silva
//  Amended AddPositionCopy to make sure the product reference of the position copy is valid 
//  (even copying on the same plan could require readding the product, say if the position is cut, 
//  and the product removed from the product list and then the clipboard pasted back).
#endregion

#endregion

using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Represents a view of a PlanogramSubComponent.
    /// </summary>
    public sealed class PlanogramSubComponentView : PlanogramSubComponentViewModelBase, IPlanItem
    {
        #region Fields

        private readonly PlanogramComponentView _parentComponentView;

        private CastedObservableCollection<PlanogramPositionView, PlanogramPositionViewModelBase> _positionsRO;
        private CastedObservableCollection<PlanogramAnnotationView, PlanogramAnnotationViewModelBase> _annotationsRO;
        private ReadOnlyObservableCollection<PlanogramSubComponentDivider> _dividersRO;
        private CalculatedValueResolver _calculatedValueResolver;
        #endregion

        #region Binding PropertyPaths

        public static readonly PropertyPath FrontImageProperty = WpfHelper.GetPropertyPath<PlanogramSubComponentView>(p => p.FrontImage);
        public static readonly PropertyPath BottomImageProperty = WpfHelper.GetPropertyPath<PlanogramSubComponentView>(p => p.BottomImage);
        public static readonly PropertyPath BackImageProperty = WpfHelper.GetPropertyPath<PlanogramSubComponentView>(p => p.BackImage);
        public static readonly PropertyPath TopImageProperty = WpfHelper.GetPropertyPath<PlanogramSubComponentView>(p => p.TopImage);
        public static readonly PropertyPath LeftImageProperty = WpfHelper.GetPropertyPath<PlanogramSubComponentView>(p => p.LeftImage);
        public static readonly PropertyPath RightImageProperty = WpfHelper.GetPropertyPath<PlanogramSubComponentView>(p => p.RightImage);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the collection of positions held by this subcomponent.
        /// </summary>
        public CastedObservableCollection<PlanogramPositionView, PlanogramPositionViewModelBase> Positions
        {
            get
            {
                if (_positionsRO == null)
                {
                    _positionsRO =
                        new CastedObservableCollection<PlanogramPositionView, PlanogramPositionViewModelBase>(base.PositionViews, true);
                }
                return _positionsRO;
            }
        }

        /// <summary>
        /// Returns the collection of annotations held by this subcomponent.
        /// </summary>
        public CastedObservableCollection<PlanogramAnnotationView, PlanogramAnnotationViewModelBase> Annotations
        {
            get
            {
                if (_annotationsRO == null)
                {
                    _annotationsRO = new CastedObservableCollection<PlanogramAnnotationView, PlanogramAnnotationViewModelBase>(base.AnnotationViews, true);
                }
                return _annotationsRO;
            }
        }

        /// <summary>
        /// Returns the collection of dividers placed on this subcomponent.
        /// </summary>
        public ReadOnlyObservableCollection<PlanogramSubComponentDivider> Dividers
        {
            get
            {
                if (_dividersRO == null)
                {
                    _dividersRO = new ReadOnlyObservableCollection<PlanogramSubComponentDivider>(base.DividerViews);
                }
                return _dividersRO;
            }
        }

        /// <summary>
        /// Helper to get/set a value against all fill colours
        /// </summary>
        public Int32 FillColour
        {
            get { return Model.FillColourFront; }
            set
            {
                BeginUpdate();
                Model.FillColourFront = value;
                Model.FillColourBack = value;
                Model.FillColourTop = value;
                Model.FillColourBottom = value;
                Model.FillColourLeft = value;
                Model.FillColourRight = value;
                EndUpdate();
            }
        }

        /// <summary>
        /// Helper to get/set a value against all transparency percentages
        /// </summary>
        public Int32 TransparencyPercent
        {
            get { return Model.TransparencyPercentFront; }
            set
            {
                BeginUpdate();
                Model.TransparencyPercentFront = value;
                Model.TransparencyPercentBack = value;
                Model.TransparencyPercentTop = value;
                Model.TransparencyPercentBottom = value;
                Model.TransparencyPercentLeft = value;
                Model.TransparencyPercentRight = value;
                EndUpdate();
            }
        }

        /// <summary>
        /// Helper to get/set a value against all images
        /// </summary>
        public PlanogramImage Image
        {
            get { return this.FrontImage; }
            set
            {
                BeginUpdate();
                this.FrontImage = value;
                this.BackImage = value;
                this.TopImage = value;
                this.BottomImage = value;
                this.LeftImage = value;
                this.RightImage = value;
                EndUpdate();
            }
        }

        /// <summary>
        /// Returns the resolver used for binding to calculated values.
        /// </summary>
        public CalculatedValueResolver CalculatedValueResolver
        {
            get
            {
                if (_calculatedValueResolver == null)
                {
                    _calculatedValueResolver = 
                        new CalculatedValueResolver(GetCalculatedValue, /*enableCaching*/true);
                }
                return _calculatedValueResolver;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="parentComponent"></param>
        /// <param name="subComponent"></param>
        public PlanogramSubComponentView(PlanogramComponentView parentComponent, PlanogramSubComponent subComponent)
            : base(parentComponent, subComponent)
        {
            _parentComponentView = parentComponent;

            InitializeChildViews();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on the model.
        /// </summary>
        /// <param name="propertyName"></param>
        protected override void OnModelPropertyChanged(String propertyName)
        {
            base.OnModelPropertyChanged(propertyName);

            switch (propertyName)
            {
                case "FillColourFront":
                    OnPropertyChanged("FillColour");
                    break;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// TOString override
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Component.Name + ": " + this.Name;
        }

        #region Annotation Child Methods

        /// <summary>
        /// Returns a view of the given annotation model.
        /// </summary>
        protected override PlanogramAnnotationViewModelBase CreateAnnotationView(PlanogramAnnotation annotation)
        {
            return new PlanogramAnnotationView(this, annotation);
        }

        /// <summary>
        /// Adds a new annotation to this subcomponent.
        /// </summary>
        public PlanogramAnnotationView AddAnnotation()
        {
            PlanogramAnnotation anno = PlanogramAnnotation.NewPlanogramAnnotation();

            //link to this
            this.ModelPlacement.AssociateAnnotation(anno);

            //add to the planogram
            this.Planogram.Model.Annotations.Add(anno);

            return Annotations.Last();
        }

        /// <summary>
        /// Adds a copy of this given annotation to this subcomponent.
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        public PlanogramAnnotationView AddAnnotationCopy(PlanogramAnnotationView src)
        {
            PlanogramAnnotation anno = src.Model.Copy();

            //link to this
            this.ModelPlacement.AssociateAnnotation(anno);

            //add to the planogram
            this.Planogram.Model.Annotations.Add(anno);

            return Annotations.Last();
        }

        /// <summary>
        /// Removes the given annotation from this subcomponent
        /// </summary>
        /// <param name="annoView"></param>
        public void RemoveAnnotation(PlanogramAnnotationView annoView)
        {
            this.Planogram.Model.Annotations.Remove(annoView.Model);
        }

        /// <summary>
        /// Moves the given annotation from its current fixture to this one.
        /// </summary>
        /// <param name="annoView"></param>
        /// <returns></returns>
        public PlanogramAnnotationView MoveAnnotation(PlanogramAnnotationView annoView)
        {
            if (annoView.IsSubComponentAnnotation)
            {
                PlanogramAnnotation annotation = annoView.Model;

                //remove the view from the original fixture
                annoView.SubComponent.AnnotationViews.Remove(annoView);

                //relink the model to this.
                this.ModelPlacement.AssociateAnnotation(annotation);

                //add a new anno view to this.
                PlanogramAnnotationView newView = new PlanogramAnnotationView(this, annotation);
                AnnotationViews.Add(newView);

                newView.IsSelectable = annoView.IsSelectable;

                return newView;
            }
            else { return null; }
        }

        #endregion

        #region Position Child Methods

        /// <summary>
        /// Returns the view for the given position.
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        protected override PlanogramPositionViewModelBase CreatePositionView(PlanogramPosition position)
        {
            PlanogramPositionView view = null;

            PlanogramProductView productView = this.Planogram.FindProduct(position.PlanogramProductId);
            if (productView != null)
            {
                view = new PlanogramPositionView(this, position, productView);
            }

            return view;
        }

        /// <summary>
        /// Finds the position views for the given position ids.
        /// </summary>
        private List<PlanogramPositionView> FindPositionViews(IEnumerable<Object> positionIds)
        {
            List<PlanogramPositionView> positions = new List<PlanogramPositionView>(positionIds.Count());

            //now try to find the newly created position views from this subcomponent
            // or revert to checking the entire planview.
            foreach (Object positionId in positionIds)
            {
                PlanogramPositionView posView =
                    this.Positions.FirstOrDefault(p => Object.Equals(p.Model.Id, positionId));

                if (posView == null)
                {
                    //check the entire plan
                    posView = this.Planogram.EnumerateAllPositions().FirstOrDefault(p => Object.Equals(p.Model.Id, positionId));
                }

                if (posView != null)
                {
                    positions.Add(posView);
                }
            }

            return positions;

        }

        #region AddPositions from Products

        /// <summary>
        /// Adds a new position for the given product
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public PlanogramPositionView AddPosition(PlanogramProductView product)
        {
            return AddPosition(product, 0, 0, 0);
        }

        /// <summary>
        /// Adds positions to the specified subcomponent for the given products.
        /// </summary>
        /// <param name="products"></param>
        /// <param name="sub"></param>
        /// <param name="localDropPosition"></param>
        /// <returns></returns>
        public List<PlanogramPositionView> AddPositions(IEnumerable<Product> products, PlanogramPositionView anchor, PlanogramPositionAnchorDirection direction)
        {
            PlanogramView plan = this.Planogram;

            //Create the plan products
            List<PlanogramProductView> positionProducts = plan.AddProducts(products);

            //Add positions for the products
            List<PlanogramPositionView> newPositions = AddPositions(positionProducts, anchor, direction);

            return newPositions;
        }

        public PlanogramPositionView AddPosition(PlanogramProductView productView, Single x, Single y, Single z)
        {
            return AddPositions(new PlanogramProductView[] { productView }, x, y, z).FirstOrDefault();
        }


        public List<PlanogramPositionView> AddPositions(IEnumerable<PlanogramProductView> products)
        {
            return AddPositions(products, 0, 0, 0);
        }

        /// <summary>
        /// Adds positions to the merch group that this subcomponent belongs to
        /// </summary>
        /// <param name="products">the products to add positions for.</param>
        /// <param name="x">the x coordinate to set against the position</param>
        /// <param name="y">the y coordinate to set against the position.</param>
        /// <param name="z">the z coordingate to set against the position.</param>
        /// <returns></returns>
        public List<PlanogramPositionView> AddPositions(IEnumerable<PlanogramProductView> products, Single x, Single y, Single z)
        {
            //if this is not merchandisable then just return empty.
            if (!this.IsMerchandisable) return new List<PlanogramPositionView>();

            var positions = new List<PlanogramPositionView>();
            List<PlanogramProductView> productViews = products as List<PlanogramProductView> ?? products.ToList();
            if (!productViews.Any()) return positions;

            PlanogramMerchandisingGroup merchGroup = GetMerchandisingGroup();
            if (merchGroup == null) return positions;

            //reset the merch group
            merchGroup.Reset();

            var newPlacements = new List<PlanogramPositionPlacement>(productViews.Count);
            foreach (PlanogramProductView productView in productViews)
            {
                PlanogramProductView productMatch = this.Planogram.AddProduct(productView.Model);
                if (productMatch == null) continue;


                //create the position
                PlanogramPosition position = PlanogramPosition.NewPlanogramPosition(-1, productMatch.Model, this.ModelPlacement);
                position.X = x;
                position.Y = y;
                position.Z = z;

                //add the position to the merch group
                newPlacements.Add(merchGroup.InsertPositionPlacement(position, productMatch.Model));
            }

            //tell the planogram we are done making changes.
            this.Planogram.NotifyMerchandisingGroupChanged(merchGroup);

            //now get the new position views
            positions.AddRange(FindPositionViews(newPlacements.Select(p => p.Position.Id)));

            return positions;
        }

        /// <summary>
        /// Adds positions to the merch group that this subcomponent belongs to for the given planogram products.
        /// relative to the requested anchor.
        /// </summary>
        public List<PlanogramPositionView> AddPositions(IEnumerable<PlanogramProductView> products, PlanogramPositionView anchor, PlanogramPositionAnchorDirection direction)
        {
            //if this is not merchandisable then just return empty.
            if (!this.IsMerchandisable) return new List<PlanogramPositionView>();

            var positions = new List<PlanogramPositionView>();
            List<PlanogramProductView> productViews = products as List<PlanogramProductView> ?? products.ToList();
            if (!productViews.Any()) return positions;

            //Insert the position placement against the merch group.
            PlanogramMerchandisingGroup merchGroup = GetMerchandisingGroup();
            if (merchGroup == null) return positions;
            merchGroup.Reset();

            PlanogramPositionPlacement currentAnchor =
                merchGroup.PositionPlacements.FirstOrDefault(p => Object.Equals(p.Position.Id, anchor.Model.Id));

            List<PlanogramPositionPlacement> newPlacements = new List<PlanogramPositionPlacement>(productViews.Count());

            foreach (PlanogramProductView productView in productViews)
            {
                PlanogramProductView productMatch = this.Planogram.AddProduct(productView.Model);
                if (productMatch == null) continue;

                PlanogramPosition position =
                    PlanogramPosition.NewPlanogramPosition(-1, productMatch.Model, this.ModelPlacement);

                //add the position to the merch group
                PlanogramPositionPlacement placement =
                    merchGroup.InsertPositionPlacement(position, productMatch.Model, currentAnchor, direction);

                newPlacements.Add(placement);
            }

            //tell the planogram to apply all changes
            this.Planogram.NotifyMerchandisingGroupChanged(merchGroup);

            //now get the new position views
            positions.AddRange(FindPositionViews(newPlacements.Select(p => p.Position.Id)));

            return positions;
        }

        /// <summary>
        /// Adds positions to the specified subcomponent for the given products.
        /// </summary>
        public List<PlanogramPositionView> AddPositions(IEnumerable<Product> products, PointValue localPosition)
        {
            //if this is not merchandisable then just return empty.
            if (!this.IsMerchandisable) return new List<PlanogramPositionView>();

            PlanogramView plan = this.Planogram;

            //Create the plan products
            List<PlanogramProductView> positionProducts = plan.AddProducts(products);

            //Add positions for the products
            List<PlanogramPositionView> newPositions = AddPositions(positionProducts, localPosition.X, localPosition.Y, localPosition.Z);

            return newPositions;
        }


        #endregion

        #region Adding a copy of another position

        /// <summary>
        ///     Copy the given position and add it to the this instance.
        /// </summary>
        /// <param name="source">The <see cref="PlanogramPositionView"/> that is to be copied and added to this instance.</param>
        /// <returns>The newly copied and added position.</returns>
        /// <remarks>This method will verify that the product in the position is one existing in the planogram (when the position comes from another planogram).
        /// <para />If the position comes from another planogram, any existing GTIN in this instance's planogram will be used before importing the product from the other planogram.
        /// </remarks>
        public PlanogramPositionView AddPositionCopy(PlanogramPositionView source)
        {
            PlanogramPosition copy = source.Model.Copy();

            //  Sync the product reference in case it is no longer valid.
            copy.PlanogramProductId = Planogram.AddProduct(source.Product.Model).Model.Id;

            //Add the position.
            this.ModelPlacement.AddPosition(copy);

            //try find position view
            return Positions.FirstOrDefault(p => p.Model == copy);
        }

        /// <summary>
        ///     Add a copy of the <paramref name="positionToCopy"/> to this instance, moving it to the specified coordinates.
        /// </summary>
        /// <param name="positionToCopy"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        private PlanogramPositionView AddPositionCopy(PlanogramPositionView positionToCopy, Single x, Single y, Single z)
        {
            BeginUpdate();

            PlanogramPositionView newPosition = AddPositionCopy(positionToCopy);
            PlanogramPositionView movedPosition = MovePosition(newPosition, x, y, z);
            //  If unable to move for any reason, just continue with the copy.
            if (movedPosition != null) newPosition = movedPosition;

            EndUpdate();

            return newPosition;
        }

        public List<PlanogramPositionView> AddPositionCopies(IEnumerable<PlanogramPositionView> positionsToCopy)
        {
            List<PlanogramPositionView> sourcePositions = positionsToCopy.ToList();
            if (sourcePositions.Count == 0) return new List<PlanogramPositionView>();

            BeginUpdate();

            sourcePositions = sourcePositions.SortPositionsToMoveByDirection(PlanogramPositionAnchorDirection.ToLeft).ToList();
            List<PlanogramPositionView> newPositions = sourcePositions.Select(view => AddPositionCopy(view, 0,0,0)).ToList();
            
            EndUpdate();

            return newPositions;
        }

        public List<PlanogramPositionView> AddPositionCopies(IEnumerable<PlanogramPositionView> positionsToCopy, PlanogramPositionView anchor, PlanogramPositionAnchorDirection direction)
        {
            BeginUpdate();

            positionsToCopy = positionsToCopy.SortPositionsToMoveByDirection(direction);

            List<PlanogramPositionView> returnList = positionsToCopy.Select(pos => AddPositionCopy(pos, anchor, direction)).ToList();

            EndUpdate();
            return returnList;
        }

        /// <summary>
        /// Adds a copy of the given position relative to the anchor.
        /// </summary>
        /// <param name="positionToCopy"></param>
        /// <param name="anchor"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        public PlanogramPositionView AddPositionCopy(PlanogramPositionView positionToCopy, PlanogramPositionView anchor, PlanogramPositionAnchorDirection direction)
        {
            BeginUpdate();

            PlanogramPositionView newPosition = AddPositionCopy(positionToCopy);
            newPosition = MovePosition(newPosition, anchor, direction);

            EndUpdate();

            return newPosition;
        }

        #endregion

        #region Move existing position to this sub

        /// <summary>
        /// Moves the given position to this subcomponent and places it according to the given anchor.
        /// </summary>
        public PlanogramPositionView MovePosition(PlanogramPositionView positionToMove, PlanogramPositionView anchor, PlanogramPositionAnchorDirection direction)
        {
            return MovePositions(new PlanogramPositionView[] { positionToMove }, anchor, direction).FirstOrDefault();
        }

        /// <summary>
        /// Moves the given positions to this subcomponent based on the given anchor.
        /// </summary>
        public List<PlanogramPositionView> MovePositions(IEnumerable<PlanogramPositionView> positionsToMove, PlanogramPositionView anchor, PlanogramPositionAnchorDirection direction)
        {
            //if this is not merchandisable then just return empty.
            if (!this.IsMerchandisable) return new List<PlanogramPositionView>();

            this.Planogram.BeginUpdate();

            positionsToMove = positionsToMove.SortPositionsToMoveByDirection(direction).ToList();

            PlanogramMerchandisingGroup merchGroup = GetMerchandisingGroup();
            if (merchGroup == null) return new List<PlanogramPositionView>();
            merchGroup.Reset();

            PlanogramPositionPlacement currentAnchor = merchGroup.FindPlacementByPositionId(anchor.Model.Id);
            if (currentAnchor == null)
            {
                Debug.Fail("Failed to find anchor");
                return new List<PlanogramPositionView>();
            }

            List<PlanogramMerchandisingGroup> affectedGroups = new List<PlanogramMerchandisingGroup> {merchGroup};

            List<PlanogramPositionPlacement> updatedPlacements = new List<PlanogramPositionPlacement>(positionsToMove.Count());

            foreach (PlanogramPositionView positionView in positionsToMove)
            {
                PlanogramPosition position = positionView.Model;

                PlanogramPositionPlacement placementToMove = merchGroup.FindPlacementByPositionId(position.Id);

                //if the position belongs to another group 
                if (placementToMove == null)
                {
                    //Get the placement from the old group
                    PlanogramMerchandisingGroup otherGroup = this.Planogram.GetMerchandisingGroup(positionView.SubComponent.ModelPlacement);
                    if (!affectedGroups.Contains(otherGroup))
                    {
                        otherGroup.Reset();
                        affectedGroups.Add(otherGroup);
                    }
                    PlanogramPositionPlacement oldPlacement = otherGroup.FindPlacementByPositionId(position.Id);
                    if (oldPlacement != null)
                    {
                        PlanogramPositionPlacement placement =
                            merchGroup.InsertExistingPositionPlacement(oldPlacement, currentAnchor, direction);

                        if (placement != null)
                        {
                            ValidatePlacementMerchandisingStyle(placement);
                            updatedPlacements.Add(placement);
                        }
                    }
                }

                //we are just moving the position within the group.
                else
                {
                    //PlanogramPositionPlacement placementToMove = merchGroup.FindPlacementByPositionId(position.Id);
                    merchGroup.MovePositionPlacement(placementToMove, currentAnchor, direction);
                    //currentAnchor = placementToMove;

                    updatedPlacements.Add(placementToMove);
                }
            }


            //Notify the planogram that it needs updating.
            this.Planogram.NotifyMerchandisingGroupChanged(affectedGroups);

            this.Planogram.EndUpdate();

            //now return new position views
            return FindPositionViews(updatedPlacements.Select(p => p.Position.Id));
        }

        /// <summary>
        /// Moves the given position to this subcomponent
        /// </summary>
        /// <param name="posView"></param>
        public PlanogramPositionView MovePosition(PlanogramPositionView positionToMove)
        {
            //if this is not merchandisable then just return empty.
            if (!this.IsMerchandisable) return null;

            PlanogramMerchandisingGroup merchGroup = GetMerchandisingGroup();
            if (merchGroup == null) return null;


            //get the last position on the group
            if (merchGroup.PositionPlacements.Any())
            {
                //TODO: determine direction based on sub strategies
                PlanogramPositionAnchorDirection direction = PlanogramPositionAnchorDirection.ToRight;

                PlanogramPositionView lastPos =
                    this.Planogram.EnumerateAllPositions().FirstOrDefault(p => Object.Equals(p.Model.Id,
                        merchGroup.PositionPlacements.OrderBy(s => s.Position.Sequence).Last().Position.Id));

                return MovePositions(new PlanogramPositionView[] { positionToMove }, lastPos, direction).FirstOrDefault();
            }
            else
            {
                return MovePosition(positionToMove, 0, 0, 0);
            }

        }

        /// <summary>
        /// Move the position to the given coordinates on this subcomponent
        /// </summary>
        /// <param name="positionToMove">the placement to move</param>
        /// <param name="x">the new x coord relative to this subcomponent</param>
        /// <param name="y">the new y coord relative to this subcomponent</param>
        /// <param name="z">the new z coord relative to this subcomponent</param>
        /// <returns></returns>
        public PlanogramPositionView MovePosition(PlanogramPositionView positionToMove, Single x, Single y, Single z)
        {
            this.Planogram.BeginUpdate();
            
            PlanogramMerchandisingGroup merchGroup = GetMerchandisingGroup();
            if (merchGroup == null) return null;

            merchGroup.Reset();

            List<PlanogramMerchandisingGroup> affectedGroups = new List<PlanogramMerchandisingGroup>();
            affectedGroups.Add(merchGroup);

            List<PlanogramPositionPlacement> updatedPlacements = new List<PlanogramPositionPlacement>(1);
            PlanogramPosition position = positionToMove.Model;
            PlanogramPositionPlacement placementToMove = merchGroup.FindPlacementByPositionId(position.Id);

            //if the position belongs to another group 
            if (placementToMove == null)
            {
                //Get the placement from the old group
                PlanogramMerchandisingGroup otherGroup = this.Planogram.GetMerchandisingGroup(positionToMove.SubComponent.ModelPlacement);
                if (!affectedGroups.Contains(otherGroup))
                {
                    otherGroup.Reset();
                    affectedGroups.Add(otherGroup);
                }
                PlanogramPositionPlacement oldPlacement = otherGroup.FindPlacementByPositionId(position.Id);
                if (oldPlacement != null)
                {
                    //add it to the new group
                    PlanogramPositionPlacement placement = merchGroup.InsertExistingPositionPlacement(oldPlacement);
                    if (placement != null)
                    {
                        ValidatePlacementMerchandisingStyle(placement);
                        updatedPlacements.Add(placement);

                        //placement.Position.X = x;
                        //placement.Position.Y = y;
                        //placement.Position.Z = z;
                        merchGroup.MovePositionPlacement(placement, x, y, z);
                    }
                }
            }

            //we are just moving the position within the group.
            else
            {
                placementToMove = merchGroup.FindPlacementByPositionId(position.Id);
                updatedPlacements.Add(placementToMove);

                
                //placementToMove.Position.X = x;
                //placementToMove.Position.Y = y;
                //placementToMove.Position.Z = z;
                merchGroup.MovePositionPlacement(placementToMove, x, y, z);
            }

            //Apply the merch group changes
            this.Planogram.NotifyMerchandisingGroupChanged(affectedGroups);

            this.Planogram.EndUpdate();

            //now return new position views
            return FindPositionViews(updatedPlacements.Select(p => p.Position.Id)).FirstOrDefault();
        }

        /// <summary>
        /// Validates given placements merchandising style, 
        /// (if the placement merchandising group is hang, then the placement position merchandising style must be unit.)
        /// </summary>
        /// <param name="placement"></param>
        private void ValidatePlacementMerchandisingStyle(PlanogramPositionPlacement placement)
        {
            if (placement.MerchandisingGroup == null || placement.Position == null)
            {
                return;
            }

            if (placement.MerchandisingGroup.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang &&
                placement.Position.MerchandisingStyle != PlanogramPositionMerchandisingStyle.Unit)
            {
                placement.Position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Unit;

                //also remove any caps
                placement.Position.FacingsXHigh = 0;
                placement.Position.FacingsXWide = 0;
                placement.Position.FacingsXDeep = 0;

                placement.Position.FacingsYHigh = 0;
                placement.Position.FacingsYWide = 0;
                placement.Position.FacingsYDeep = 0;

                placement.Position.FacingsZHigh = 0;
                placement.Position.FacingsZWide = 0;
                placement.Position.FacingsZDeep = 0;
            } 
        }

        #endregion

        #region Removing Positions

        /// <summary>
        /// Remove the given position from this subcomponent
        /// </summary>
        /// <param name="posView"></param>
        public void RemovePosition(PlanogramPositionView posView)
        {
            if (Positions.Contains(posView))
            {
                this.Planogram.Model.Positions.Remove(posView.Model);
            }
        }

        #endregion

        #endregion

        /// <summary>
        /// Gets the merchandising group related to this subcomponent placement.
        /// </summary>
        public PlanogramMerchandisingGroup GetMerchandisingGroup()
        {
            PlanogramView plan = this.Planogram;
            if (plan == null) return null;

            return plan.GetMerchandisingGroup(this.ModelPlacement);
        }
        
        public AxisType[] GetAxisPriorityOrder()
        {
            //Try to set by component type.
            switch (this.ParentComponentView.ComponentType)
            {
                case PlanogramComponentType.Bar:
                case PlanogramComponentType.Shelf:
                case PlanogramComponentType.Pallet:
                    return new AxisType[] { AxisType.X, AxisType.Z, AxisType.Y };

                case PlanogramComponentType.Chest:
                    return new AxisType[] { AxisType.X, AxisType.Z, AxisType.Y };

                case PlanogramComponentType.ClipStrip:
                    return new AxisType[] { AxisType.Y, AxisType.Z, AxisType.X };

                case PlanogramComponentType.Rod:
                    return new AxisType[] { AxisType.Z, AxisType.X, AxisType.Y };

                case PlanogramComponentType.Peg:
                    return new AxisType[] { AxisType.X, AxisType.Y, AxisType.Z };

                case PlanogramComponentType.SlotWall:
                    return new AxisType[] { AxisType.X, AxisType.Y, AxisType.Z };
            }



            //otherwise use merch values
            if (this.ParentComponentView.IsMerchandisedTopDown)
            {
                return new AxisType[] { AxisType.X, AxisType.Z, AxisType.Y };
            }

            switch (this.MerchandisingType)
            {
                default:
                    return new AxisType[] { AxisType.X, AxisType.Y, AxisType.Z };

                case PlanogramSubComponentMerchandisingType.None:
                    return new AxisType[0];

                case PlanogramSubComponentMerchandisingType.Stack:
                    return new AxisType[] { AxisType.X, AxisType.Z, AxisType.Y };

                case PlanogramSubComponentMerchandisingType.Hang:
                    return new AxisType[] { AxisType.X, AxisType.Y, AxisType.Z };

                case PlanogramSubComponentMerchandisingType.HangFromBottom:
                    return new AxisType[] { AxisType.X, AxisType.Z, AxisType.Y };
            }

        }

        /// <summary>
        /// Called by the parent planogram to notify that processing has completed
        /// </summary>
        internal void NotifyPlanProcessCompleting()
        {
            //if we have calculated values cached then update them now.
            if (_calculatedValueResolver != null) _calculatedValueResolver.RefreshValues();

            //notify all child views
            foreach (var position in this.Positions)
            {
                position.NotifyPlanProcessCompleting();
            }
        }


        /// <summary>
        /// Resolves the given expression for this item
        /// </summary>
        public Object GetCalculatedValue(String expressionText)
        {
            PlanogramSubComponentPlacement modelPlacement = this.ModelPlacement;
            if (modelPlacement == null) return null;

            return PlanogramFieldHelper.ResolveFieldExpression(expressionText,
                PlanogramItemPlacement.NewPlanogramItemPlacement(subComponentPlacement: modelPlacement));
        }


        /// <summary>
        /// Updates the dividers collection
        /// </summary>
        public void UpdateDividers(IEnumerable<PlanogramDividerPlacement> dividers, Single xOffset)
        {
            //TODO: Get Rid of this method.

            if (DividerViews.Count > 0)
            {
                DividerViews.Clear();
            }

            foreach (PlanogramDividerPlacement d in dividers)
            {
                DividerViews.Add(new PlanogramSubComponentDivider(d, xOffset));
            }
        }

        /// <summary>
        /// Updates the dividers collection
        /// </summary>
        public void UpdateDividers(IEnumerable<PlanogramSubComponentDivider> dividers)
        {
            if (DividerViews.Count > 0)
            {
                DividerViews.Clear();
            }

            foreach (PlanogramSubComponentDivider d in dividers)
            {
                DividerViews.Add(d);
            }
        }

        /// <summary>
        ///     Paste the given <paramref name="views"/> on this instance close to the <paramref name="targetCoordinates"/>.
        /// </summary>
        /// <param name="views">The <see cref="PlanogramPositionView"/> items to be pasted on this instance.</param>
        /// <param name="targetCoordinates">The local coordinates near which the items should be pasted.</param>
        /// <param name="createCopy"></param>
        /// <returns></returns>
        internal List<PlanogramPositionView> PastePositionViews(
            List<PlanogramPositionView> views,
            PointValue targetCoordinates,
            Boolean createCopy = false)
        {
            var pastedPositionViews = new List<PlanogramPositionView>();
            if (views.Count == 0) return pastedPositionViews;

            PlanogramPositionView view = views.First();
            Boolean acrossPlanograms = !Equals(view.Planogram, Planogram);

            //  Across planograms should always copy.
            if (acrossPlanograms) createCopy = true;

            PlanogramPositionView pasteAnchor =
                createCopy
                    ? AddPositionCopy(view, targetCoordinates.X, targetCoordinates.Y, targetCoordinates.Z)
                    : MovePosition(view, targetCoordinates.X, targetCoordinates.Y, targetCoordinates.Z);
            pastedPositionViews.Add(pasteAnchor);
            pastedPositionViews.AddRange(
                views.Skip(1).Reverse().Select(v =>
                                               MovePosition(createCopy
                                                                ? AddPositionCopy(v)
                                                                : MovePosition(v),
                                                            pasteAnchor,
                                                            PlanogramPositionAnchorDirection.ToRight)));

            return pastedPositionViews;
        }

        #endregion

        #region Static Helpers

        public static IEnumerable<ObjectFieldInfo> EnumerateDefaultPlanItemFields()
        {
            String[] defaults = new String[]
            {
                PlanogramSubComponent.MerchandisingStrategyXProperty.Name,
                PlanogramSubComponent.MerchandisingStrategyYProperty.Name,
                PlanogramSubComponent.MerchandisingStrategyZProperty.Name,
                PlanogramSubComponent.MerchandisableHeightProperty.Name,
                PlanogramSubComponent.MerchandisableDepthProperty.Name,
                PlanogramSubComponent.CombineTypeProperty.Name,

                PlanogramSubComponent.DividerObstructionHeightProperty.Name,
                PlanogramSubComponent.DividerObstructionWidthProperty.Name,
                PlanogramSubComponent.DividerObstructionDepthProperty.Name,
                PlanogramSubComponent.DividerObstructionStartXProperty.Name,
                PlanogramSubComponent.DividerObstructionSpacingXProperty.Name,
                PlanogramSubComponent.DividerObstructionStartYProperty.Name,
                PlanogramSubComponent.DividerObstructionSpacingYProperty.Name,
                PlanogramSubComponent.DividerObstructionStartZProperty.Name,
                PlanogramSubComponent.DividerObstructionSpacingZProperty.Name,
                PlanogramSubComponent.IsDividerObstructionAtStartProperty.Name,
                PlanogramSubComponent.IsDividerObstructionAtEndProperty.Name,
                PlanogramSubComponent.IsDividerObstructionByFacingProperty.Name,

                PlanogramSubComponent.NotchStartXProperty.Name,
                PlanogramSubComponent.NotchSpacingXProperty.Name,
                PlanogramSubComponent.NotchStartYProperty.Name,
                PlanogramSubComponent.NotchSpacingYProperty.Name,
                PlanogramSubComponent.NotchHeightProperty.Name,
                PlanogramSubComponent.NotchWidthProperty.Name,
                PlanogramSubComponent.IsNotchPlacedOnFrontProperty.Name,
                PlanogramSubComponent.NotchStyleTypeProperty.Name,
                PlanogramSubComponent.RiserHeightProperty.Name,
                PlanogramSubComponent.RiserThicknessProperty.Name,
                PlanogramSubComponent.IsRiserPlacedOnFrontProperty.Name,

               PlanogramSubComponent.MerchConstraintRow1StartXProperty.Name,
               PlanogramSubComponent.MerchConstraintRow1SpacingXProperty.Name,
               PlanogramSubComponent.MerchConstraintRow1StartYProperty.Name,
               PlanogramSubComponent.MerchConstraintRow1SpacingYProperty.Name,
               PlanogramSubComponent.MerchConstraintRow1HeightProperty.Name,
               PlanogramSubComponent.MerchConstraintRow1WidthProperty.Name

            };


            foreach (String fieldName in defaults)
            {
                ObjectFieldInfo field = EnumerateDisplayableFields().FirstOrDefault(f => f.PropertyName == fieldName);
                if (field == null) continue;
                else yield return field;
            }
        }
        
        #endregion

        #region IPlanItem Members

        public PlanogramView Planogram
        {
            get { return _parentComponentView.Planogram; }
        }

        public PlanogramFixtureView Fixture
        {
            get { return _parentComponentView.Fixture; }
        }

        public PlanogramAssemblyView Assembly
        {
            get { return _parentComponentView.Assembly; }
        }

        public PlanogramComponentView Component
        {
            get { return _parentComponentView; }
        }

        PlanogramSubComponentView IPlanItem.SubComponent
        {
            get { return this; }
        }

        PlanogramPositionView IPlanItem.Position
        {
            get { return null; }
        }

        PlanogramProductView IPlanItem.Product
        {
            get { return null; }
        }

        PlanogramAnnotationView IPlanItem.Annotation
        {
            get { return null; }
        }

        PlanItemType IPlanItem.PlanItemType
        {
            get { return Common.PlanItemType.SubComponent; }
        }

        public Boolean IsSelectable { get; set; }

        #endregion

        #region IDisposable

        protected override void OnDisposing(bool disposing)
        {
            if (_calculatedValueResolver != null) _calculatedValueResolver.Dispose();
            base.OnDisposing(disposing);
        }

        #endregion
    }

    public enum PlanogramSubComponentFaceType
    {
        Front,
        Back,
        Top,
        Bottom,
        Left,
        Right
    }

}