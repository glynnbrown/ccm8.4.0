﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{

    public sealed class PlanogramViewObjectView : ViewModelObjectView<PlanogramView>
    {

        #region Constructor

        public PlanogramViewObjectView() { }

        #endregion

    }


}
