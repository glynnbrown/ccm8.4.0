﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (V8 1.0)
// V8-25395 : A.Probyn
//  Created
// V8-27648 : A.Probyn ~ Updated to support ProductLibraryProduct
// V8-28001 : A.Kuszyk
//  Added Events and Handlers for repository changing events.
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;
using System.Collections.Generic;
using System.IO;
using Csla;
using Galleria.Framework.Collections;
using System.ComponentModel;
using Galleria.Ccm.Common.Wpf;
using Galleria.Ccm.Common.Wpf.Helpers;
using System.Data;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Viewmodel holding a ProductLibrary model object
    /// </summary>
    public sealed class ProductLibraryViewModel : ViewStateObject<Ccm.Model.ProductLibrary>, IDisposable
    {
        #region Fields

        private Boolean _isLoading;
        private readonly BulkObservableCollection<ProductLibraryProduct> _productData = new BulkObservableCollection<ProductLibraryProduct>();
        private String _dataSourceName;

        #endregion

        #region Properties

        /// <summary>
        /// Returns true when the library is loading.
        /// </summary>
        public Boolean IsLoading
        {
            get { return _isLoading; }
            private set
            {
                _isLoading = value;
                OnPropertyChanged("IsLoading");
            }
        }

        /// <summary>
        /// Gets the collection of products for the current product library.
        /// </summary>
        public BulkObservableCollection<ProductLibraryProduct> Products
        {
            get { return _productData; }
        }

        /// <summary>
        /// Gets/Sets the name of the source
        /// from which the library should load data.
        /// </summary>
        public String DataSourceName
        {
            get { return _dataSourceName; }
            set
            {
                _dataSourceName = value;
                OnPropertyChanged("DateSourceName");
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ProductLibraryViewModel()
        {
            App.ViewStateLoaded += OnViewStateLoaded;
        }

        #endregion

        #region Events

        /// <summary>
        /// Fired when the UI needs to clear the products in the library.
        /// </summary>
        public event EventHandler ProductsRequireClearing;

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when the ViewState has been loaded onto App.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnViewStateLoaded(Object sender, EventArgs e)
        {
            App.ViewState.RepositoryConnectionChanged += OnRepositoryConnectionChanged;
        }

        /// <summary>
        /// Called when the repository connection has been changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnRepositoryConnectionChanged(Object sender, EventArgs e)
        {
            if (ProductsRequireClearing != null) ProductsRequireClearing(this, EventArgs.Empty);
        }

        /// <summary>
        /// Called whenever the model changes.
        /// </summary>
        /// <param name="e"></param>
        protected override void RaiseModelChangedEvent(ViewStateObjectModelChangedEventArgs<Model.ProductLibrary> e)
        {
            Products.Clear();

            base.RaiseModelChangedEvent(e);

            //dispose of the old model.
            if (e.OldModel != null)
            {
                e.OldModel.Dispose();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the model
        /// </summary>
        public void FetchById(Object productLibraryId, Boolean asReadOnly)
        {

            if (productLibraryId is Int32 && CCMClient.ViewState.EntityId > 0
                && !System.IO.File.Exists(Convert.ToString(productLibraryId)))
            {
                //load from the repository
                Fetch(new Ccm.Model.ProductLibrary.FetchByIdCriteria(ProductLibraryDalFactoryType.Unknown, productLibraryId, asReadOnly));
                return;
            }

            else if (productLibraryId is String)
            {
                //Fetch from file:

                String folderPath = Path.GetDirectoryName(productLibraryId.ToString());
                String ext = String.Format("{0}{1}", "*", Ccm.Model.ProductLibrary.FileExtension);

                Object id = null;
                if (Directory.Exists(folderPath))
                {
                    foreach (String fileName in
                        Directory.GetFiles(folderPath, ext, SearchOption.TopDirectoryOnly))
                    {
                        if (fileName.Equals(productLibraryId))
                        {
                            id = fileName;
                        }
                    }
                }


                Fetch(new Ccm.Model.ProductLibrary.FetchByIdCriteria(ProductLibraryDalFactoryType.FileSystem, id, asReadOnly));
            }

        }

        /// <summary>
        /// Saves the model synchronously
        /// </summary>
        public void SaveAsFile(String filePath)
        {
            //correct the ids of all new items
            if (this.Model.IsNew)
            {
                this.Model.Id = filePath;
            }

            Update();
        }

        #region FetchByProductUniverseIdAsync

        /// <summary>
        /// Populates this model for the given product universe.
        /// </summary>
        /// <param name="productUniverseId"></param>
        public void FetchByProductUniverseAsync(ProductUniverseInfo universeInfo)
        {
            if (IsLoading) throw new ArgumentException("Already loading");

            Products.Clear();
            IsLoading = true;

            var importWorker = new Csla.Threading.BackgroundWorker();
            importWorker.DoWork += FetchProductUniverseProductsWorker_DoWork;
            importWorker.RunWorkerCompleted += FetchProductUniverseProducts_RunWorkerCompleted;
            importWorker.WorkerReportsProgress = false;
            importWorker.WorkerSupportsCancellation = false;

            importWorker.RunWorkerAsync(new object[] { universeInfo });

        }

        /// <summary>
        ///     Work for fetching the product universe products
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FetchProductUniverseProductsWorker_DoWork(Object sender, DoWorkEventArgs e)
        {
            //Get generic args
            var args = (Object[])e.Argument;
            ProductUniverseInfo productUniverse = (ProductUniverseInfo)args[0];

            //Load full fat products
            ProductList productList = ProductList.FetchByProductUniverseId(productUniverse.Id);

            //Set result
            e.Result = new Object[] { productUniverse, productList };
        }

        /// <summary>
        ///     Worker completed for fetching the product universe products
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FetchProductUniverseProducts_RunWorkerCompleted(Object sender, RunWorkerCompletedEventArgs e)
        {
            var importWorker = (BackgroundWorker)sender;
            importWorker.DoWork -= FetchProductUniverseProductsWorker_DoWork;
            importWorker.RunWorkerCompleted -= FetchProductUniverseProducts_RunWorkerCompleted;

            if (e.Error == null)
            {
                Object[] result = (Object[])e.Result;
                ProductUniverseInfo universe = (ProductUniverseInfo)result[0];
                ProductList products = (ProductList)result[1];

                //Convert to library products with no performance data
                List<ProductLibraryProduct> libProducts = new List<ProductLibraryProduct>();
                foreach (Product product in products)
                {
                    libProducts.Add(ProductLibraryProduct.NewProductLibraryProduct(product));
                }

                this.Products.Clear();
                this.Products.AddRange(libProducts);
            }

            IsLoading = false;
        }

        #endregion

        /// <summary>
        /// Raises a property changed event.
        /// </summary>
        private void OnPropertyChanged(String propertyName)
        {
            base.RaisePropertyChangedEvent(propertyName);
        }

        /// <summary>
        /// Loads the product library for the given id.
        /// </summary>
        public Boolean SetLoadedProductLibrary(Object productLibraryId)
        {
            //if the file does not exist then try to fall back on the default.
            if (!System.IO.File.Exists(productLibraryId.ToString()))
            {
                Object defaultTemplateId = App.ViewState.Settings.Model.DefaultProductUniverseTemplateId;
                if (defaultTemplateId != null)
                {
                    //check if the id is a file path
                    if (System.IO.File.Exists(defaultTemplateId.ToString()))
                    {
                        productLibraryId = defaultTemplateId;
                    }
                    else if (App.ViewState.IsConnectedToRepository)
                    {
                        //otherwise look for the name in the repository
                        try
                        {
                            ProductLibraryInfoList infos = ProductLibraryInfoList.FetchByEntityId(App.ViewState.EntityId);
                            ProductLibraryInfo info = infos.FirstOrDefault(p => String.Compare(p.Name, Convert.ToString(defaultTemplateId), true) == 0);
                            if (info == null) return false;

                            productLibraryId = info.Id;
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    }
                    else return false;
                }
                else return false;
            }


            //Load the full product library details into the panel
            try
            {
                FetchById(productLibraryId, /*asReadonly*/true);
            }
            catch (Exception ex)
            {
                CommonHelper.RecordException(ex);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Returns true if the currently loaded template is valid for the 
        /// specified datasource name
        /// </summary>
        /// <returns></returns>
        public Boolean IsModelValidForDataSource()
        {
            if (this.Model == null) return false;
            if (String.IsNullOrEmpty(this.DataSourceName)) return true;
            return ProductLibraryHelper.IsValidTemplateForFile(this.Model, this.DataSourceName);
        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            App.ViewState.RepositoryConnectionChanged -= OnRepositoryConnectionChanged;
            App.ViewStateLoaded -= OnViewStateLoaded;
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    if (this.Model != null)
                    {
                        Ccm.Model.ProductLibrary m = this.Model;

                        this.Model = null;
                        m.Dispose();

                    }
                }
                _isDisposed = true;
            }
        }

        #endregion
    }

    /// <summary>
    /// Provides static helper methods for actions related
    /// to product libraries.
    /// </summary>
    public static class ProductLibraryHelper
    {

        /// <summary>
        /// Returns true if the source appears to match the template
        /// </summary>
        /// <param name="template"></param>
        /// <param name="datasourcePath"></param>
        /// <returns></returns>
        public static Boolean IsValidTemplateForFile(Model.ProductLibrary template, String datasourcePath)
        {
            if (template == null) return false;
            if(!System.IO.File.Exists(datasourcePath)) return false;

            //check that the file type is valid.
            if(!GetValidDataSourceExtensions(template.FileType).Contains(System.IO.Path.GetExtension(datasourcePath)))
                return false;

            if(template.FileType == ImportDefinitionFileType.Excel)
            {
                //check that the library has all mandatory fields mapped.
                if (template.ColumnMappings.Any(m => m.IsMandatory && String.IsNullOrEmpty(m.Source)))
                    return false;

                //check that the excel file contains the selected worksheet
                if(!FileHelper.LoadSelectedFileWorksheets(datasourcePath).Contains(template.ExcelWorkbookSheet))
                    return false;

                //check that column headers are available
                DataTable data = FileHelper.LoadWorksheetFirstRowOnly(datasourcePath, template.ExcelWorkbookSheet, template.IsFirstRowHeaders);
                foreach (ProductLibraryColumnMapping mapping in template.ColumnMappings)
                {
                    if (!mapping.IsMapped) continue;

                    Boolean colFound = false;
                    foreach (DataColumn col in data.Columns)
                    {
                        if (col.ColumnName == mapping.Source)
                        {
                            colFound = true;
                            break;
                        }
                    }
                    if (!colFound) return false;
                }


            }

            return true;
        }

         /// <summary>
        /// Returns a list of valid data source extensions for the given template type.
        /// </summary>
        public static String[] GetValidDataSourceExtensions()
        {
            return 
                GetValidDataSourceExtensions(ImportDefinitionFileType.Excel)
                .Union(GetValidDataSourceExtensions(ImportDefinitionFileType.Text))
                .ToArray();
        }

        /// <summary>
        /// Returns a list of valid data source extensions for the given template type.
        /// </summary>
        /// <param name="templateType"></param>
        /// <returns></returns>
        public static String[] GetValidDataSourceExtensions(ImportDefinitionFileType templateType)
        {
            switch (templateType)
            {
                default:
                case ImportDefinitionFileType.Excel:
                    {
                        return
                            new String[]
                            {
                                ".xlsx",
                                ".xls"
                            };
                    }
                

                case ImportDefinitionFileType.Text:
                    {
                        return
                           new String[]
                            {
                                ".txt",
                                ".csv"
                            };
                    }
            }
        }

        /// <summary>
        /// Returns the type of product library we are connected to.
        /// </summary>
        /// <returns></returns>
        public static ProductLibraryDalFactoryType GetConnectedProductLibraryType()
        {
            //TODO: Work out how to determine if we are connected to a db.
            return ProductLibraryDalFactoryType.FileSystem;
        }

        /// <summary>
        /// Returns POGPL path from whatever path is passed in
        /// </summary>
        /// <returns></returns>
        public static Object ResolveProductLibraryIdFromFilePath(String filePath)
        {
            if (Path.GetExtension(filePath) != Ccm.Model.ProductLibrary.FileExtension)
            {
                return Path.ChangeExtension(filePath, Ccm.Model.ProductLibrary.FileExtension);
            }
            return filePath;
        }

        /// <summary>
        /// Static helper to reesolve the file path from the product library id and filename
        /// </summary>
        /// <param name="productLibraryId"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static String ResolveFilePathFromProductLibraryId(Object productLibraryId)
        {
            if (productLibraryId != null)
            {
                //Lookup directory name
                String directory = Path.GetDirectoryName(productLibraryId.ToString());
                //Extract file name
                String productLibraryName = Path.GetFileNameWithoutExtension(productLibraryId.ToString());
                if (!string.IsNullOrEmpty(directory))
                {
                    //Get all files named the same as product library template
                    foreach (String filePath in
                        Directory.GetFiles(directory, String.Format("{0}*", productLibraryName), SearchOption.TopDirectoryOnly))
                    {
                        //Get extension
                        String extension = Path.GetExtension(filePath);
                        //Ensure template is excluded
                        if (extension != Ccm.Model.ProductLibrary.FileExtension)
                        {
                            //Get file name
                            String fileName = Path.GetFileNameWithoutExtension(filePath);
                            if (fileName.Equals(productLibraryName))
                            {
                                return filePath;
                            }
                        }
                    }
                }
            }
            //Return nothing
            return null;
        }
    }
}
