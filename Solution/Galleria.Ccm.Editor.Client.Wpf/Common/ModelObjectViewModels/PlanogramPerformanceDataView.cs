﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Windows;
//using Galleria.Framework.Planograms.Model;
//using Galleria.Framework.Helpers;

//namespace Galleria.Ccm.Editor.Client.Wpf.Common
//{
//    public class PlanogramPerformanceDataView : IPlanItem
//    {
//        #region Fields
//        private PlanogramPerformanceData _performanceData;
//        private PlanogramPerformanceMetricList _performanceMetrics;
//        #endregion

//        #region Binding Property Paths

//        public static readonly PropertyPath P1NameProperty = WpfHelper.GetPropertyPath<PlanogramPerformanceDataView>(p => p.P1Name);
//        public static readonly PropertyPath P1VisibilityProperty = WpfHelper.GetPropertyPath<PlanogramPerformanceDataView>(p => p.P1Visibility);
//        public static readonly PropertyPath P1ValueProperty = WpfHelper.GetPropertyPath<PlanogramPerformanceDataView>(p => p.P1Value);

//        #endregion

//        #region Constructor
//        public PlanogramPerformanceDataView(PlanogramPerformance planogramPerformance, PlanogramProduct planogramProduct)
//        {
//            _performanceData = planogramPerformance.PerformanceData
//                .Where(p => p.PlanogramProductId.Equals(planogramProduct.Id))
//                .FirstOrDefault();
//            if (_performanceData == null)
//            {
//                throw new ArgumentException("PlanogramPerformance must contain a data item for the given PlanogramProduct");
//            }
//            _performanceMetrics = planogramPerformance.Metrics;
//        }
//        #endregion

//        #region Properties

//        #region P1
//        public Single? P1Value
//        {
//            get { return _performanceData.P1; }
//            set { _performanceData.P1 = value; }
//        }

//        public Visibility P1Visibility
//        {
//            get
//            {
//                if (_performanceMetrics.Any(m => m.MetricId == 1))
//                {
//                    return Visibility.Visible;
//                }
//                return Visibility.Collapsed;
//            }
//        }

//        public String P1Name
//        {
//            get
//            {
//                if (_performanceMetrics.Any(m => m.MetricId == 1))
//                {
//                    return _performanceMetrics
//                        .Where(m => m.MetricId == 1)
//                        .FirstOrDefault()
//                        .Name;
//                }
//                return String.Empty;
//            }
//        } 
//        #endregion

//        #endregion

//        #region IPlanItem members
//        public PlanogramView Planogram
//        {
//            get { return null; }
//        }

//        public PlanogramFixtureView Fixture
//        {
//            get { return null; }
//        }

//        public PlanogramAssemblyView Assembly
//        {
//            get { return null; }
//        }

//        public PlanogramComponentView Component
//        {
//            get { return null; }
//        }

//        public PlanogramSubComponentView SubComponent
//        {
//            get { return null; }
//        }

//        public PlanogramPositionView Position
//        {
//            get { return null; }
//        }

//        public PlanogramProductView Product
//        {
//            get { return null; }
//        }

//        public PlanogramAnnotationView Annotation
//        {
//            get { return null; }
//        }

//        public PlanogramPerformanceDataView PerformanceData
//        {
//            get { return this; }
//        }

//        public PlanItemType PlanItemType
//        {
//            get { throw new NotImplementedException(); }
//        }

//        public bool IsSelectable
//        {
//            get
//            {
//                throw new NotImplementedException();
//            }
//            set
//            {
//                throw new NotImplementedException();
//            }
//        }

//        public float X
//        {
//            get
//            {
//                throw new NotImplementedException();
//            }
//            set
//            {
//                throw new NotImplementedException();
//            }
//        }

//        public float Y
//        {
//            get
//            {
//                throw new NotImplementedException();
//            }
//            set
//            {
//                throw new NotImplementedException();
//            }
//        }

//        public float Z
//        {
//            get
//            {
//                throw new NotImplementedException();
//            }
//            set
//            {
//                throw new NotImplementedException();
//            }
//        }

//        public float Angle
//        {
//            get
//            {
//                throw new NotImplementedException();
//            }
//            set
//            {
//                throw new NotImplementedException();
//            }
//        }

//        public float Slope
//        {
//            get
//            {
//                throw new NotImplementedException();
//            }
//            set
//            {
//                throw new NotImplementedException();
//            }
//        }

//        public float Roll
//        {
//            get
//            {
//                throw new NotImplementedException();
//            }
//            set
//            {
//                throw new NotImplementedException();
//            }
//        }

//        public float Height
//        {
//            get { throw new NotImplementedException(); }
//        }

//        public float Width
//        {
//            get { throw new NotImplementedException(); }
//        }

//        public float Depth
//        {
//            get { throw new NotImplementedException(); }
//        }

//        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
//        #endregion
//    }
//}
