﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-27980 : A.Silva
//  Moved over from PlanogramView.cs
#endregion
#region Version History: (CCM v8.0)
// V8-29054 : M.Pettit
//  Added ShowWarningToUser property - indicates whether users settings should allow this row to be visible
//  Added GTIN to position level descriptions
#endregion
#region Version History: (CCM v8.2.0)
// V8-30705 : A.Probyn
//      Added new assortment rule warning types
//      Extended to support assortment warnings with no positions.
// V8-29439 : J.Pickup
//  Added ShowInstantNotificationWarningToUser property - indicates whether users settings should allow this row to be visible for instant notifications.
#endregion
#region Version History: (CCM v8.3.0)
// V8-31835 : N.Haywood
//  Added Illegal and Legal MetaData and Validation
// V8-32088 : M.Pettit
//  Added Short/Full descriptions, imagesource. Split out identifier into new property
// V8-32396 : A.Probyn
//  Added default for Description incase it's not yet determined by the user.
// V8-32521 : J.Pickup
//  Added componentIsOutsideOfFixtureArea to the shouldShowInstantNotification.
// V8-32590 : J.Pickup
//  Added now checks whether to show correctly.
// V8-32593 : A.Silva
//  Refactored NewValidationWarningRow to eliminate duplicate code. Amended it too, so that it stores the source assembly component along with the assembly.
//  Refactored GetIdentifier to make adding or changing the different identifiers easier. 
//  Amended the identifier generation for assembly components so that they include the assembly name if available.
// V8-32625  : J.Pickup
//  GroupName now considers the outside offixture area as a component grouping. 
// V8-32867 : M.Brumby
//  Added ComponentIsOutsideOfFixtureArea to the warning image sources
#endregion
#endregion

using System;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Windows;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using System.Windows.Media;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    ///     Information about an Editor Validation Warning.
    /// </summary>
    public sealed class ValidationWarningRow : ViewModelObject, IEquatable<ValidationWarningRow>
    {
        #region Fields

        private PlanogramAssemblyComponent _assemblyComponent;
        private PlanogramFixtureAssembly _fixtureAssembly;
        private PlanogramFixtureComponent _fixtureComponent;
        private Boolean _isCorrected;
        private PlanogramPosition _position;
        private PlanogramValidationWarningType _warningType;
        private String _description = null;
        private String _fullDescription = null;
        private String _shortDescription = null;
        private String _identifier = null;
        private PlanogramProduct _product;
        private ImageSource _imageSource;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath AssemblyComponentProperty = GetPropertyPath(o => o.AssemblyComponent);
        public static readonly PropertyPath FixtureAssemblyProperty = GetPropertyPath(o => o.FixtureAssembly);
        public static readonly PropertyPath FixtureComponentProperty = GetPropertyPath(o => o.FixtureComponent);
        public static readonly PropertyPath IsCorrectedProperty = GetPropertyPath(o => o.IsCorrected);
        public static readonly PropertyPath ImageSourceProperty = GetPropertyPath(o => o.ImageSource);
        public static readonly PropertyPath PositionProperty = GetPropertyPath(o => o.Position);
        public static readonly PropertyPath ProductProperty = GetPropertyPath(o => o.Product);
        public static readonly PropertyPath WarningTypeProperty = GetPropertyPath(o => o.WarningType);
        public static readonly PropertyPath DescriptionProperty = GetPropertyPath(o => o.Description);
        public static readonly PropertyPath ShortDescriptionProperty = GetPropertyPath(o => o.ShortDescription);
        public static readonly PropertyPath IdentifierProperty = GetPropertyPath(o => o.Identifier);
        public static readonly PropertyPath ShowWarningToUserProperty = GetPropertyPath(o => o.ShowWarningToUser);
        public static readonly PropertyPath ShowInstantNotificationWarningToUserProperty = GetPropertyPath(o => o.ShowInstantNotificationWarningToUser);
        private static String _productAttributePropertyNameReplaceRegexString = @"[\[\]']";

        #endregion

        #region Properties

        public PlanogramAssemblyComponent AssemblyComponent
        {
            get { return _assemblyComponent; }
            private set
            {
                _assemblyComponent = value;
                OnPropertyChanged(AssemblyComponentProperty);
            }
        }

        //The displayed description. This may be the Short or full description which are set once
        public String Description
        {
            get
            {
                //If description not yet set, fall back to full description
                if (String.IsNullOrEmpty(_description))
                {
                    return FullDescription;
                }
                else
                {
                    return _description;
                }
            }
            set
            {
                _description = value;
                OnPropertyChanged(DescriptionProperty);
            }
        }

        public String Identifier { get { return _identifier ?? (_identifier = GetIdentifier()); } }

        public String ShortDescription
        {
            get
            {
                if (_shortDescription == null)
                {
                    _shortDescription = GetShortDescription();
                }
                return _shortDescription;
            }
        }

        public String FullDescription
        {
            get
            {
                if (_fullDescription == null)
                {
                    _fullDescription = GetFullDescription();
                }
                return _fullDescription;
            }
        }

        public PlanogramFixtureAssembly FixtureAssembly
        {
            get { return _fixtureAssembly; }
            private set
            {
                _fixtureAssembly = value;
                OnPropertyChanged(FixtureAssemblyProperty);
            }
        }

        public PlanogramFixtureComponent FixtureComponent
        {
            get { return _fixtureComponent; }
            private set
            {
                _fixtureComponent = value;
                OnPropertyChanged(FixtureComponentProperty);
            }
        }

        public Boolean IsCorrected
        {
            get { return _isCorrected; }
            set
            {
                _isCorrected = value;
                OnPropertyChanged(IsCorrectedProperty);
            }
        }

        public PlanogramPosition Position
        {
            get { return _position; }
            private set
            {
                _position = value;
                OnPropertyChanged(PositionProperty);
            }
        }

        public PlanogramProduct Product
        {
            get { return _product; }
            private set
            {
                _product = value;
                OnPropertyChanged(ProductProperty);
            }
        }

        public PlanogramValidationWarningType WarningType
        {
            get { return _warningType; }
            set
            {
                _warningType = value;
                OnPropertyChanged(WarningTypeProperty);
            }
        }

        /// <summary>
        /// Associated Product Attribute Comparison ResultGet
        /// </summary>
        public ProductAttributeComparisonResult ProductAttributeComparisonResult { get; private set; }

        /// <summary>
        /// Property that holds the image to display in the warning lists
        /// </summary>
        public ImageSource ImageSource
        {
            get { return _imageSource; }
            set
            {
                _imageSource = value;
                OnPropertyChanged(ImageSourceProperty);
            }
        }

        /// <summary>
        /// Property that indicates whether users settings should allow this row to be visible
        /// </summary>
        public Boolean ShowWarningToUser
        {
            get 
            {
                switch (WarningType)
                {
                    case PlanogramValidationWarningType.ComponentHasCollisions:
                        return App.ViewState.Settings.Model.ShowComponentHasCollisionsWarnings;
                    case PlanogramValidationWarningType.ComponentIsOverfilled:
                        return App.ViewState.Settings.Model.ShowComponentIsOverfilledWarnings;
                    case PlanogramValidationWarningType.ComponentIsOutsideOfFixtureArea:
                        return App.ViewState.Settings.Model.ShowComponentIsOutsideOfFixtureAreaWarnings;
                    case PlanogramValidationWarningType.ComponentSlopeWithNoRiser:
                        return App.ViewState.Settings.Model.ShowComponentSlopeWithNoRiserWarnings;
                    case PlanogramValidationWarningType.PegIsOverfilled:
                        return App.ViewState.Settings.Model.ShowPegIsOverfilledWarnings;
                    case PlanogramValidationWarningType.PositionCannotBreakTrayBack:
                        return App.ViewState.Settings.Model.ShowPositionCannotBreakTrayBackWarnings;
                    case PlanogramValidationWarningType.PositionCannotBreakTrayDown:
                        return App.ViewState.Settings.Model.ShowPositionCannotBreakTrayDownWarnings;
                    case PlanogramValidationWarningType.PositionCannotBreakTrayTop:
                        return App.ViewState.Settings.Model.ShowPositionCannotBreakTrayTopWarnings;
                    case PlanogramValidationWarningType.PositionCannotBreakTrayUp:
                        return App.ViewState.Settings.Model.ShowPositionCannotBreakTrayUpWarnings;
                    case PlanogramValidationWarningType.PositionDoesNotAchieveMinDeep:
                        return App.ViewState.Settings.Model.ShowPositionDoesNotAchieveMinDeepWarnings;
                    case PlanogramValidationWarningType.PositionExceedsMaxDeep:
                        return App.ViewState.Settings.Model.ShowPositionExceedsMaxDeepWarnings;
                    case PlanogramValidationWarningType.PositionExceedsMaxRightCap:
                        return App.ViewState.Settings.Model.ShowPositionExceedsMaxRightCapWarnings;
                    case PlanogramValidationWarningType.PositionExceedsMaxStack:
                        return App.ViewState.Settings.Model.ShowPositionExceedsMaxStackWarnings;
                    case PlanogramValidationWarningType.PositionExceedsMaxTopCap:
                        return App.ViewState.Settings.Model.ShowPositionExceedsMaxTopCapWarnings;
                    case PlanogramValidationWarningType.PositionHasCollisions:
                        return App.ViewState.Settings.Model.ShowPositionHasCollisionsWarnings;
                    case PlanogramValidationWarningType.PositionHasInvalidMerchStyleSettings:
                        return App.ViewState.Settings.Model.ShowPositionHasInvalidMerchStyleSettingsWarnings;
                    case PlanogramValidationWarningType.PositionIsHangingTray:
                        return App.ViewState.Settings.Model.ShowPositionIsHangingTrayWarnings;
                    case PlanogramValidationWarningType.PositionIsHangingWithoutPeg:
                        return App.ViewState.Settings.Model.ShowPositionIsHangingWithoutPegWarnings;
                    case PlanogramValidationWarningType.PositionIsOutsideMerchandisingSpace:
                        return App.ViewState.Settings.Model.ShowPositionIsOutsideMerchandisingSpaceWarnings;
                    case PlanogramValidationWarningType.ProductDoesNotAchieveMinCases:
                        return App.ViewState.Settings.Model.ShowProductDoesNotAchieveMinCasesWarnings;
                    case PlanogramValidationWarningType.ProductDoesNotAchieveMinDeliveries:
                        return App.ViewState.Settings.Model.ShowProductDoesNotAchieveMinDeliveriesWarnings;
                    case PlanogramValidationWarningType.ProductDoesNotAchieveMinDos:
                        return App.ViewState.Settings.Model.ShowProductDoesNotAchieveMinDosWarnings;
                    case PlanogramValidationWarningType.ProductDoesNotAchieveMinShelfLife:
                        return App.ViewState.Settings.Model.ShowProductDoesNotAchieveMinShelfLifeWarnings;
                    case PlanogramValidationWarningType.ProductHasDuplicateGtin:
                        return App.ViewState.Settings.Model.ShowProductHasDuplicateGtinWarnings;
                    case PlanogramValidationWarningType.ProductBreaksLocalLineRule:
                        return App.ViewState.Settings.Model.ShowAssortmentLocalLineRulesWarnings;
                    case PlanogramValidationWarningType.ProductBreaksDistributionRule:
                        return App.ViewState.Settings.Model.ShowAssortmentDistributionRulesWarnings;
                    case PlanogramValidationWarningType.ProductBreaksProductRule:
                    case PlanogramValidationWarningType.ProductBreaksDelistProductRule:
                    case PlanogramValidationWarningType.ProductBreaksForceProductRuleBroken:
                    case PlanogramValidationWarningType.ProductBreaksMinimumHurdleProductRule:
                    case PlanogramValidationWarningType.ProductBreaksPreserveProductRuleBroken:
                        return App.ViewState.Settings.Model.ShowAssortmentProductRulesWarnings;
                    case PlanogramValidationWarningType.ProductBreaksFamilyRule:
                    case PlanogramValidationWarningType.ProductBreaksMaximumProductFamilyRule:
                    case PlanogramValidationWarningType.ProductBreaksMinimumProductFamilyRule:
                    case PlanogramValidationWarningType.ProductBreaksDependencyFamilyRule:
                        return App.ViewState.Settings.Model.ShowAssortmentFamilyRulesWarnings;
                    case PlanogramValidationWarningType.ProductBreaksInheritanceRule:
                        return App.ViewState.Settings.Model.ShowAssortmentInheritanceRulesWarnings;
                    case PlanogramValidationWarningType.ProductBreaksCoreRule:
                        return App.ViewState.Settings.Model.ShowAssortmentCoreRulesWarnings;
                    case PlanogramValidationWarningType.ProductBreaksLocationProductIllegalRule:
                        return App.ViewState.Settings.Model.ShowLocationProductIllegalWarnings;
                    case PlanogramValidationWarningType.ProductBreaksLocationProductLegalRule:
                        return App.ViewState.Settings.Model.ShowLocationProductLegalWarnings;
                    case PlanogramValidationWarningType.PositionPegHolesAreInvalidRule:
                        return App.ViewState.Settings.Model.ShowPositionPegHolesAreInvalidWarnings;
                    default:
                        return true;
                }
            }
        }

        /// <summary>
        /// Property that indicates whether users settings should allow this row to be visible
        /// </summary>
        public Boolean ShowInstantNotificationWarningToUser
        {
            get
            {
                switch (WarningType)
                {
                    case PlanogramValidationWarningType.ComponentHasCollisions:
                        return App.ViewState.Settings.Model.ShowInstantComponentHasCollisionsWarnings;
                    case PlanogramValidationWarningType.ComponentIsOverfilled:
                        return App.ViewState.Settings.Model.ShowInstantComponentIsOverfilledWarnings;
                    case PlanogramValidationWarningType.ComponentSlopeWithNoRiser:
                        return App.ViewState.Settings.Model.ShowInstantComponentSlopeWithNoRiserWarnings;
                    case PlanogramValidationWarningType.PegIsOverfilled:
                        return App.ViewState.Settings.Model.ShowInstantPegIsOverfilledWarnings;
                    case PlanogramValidationWarningType.ComponentIsOutsideOfFixtureArea:
                        return App.ViewState.Settings.Model.ShowInstantComponentIsOutsideOfFixtureAreaWarnings;
                    case PlanogramValidationWarningType.PositionCannotBreakTrayBack:
                        return App.ViewState.Settings.Model.ShowInstantPositionCannotBreakTrayBackWarnings;
                    case PlanogramValidationWarningType.PositionCannotBreakTrayDown:
                        return App.ViewState.Settings.Model.ShowInstantPositionCannotBreakTrayDownWarnings;
                    case PlanogramValidationWarningType.PositionCannotBreakTrayTop:
                        return App.ViewState.Settings.Model.ShowInstantPositionCannotBreakTrayTopWarnings;
                    case PlanogramValidationWarningType.PositionCannotBreakTrayUp:
                        return App.ViewState.Settings.Model.ShowInstantPositionCannotBreakTrayUpWarnings;
                    case PlanogramValidationWarningType.PositionDoesNotAchieveMinDeep:
                        return App.ViewState.Settings.Model.ShowInstantPositionDoesNotAchieveMinDeepWarnings;
                    case PlanogramValidationWarningType.PositionExceedsMaxDeep:
                        return App.ViewState.Settings.Model.ShowInstantPositionExceedsMaxDeepWarnings;
                    case PlanogramValidationWarningType.PositionExceedsMaxRightCap:
                        return App.ViewState.Settings.Model.ShowInstantPositionExceedsMaxRightCapWarnings;
                    case PlanogramValidationWarningType.PositionExceedsMaxStack:
                        return App.ViewState.Settings.Model.ShowInstantPositionExceedsMaxStackWarnings;
                    case PlanogramValidationWarningType.PositionExceedsMaxTopCap:
                        return App.ViewState.Settings.Model.ShowInstantPositionExceedsMaxTopCapWarnings;
                    case PlanogramValidationWarningType.PositionHasCollisions:
                        return App.ViewState.Settings.Model.ShowInstantPositionHasCollisionsWarnings;
                    case PlanogramValidationWarningType.PositionHasInvalidMerchStyleSettings:
                        return App.ViewState.Settings.Model.ShowInstantPositionHasInvalidMerchStyleSettingsWarnings;
                    case PlanogramValidationWarningType.PositionIsHangingTray:
                        return App.ViewState.Settings.Model.ShowInstantPositionIsHangingTrayWarnings;
                    case PlanogramValidationWarningType.PositionIsHangingWithoutPeg:
                        return App.ViewState.Settings.Model.ShowInstantPositionIsHangingWithoutPegWarnings;
                    case PlanogramValidationWarningType.PositionIsOutsideMerchandisingSpace:
                        return App.ViewState.Settings.Model.ShowInstantPositionIsOutsideMerchandisingSpaceWarnings;
                    case PlanogramValidationWarningType.ProductDoesNotAchieveMinCases:
                        return App.ViewState.Settings.Model.ShowInstantProductDoesNotAchieveMinCasesWarnings;
                    case PlanogramValidationWarningType.ProductDoesNotAchieveMinDeliveries:
                        return App.ViewState.Settings.Model.ShowInstantProductDoesNotAchieveMinDeliveriesWarnings;
                    case PlanogramValidationWarningType.ProductDoesNotAchieveMinDos:
                        return App.ViewState.Settings.Model.ShowInstantProductDoesNotAchieveMinDosWarnings;
                    case PlanogramValidationWarningType.ProductDoesNotAchieveMinShelfLife:
                        return App.ViewState.Settings.Model.ShowInstantProductDoesNotAchieveMinShelfLifeWarnings;
                    case PlanogramValidationWarningType.ProductHasDuplicateGtin:
                        return App.ViewState.Settings.Model.ShowInstantProductHasDuplicateGtinWarnings;
                    case PlanogramValidationWarningType.ProductBreaksLocalLineRule:
                        return App.ViewState.Settings.Model.ShowInstantAssortmentLocalLineRulesWarnings;
                    case PlanogramValidationWarningType.ProductBreaksDistributionRule:
                        return App.ViewState.Settings.Model.ShowInstantAssortmentDistributionRulesWarnings;
                    case PlanogramValidationWarningType.ProductBreaksProductRule:
                    case PlanogramValidationWarningType.ProductBreaksDelistProductRule:
                    case PlanogramValidationWarningType.ProductBreaksForceProductRuleBroken:
                    case PlanogramValidationWarningType.ProductBreaksMinimumHurdleProductRule:
                    case PlanogramValidationWarningType.ProductBreaksPreserveProductRuleBroken:
                        return App.ViewState.Settings.Model.ShowInstantAssortmentProductRulesWarnings;
                    case PlanogramValidationWarningType.ProductBreaksFamilyRule:
                    case PlanogramValidationWarningType.ProductBreaksMaximumProductFamilyRule:
                    case PlanogramValidationWarningType.ProductBreaksMinimumProductFamilyRule:
                    case PlanogramValidationWarningType.ProductBreaksDependencyFamilyRule:
                    case PlanogramValidationWarningType.ProductBreaksDelistFamilyRule:
                        return App.ViewState.Settings.Model.ShowInstantAssortmentFamilyRulesWarnings;
                    case PlanogramValidationWarningType.ProductBreaksInheritanceRule:
                        return App.ViewState.Settings.Model.ShowInstantAssortmentInheritanceRulesWarnings;
                    case PlanogramValidationWarningType.ProductBreaksCoreRule:
                        return App.ViewState.Settings.Model.ShowInstantAssortmentCoreRulesWarnings;
                    case PlanogramValidationWarningType.ProductBreaksLocationProductIllegalRule:
                        return App.ViewState.Settings.Model.ShowInstantLocationProductIllegalWarnings;
                    case PlanogramValidationWarningType.ProductBreaksLocationProductLegalRule:
                        return App.ViewState.Settings.Model.ShowLocationProductLegalWarnings;
                    case PlanogramValidationWarningType.PositionPegHolesAreInvalidRule:
                        return App.ViewState.Settings.Model.ShowInstantPositionPegHolesAreInvalidWarnings;
                    default:
                        return true;
                }
            }
        }

        /// <summary>
        ///     Gets the group name for this <see cref="ValidationWarningRow"/>.
        /// </summary>
        public String GroupName
        {
            get
            {
                switch (WarningType)
                {
                    case PlanogramValidationWarningType.ComponentHasCollisions:
                    case PlanogramValidationWarningType.ComponentIsOverfilled:
                    case PlanogramValidationWarningType.ComponentSlopeWithNoRiser:
                    case PlanogramValidationWarningType.ComponentIsOutsideOfFixtureArea:
                        return Message.ValidationWarningRow_GroupName_ComponentWarnings;
                    case PlanogramValidationWarningType.PegIsOverfilled:
                        return Message.ValidationWarningRow_GroupName_PegWarnings;
                    case PlanogramValidationWarningType.PositionCannotBreakTrayBack:
                    case PlanogramValidationWarningType.PositionCannotBreakTrayDown:
                    case PlanogramValidationWarningType.PositionCannotBreakTrayTop:
                    case PlanogramValidationWarningType.PositionCannotBreakTrayUp:
                    case PlanogramValidationWarningType.PositionDoesNotAchieveMinDeep:
                    case PlanogramValidationWarningType.PositionExceedsMaxDeep:
                    case PlanogramValidationWarningType.PositionExceedsMaxRightCap:
                    case PlanogramValidationWarningType.PositionExceedsMaxStack:
                    case PlanogramValidationWarningType.PositionExceedsMaxTopCap:
                    case PlanogramValidationWarningType.PositionHasCollisions:
                    case PlanogramValidationWarningType.PositionHasInvalidMerchStyleSettings:
                    case PlanogramValidationWarningType.PositionIsHangingTray:
                    case PlanogramValidationWarningType.PositionIsHangingWithoutPeg:
                    case PlanogramValidationWarningType.PositionIsOutsideMerchandisingSpace:
                    case PlanogramValidationWarningType.PositionPegHolesAreInvalidRule:
                        return Message.ValidationWarningRow_GroupName_PositionWarnings;
                    case PlanogramValidationWarningType.ProductDoesNotAchieveMinCases:
                    case PlanogramValidationWarningType.ProductDoesNotAchieveMinDeliveries:
                    case PlanogramValidationWarningType.ProductDoesNotAchieveMinDos:
                    case PlanogramValidationWarningType.ProductDoesNotAchieveMinShelfLife:
                    case PlanogramValidationWarningType.ProductHasDuplicateGtin:
                        return Message.ValidationWarningRow_GroupName_ProductWarnings;
                    case PlanogramValidationWarningType.ProductBreaksLocalLineRule:
                    case PlanogramValidationWarningType.ProductBreaksDistributionRule:
                    case PlanogramValidationWarningType.ProductBreaksProductRule:
                    case PlanogramValidationWarningType.ProductBreaksFamilyRule:
                    case PlanogramValidationWarningType.ProductBreaksInheritanceRule:
                    case PlanogramValidationWarningType.ProductBreaksCoreRule:
                    case PlanogramValidationWarningType.ProductBreaksDelistProductRule:
                    case PlanogramValidationWarningType.ProductBreaksMinimumHurdleProductRule:
                    case PlanogramValidationWarningType.ProductBreaksPreserveProductRuleBroken:
                    case PlanogramValidationWarningType.ProductBreaksForceProductRuleBroken:
                    case PlanogramValidationWarningType.ProductBreaksDependencyFamilyRule:
                    case PlanogramValidationWarningType.ProductBreaksMaximumProductFamilyRule:
                    case PlanogramValidationWarningType.ProductBreaksMinimumProductFamilyRule:
                    case PlanogramValidationWarningType.ProductBreaksDelistFamilyRule:
                        return Message.ValidationWarningRow_GroupName_AssortmentWarnings;
                    case PlanogramValidationWarningType.ProductAttributeDoesNotMatchMasterProductAttribute:
                        return "Product Attribute Validation";
                    default:
                        return Message.ValidationWarningRow_GroupName_OtherWarnings;
                }
            }
        }

        #endregion

        #region Constructor

        private ValidationWarningRow()
        {
        }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Create and return a new instance of <see cref="ValidationWarningRow"/> from the given <paramref name="warning"/>.
        /// </summary>
        /// <param name="warning">The instance of <see cref="PlanogramValidationWarning"/> from which to create a new row.</param>
        /// <returns></returns>
        public static ValidationWarningRow NewValidationWarningRow(PlanogramValidationWarning warning)
        {
            return new ValidationWarningRow
                   {
                       WarningType = warning.WarningType,
                       Position = warning.SourcePosition,
                       FixtureComponent = warning.SourceFixtureComponent,
                       FixtureAssembly = warning.SourceFixtureAssembly,
                       AssemblyComponent = warning.SourceAssemblyComponent,
                       Product = warning.SourceProduct,
                       ImageSource = SetImageSource(warning.WarningType),
                       IsCorrected = false,
                       ProductAttributeComparisonResult = warning.ProductAttributeComparisonResult
            };
        }

        #endregion

        #region Methods

        private static ImageSource SetImageSource(PlanogramValidationWarningType warningType)
        {
            switch (warningType)
            {
                case PlanogramValidationWarningType.ComponentIsOutsideOfFixtureArea:
                case PlanogramValidationWarningType.ComponentHasCollisions:
                case PlanogramValidationWarningType.ComponentIsOverfilled:
                case PlanogramValidationWarningType.ComponentSlopeWithNoRiser:
                    return ImageResources.EditorValidation_ComponentItemWarning;
                case PlanogramValidationWarningType.PegIsOverfilled:
                case PlanogramValidationWarningType.PositionCannotBreakTrayBack:
                case PlanogramValidationWarningType.PositionCannotBreakTrayDown:
                case PlanogramValidationWarningType.PositionCannotBreakTrayTop:
                case PlanogramValidationWarningType.PositionCannotBreakTrayUp:
                case PlanogramValidationWarningType.PositionDoesNotAchieveMinDeep:
                case PlanogramValidationWarningType.PositionExceedsMaxDeep:
                case PlanogramValidationWarningType.PositionExceedsMaxRightCap:
                case PlanogramValidationWarningType.PositionExceedsMaxStack:
                case PlanogramValidationWarningType.PositionExceedsMaxTopCap:
                case PlanogramValidationWarningType.PositionHasCollisions:
                case PlanogramValidationWarningType.PositionHasInvalidMerchStyleSettings:
                case PlanogramValidationWarningType.PositionIsHangingTray:
                case PlanogramValidationWarningType.PositionIsHangingWithoutPeg:
                case PlanogramValidationWarningType.PositionIsOutsideMerchandisingSpace:
                case PlanogramValidationWarningType.PositionPegHolesAreInvalidRule:
                    return ImageResources.EditorValidation_PositionItemWarning;
                case PlanogramValidationWarningType.ProductDoesNotAchieveMinCases:
                case PlanogramValidationWarningType.ProductDoesNotAchieveMinDeliveries:
                case PlanogramValidationWarningType.ProductDoesNotAchieveMinDos:
                case PlanogramValidationWarningType.ProductDoesNotAchieveMinShelfLife:
                    return ImageResources.EditorValidation_InventoryItemWarning;
                case PlanogramValidationWarningType.ProductHasDuplicateGtin:
                    return ImageResources.EditorValidation_ProductItemWarning;
                case PlanogramValidationWarningType.ProductBreaksLocalLineRule:
                case PlanogramValidationWarningType.ProductBreaksDistributionRule:
                case PlanogramValidationWarningType.ProductBreaksProductRule:
                case PlanogramValidationWarningType.ProductBreaksFamilyRule:
                case PlanogramValidationWarningType.ProductBreaksInheritanceRule:
                case PlanogramValidationWarningType.ProductBreaksCoreRule:
                case PlanogramValidationWarningType.ProductBreaksDelistProductRule:
                case PlanogramValidationWarningType.ProductBreaksMinimumHurdleProductRule:
                case PlanogramValidationWarningType.ProductBreaksPreserveProductRuleBroken:
                case PlanogramValidationWarningType.ProductBreaksForceProductRuleBroken:
                case PlanogramValidationWarningType.ProductBreaksDependencyFamilyRule:
                case PlanogramValidationWarningType.ProductBreaksMaximumProductFamilyRule:
                case PlanogramValidationWarningType.ProductBreaksMinimumProductFamilyRule:
                    return ImageResources.EditorValidation_RuleItemWarning;
                default:
                    return ImageResources.EditorValidation_ItemsWarning;
            }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override String ToString()
        {
            return Description;
        }

        #region Get Identifier methods

        /// <summary>
        ///     Get the identifier for the specific source of this instance.
        /// </summary>
        /// <returns></returns>
        private String GetIdentifier()
        {
            String identifier;

            if (TryGetFixtureComponentIdentifier(out identifier)) return identifier;
            if (TryGetAssemblyComponentIdentifier(out identifier)) return identifier;
            if (TryGetAssemblyIdentifier(out identifier)) return identifier;
            if (TryGetProductIdentifier(out identifier)) return identifier;

            return Message.ValidationWarningRow_Description_Unknown;
        }

        /// <summary>
        ///     Try get the identifier for a fixture component source. If the source is not a fixture component, <c>null</c> will be returned.
        /// </summary>
        /// <param name="identifier">Will return the identifier for the source if it is a fixture component, or null otherwise.</param>
        /// <returns></returns>
        private Boolean TryGetFixtureComponentIdentifier(out String identifier)
        {
            identifier = null;
            PlanogramComponent component = FixtureComponent == null ? null : FixtureComponent.GetPlanogramComponent();
            if (component == null) return false;

            identifier = component.Name;
            return true;
        }

        /// <summary>
        ///     Try get the identifier for an assembly component source. If the source is not a assembly component, <c>null</c> will be returned.
        /// </summary>
        /// <param name="identifier">Will return the identifier for the source if it is a assembly component, or null otherwise.</param>
        /// <returns></returns>
        private Boolean TryGetAssemblyComponentIdentifier(out String identifier)
        {
            const String assemblyComponentIdentifierMask = @"{0} : {1}";
            identifier = null;
            PlanogramComponent assemblyComponent = AssemblyComponent == null ? null : AssemblyComponent.GetPlanogramComponent();
            PlanogramAssembly assembly = FixtureAssembly == null ? null : FixtureAssembly.GetPlanogramAssembly();
            if (assemblyComponent == null) return false;

            identifier = assembly == null || String.IsNullOrWhiteSpace(assembly.Name)
                             ? assemblyComponent.Name
                             : String.Format(assemblyComponentIdentifierMask, assemblyComponent.Name, assembly.Name);
            return true;
        }

        /// <summary>
        ///     Try get the identifier for an assembly source. If the source is not a assembly, <c>null</c> will be returned.
        /// </summary>
        /// <param name="identifier">Will return the identifier for the source if it is a assembly, or null otherwise.</param>
        /// <returns></returns>
        private Boolean TryGetAssemblyIdentifier(out String identifier)
        {
            identifier = null;
            PlanogramAssembly assembly = FixtureAssembly == null ? null : FixtureAssembly.GetPlanogramAssembly();
            if (assembly == null) return false;

            identifier = assembly.Name;
            return true;
        }

        /// <summary>
        ///     Try get the identifier for a product source. If the source is not a product, <c>null</c> will be returned.
        /// </summary>
        /// <param name="identifier">Will return the identifier for the source if it is a product, or null otherwise.</param>
        /// <returns></returns>
        private Boolean TryGetProductIdentifier(out String identifier)
        {
            const String productIdentifierMask = @"{0} : {1}";
            identifier = null;
            PlanogramProduct product = Position == null ? Product : Position.GetPlanogramProduct();
            if (product == null) return false;

            identifier = String.IsNullOrWhiteSpace(product.Name) ? product.Gtin : String.Format(productIdentifierMask, product.Gtin, product.Name);
            return true;
        }

        #endregion

        private String GetFullDescription()
        {
            switch (WarningType)
            {
                case PlanogramValidationWarningType.ProductAttributeDoesNotMatchMasterProductAttribute:
                    return String.Format(Message.Message_Enum_PlanogramValidationWarningType_ProductAttributeDoesNotMatchMasterProductAttribute, new Object[]
                    {
                        Regex.Replace(ProductAttributeComparisonResult.ComparedProductAttribute, _productAttributePropertyNameReplaceRegexString, "").Split('.').Last(),
                        HandleEmptyString(ProductAttributeComparisonResult.MasterDataValue),
                        HandleEmptyString(ProductAttributeComparisonResult.ProductValue)
                    });

                default:
                    return PlanogramValidationWarningTypeHelper.FriendlyNames[WarningType];
            }
            
        }

        private String GetShortDescription()
        {
            switch (WarningType)
            {
                case PlanogramValidationWarningType.ProductAttributeDoesNotMatchMasterProductAttribute:
                    return String.Format(Message.Message_Enum_PlanogramValidationWarningType_ProductAttributeDoesNotMatchMasterProductAttributeShortDescription, new Object[]
                    {
                         Regex.Replace(ProductAttributeComparisonResult.ComparedProductAttribute, _productAttributePropertyNameReplaceRegexString, "").Split('.').Last(),
                        HandleEmptyString (ProductAttributeComparisonResult.MasterDataValue),
                        HandleEmptyString (ProductAttributeComparisonResult.ProductValue)
                    });

                default:
                    return PlanogramValidationWarningTypeHelper.FriendlyShortNames[WarningType];

            }
        }

        private String HandleEmptyString(String value)
        {
            return String.IsNullOrEmpty(value) ? "Empty" : value;
        }

        #endregion

        #region IEquatable members

        public Boolean Equals(ValidationWarningRow other)
        {
            if (other == null) return false;
            if (other.WarningType != this.WarningType) return false;

            //AssemblyComponent
            if ((other.AssemblyComponent != null) && (this.AssemblyComponent != null))
            {
                if (!other.AssemblyComponent.Id.Equals(this.AssemblyComponent.Id)) { return false; }
            }
            if ((other.AssemblyComponent != null) && (this.AssemblyComponent == null)) { return false; }
            if ((other.AssemblyComponent == null) && (this.AssemblyComponent != null)) { return false; }

            //Fixture Assembly
            if ((other.FixtureAssembly != null) && (this.FixtureAssembly != null))
            {
                if (!other.FixtureAssembly.Id.Equals(this.FixtureAssembly.Id)) { return false; }
            }
            if ((other.FixtureAssembly != null) && (this.FixtureAssembly == null)) { return false; }
            if ((other.FixtureAssembly == null) && (this.FixtureAssembly != null)) { return false; }

            //Fixture Component
            if ((other.FixtureComponent != null) && (this.FixtureComponent != null))
            {
                if (!other.FixtureComponent.Id.Equals(this.FixtureComponent.Id)) { return false; }
            }
            if ((other.FixtureComponent != null) && (this.FixtureComponent == null)) { return false; }
            if ((other.FixtureComponent == null) && (this.FixtureComponent != null)) { return false; }


            //Position
            if ((other.Position != null) && (this.Position != null))
            {
                if (!other.Position.Id.Equals(this.Position.Id)) { return false; }
            }
            if ((other.Position != null) && (this.Position == null)) { return false; }
            if ((other.Position == null) && (this.Position != null)) { return false; }

            //Product
            if ((other.Product != null) && (this.Product != null))
            {
                if (!other.Product.Id.Equals(this.Product.Id)) { return false; }
            }
            if ((other.Product != null) && (this.Product == null)) { return false; }
            if ((other.Product == null) && (this.Product != null)) { return false; }

            return true;
        }

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current <see cref="T:System.Object" />.
        /// </returns>
        public override int GetHashCode()
        {
            unchecked
            {
                return (int)WarningType + (FixtureComponent != null ? FixtureComponent.GetHashCode() : 0) +
                       (AssemblyComponent != null ? AssemblyComponent.GetHashCode() : 0) +
                       (FixtureAssembly != null ? FixtureAssembly.GetHashCode() : 0) +
                       (Position != null ? Position.GetHashCode() : 0) +
                       (Product != null ? Product.GetHashCode() : 0);
            }
        }

        public static bool operator ==(ValidationWarningRow left, ValidationWarningRow right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ValidationWarningRow left, ValidationWarningRow right)
        {
            return !Equals(left, right);
        }


        /// <summary>
        ///     Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        ///     <see cref="T:System.Object" />.
        /// </summary>
        /// <returns>
        ///     true if the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />;
        ///     otherwise, false.
        /// </returns>
        /// <param name="obj">The <see cref="T:System.Object" /> to compare with the current <see cref="T:System.Object" />. </param>
        public override bool Equals(object obj)
        {
            var other = obj as ValidationWarningRow;
            return Equals(other);
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (IsDisposed) return;

            if (disposing)
            {
                // dispose resources here.
            }

            IsDisposed = true;
        }

        #endregion

        #region Static Helper Methods

        /// <summary>
        ///     Helper method to get the property path for the <see cref="ValidationWarningRow" /> property indicated by the
        ///     <paramref name="expression" />.
        /// </summary>
        /// <param name="expression">Lambda expression to access the property.</param>
        /// <returns>
        ///     Returns a <see cref="PropertyPath" /> Object with the path to the property in the
        ///     <paramref name="expression" />.
        /// </returns>
        private static PropertyPath GetPropertyPath(Expression<Func<ValidationWarningRow, Object>> expression)
        {
            return WpfHelper.GetPropertyPath(expression);
        }

        #endregion
    }
}
