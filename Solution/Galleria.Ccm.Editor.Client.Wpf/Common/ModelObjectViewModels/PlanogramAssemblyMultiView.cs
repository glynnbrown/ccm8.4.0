﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26149 : L.Hodson
//  Created
#endregion

#region Version History: (CCM 8.03)
// V8-29427 : L.Luong
//  Added Meta Data properties
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.Attributes;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Presents a merged view of multiple PlanogramAssemblyViews
    /// </summary>
    public sealed class PlanogramAssemblyMultiView : ViewModelObject, IDataErrorInfo
    {
        #region Fields

        private IEnumerable<PlanogramAssemblyView> _items;

        private PlanogramView _plan;
        private Boolean _isLoggingUndoableActions = true;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath NameProperty = GetPropertyPath(p => p.Name);
        public static readonly PropertyPath HeightProperty = GetPropertyPath(p => p.Height);
        public static readonly PropertyPath WidthProperty = GetPropertyPath(p => p.Width);
        public static readonly PropertyPath DepthProperty = GetPropertyPath(p => p.Depth);
        public static readonly PropertyPath WorldXProperty = GetPropertyPath(p => p.WorldX);
        public static readonly PropertyPath WorldYProperty = GetPropertyPath(p => p.WorldY);
        public static readonly PropertyPath WorldZProperty = GetPropertyPath(p => p.WorldZ);
        public static readonly PropertyPath WorldAngleProperty = GetPropertyPath(p => p.WorldAngle);
        public static readonly PropertyPath WorldSlopeProperty = GetPropertyPath(p => p.WorldSlope);
        public static readonly PropertyPath WorldRollProperty = GetPropertyPath(p => p.WorldRoll);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets whether changes should be logged as undoable actions.
        /// Note - this requires that a plan was specified on create.
        /// </summary>
        public Boolean IsLoggingUndoableActions
        {
            get { return _plan != null && _isLoggingUndoableActions; }
            set { _isLoggingUndoableActions = value; }
        }

        public IEnumerable<PlanogramAssemblyView> Items
        {
            get
            {
                return _items;
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramAssembly), "NameProperty")]
        public String Name
        {
            get
            {
                if (Items.Count() == 1)
                {
                    return Items.First().Name;
                }
                else
                {
                    return "<Multiple>";
                }
            }
            set
            {
                if (Items.Count() == 1)
                {
                    SetPropertyValue(Items, PlanogramAssembly.NameProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramAssembly), "HeightProperty")]
        public Single? Height
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAssembly.HeightProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramAssembly), "WidthProperty")]
        public Single? Width
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAssembly.WidthProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramAssembly), "DepthProperty")]
        public Single? Depth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAssembly.DepthProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "XProperty")]
        public Single? WorldX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAssemblyView.WorldXProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramAssemblyView.WorldXProperty.Path, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "YProperty")]
        public Single? WorldY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAssemblyView.WorldYProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramAssemblyView.WorldYProperty.Path, value);
                    foreach (var s in this.Items)
                    {
                        s.Y = value.Value;
                    }
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "ZProperty")]
        public Single? WorldZ
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAssemblyView.WorldZProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramAssemblyView.WorldZProperty.Path, value);
                    foreach (var s in this.Items)
                    {
                        s.Z = value.Value;
                    }
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "AngleProperty")]
        public Single? WorldAngle
        {
            get
            {
                Single? value = GetCommonPropertyValue<Single?>(Items, PlanogramAssemblyView.WorldAngleProperty.Path);

                //convert to degrees
                if (value.HasValue)
                {
                    value = Convert.ToSingle(CommonHelper.ToDegrees(value.Value));
                }

                return value;
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramAssemblyView.WorldAngleProperty.Path,
                        Convert.ToSingle(CommonHelper.ToRadians(value.Value)));
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "SlopeProperty")]
        public Single? WorldSlope
        {
            get
            {
                Single? value = GetCommonPropertyValue<Single?>(Items, PlanogramAssemblyView.WorldSlopeProperty.Path);

                //convert to degrees
                if (value.HasValue)
                {
                    value = Convert.ToSingle(CommonHelper.ToDegrees(value.Value));
                }

                return value;
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramAssemblyView.WorldSlopeProperty.Path,
                        Convert.ToSingle(CommonHelper.ToRadians(value.Value)));
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "RollProperty")]
        public Single? WorldRoll
        {
            get
            {
                Single? value = GetCommonPropertyValue<Single?>(Items, PlanogramAssemblyView.WorldRollProperty.Path);

                //convert to degrees
                if (value.HasValue)
                {
                    value = Convert.ToSingle(CommonHelper.ToDegrees(value.Value));
                }

                return value;
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramAssemblyView.WorldRollProperty.Path,
                        Convert.ToSingle(CommonHelper.ToRadians(value.Value)));
                }
            }
        }

        #region Meta Data Properties

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaComponentCountProperty")]
        public Int32? MetaComponentCount
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramAssemblyView.MetaComponentCountProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaTotalMerchandisableLinearSpaceProperty")]
        public Single? MetaTotalMerchandisableLinearSpace
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAssemblyView.MetaTotalMerchandisableLinearSpaceProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaTotalMerchandisableAreaSpaceProperty")]
        public Single? MetaTotalMerchandisableAreaSpace
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAssemblyView.MetaTotalMerchandisableAreaSpaceProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaTotalMerchandisableVolumetricSpaceProperty")]
        public Single? MetaTotalMerchandisableVolumetricSpace
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAssemblyView.MetaTotalMerchandisableVolumetricSpaceProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaTotalLinearWhiteSpaceProperty")]
        public Single? MetaTotalLinearWhiteSpace
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAssemblyView.MetaTotalLinearWhiteSpaceProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaTotalAreaWhiteSpaceProperty")]
        public Single? MetaTotalAreaWhiteSpace
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAssemblyView.MetaTotalAreaWhiteSpaceProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaTotalVolumetricWhiteSpaceProperty")]
        public Single? MetaTotalVolumetricWhiteSpace
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAssemblyView.MetaTotalVolumetricWhiteSpaceProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaProductsPlacedProperty")]
        public Int32? MetaProductsPlaced
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramAssemblyView.MetaProductsPlacedProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaNewProductsProperty")]
        public Int32? MetaNewProducts
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramAssemblyView.MetaNewProductsProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaChangesFromPreviousCountProperty")]
        public Int32? MetaChangesFromPreviousCount
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramAssemblyView.MetaChangesFromPreviousCountProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaChangeFromPreviousStarRatingProperty")]
        public Int32? MetaChangeFromPreviousStarRating
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramAssemblyView.MetaChangeFromPreviousStarRatingProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaBlocksDroppedProperty")]
        public Int32? MetaBlocksDropped
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramAssemblyView.MetaBlocksDroppedProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaNotAchievedInventoryProperty")]
        public Int32? MetaNotAchievedInventory
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramAssemblyView.MetaNotAchievedInventoryProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaTotalFacingsProperty")]
        public Int32? MetaTotalFacings
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramAssemblyView.MetaTotalFacingsProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaAverageFacingsProperty")]
        public Int16? MetaAverageFacings
        {
            get
            {
                return GetCommonPropertyValue<Int16?>(Items, PlanogramAssemblyView.MetaAverageFacingsProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaTotalUnitsProperty")]
        public Int32? MetaTotalUnits
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramAssemblyView.MetaTotalUnitsProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaAverageUnitsProperty")]
        public Int32? MetaAverageUnits
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramAssemblyView.MetaAverageUnitsProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaMinDosProperty")]
        public Single? MetaMinDos
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAssemblyView.MetaMinDosProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaMaxDosProperty")]
        public Single? MetaMaxDos
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAssemblyView.MetaMaxDosProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaAverageDosProperty")]
        public Single? MetaAverageDos
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAssemblyView.MetaAverageDosProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaMinCasesProperty")]
        public Single? MetaMinCases
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAssemblyView.MetaMinCasesProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaAverageCasesProperty")]
        public Single? MetaAverageCases
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAssemblyView.MetaAverageCasesProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaMaxCasesProperty")]
        public Single? MetaMaxCases
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAssemblyView.MetaMaxCasesProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaSpaceToUnitsIndexProperty")]
        public Int16? MetaSpaceToUnitsIndex
        {
            get
            {
                return GetCommonPropertyValue<Int16?>(Items, PlanogramAssemblyView.MetaSpaceToUnitsIndexProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaTotalFrontFacingsProperty")]
        public Int16? MetaTotalFrontFacings
        {
            get
            {
                return GetCommonPropertyValue<Int16?>(Items, PlanogramAssemblyView.MetaTotalFrontFacingsProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureAssembly), "MetaAverageFrontFacingsProperty")]
        public Single? MetaAverageFrontFacings
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAssemblyView.MetaAverageFrontFacingsProperty.Path);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="items"></param>
        public PlanogramAssemblyMultiView(IEnumerable<PlanogramAssemblyView> items)
        {
            _items = items.ToList();
            _plan = items.First().Planogram;

            foreach (var i in items)
            {
                i.PropertyChanged += PlanItem_PropertyChanged;
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on any linked plan item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(e.PropertyName);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the common value of the given property for all the items
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        private static T GetCommonPropertyValue<T>(IEnumerable<Object> items, String propertyName)
        {
            T value = default(T);

            if (items.Any())
            {
                Type classType = items.First().GetType();
                System.Reflection.PropertyInfo pInfo = classType.GetProperty(propertyName);
                if (pInfo != null)
                {
                    Boolean isFirst = true;
                    foreach (Object item in items)
                    {
                        T itemValue = (T)pInfo.GetValue(item, null);
                        if (isFirst)
                        {
                            value = itemValue;
                        }
                        else if (!Object.Equals(itemValue, value))
                        {
                            return default(T);
                        }

                        isFirst = false;
                    }
                }
            }

            return value;

        }

        /// <summary>
        /// Sets the given value against
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        private void SetPropertyValue<T>(IEnumerable<Object> items, String propertyName, T value)
        {
            if (items.Any())
            {

                //Start logging the change
                Boolean isProcessingUndoable = false;
                if (IsLoggingUndoableActions)
                {
                    if (!_plan.IsRecordingUndoableAction)
                    {
                        isProcessingUndoable = true;
                        _plan.BeginUndoableAction(propertyName);
                    }
                }

                //make the property change
                Type classType = items.First().GetType();
                System.Reflection.PropertyInfo pInfo = classType.GetProperty(propertyName);

                if (pInfo != null)
                {
                    foreach (Object item in items)
                    {
                        pInfo.SetValue(item, value, null);
                    }
                }

                //Commit the log.
                if (isProcessingUndoable)
                {
                    _plan.EndUndoableAction();
                }
            }
        }


        #endregion

        #region IDataErrorInfo Members

        public String Error
        {
            get { throw new NotImplementedException(); }
        }

        public String this[String columnName]
        {
            get
            {
                return ((IDataErrorInfo)_items.First())[columnName];
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                foreach (var i in _items)
                {
                    i.PropertyChanged -= PlanItem_PropertyChanged;
                }
                base.IsDisposed = true;
            }
        }

        #endregion

        #region Static Helpers

        private static PropertyPath GetPropertyPath(System.Linq.Expressions.Expression<Func<PlanogramAssemblyMultiView, Object>> expression)
        {
            return WpfHelper.GetPropertyPath<PlanogramAssemblyMultiView>(expression);
        }

        #endregion
    }
}
