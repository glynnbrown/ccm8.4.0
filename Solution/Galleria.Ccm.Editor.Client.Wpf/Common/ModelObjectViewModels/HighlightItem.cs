﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-24801 L.Hodson
//  Created
// V8-25436 : L.Luong
//  highlight now takes in a dal type
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Readonly UI view of a Highlight
    /// </summary>
    public sealed class HighlightItem : IPlanogramHighlight
    {
        #region Properties

        public Object Id { get; private set; }
        public String Name { get; private set; }
        public HighlightType Type { get; private set; }
        public HighlightMethodType MethodType { get; private set; }
        public Boolean IsFilterEnabled { get; private set; }
        public Boolean IsPreFilter { get; private set; }
        public Boolean IsAndFilter { get; private set; }
        public String Field1 { get; private set; }
        public String Field2 { get; private set; }
        public HighlightQuadrantType QuadrantXType { get; private set; }
        public Single QuadrantXConstant { get; private set; }
        public HighlightQuadrantType QuadrantYType { get; private set; }
        public Single QuadrantYConstant { get; private set; }

        public IEnumerable<HighlightGroupItem> Groups { get; private set; }
        public IEnumerable<HighlightFilterItem> Filters { get; private set; }
        public IEnumerable<HighlightCharacteristicItem> Characteristics { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="model"></param>
        public HighlightItem(Highlight model)
            :this(model, model.Id)
        {
        }

        /// <summary>
        /// Creates a new instance of this type from the given model.
        /// </summary>
        /// <param name="model"></param>
        public HighlightItem(Highlight model, Object itemId)
        {
            this.Id = itemId;
            this.Name = model.Name;
            this.Type = model.Type;
            this.MethodType = model.MethodType;
            this.IsFilterEnabled = model.IsFilterEnabled;
            this.IsPreFilter = model.IsPreFilter;
            this.IsAndFilter = model.IsAndFilter;
            this.Field1 = model.Field1;
            this.Field2 = model.Field2;
            this.QuadrantXType = model.QuadrantXType;
            this.QuadrantXConstant = model.QuadrantXConstant;
            this.QuadrantYType = model.QuadrantYType;
            this.QuadrantYConstant = model.QuadrantYConstant;

            List<HighlightGroupItem> groupList = new List<HighlightGroupItem>(model.Groups.Count);
            foreach (HighlightGroup r in model.Groups)
            {
                groupList.Add(new HighlightGroupItem(r));
            }
            this.Groups = groupList;

            List<HighlightCharacteristicItem> characteristicList = new List<HighlightCharacteristicItem>(model.Characteristics.Count);
            foreach (HighlightCharacteristic r in model.Characteristics)
            {
                characteristicList.Add(new HighlightCharacteristicItem(r));
            }
            this.Characteristics = characteristicList;

            List<HighlightFilterItem> filterList = new List<HighlightFilterItem>(model.Filters.Count);
            foreach (HighlightFilter r in model.Filters)
            {
                filterList.Add(new HighlightFilterItem(r));
            }
            this.Filters = filterList;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a new Highlight model from this item.
        /// </summary>
        /// <returns></returns>
        public Highlight ToHighlight()
        {
            Highlight model = Highlight.NewHighlight();

            model.Name = this.Name;
            model.Type = this.Type;
            model.MethodType = this.MethodType;
            model.IsFilterEnabled = this.IsFilterEnabled;
            model.IsPreFilter = this.IsPreFilter;
            model.IsAndFilter = this.IsAndFilter;
            model.Field1 = this.Field1;
            model.Field2 = this.Field2;
            model.QuadrantXType = this.QuadrantXType;
            model.QuadrantXConstant = this.QuadrantXConstant;
            model.QuadrantYType = this.QuadrantYType;
            model.QuadrantYConstant = this.QuadrantYConstant;

            foreach (var f in this.Filters)
            {
                model.Filters.Add(f.ToHighlightFilter());
            }

            foreach (var g in this.Groups)
            {
                model.Groups.Add(g.ToHighlightGroup());
            }

            foreach (var c in this.Characteristics)
            {
                model.Characteristics.Add(c.ToHighlightCharacteristic());
            }

            return model;
        }

        #endregion


        PlanogramHighlightMethodType IPlanogramHighlight.MethodType
        {
            get
            {
                return EnumHelper.Parse<PlanogramHighlightMethodType>(
                    MethodType.ToString(), PlanogramHighlightMethodType.Group);
            }
        }

        PlanogramHighlightQuadrantType IPlanogramHighlight.QuadrantXType
        {
            get
            {
                return EnumHelper.Parse<PlanogramHighlightQuadrantType>(
                    QuadrantXType.ToString(), PlanogramHighlightQuadrantType.Mean);
            }
        }

        PlanogramHighlightQuadrantType IPlanogramHighlight.QuadrantYType
        {
            get
            {
                return EnumHelper.Parse<PlanogramHighlightQuadrantType>(
                    QuadrantYType.ToString(), PlanogramHighlightQuadrantType.Mean);
            }
        }

        IEnumerable<IPlanogramHighlightGroup> IPlanogramHighlight.Groups
        {
            get { return Groups; }
        }

        IEnumerable<IPlanogramHighlightFilter> IPlanogramHighlight.Filters
        {
            get { return Filters; }
        }

        IEnumerable<IPlanogramHighlightCharacteristic> IPlanogramHighlight.Characteristics
        {
            get { return Characteristics; }
        }

        public void ClearGroups()
        {
            ((List<HighlightGroupItem>)this.Groups).Clear();
        }
    }

    /// <summary>
    /// UI view of a HighlightGroup
    /// </summary>
    public sealed class HighlightGroupItem : IPlanogramHighlightGroup
    {
        #region Properties

        public Int32 Order { get; private set; }
        public String Name { get; private set; }
        public String DisplayName { get; private set; }
        public Int32 FillColour { get; private set; }
        public HighlightFillPatternType FillPatternType { get; private set; }

        #endregion

        #region Constructor

        public HighlightGroupItem(HighlightGroup group)
        {
            Order = group.Order;
            Name = group.Name;
            DisplayName = group.DisplayName;
            FillColour = group.FillColour;
            FillPatternType = group.FillPatternType;
        }

        #endregion

        #region Methods

        public HighlightGroup ToHighlightGroup()
        {
            HighlightGroup group = HighlightGroup.NewHighlightGroup();

            group.Order = Order;
            group.Name = Name;
            group.DisplayName = DisplayName;
            group.FillColour = FillColour;
            group.FillPatternType = FillPatternType;

            return group;
        }

        #endregion


        PlanogramHighlightFillPatternType IPlanogramHighlightGroup.FillPatternType
        {
            get { return EnumHelper.Parse<PlanogramHighlightFillPatternType>(
                FillPatternType.ToString(), PlanogramHighlightFillPatternType.Solid); }
        }
    }

    /// <summary>
    /// UI view of a HighlightFilter
    /// </summary>
    public sealed class HighlightFilterItem : IPlanogramHighlightFilter
    {
        #region Properties

        public String Field { get; private set; }
        public HighlightFilterType Type { get; private set; }
        public String Value { get; private set; }

        #endregion

        #region Constructor

        public HighlightFilterItem(HighlightFilter filter)
        {
            Field = filter.Field;
            Type = filter.Type;
            Value = filter.Value;
        }

        #endregion

        #region Methods

        public HighlightFilter ToHighlightFilter()
        {
            HighlightFilter filter = HighlightFilter.NewHighlightFilter();

            filter.Field = Field;
            filter.Type = Type;
            filter.Value = Value;

            return filter;
        }

        #endregion


        PlanogramHighlightFilterType IPlanogramHighlightFilter.Type
        {
            get { return EnumHelper.Parse<PlanogramHighlightFilterType>(
                Type.ToString(), PlanogramHighlightFilterType.Equals); }
        }
    }

    /// <summary>
    /// UI view of a HighlightCharacteristic
    /// </summary>
    public sealed class HighlightCharacteristicItem : IPlanogramHighlightCharacteristic
    {
        #region Properties

        public String Name { get; private set; }
        public Boolean IsAndFilter { get; private set; }
        public IEnumerable<HighlightCharacteristicRuleItem> Rules { get; private set; }

        #endregion

        #region Constructor

        public HighlightCharacteristicItem(HighlightCharacteristic model)
        {
            Name = model.Name;
            IsAndFilter = model.IsAndFilter;

            List<HighlightCharacteristicRuleItem> ruleList = new List<HighlightCharacteristicRuleItem>(model.Rules.Count);
            foreach (HighlightCharacteristicRule r in model.Rules)
            {
                ruleList.Add(new HighlightCharacteristicRuleItem(r));
            }
            this.Rules = ruleList;
        }

        #endregion

        #region Methods

        public HighlightCharacteristic ToHighlightCharacteristic()
        {
            HighlightCharacteristic model = HighlightCharacteristic.NewHighlightCharacteristic();

            model.Name = Name;
            model.IsAndFilter = IsAndFilter;

            foreach (HighlightCharacteristicRuleItem r in this.Rules)
            {
                model.Rules.Add(r.ToHighlightCharacteristicRule());
            }

            return model;
        }

        #endregion


        IEnumerable<IPlanogramHighlightCharacteristicRule> IPlanogramHighlightCharacteristic.Rules
        {
            get { return Rules; }
        }
    }

    /// <summary>
    /// UI view of a HighlightCharacteristicRule
    /// </summary>
    public sealed class HighlightCharacteristicRuleItem : IPlanogramHighlightCharacteristicRule
    {
         #region Properties

        public String Field { get; private set; }
        public HighlightCharacteristicRuleType Type { get; private set; }
        public String Value { get; private set; }

        #endregion

        #region Constructor

        public HighlightCharacteristicRuleItem(HighlightCharacteristicRule model)
        {
            Field = model.Field;
            Type = model.Type;
            Value = model.Value;
        }

        #endregion

        #region Methods

        public HighlightCharacteristicRule ToHighlightCharacteristicRule()
        {
            HighlightCharacteristicRule model = HighlightCharacteristicRule.NewHighlightCharacteristicRule();

            model.Field = Field;
            model.Type = Type;
            model.Value = Value;

            return model;
        }

        #endregion

        PlanogramHighlightCharacteristicRuleType IPlanogramHighlightCharacteristicRule.Type
        {
            get { return EnumHelper.Parse<PlanogramHighlightCharacteristicRuleType>(
                Type.ToString(), PlanogramHighlightCharacteristicRuleType.Equals); }
        }
    }


}
