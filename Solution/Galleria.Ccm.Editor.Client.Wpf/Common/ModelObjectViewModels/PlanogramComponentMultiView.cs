﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26149 : L.Hodson
//  Created
// V8-26442 :I.George
// Added CustomAttribute
// V8-27570 : A.Silva
//      Added ComponentSequenceNumber.

#endregion
#region Version History: (CCM 8.01)
// V8-28344 : L.Luong
//  Added Line properties

#endregion
#region Version History: (CCM 8.03)
// V8-29427 : L.Luong
//  Added Meta Data properties
#endregion
#region Version History: (CCM 8.1.0)
// V8-29446 : M.Pettit
//  Manually entered values of 0 for Height,Width,Depth are ignored
#endregion
#region Version History: (CCM 8.2)
// V8-30936 : M.Brumby
//  Added slotwall
#endregion
#region Version History: (CCM 8.3)
// V8-32521 : J.Pickup
//  Added MetaIsOutsideOfFixtureArea
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Csla;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Attributes;
using Galleria.Framework.Controls.Wpf.PropertyEditor;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using static Galleria.Ccm.Common.Wpf.Helpers.CommonHelper;
using static Galleria.Ccm.Common.Wpf.Resources.Language.Message;
using CustomAttributeData = Galleria.Framework.Planograms.Model.CustomAttributeData;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Provides a merged view of multiple PlanogramComponentViews
    /// </summary>
    public sealed class PlanogramComponentMultiView : ViewModelObject, IDataErrorInfo
    {
        #region Fields

        private IEnumerable<PlanogramComponentView> _items;

        private PlanogramView _plan;
        private Boolean _isLoggingUndoableActions = true;

        private readonly CustomAttributeDataMultiView _customAttributesView;

        private Boolean _canUserEditSequenceNumber;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath NameProperty = GetPropertyPath(p => p.Name);
        public static readonly PropertyPath HeightProperty = GetPropertyPath(p => p.Height);
        public static readonly PropertyPath WidthProperty = GetPropertyPath(p => p.Width);
        public static readonly PropertyPath DepthProperty = GetPropertyPath(p => p.Depth);
        public static readonly PropertyPath MetaWorldXProperty = GetPropertyPath(p => p.MetaWorldX);
        public static readonly PropertyPath MetaWorldYProperty = GetPropertyPath(p => p.MetaWorldY);
        public static readonly PropertyPath MetaWorldZProperty = GetPropertyPath(p => p.MetaWorldZ);
        public static readonly PropertyPath MetaWorldAngleProperty = GetPropertyPath(p => p.MetaWorldAngle);
        public static readonly PropertyPath MetaWorldSlopeProperty = GetPropertyPath(p => p.MetaWorldSlope);
        public static readonly PropertyPath MetaWorldRollProperty = GetPropertyPath(p => p.MetaWorldRoll);
        public static readonly PropertyPath IsMoveableProperty = GetPropertyPath(p => p.IsMoveable);
        public static readonly PropertyPath IsDisplayOnlyProperty = GetPropertyPath(p => p.IsDisplayOnly);
        public static readonly PropertyPath CanAttachShelfEdgeLabelProperty = GetPropertyPath(p => p.CanAttachShelfEdgeLabel);
        public static readonly PropertyPath RetailerReferenceCodeProperty = GetPropertyPath(p => p.RetailerReferenceCode);
        public static readonly PropertyPath BarcodeProperty = GetPropertyPath(p => p.Barcode);
        public static readonly PropertyPath ManufacturerProperty = GetPropertyPath(p => p.Manufacturer);
        public static readonly PropertyPath ManufacturerPartNameProperty = GetPropertyPath(p => p.ManufacturerPartName);
        public static readonly PropertyPath ManufacturerPartNumberProperty = GetPropertyPath(p => p.ManufacturerPartNumber);
        public static readonly PropertyPath SupplierNameProperty = GetPropertyPath(p => p.SupplierName);
        public static readonly PropertyPath SupplierPartNumberProperty = GetPropertyPath(p => p.SupplierPartNumber);
        public static readonly PropertyPath SupplierCostPriceProperty = GetPropertyPath(p => p.SupplierCostPrice);
        public static readonly PropertyPath SupplierDiscountProperty = GetPropertyPath(p => p.SupplierDiscount);
        public static readonly PropertyPath MinPurchaseQtyProperty = GetPropertyPath(p => p.MinPurchaseQty);
        public static readonly PropertyPath WeightLimitProperty = GetPropertyPath(p => p.WeightLimit);
        public static readonly PropertyPath WeightProperty = GetPropertyPath(p => p.Weight);
        public static readonly PropertyPath VolumeProperty = GetPropertyPath(p => p.Volume);
        public static readonly PropertyPath DiameterProperty = GetPropertyPath(p => p.Diameter);
        public static readonly PropertyPath CapacityProperty = GetPropertyPath(p => p.Capacity);
        public static readonly PropertyPath ComponentTypeProperty = GetPropertyPath(p => p.ComponentType);
        public static readonly PropertyPath FillColourProperty = GetPropertyPath(p => p.FillColour);
        public static readonly PropertyPath LineColourProperty = GetPropertyPath(p => p.LineColour);
        public static readonly PropertyPath LineThicknessProperty = GetPropertyPath(p => p.LineThickness);
        public static readonly PropertyPath NotchNumberProperty = GetPropertyPath(p => p.NotchNumber);
        public static readonly PropertyPath IsNotchNumberPropertyVisibleProperty = GetPropertyPath(p => p.IsNotchNumberPropertyVisible);
        public static readonly PropertyPath ComponentSequenceNumberProperty = GetPropertyPath(p => p.ComponentSequenceNumber);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets whether changes should be logged as undoable actions.
        /// Note - this requires that a plan was specified on create.
        /// </summary>
        public Boolean IsLoggingUndoableActions
        {
            get { return _plan != null && _isLoggingUndoableActions; }
            set { _isLoggingUndoableActions = value; }
        }

        public IEnumerable<PlanogramComponentView> Items
        {
            get
            {
                return _items;
            }
        }

        /// <summary>
        /// Returns the number of items merged by this view.
        /// </summary>
        public Int32 Count
        {
            get { return Items.Count(); }
        }

        /// <summary>
        /// Returns true if the user can edit sequence numbers.
        /// </summary>
        public Boolean CanUserEditSequenceNumber
        {
            get { return _canUserEditSequenceNumber; }
            private set 
            { 
                _canUserEditSequenceNumber = value;
                OnPropertyChanged("CanUserEditSequenceNumber");
            }
        }

        #region Model Properties

        [ModelObjectDisplayName(typeof(PlanogramComponent), "NameProperty")]
        public String Name
        {
            get
            {
                if (Items.Count() == 1)
                {
                    return Items.First().Name;
                }
                else
                {
                    return "<Multiple>";
                }
            }
            set
            {
                if (Items.Count() == 1)
                {
                    SetPropertyValue(Items, PlanogramComponent.NameProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponent), "ComponentTypeProperty")]
        [PropertyEditorItemsSource(typeof(PropertyEditorItemsSource<PlanogramComponentType>))]
        public PlanogramComponentType? ComponentType
        {
            get
            {
                return GetCommonPropertyValue<PlanogramComponentType?>(Items, PlanogramComponent.ComponentTypeProperty.Name);
            }
            private set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramComponent.ComponentTypeProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponent), "HeightProperty")]
        public Single? Height
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramComponent.HeightProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    if (!value.Value.Equals(0F))
                    {
                        SetPropertyValue(this.Items, PlanogramComponent.HeightProperty.Name, value.Value);
                    }
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponent), "WidthProperty")]
        public Single? Width
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramComponent.WidthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    if (!value.Value.Equals(0F))
                    {
                        SetPropertyValue(this.Items, PlanogramComponent.WidthProperty.Name, value.Value);
                    }
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponent), "DepthProperty")]
        public Single? Depth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramComponent.DepthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    if (!value.Value.Equals(0F))
                    {
                        SetPropertyValue(this.Items, PlanogramComponent.DepthProperty.Name, value.Value);
                    }
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaWorldXProperty")]
        public Single? MetaWorldX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureComponent.MetaWorldXProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(this.Items, PlanogramFixtureComponent.MetaWorldXProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaWorldYProperty")]
        public Single? MetaWorldY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureComponent.MetaWorldYProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(this.Items, PlanogramFixtureComponent.MetaWorldYProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaWorldZProperty")]
        public Single? MetaWorldZ
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureComponent.MetaWorldZProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(this.Items, PlanogramFixtureComponent.MetaWorldZProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaWorldAngleProperty")]
        public Single? MetaWorldAngle
        {
            get
            {
                Single? value = GetCommonPropertyValue<Single?>(Items, PlanogramFixtureComponent.MetaWorldAngleProperty.Name);

                //convert to degrees
                if (value.HasValue)
                {
                    value = Convert.ToSingle(ToDegrees(value.Value));
                }

                return value;
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramFixtureComponent.MetaWorldAngleProperty.Name,
                        Convert.ToSingle(ToRadians(value.Value)));
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaWorldSlopeProperty")]
        public Single? MetaWorldSlope
        {
            get
            {
                Single? value = GetCommonPropertyValue<Single?>(Items, PlanogramFixtureComponent.MetaWorldSlopeProperty.Name);

                //convert to degrees
                if (value.HasValue)
                {
                    value = Convert.ToSingle(ToDegrees(value.Value));
                }


                return value;
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramFixtureComponent.MetaWorldSlopeProperty.Name,
                        Convert.ToSingle(ToRadians(value.Value)));
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaWorldRollProperty")]
        public Single? MetaWorldRoll
        {
            get
            {
                Single? value = GetCommonPropertyValue<Single?>(Items, PlanogramFixtureComponent.MetaWorldRollProperty.Name);

                //convert to degrees
                if (value.HasValue)
                {
                    value = Convert.ToSingle(ToDegrees(value.Value));
                }

                return value;
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramFixtureComponent.MetaWorldRollProperty.Name,
                      Convert.ToSingle(ToRadians(value.Value)));
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaIsOutsideOfFixtureAreaProperty")]
        public Boolean? MetaIsOutsideOfFixtureArea
        {
            get
            {
                Boolean? value = GetCommonPropertyValue<Boolean?>(Items, PlanogramFixtureComponent.MetaIsOutsideOfFixtureAreaProperty.Name);
                return value;
            }
            set
            {
                    SetPropertyValue(Items, PlanogramFixtureComponent.MetaIsOutsideOfFixtureAreaProperty.Name, value.Value);
            }
        }
        

        [ModelObjectDisplayName(typeof(PlanogramComponent), "IsMoveableProperty")]
        public Boolean? IsMoveable
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramComponent.IsMoveableProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramComponent.IsMoveableProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponent), "IsDisplayOnlyProperty")]
        public Boolean? IsDisplayOnly
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramComponent.IsDisplayOnlyProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramComponent.IsDisplayOnlyProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponent), "CanAttachShelfEdgeLabelProperty")]
        public Boolean? CanAttachShelfEdgeLabel
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramComponent.CanAttachShelfEdgeLabelProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramComponent.CanAttachShelfEdgeLabelProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponent), "RetailerReferenceCodeProperty")]
        public String RetailerReferenceCode
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramComponent.RetailerReferenceCodeProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramComponent.RetailerReferenceCodeProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponent), "BarCodeProperty")]
        public String Barcode
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramComponent.BarCodeProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramComponent.BarCodeProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponent), "ManufacturerProperty")]
        public String Manufacturer
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramComponent.ManufacturerProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramComponent.ManufacturerProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponent), "ManufacturerPartNameProperty")]
        public String ManufacturerPartName
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramComponent.ManufacturerPartNameProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramComponent.ManufacturerPartNameProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponent), "ManufacturerPartNumberProperty")]
        public String ManufacturerPartNumber
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramComponent.ManufacturerPartNumberProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramComponent.ManufacturerPartNumberProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponent), "SupplierNameProperty")]
        public String SupplierName
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramComponent.SupplierNameProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramComponent.SupplierNameProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponent), "SupplierPartNumberProperty")]
        public String SupplierPartNumber
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramComponent.SupplierPartNumberProperty.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramComponent.SupplierPartNumberProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponent), "SupplierCostPriceProperty")]
        public Single? SupplierCostPrice
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramComponent.SupplierCostPriceProperty.Name);
            }
            set
            {
                SetPropertyValue(Items, PlanogramComponent.SupplierCostPriceProperty.Name, value);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponent), "SupplierDiscountProperty")]
        public Single? SupplierDiscount
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramComponent.SupplierDiscountProperty.Name);
            }
            set
            {
                SetPropertyValue(Items, PlanogramComponent.SupplierDiscountProperty.Name, value);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponent), "MinPurchaseQtyProperty")]
        public Int32? MinPurchaseQty
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramComponent.MinPurchaseQtyProperty.Name);
            }
            set
            {
                SetPropertyValue(Items, PlanogramComponent.MinPurchaseQtyProperty.Name, value);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponent), "WeightLimitProperty")]
        public Single? WeightLimit
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramComponent.WeightLimitProperty.Name);
            }
            set
            {
                SetPropertyValue(Items, PlanogramComponent.WeightLimitProperty.Name, value);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponent), "WeightProperty")]
        public Single? Weight
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramComponent.WeightProperty.Name);
            }
            set
            {
                SetPropertyValue(Items, PlanogramComponent.WeightProperty.Name, value);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponent), "VolumeProperty")]
        public Single? Volume
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramComponent.VolumeProperty.Name);
            }
            set
            {
                SetPropertyValue(Items, PlanogramComponent.VolumeProperty.Name, value);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponent), "DiameterProperty")]
        public Single? Diameter
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramComponent.DiameterProperty.Name);
            }
            set
            {
                SetPropertyValue(Items, PlanogramComponent.DiameterProperty.Name, value);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponent), "CapacityProperty")]
        public Int16? Capacity
        {
            get
            {
                return GetCommonPropertyValue<Int16?>(Items, PlanogramComponent.CapacityProperty.Name);
            }
            set
            {
                SetPropertyValue(Items, PlanogramComponent.CapacityProperty.Name, value);
            }
        }

        [DisplayName("Fill Colour")]
        public Color FillColour
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, "FillColour");

                if (val.HasValue)
                {
                    return IntToColor(val.Value);
                }
                else
                {
                    return Colors.White;
                }
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, "FillColour", ColorToInt(value));
                }
            }
        }

        [DisplayName("Line Colour")]
        public Color LineColour
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, "LineColour");

                if (val.HasValue)
                {
                    return IntToColor(val.Value);
                }
                else
                {
                    return Colors.White;
                }
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, "LineColour", ColorToInt(value));
                }
            }
        }

        [DisplayName("Line Thickness")]
        public Single? LineThickness
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, "LineThickness");
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, "LineThickness", value);
                }
            }
        }

        [DisplayName("Image")]
        public PlanogramImage Image
        {
            get
            {
                PlanogramImage value = null;
                if (this.Items.Any())
                {
                    value = this.Items.First().SubComponentImage;
                    if (this.Items.Any(s => s.SubComponentImage != value))
                    {
                        value = null;
                    }
                }
                return value;
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, "SubComponentImage", value);
                }
            }
        }

        [LocalisedDisplayName(typeof(Message), "PlanogramComponentView_NotchNumber")]
        public Int32? NotchNumber
        {
            get
            {
                Int32? value = null;
                if (this.Items.Any())
                {
                    value = this.Items.First().NotchNumber;
                    if (this.Items.Any(s => s.NotchNumber != value))
                    {
                        value = null;
                    }
                }
                return value;
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, "NotchNumber", value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "ComponentSequenceNumberProperty")]
        public Int16? ComponentSequenceNumber
        {
            get { return GetCommonPropertyValue<Int16?>(Items, PlanogramFixtureComponent.ComponentSequenceNumberProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramFixtureComponent.ComponentSequenceNumberProperty.Name, value.Value);
                }
            }
        }

        #endregion

        #region Visibility Properties

        /// <summary>
        /// Returns true if the notch number property
        /// should be displayed for this selection
        /// </summary>
        public Boolean IsNotchNumberPropertyVisible
        {
            get
            {
                return
                    this.Items.All(c =>
                        c.ComponentType == PlanogramComponentType.Peg
                        || c.ComponentType == PlanogramComponentType.Shelf
                        || c.ComponentType == PlanogramComponentType.Bar
                        || c.ComponentType == PlanogramComponentType.Custom 
                        || c.ComponentType == PlanogramComponentType.SlotWall);
            }
        }

        #endregion

        #region Meta Data Properties

        [ModelObjectDisplayName(typeof(PlanogramComponent), "MetaNumberOfSubComponentsProperty")]
        public Int16? MetaNumberOfSubComponents
        {
            get
            {
                return GetCommonPropertyValue<Int16?>(Items, PlanogramComponent.MetaNumberOfSubComponentsProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponent), "MetaNumberOfMerchandisedSubComponentsProperty")]
        public Int16? MetaNumberOfMerchandisedSubComponents
        {
            get
            {
                return GetCommonPropertyValue<Int16?>(Items, PlanogramComponent.MetaNumberOfMerchandisedSubComponentsProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaTotalMerchandisableLinearSpaceProperty")]
        public Single? MetaTotalMerchandisableLinearSpace
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureComponent.MetaTotalMerchandisableLinearSpaceProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaTotalMerchandisableAreaSpaceProperty")]
        public Single? MetaTotalMerchandisableAreaSpace
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureComponent.MetaTotalMerchandisableAreaSpaceProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaTotalMerchandisableVolumetricSpaceProperty")]
        public Single? MetaTotalMerchandisableVolumetricSpace
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureComponent.MetaTotalMerchandisableVolumetricSpaceProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaTotalLinearWhiteSpaceProperty")]
        public Single? MetaTotalLinearWhiteSpace
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureComponent.MetaTotalLinearWhiteSpaceProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaTotalAreaWhiteSpaceProperty")]
        public Single? MetaTotalAreaWhiteSpace
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureComponent.MetaTotalAreaWhiteSpaceProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaTotalVolumetricWhiteSpaceProperty")]
        public Single? MetaTotalVolumetricWhiteSpace
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureComponent.MetaTotalVolumetricWhiteSpaceProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaProductsPlacedProperty")]
        public Int32? MetaProductsPlaced
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureComponent.MetaProductsPlacedProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaNewProductsProperty")]
        public Int32? MetaNewProducts
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureComponent.MetaNewProductsProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaChangesFromPreviousCountProperty")]
        public Int32? MetaChangesFromPreviousCount
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureComponent.MetaChangesFromPreviousCountProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaChangeFromPreviousStarRatingProperty")]
        public Int32? MetaChangeFromPreviousStarRating
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureComponent.MetaChangeFromPreviousStarRatingProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaBlocksDroppedProperty")]
        public Int32? MetaBlocksDropped
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureComponent.MetaBlocksDroppedProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaNotAchievedInventoryProperty")]
        public Int32? MetaNotAchievedInventory
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureComponent.MetaNotAchievedInventoryProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaTotalFacingsProperty")]
        public Int32? MetaTotalFacings
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureComponent.MetaTotalFacingsProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaAverageFacingsProperty")]
        public Int16? MetaAverageFacings
        {
            get
            {
                return GetCommonPropertyValue<Int16?>(Items, PlanogramFixtureComponent.MetaAverageFacingsProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaTotalUnitsProperty")]
        public Int32? MetaTotalUnits
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureComponent.MetaTotalUnitsProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaAverageUnitsProperty")]
        public Int32? MetaAverageUnits
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureComponent.MetaAverageUnitsProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaMinDosProperty")]
        public Single? MetaMinDos
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureComponent.MetaMinDosProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaMaxDosProperty")]
        public Single? MetaMaxDos
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureComponent.MetaMaxDosProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaAverageDosProperty")]
        public Single? MetaAverageDos
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureComponent.MetaAverageDosProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaMinCasesProperty")]
        public Single? MetaMinCases
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureComponent.MetaMinCasesProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaAverageCasesProperty")]
        public Single? MetaAverageCases
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureComponent.MetaAverageCasesProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaMaxCasesProperty")]
        public Single? MetaMaxCases
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureComponent.MetaMaxCasesProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaSpaceToUnitsIndexProperty")]
        public Int16? MetaSpaceToUnitsIndex
        {
            get
            {
                return GetCommonPropertyValue<Int16?>(Items, PlanogramFixtureComponent.MetaSpaceToUnitsIndexProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaIsComponentSlopeWithNoRiserProperty")]
        public Boolean? MetaIsComponentSlopeWithNoRiser
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramFixtureComponent.MetaIsComponentSlopeWithNoRiserProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaIsOverMerchandisedDepthProperty")]
        public Boolean? MetaIsOverMerchandisedDepth
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramFixtureComponent.MetaIsOverMerchandisedDepthProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaIsOverMerchandisedHeightProperty")]
        public Boolean? MetaIsOverMerchandisedHeight
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramFixtureComponent.MetaIsOverMerchandisedHeightProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaIsOverMerchandisedWidthProperty")]
        public Boolean? MetaIsOverMerchandisedWidth
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramFixtureComponent.MetaIsOverMerchandisedWidthProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaTotalComponentCollisionsProperty")]
        public Int32? MetaTotalComponentCollisions
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureComponent.MetaTotalComponentCollisionsProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaTotalPositionCollisionsProperty")]
        public Int32? MetaTotalPositionCollisions
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureComponent.MetaTotalPositionCollisionsProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaTotalFrontFacingsProperty")]
        public Int16? MetaTotalFrontFacings
        {
            get
            {
                return GetCommonPropertyValue<Int16?>(Items, PlanogramFixtureComponent.MetaTotalFrontFacingsProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaAverageFrontFacingsProperty")]
        public Single? MetaAverageFrontFacings
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureComponent.MetaAverageFrontFacingsProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureComponent), "MetaPercentageLinearSpaceFilledProperty")]
        public Single? MetaPercentageLinearSpaceFilled
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureComponent.MetaPercentageLinearSpaceFilledProperty.Name);
            }
        }

        #endregion

        /// <summary>
        /// Returns a multi view of customattribute data for the current products
        /// </summary>
        public CustomAttributeDataMultiView CustomAttributes
        {
            get { return _customAttributesView; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="items"></param>
        public PlanogramComponentMultiView(IEnumerable<PlanogramComponentView> items)
        {
            _items = items.ToList();
            _plan = _items.First().Planogram;

            _customAttributesView = new CustomAttributeDataMultiView(GetCustomAttributes(_items), _plan);

            foreach (PlanogramComponentView i in _items)
            {
                i.PropertyChanged += PlanItem_PropertyChanged;
            }

            CanUserEditSequenceNumber = !_plan?.Model.RenumberingStrategy.IsEnabled ?? false;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on any linked plan item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(e.PropertyName);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the common value of the given property for all the items
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        private static T GetCommonPropertyValue<T>(IEnumerable<Object> items, String propertyName)
        {
            T value = default(T);

            if (items.Any())
            {
                Type classType = items.First().GetType();
                PropertyInfo pInfo = classType.GetProperty(propertyName);
                if (pInfo != null)
                {
                    Boolean isFirst = true;
                    foreach (Object item in items)
                    {
                        T itemValue = (T)pInfo.GetValue(item, null);
                        if (isFirst)
                        {
                            value = itemValue;
                        }
                        else if (!Object.Equals(itemValue, value))
                        {
                            return default(T);
                        }

                        isFirst = false;
                    }
                }
            }

            return value;

        }

        /// <summary>
        /// Sets the given value against
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        private void SetPropertyValue<T>(IEnumerable<Object> items, String propertyName, T value)
        {
            if (items.Any())
            {

                //Start logging the change
                Boolean isProcessingUndoable = false;
                if (IsLoggingUndoableActions)
                {
                    if (!_plan.IsRecordingUndoableAction)
                    {
                        isProcessingUndoable = true;
                        _plan.BeginUndoableAction(propertyName);
                    }
                }

                //make the property change
                Type classType = items.First().GetType();
                PropertyInfo pInfo = classType.GetProperty(propertyName);

                if (pInfo != null)
                {
                    foreach (Object item in items)
                    {
                        pInfo.SetValue(item, value, null);
                    }
                }

                //Commit the log.
                if (isProcessingUndoable)
                {
                    _plan.EndUndoableAction();
                }
            }
        }

        #endregion

        #region IDataErrorInfo Members

        public String Error
        {
            get { throw new NotImplementedException(); }
        }

        public String this[String columnName]
        {
            get
            {
                return ((IDataErrorInfo)_items.First())[columnName];
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                foreach (var i in _items)
                {
                    i.PropertyChanged -= PlanItem_PropertyChanged;
                }
                base.IsDisposed = true;
            }
        }

        #endregion

        #region Static Helpers

        private static PropertyPath GetPropertyPath(System.Linq.Expressions.Expression<Func<PlanogramComponentMultiView, Object>> expression)
        {
            return WpfHelper.GetPropertyPath<PlanogramComponentMultiView>(expression);
        }

        /// <summary>
        /// Get the <see cref="CustomAttributeData"/> present in the <paramref name="source"/>.
        /// </summary>
        /// <param name="source">Enumeration of <see cref="PlanogramComponentView"/> instances to get the attributes from.</param>
        /// <returns></returns>
        /// <remarks>If there is any <see cref="DataPortalException"/> thrown, an empty list of attributes will be returned.</remarks>
        private static IEnumerable<CustomAttributeData> GetCustomAttributes(IEnumerable<PlanogramComponentView> source)
        {
            IEnumerable<CustomAttributeData> attributes;
            try
            {
                attributes = source.Select(i => i.Component.CustomAttributes).Where(data => data != null).ToList();
            }
            catch (DataPortalException ex)
            {
                GetWindowService().ShowErrorMessage(Application_Database_ConnectionLostHeader, Application_Database_ConnectionLostDescription);
                RecordException(ex);
                attributes = new List<CustomAttributeData>();
            }
            return attributes;
        }

        #endregion
    }

    /// <summary>
    /// Validation rule for use when binding Int16 values to to model objects
    /// </summary>
    public class NullInt16ValidationRule : ValidationRule
    {
        #region Fields

        private Int16 _max = Int16.MaxValue;
        private Int16 _min = 0;

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public NullInt16ValidationRule() : base(ValidationStep.RawProposedValue, true) { }

        #endregion

        #region Methods
        /// <summary>
        /// Performs the validation of a text value as a Int16 type
        /// </summary>
        /// <param name="value">The value to validate</param>
        /// <param name="cultureInfo">The current culture</param>
        /// <returns>The validation results</returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // assume success
            ValidationResult validationResult = ValidationResult.ValidResult;

            bool canConvert = true;
            Int16 convertValue = 0;

            if (value != null)
            {
                var localNumberFormat = cultureInfo.NumberFormat;
                String strVal = (String)value;

                if (strVal.Length > 0)
                {
                    var signCheck = strVal.Trim().StartsWith(localNumberFormat.PositiveSign) || strVal.Trim().StartsWith(localNumberFormat.NegativeSign);
                    var allDigitsCheck = strVal.All(char.IsDigit);

                    // if doesn't contain only digits
                    if (!allDigitsCheck && !signCheck)
                    {
                        return new ValidationResult(false, Message.ComponentProperties_ViewComponent_DigitsOnly);
                    }
                    else
                    {
                        // try parsing to an Int16 object
                        canConvert = Int16.TryParse(strVal, out convertValue);
                    }
                }

                if (!canConvert)
                {
                    // value is out of Int16 bounds
                    // min and max should really be Int16.MinValue and Int16.MaxValue respectively
                    return new ValidationResult(false, String.Format(
                         Message.ComponentProperties_ViewComponent_ValueExceedsAllRange, _min, _max));
                }
                else if (convertValue < _min)
                {
                    return new ValidationResult(false, Message.ComponentProperties_ViewComponent_LessThanZero);
                }
				else if (convertValue > _max)
                {
                    return new ValidationResult(false, String.Format(
                         Message.ComponentProperties_ViewComponent_ValueExceedsMax, _max));
                }
                else
                {
                    return new ValidationResult(true, null);
                }
            }

            return validationResult;
        }
        #endregion
    }

    /// <summary>
    /// Validation rule for use when binding Int32 values to to model objects
    /// </summary>
    public class NullInt32ValidationRule : ValidationRule
    {
        #region Fields

        private Int32 _max = Int32.MaxValue;
        private Int32 _min = 0;

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public NullInt32ValidationRule() : base(ValidationStep.RawProposedValue, true) { }

        #endregion

        #region Methods
        /// <summary>
        /// Performs the validation of a text value as a Int32 type
        /// </summary>
        /// <param name="value">The value to validate</param>
        /// <param name="cultureInfo">The current culture</param>
        /// <returns>The validation results</returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // assume success
            ValidationResult validationResult = ValidationResult.ValidResult;

            bool canConvert = true;
            Int32 convertValue = 0;

            if (value != null)
            {
                var localNumberFormat = cultureInfo.NumberFormat;
                String strVal = (String)value;

                if (strVal.Length > 0)
                {
                    var signCheck = strVal.Trim().StartsWith(localNumberFormat.PositiveSign) || strVal.Trim().StartsWith(localNumberFormat.NegativeSign);
                    var allDigitsCheck = strVal.All(char.IsDigit);

                    // if doesn't contain only digits
                    if (!allDigitsCheck && !signCheck)
                    {
                        return new ValidationResult(false, Message.ComponentProperties_ViewComponent_DigitsOnly);
                    }
                    else
                    {
                        // try parsing as an Int32 type
                        canConvert = Int32.TryParse(strVal, out convertValue);
                    }
                }

                if (!canConvert)
                {
                    // value is out of Int32 bounds
                    // min and max should really be Int32.MinValue and Int32.MaxValue respectively
                    return new ValidationResult(false, String.Format(
                         Message.ComponentProperties_ViewComponent_ValueExceedsAllRange, _min, _max));
                }
                else if ((convertValue < _min))
                {
                    return new ValidationResult(false, Message.ComponentProperties_ViewComponent_LessThanZero);
                }
                else if (convertValue > _max)
                {
                    return new ValidationResult(false, String.Format(
                         Message.ComponentProperties_ViewComponent_ValueExceedsMax, _max));
                }
                else
                {
                    return new ValidationResult(true, null);
                }
            }

            return validationResult;
        }
        #endregion
    }

    /// <summary>
    /// Validation rule for use when binding decimal values to to model objects
    /// </summary>
    public class NullFloatValidationRule : ValidationRule
    {
        #region Fields

        private Single _max = Single.MaxValue;
        private Single _min = 0;

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public NullFloatValidationRule() : base(ValidationStep.RawProposedValue, true) { }

        #endregion

        #region Methods
        /// <summary>
        /// Performs the validation of a text value as a float type
        /// </summary>
        /// <param name="value">The value to validate</param>
        /// <param name="cultureInfo">The current culture</param>
        /// <returns>The validation results</returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // assume success
            ValidationResult validationResult = ValidationResult.ValidResult;

            bool canConvert = true;
            Single convertValue = 0;

            if (value != null)
            {
                // Check if the value is already a float type, not a String value
                if (value is float)
                {
                    return new ValidationResult(true, null);
                }

                if (((String)value).Length > 0)
                {
                    var localNumberFormat = cultureInfo.NumberFormat;

                    // Check if the text only contains numbers, the decimal separator or the sign characters ('+' / '-')
                    String strVal = (String)value;
                    var decimalPointCheck = strVal.Contains(localNumberFormat.NumberDecimalSeparator);
                    var signCheck = strVal.Trim().StartsWith(localNumberFormat.PositiveSign) || strVal.Trim().StartsWith(localNumberFormat.NegativeSign);
                    var onlyNumbersCheck = !(strVal.Any(Char.IsLetter) || strVal.Any(Char.IsPunctuation));

                    if (!onlyNumbersCheck && !signCheck && !decimalPointCheck)
                    {
                        // Only allow digits, the decimal separator and '+' and '-'
                        return new ValidationResult(false,
                            String.Format(Message.ComponentProperties_ViewComponent_DigitsSeparatorOnly,
                                            localNumberFormat.NegativeSign,
                                            localNumberFormat.NumberDecimalSeparator));
                    }
                    else
                    {
                        // try parsing to a Single value
                        canConvert = Single.TryParse(strVal, out convertValue);
                    }
                }

                if (!canConvert)
                {
                    // value is out of Int32 bounds
                    // min and max should really be Int32.MinValue and Int32.MaxValue respectively
                    //  but, this is a dirty implementation so i am asking user to put value directly in valid bounds
                    //return new ValidationResult(false, String.Format(
                    //     Message.ComponentProperties_ViewComponent_ValueExceedsAllRange, _min, _max));
                    return new ValidationResult(false, Message.ComponentProperties_ViewComponent_CannotParse);

                }
                else if (convertValue < _min)
                {
                    return new ValidationResult(false, Message.ComponentProperties_ViewComponent_LessThanZero);
                }
                else if (convertValue > _max)
                {
                    return new ValidationResult(false, String.Format(
                         Message.ComponentProperties_ViewComponent_ValueExceedsMax, _max));
                }
                else
                {
                    return new ValidationResult(true, null);
                }
            }

            return validationResult;
        }
        #endregion
    }

    /// <summary>
    /// Validation rule for use when binding decimal values to to model objects that need to be
    /// greater than 0
    /// </summary>
    public class GreaterThanZeroFloatValidationRule : ValidationRule
    {
        #region Fields

        private Single _max = Single.MaxValue;
        private Single _min = 0;

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public GreaterThanZeroFloatValidationRule() : base(ValidationStep.RawProposedValue, true) { }

        #endregion

        #region Methods
        /// <summary>
        /// Performs the validation of a text value as a float type
        /// </summary>
        /// <param name="value">The value to validate</param>
        /// <param name="cultureInfo">The current culture</param>
        /// <returns>The validation results</returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // assume success
            ValidationResult validationResult = ValidationResult.ValidResult;

            bool canConvert = true;
            Single convertValue = 0;

            if (value != null)
            {
                // Check if the value is already a float type, not a String value
                if (value is float)
                {
                    return new ValidationResult(true, null);
                }

                if (((String)value).Length > 0)
                {
                    var localNumberFormat = cultureInfo.NumberFormat;

                    // Check if the text only contains numbers, the decimal separator or the sign characters ('+' / '-')
                    String strVal = (String)value;
                    var decimalPointCheck = strVal.Contains(localNumberFormat.NumberDecimalSeparator);
                    var signCheck = strVal.Trim().StartsWith(localNumberFormat.PositiveSign) || strVal.Trim().StartsWith(localNumberFormat.NegativeSign);
                    var onlyNumbersCheck = !(strVal.Any(Char.IsLetter) || strVal.Any(Char.IsPunctuation));

                    if (!onlyNumbersCheck && !signCheck && !decimalPointCheck)
                    {
                        // Only allow digits, the decimal separator and '+' and '-'
                        return new ValidationResult(false,
                            String.Format(Message.ComponentProperties_ViewComponent_DigitsSeparatorOnly,
                                            localNumberFormat.NegativeSign,
                                            localNumberFormat.NumberDecimalSeparator));
                    }
                    else
                    {
                        // try parsing to a Single value
                        canConvert = Single.TryParse(strVal, out convertValue);
                    }
                }

                if (!canConvert)
                {
                    // value is out of Int32 bounds
                    // min and max should really be Int32.MinValue and Int32.MaxValue respectively
                    //  but, this is a dirty implementation so i am asking user to put value directly in valid bounds
                    //return new ValidationResult(false, String.Format(
                    //     Message.ComponentProperties_ViewComponent_ValueExceedsAllRange, _min, _max));
                    return new ValidationResult(false, Message.ComponentProperties_ViewComponent_CannotParse);

                }
                else if (convertValue <= _min)
                {
                    return new ValidationResult(false, String.Format(Message.ComponentProperties_ViewComponent_LessThanOrEqualToMin, _min));
                }
                else if (convertValue > _max)
                {
                    return new ValidationResult(false, String.Format(
                         Message.ComponentProperties_ViewComponent_ValueExceedsMax, _max));
                }
                else
                {
                    return new ValidationResult(true, null);
                }
            }

            return validationResult;
        }
        #endregion
    }
}
