﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26438 : L.Ineson
//  Created
//V8-26787 : L.Ineson
//  Changes to planitemfield
#endregion

#region Version History: (CCM 8.03)
// V8-29427 : L.Luong
//  Added Meta Data properties
#endregion

#region Version History: CCM820

// V8-31526 : A.Silva
//  Amended expected Type for GetCommonPropertyValue when retrieving MetaMinDos, MetaMaxDos, MetaAverageDos, MetaMinCases, MetaMaxCases, MetaAverageCases.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.Attributes;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Presents a merged view of multiple PlanogramFixtureViews
    /// </summary>
    public sealed class PlanogramFixtureMultiView : ViewModelObject, IDataErrorInfo
    {
        #region Fields

        private IEnumerable<PlanogramFixtureView> _items;

        private PlanogramView _plan;
        private Boolean _isLoggingUndoableActions = true;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath NameProperty = GetPropertyPath(p => p.Name);
        public static readonly PropertyPath HeightProperty = GetPropertyPath(p => p.Height);
        public static readonly PropertyPath WidthProperty = GetPropertyPath(p => p.Width);
        public static readonly PropertyPath DepthProperty = GetPropertyPath(p => p.Depth);
        public static readonly PropertyPath XProperty = GetPropertyPath(p => p.X);
        public static readonly PropertyPath YProperty = GetPropertyPath(p => p.Y);
        public static readonly PropertyPath ZProperty = GetPropertyPath(p => p.Z);
        public static readonly PropertyPath AngleProperty = GetPropertyPath(p => p.Angle);
        public static readonly PropertyPath SlopeProperty = GetPropertyPath(p => p.Slope);
        public static readonly PropertyPath RollProperty = GetPropertyPath(p => p.Roll);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets whether changes should be logged as undoable actions.
        /// Note - this requires that a plan was specified on create.
        /// </summary>
        public Boolean IsLoggingUndoableActions
        {
            get { return _plan != null && _isLoggingUndoableActions; }
            set { _isLoggingUndoableActions = value; }
        }

        public IEnumerable<PlanogramFixtureView> Items
        {
            get
            {
                return _items;
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixture), "NameProperty")]
        public String Name
        {
            get
            {
                if (Items.Count() == 1)
                {
                    return Items.First().Name;
                }
                else
                {
                    return "<Multiple>";
                }
            }
            set
            {
                if (Items.Count() == 1)
                {
                    SetPropertyValue(Items, PlanogramFixture.NameProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixture), "HeightProperty")]
        public Single? Height
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixture.HeightProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramFixture.HeightProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixture), "WidthProperty")]
        public Single? Width
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixture.WidthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramFixture.WidthProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixture), "DepthProperty")]
        public Single? Depth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixture.DepthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramFixture.DepthProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "XProperty")]
        public Single? X
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureItem.XProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramFixtureItem.XProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "YProperty")]
        public Single? Y
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureItem.YProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramFixtureItem.YProperty.Name, value);
                    foreach (var s in this.Items)
                    {
                        s.Y = value.Value;
                    }
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "ZProperty")]
        public Single? Z
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureItem.ZProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramFixtureItem.ZProperty.Name, value);
                    foreach (var s in this.Items)
                    {
                        s.Z = value.Value;
                    }
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "AngleProperty")]
        public Single? Angle
        {
            get
            {
                Single? value = GetCommonPropertyValue<Single?>(Items, PlanogramFixtureItem.AngleProperty.Name);

                //convert to degrees
                if (value.HasValue)
                {
                    value = Convert.ToSingle(CommonHelper.ToDegrees(value.Value));
                }

                return value;
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramFixtureItem.AngleProperty.Name,
                        Convert.ToSingle(CommonHelper.ToRadians(value.Value)));
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "SlopeProperty")]
        public Single? Slope
        {
            get
            {
                Single? value = GetCommonPropertyValue<Single?>(Items, PlanogramFixtureItem.SlopeProperty.Name);

                //convert to degrees
                if (value.HasValue)
                {
                    value = Convert.ToSingle(CommonHelper.ToDegrees(value.Value));
                }

                return value;
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramFixtureItem.SlopeProperty.Name,
                        Convert.ToSingle(CommonHelper.ToRadians(value.Value)));
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "RollProperty")]
        public Single? Roll
        {
            get
            {
                Single? value = GetCommonPropertyValue<Single?>(Items, PlanogramFixtureItem.RollProperty.Name);

                //convert to degrees
                if (value.HasValue)
                {
                    value = Convert.ToSingle(CommonHelper.ToDegrees(value.Value));
                }

                return value;
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramFixtureItem.RollProperty.Name,
                        Convert.ToSingle(CommonHelper.ToRadians(value.Value)));
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "BaySequenceNumberProperty")]
        public Int16? BaySequenceNumber
        {
            get
            {
                return GetCommonPropertyValue<Int16?>(Items, PlanogramFixtureItem.BaySequenceNumberProperty.Name);
            }
        }

        #region Meta Data Properties

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaComponentCountProperty")]
        public Int32? MetaComponentCount
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureItem.MetaComponentCountProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaTotalMerchandisableLinearSpaceProperty")]
        public Single? MetaTotalMerchandisableLinearSpace
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureItem.MetaTotalMerchandisableLinearSpaceProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaTotalMerchandisableAreaSpaceProperty")]
        public Single? MetaTotalMerchandisableAreaSpace
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureItem.MetaTotalMerchandisableAreaSpaceProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaTotalMerchandisableVolumetricSpaceProperty")]
        public Single? MetaTotalMerchandisableVolumetricSpace
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureItem.MetaTotalMerchandisableVolumetricSpaceProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaTotalLinearWhiteSpaceProperty")]
        public Single? MetaTotalLinearWhiteSpace
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureItem.MetaTotalLinearWhiteSpaceProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaTotalAreaWhiteSpaceProperty")]
        public Single? MetaTotalAreaWhiteSpace
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureItem.MetaTotalAreaWhiteSpaceProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaTotalVolumetricWhiteSpaceProperty")]
        public Single? MetaTotalVolumetricWhiteSpace
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureItem.MetaTotalVolumetricWhiteSpaceProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaProductsPlacedProperty")]
        public Int32? MetaProductsPlaced
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureItem.MetaProductsPlacedProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaNewProductsProperty")]
        public Int32? MetaNewProducts
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureItem.MetaNewProductsProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaChangesFromPreviousCountProperty")]
        public Int32? MetaChangesFromPreviousCount
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureItem.MetaChangesFromPreviousCountProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaChangeFromPreviousStarRatingProperty")]
        public Int32? MetaChangeFromPreviousStarRating
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureItem.MetaChangeFromPreviousStarRatingProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaBlocksDroppedProperty")]
        public Int32? MetaBlocksDropped
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureItem.MetaBlocksDroppedProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaNotAchievedInventoryProperty")]
        public Int32? MetaNotAchievedInventory
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureItem.MetaNotAchievedInventoryProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaTotalFacingsProperty")]
        public Int32? MetaTotalFacings
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureItem.MetaTotalFacingsProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaAverageFacingsProperty")]
        public Int16? MetaAverageFacings
        {
            get
            {
                return GetCommonPropertyValue<Int16?>(Items, PlanogramFixtureItem.MetaAverageFacingsProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaTotalUnitsProperty")]
        public Int32? MetaTotalUnits
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureItem.MetaTotalUnitsProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaAverageUnitsProperty")]
        public Int32? MetaAverageUnits
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureItem.MetaAverageUnitsProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaMinDosProperty")]
        public Single? MetaMinDos
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureItem.MetaMinDosProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaMaxDosProperty")]
        public Single? MetaMaxDos
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureItem.MetaMaxDosProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaAverageDosProperty")]
        public Single? MetaAverageDos
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureItem.MetaAverageDosProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaMinCasesProperty")]
        public Single? MetaMinCases
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureItem.MetaMinCasesProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaAverageCasesProperty")]
        public Single? MetaAverageCases
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureItem.MetaAverageCasesProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaMaxCasesProperty")]
        public Single? MetaMaxCases
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureItem.MetaMaxCasesProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaSpaceToUnitsIndexProperty")]
        public Int16? MetaSpaceToUnitsIndex
        {
            get
            {
                return GetCommonPropertyValue<Int16?>(Items, PlanogramFixtureItem.MetaSpaceToUnitsIndexProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaTotalComponentCollisionsProperty")]
        public Int32? MetaTotalComponentCollisions
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureItem.MetaTotalComponentCollisionsProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaTotalComponentsOverMerchandisedDepthProperty")]
        public Int32? MetaTotalComponentsOverMerchandisedDepth
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureItem.MetaTotalComponentsOverMerchandisedDepthProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaTotalComponentsOverMerchandisedHeightProperty")]
        public Int32? MetaTotalComponentsOverMerchandisedHeight
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureItem.MetaTotalComponentsOverMerchandisedHeightProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaTotalComponentsOverMerchandisedWidthProperty")]
        public Int32? MetaTotalComponentsOverMerchandisedWidth
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureItem.MetaTotalComponentsOverMerchandisedWidthProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaTotalPositionCollisionsProperty")]
        public Int32? MetaTotalPositionCollisions
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramFixtureItem.MetaTotalPositionCollisionsProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaTotalFrontFacingsProperty")]
        public Int16? MetaTotalFrontFacings
        {
            get
            {
                return GetCommonPropertyValue<Int16?>(Items, PlanogramFixtureItem.MetaTotalFrontFacingsProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramFixtureItem), "MetaAverageFrontFacingsProperty")]
        public Single? MetaAverageFrontFacings
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramFixtureItem.MetaAverageFrontFacingsProperty.Name);
            }
        }

        #endregion
        //TODO:


        //Show backboard.
        //backboard height width depth colour
        // front notches
        // notch style
        // notch height width
        // notch start x, spacing x, start y spacing y

        //show base
        // base height, base width, base depth
        // base colour.
   

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="items"></param>
        public PlanogramFixtureMultiView(IEnumerable<PlanogramFixtureView> items)
        {
            _items = items.ToList();
            _plan = items.First().Planogram;

            foreach (var i in items)
            {
                i.PropertyChanged += PlanItem_PropertyChanged;
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on any linked plan item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(e.PropertyName);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the common value of the given property for all the items
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        private static T GetCommonPropertyValue<T>(IEnumerable<Object> items, String propertyName)
        {
            T value = default(T);

            if (items.Any())
            {
                Type classType = items.First().GetType();
                System.Reflection.PropertyInfo pInfo = classType.GetProperty(propertyName);
                if (pInfo != null)
                {
                    Boolean isFirst = true;
                    foreach (Object item in items)
                    {
                        T itemValue = (T)pInfo.GetValue(item, null);
                        if (isFirst)
                        {
                            value = itemValue;
                        }
                        else if (!Object.Equals(itemValue, value))
                        {
                            return default(T);
                        }

                        isFirst = false;
                    }
                }
            }

            return value;

        }

        /// <summary>
        /// Sets the given value against
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        private void SetPropertyValue<T>(IEnumerable<Object> items, String propertyName, T value)
        {
            if (items.Any())
            {

                //Start logging the change
                Boolean isProcessingUndoable = false;
                if (IsLoggingUndoableActions)
                {
                    if (!_plan.IsRecordingUndoableAction)
                    {
                        isProcessingUndoable = true;
                        _plan.BeginUndoableAction(propertyName);
                    }
                }

                //make the property change
                Type classType = items.First().GetType();
                System.Reflection.PropertyInfo pInfo = classType.GetProperty(propertyName);

                if (pInfo != null)
                {
                    foreach (Object item in items)
                    {
                        pInfo.SetValue(item, value, null);
                    }
                }

                //Commit the log.
                if (isProcessingUndoable)
                {
                    _plan.EndUndoableAction();
                }
            }
        }

        #endregion

        #region IDataErrorInfo Members

        public String Error
        {
            get { throw new NotImplementedException(); }
        }

        public String this[String columnName]
        {
            get
            {
                return ((IDataErrorInfo)_items.First())[columnName];
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                foreach (var i in _items)
                {
                    i.PropertyChanged -= PlanItem_PropertyChanged;
                }
                base.IsDisposed = true;
            }
        }

        #endregion

        #region Static Helpers

        /// <summary>
        /// Returns the list of plan item fields for this view.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> GetPlanItemFields()
        {
            String ownerFriendlyName = PlanItemHelper.FriendlyNames[Common.PlanItemType.Fixture];
            
            Type ownerType = typeof(PlanogramFixture);
            yield return ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName, PlanogramFixture.NameProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName, PlanogramFixture.HeightProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName, PlanogramFixture.WidthProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName, PlanogramFixture.DepthProperty);

            ownerType = typeof(PlanogramFixtureItem);
            yield return ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName, PlanogramFixtureItem.XProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName, PlanogramFixtureItem.YProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName, PlanogramFixtureItem.ZProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName, PlanogramFixtureItem.SlopeProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName, PlanogramFixtureItem.AngleProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName, PlanogramFixtureItem.RollProperty);
        }
        
        private static PropertyPath GetPropertyPath(System.Linq.Expressions.Expression<Func<PlanogramFixtureMultiView, Object>> expression)
        {
            return WpfHelper.GetPropertyPath<PlanogramFixtureMultiView>(expression);
        }

        #endregion


    }
}
