﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0)

// L.Hodson
//  Created
// V8-25734 : L.Ineson
//  Moved out rendering controls to framework.
// CCM-25977 : N.Haywood
//  Overrided equals method
// V8-26003 : A.Probyn
//  Added defensive code to ProcessCollectionUndoRedo method to stop any other index exceptions being thrown if something else is missed.
//  Included Annotations in AddFixtureCopy method
// V8-26041 : A.Kuszyk
//  Amended AddProducts to load image data from Products.
// V8-26338 : A.Kuszyk
//  Added the PerformanceMetrics property.
// V8-26686 : A.Silva ~ Added EnumerateAllPlanItemFields to obtain the PlanItemFields for the metadata in the planogram.
//V8-26787 : L.Ineson
//  Changes to planitemfield
// V8-26702 : A.Kuszyk
//  Protected AddProducts against being called when not connected to a repository.
// V8-27153 : A.Silva
//      Added RenumberingStrategy to expose that of the planogram.
// V8-27496 : L.Luong
//  Added SavingSettings and CheckSaveSettings Method
// V8-27589 : A.Silva
//      Added ValidationWarningRows.
// V8-25922 : L.Ineson
//  Split out PlanogramDesignView and PlanogramFixtureView into their own classes.
// V8-27643 : L.Ineson
//  Record undoable action no longer clears if a different property name gets notified first.
// V8-27744 : A.Silva
//      CheckValidationWarnings now runs in the background, and updates the UI when it is done.
// V8-27742 : A.Silva
//      Amended to no longer update the status bar icon directly. ValidationWarningsChanged event raised after modifying the validation warnings.
//      Added ComponentIsOverfilled, PositionIsOutsideMerchandisingSpace, PegIsOverfilled 
//      ProductGtinIsDuplicated, IsMaxStackExceeded, IsMaxCapExceeded, IsMinDeepNotAchieved
//      IsMaxDeepExceeded, IsCannotBreakTrayBackIgnored, IsCannotBreakTrayTopIgnored validation warning checks.
// V8-27836 : A.Silva
//      Moved ProductGtinIsDuplicated to warning check for products, added Product IsMinDosAchieved, 
//      IsMinCasesAchieved, IsMinDeliveriesAchieved, IsMinShelfLifeAchieved. 
// V8-27924 : A.Silva
//      Added warning check for IsMaxTopCapExceeded in Position Warnings.
// V8-27938 : N.Haywood
//  exposed missing properties for data sheets
// V8-25976 : L.Ineson
//  Moved get validation warnings methods into model.
// V8-27980 : A.Silva
//      Moved ValidationWarningRow to its own file.
// V8-27672 : A.Probyn
//      Updated RemoveFixture to update x positions of other fixtures.
// V8-28542 : A.Kuszyk
//  Amended AddPlanItemCopy to shift pasted fixture to the right of the copied fixture 
//  and shift all fixtures to the right over. Also shifted pasted positions to the right of the copy.

#endregion

#region Version History: (CCM 801)

// V8-27897 : A.Kuszyk
//  Added UpdateProducts method.
// V8-28676 : A.Silva
//      Amended AddProducts method so that images are no longer fetched 
//      and stored in the planogram when adding products, even when there
//      is a repository connection.
//      Added RemoveOrphanImages.
// V8-28764 : A.Kuszyk
//  Amended AddPlanItemCopy() to optionally accept an IPlanItem target and evaluate this for 
//  Position paste operations.
// V8-28887 : A.Silva
//      Amended RemoveOrphanImages() so that the purged images get their ImageData scrapped to avoid a memory leak.
// V8-25320 : A.Kuszyk
//  Amended ProcessCollectionUndoRedo to persist fixture names after undoing removals. 

#endregion

#region Version History: (CCM 802)

// V8-28303 : L.Ineson
//  Stopped plan remerchandising when customattributes property loads.
//  Made sure that this actually keeps a track of subcomponent groups and merch bounds that have changed.
// V8-28766: J.Pickup
//  Introduced a call to autofill wide, high, deep during ProcessPlanChanges.

#endregion

#region Version History: (CCM 803)

// V8-27930 : L.Ineson
//  Made changes to how undo redo works. 
// This now records changes made during plan processing and also single property changes.
// V8-29291 : L.Ineson
//  Made changes to how flagging works and made changes to speed things up.
// V8-29622 : L.Ineson
//  Changes made to allow adding positions to work using merch group methods.
// V8-29726 : L.Ineson
//  Added CleanUpOrphanedParts
// V8-28766 : J.Pickup
//  Changed temporary call to autofill helper to use the method on the Merch group in line with new behaviour. (Work still in progress).
// V8-30207 : J.Pickup
//  Now called on dirty merchGroups only.

#endregion

#region Version History: (CCM 810)

//  V8-29662 : M.Brumby
//      Changed ordering of fixtures to take into account
//      the numbering strategy.
// V8-29785 : L.Ineson
//  Metadata update will now re-mark plan 
//  back to initialized if it was before the change.
// V8-29929 : L.Ineson
//  Metadata and validation warnings are now procesed together.
// V8-29978 : M.Pettit
//  Undo/Redo failed to reset suppressChangeRecording correctly
//  CompleteUndoableAction did not reset parameters correctly after taking local copy of the current processing action
// V8-29545 : M.Pettit
//  Copying plan component now paces it above copied item in next available space
// V8-30175 : L.Ineson
//  Made sure that queue plan process is called after undo or redo action is performed.
// V8-29857 : M.Pettit
//  Amended AddFixtureCopy as BaySequenceNumber was incorrectly calculated
// V8-30170 : L.Ineson
//  Made further processing changes to prevent background update being applied when 
//  the position has moved.
//  Also made sure that when freezing a plan, a full immediate process update is forced.
// V8-29857 : M.Pettit
//  Amended RemoveFixture so that when called by an undo action, the code does not re-adjust other fixtures
// V8-28962 : L.Ineson
//  More changes to undo redo to take autofill into account.

#endregion

#region Version History: (CCM 811)

// V8-30313 : L.Ineson
//  Autofill now take in the list of all plan merch groups.
// V8-30347 : L.Ineson
//  Made sure that IsRecordingUndoableAction flag always gets turned off even
//  if there were no actions.
// V8-30370 : N.Haywood
//  Added Model.ResequenceBays(); when undoing a fixture removal
// V8-27469 : A.Probyn
//  ~ Updated ProcessCollectionUndoRedo so that name property values are persisted.
//  ~ Also fixed a bug where multiple property updates were being undone in the undo method, but resulting in more
//    actions being recorded (shouldn't be happening).
// V8-30334 : A.Silva
//  Updated code in SubComponentGroup constructors and refactored code to eliminate duplication of code.
// V8-30519 : L.Ineson
//  Added model child changing to planogram structure and ammended the undo redo to use this instead.
// V8-30570 : L.Luong
//  Changed the remove positions in the remove fixture as undoing was not recognising child changes when fixtures had 6 or more positions
// V8-30581 : A.Kuszyk
//  Ensured dividers are kept up-to-date after autofill and save operation.
// V8-30585 : A.Probyn
//  Slight alteration to AddPlanItemCopy to defend against null values when positions are copied and no components exists.
// V8-30670 : A.Kuszyk
//  Added defensive checks to OnModelChildChanged to ensure _redoActions is only cleared when we're on the UI thread.
// V8-30699 - L.Ineson
//  Removed chnage to AddFixtureCopy.
#endregion

#region Version History: (CCM 820)
// V8-30687 : L.Ineson
//  Any ongoing process action now gets cancelled when we start recording an undo action.
// V8-30795 : A.Kuszyk
//  Amended AddPlanItemCopy to hand shiftHeight flag correctly for positions.
// V8-29968 : A.Kuszyk
//  Added MerchandisingGroupsProcessed event and ensured that plan changes aren't processed for changes to the sequence.
// V8-30536 : A.Kuszyk
//  Added MerchandisingVolumePercentagesChanged event and re-factored background processing to ensure merchandising groups
//  are only instantiated once.
// V8-30936 : M.Brumby
//  Added slotwall
// V8-30885 : L.Ineson
//  Added renumbering update to processing.
// V8-30754 : A.Silva
//  Amended PlanBackgroundProcessing_DoWork so that the correct plan clone is used when the package contains more than one plan.
//V8-30737 : I.George
//  Added CopyPlangoramMetadataState for assortment and blocking
// V8-31061 : A.Probyn
//  Added SupressPlanUpdatesDuringEdit and updated OnChildChanged to supress merchandising group processing on update of
//  merchandising strategy change during uncommited edit of component properties.
// V8-31146 : L.Ineson
//  Undo action cancellation now reprocesses even if there were no actions so that background process is not missed.
//  Amended previous change as it caused issues with sequencing, have also switched over to use the existing IsRecordingUndoableAction flag.
// V8-31165 : D.Pleasance
//  Amended PerformBackgroundProcessWork, so that product sequence is also calculated if planogram is sequence type.
// V8-31199 : L.Ineson
//  Forced plan children to load on fetch.
//  Amended how planogram update is forced on save.
// V8-31237 : J.Pickup
//  Added an order to which the merch groups are auto'd which adds consistency and solves issue with hang groups that have several facings high.
// V8-31363 : M.Brumby
//  Make and earlier call to PlanProcessCompleting in NotifyMerchandisingGroupChanged so that data that does not
//  require meta data calculations can be used to update labels and highlights earlier.
// V8-31368 : L.Ineson
//  Made sure that image properties do not cause the plan to be reprocessed.
// V8-31426 : M.Brumby
//  Removed assumed copied component Y translation as it didn't take into account cut and paste components.
// V8-31423 : M.Shelley
//  Added a new method (CopyConsumerDecisionTreeMetadata) to copy the consumer decision tree metadata for ALL the nodes in the tree
#endregion

#region Version History: (CCM 830)
// V8-31529 : L.Ineson
//  Made sure that fixture assembly metadata gets copied back.
// V8-31557 : M.Shelley
//  Add the functionality to append a selected planogram to the right of the current planogram.
//  Added code to copy over the Assortment products
// V8-31612 : A.Kuszyk
//  Optimised the recording of undoable actions for Begin/EndUndoableAction.
// V8-31638 : L.Ineson
//  Made sure that plan cache now gets forcibly cleared before reprocessing.
// V8-31542 : L.Ineson
//  Added IPlanItem implementation.
// V8-31885 : A.Silva
//  Added Comparison property
// V8-31957 : L.Ineson
//  Made sure that background process does not commit if plan change flag is true
// V8-31833 : L.Ineson
//  Moved FillSelectedPositionsOnGivenAxis logic here.
// V8-31804 : A.Kuszyk
//  Added shouldCalculateMetadata to constructors to allow metadata calculations to be disabled in unit tests.
// V8-32006 : N.Haywood
//  Cleared the redo action if the action fails
//V8-31996 : L.Ineson
//  Removed SuppressBackgroundProcessing - Please use existing BeginUpdate and EndUpdate methods.
// V8-32145 : A.Silva
//  Corrected calls to renumbering bays, to use the Renumbering Strategy.
// V8-32178 : A.Silva
//  Amended FillPositionsOnGivenAxis to not use obsolete method ProcessAutoCap.
// V8-32132 : A.Silva
//  Refactored and amended AppendPlanogram so that products are correctly added from the appended planogram.
//V8-32277 : L.Ineson
//  Added PlangoramModelIsValid
//V8-32296 : L.Ineson
//  Copying products now copies perf data.
//V8-32115 : L.Ineson
//  Simplified undo redo behaviour.
// V8-32523 : L.Ineson
//  Added cleanup of orphaned annotations.
// V8-32518 : N.Haywood
//  Added moving to world position for pasting to other plans
// V8-32498 : A.Silva
//  Amended AddPlanItemCopy to use TryFindMatchingFixtureView so that a valid fixture view is selected. If there is none, just abort copying that particular item.
// V8-32593 : A.Silva
//  Refactored UpdateValidationWarnings to move the PlanogramValidationWarning to ValidationWarningRow logic to the ValidationWarningRow class itself.
// V8-32633 : N.Haywood
//  Moved CleanUpOrphanedParts to the planogram model
// V8-32675 : L.Ineson
//  Reworked post processing to make things feel more responsive.
// V8-32599 : A.Heathcote
//  Added the ShouldShowValidation method and the for each loops when adding ValidationWarningRow to _notifierRows.
// V8-32763 : L.Ineson
//  Ensured that validation warnings are now removed if they relate to items that have been removed from the plan.
// V8-32782 : L.Ineson
// Plan fixture now gets passed into create fixture overide
// V8-32820 : A.Kuszyk
//  Ensured that position sequence colours and numbers are updated following re-sequencing.
// V8-32884 : A.Kuszyk
//  Ensured that metadata is calculated after the sequencing information is re-generated.
// V8-32795 : L.Bailey
//  Dragging bays between plans mixes up products
// V8-32905 : M.Brumby
//  Fix for empty new bays
// V8-32997 : A.Kuszyk
//  Re-added cloning to tertiary processing.
// CCM-18577 : A.Silva
//  Amended and refactored AddPlanItemCopies so that it copies selected child items when there are any, or all child items if not.
// CCM-18613 : A.Silva
//  Amended CopyItemsOfType so that failed copies are not added to the copied items list (resulting in null positions).
#endregion

#endregion

using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Editor.Client.Wpf.PlanProperties;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Logging;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;
using Galleria.Framework.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Provides a view of a specific planogram
    /// </summary>
    public sealed class PlanogramView : PlanogramViewModelBase, IPlanItem
    {
        #region Nested Classes

        private sealed class SubComponentGroup
        {
            #region Fields

            public readonly ReadOnlyCollection<PlanogramSubComponentPlacement> Subs;
            public readonly ReadOnlyCollection<RectValue> MerchandisingBounds;

            #endregion

            #region Constructors

            /// <summary>
            ///     Initializes a new instance of <see cref="SubComponentGroup" />.
            /// </summary>
            /// <param name="items">
            ///     The enumeration of <see cref="PlanogramSubComponentPlacement" /> items to add to the instance's
            ///     <see cref="Subs" /> collection.
            /// </param>
            private SubComponentGroup(IEnumerable<PlanogramSubComponentPlacement> items)
            {
                const Boolean ignoreCollisions = false;
                Subs = new ReadOnlyCollection<PlanogramSubComponentPlacement>(items.ToList());
                IEnumerable<PlanogramSubComponentPlacement> placements =
                    Subs.Count > 0
                        ? Subs.First().Planogram.GetPlanogramSubComponentPlacements()
                        : new List<PlanogramSubComponentPlacement>();
                IEnumerable<RectValue> merchandisingBounds = Subs.OrderBy(s => s.SubComponent.Id).Select(item => item.GetWorldMerchandisingSpaceBounds(ignoreCollisions, placements.ToList()));
                MerchandisingBounds = new ReadOnlyCollection<RectValue>(merchandisingBounds.ToList());
            }

            /// <summary>
            ///     Initializes a new instance of <see cref="SubComponentGroup" />.
            /// </summary>
            /// <param name="items">
            ///     The enumeration of <see cref="PlanogramSubComponentView" /> items containing a reference to the
            ///     <see cref="PlanogramSubComponentPlacement" /> items to add to the instance's <see cref="Subs" /> collection.
            /// </param>
            public SubComponentGroup(IEnumerable<PlanogramSubComponentView> items) : this(items.Select(item => item.ModelPlacement)) { }

            /// <summary>
            ///     Initializes a new instance of <see cref="SubComponentGroup" />.
            /// </summary>
            /// <param name="merchGroup">
            ///     The <see cref="PlanogramMerchandisingGroup" /> to obtain the enumeration of
            ///     <see cref="PlanogramSubComponentPlacement" /> items to add to the instance's <see cref="Subs" /> collection.
            /// </param>
            public SubComponentGroup(PlanogramMerchandisingGroup merchGroup) : this(merchGroup.SubComponentPlacements) { }

            #endregion

            #region Methods

            public IEnumerable<PlanogramSubComponentPlacementId> GetPlacementKeys()
            {
                foreach (PlanogramSubComponentPlacement sub in Subs.OrderBy(s => s.SubComponent.Id))
                    yield return (PlanogramSubComponentPlacementId)sub.Id;
            }

            #endregion
        }

        private sealed class ResequenceAction
        {
            public Object SubComponentId { get; set; }
            public Boolean ResequenceX { get; set; }
            public Boolean ResequenceY { get; set; }
            public Boolean ResequenceZ { get; set; }
        }

        #endregion

        #region Fields

        /// <summary>
        /// Flags whether or not metadata should be calculated on plan changes. True by default. This will almost
        /// always be true, apart from in some unit testing scenarios.
        /// </summary>
        private Boolean _shouldCalculateMetadata = true;

        private PackageViewModel _parentPackageView; //ref to the viewmodel for the parent package
        private CastedObservableCollection<PlanogramFixtureView, PlanogramFixtureViewModelBase> _fixturesRO; //list of fixture viewmodels
        private CastedObservableCollection<PlanogramProductView, PlanogramProductViewModelBase> _productsRO; //list of product viewmodels.
        private readonly BulkObservableCollection<ValidationWarningRow> _validationWarningRows = new BulkObservableCollection<ValidationWarningRow>(); //list of validation warnings.

        //UNDO REDO
        const Int32 _undoMax = 10;//max no of items to be held in undo list.
        private readonly ObservableCollection<ModelUndoAction> _undoActions = new ObservableCollection<ModelUndoAction>(); //undo list
        private readonly ObservableCollection<ModelUndoAction> _redoActions = new ObservableCollection<ModelUndoAction>(); //redo list
        private Boolean _suppressChangeRecording;
        private Boolean _isRecordingUndoableGroupAction;
        private ModelUndoAction _processingUndoAction;

        //PLAN PROCESSING
        private Boolean _preventProcessQueueing;
        private Boolean _isProcessingPlanChanges; //indicates that the plan is currently processing.
        private readonly System.Timers.Timer _planProcessDelayTimer; //processing delay timer.
        private List<SubComponentGroup> _prevGroups = new List<SubComponentGroup>(); //list of groups processed last time.
        private Boolean _isSizeOutdated; //flag to indicate if the plan bounds needs recalculating.
        private Boolean _isFrozen; // flag to indicate if the plan is currently frozen
        private List<PlanogramMerchandisingGroup> _pendingMerchGroups = new List<PlanogramMerchandisingGroup>();
        private Boolean _isPlanChanged;
        List<Object> _pendingAutofillSubcomponentIds = new List<Object>();
        private Boolean _isPlanUpToDate; //indicator to ui that the plan has not yet been fully processed
        private Boolean _isPlanogramModelValid; //cache of the status of the model isValid.
        private Boolean _isBackgroundProcessRestartRequired;
        private CancellationTokenSource _postProcessingCancellationSource; // the cancellation token source for the post processing.

        private PlanogramDesignView _flattenedView; //the design view of the plan.
        private Boolean _isLoadingImages;//flag to indicate if images are currently being loaded
        private DisplayUnitOfMeasureCollection _displayUnits = DisplayUnitOfMeasureCollection.Empty;//the uom display collection to use.

        private CalculatedValueResolver _calculatedValueResolver;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath FixturesProperty = WpfHelper.GetPropertyPath<PlanogramView>(p => p.Fixtures);
        public static readonly PropertyPath ProductsProperty = WpfHelper.GetPropertyPath<PlanogramView>(p => p.Products);
        public static readonly PropertyPath IsEditingProperty = WpfHelper.GetPropertyPath<PlanogramView>(p => p.IsRecordingUndoableAction);
        public static readonly PropertyPath IsLoadingImagesProperty = WpfHelper.GetPropertyPath<PlanogramView>(p => p.IsLoadingImages);
        public static readonly PropertyPath PerformanceMetricsProperty = WpfHelper.GetPropertyPath<PlanogramView>(p => p.PerformanceMetrics);
        public static readonly PropertyPath DisplayUnitsProperty = WpfHelper.GetPropertyPath<PlanogramView>(p => p.DisplayUnits);
        public static readonly PropertyPath IsPlanUpToDateProperty = WpfHelper.GetPropertyPath<PlanogramView>(p => p.IsPlanUpToDate);
        public static readonly PropertyPath RenumberingStrategyProperty = WpfHelper.GetPropertyPath<PlanogramView>(p => p.RenumberingStrategy);
        public static readonly PropertyPath ValidationWarningRowsProperty = WpfHelper.GetPropertyPath<PlanogramView>(p => p.ValidationWarningRows);
        public static readonly PropertyPath ComparisonProperty = WpfHelper.GetPropertyPath<PlanogramView>(p => p.Comparison);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the viewmodel for the parent package.
        /// </summary>
        public PackageViewModel ParentPackageView
        {
            get { return _parentPackageView; }
        }

        /// <summary>
        /// Returns the collection of planogram fixture views
        /// </summary>
        public CastedObservableCollection<PlanogramFixtureView, PlanogramFixtureViewModelBase> Fixtures
        {
            get
            {
                if (_fixturesRO == null)
                {
                    _fixturesRO =
                        new CastedObservableCollection<PlanogramFixtureView, PlanogramFixtureViewModelBase>(base.FixtureViews, true);
                }
                return _fixturesRO;
            }
        }

        /// <summary>
        /// Returns the collection of planogram product views.
        /// </summary>
        public CastedObservableCollection<PlanogramProductView, PlanogramProductViewModelBase> Products
        {
            get
            {
                if (_productsRO == null)
                {
                    _productsRO =
                        new CastedObservableCollection<PlanogramProductView, PlanogramProductViewModelBase>(base.ProductViews, true);
                }
                return _productsRO;
            }
        }

        /// <summary>
        /// Returns the collection of undo actions available for this planogram.
        /// </summary>
        public ReadOnlyObservableCollection<ModelUndoAction> UndoActions
        {
            get { return new ReadOnlyObservableCollection<ModelUndoAction>(_undoActions); }
        }

        /// <summary>
        /// Returns the collection of redo actions available for this planogram
        /// </summary>
        public ReadOnlyObservableCollection<ModelUndoAction> RedoActions
        {
            get { return new ReadOnlyObservableCollection<ModelUndoAction>(_redoActions); }
        }

        /// <summary>
        /// Returns a flattened view of this planogram.
        /// </summary>
        public PlanogramDesignView FlattenedView
        {
            get
            {
                if (_flattenedView == null)
                {
                    _flattenedView = new PlanogramDesignView(this);
                }
                return _flattenedView;
            }
        }

        /// <summary>
        /// Returns true if product images are currently loading.
        /// </summary>
        public Boolean IsLoadingImages
        {
            get { return _isLoadingImages; }
            private set
            {
                _isLoadingImages = value;
                OnPropertyChanged(IsLoadingImagesProperty);
            }
        }

        /// <summary>
        /// Collection of all images used by the planogram
        /// </summary>
        public PlanogramImageList Images
        {
            get
            {
                PlanogramImageList imageList = this.Model.ImagesAsync;
                if (imageList == null)
                {
                    this.IsLoadingImages = true;
                }
                return imageList;
            }
        }

        /// <summary>
        /// The Performance Metrics stored for this Planogram.
        /// </summary>
        public PlanogramPerformanceMetricList PerformanceMetrics
        {
            get
            {
                if (Model.Performance == null) return null;
                return Model.Performance.Metrics;
            }
        }

        /// <summary>
        /// Return the display units collection to use for this planogram.
        /// </summary>
        public DisplayUnitOfMeasureCollection DisplayUnits
        {
            get { return _displayUnits; }
            private set
            {
                _displayUnits = value;
                OnPropertyChanged(DisplayUnitsProperty);
            }
        }

        /// <summary>
        ///     Gets or sets the value of the <see cref="RenumberingStrategy"/> property, notifying when the value changes.
        /// </summary>
        public PlanogramRenumberingStrategy RenumberingStrategy
        {
            get { return Model == null ? null : Model.RenumberingStrategy; }
        }

        /// <summary>
        /// Returns the save settings object for this plan.
        /// </summary>
        public PackageSaveSettings SaveSettings
        {
            get
            {
                return (this.ParentPackageView != null) ?
                    this.ParentPackageView.SaveSettings : new PackageSaveSettings();
            }
        }

        public BulkObservableCollection<ValidationWarningRow> ValidationWarningRows
        {
            get { return _validationWarningRows; }
        }

        /// <summary>
        /// Gets/Sets whether the plan is processing changes.
        /// </summary>
        private Boolean IsProcessingPlanChanges
        {
            get { return _isProcessingPlanChanges; }
            set { _isProcessingPlanChanges = value; }
        }

        /// <summary>
        /// Gets whether the planogram is fully up to date.
        /// Eg. all proccessed, metadata calculated etc.
        /// This is used by the ui to show the plan processing indicator.
        /// </summary>
        public Boolean IsPlanUpToDate
        {
            get { return _isPlanUpToDate; }
            private set
            {
                _isPlanUpToDate = value;
                OnPropertyChanged(IsPlanUpToDateProperty);
            }
        }

        /// <summary>
        /// Returns true if the planogram model is valid.
        /// This should be called instead of the Model.IsValid.
        /// </summary>
        public Boolean IsPlanogramModelValid
        {
            get { return _isPlanogramModelValid; }
            private set
            {
                _isPlanogramModelValid = value;
                OnPropertyChanged("IsPlanogramModelValid");
            }
        }

        /// <summary>
        ///     Gets the underlying <see cref="PlanogramComparison"/> object.
        /// </summary>
        public PlanogramComparison Comparison { get { return Model != null ? Model.Comparison : null; } }

        /// <summary>
        /// Flags whether or not metadata should be calculated on plan changes. True by default. This will almost
        /// always be true, apart from in some unit testing scenarios.
        /// </summary>
        public Boolean ShouldCalculateMetadata
        {
            get { return _shouldCalculateMetadata; }
            set
            {
                Debug.Assert(App.IsUnitTesting, "Are you sure you want to be altering metadata calculations in the UI?!");
                _shouldCalculateMetadata = value;
            }
        }

        /// <summary>
        /// Returns the resolver used for binding to calculated values.
        /// </summary>
        public CalculatedValueResolver CalculatedValueResolver
        {
            get
            {
                if (_calculatedValueResolver == null)
                {
                    _calculatedValueResolver =
                        new CalculatedValueResolver(GetCalculatedValue, /*enableCaching*/true);
                }
                return _calculatedValueResolver;
            }
        }

        private void OnPropertyChanged(PropertyPath property)
        {
            OnPropertyChanged(property.Path);
        }

        #endregion

        #region Events

        #region ModelChanged

        /// <summary>
        /// Called when the model changes.
        /// </summary>
        public event EventHandler<ViewStateObjectModelChangedEventArgs<Planogram>> ModelChanged;

        #endregion

        #region BoundsChanged

        public event EventHandler BoundsChanged;

        /// <summary>
        /// Called when the size or position of this model changes.
        /// </summary>
        private void OnBoundsChanged()
        {
            if (BoundsChanged != null)
            {
                BoundsChanged(this, EventArgs.Empty);
            }
        }

        #endregion

        #region ValidationWarningsChanged

        public event EventHandler ValidationWarningsChanged;

        /// <summary>
        ///     Called when the validation warning rows are changed.
        /// </summary>
        private void OnValidationWarningsChanged()
        {
            if (ValidationWarningsChanged != null)
            {
                ValidationWarningsChanged(this, EventArgs.Empty);
            }
        }

        #endregion

        #region PlanProcessCompleting

        /// <summary>
        /// Even to notify when a plan process is finishing.
        /// Allows linked controls to update as they require.
        /// </summary>
        public event EventHandler PlanProcessCompleting;

        private void OnPlanProcessCompleting()
        {
            if (PlanProcessCompleting != null) PlanProcessCompleting(this, EventArgs.Empty);

            //notify all child views.
            foreach (PlanogramProductView p in this.Products) p.NotifyPlanProcessCompleting();

            foreach (PlanogramPositionView p in EnumerateAllPositions()) p.NotifyPlanProcessCompleting();

            //update the planogram is valid flag
            this.IsPlanogramModelValid = this.Model.IsValid;
        }

        #endregion

        /// <summary>
        /// Fired when merchandising groups within this Planogram are processed.
        /// </summary>
        public event EventHandler MerchandisingGroupsProcessed;

        /// <summary>
        /// Raised when the merchandising volume percentages have changed for this plans blocking.
        /// </summary>
        public event EventHandler<EventArgs<Dictionary<Object, Dictionary<Object, Double>>>> MerchandisingVolumePercentagesChanged;

        #endregion

        #region Constructor

        /// <summary>
        /// Testing only.
        /// </summary>
        /// <param name="plan"></param>
        [Obsolete("Need to swap over to go through package view properly")]
        public PlanogramView(Planogram plan, Boolean shouldCalculateMetadata = true)
            : this(plan, null, shouldCalculateMetadata)
        { }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="packageFilePath"></param>
        public PlanogramView(Planogram plan, PackageViewModel parentPackageView, Boolean shouldCalculateMetadata = true)
        {
            _shouldCalculateMetadata = shouldCalculateMetadata;

            //create the processing timer
            _planProcessDelayTimer = new System.Timers.Timer(100);
            _planProcessDelayTimer.AutoReset = false;
            _planProcessDelayTimer.Elapsed += PlanProcessTimer_Elapsed;

            _parentPackageView = parentPackageView;

            //set the file path and model
            SetModel(plan, ViewStateObjectModelChangeReason.Direct);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property is changing on the planogram
        /// </summary>
        protected override void OnModelPropertyChanging(Object sender, String propertyName)
        {
            //If this is a property change, then check if it is one we dont care about.
            Boolean processUndo = true;
            if (String.IsNullOrEmpty(propertyName)
                || propertyName == "CustomAttributes"
                || propertyName.EndsWith("Async")
                || propertyName.StartsWith("Meta"))
            {
                processUndo = false;
            }


            //record this action.
            if (processUndo)
            {
                RecordUndoableAction(sender, new PropertyChangingEventArgs(propertyName));
            }

            base.OnModelPropertyChanging(sender, propertyName);
        }

        /// <summary>
        /// Called whenever a property has changed on the planogram.
        /// </summary>
        protected override void OnModelPropertyChanged(Object sender, String propertyName)
        {
            //If this is a property change, then check if it is one we dont care about.
            if (String.IsNullOrEmpty(propertyName)
                || propertyName == "CustomAttributes"
                || propertyName.EndsWith("Async")
                || propertyName.StartsWith("Meta"))
            {
                base.OnModelPropertyChanged(sender, propertyName);
                return;
            }


            //record this action and complete it immediately if we are not recording.
            RecordUndoableAction(sender, new PropertyChangedEventArgs(propertyName),
                /*complete*/!IsRecordingUndoableAction && !_isProcessingPlanChanges);

            //Raise on property changed.
            base.OnModelPropertyChanged(sender, propertyName);


            //update the display units collection if required.
            if (propertyName == Planogram.CurrencyUnitsOfMeasureProperty.Name
                || propertyName == Planogram.AreaUnitsOfMeasureProperty.Name
                || propertyName == Planogram.LengthUnitsOfMeasureProperty.Name
                || propertyName == Planogram.AngleUnitsOfMeasureProperty.Name
                || propertyName == Planogram.VolumeUnitsOfMeasureProperty.Name
                || propertyName == Planogram.WeightUnitsOfMeasureProperty.Name)
            {
                this.DisplayUnits = DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(this.Model);
                return;
            }
            else if (propertyName == Planogram.ProductPlacementXProperty.Name
                || propertyName == Planogram.ProductPlacementYProperty.Name
                || propertyName == Planogram.ProductPlacementZProperty.Name)
            {
                FlagAll();
            }
            else if (propertyName == Planogram.ImagesProperty.Name)
            {
                return;
            }
            QueueProcessPlanChanges();
        }

        /// <summary>
        /// Called whenever the planogram model raises the ChildChanging event.
        /// </summary>
        protected override void OnModelChildChanging(ChildChangingEventArgs e)
        {
            //If this is a property change, then check if it is one we dont care about.
            Boolean processUndo = true;
            if (e.PropertyChangingArgs != null
                && (String.IsNullOrEmpty(e.PropertyChangingArgs.PropertyName)
                || e.PropertyChangingArgs.PropertyName == "CustomAttributes"
                || e.PropertyChangingArgs.PropertyName.EndsWith("Async")
                || e.PropertyChangingArgs.PropertyName.StartsWith("Meta")))
            {
                processUndo = false;
            }

            //Process the event for undo 
            if (processUndo)
            {
                RecordUndoableAction(e);
            }

            base.OnModelChildChanging(e);
        }

        /// <summary>
        /// Called whenever a child of the model changes.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnModelChildChanged(Csla.Core.ChildChangedEventArgs e)
        {
            // Clear the redo actions, if we're on the UI thread.
            Dispatcher dispatcher = Dispatcher.FromThread(Thread.CurrentThread);
            if (dispatcher != null)
            {
                if (!IsProcessingPlanChanges && !_preventProcessQueueing)
                {
                    try
                    {
                        _redoActions.Clear();
                        CommandManager.InvalidateRequerySuggested();
                    }
                    catch (NotSupportedException)
                    {
                        // If, for some reason (likely we're on the wrong thread), we couldn't
                        // clear the redo actions, just carry on regardless.
                    }
                }
            }

            //We dont want to process the undo if the property is
            // an async loaded one
            Boolean processUndo = true;
            if (e.PropertyChangedArgs != null
                && (String.IsNullOrEmpty(e.PropertyChangedArgs.PropertyName)
                || e.PropertyChangedArgs.PropertyName == "CustomAttributes"
                || e.PropertyChangedArgs.PropertyName.EndsWith("Async")
                || e.PropertyChangedArgs.PropertyName.StartsWith("Meta")))
            {
                processUndo = false;
            }

            //Process the event for undo.
            if (processUndo)
            {
                RecordUndoableAction(e);
            }


            base.OnModelChildChanged(e);


            //if this is not a model which has a view then we need 
            // to queue a plan process change here
            if (processUndo)
            {
                Type modelType = e.ChildObject.GetType();
                if (modelType != typeof(PlanogramProduct)
                   && modelType != typeof(PlanogramPosition)
                   && modelType != typeof(PlanogramFixture)
                   && modelType != typeof(PlanogramFixtureItem)
                   && modelType != typeof(PlanogramFixtureComponent)
                   && modelType != typeof(PlanogramFixtureAssembly)
                   && modelType != typeof(PlanogramAssemblyComponent)
                   && modelType != typeof(PlanogramComponent)
                   && modelType != typeof(PlanogramSubComponent)
                   && modelType != typeof(PlanogramAnnotation)
                   && modelType != typeof(PlanogramSequenceGroupList) // We're also not interested in changes to the sequence groups, 
                   && modelType != typeof(PlanogramSequenceGroupProductList) // because we handle them separately.
                   && modelType != typeof(PlanogramSequenceGroupProduct)
                   && modelType != typeof(PlanogramSequenceGroup))
                {
                    QueueProcessPlanChanges();
                }
            }

        }

        /// <summary>
        /// Carries out any actions required in response to a child object change.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnChildChanged(ViewModelChildChangedEventArgs e)
        {
            //Don't do anything if this was just an isupdating flag.
            if (e.PropertyChangedArgs != null
                && (e.PropertyChangedArgs.PropertyName == "IsUpdating"
                || e.PropertyChangedArgs.PropertyName == "CustomAttributes"
                || e.PropertyChangedArgs.PropertyName.EndsWith("Async")
                || e.PropertyChangedArgs.PropertyName == "IsSelectable"
                || e.PropertyChangedArgs.PropertyName.Contains("Image")
                || e.PropertyChangedArgs.PropertyName.StartsWith("Meta"))) return;

            //record this action.
            //RecordUndoableAction(e);

            base.OnChildChanged(e);


            //if new items have been added then set their initial selectable state
            if (e.CollectionChangedArgs != null)
            {
                if (e.CollectionChangedArgs.NewItems != null)
                {
                    foreach (Object o in e.CollectionChangedArgs.NewItems)
                    {
                        InitialiseSelectionState(o as IPlanItem);
                    }
                }
            }


            //flag any changes before process
            //if we are not already processing.
            if (this.IsProcessingPlanChanges) return;

            #region Collection Changed Events:
            if (e.CollectionChangedArgs != null)
            {
                //flag the plan size as outdated.
                _isSizeOutdated = true;

                if (e.ChildObject is PlanogramFixtureView)
                {
                    Flag(((PlanogramFixtureView)e.ChildObject).FixtureItemModel);
                }
                else if (e.ChildObject is PlanogramAssemblyView)
                {
                    Flag(((PlanogramAssemblyView)e.ChildObject).AssemblyModel);
                }
                else if (e.ChildObject is PlanogramComponentView)
                {
                    Flag(((PlanogramComponentView)e.ChildObject).ComponentModel);
                }
                else if (e.ChildObject is PlanogramSubComponentView)
                {
                    Flag(((PlanogramSubComponentView)e.ChildObject).Model);
                }

            }
            #endregion
            

            #region Property Changes
            if (e.PropertyChangedArgs != null)
            {
                //flag the plan size as updated if a bounds property has changed.
                String[] boundsProperties = new String[] { "Height", "Width", "Depth", "X", "Y", "Z", "Slope", "Angle", "Roll" };
                if (boundsProperties.Contains(e.PropertyChangedArgs.PropertyName))
                {
                    _isSizeOutdated = true;
                }


                String typeName = e.ChildObject.GetType().Name;
                String propertyName = e.PropertyChangedArgs.PropertyName;

                switch (typeName)
                {
                    //PlanogramPosition
                    case "PlanogramPositionView":
                        {
                            PlanogramPositionView position = e.ChildObject as PlanogramPositionView;
                            if (position != null &&
                                MerchGroupProcessingHelper.IsMerchandisingGroupProcessRequired(position, propertyName))
                            {
                                Flag(position.SubComponent.Model);
                            }
                        }
                        break;
                    

                    //PlanogramProduct
                    case "PlanogramProductView":
                        {
                            PlanogramProductView product = e.ChildObject as PlanogramProductView;
                            if (product != null &&
                                MerchGroupProcessingHelper.IsMerchandisingGroupProcessRequired(product, propertyName))
                            {
                                Flag(product.Model);
                            }
                        }
                        break;
                   

                    //PlanogramSubComponent
                    case "PlanogramSubComponentView":
                        {
                            PlanogramSubComponentView sub = e.ChildObject as PlanogramSubComponentView;

                            if (sub != null
                                && MerchGroupProcessingHelper.IsMerchandisingGroupProcessRequired(sub, propertyName))
                            {
                                PlanogramMerchandisingGroup merchGroup = sub.GetMerchandisingGroup();

                                //resequence positions if a merch strategy has changed
                                if (propertyName == PlanogramSubComponent.MerchandisingStrategyXProperty.Name
                                    && merchGroup != null)
                                {
                                    merchGroup.ResequencePositionsByX();

                                    if (this.IsRecordingUndoableAction)
                                    {
                                        Flag(sub.Model);
                                    }
                                    else
                                    {
                                        NotifyMerchandisingGroupChanged(merchGroup);
                                        return;
                                    }
                                }
                                else if (propertyName == PlanogramSubComponent.MerchandisingStrategyYProperty.Name
                                    && merchGroup != null)
                                {
                                    merchGroup.ResequencePositionsByY();

                                    if (this.IsRecordingUndoableAction)
                                    {
                                        Flag(sub.Model);
                                    }
                                    else
                                    {
                                        NotifyMerchandisingGroupChanged(merchGroup);
                                        return;
                                    }
                                }
                                else if (propertyName == PlanogramSubComponent.MerchandisingStrategyZProperty.Name
                                    && merchGroup != null)
                                {
                                    merchGroup.ResequencePositionsByZ();

                                    if (this.IsRecordingUndoableAction)
                                    {
                                        Flag(sub.Model);
                                    }
                                    else
                                    {
                                        NotifyMerchandisingGroupChanged(merchGroup);
                                        return;
                                    }
                                }
                                else
                                {
                                    Flag(sub.Model);
                                }
                            }
                        }
                        break;
                    

                    //PlanogramComponent
                    case "PlanogramComponentView":
                        {
                            PlanogramComponentView component = e.ChildObject as PlanogramComponentView;
                            if (component != null &&
                                MerchGroupProcessingHelper.IsMerchandisingGroupProcessRequired(component, propertyName))
                            {
                                Flag(component.ComponentModel);
                            }
                        }
                        break;
                    

                    //PlanogramAssembly
                    case "PlanogramAssemblyView":
                        {
                            PlanogramAssemblyView assembly = e.ChildObject as PlanogramAssemblyView;
                            if (assembly != null) Flag(assembly.AssemblyModel);
                        }
                        break;
                    

                   //PlanogramFixture
                    case "PlanogramFixtureView":
                        {
                            PlanogramFixtureView fixture = e.ChildObject as PlanogramFixtureView;
                            if (fixture != null
                                && MerchGroupProcessingHelper.IsMerchandisingGroupProcessRequired(fixture, propertyName))
                            {
                                Flag(fixture.FixtureItemModel);
                            }
                        }
                        break;
                        

                }
            }
            #endregion


            //queue a reprocess of plan changes
            QueueProcessPlanChanges();
        }

        #endregion

        #region Methods

        #region Model Methods

        /// <summary>
        /// Update this planogram view with the given model.
        /// </summary>
        /// <param name="planModel"></param>
        internal void UpdateView(Planogram planModel)
        {
            SetModel(planModel, ViewStateObjectModelChangeReason.Update);
        }

        /// <summary>
        /// Sets the model property value.
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="reason"></param>
        protected override void SetModel(Planogram newModel, ViewStateObjectModelChangeReason reason)
        {
            EndUndoableAction(); //end any ongoing record.

            // Before we change the model, we're going to need to cache all the divider positions so that
            // we can re-add them once we've re-generated the view models.
            Dictionary<Object, IEnumerable<PlanogramSubComponentDivider>> dividersBySubComponentId =
                this.EnumerateAllSubComponents().ToLookupDictionary(s => s.Model.Id, s => s.Dividers.Select(d => d));

            //make sure this is no longer flagged as frozen.
            Boolean wasFrozen = _isFrozen;
            _isFrozen = false;

            //prevent reprocessing until we are done.
            this.IsProcessingPlanChanges = true;
            _suppressChangeRecording = true;

            Planogram oldModel = Model;

            //update for the model change.
            ClearAllUndoRedoActions();

            //unsubscribe from the 
            if (oldModel != null)
            {
                OnRemoveEventHooks(oldModel);
            }

            Model = newModel;
            OnPropertyChanged("Model");

            if (newModel != null)
            {
                //clean up the model before we load it.
                newModel.CleanUpOrphanedParts();

                //force the immediate load of some of the planogram child objects that we
                // know will be required immediately.
                // This is purely so that they are fetched whilst the load screen is visible 
                // and the reprocess is halted.
                newModel.RenumberingStrategy.LoadChildren();
                newModel.Inventory.LoadChildren();
                newModel.Sequence.LoadChildren();
                newModel.Assortment.LoadChildren();
                newModel.Blocking.LoadChildren();
                newModel.Products.LoadChildren(); //to force cust attributes load.
                newModel.ConsumerDecisionTree.LoadChildren();
                newModel.Comparison.LoadChildren();
                newModel.EventLogs.LoadChildren();
                newModel.ValidationTemplate.LoadChildren();

                //attach to the new model.
                OnAddEventHooks(newModel);
            }

            //reset all child views.
            OnPlanogramProductsBulkCollectionChanged(new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));
            OnPlanogramFixtureItemsBulkCollectionChanged(new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));
            InitialiseSelectableChildren();

            this.DisplayUnits = (newModel != null) ? DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(newModel)
                : DisplayUnitOfMeasureCollection.Empty;


            //fire the notification event.
            if (ModelChanged != null)
            {
                ModelChanged(this, new ViewStateObjectModelChangedEventArgs<Planogram>(reason, newModel, oldModel, null));
            }

            // Now that we've changed the model we need to re-add dividers to the subcomponent views, because they won't have been
            // added when they were re-instantiated.
            this.UpdateDividers(dividersBySubComponentId);

            //Reprocess the plan immediately to ensure that everything is correctly displayed.
            // We only need to do this however if the plan was not previously frozen.
            this.IsProcessingPlanChanges = false;
            if (!wasFrozen)
            {
                //nb - dont queue here - process immediately.
                ProcessPlanChanges(/*reprocessAll*/true);
            }

            _suppressChangeRecording = false;
        }

        /// <summary>
        /// Freezes the current plan.
        /// Stopping it from reprocessing events.
        /// </summary>
        public void FreezePlan()
        {
            _isFrozen = true;

            _preventProcessQueueing = true;

            //cancel any queued processing
            _planProcessDelayTimer.Stop();
            CancelPostProcessing(false);


            //Dispose of any groups in the pending list that are not being processed
            foreach (PlanogramMerchandisingGroup pendingGroup in _pendingMerchGroups)
            {
                pendingGroup.Dispose();
            }
            _pendingMerchGroups.Clear();

            //Clean up any orphaned parts
            this.Model.CleanUpOrphanedParts();

            //ensure the plan is fully up to date before we freeze it:
            Planogram plan = this.Model;
            using (PlanogramMerchandisingGroupList merchGroupList = plan.GetMerchandisingGroups())
            {
                //Autofill
                if (plan.ProductPlacementX == PlanogramProductPlacementXType.FillWide
                    || plan.ProductPlacementY == PlanogramProductPlacementYType.FillHigh
                    || plan.ProductPlacementZ == PlanogramProductPlacementZType.FillDeep)
                {
                    merchGroupList.RemerchandiseAllGroups();

                    foreach (PlanogramMerchandisingGroup merchGroup in merchGroupList)
                    {
                        Boolean isFirst = true;
                        foreach (var sub in merchGroup.SubComponentPlacements)
                        {
                            PlanogramSubComponentView subView = FindSubComponentView(sub);
                            if (subView == null) continue;

                            if (isFirst)
                            {
                                subView.UpdateDividers(merchGroup.DividerPlacements, 0);
                                isFirst = false;
                            }
                            else
                            {
                                //null off others.
                                subView.UpdateDividers(new List<PlanogramDividerPlacement>(), 0);
                            }
                        }
                    }
                }
                else if (_flaggedModels.Any())
                {
                    //just process changed groups normally.
                    List<PlanogramSubComponentPlacementId> dirtyPlacementIds =
                        GetDirtySubComponentPlacementIds(GetSubComponentGroups());

                    foreach (PlanogramMerchandisingGroup merchGroup in merchGroupList)
                    {
                        //only process if the group is not fully up to date.
                        if (!merchGroup.SubComponentPlacements.Select(s => s.Id).Any(id => dirtyPlacementIds.Contains(id)))
                            continue;

                        merchGroup.Process();
                        merchGroup.ApplyEdit();

                        Boolean isFirst = true;
                        foreach (var sub in merchGroup.SubComponentPlacements)
                        {
                            PlanogramSubComponentView subView = FindSubComponentView(sub);
                            if (subView == null) continue;

                            if (isFirst)
                            {
                                subView.UpdateDividers(merchGroup.DividerPlacements, 0);
                                isFirst = false;
                            }
                            else
                            {
                                //null off others.
                                subView.UpdateDividers(new List<PlanogramDividerPlacement>(), 0);
                            }
                        }
                    }
                }
                _flaggedModels.Clear();


                //Update the planogram size
                if (_isSizeOutdated) UpdatePlanogramBounds();

                //  Reapply the renumbering strategy.
                plan.RenumberingStrategy.RenumberBays();

                //Update sequence template
                if (plan.PlanogramType == PlanogramType.SequenceTemplate && plan.Sequence != null)
                {
                    plan.Sequence.ApplyFromMerchandisingGroups(merchGroupList);
                }

                //nb - dont need to update metadata as background catchup will do this anyway.
            }


            _isPlanChanged = false;
            _preventProcessQueueing = false;

            //Now detach the view from the model so that
            // updates are no longer processed.
            OnRemoveEventHooks(this.Model);

            this.IsPlanUpToDate = true;

            //dispose of children to dettach event handlers
            // they will be recreated when the plan model gets reloaded anyway.
            foreach (PlanogramFixtureView fixtureView in this.Fixtures)
            {
                fixtureView.Dispose();
            }
            foreach (PlanogramProductView productView in this.Products)
            {
                productView.Dispose();
            }

        }

        #endregion

        #region Plan Processing

        /// <summary>
        /// Queues another plan process action
        /// </summary>
        /// <param name="reprocessAll"></param>
        private void QueueProcessPlanChanges()
        {
            if (this.IsProcessingPlanChanges) return;
            if (_preventProcessQueueing) return;

            _isPlanChanged = true;

            //Don't allow queuing if we are processing a group of  undoable actions.
            // or we are flagged as making a big change
            if (IsRecordingUndoableAction) return;
            if (this.IsUpdating) return;

            //flag to the ui that the plan is no longer up to date.
            this.IsPlanUpToDate = false;

            if (App.IsUnitTesting)
            {
                //if we are unit testing the just reprocess immediately.
                ProcessPlanChanges();
            }
            else
            {
                //restart the timer
                _planProcessDelayTimer.Stop();
                _planProcessDelayTimer.Start();
            }

        }

        /// <summary>
        /// Called when the plan process delay timer elapses.
        /// </summary>
        private void PlanProcessTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //call the process method use the dispatcher to do this
            //so that we are back on the correct thread to update the ui.
            App.Current.Dispatcher.BeginInvoke((Action)(() => ProcessPlanChanges()), priority: DispatcherPriority.Normal);
        }

        /// <summary>
        /// Processes the plan, updating sizes and replacing positions.
        /// </summary>
        /// <param name="reprocessAll"></param>
        private void ProcessPlanChanges(Boolean reprocessAll = false)
        {
            if (this.IsProcessingPlanChanges) return;
            if (this.Model == null) return;
            if (_isFrozen) return;

            _isPlanChanged = false;

            //get the list of groups that need to be reprocesed.
            List<PlanogramMerchandisingGroup> merchGroupsToReprocess = GetMerchGroupsToReprocess(reprocessAll);

            //reprocess the groups
            NotifyMerchandisingGroupChanged(merchGroupsToReprocess);
        }

        /// <summary>
        /// The main process method. 
        /// </summary>
        /// <param name="reprocessAll">If false, only groups flagged as dirty will be processed.</param>
        private List<PlanogramMerchandisingGroup> GetMerchGroupsToReprocess(Boolean reprocessAll)
        {
            //dont bother if we have no groups to process.
            if (!reprocessAll && !_flaggedModels.Any()) return new List<PlanogramMerchandisingGroup>();

            if (reprocessAll)
            {
                List<PlanogramMerchandisingGroup> merchGroupList = this.Model.GetMerchandisingGroups();

                List<SubComponentGroup> subGroups = new List<SubComponentGroup>(merchGroupList.Count);
                foreach (PlanogramMerchandisingGroup merchGroup in merchGroupList)
                {
                    subGroups.Add(new SubComponentGroup(merchGroup));
                }

                //store the last groups so that we can compare next time.
                _prevGroups.Clear();
                _prevGroups.AddRange(subGroups);

                _flaggedModels.Clear();

                return merchGroupList;
            }
            else
            {
                //determine the merch groups to be processed 
                List<SubComponentGroup> subGroups = GetSubComponentGroups();


                //get the list of dirty placement ids.
                List<PlanogramSubComponentPlacementId> dirtyPlacementIds = GetDirtySubComponentPlacementIds(subGroups);
                _flaggedModels.Clear();

                List<PlanogramMerchandisingGroup> groupsToReprocess = new List<PlanogramMerchandisingGroup>();

                foreach (SubComponentGroup subGrouping in subGroups)
                {
                    if (reprocessAll || subGrouping.Subs.Any(s => dirtyPlacementIds.Contains(s.Id)))
                    {
                        groupsToReprocess.Add(PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subGrouping.Subs));
                    }
                }

                //store the last groups so that we can compare next time.
                _prevGroups.Clear();
                _prevGroups.AddRange(subGroups);

                return groupsToReprocess;
            }
        }

        /// <summary>
        /// Notifies the planogram that changes have been made to the given merchandising group which should now be applied.
        /// </summary>
        public void NotifyMerchandisingGroupChanged(PlanogramMerchandisingGroup group)
        {
            NotifyMerchandisingGroupChanged(new PlanogramMerchandisingGroup[] { group });
        }

        /// <summary>
        /// Notifies the planogram that changes have been made to the given merchandising groups which should now be applied.
        /// </summary>
        /// <param name="changedGroups">the changed groups.</param>
        public void NotifyMerchandisingGroupChanged(IEnumerable<PlanogramMerchandisingGroup> changedGroups)
        {
            if (this.IsProcessingPlanChanges) return;

            using (CodePerformanceMetric m = new CodePerformanceMetric())
            {
                this.IsProcessingPlanChanges = true;
                BeginUpdate();

                Planogram plan = this.Model;

                //flag to the ui that the plan is no longer up to date.
                this.IsPlanUpToDate = false;

                //ensure that the plan caches are clear before we start.
                ((IModelReadOnlyObject)plan).ClearCache();

                //Dispose of any groups in the pending list that are not being processed
                foreach (PlanogramMerchandisingGroup pendingGroup in _pendingMerchGroups)
                {
                    if (!changedGroups.Contains(pendingGroup)) pendingGroup.Dispose();
                }
                _pendingMerchGroups.Clear();

                //Apply the changes
                foreach (PlanogramMerchandisingGroup merchGroup in changedGroups)
                {
                    //Add the subcomponent ids to be autofilled.
                    foreach (var subPlacement in merchGroup.SubComponentPlacements)
                    {
                        if (!_pendingAutofillSubcomponentIds.Contains(subPlacement.SubComponent.Id))
                        {
                            _pendingAutofillSubcomponentIds.Add(subPlacement.SubComponent.Id);
                        }
                    }

                    //perform a process of the group
                    // if autofill is required this will be processed again in the background later.
                    merchGroup.Process();
                    merchGroup.ApplyEdit();

                    Boolean isFirst = true;
                    foreach (var sub in merchGroup.SubComponentPlacements)
                    {
                        PlanogramSubComponentView subView = FindSubComponentView(sub);
                        if (subView == null) continue;

                        if (isFirst)
                        {
                            subView.UpdateDividers(merchGroup.DividerPlacements, 0);
                            isFirst = false;
                        }
                        else
                        {
                            //null off others.
                            subView.UpdateDividers(new List<PlanogramDividerPlacement>(), 0);
                        }
                    }

                    //dispose of the group now that we are done with it
                    merchGroup.Dispose();
                }


                //update any other planogram data:

                //Update the planogram size
                if (_isSizeOutdated) UpdatePlanogramBounds();

                //Reapply renumbering strategy:
                plan.RenumberingStrategy.RenumberBays();

                //if an undoable action is active, but we are not recording
                // then complete it.
                if (_processingUndoAction != null && !IsRecordingUndoableAction)
                {
                    CompleteUndoableAction();
                }

                //Raise PlanProcessCompleting early to allow labels and highlights to update that don't require metadata.
                OnPlanProcessCompleting();

                //now kick off a meta data and validation warnings update
                //BeginPlanBackgroundProcessing();


                EndUpdate();
                this.IsProcessingPlanChanges = false;

                // Finally, notify any subscribers that we've processed some merchandising groups.
                if (changedGroups.Any() && MerchandisingGroupsProcessed != null)
                {
                    MerchandisingGroupsProcessed(this, EventArgs.Empty);
                }
            }

            //now kick off any processing that can take place asynchronously.
            BeginPlanPostProcessing();
        }

        #region Post Processing

        /// <summary>
        /// Begins procssing all updates which can be done asyncronously.
        /// </summary>
        private void BeginPlanPostProcessing()
        {
            if (_postProcessingCancellationSource != null)
            {
                _postProcessingCancellationSource.Cancel();
                _isBackgroundProcessRestartRequired = true;
                return;
            }

            if (this.Model == null || this.Model.Parent == null) return;

            //flag to the ui that the plan is no longer up to date.
            //this flag may have already been set but this is just to make sure.
            this.IsPlanUpToDate = false;
            _isBackgroundProcessRestartRequired = false;

            PostProcessContext processContext =
                new PostProcessContext(this.Model, App.ViewState.EntityId, _pendingAutofillSubcomponentIds.ToList());
            processContext.ShouldCalculateMetadata = _shouldCalculateMetadata;

            if (/*isAsync*/!_isFrozen && !App.IsUnitTesting)
            {
                _postProcessingCancellationSource = new CancellationTokenSource();
                CancellationToken ct = _postProcessingCancellationSource.Token;

                processContext.CancellationToken = ct;

                //First perform secondary priority tasks,
                //these are tasks which need to be processed async but should not wait ages before the plan is updated.
                // e.g. autofill
                System.Threading.Tasks.Task.Factory.StartNew<PostProcessContext>(
                    (o) =>
                    {
                        PostProcessContext args = (PostProcessContext)o;
                        if (!ct.IsCancellationRequested)
                        {
                            try
                            {
                                OnDoSecondaryProcessing(args);
                            }
                            catch (Exception ex)
                            {
                                args.Error = ex;
                            }
                        }
                        return args;

                    }, processContext, ct,
                    TaskCreationOptions.PreferFairness,
                    TaskScheduler.Default)

                    //now update the plan on the main thread:
                    .ContinueWith(
                    (t) =>
                    {
                        Boolean wasSuccess = DoSecondaryProcessingCompletedTask(t, ct);
                        if (!wasSuccess || !t.Result.ShouldCalculateMetadata) return;


                        //kick off tertiary processing
                        System.Threading.Tasks.Task.Factory.StartNew<PostProcessContext>(
                            (o) =>
                            {
                                if (!ct.IsCancellationRequested)
                                {
                                    try
                                    {
                                        OnDoTertiaryProcessing((PostProcessContext)o);
                                    }
                                    catch (Exception ex)
                                    {
                                        ((PostProcessContext)o).Error = ex;
                                    }
                                }
                                return (PostProcessContext)o;
                            },
                            t.Result,
                            ct, //use the original token so this does not excute if the prev task was cancelled.
                            TaskCreationOptions.LongRunning | TaskCreationOptions.PreferFairness,
                            TaskScheduler.Default)

                            //update the last round of processing back to the plan on the main thread.
                            .ContinueWith(
                            (tt) =>
                            {
                                DoTertiaryProcessingCompletedTask(tt, ct);
                            },
                            CancellationToken.None, //no token so this aways executes
                            TaskContinuationOptions.OnlyOnRanToCompletion | TaskContinuationOptions.PreferFairness | TaskContinuationOptions.ExecuteSynchronously,
                            App.UITaskScheduler);//back on the main thread


                    },
                    CancellationToken.None, //no token so this aways executes
                    TaskContinuationOptions.None | TaskContinuationOptions.PreferFairness | TaskContinuationOptions.ExecuteSynchronously,
                    App.UITaskScheduler);//back on the main thread
            }
            else
            {
                //Carry out the same tasks synchronously.
                OnDoSecondaryProcessing(processContext);
                OnSecondaryProcessingCompleted(processContext);
                if (processContext.ShouldCalculateMetadata)
                {
                    OnDoTertiaryProcessing(processContext);
                    OnTertiaryProcessingCompleted(processContext);
                }
                OnPostProcessingCompleted();
            }

        }

        /// <summary>
        /// Called when post processing has fully completed.
        /// </summary>
        private void OnPostProcessingCompleted()
        {
            //dispose of the ct token
            if (_postProcessingCancellationSource != null)
            {
                _postProcessingCancellationSource.Dispose();
                _postProcessingCancellationSource = null;
            }

            //notify all fixtures
            foreach (var fixture in this.Fixtures) fixture.NotifyPlanProcessCompleting();

            //flag to the ui that the plan is now up to date
            this.IsPlanUpToDate = true;
        }

        /// <summary>
        /// Called when post processing was cancelled or faulted.
        /// </summary>
        private void OnPostProcessingCancelledOrFaulted(PostProcessContext context, Exception error)
        {
            if (error != null)
            {
                CommonHelper.RecordException(error);
                _isBackgroundProcessRestartRequired = true;
            }

            if (context != null && context.AutofillMerchGroups != null)
            {
                context.AutofillMerchGroups.Dispose();
                context.AutofillMerchGroups = null;
            }

            //dispose of the ct token
            if (_postProcessingCancellationSource != null)
            {
                _postProcessingCancellationSource.Dispose();
                _postProcessingCancellationSource = null;
            }

            //check if we need to restart
            if (_isBackgroundProcessRestartRequired)
            {
                BeginPlanPostProcessing();
            }
            else
            {
                this.IsPlanUpToDate = true;
            }
        }

        /// <summary>
        /// Carries out secondary priority processing.
        /// These are actions which can be done on a delay but are needed fairly soon - e.g. autofill.
        /// </summary>
        /// <param name="context"></param>
        private void OnDoSecondaryProcessing(PostProcessContext context)
        {
            using (CodePerformanceMetric m = new CodePerformanceMetric())
            {
                context.DividersToUpdateBySubComponentId = null;
                context.MerchandisingVolumePercentages = null;

                if (context.CancellationToken != null && context.CancellationToken.IsCancellationRequested) return;

                Planogram plan = context.Planogram;

                //ensure that the plan caches are clear before we start.
                ((IModelReadOnlyObject)plan).ClearCache();

                PlanogramMerchandisingGroupList merchGroupList = plan.GetMerchandisingGroups();

                //using (PlanogramMerchandisingGroupList merchGroupList = plan.GetMerchandisingGroups())
                {
                    //Blocking Merchandising Space:
                    Single designWidth;
                    Single designHeight;
                    Single designHeightOffset;
                    context.Planogram.GetBlockingAreaSize(out designHeight, out designWidth, out designHeightOffset);
                    context.MerchandisingVolumePercentages = plan.Blocking.ToDictionary(
                        b => b.Id,
                        b => b.GetMerchandisingVolumePercentagesByGroupId(merchGroupList, designWidth, designHeight, designHeightOffset));


                    if (context.CancellationToken != null && context.CancellationToken.IsCancellationRequested) return;

                    // Autofill:
                    if (plan.ProductPlacementX == PlanogramProductPlacementXType.FillWide
                                || plan.ProductPlacementY == PlanogramProductPlacementYType.FillHigh
                                || plan.ProductPlacementZ == PlanogramProductPlacementZType.FillDeep)
                    {
                        context.AutofillMerchGroups = merchGroupList;

                        //if we have any hanging components shrink down all of the affected groups first.
                        // This is so that bars which overlap with shelves get processed correctly.
                        if (merchGroupList.Any(g => g.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang
                            || g.MerchandisingType == PlanogramSubComponentMerchandisingType.HangFromBottom))
                        {
                            foreach (PlanogramMerchandisingGroup merchGroup in merchGroupList)
                            {
                                Boolean shouldFillX = (plan.ProductPlacementX == PlanogramProductPlacementXType.FillWide
                                    && merchGroup.StrategyX != PlanogramSubComponentXMerchStrategyType.Manual);

                                Boolean shouldFillY = (plan.ProductPlacementY == PlanogramProductPlacementYType.FillHigh
                                    && merchGroup.StrategyY != PlanogramSubComponentYMerchStrategyType.Manual);

                                Boolean shouldFillZ = (plan.ProductPlacementZ == PlanogramProductPlacementZType.FillDeep
                                    && merchGroup.StrategyZ != PlanogramSubComponentZMerchStrategyType.Manual);

                                if ((shouldFillX || shouldFillY || shouldFillZ)
                                && merchGroup.SubComponentPlacements.Any(s => context.AutofillSubComponentIds.Contains(s.SubComponent.Id)))
                                {
                                    foreach (PlanogramPositionPlacement positionPlacement in merchGroup.PositionPlacements)
                                    {
                                        positionPlacement.SetMinimumUnits(shouldFillX, shouldFillY, shouldFillZ);
                                    }
                                }
                            }
                        }
                        if (context.CancellationToken != null && context.CancellationToken.IsCancellationRequested) return;

                        //Process the autofill working from the top to the bottom of the plan.
                        foreach (PlanogramMerchandisingGroup merchGroup in
                            merchGroupList.OrderByDescending(mgl => mgl.SubComponentPlacements.First().GetPlanogramRelativeCoordinates().Y))
                        {
                            Boolean shouldFillX = (plan.ProductPlacementX == PlanogramProductPlacementXType.FillWide
                                && merchGroup.StrategyX != PlanogramSubComponentXMerchStrategyType.Manual);

                            Boolean shouldFillY = (plan.ProductPlacementY == PlanogramProductPlacementYType.FillHigh
                                && merchGroup.StrategyY != PlanogramSubComponentYMerchStrategyType.Manual);

                            Boolean shouldFillZ = (plan.ProductPlacementZ == PlanogramProductPlacementZType.FillDeep
                                && merchGroup.StrategyZ != PlanogramSubComponentZMerchStrategyType.Manual);

                            if ((shouldFillX || shouldFillY || shouldFillZ)
                                && merchGroup.SubComponentPlacements.Any(s => context.AutofillSubComponentIds.Contains(s.SubComponent.Id)))
                            {
                                merchGroup.Autofill(merchGroupList);
                                if (context.CancellationToken != null && context.CancellationToken.IsCancellationRequested) break;

                                //merchGroup.ApplyEdit();

                                // Add the dividers we need to update to the output dictionary for the first
                                // subcomponent in the merch group.
                                if (context.DividersToUpdateBySubComponentId == null)
                                {
                                    context.DividersToUpdateBySubComponentId = new Dictionary<Object, IEnumerable<PlanogramSubComponentDivider>>();
                                }
                                context.DividersToUpdateBySubComponentId[merchGroup.SubComponentPlacements.First().SubComponent.Id] =
                                    merchGroup.DividerPlacements.Select(d => new PlanogramSubComponentDivider(d, 0f));

                                if (context.CancellationToken != null && context.CancellationToken.IsCancellationRequested) break;
                            }
                        }

                    }
                    else
                    {
                        merchGroupList.Dispose();
                        merchGroupList = null;
                    }
                }
            }
        }

        /// <summary>
        /// Called when secondary processing is complete to update the results
        /// back to the planogram model.
        /// </summary>
        /// <param name="context"></param>
        private void OnSecondaryProcessingCompleted(PostProcessContext context)
        {
            using (CodePerformanceMetric m = new CodePerformanceMetric())
            {
                // Indicate that the blocking merchandising percentages can be updated.
                if (MerchandisingVolumePercentagesChanged != null && context.MerchandisingVolumePercentages != null)
                {
                    MerchandisingVolumePercentagesChanged(
                        this, new EventArgs<Dictionary<Object, Dictionary<Object, Double>>>(context.MerchandisingVolumePercentages));
                }

                //Before we do anything take a note of whether the plan was marked as initialized.
                // If it was then we don't want the metadata update to affect this.
                Boolean isPlanInitialized = this.Model.IsInitialized;

                if (context.AutofillMerchGroups != null)
                {
                    //apply the metadata changes back to the current plan model
                    //_suppressChangeRecording = true;
                    _preventProcessQueueing = true;
                    BeginUndoablePostProcessAction();
                    BeginUpdate();

                    foreach (PlanogramMerchandisingGroup merchGroup in context.AutofillMerchGroups)
                    {
                        merchGroup.ApplyEdit();
                    }
                    context.AutofillMerchGroups.Dispose();
                    context.AutofillMerchGroups = null;


                    // Update any dividers that the background worker execution told us about.
                    UpdateDividers(context.DividersToUpdateBySubComponentId);


                    EndUpdate();
                    EndUndoableBackgroundProcessAction();

                    //_suppressChangeRecording = false;
                    _preventProcessQueueing = false;
                }


                //Raise the event to allow any other items to update as required
                OnPlanProcessCompleting();

                //the planogram was in an initialized state before we updated, then force it back.
                if (isPlanInitialized) this.Model.MarkGraphAsInitialized();

                // Remove any pending autofill subcomponent ids.
                context.AutofillSubComponentIds.ForEach(id => _pendingAutofillSubcomponentIds.Remove(id));
            }
        }

        /// <summary>
        /// Carries out processing which can be done on a delay and is not really urgent.
        /// e.g. metadata.
        /// </summary>
        /// <param name="context"></param>
        private static void OnDoTertiaryProcessing(PostProcessContext context)
        {
            using (CodePerformanceMetric m = new CodePerformanceMetric())
            {
                // Clone the planogram in advance of the metadata and sequencing calculations.
                // If the plan is not cloned, this can potentially cause issues for methods
                // that are executed on the planogram, since the planogra, instance is not owned
                // by this thread. In particular, at this time (V8-32997) cloning is required for
                // the plan to be re-sequenced when it is a sequence template.
                SetPlanogramClone(context);

                if (context.CancellationToken.IsCancellationRequested) return;

                Planogram plan = context.Planogram;

                using (PlanogramMerchandisingGroupList merchGroupList = plan.GetMerchandisingGroups())
                {
                    // Update the sequence in the planogram, if necessary. We do this before the 
                    // metadata is calculated, because the sequencing can affect the result of
                    // some metadata calculations.
                    if (plan.PlanogramType == PlanogramType.SequenceTemplate && plan.Sequence != null)
                    {
                        plan.Sequence.ApplyFromMerchandisingGroups(merchGroupList);
                    }

                    PlanogramMetadataHelper metadataHelper =
                        PlanogramMetadataHelper.NewPlanogramMetadataHelper(context.EntityId, plan.Parent);

                    context.PlanWarnings = plan.CalculateMetadataAndGetValidationWarnings(
                        /*createImages*/false, metadataHelper, merchGroupList);

                }
            }
        }

        /// <summary>
        /// Sets a clone of the current planogram against the context.
        /// This is a temporary method which we need to look at removing to improve performance.
        /// </summary>
        /// <param name="context"></param>
        private static void SetPlanogramClone(PostProcessContext context)
        {
            using (CodePerformanceMetric p = new CodePerformanceMetric())
            {
                Planogram src = (Planogram)context.Planogram;
                Package pkClone = src.Parent.Clone(false);
                Planogram planClone = pkClone.Planograms[src.Parent.Planograms.IndexOf(src)];
                context.Planogram = planClone;
            }
        }

        /// <summary>
        /// Called when tertiary processing is completed to update the results
        /// back to the planogram model.
        /// </summary>
        /// <param name="context"></param>
        private void OnTertiaryProcessingCompleted(PostProcessContext context)
        {
            using (CodePerformanceMetric m = new CodePerformanceMetric())
            {
                _suppressChangeRecording = true;

                //Before we do anything take a note of whether the plan was marked as initialized.
                // If it was then we don't want the metadata update to affect this.
                Boolean isPlanInitialized = this.Model.IsInitialized;


                //apply the metadata changes back to the current plan model
                _preventProcessQueueing = true;
                //BeginUndoableAction();
                BeginUpdate();

                //if the model are not the same then we need to copy accross the
                //calculated values.
                if (context.Planogram != this.Model)
                {
                    try
                    {
                        CopyPlanogramMetadataState(context.Planogram, this.Model);
                    }
                    catch (Exception ex)
                    {
                        CommonHelper.RecordException(ex);
                    }
                }


                EndUpdate();
                //EndUndoableBackgroundProcessAction();


                _preventProcessQueueing = false;

                //update warnings
                UpdateValidationWarnings(context.PlanWarnings);


                //Raise the event to allow any other items to update as required
                OnPlanProcessCompleting();

                //the planogram was in an initialized state before we updated, then force it back.
                if (isPlanInitialized) this.Model.MarkGraphAsInitialized();

                _suppressChangeRecording = false;
            }
        }

        /// <summary>
        /// Cancels any ongoing async post process task.
        /// </summary>
        /// <param name="restart"></param>
        private void CancelPostProcessing(Boolean restart)
        {
            if (_postProcessingCancellationSource != null)
            {
                _postProcessingCancellationSource.Cancel();
                _isBackgroundProcessRestartRequired = restart;
            }
        }


        #region Post Processing helpers

        /// <summary>
        /// Class to hold args required by the post process.
        /// </summary>
        private class PostProcessContext
        {
            public CancellationToken CancellationToken { get; set; }
            public Exception Error { get; set; }

            public Planogram Planogram { get; set; }
            public List<Object> AutofillSubComponentIds { get; private set; }
            public Int32 EntityId { get; private set; }

            public Dictionary<Object, IEnumerable<PlanogramSubComponentDivider>> DividersToUpdateBySubComponentId { get; set; }
            public Dictionary<Object, Dictionary<Object, Double>> MerchandisingVolumePercentages { get; set; }
            public List<PlanogramValidationWarning> PlanWarnings { get; set; }
            public Boolean ShouldCalculateMetadata { get; set; }

            public PlanogramMerchandisingGroupList AutofillMerchGroups { get; set; }

            public PostProcessContext(Planogram plan, Int32 entityId, List<Object> autofillSubComponentIds)
            {
                Planogram = plan;
                EntityId = entityId;
                AutofillSubComponentIds = autofillSubComponentIds;
            }
        }

        private Boolean DoSecondaryProcessingCompletedTask(Task<PostProcessContext> t, CancellationToken ct)
        {
            //errored:
            if (t.IsFaulted || t.Exception != null)
            {
                OnPostProcessingCancelledOrFaulted(null, t.Exception);
                return false;
            }

            //cancelled:
            else if (t.IsCanceled || ct.IsCancellationRequested)
            {
                OnPostProcessingCancelledOrFaulted(t.Result, t.Result.Error);
                return false;
            }

            //process:
            else
            {
                try
                {
                    OnSecondaryProcessingCompleted(t.Result);
                }
                catch (Exception ex)
                {
                    t.Result.Error = ex;
                    OnPostProcessingCancelledOrFaulted(t.Result, ex);
                    return false;
                }
            }

            return true;
        }


        private Boolean DoTertiaryProcessingCompletedTask(Task<PostProcessContext> t, CancellationToken ct)
        {
            //errored:
            if (t.IsFaulted || t.Exception != null)
            {
                OnPostProcessingCancelledOrFaulted(null, t.Exception);
                return false;
            }

            //cancelled:
            else if (t.IsCanceled || ct.IsCancellationRequested)
            {
                OnPostProcessingCancelledOrFaulted(t.Result, t.Result.Error);
                return false;
            }

            //process:
            else
            {
                try
                {
                    OnTertiaryProcessingCompleted(t.Result);
                    OnPostProcessingCompleted();
                }
                catch (Exception ex)
                {
                    t.Result.Error = ex;
                    OnPostProcessingCancelledOrFaulted(t.Result, t.Result.Error);
                    return false;
                }

            }
            return true;
        }


        /// <summary>
        /// Copies all metadata values from the source planogram to the destination one.
        /// </summary>
        private static void CopyPlanogramMetadataState(Planogram source, Planogram dest)
        {
            //Positions:
            CopyPlangoramMetadataState(source.Positions, dest.Positions);

            // Sequencing information.
            if (dest.PlanogramType == PlanogramType.SequenceTemplate &&
                App.MainPageViewModel != null &&
                App.MainPageViewModel.ActivePlanController != null &&
                dest.Sequence != null &&
                !App.MainPageViewModel.ActivePlanController.IsSequenceUpToDate)
            {
                dest.Sequence.LoadFrom(source.Sequence, updateSequenceData: false);
                App.MainPageViewModel.ActivePlanController.IsSequenceUpToDate = true;

                // Update position sequence colours and numbers
                Dictionary<Object, PlanogramPosition> sourcePositionsById = source.Positions.ToDictionary(p => p.Id);
                foreach (PlanogramPosition destPosition in dest.Positions)
                {
                    PlanogramPosition sourcePosition;
                    if (!sourcePositionsById.TryGetValue(destPosition.Id, out sourcePosition)) continue;
                    destPosition.CopySequenceValues(sourcePosition);
                }
            }

            //Products
            CopyPlangoramMetadataState(source.Products, dest.Products);

            //Components
            CopyPlangoramMetadataState(source.Components, dest.Components);

            //AssemblyComponents
            CopyPlangoramMetadataState(source.Assemblies.SelectMany(a => a.Components), dest.Assemblies.SelectMany(a => a.Components));

            //Assemblies
            CopyPlangoramMetadataState(source.Assemblies, dest.Assemblies);

            //FixtureComponents
            CopyPlangoramMetadataState(source.Fixtures.SelectMany(a => a.Components), dest.Fixtures.SelectMany(a => a.Components));

            //FixtureAssemblies
            CopyPlangoramMetadataState(source.Fixtures.SelectMany(a => a.Assemblies), dest.Fixtures.SelectMany(a => a.Assemblies));

            //Fixtures
            CopyPlangoramMetadataState(source.Fixtures, dest.Fixtures);

            //FixtureItems
            CopyPlangoramMetadataState(source.FixtureItems, dest.FixtureItems);

            //Performance Data
            if (source.IsPropertyLoaded(Planogram.PerformanceProperty)
                && source.Performance.IsPropertyLoaded(PlanogramPerformance.PerformanceDataProperty))
            {
                CopyPlangoramMetadataState(source.Performance.PerformanceData, dest.Performance.PerformanceData);
            }

            //Assortment
            if (source.IsPropertyLoaded(Planogram.AssortmentProperty))
            {
                CopyPlangoramMetadataState(source.Assortment.Products, dest.Assortment.Products);
            }

            //blocking groups
            if (source.IsPropertyLoaded(Planogram.BlockingProperty))
            {
                CopyPlangoramMetadataState(source.Blocking.SelectMany(a => a.Groups), dest.Blocking.SelectMany(a => a.Groups));
            }

            //Consumer decision tree node
            if (source.IsPropertyLoaded(Planogram.ConsumerDecisionTreeProperty))
            {
                CopyConsumerDecisionTreeMetadata(source.ConsumerDecisionTree.RootNode, dest.ConsumerDecisionTree.RootNode);
            }

            //Plan
            dest.CopyMetadataState(source);

            //Package
            dest.Parent.CopyMetadataState(source.Parent);
        }

        /// <summary>
        /// Copies the values of all metadata properties from the source list items to the destination list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sourceList"></param>
        /// <param name="destList"></param>
        private static void CopyPlangoramMetadataState<T>(IEnumerable<T> sourceList, IEnumerable<T> destList)
            where T : Galleria.Framework.Planograms.Model.ModelObject<T>
        {
            //check that the copy is valid
            if (!(sourceList.Count() == destList.Count()
                && sourceList.Select(p => p.Id).SequenceEqual(destList.Select(p => p.Id))))
            {
                throw new ArgumentException();
            }

            Debug.Assert(sourceList.Count() == destList.Count());

            for (Int32 i = 0; i < sourceList.Count(); i++)
            {
                T source = sourceList.ElementAt(i);
                T dest = destList.ElementAt(i);

                dest.CopyMetadataState(source);
            }
        }

        /// <summary>
        /// Recursively copy the consumer decision tree and all child nodes down the tree
        /// </summary>
        /// <param name="sourceRootNode">The source consumer decision tree node</param>
        /// <param name="destRootNode">The destination consumer decision tree node</param>
        private static void CopyConsumerDecisionTreeMetadata(PlanogramConsumerDecisionTreeNode sourceRootNode, PlanogramConsumerDecisionTreeNode destRootNode)
        {
            if (sourceRootNode.ChildList.Count() != destRootNode.ChildList.Count())
            {
                throw new ArgumentException();
            }

            for (int subNodeCount = 0; subNodeCount < sourceRootNode.ChildList.Count(); subNodeCount++)
            {
                var srcSubNode = sourceRootNode.ChildList[subNodeCount];
                var destSubNode = destRootNode.ChildList[subNodeCount];

                CopyConsumerDecisionTreeMetadata(srcSubNode, destSubNode);
            }

            destRootNode.CopyMetadataState(sourceRootNode);
        }

        /// <summary>
        /// Updates the dividers on the child <see cref="PlanogramSubComponentView"/>s of this <see cref="PlanogramView"/>.
        /// </summary>
        /// <param name="dividersToUpdateBySubComponentId">A dictionary of the dividers to update, keyed by <see cref="PlanogramSubComponent"/> Id.</param>
        /// <remarks>
        /// If no dividers are provided in the dictionary of a <see cref="PlanogramSubComponentView"/> in this <see cref="PlanogramView"/>
        /// then the dividers for that <see cref="PlanogramSubComponentView"/> will be cleared.
        /// </remarks>
        private void UpdateDividers(Dictionary<Object, IEnumerable<PlanogramSubComponentDivider>> dividersToUpdateBySubComponentId)
        {
            if (dividersToUpdateBySubComponentId == null) return;

            foreach (PlanogramSubComponentView subCompView in this.EnumerateAllSubComponents())
            {
                IEnumerable<PlanogramSubComponentDivider> dividers;
                // If the subcomponent is in the dictionary, we need to update it.
                if (dividersToUpdateBySubComponentId.TryGetValue(subCompView.Model.Id, out dividers))
                {
                    subCompView.UpdateDividers(dividers);
                }
            }
        }

        #endregion

        #endregion


        /// <summary>
        /// Returns the merchandising group for the given placement.
        /// </summary>
        /// <remarks>This will likely be changed in the future to get the group from a cache.</remarks>
        internal PlanogramMerchandisingGroup GetMerchandisingGroup(PlanogramSubComponentPlacement placement)
        {
            if (placement.Planogram != this.Model) return null;
            if (placement.SubComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.None) return null;

            //check if the placement exists in one of the pending groups.
            Object id = placement.Id;

            PlanogramMerchandisingGroup group =
                _pendingMerchGroups.FirstOrDefault(g => g.SubComponentPlacements.Any(s => Object.Equals(s.Id, id)));

            if (group == null)
            {
                //create the group if required.
                group = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(placement.GetCombinedWithList());
                _pendingMerchGroups.Add(group);
            }
            return group;
        }

        /// <summary>
        /// Returns a list of subcomponent groups for the entire plan.
        /// </summary>
        private List<SubComponentGroup> GetSubComponentGroups()
        {
            List<SubComponentGroup> groups = new List<SubComponentGroup>();

            //get a list of all subs to be processed and all that may be combined.
            List<PlanogramSubComponentView> remainingSubs = EnumerateMerchandisableSubComponents().ToList();
            List<PlanogramSubComponentPlacement> allCombinable = remainingSubs.Select(s => s.ModelPlacement).ToList();

            while (remainingSubs.Count > 0)
            {
                PlanogramSubComponentView target = remainingSubs.First();
                List<PlanogramSubComponentPlacement> combinedWithPlacements = target.ModelPlacement.GetCombinedWithList();

                List<PlanogramSubComponentView> combinedWith;
                if (combinedWithPlacements.Count() == 1)
                {
                    combinedWith = new List<PlanogramSubComponentView> { target };
                }
                else
                {
                    combinedWith =
                        remainingSubs.Where(s => combinedWithPlacements.Contains(s.ModelPlacement))
                        .OrderBy(s => combinedWithPlacements.IndexOf(s.ModelPlacement))
                        .ToList();
                }


                //only add if we actually have positions in the group.
                if (combinedWith.SelectMany(p => p.ModelPlacement.GetPlanogramPositions()).Any())
                {
                    groups.Add(new SubComponentGroup(combinedWith));
                }

                combinedWith.ForEach(s => remainingSubs.Remove(s));
            }

            return groups;
        }

        /// <summary>
        /// Forces an update of all planogram size related calculations.
        /// </summary>
        public void UpdatePlanogramBounds()
        {
            Boolean isRecordingSuppressed = _suppressChangeRecording;

            _suppressChangeRecording = true;

            // Components
            foreach (PlanogramComponent component in this.Model.Components)
            {
                component.UpdateSizeFromSubComponents();
            }

            // Assemblies
            foreach (PlanogramAssembly assembly in this.Model.Assemblies)
            {
                assembly.UpdateSizeFromComponents();
            }

            // Fixtures - Don't update as this now gets set by the user.

            // Planogram
            this.Model.UpdateSizeFromFixtures();


            _suppressChangeRecording = isRecordingSuppressed;

            _isSizeOutdated = false;
            OnBoundsChanged();
        }

        /// <summary>
        /// Processes the given highlight against this plan.
        /// </summary>
        public PlanogramHighlightResult ProcessHighlight(HighlightItem highlight)
        {
            return PlanogramHighlightHelper.ProcessPositionHighlight(this.Model, highlight);
        }

        #region Flag Methods

        /// <summary>
        /// Dictionary of models that have been flagged as having changed since the last plan process.
        /// </summary>
        private Dictionary<String, List<IModelObject>> _flaggedModels = new Dictionary<String, List<IModelObject>>();

        /// <summary>
        /// Adds the given model to the flagged models list
        /// </summary>
        /// <typeparam name="PMODEL"></typeparam>
        /// <param name="model"></param>
        private void Flag<PMODEL>(PMODEL model)
            where PMODEL : Galleria.Framework.Planograms.Model.ModelObject<PMODEL>
        {
            String modelTypeName = typeof(PMODEL).Name;

            List<IModelObject> modelList;

            //add the list if it does not exist
            if (!_flaggedModels.TryGetValue(modelTypeName, out modelList))
            {
                modelList = new List<IModelObject>();
                _flaggedModels[modelTypeName] = modelList;
            }

            //add the model if required.
            if (!modelList.Contains(model))
            {
                modelList.Add(model);
            }
        }

        /// <summary>
        /// Adds the given sub group to the flagged models list
        /// </summary>
        /// <param name="subGroup"></param>
        private void Flag(SubComponentGroup subGroup)
        {
            foreach (PlanogramSubComponentPlacement s in subGroup.Subs)
            {
                Flag(s.SubComponent);
            }
        }

        /// <summary>
        /// Adds all subcomponents in this planogram to the flagged models list.
        /// </summary>
        private void FlagAll()
        {
            foreach (var sub in EnumerateAllSubComponents())
            {
                Flag(sub.Model);
            }
        }

        /// <summary>
        /// Returns the list of refs for the given subids.
        /// </summary>
        /// <returns></returns>
        private List<PlanogramSubComponentPlacementId> GetDirtySubComponentPlacementIds(List<SubComponentGroup> subGroups)
        {
            //compare the current groups against the old
            // flag up any where the group content has changed or the merch area bounds have.
            Dictionary<PlanogramSubComponentPlacementId, SubComponentGroup> oldPlacementsToGroups =
                new Dictionary<PlanogramSubComponentPlacementId, SubComponentGroup>();
            foreach (SubComponentGroup subGroup in _prevGroups)
            {
                foreach (PlanogramSubComponentPlacement subPlacement in subGroup.Subs)
                {
                    oldPlacementsToGroups[(PlanogramSubComponentPlacementId)subPlacement.Id] = subGroup;
                }
            }


            foreach (SubComponentGroup subGroup in subGroups)
            {
                foreach (PlanogramSubComponentPlacement subPlacement in subGroup.Subs)
                {
                    PlanogramSubComponentPlacementId placementId = (PlanogramSubComponentPlacementId)subPlacement.Id;

                    SubComponentGroup oldGroup = null;
                    oldPlacementsToGroups.TryGetValue(placementId, out oldGroup);

                    if (oldGroup == null)
                    {
                        //no prev group, so flag up all in the new one.
                        Flag(subGroup);
                    }
                    else if (oldGroup.Subs.Count != subGroup.Subs.Count
                        || !oldGroup.GetPlacementKeys().SequenceEqual(subGroup.GetPlacementKeys()))
                    {
                        //subs have changed - flag up both groups.
                        Flag(subGroup);
                        Flag(oldGroup);
                    }
                    else if (!oldGroup.MerchandisingBounds.SequenceEqual(subGroup.MerchandisingBounds))
                    {
                        //compare the merch bounds of all subs.
                        for (Int32 i = 0; i < subGroup.Subs.Count; i++)
                        {
                            if (subGroup.MerchandisingBounds[i] != oldGroup.MerchandisingBounds[i])
                            {
                                Flag(subGroup.Subs[i].SubComponent);
                            }
                        }

                    }
                }

            }


            List<Object> subIds = new List<object>();

            Planogram planModel = this.Model;
            List<IModelObject> modelList;


            //Products
            if (_flaggedModels.TryGetValue(typeof(PlanogramProduct).Name, out modelList))
            {
                foreach (PlanogramProduct product in modelList.Distinct())
                {
                    //flag all positions for this product
                    foreach (var position in product.GetPlanogramPositions())
                    {
                        subIds.Add(position.PlanogramSubComponentId);
                    }
                }
            }

            //Positions
            if (_flaggedModels.TryGetValue(typeof(PlanogramPosition).Name, out modelList))
            {
                foreach (PlanogramPosition position in modelList.Distinct())
                {
                    subIds.Add(position.PlanogramSubComponentId);
                }
            }

            //Components
            List<Object> componentIds = new List<Object>();
            if (_flaggedModels.TryGetValue(typeof(PlanogramComponent).Name, out modelList))
            {
                componentIds.AddRange(modelList.Distinct().Cast<PlanogramComponent>().Select(c => c.Id));
            }
            if (_flaggedModels.TryGetValue(typeof(PlanogramAssemblyComponent).Name, out modelList))
            {
                componentIds.AddRange(modelList.Distinct().Cast<PlanogramAssemblyComponent>().Select(c => c.PlanogramComponentId));
            }
            if (_flaggedModels.TryGetValue(typeof(PlanogramFixtureComponent).Name, out modelList))
            {
                componentIds.AddRange(modelList.Distinct().Cast<PlanogramFixtureComponent>().Select(c => c.PlanogramComponentId));
            }
            if (_flaggedModels.TryGetValue(typeof(PlanogramFixture).Name, out modelList))
            {
                componentIds.AddRange(modelList.Distinct().Cast<PlanogramFixture>().SelectMany(f => f.Components.Select(c => c.PlanogramComponentId)));
            }
            if (_flaggedModels.TryGetValue(typeof(PlanogramFixtureItem).Name, out modelList))
            {
                foreach (PlanogramFixtureItem fixtureItem in modelList.Distinct())
                {
                    PlanogramFixture fixture = planModel.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
                    if (fixture == null) continue;
                    componentIds.AddRange(fixture.Components.Select(c => c.PlanogramComponentId));
                }
            }


            foreach (Object componentId in componentIds.Distinct())
            {
                PlanogramComponent component = planModel.Components.FindById(componentId);
                if (component == null) continue;
                subIds.AddRange(component.SubComponents.Select(s => s.Id));
            }


            //SubComponents
            if (_flaggedModels.TryGetValue(typeof(PlanogramSubComponent).Name, out modelList))
            {
                subIds.AddRange(modelList.Distinct().Cast<PlanogramSubComponent>().Select(m => m.Id));
            }


            subIds = subIds.Distinct().ToList();

            List<PlanogramSubComponentPlacementId> subPlacementIds = new List<PlanogramSubComponentPlacementId>();
            foreach (PlanogramSubComponentView sub in EnumerateMerchandisableSubComponents())
            {
                if (subIds.Contains(sub.Model.Id))
                {
                    subPlacementIds.Add((PlanogramSubComponentPlacementId)sub.ModelPlacement.Id);
                }
            }
            return subPlacementIds;
        }

        #endregion

        #region Validation


        /// <summary>
        /// This method uses the passed warning parameter's type to check that the 
        /// notifications for this type have been turned on in the settings.
        /// </summary>
        /// <param name="warning"></param>
        /// <returns></returns>
        private bool ShouldShowValidationNotification(ValidationWarningRow warning)
        {
            if (warning.WarningType.Equals(PlanogramValidationWarningType.ComponentHasCollisions)
                && App.ViewState.Settings.Model.ShowInstantComponentHasCollisionsWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.ComponentIsOutsideOfFixtureArea)
                && App.ViewState.Settings.Model.ShowInstantComponentIsOutsideOfFixtureAreaWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.ComponentIsOverfilled)
                && App.ViewState.Settings.Model.ShowInstantComponentIsOverfilledWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.ComponentSlopeWithNoRiser)
                && App.ViewState.Settings.Model.ShowInstantComponentSlopeWithNoRiserWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.PegIsOverfilled)
                && App.ViewState.Settings.Model.ShowInstantPegIsOverfilledWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.PositionCannotBreakTrayBack)
                && App.ViewState.Settings.Model.ShowInstantPositionCannotBreakTrayBackWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.PositionCannotBreakTrayDown)
                && App.ViewState.Settings.Model.ShowInstantPositionCannotBreakTrayDownWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.PositionCannotBreakTrayTop)
                && App.ViewState.Settings.Model.ShowInstantPositionCannotBreakTrayTopWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.PositionCannotBreakTrayUp)
                && App.ViewState.Settings.Model.ShowInstantPositionCannotBreakTrayUpWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.PositionDoesNotAchieveMinDeep)
                && App.ViewState.Settings.Model.ShowInstantPositionDoesNotAchieveMinDeepWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.PositionExceedsMaxDeep)
                && App.ViewState.Settings.Model.ShowInstantPositionExceedsMaxDeepWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.PositionExceedsMaxRightCap)
                && App.ViewState.Settings.Model.ShowInstantPositionExceedsMaxRightCapWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.PositionExceedsMaxStack)
                && App.ViewState.Settings.Model.ShowInstantPositionExceedsMaxStackWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.PositionExceedsMaxTopCap)
                && App.ViewState.Settings.Model.ShowInstantPositionExceedsMaxTopCapWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.PositionHasCollisions)
                && App.ViewState.Settings.Model.ShowInstantPositionHasCollisionsWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.PositionHasInvalidMerchStyleSettings)
                && App.ViewState.Settings.Model.ShowInstantPositionHasInvalidMerchStyleSettingsWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.PositionIsHangingTray)
                && App.ViewState.Settings.Model.ShowInstantPositionIsHangingTrayWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.PositionIsHangingWithoutPeg)
                && App.ViewState.Settings.Model.ShowInstantPositionIsHangingWithoutPegWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.PositionIsOutsideMerchandisingSpace)
                && App.ViewState.Settings.Model.ShowInstantPositionIsOutsideMerchandisingSpaceWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.ProductBreaksDistributionRule)
                && App.ViewState.Settings.Model.ShowInstantAssortmentDistributionRulesWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.ProductBreaksInheritanceRule)
                && App.ViewState.Settings.Model.ShowInstantAssortmentInheritanceRulesWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.ProductBreaksLocalLineRule)
                && App.ViewState.Settings.Model.ShowInstantAssortmentLocalLineRulesWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.ProductBreaksLocationProductIllegalRule)
                && App.ViewState.Settings.Model.ShowInstantAssortmentProductRulesWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.ProductBreaksCoreRule)
                && App.ViewState.Settings.Model.ShowInstantAssortmentCoreRulesWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.ProductBreaksLocationProductIllegalRule)
                && App.ViewState.Settings.Model.ShowInstantLocationProductIllegalWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.ProductBreaksLocationProductLegalRule)
                && App.ViewState.Settings.Model.ShowInstantLocationProductLegalWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.ProductHasDuplicateGtin)
                && App.ViewState.Settings.Model.ShowInstantProductHasDuplicateGtinWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.ProductDoesNotAchieveMinCases)
                && App.ViewState.Settings.Model.ShowInstantProductDoesNotAchieveMinCasesWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.ProductDoesNotAchieveMinDeliveries)
                && App.ViewState.Settings.Model.ShowInstantProductDoesNotAchieveMinDeliveriesWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.ProductDoesNotAchieveMinDos)
                && App.ViewState.Settings.Model.ShowInstantProductDoesNotAchieveMinDosWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.ProductDoesNotAchieveMinShelfLife)
                && App.ViewState.Settings.Model.ShowInstantProductDoesNotAchieveMinShelfLifeWarnings) return true;

            else if ((warning.WarningType.Equals(PlanogramValidationWarningType.ProductBreaksProductRule)
                || warning.WarningType.Equals(PlanogramValidationWarningType.ProductBreaksDelistProductRule)
                || warning.WarningType.Equals(PlanogramValidationWarningType.ProductBreaksForceProductRuleBroken)
                || warning.WarningType.Equals(PlanogramValidationWarningType.ProductBreaksPreserveProductRuleBroken)
                || warning.WarningType.Equals(PlanogramValidationWarningType.ProductBreaksMinimumHurdleProductRule)
                ) && App.ViewState.Settings.Model.ShowInstantAssortmentProductRulesWarnings) return true;

            else if ((warning.WarningType.Equals(PlanogramValidationWarningType.ProductBreaksFamilyRule)
                || warning.WarningType.Equals(PlanogramValidationWarningType.ProductBreaksDelistFamilyRule)
                || warning.WarningType.Equals(PlanogramValidationWarningType.ProductBreaksMaximumProductFamilyRule)
                || warning.WarningType.Equals(PlanogramValidationWarningType.ProductBreaksMinimumHurdleProductRule)
                || warning.WarningType.Equals(PlanogramValidationWarningType.ProductBreaksDependencyFamilyRule)
                ) && App.ViewState.Settings.Model.ShowInstantAssortmentFamilyRulesWarnings) return true;

            else if (warning.WarningType.Equals(PlanogramValidationWarningType.NotSet)) return true;


            else if (warning.WarningType.Equals(PlanogramValidationWarningType.PositionPegHolesAreInvalidRule)
                && App.ViewState.Settings.Model.ShowInstantPositionPegHolesAreInvalidWarnings) return true;

            else return false;
        }

        /// <summary>
        ///     Checks all validation warnings in the planogram. Optionally resets the list of warnings, instead of just updating it.
        /// </summary>
        /// <param name="resetList">Whether to clear the list of warnings or just update it.</param>
        public void CheckValidationWarnings(Boolean resetList = false)
        {
            if (resetList) _validationWarningRows.Clear();
            UpdateValidationWarnings(this.Model.GetAllBrokenValidationWarnings());
        }

        Boolean _firstPass = true;
        List<ValidationWarningRow> _oldCorrectedRows = new List<ValidationWarningRow>();
        private void UpdateValidationWarnings(List<PlanogramValidationWarning> planWarnings)
        {
            //Get the list of warning rows for the planogram.
            var warnings = new List<ValidationWarningRow>(planWarnings.Select(ValidationWarningRow.NewValidationWarningRow));

            // Update existing warnings status.
            foreach (var row in _validationWarningRows)
            {
                var isCorrected = warnings.All(warningRow => warningRow != row);
                row.IsCorrected = isCorrected;
            }

            //First Remove warnings for items which no longer exist.
            foreach (ValidationWarningRow warning in _validationWarningRows.ToArray())
            {
                if (
                    (warning.FixtureComponent != null
                        && !EnumerateAllFixtureComponents().Any(f => Object.Equals(f.FixtureComponentModel.Id, warning.FixtureComponent.Id)))
                    ||
                    (warning.FixtureAssembly != null
                        && !EnumerateAllAssemblies().Any(a => Object.Equals(a.FixtureAssemblyModel.Id, warning.FixtureAssembly.Id)))
                    ||
                    (warning.AssemblyComponent != null
                        && !EnumerateAllAssemblyComponents().Any(a => Object.Equals(a.AssemblyComponentModel.Id, warning.AssemblyComponent.Id)))
                    ||
                    (warning.Position != null
                        && !EnumerateAllPositions().Any(a => Object.Equals(a.Model.Id, warning.Position.Id)))
                    ||
                    (warning.Product != null
                        && !Products.Any(a => Object.Equals(a.Model.Id, warning.Product.Id)))
                    )
                {
                    _validationWarningRows.Remove(warning);
                }
            }


            // Add new validation warnings.
            IEnumerable<ValidationWarningRow> newWarningRows = warnings.Where(row => _validationWarningRows.All(warningRow => warningRow != row)).ToList();
            if (newWarningRows.Any()) _validationWarningRows.AddRange(newWarningRows);


            //V8-29439: Calculate real time validation notification. New validation warnings:
            List<ValidationWarningRow> _notifierRows = new List<ValidationWarningRow>();
            foreach (var newWarning in newWarningRows)
            {
                if (ShouldShowValidationNotification(newWarning))
                {
                    _notifierRows.Add(newWarning);
                }
                else continue;
            }

            //Previously corrected warnings.
            List<ValidationWarningRow> reBrokenValidation = warnings.Where(w => w.IsCorrected == false && _oldCorrectedRows.Contains(w)).ToList();

            foreach (var reBrokenWarning in reBrokenValidation)
            {
                if (ShouldShowValidationNotification(reBrokenWarning))
                {
                    _notifierRows.Add(reBrokenWarning);
                }
            }

            if (_notifierRows.Any(nr => nr.ShowInstantNotificationWarningToUser) && !_firstPass) new PlanogramValidationNotifierWindowViewModel(_notifierRows).ShowDialog();

            //Update old corrected rows for next pass.
            _oldCorrectedRows.Clear();
            _oldCorrectedRows.AddRange(_validationWarningRows.Where(vwr => vwr.IsCorrected == true));

            OnValidationWarningsChanged();

            _firstPass = false;
        }

        #endregion

        public override void EndUpdate()
        {
            base.EndUpdate();

            //if the plan changed during the update but has not processed 
            // yet then make sure that it does so now.
            if (_isPlanChanged)
            {
                QueueProcessPlanChanges();
            }
        }

        #endregion

        #region Fixture Child Methods

        /// <summary>
        /// Creates a new view of the given fixture
        /// </summary>
        /// <param name="fixture"></param>
        /// <returns></returns>
        protected override PlanogramFixtureViewModelBase CreateFixtureView(PlanogramFixtureItem fixtureItem)
        {
            PlanogramFixture fixture = fixtureItem.GetPlanogramFixture();
            if (fixture == null) return null;
            return new PlanogramFixtureView(this, fixtureItem, fixture);
        }

        /// <summary>
        /// Adds a new fixture to this planogram
        /// </summary>
        public PlanogramFixtureView AddFixture()
        {
            return AddFixture(this.Model.Fixtures.Add(
                String.Format(CultureInfo.CurrentCulture, Message.NewPlanogramFixtureName, Model.Fixtures.Count),
                App.ViewState.Settings.Model));
        }

        /// <summary>
        /// Adds the new fixture model to this planogram
        /// </summary>
        /// <param name="fixture"></param>
        /// <returns></returns>
        private PlanogramFixtureView AddFixture(PlanogramFixture fixture)
        {
            PlanogramFixtureItem fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem();
            fixtureItem.PlanogramFixtureId = fixture.Id;
            Model.FixtureItems.Add(fixtureItem);

            fixtureItem.BaySequenceNumber = (Int16)Fixtures.Count;

            return Fixtures.LastOrDefault();
        }

        /// <summary>
        /// Adds a copy of the given fixture to this planogram
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        public PlanogramFixtureView AddFixtureCopy(PlanogramFixtureView src,
            Boolean copyComponents = true, Boolean copyPositions = true, Boolean renumberBays = true)
        {
            //create shallow copies of the models
            PlanogramFixture modelCopy = src.FixtureModel.Copy();
            modelCopy.Assemblies.Clear();
            modelCopy.Components.Clear();
            modelCopy.Name = String.Format(CultureInfo.CurrentCulture, Message.NewPlanogramFixtureName, Model.Fixtures.Count);
            Model.Fixtures.Add(modelCopy);

            PlanogramFixtureItem modelItemCopy = src.FixtureItemModel.Copy();
            modelItemCopy.PlanogramFixtureId = modelCopy.Id;
            Model.FixtureItems.Add(modelItemCopy);
            //relocated to after the add so the list is updated correctly before counting
            modelItemCopy.BaySequenceNumber = (Int16)Fixtures.Count;

            PlanogramFixtureView view = Fixtures.LastOrDefault();
            view.IsSelectable = src.IsSelectable;

            if (copyComponents)
            {
                foreach (var assemblySrc in src.Assemblies)
                {
                    view.AddAssemblyCopy(assemblySrc, copyPositions);
                }

                foreach (var componentSrc in src.Components)
                {
                    view.AddComponentCopy(componentSrc, copyPositions);
                }

                foreach (var annotationSrc in src.Annotations)
                {
                    view.AddAnnotationCopy(annotationSrc);
                }
            }

            if (renumberBays)
            {
                RenumberingStrategy.RenumberBays();
            }

            return view;
        }

        /// <summary>
        /// Removes the given fixture from this planogram
        /// </summary>
        /// <param name="fixtureView"></param>
        /// <param name="isUndoAction">If the action is an undo we cannot rely on the x-position of the fixture being correct as it will already have been reverted to 0 by a previous undo</param>
        public void RemoveFixture(PlanogramFixtureView fixtureView, Boolean isUndoAction = false)
        {
            Debug.Assert(this.Fixtures.Contains(fixtureView), "Deleting fixture from wrong plan");
            if (!this.Fixtures.Contains(fixtureView)) return;

            List<PlanogramPosition> positions =
            fixtureView.FixtureItemModel.GetPlanogramSubComponentPlacements()
            .SelectMany(s => s.GetPlanogramPositions()).Distinct().ToList();

            //Take ref of fixture so we can get position
            PlanogramFixtureItem fixtureItemForRemoval = fixtureView.FixtureItemModel;

            //Remove fixture
            using (PlanogramUndoableAction undoAction = NewUndoableAction(supressPlanUpdates: false))
            {
                PlanogramFixture fixtureForRemoval = fixtureItemForRemoval.GetPlanogramFixture();

                this.Model.Fixtures.RemoveFixtureAndDependants(fixtureView.FixtureModel);
                //Model.FixtureItems.Remove(fixtureView.FixtureItemModel);

                //Update other fixture positions
                //NOTE: If this method is called by a parent Undo action, the fixture to be removed 
                // may have already had its BaySequence Number, X-position etc reset by previous
                // undo Property Set actions in the undo sequence so we cannot rely on the x-position 
                // to determine if any other fixtures need to have their x-poisitions reset. This will 
                //be covered by other Property Undo actions though.
                if (!isUndoAction)
                {
                    this.UpdateFixtureItemPositionAfterRemoval(fixtureItemForRemoval, fixtureForRemoval);
                }

                //remove linked positions
                //foreach (PlanogramPosition position in positions)
                //{
                //    this.Model.Positions.Remove(position);
                //}
            }
        }

        /// <summary>
        /// Updates other fixtures x position in relation to the one removed
        /// </summary>
        /// <param name="fixtureItemForRemoval"></param>
        private void UpdateFixtureItemPositionAfterRemoval(PlanogramFixtureItem fixtureItemForRemoval, PlanogramFixture fixtureForRemoval)
        {
            //Get full fixture
            //PlanogramFixture fixtureForRemoval = fixtureItemForRemoval.GetPlanogramFixture();
            //Debug.Assert((fixtureForRemoval != null), "Fixture should have been found");

            if (Model.FixtureItems.Count > 0)
            {
                //Alter fixtures further down x position
                IEnumerable<PlanogramFixtureItem> fixturesToReposition = Model.FixtureItems.Where(p => p.X > fixtureItemForRemoval.X);

                //Subtract removed fixtures width from remaining fixtures x positions
                foreach (PlanogramFixtureItem fixtureItem in fixturesToReposition)
                {
                    fixtureItem.X -= fixtureForRemoval.Width;
                }
            }
        }

        /// <summary>
        /// Adds a new fixture to the planogram.
        /// </summary>
        /// <param name="fixtureToCopy"></param>
        /// <param name="insertBefore"></param>
        /// <param name="copyAllComponents"></param>
        /// <returns></returns>
        public PlanogramFixtureView AddFixtureFromCopy(PlanogramFixtureView fixtureToCopy,
            Boolean insertBefore, Boolean copyAllComponents, Boolean copyAllPositions)
        {
            PlanogramFixtureView newFixture = null;


            if (fixtureToCopy != null)
            {
                newFixture = AddFixtureCopy(fixtureToCopy, copyAllComponents, copyAllPositions);
                if (!copyAllComponents)
                {
                    //copy in the backboard and base individually.
                    PlanogramComponentView backboardToCopy = fixtureToCopy.EnumerateAllComponents().FirstOrDefault(f => f.ComponentType == PlanogramComponentType.Backboard);
                    if (backboardToCopy != null)
                    {
                        newFixture.AddComponentCopy(backboardToCopy, copyAllPositions);
                    }

                    PlanogramComponentView baseComponentToCopy = fixtureToCopy.EnumerateAllComponents().FirstOrDefault(f => f.ComponentType == PlanogramComponentType.Base);
                    if (baseComponentToCopy != null)
                    {
                        newFixture.AddComponentCopy(baseComponentToCopy, copyAllPositions);
                    }
                }

                //adjust fixture x position.
                if (insertBefore)
                {
                    // insert before
                    newFixture.X = fixtureToCopy.X - newFixture.Width;

                    // left shift any fixtures before the new one
                    foreach (PlanogramFixtureView f in this.Fixtures)
                    {
                        if (f != newFixture)
                        {
                            if (f.X < (newFixture.X + newFixture.Width))
                            {
                                f.X -= newFixture.Width;
                            }
                        }
                    }

                    // reset min x to 0
                    Single minX = this.Fixtures.Min(p => p.X);
                    if (minX < 0)
                    {
                        foreach (PlanogramFixtureView f in this.Fixtures)
                        {
                            f.X -= minX;
                        }
                    }

                    // resequence bays
                    RenumberingStrategy.RenumberBays();
                }
                else
                {
                    // insert after
                    newFixture.X = fixtureToCopy.X + fixtureToCopy.Width;

                    // bump along any other fixtures after the new one
                    foreach (PlanogramFixtureView f in this.Fixtures)
                    {
                        if (f != newFixture)
                        {
                            if (f.X >= newFixture.X)
                            {
                                f.X += newFixture.Width;
                            }
                        }
                    }

                    // resequence bays
                    RenumberingStrategy.RenumberBays();

                }
            }
            else
            {
                newFixture = AddFixture();
            }


            newFixture.Name = String.Format(CultureInfo.CurrentCulture,
                Message.NewPlanogramFixtureName, this.Fixtures.Count);


            //force the plan cache to clear before we return 
            ((IModelReadOnlyObject)this.Model).ClearCache();

            return newFixture;
        }

        /// <summary>
        ///     Get the <c>Fixture View</c> in this planogram that matches the one in the <paramref name="planItem"/> OR the first one if none match.
        /// </summary>
        /// <param name="planItem">The instance of <see cref="IPlanItem"/> for which to find the target fixture view.</param>
        /// <param name="matchingFixtureView"></param>
        /// <returns>The instance of <see cref="PlanogramFixtureView"/> that should be used as target when pasting the <paramref name="planItem"/>.</returns>
        /// <remarks>If the target planogram does not contain any Fixtures, this method will return <c>null</c>.</remarks>
        private Boolean TryFindMatchingFixtureView(IPlanItem planItem, out PlanogramFixtureView matchingFixtureView)
        {
            //  Start with the source's original fixture.
            matchingFixtureView = planItem.Fixture;

            //  If that fixture does not exist in this planogram, get the first available fixture.
            if (!Fixtures.Contains(matchingFixtureView)) matchingFixtureView = Fixtures.FirstOrDefault();

            //  Return the match.
            return matchingFixtureView != null;
        }

        #endregion

        #region Product Child Methods

        /// <summary>
        /// Updates the products in this model with the attributes from those provided.
        /// </summary>
        /// <param name="productLibraryProducts">The product library products to use when updating product attributes.</param>
        /// <returns>The number of products that were updated.</returns>
        public Int32 UpdateProducts(IEnumerable<ProductLibraryProduct> productLibraryProducts, BulkObservableCollection<ProductAttributeSelectorRow> selectorRows)
        {
            //Keep count.
            Int32 updatedProducts = 0;

            //List of attributes we are going to update
            IEnumerable<String> attributeNames = selectorRows.Select(a => a.PropertyName);

            // First, build a library of the plan's products so that we can retrieve them by GTIN.
            Dictionary<String, PlanogramProduct> productsByGtin = this.Model.Products.ToDictionary(p => p.Gtin, p => p);

            // Ensure that these changes occur in one undoable action and that plan changes aren't processed until we're finished.
            using (PlanogramUndoableAction undoAction = NewUndoableAction(supressPlanUpdates: true))
            {
                // before updating any products on the plan we need to update all products on the plan that do not exist in the product library with default values.

                // aquire gtin,product dictonary for all products not in universe
                Dictionary<String, PlanogramProduct> productsByGtinForNotInUniverse = new Dictionary<string, PlanogramProduct>(productsByGtin);
                // removes universe products from our collection 
                foreach (ProductLibraryProduct productLibraryProduct in productLibraryProducts)
                {
                    foreach (ProductLibraryProduct productInLibrary in productLibraryProducts)
                    {
                        productsByGtinForNotInUniverse.Remove(productInLibrary.Gtin);
                    }
                }

                // filter the selectors to those that specify to actually set a default value
                List<ProductAttributeSelectorRow> selectorsToApplyDefaultFrom = selectorRows.Where(s => s.IsDefaultValueApplied).ToList();
                PlanogramProduct productWithDefaultValues = PlanogramProduct.NewPlanogramProduct();

                // productNotInUniverse needs updating based on foreach selector row.
                foreach (ProductAttributeSelectorRow currentSelector in selectorsToApplyDefaultFrom)
                {
                    // set productWithDefaultValues based on currentSelector.PropertyName
                    Type type = productWithDefaultValues.GetType();
                    PropertyInfo prop = type.GetProperty(currentSelector.PropertyName);
                    prop.SetValue(productWithDefaultValues, currentSelector.DefaultValue, null);
                }


                //Update with default values.
                foreach (PlanogramProduct productNotInUniverse in productsByGtinForNotInUniverse.Values)
                {
                    PlanogramProduct nonUpdatedProduct = productNotInUniverse.Copy();
                    nonUpdatedProduct.Id = productNotInUniverse.Id;
                    nonUpdatedProduct.CustomAttributes.Id = productNotInUniverse.CustomAttributes.Id;

                    selectorsToApplyDefaultFrom.Select(s => s.PropertyName);

                    foreach (var selector in selectorsToApplyDefaultFrom)
                    {
                        productNotInUniverse.UpdateFrom(productWithDefaultValues, selectorsToApplyDefaultFrom.Select(s => s.PropertyName).ToArray());
                    }

                    if (!productNotInUniverse.Compare(nonUpdatedProduct))
                    {
                        //If something changed then update count.
                        updatedProducts++;
                    }
                }

                // Now, iterate over the products we've been given and try to update their
                // matching products in the planogram.
                foreach (ProductLibraryProduct productLibraryProduct in productLibraryProducts)
                {
                    //Get the product if exists on the plan and update it. If doesn't exist on the plan we can't update using it.
                    PlanogramProduct product;
                    if (!productsByGtin.TryGetValue(productLibraryProduct.Gtin, out product)) continue;

                    PlanogramProduct nonUpdatedProduct = product.Copy();
                    nonUpdatedProduct.Id = product.Id;
                    nonUpdatedProduct.CustomAttributes.Id = product.CustomAttributes.Id;

                    product.UpdateFrom(productLibraryProduct, attributeNames.ToArray());

                    if (!product.Compare(nonUpdatedProduct))
                    {
                        updatedProducts++;
                    }
                }
            }

            return updatedProducts;
        }

        /// <summary>
        /// Creates a new view of the given planogram product model.
        /// </summary>
        protected override PlanogramProductViewModelBase CreateProductView(PlanogramProduct product)
        {
            return new PlanogramProductView(this, product);
        }

        /// <summary>
        /// Adds a new product to this planogram
        /// </summary>
        /// <returns></returns>
        public PlanogramProductView AddProduct()
        {
            PlanogramProduct product = PlanogramProduct.NewPlanogramProduct();

            //apply defaults
            var settings = App.ViewState.Settings.Model;
            product.Height = settings.ProductHeight;
            product.Width = settings.ProductWidth;
            product.Depth = settings.ProductDepth;

            Model.Products.Add(product);
            return Products.Last();
        }

        /// <summary>
        /// Removes the given product from this planogram.
        /// </summary>
        /// <param name="prodView"></param>
        public void RemoveProduct(PlanogramProductView prodView)
        {
            Debug.Assert(this.Products.Contains(prodView), "Deleting product from wrong plan");
            if (!this.Products.Contains(prodView)) return;

            Object productId = prodView.Model.Id;
            List<PlanogramPosition> positions =
                this.Model.Positions.Where(p => Object.Equals(productId, p.PlanogramProductId))
                .ToList();

            Model.Products.Remove(prodView.Model);

            //delete all related positions
            this.Model.Positions.RemoveList(positions);
        }

        /// <summary>
        /// Returns the view of the planogramproduct with the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PlanogramProductView FindProduct(Object id)
        {
            return this.Products.FirstOrDefault(p => Equals(p.Model.Id, id));
        }

        /// <summary>
        /// Checks the plan for an existing product with the same gtin and either returns that or adds a new copy.
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public PlanogramProductView AddProduct(IPlanogramProduct product)
        {
            //  Check whether the product actually exists in the planogram before adding the position.
            PlanogramProductView productMatch = this.Products.FirstOrDefault(view => Equals(view.Gtin, product.Gtin));
            if (productMatch != null) return productMatch;

            return AddProducts(new List<IPlanogramProduct> { product }).FirstOrDefault();
        }

        /// <summary>
        /// Adds the given master products to this plan.
        /// </summary>
        /// <param name="products"></param>
        /// <returns></returns>
        public List<PlanogramProductView> AddProducts(IEnumerable<IPlanogramProduct> products)
        {
            //Create the plan products
            List<PlanogramProductView> planProducts = new List<PlanogramProductView>();

            //temp fix to stop slowdown while the model adds perf data and checks for uniqueness.
            Boolean oldRaise = this.Model.Performance.PerformanceData.RaiseListChangedEvents;
            this.Model.Performance.PerformanceData.RaiseListChangedEvents = false;

            Dictionary<String, PlanogramProductView> prodDict = this.Products.ToDictionary(p => p.Gtin, StringComparer.OrdinalIgnoreCase);
            foreach (IPlanogramProduct masterProduct in products)
            {
                PlanogramProductView planProduct;

                if (!prodDict.TryGetValue(masterProduct.Gtin, out planProduct))
                {
                    PlanogramProduct planogramProduct = PlanogramProduct.NewPlanogramProduct(masterProduct);
                    Model.Products.Add(planogramProduct);

                    //if this is a planogram product then we should also copy performance data values
                    PlanogramProduct masterPlanProduct = masterProduct as PlanogramProduct;
                    if (masterPlanProduct != null)
                    {
                        PlanogramPerformanceData srcData = masterPlanProduct.GetPlanogramPerformanceData();
                        PlanogramPerformanceData targetData = planogramProduct.GetPlanogramPerformanceData();
                        if (srcData != null && targetData != null)
                        {
                            for (Byte i = 1; i <= 20; i++)
                            {
                                targetData.SetPerformanceData(i, srcData.GetPerformanceData(i));
                            }
                        }
                    }
                    planProduct = this.Products.Last(p => String.CompareOrdinal(p.Gtin, masterProduct.Gtin) == 0);
                }

                planProducts.Add(planProduct);
            }

            this.Model.Performance.PerformanceData.RaiseListChangedEvents = oldRaise;

            return planProducts;
        }


        /// <summary>
        /// Returns a count of how many positions exist for the given product
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public Int32 GetPositionCount(PlanogramProductView product)
        {
            Object productId = product.Model.Id;

            return Model.Positions.Count(p => Equals(p.PlanogramProductId, productId));
        }

        /// <summary>
        /// Handles the filling of an axis within a merchgroup on only the selected positions
        /// </summary>
        /// <param name="axisToFill">The axis we would like to fill the selected positions on.</param>
        /// <remarks>Shrinks the positions down to mimium before filling wide - does not increment from start facings.</remarks>
        public void FillPositionsOnGivenAxis(AxisType axisToFill, IEnumerable<IPlanItem> planItems)
        {
            //start an undoable action
            Boolean newUndoableAction = (!this.IsRecordingUndoableAction);
            if (newUndoableAction) BeginUndoableAction();


            Dictionary<Object, List<PlanogramPositionView>> subPlacementIdToPositionView =
               planItems.Where(spi => spi.PlanItemType == PlanItemType.Position)
                .GroupBy(pi => pi.SubComponent.ModelPlacement)
                .ToLookupDictionary(g => g.Key.Id, g => g.Select(p => p.Position).ToList());


            using (PlanogramMerchandisingGroupList merchGroupList = this.Model.GetMerchandisingGroups())
            {


                List<PlanogramMerchandisingGroup> changedGroups = new List<PlanogramMerchandisingGroup>();

                foreach (PlanogramMerchandisingGroup merchGroup in merchGroupList)
                {
                    PlanogramMerchandisingSpace merchSpace = merchGroup.GetMerchandisingSpace();


                    //Create a list of all external boundaries of other subcomponents and positions on the plan
                    // that could interfere with the layout of this group.
                    List<BoundaryInfo> externalObstructions = merchGroup.GetExternalObstructions(merchSpace, merchGroupList);

                    //find any position placements for the selected items.
                    List<PlanogramPositionPlacement> positionPlacementList = new List<PlanogramPositionPlacement>();
                    foreach (PlanogramSubComponentPlacement subPlacement in merchGroup.SubComponentPlacements)
                    {
                        if (subPlacementIdToPositionView.ContainsKey(subPlacement.Id))
                        {
                            foreach (PlanogramPositionView p in subPlacementIdToPositionView[subPlacement.Id])
                            {
                                PlanogramPositionPlacement pPlacement = merchGroup.FindPlacementByPositionId(p.Model.Id);
                                if (pPlacement != null) positionPlacementList.Add(pPlacement);
                            }
                        }
                    }
                    if (positionPlacementList.Count == 0) continue;

                    //keep a not that the group has changed.
                    changedGroups.Add(merchGroup);




                    // Don't forget to order the list..
                    positionPlacementList = merchGroup.OrderPositionPlacementsByRankThenGtin(merchGroup.PositionPlacements,
                        this.Model.Assortment, positionPlacementList);


                    //Based on method argument fill the specif axis appropriatley.
                    switch (axisToFill)
                    {
                        case AxisType.X:
                            // Before we try to fill the products wide we shrink the products down to their minium facing size but only on sepecifed axis.
                            // Then Process autofill along the X Axis and ensure that they are definitley have capping applied where possible.
                            foreach (var pp in positionPlacementList)
                            {
                                pp.SetMinimumUnits(true, false, false, true);
                            }
                            merchGroup.ProcessAutofill(axisToFill, positionPlacementList, externalObstructions, merchSpace);
                            break;

                        case AxisType.Y:
                            // Before we try to fill the products wide we shrink the products down to their minium facing size but only on sepecifed axis.
                            // Then Process autofill along the X Axis and ensure that they are definitley have capping applied where possible.
                            foreach (var pp in positionPlacementList)
                            {
                                pp.SetMinimumUnits(false, true, false, true);
                            }
                            merchGroup.ProcessAutofill(axisToFill, positionPlacementList, externalObstructions, merchSpace);
                            break;

                        case AxisType.Z:
                            // Before we try to fill the products wide we shrink the products down to their minium facing size but only on sepecifed axis.
                            // Then Process autofill along the X Axis and ensure that they are definitley have capping applied where possible.
                            foreach (var pp in positionPlacementList)
                            {
                                pp.SetMinimumUnits(false, false, true, true);
                            }
                            merchGroup.ProcessAutofill(axisToFill, positionPlacementList, externalObstructions, merchSpace);
                            break;

                        default:
                            Debug.Fail("The axis to be filled is unknown.");
                            break;
                    }

                    //planoMerchGroup.ApplyEdit();
                    positionPlacementList.Clear();
                }



                //notify the planogram that groups have changed so it can reprocess
                NotifyMerchandisingGroupChanged(changedGroups);
            }

            //end the undoable action
            if (newUndoableAction) EndUndoableAction();
        }

        #endregion

        #region General Child Methods

        /// <summary>
        /// Sets the initial selection state of all child items.
        /// </summary>
        private void InitialiseSelectableChildren()
        {
            //initialize all components, assemblies, annotations and positions as selectable
            foreach (var f in this.Fixtures)
            {
                Boolean initialBackboardFound = false;

                foreach (var a in f.Assemblies)
                {
                    if (a.Components.Count == 1)
                    {
                        a.Components[0].IsSelectable = true;
                    }
                    else
                    {
                        a.IsSelectable = true;
                    }
                }

                foreach (var c in f.Components)
                {
                    if (!initialBackboardFound
                        && c.ComponentType == PlanogramComponentType.Backboard)
                    {
                        initialBackboardFound = true;
                    }
                    else if (c.ComponentType != PlanogramComponentType.Base)
                    {
                        c.IsSelectable = true;
                    }
                }
            }

            foreach (var c in EnumerateAllAnnotations())
            {
                c.IsSelectable = true;
            }
            foreach (var positionView in EnumerateAllPositions())
            {
                positionView.IsSelectable = true;
            }
        }

        /// <summary>
        /// Initializes the selection state of the given plan item.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="isRecursive"></param>
        public static void InitialiseSelectionState(IPlanItem item, Boolean isRecursive = true)
        {
            if (item == null) { return; }

            switch (item.PlanItemType)
            {
                case PlanItemType.Fixture:
                    item.IsSelectable = false;

                    if (isRecursive)
                    {
                        foreach (PlanogramAssemblyView assembly in item.Fixture.Assemblies)
                        {
                            InitialiseSelectionState(assembly, isRecursive);
                        }
                        foreach (PlanogramComponentView component in item.Fixture.Components)
                        {
                            InitialiseSelectionState(component, isRecursive);
                        }
                        foreach (PlanogramAnnotationView anno in item.Fixture.Annotations)
                        {
                            InitialiseSelectionState(anno, isRecursive);
                        }
                    }
                    break;

                case PlanItemType.Assembly:
                    item.IsSelectable = true;

                    if (isRecursive)
                    {
                        foreach (PlanogramComponentView component in item.Assembly.Components)
                        {
                            InitialiseSelectionState(component, isRecursive);
                        }
                    }
                    break;

                case PlanItemType.Component:
                    if (item.Assembly == null)
                    {
                        if (item.Component.ComponentType == PlanogramComponentType.Base)
                        {
                            item.IsSelectable = false;
                        }
                        else if (item.Component.ComponentType == PlanogramComponentType.Backboard)
                        {
                            item.IsSelectable =
                                item.Component != item.Fixture.Components.FirstOrDefault(c => c.ComponentType == PlanogramComponentType.Backboard);
                        }
                        else
                        {
                            item.IsSelectable = true;
                        }
                    }
                    else
                    {
                        item.IsSelectable = false;
                    }

                    if (isRecursive)
                    {
                        foreach (PlanogramSubComponentView sub in item.Component.SubComponents)
                        {
                            foreach (PlanogramPositionView pos in sub.Positions)
                            {
                                InitialiseSelectionState(pos, isRecursive);
                            }
                            foreach (PlanogramAnnotationView anno in sub.Annotations)
                            {
                                InitialiseSelectionState(anno, isRecursive);
                            }
                        }
                    }
                    break;

                case PlanItemType.Position:
                case PlanItemType.Annotation:
                    item.IsSelectable = true;
                    break;

            }
        }

        /// <summary>
        /// Enumerates through all fixtures in this plan in sequence order.
        /// </summary>
        public IEnumerable<PlanogramFixtureView> EnumerateAllFixtures()
        {
            //If our numbering strategy isn't left to right then we need to get the fixtures in reverse
            //order. As the numbering strategy should not effect the order of the bays.
            if (this.RenumberingStrategy != null &&
                this.RenumberingStrategy.BayComponentXRenumberStrategy == Framework.Planograms.Model.PlanogramRenumberingStrategyXRenumberType.RightToLeft)
            {
                foreach (var fixture in this.Fixtures.OrderByDescending(f => f.BaySequenceNumber))
                {
                    yield return fixture;
                }
            }
            else
            {
                foreach (var fixture in this.Fixtures.OrderBy(f => f.BaySequenceNumber))
                {
                    yield return fixture;
                }
            }
        }

        /// <summary>
        /// Enumerates through all assemblies in this plan in sequence order.
        /// </summary>
        public IEnumerable<PlanogramAssemblyView> EnumerateAllAssemblies()
        {
            foreach (var fixture in EnumerateAllFixtures())
            {
                foreach (var assembly in fixture.Assemblies.OrderBy(a => a.Y))
                {
                    yield return assembly;
                }
            }
        }

        /// <summary>
        /// Enumerates through all components in this plan in sequence order
        /// </summary>
        public IEnumerable<PlanogramComponentView> EnumerateAllComponents()
        {
            foreach (var fixture in EnumerateAllFixtures())
            {
                foreach (var item in fixture.Assemblies.Cast<IPlanItem>()
                    .Union(fixture.Components.Cast<IPlanItem>())
                    .OrderBy(p => p.Y))
                {
                    if (item.PlanItemType == PlanItemType.Assembly)
                    {
                        foreach (var component in item.Assembly.Components.OrderBy(c => c.Y))
                        {
                            yield return component;
                        }
                    }
                    else
                    {
                        yield return item.Component;
                    }
                }
            }
        }

        /// <summary>
        /// Enumerates through all fixture components in this plan in sequence order
        /// </summary>
        public IEnumerable<PlanogramComponentView> EnumerateAllFixtureComponents()
        {
            foreach (var fixture in EnumerateAllFixtures())
            {
                foreach (var component in fixture.Components.OrderBy(p => p.Y))
                {
                    yield return component;
                }
            }
        }

        /// <summary>
        /// Enumerates through all assembly components in this plan in sequence order
        /// </summary>
        public IEnumerable<PlanogramComponentView> EnumerateAllAssemblyComponents()
        {
            foreach (var assembly in EnumerateAllAssemblies())
            {
                foreach (var component in assembly.Components.OrderBy(p => p.Y))
                {
                    yield return component;
                }
            }
        }

        /// <summary>
        /// Enumerates through all subcomponents in this plan in sequence order.
        /// </summary>
        public IEnumerable<PlanogramSubComponentView> EnumerateAllSubComponents()
        {
            foreach (var component in EnumerateAllComponents())
            {
                foreach (var sub in component.SubComponents.OrderBy(s => s.Y))
                {
                    yield return sub;
                }
            }
        }

        /// <summary>
        /// Returns the subcomponent view for the given model placement
        /// </summary>
        private PlanogramSubComponentView FindSubComponentView(PlanogramSubComponentPlacement modelPlacement)
        {
            return
                EnumerateMerchandisableSubComponents()
                .FirstOrDefault(s => Object.Equals(s.ModelPlacement.Id, modelPlacement.Id));
        }

        /// <summary>
        /// Enumerates through all merchandisiable subcomponents on this plan
        /// in sequence order.
        /// </summary>
        public IEnumerable<PlanogramSubComponentView> EnumerateMerchandisableSubComponents()
        {
            foreach (var sub in EnumerateAllSubComponents())
            {
                if (sub.IsMerchandisable)
                {
                    yield return sub;
                }
            }
        }

        /// <summary>
        /// Enumerates through all positions in the planogram in sequence.
        /// </summary>
        public IEnumerable<PlanogramPositionView> EnumerateAllPositions()
        {
            foreach (var sub in EnumerateAllSubComponents())
            {
                foreach (var pos in sub.Positions.OrderBy(p => p.Sequence))
                {
                    yield return pos;
                }
            }
        }

        /// <summary>
        /// Enumerates through all annotations in this plan.
        /// </summary>
        public IEnumerable<PlanogramAnnotationView> EnumerateAllAnnotations()
        {
            foreach (var fixture in this.Fixtures)
            {
                //fixture level annotations:
                foreach (var anno in fixture.Annotations)
                {
                    yield return anno;
                }

                foreach (var assembly in fixture.Assemblies)
                {
                    foreach (var component in assembly.Components)
                    {
                        //assembly component annotations:
                        foreach (var anno in component.Annotations)
                        {
                            yield return anno;
                        }

                        foreach (var subcomponent in component.SubComponents)
                        {
                            foreach (var anno in subcomponent.Annotations)
                            {
                                yield return anno;
                            }
                        }
                    }
                }
                foreach (var component in fixture.Components)
                {
                    //fixture component annotations:
                    foreach (var anno in component.Annotations)
                    {
                        yield return anno;
                    }

                    foreach (var subcomponent in component.SubComponents)
                    {
                        //subcomponent annotations:
                        foreach (var anno in subcomponent.Annotations)
                        {
                            yield return anno;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Removes the given item from this plan.
        /// </summary>
        /// <param name="item">the item to remove.</param>
        public void RemovePlanItem(IPlanItem item)
        {
            switch (item.PlanItemType)
            {
                case PlanItemType.Position:
                    Model.Positions.Remove(item.Position.Model);
                    break;

                case PlanItemType.Fixture:
                    RemoveFixture(item.Fixture);
                    break;

                case PlanItemType.Assembly:
                    item.Fixture.RemoveAssembly(item.Assembly);
                    break;

                case PlanItemType.Component:
                    if (item.Assembly != null)
                    {
                        item.Assembly.RemoveComponent(item.Component);
                    }
                    else
                    {
                        item.Fixture.RemoveComponent(item.Component);
                    }
                    break;

                case PlanItemType.SubComponent:
                    item.Component.RemoveSubComponent(item.SubComponent);
                    break;

                case PlanItemType.Product:
                    RemoveProduct(item.Product);
                    break;

                case PlanItemType.Annotation:
                    this.Model.Annotations.Remove(item.Annotation.Model);
                    break;
            }

            CleanUpAssemblies(this.Model);
        }

        /// <summary>
        /// Removes any empty fixtures and assemblies
        /// </summary>
        private static void CleanUpAssemblies(Planogram planogram)
        {
            //clean up any empty assemblies
            List<PlanogramAssembly> removeAssemblies = new List<PlanogramAssembly>();

            foreach (PlanogramAssembly assembly in planogram.Assemblies)
            {
                if (assembly.Components.Count == 0)
                {
                    removeAssemblies.Add(assembly);
                }
            }

            foreach (PlanogramFixture fixture in planogram.Fixtures)
            {
                foreach (PlanogramFixtureAssembly fixtureAssembly in fixture.Assemblies.ToList())
                {
                    if (removeAssemblies.Select(a => a.Id).Contains(fixtureAssembly.PlanogramAssemblyId))
                    {
                        fixture.Assemblies.Remove(fixtureAssembly);
                    }
                }
            }


            planogram.Assemblies.RemoveList(removeAssemblies);
        }

        /// <summary>
        /// Adds a new copy of the given item to this planogram.
        /// </summary>
        /// <param name="srcItem"></param>
        /// <param name="copyChildPositions">indicates whether child positions of fixture type items should be copied.</param>
        /// <param name="targetItem">The target that the planItem should be added to.</param>
        /// <returns>The new plan item.</returns>
        public IPlanItem AddPlanItemCopy(IPlanItem srcItem, Boolean shiftHeight, Boolean copyChildPositions, IPlanItem targetItem = null, Boolean renumberBays = true)
        {
            IPlanItem copy = null;

            switch (srcItem.PlanItemType)
            {
                #region Fixture
                case PlanItemType.Fixture:
                    {
                        PlanogramFixtureView fixtureView = AddFixtureCopy(srcItem.Fixture, true, copyChildPositions, renumberBays);
                        if (shiftHeight)
                        {
                            PlanogramFixtureViewModelBase lastFixture = FixtureViews.
                                Where(f => !f.FixtureItemModel.Id.Equals(fixtureView.FixtureItemModel.Id)).
                                OrderBy(f => f.X).LastOrDefault();

                            if (lastFixture != null)
                            {
                                fixtureView.X = lastFixture.Width + lastFixture.X;
                                fixtureView.BaySequenceNumber = (Int16)(lastFixture.BaySequenceNumber + 1);
                            }
                            else
                            {
                                fixtureView.X = 0;
                            }
                        }
                        copy = fixtureView;
                    }
                    break;
                #endregion

                #region Assembly
                case PlanItemType.Assembly:
                    {
                        //  Try finding the matching fixture view or a default one.
                        //  If there is none... just return null as there is no fixture to copy this plan item to.
                        PlanogramFixtureView fixtureView;
                        if (!TryFindMatchingFixtureView(srcItem, out fixtureView)) return null;

                        PlanogramAssemblyView assemblyView = fixtureView.AddAssemblyCopy(srcItem.Assembly, copyChildPositions);

                        if (shiftHeight)
                        {
                            assemblyView.Y += assemblyView.Height;
                        }

                        copy = assemblyView;
                    }
                    break;
                #endregion

                #region Component
                case PlanItemType.Component:
                    {
                        //  Try finding the matching fixture view or a default one.
                        //  If there is none... just return null as there is no fixture to copy this plan item to.
                        PlanogramFixtureView fixtureView;
                        if (!TryFindMatchingFixtureView(srcItem, out fixtureView)) return null;

                        Boolean isSourcePlan = srcItem.Planogram == this;

                        //Build a list of rect values that describe the space currently 
                        //consumed by existing components
                        List<RectValue> existingComponentRects = new List<RectValue>();
                        foreach (PlanogramComponentView compView in fixtureView.Components.
                            Where(c => c.ComponentModel.IsMerchandisable).OrderBy(c => c.Y))
                        {
                            existingComponentRects.Add(compView.GetBoundingRectangle());
                        }

                        //create the component view
                        PlanogramComponentView componentView;
                        if (srcItem.Assembly != null)
                        {
                            PlanogramAssemblyView assemblyView = (srcItem.Planogram == this) ? srcItem.Assembly : fixtureView.AddAssembly();
                            componentView = assemblyView.AddComponentCopy(srcItem.Component, copyChildPositions, !isSourcePlan);
                        }
                        else
                        {
                            componentView = fixtureView.AddComponentCopy(srcItem.Component, copyChildPositions, !isSourcePlan);
                        }

                        //set its initial position above the copied item and that 
                        //does not conflict with other components
                        if (shiftHeight)
                        {
                            //get the rect that describes the copied item
                            RectValue srcItemRect = srcItem.Component.GetBoundingRectangle();

                            //V8-31426 : don't do initially put item above copied otherwise
                            //cut and pasted components will start off massivly high. Instead
                            //rely on the intersect code below.
                            //componentView.Y += srcItemRect.Height + 0.1F;
                            RectValue componentRect = componentView.GetBoundingRectangle();

                            Boolean foundSpace = false;
                            while (!foundSpace)
                            {
                                componentRect = componentView.GetBoundingRectangle();
                                foundSpace = true;
                                foreach (RectValue rect in existingComponentRects.
                                    Where(r => (r.Y + r.Height) > componentRect.Y))
                                {
                                    if (componentRect.IntersectsWith(rect))
                                    {
                                        //conflict found
                                        foundSpace = false;
                                        componentView.Y += (rect.Y - componentRect.Y) + rect.Height;
                                        break;
                                    }
                                }
                            }
                        }

                        copy = componentView;
                    }
                    break;
                #endregion

                #region SubComponent

                //TODO: SORT Out

                case PlanItemType.SubComponent:
                    {
                        //  Try finding the matching fixture view or a default one.
                        //  If there is none... just return null as there is no fixture to copy this plan item to.
                        PlanogramFixtureView fixtureView;
                        if (!TryFindMatchingFixtureView(srcItem, out fixtureView)) return null;

                        PlanogramComponentView componentView;

                        if (srcItem.Assembly != null)
                        {
                            PlanogramAssemblyView assemblyView = (srcItem.Planogram == this) ? srcItem.Assembly : fixtureView.AddAssembly();
                            componentView = (srcItem.Planogram == this) ? srcItem.Component : assemblyView.AddComponentCopy(srcItem.Component, copyChildPositions);
                        }
                        else
                        {
                            componentView = (srcItem.Planogram == this) ? srcItem.Component : fixtureView.AddComponentCopy(srcItem.Component, copyChildPositions);
                        }


                        PlanogramSubComponentView subComponentView = componentView.AddSubComponentCopy(srcItem.SubComponent, copyChildPositions);

                        if (shiftHeight)
                        {
                            subComponentView.Y += subComponentView.Height;
                        }

                        copy = subComponentView;
                    }
                    break;
                #endregion

                #region Annotation

                //todo: sort out

                case PlanItemType.Annotation:
                    {
                        //  Try finding the matching fixture view or a default one.
                        //  If there is none... just return null as there is no fixture to copy this plan item to.
                        PlanogramFixtureView fixtureView;
                        if (!TryFindMatchingFixtureView(srcItem, out fixtureView)) return null;

                        PlanogramAnnotationView annoView;

                        if (srcItem.SubComponent == null)
                        {
                            annoView = fixtureView.AddAnnotationCopy(srcItem.Annotation);
                        }
                        else
                        {
                            PlanogramComponentView componentView;

                            if (srcItem.Assembly != null)
                            {
                                PlanogramAssemblyView assemblyView = (srcItem.Planogram == this) ? srcItem.Assembly : fixtureView.AddAssembly();
                                componentView = (srcItem.Planogram == this) ? srcItem.Component : assemblyView.AddComponentCopy(srcItem.Component);

                            }
                            else
                            {
                                componentView = (srcItem.Planogram == this) ? srcItem.Component : fixtureView.Components.FirstOrDefault();
                            }


                            PlanogramSubComponentView subComponentView = (srcItem.Planogram == this) ? srcItem.SubComponent : componentView.AddSubComponent();

                            annoView = subComponentView.AddAnnotationCopy(srcItem.Annotation);
                        }

                        copy = annoView;
                    }
                    break;
                #endregion

                #region Position

                case PlanItemType.Position:
                    {
                        PlanogramSubComponentView subView;

                        // Try and get a target for this position based on the target item.
                        if (targetItem != null && targetItem.Component != null)
                        {
                            subView = targetItem.Component.SubComponents.FirstOrDefault(s => s.IsMerchandisable);
                        }
                        else if (targetItem != null && targetItem.Assembly != null)
                        {
                            subView = targetItem.Assembly.Components.FirstOrDefault().SubComponents.FirstOrDefault(s => s.IsMerchandisable);
                        }
                        else
                        // Add to the nearest merchandisable component of the same type.
                        {
                            subView = this.EnumerateMerchandisableSubComponents().
                                FirstOrDefault(s => s.Component.ComponentType == srcItem.Component.ComponentType);
                        }

                        if (subView != null)
                        {
                            copy = subView.AddPositionCopy(srcItem.Position);

                            if (shiftHeight)
                            {
                                var lastPosition = subView.Positions.OrderBy(p => p.X).LastOrDefault();

                                // If the position isn't the last one, then we need to adjust its location
                                // to be on the end, because the shiftHeight flag was set.
                                if (copy != lastPosition)
                                {
                                    copy.X = lastPosition.X + lastPosition.Width;
                                    copy.Position.SequenceX = (Int16)(lastPosition.SequenceX + 1);
                                }
                            }
                        }
                    }
                    break;
                #endregion

                case PlanItemType.Product:
                    AddProduct(srcItem.Product.Model);
                    break;

                default:
                    Debug.Fail("Not Implemented");
                    break;
            }

            if (copy != null)
            {
                copy.IsSelectable = true;
            }

            return copy;
        }

        /// <summary>
        ///     Add copies of the given <paramref name="sourcePlanItems"/> to the <paramref name="targetItem"/>.
        /// </summary>
        /// <param name="sourcePlanItems"><see cref="IEnumerable{T}"/> of <see cref="IPlanItem"/> to be copied.</param>
        /// <param name="targetItem">The <see cref="IPlanItem"/> that serves as the starting target for the copy operation.</param>
        /// <remarks>
        ///     The items are added in object graph hierarchy, from fixtures down to products.
        ///     This ensures that the selected child items of a copy targer the copy to look like the original.
        ///     If no children of a particular type are selected ALL children for the parent type will be copied.</remarks>
        public List<IPlanItem> AddPlanItemCopies(IEnumerable<IPlanItem> sourcePlanItems, IPlanItem targetItem)
        {
            const Boolean shiftHeight = true;
            const Boolean supressPlanUpdates = true;

            Boolean copyChildPositions = App.ViewState.SelectionMode != PlanItemSelectionType.OnlyComponents;

            var itemCopies = new List<IPlanItem>();
            //  If there are no items to copy just return;
            if (!sourcePlanItems.Any()) return itemCopies;

            using (NewUndoableAction(!supressPlanUpdates))
            {
                //  Order the items to ensure they are placed as they were originally.
                List<IPlanItem> toCopyList = sourcePlanItems
                    .OrderBy(item => item.Fixture?.BaySequenceNumber ?? 0)
                    .ThenBy(item => item.Component?.ComponentSequenceNumber ?? 0)
                    .ThenBy(item => item.Position?.SequenceX ?? 0)
                    .ThenBy(item => item.Position?.SequenceY ?? 0)
                    .ThenBy(item => item.Position?.SequenceZ ?? 0)
                    .ToList();

                //  Copy every type of item in order, so that dependant child items are copied on their parent's copies.
                itemCopies.AddRange(CopyItemsOfType(PlanItemType.Fixture, toCopyList, shiftHeight, copyChildPositions, targetItem));
                itemCopies.AddRange(CopyItemsOfType(PlanItemType.Assembly, toCopyList, shiftHeight, copyChildPositions, targetItem));
                itemCopies.AddRange(CopyItemsOfType(PlanItemType.Component, toCopyList, shiftHeight, copyChildPositions && toCopyList.All(item => item.PlanItemType != PlanItemType.Position), targetItem));
                itemCopies.AddRange(CopyItemsOfType(PlanItemType.SubComponent, toCopyList, shiftHeight, copyChildPositions, targetItem));
                itemCopies.AddRange(CopyItemsOfType(PlanItemType.Annotation, toCopyList, shiftHeight, copyChildPositions, targetItem));
                itemCopies.AddRange(CopyItemsOfType(PlanItemType.Position, toCopyList, shiftHeight, copyChildPositions, targetItem));
                itemCopies.AddRange(CopyItemsOfType(PlanItemType.Product, toCopyList, shiftHeight, copyChildPositions, targetItem));
            }

            return itemCopies;
        }

        /// <summary>
        /// Creates a new planogram image from the given data.
        /// </summary>
        public PlanogramImage AddPlanogramImage(Byte[] imgData, String fileName, String description)
        {
            PlanogramImage img = PlanogramImage.NewPlanogramImage();
            img.ImageData = imgData;
            img.FileName = fileName;
            img.Description = description;


            Model.Images.Add(img);

            return img;
        }

        /// <summary>
        /// Append another plan at the right side of the current plan
        /// </summary>
        /// <param name="appendingPlanInfo">The PlanogramInfo object of the plan to append</param>
        /// <returns>True if the appending succeeded, false otherwise</returns>
        public Boolean AppendPlanogram(PlanogramInfo appendingPlanInfo)
        {
            //  Append the planogram referenced in the planogram info, if there is an info.
            return appendingPlanInfo != null && AppendPlanogram(Package.FetchById(appendingPlanInfo.Id).Planograms.FirstOrDefault());
        }

        /// <summary>
        /// Append another plan at the right side of the current plan
        /// </summary>
        /// <param name="appendingPlan">The PlanogramInfo object of the plan to append</param>
        /// <returns>True if the appending succeeded, false otherwise</returns>
        public Boolean AppendPlanogram(Planogram appendingPlan)
        {
            //  Check whether there is actually an appending plan.
            if (appendingPlan == null) return false;

            using (PlanogramUndoableAction undoableAction = NewUndoableAction(supressPlanUpdates: false))
            {
                var appendingPlanView = new PlanogramView(appendingPlan, null);

                //  First, make sure all new products are available for appended positions.
                Model.Products.AppendNew(appendingPlan.Products);

                //  Next, make sure any new assortment products are appended too.
                Model.Assortment.Products.AppendNew(appendingPlan.Assortment.Products);

                //  Finally, append all the fixtures in the other plan. Each fixture will be copied with everything inside.
                Single cumulativeWidth = this.Width;
                var appendSequence = (Int16)appendingPlan.Fixtures.Count;

                //  Append the fixtures in their original bay sequence order.
                foreach (PlanogramFixtureView subFixture in appendingPlanView.Fixtures.OrderBy(x => x.BaySequenceNumber))
                {
                    subFixture.X = cumulativeWidth;
                    subFixture.BaySequenceNumber = ++appendSequence;

                    AddFixtureCopy(subFixture);

                    cumulativeWidth += subFixture.Width;
                }
            }

            return true;
        }

        /// <summary>
        /// [V8-30267] Temporary method to adjust the defaults for imperial
        /// Next version we should tie this into the plan settings properly.
        /// </summary>
        /// <param name="component"></param>
        internal static void AdjustDefaultsForUOM(PlanogramLengthUnitOfMeasureType uom, PlanogramComponent component)
        {
            if (uom != PlanogramLengthUnitOfMeasureType.Inches) return;

            PlanogramSubComponent sub = component.SubComponents.FirstOrDefault();
            if (sub == null) return;

            switch (component.ComponentType)
            {
                case PlanogramComponentType.Backboard:
                case PlanogramComponentType.Base:
                    sub.LineThickness = 0.2F;
                    break;

                case PlanogramComponentType.Peg:
                    sub.LineThickness = 0.2F;
                    sub.MerchConstraintRow1StartX = 2F;
                    sub.MerchConstraintRow1StartY = 2F;
                    sub.MerchConstraintRow1SpacingX = 2F;
                    sub.MerchConstraintRow1SpacingY = 2F;
                    sub.MerchConstraintRow1Height = 0.5F;
                    sub.MerchConstraintRow2Height = 0.5F;
                    sub.MerchConstraintRow1Width = 0.5F;
                    sub.MerchConstraintRow2Width = 0.5F;
                    break;

                case PlanogramComponentType.ClipStrip:
                    sub.LineThickness = 0.2F;
                    sub.MerchConstraintRow1StartX = 1F;
                    sub.MerchConstraintRow1StartY = 2F;
                    sub.MerchConstraintRow1SpacingX = 1F;
                    sub.MerchConstraintRow1SpacingY = 2F;
                    sub.MerchConstraintRow1Height = 0.5F;
                    sub.MerchConstraintRow2Height = 0.5F;
                    sub.MerchConstraintRow1Width = 0.5F;
                    sub.MerchConstraintRow2Width = 0.5F;
                    break;

                case PlanogramComponentType.Chest:
                    sub.LineThickness = 0.2F;
                    break;

                case PlanogramComponentType.Bar:
                case PlanogramComponentType.Rod:
                case PlanogramComponentType.Pallet:
                case PlanogramComponentType.Panel:
                    sub.LineThickness = 0.2F;
                    break;

                case PlanogramComponentType.Shelf:
                    sub.LineThickness = 0.2F;
                    sub.RiserThickness = 0.5F;
                    break;

                case PlanogramComponentType.SlotWall:
                    sub.LineThickness = 0.2F;
                    sub.MerchConstraintRow1StartX = 0F;
                    sub.MerchConstraintRow1StartY = 2F;
                    sub.MerchConstraintRow1SpacingX = 0F;
                    sub.MerchConstraintRow1SpacingY = 2F;
                    sub.MerchConstraintRow1Height = 0.5F;
                    sub.MerchConstraintRow2Height = 0F;
                    break;
            }
        }

        /// <summary>
        ///     Copy all <see cref="IPlanItem"/> instances of the given <paramref name="planItemType"/> in <paramref name="selectedItems"/> and with the given <paramref name="targetItem"/>.
        /// </summary>
        /// <param name="planItemType">The <see cref="PlanItemType"/> that the items in <paramref name="selectedItems"/> need to be of for them to be copied.</param>
        /// <param name="selectedItems">The <see cref="ICollection{T}"/> of <see cref="IPlanItem"/> instances to copy.</param>
        /// <param name="shiftHeight">Whether the copied items might need to be displaced from the original.</param>
        /// <param name="copyChildPositions">Whether to copy the child items along with the copies.</param>
        /// <param name="targetItem">The target for the copies to be placed around.</param>
        /// <returns>An <see cref="IEnumerable{T}"/> of <see cref="IPlanType"/> with the copies.</returns>
        /// <remarks>If the <paramref name="planItemType"/> is <see cref="PlanItemType.Position"/> and the <paramref name="targetItem"/> does not contain a <see cref="IPlanItem.Component"/> to copy them to, 
        /// the positions will be considered orphaned and not copied.</remarks>
        private IEnumerable<IPlanItem> CopyItemsOfType(PlanItemType planItemType, ICollection<IPlanItem> selectedItems, Boolean shiftHeight, Boolean copyChildPositions, IPlanItem targetItem)
        {
            List<IPlanItem> originalItems = selectedItems.Where(item => item.PlanItemType == planItemType).ToList();
            var copiedItems = new List<IPlanItem>();

            // Check that the target item is valid if the items to paste are positions.
            if (planItemType == PlanItemType.Position && targetItem?.Component == null)
            {
                //  Do not process any of the orphaned positions.
                foreach (IPlanItem item in originalItems)
                {
                    selectedItems.Remove(item);
                }

                return copiedItems;
            }

            foreach (IPlanItem original in originalItems)
            {
                //  Do not process this source item again.
                selectedItems.Remove(original);

                //  Find any child items that were selected to be copied along.
                List<IPlanItem> childItems = selectedItems.Where(item => BelongsTo(item, original)).ToList();

                //  Copy the source item and any child items that should be copied with it.
                //  If there are any selected child items then copyChildPositions will be overriden and no other child will be copied along,
                //  only the selected ones.
                IPlanItem copy = AddPlanItemCopy(original, shiftHeight, copyChildPositions && !childItems.Any(), targetItem);
                if (copy == null) continue;
                copiedItems.Add(copy);
                copiedItems.AddRange(AddPlanItemCopies(childItems, copy));

                //  Do not process any of the child items again.
                foreach (IPlanItem item in childItems)
                {
                    selectedItems.Remove(item);
                }
            }

            return copiedItems;
        }

        /// <summary>
        ///     Checks whether the fiven <paramref name="item"/> is a descendant of the <paramref name="source"/> item.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        private static Boolean BelongsTo(IPlanItem item, IPlanItem source)
        {
            switch (source.PlanItemType)
            {
                case PlanItemType.Fixture:
                    return source.Equals(item.Fixture);
                case PlanItemType.Assembly:
                    return source.Equals(item.Assembly);
                case PlanItemType.Component:
                    return source.Equals(item.Component);
                case PlanItemType.SubComponent:
                    return source.Equals(item.SubComponent);
                case PlanItemType.Position:
                    return source.Equals(item.Position);
                case PlanItemType.Annotation:
                    return source.Equals(item.Annotation);
                case PlanItemType.Product:
                    return source.Equals(item.Product);
                case PlanItemType.Planogram:
                    return source.Equals(item.Planogram);
                default:
                    return false;
            }
        }

        #endregion

        #region UndoableAction Recording

        /// <summary>
        /// Returns true if this view is currently in editing mode
        /// During this time any performance intensive updates should be supressed.
        /// </summary>
        public Boolean IsRecordingUndoableAction
        {
            get { return _isRecordingUndoableGroupAction; }
            private set
            {
                _isRecordingUndoableGroupAction = value;
                OnPropertyChanged(IsEditingProperty);
            }
        }

        /// <summary>
        /// Returns a new undoable action, to start recording immeidately until it is disposed.
        /// </summary>
        /// <returns></returns>
        public PlanogramUndoableAction NewUndoableAction()
        {
            return NewUndoableAction(null, false);
        }

        public PlanogramUndoableAction NewUndoableAction(Boolean supressPlanUpdates)
        {
            return NewUndoableAction(null, supressPlanUpdates);
        }

        public PlanogramUndoableAction NewUndoableAction(String undoDescription, Boolean supressPlanUpdates)
        {
            return PlanogramUndoableAction.New(this, undoDescription, supressPlanUpdates);
        }

        /// <summary>
        /// Begins a new edit on this planogram.
        /// This will start recording a new undo action.
        /// </summary>
        public void BeginUndoableAction()
        {
            BeginUndoableAction(null);
        }

        public void BeginUndoableAction(String undoDescription)
        {
            //if we are already recording a group, just ignore this call.
            if (IsRecordingUndoableAction) return;

            //cancel any queued background processing
            CancelPostProcessing(false);

            //if we are recording a single action, complete it.
            if (_processingUndoAction != null) CompleteUndoableAction();

            //Set the flag and create the new action
            this.IsRecordingUndoableAction = true;
            _processingUndoAction = new ModelUndoAction();
            _processingUndoAction.DescriptionText = undoDescription;
        }

        private void BeginUndoablePostProcessAction()
        {
            //if we are already recording a group, just ignore this call.
            if (IsRecordingUndoableAction) return;

            //if we are recording a single action, complete it.
            if (_processingUndoAction != null) CompleteUndoableAction();

            //Set the flag and create the new action
            this.IsRecordingUndoableAction = true;
            _processingUndoAction = new ModelUndoAction();
        }

        /// <summary>
        /// Cancels any outstanding action record.
        /// </summary>
        public void CancelUndoableAction(Boolean undoLoggedChanges)
        {
            //if nothing changed, return.
            if (_processingUndoAction == null) return;

            if (undoLoggedChanges && _processingUndoAction.ActionItems.Count > 0)
            {
                //commit the action then undo it.
                CompleteUndoableAction();
                Undo();
                _redoActions.Remove(_redoActions.FirstOrDefault());
            }
            else
            {
                //just discard the action
                _processingUndoAction = null;
                IsRecordingUndoableAction = false;

                //queue another process just in case we need to rerun
                // the background process worker again.
                if (_isPlanChanged) QueueProcessPlanChanges();
            }
        }

        /// <summary>
        /// Completes the current edit.
        /// A new undo action will be created for actions recorded
        /// </summary>
        /// <returns>true if an action was created.</returns>
        public void EndUndoableAction()
        {
            //turn off recording
            this.IsRecordingUndoableAction = false;

            //if nothing changed, return false.
            if (_processingUndoAction == null) return;

            //queue a plan reprocess for any changes.
            // Only do this if we actually had changes otherwise
            // using keyboard to select items on plan will reprocess the plan.
            if (_processingUndoAction.ActionItems.Any())
                QueueProcessPlanChanges();
        }

        /// <summary>
        /// Processes the given ChildChangingEventArgs to record
        /// an undoable action if required.
        /// </summary>
        private void RecordUndoableAction(ChildChangingEventArgs e)
        {
            if (_suppressChangeRecording) return;

            //PropertyChanging:
            if (e.PropertyChangingArgs != null)
            {
                if (String.IsNullOrEmpty(e.PropertyChangingArgs.PropertyName)) return;
                if (e.PropertyChangingArgs.PropertyName == "IsUpdating") return;

                //If we have no active action then create one.
                if (_processingUndoAction == null)
                    _processingUndoAction = new ModelUndoAction();

                RecordUndoableAction(e.ChildObject, e.PropertyChangingArgs);
            }

            //Collection changing - not required.
        }

        /// <summary>
        /// Processes the given ChildChangedEventArgs to record
        /// an undoable action if required.
        /// </summary>
        private void RecordUndoableAction(Csla.Core.ChildChangedEventArgs e)
        {
            if (_suppressChangeRecording) return;

            //** Property Change:
            if (e.PropertyChangedArgs != null)
            {
                //just return out if we have no original state saved
                if (_processingUndoAction == null) return;

                RecordUndoableAction(e.ChildObject, e.PropertyChangedArgs, false);
            }

            //** Collection Change:
            else if (e.CollectionChangedArgs != null)
            {
                //If we have no active action then create one.
                if (_processingUndoAction == null)
                    _processingUndoAction = new ModelUndoAction();

                //create the collection changed action.
                switch (e.CollectionChangedArgs.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        {
                            ModelUndoCollectionActionItem item = new ModelUndoCollectionActionItem();
                            item.ActionType = e.CollectionChangedArgs.Action;
                            item.ModelList = e.ChildObject as IList;
                            item.ModelObject = e.CollectionChangedArgs.NewItems[0] as IModelObject;
                            _processingUndoAction.ActionItems.Add(item);
                        }
                        break;

                    case NotifyCollectionChangedAction.Remove:
                        {
                            ModelUndoCollectionActionItem item = new ModelUndoCollectionActionItem();
                            item.ActionType = e.CollectionChangedArgs.Action;
                            item.ModelList = e.ChildObject as IList;
                            item.ModelObject = e.CollectionChangedArgs.OldItems[0] as IModelObject;
                            _processingUndoAction.ActionItems.Add(item);
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Records a property changing action
        /// </summary>
        private void RecordUndoableAction(Object sender, PropertyChangingEventArgs e)
        {
            if (_suppressChangeRecording) return;

            //if the property has no name, dont bother.
            if (String.IsNullOrEmpty(e.PropertyName)) return;
            if (e.PropertyName == "IsUpdating") return;

            //If we have no active action then create one.
            if (_processingUndoAction == null)
                _processingUndoAction = new ModelUndoAction();

            //add the action item.
            try
            {
                System.Reflection.PropertyInfo pInfo = sender.GetType().GetProperty(e.PropertyName);
                if (pInfo != null &&
                    pInfo.CanRead &&
                    pInfo.GetSetMethod() != null)
                {
                    Object value = pInfo.GetValue(sender, null);

                    ModelUndoPropertyActionItem item = new ModelUndoPropertyActionItem();
                    item.TargetObject = sender;
                    item.PropertyName = e.PropertyName;
                    item.OldValue = value;
                    item.NewValue = value;
                    item.IsBusy = true;
                    _processingUndoAction.ActionItems.Add(item);
                }
            }
            catch (Exception ex)
            {
                LocalHelper.RecordException(ex);
            }
        }

        /// <summary>
        /// Adds a new action item to the current undoable action.
        /// </summary>
        private void RecordUndoableAction(Object sender, PropertyChangedEventArgs e, Boolean completeAction)
        {
            if (_suppressChangeRecording) return;

            //just return out if we have no original state saved
            // or if the property is one we cannot record.
            if (_processingUndoAction == null) return;
            if (String.IsNullOrEmpty(e.PropertyName)) return;


            //find the initial state in the current action.
            ModelUndoPropertyActionItem item = _processingUndoAction.ActionItems.OfType<ModelUndoPropertyActionItem>().LastOrDefault(a => a.TargetObject == sender && a.PropertyName == e.PropertyName);
            if (item == null) return;

            //log the change.
            System.Reflection.PropertyInfo pInfo = sender.GetType().GetProperty(e.PropertyName);
            if (pInfo != null &&
                pInfo.GetSetMethod() != null) item.NewValue = pInfo.GetValue(sender, null);

            //flag the property change action as ready.
            item.IsBusy = false;

            //complete the action immediately if required.
            if (completeAction) CompleteUndoableAction();
        }

        /// <summary>
        /// Completes the current;y processing undoable action.
        /// </summary>
        private void CompleteUndoableAction()
        {
            //return out if we are null or still processing.
            if (_processingUndoAction == null) return;
            if (_processingUndoAction.ActionItems.Any(u => u.IsBusy)) return;

            //Copy the action we are undoing
            ModelUndoAction undo = _processingUndoAction;

            //reset the main action
            _processingUndoAction = null;
            IsRecordingUndoableAction = false;

            if (undo.ActionItems.Count > 0)
            {
                //add the undo action
                _undoActions.Insert(0, undo);

                if (_undoActions.Count > _undoMax)
                    _undoActions.Remove(_undoActions.Last());

                //Request a requery so that the undo button updates.
                CommandManager.InvalidateRequerySuggested();
            }
        }

        /// <summary>
        /// Undos all actions upto the one given.
        /// If no action is provided only the last action is removed.
        /// </summary>
        public void Undo()
        {
            Undo(null);
        }

        public void Undo(ModelUndoAction action)
        {
            ModelUndoAction ac = (action != null) ? action : _undoActions.FirstOrDefault();

            if (ac != null &&
                _undoActions.Contains(ac) &&
                !_isProcessingPlanChanges)
            {
                //flag up that we are processing changes
                // so that things don't get reprocessed.
                _suppressChangeRecording = true;
                _preventProcessQueueing = true;


                foreach (ModelUndoAction undoAct in _undoActions.ToList())
                {
                    _undoActions.Remove(undoAct);

                    if (undoAct.Undo())
                    {
                        //add as a redo item.
                        _redoActions.Insert(0, undoAct);
                        if (_redoActions.Count > _undoMax)
                            _redoActions.Remove(_redoActions.Last());
                    }
                    else
                    {
                        Debug.Fail("Failed to undo last action");
                        break;
                    }

                    if (undoAct == ac) break;
                }


                //reprocess immediately.
                _preventProcessQueueing = false;

                //Ensure supressing change recording
                _suppressChangeRecording = true;

                ProcessPlanChanges();

                //cancel off process flag.
                _suppressChangeRecording = false;
            }
        }

        /// <summary>
        /// Reapplied all actions upto the one given
        /// If no action is provided only the last action is redone.
        /// </summary>
        public void Redo()
        {
            Redo(null);
        }

        public void Redo(ModelUndoAction action)
        {
            ModelUndoAction ac = (action != null) ? action : _redoActions.FirstOrDefault();

            if (ac != null &&
                _redoActions.Contains(ac) &&
                !_isProcessingPlanChanges)
            {
                //flag up that we are processing changes
                // so that things don't get reprocessed.
                _preventProcessQueueing = true;
                _suppressChangeRecording = true;

                foreach (ModelUndoAction redoAction in _redoActions.ToList())
                {
                    if (redoAction.Redo())
                    {
                        _redoActions.Remove(redoAction);

                        //add as a undo item again.
                        _undoActions.Insert(0, redoAction);
                        if (_undoActions.Count > _undoMax)
                            _undoActions.Remove(_undoActions.Last());
                    }
                    else
                    {
                        Debug.Fail("Failed to redo last action");
                        _redoActions.Remove(redoAction);
                        break;
                    }

                    if (redoAction == ac) break;
                }

                //reprocess immediately.
                _preventProcessQueueing = false;
                ProcessPlanChanges();

                //reallow change recording.
                _suppressChangeRecording = false;
            }
        }

        /// <summary>
        /// Clears out all existing undo and redo actions.
        /// </summary>
        public void ClearAllUndoRedoActions()
        {
            _undoActions.Clear();
            _redoActions.Clear();
        }

        /// <summary>
        /// Completes an undoable action that is being recorded as part of the
        /// background process update change commit.
        /// </summary>
        private void EndUndoableBackgroundProcessAction()
        {
            //turn off recording
            this.IsRecordingUndoableAction = false;

            if (_undoActions.Count == 0) return;

            //if nothing changed, return false.
            if (_processingUndoAction == null) return;


            //complete the action:

            //Copy the action we are undoing
            ModelUndoAction undo = _processingUndoAction;


            //reset the main action
            _processingUndoAction = null;

            if (undo.ActionItems.Count > 0)
            {
                ModelUndoAction mergeIntoAction = _undoActions.FirstOrDefault();
                if (mergeIntoAction != null)
                    mergeIntoAction.ActionItems.AddRange(undo.ActionItems);
            }
        }

        public sealed class PlanogramUndoableAction : IDisposable
        {
            private PlanogramView PlanogramView { get; set; }
            private Boolean IsRecording { get; set; }
            private Boolean IsUpdating { get; set; }

            internal static PlanogramUndoableAction New(PlanogramView plan, String description, Boolean supressPlanUpdates)
            {
                return new PlanogramUndoableAction(plan, description, supressPlanUpdates);
            }

            private PlanogramUndoableAction(PlanogramView plan, String description, Boolean supressPlanUpdates)
            {
                PlanogramView = plan;

                this.IsRecording = !plan.IsRecordingUndoableAction;
                this.IsUpdating = !supressPlanUpdates && !plan.IsUpdating;

                if (IsRecording) plan.BeginUndoableAction(description);
                if (IsUpdating) plan.BeginUpdate();
            }


            public void Dispose()
            {
                if (IsRecording)
                {
                    PlanogramView.EndUndoableAction();
                    IsRecording = false;
                }

                if (IsUpdating)
                {
                    PlanogramView.EndUpdate();
                    IsUpdating = false;
                }
            }
        }

        #endregion

        #region Fields

        /// <summary>
        /// Enumerates through the default set of displayable fields for this type.
        /// </summary>
        public static IEnumerable<ObjectFieldInfo> EnumerateDefaultPlanItemFields()
        {
            String[] defaults = new String[]
                                {
                                    Planogram.NameProperty.Name, Planogram.CategoryNameProperty.Name, Planogram.LocationNameProperty.Name, Planogram.HeightProperty.Name, Planogram.WidthProperty.Name, Planogram.DepthProperty.Name,
                                };

            foreach (String fieldName in defaults)
            {
                ObjectFieldInfo field = EnumerateDisplayableFields().FirstOrDefault(f => f.PropertyName == fieldName);
                if (field == null) continue;
                else yield return field;
            }

            //Also show all metadata properties for now.
            foreach (ObjectFieldInfo o in EnumerateDisplayableFields())
            {
                if (o.PropertyName.StartsWith("Meta"))
                    yield return o;
            }
        }

        /// <summary>
        /// Resolves the given expression for this item
        /// </summary>
        public Object GetCalculatedValue(String expressionText)
        {
            return PlanogramFieldHelper.ResolveFieldExpression(expressionText, PlanogramItemPlacement.NewPlanogramItemPlacement(planogram: this.Model));
        }

        #endregion

        #endregion

        #region IPlanItem Members

        Boolean IPlanItem.IsSelectable { get { return false; } set { throw new NotSupportedException(); } }
        PlanItemType IPlanItem.PlanItemType { get { return Common.PlanItemType.Planogram; } }
        PlanogramView IPlanItem.Planogram { get { return this; } }
        PlanogramFixtureView IPlanItem.Fixture { get { return null; } }
        PlanogramAssemblyView IPlanItem.Assembly { get { return null; } }
        PlanogramComponentView IPlanItem.Component { get { return null; } }
        PlanogramSubComponentView IPlanItem.SubComponent { get { return null; } }
        PlanogramAnnotationView IPlanItem.Annotation { get { return null; } }
        PlanogramPositionView IPlanItem.Position { get { return null; } }
        PlanogramProductView IPlanItem.Product { get { return null; } }
        Single IPlanItem.Slope { get { return 0; } set { throw new NotSupportedException(); } }
        Single IPlanItem.Angle { get { return 0; } set { throw new NotSupportedException(); } }
        Single IPlanItem.Roll { get { return 0; } set { throw new NotSupportedException(); } }
        Single IPlanItem.X { get { return 0; } set { throw new NotSupportedException(); } }
        Single IPlanItem.Y { get { return 0; } set { throw new NotSupportedException(); } }
        Single IPlanItem.Z { get { return 0; } set { throw new NotSupportedException(); } }

        #endregion

        #region IDisposable Members

        protected override void OnDisposing(bool disposing)
        {
            base.OnDisposing(disposing);

            _planProcessDelayTimer.Elapsed -= PlanProcessTimer_Elapsed;
            CancelPostProcessing(false);
            _parentPackageView = null;

            if (_flattenedView != null)
            {
                _flattenedView.Dispose();
                _flattenedView = null;
            }
        }

        #endregion
    }
}