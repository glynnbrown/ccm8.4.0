﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
//V8-26787 : L.Ineson
//  Changes to planitemfield
// V8-27938 : N.Haywood
//  Added missing properties
#endregion

#region Version History: (CCM 8.03)
// V8-29427 : L.Luong
//  Added Meta Data properties
#endregion

#region Version History: (CCM 8.3)
//V8-31832 : L.Ineson
// Implemented ICalculatedValueResolver
// V8-32518 : N.Haywood
//  Added moveToWorldPos for addcomponentcopy
// V8-32782 : L.Ineson
// Plan assembly now gets passed into constructor
#endregion
#endregion

using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Viewmodel of a PlanogramAssembly
    /// </summary>
    public sealed class PlanogramAssemblyView : PlanogramAssemblyViewModelBase, IPlanItem
    {
        #region Fields

        private readonly PlanogramFixtureView _parentFixtureView;
        private CastedObservableCollection<PlanogramComponentView, PlanogramComponentViewModelBase> _componentsRO;
        private CalculatedValueResolver _calculatedValueResolver;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath WorldXProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.WorldX);
        public static readonly PropertyPath WorldYProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.WorldY);
        public static readonly PropertyPath WorldZProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.WorldZ);
        public static readonly PropertyPath WorldAngleProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.WorldAngle);
        public static readonly PropertyPath WorldSlopeProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.WorldSlope);
        public static readonly PropertyPath WorldRollProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.WorldRoll);

        // Meta Data
        public static readonly PropertyPath MetaComponentCountProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaComponentCount);
        public static readonly PropertyPath MetaTotalMerchandisableLinearSpaceProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaTotalMerchandisableLinearSpace);
        public static readonly PropertyPath MetaTotalMerchandisableAreaSpaceProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaTotalMerchandisableAreaSpace);
        public static readonly PropertyPath MetaTotalMerchandisableVolumetricSpaceProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaTotalMerchandisableVolumetricSpace);
        public static readonly PropertyPath MetaTotalLinearWhiteSpaceProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaTotalLinearWhiteSpace);
        public static readonly PropertyPath MetaTotalAreaWhiteSpaceProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaTotalAreaWhiteSpace);
        public static readonly PropertyPath MetaTotalVolumetricWhiteSpaceProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaTotalVolumetricWhiteSpace);
        public static readonly PropertyPath MetaProductsPlacedProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaProductsPlaced);
        public static readonly PropertyPath MetaNewProductsProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaNewProducts);
        public static readonly PropertyPath MetaChangesFromPreviousCountProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaChangesFromPreviousCount);
        public static readonly PropertyPath MetaChangeFromPreviousStarRatingProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaChangeFromPreviousStarRating);
        public static readonly PropertyPath MetaBlocksDroppedProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaBlocksDropped);
        public static readonly PropertyPath MetaNotAchievedInventoryProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaNotAchievedInventory);
        public static readonly PropertyPath MetaTotalFacingsProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaTotalFacings);
        public static readonly PropertyPath MetaAverageFacingsProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaAverageFacings);
        public static readonly PropertyPath MetaTotalUnitsProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaTotalUnits);
        public static readonly PropertyPath MetaAverageUnitsProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaAverageUnits);
        public static readonly PropertyPath MetaMinDosProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaMinDos);
        public static readonly PropertyPath MetaMaxDosProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaMaxDos);
        public static readonly PropertyPath MetaAverageDosProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaAverageDos);
        public static readonly PropertyPath MetaMinCasesProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaMinCases);
        public static readonly PropertyPath MetaAverageCasesProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaAverageCases);
        public static readonly PropertyPath MetaMaxCasesProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaMaxCases);
        public static readonly PropertyPath MetaSpaceToUnitsIndexProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaSpaceToUnitsIndex);
        public static readonly PropertyPath MetaTotalFrontFacingsProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaTotalFrontFacings);
        public static readonly PropertyPath MetaAverageFrontFacingsProperty = WpfHelper.GetPropertyPath<PlanogramAssemblyView>(p => p.MetaAverageFrontFacings);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the collection of child component views.
        /// </summary>
        public CastedObservableCollection<PlanogramComponentView, PlanogramComponentViewModelBase> Components
        {
            get 
            {
                if (_componentsRO == null)
                {
                    _componentsRO = 
                        new CastedObservableCollection<PlanogramComponentView,PlanogramComponentViewModelBase>(base.ComponentViews, true);
                }
                return _componentsRO;
            }
        }

        public Int32 NumberOfComponents
        {
            get
            {
                return _componentsRO.Count;
            }
        }

        /// <summary>
        /// Returns the resolver used for binding to calculated values.
        /// </summary>
        public CalculatedValueResolver CalculatedValueResolver
        {
            get
            {
                if (_calculatedValueResolver == null)
                {
                    _calculatedValueResolver = 
                        new CalculatedValueResolver(GetCalculatedValue, /*enableCaching*/true);
                }
                return _calculatedValueResolver;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="parentFixtureView"></param>
        /// <param name="fixtureAssembly"></param>
        public PlanogramAssemblyView(PlanogramFixtureView parentFixtureView, PlanogramFixtureAssembly fixtureAssembly, PlanogramAssembly assembly)
            : base(parentFixtureView, fixtureAssembly, assembly)
        {
            _parentFixtureView = parentFixtureView;
            InitializeChildViews();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates a new viewmodel of the given component.
        /// </summary>
        /// <param name="assemblyComponent"></param>
        /// <returns></returns>
        protected override PlanogramComponentViewModelBase CreateComponentView(PlanogramAssemblyComponent assemblyComponent)
        {
            PlanogramComponent component = assemblyComponent.GetPlanogramComponent();
            if (component == null) return null;
            return new PlanogramComponentView(this, assemblyComponent, component);
        }

        /// <summary>
        /// Adds a new component setup as per defaults of the given type.
        /// </summary>
        /// <param name="componentType"></param>
        /// <returns></returns>
        public PlanogramComponentView AddComponent(PlanogramComponentType componentType)
        {
            Single height;
            Single width;
            Single depth;

            LocalHelper.GetDefaultComponentSize((FixtureComponentType)componentType, App.ViewState.Settings.Model,
                out height, out width, out depth);

            PlanogramAssemblyComponent ac = this.AssemblyModel.Components.Add(componentType, width, height, depth);

            return this.Components.First(c => c.AssemblyComponentModel == ac);
        }

        /// <summary>
        /// Adds a copy of the given component to this assembly.
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        public PlanogramComponentView AddComponentCopy(PlanogramComponentView src, Boolean copyPositions = true, Boolean moveToWorldPos = false)
        {
            Planogram planModel = this.AssemblyModel.Parent;

            //create shallow model copies
            PlanogramComponent componentCopy = src.ComponentModel.Copy();
            componentCopy.Name = String.Format(CultureInfo.CurrentCulture, Message.NewPlanogramComponentName, this.Planogram.Model.Components.Count);
            componentCopy.SubComponents.Clear();
            planModel.Components.Add(componentCopy);

            if (src.FixtureComponentModel != null)
            {
                PlanogramFixtureComponent srcFixtureComponent = src.FixtureComponentModel;

                PlanogramAssemblyComponent assemblyComponent = PlanogramAssemblyComponent.NewPlanogramAssemblyComponent();
                assemblyComponent.PlanogramComponentId = componentCopy.Id;
                assemblyComponent.X = srcFixtureComponent.X;
                assemblyComponent.Y = srcFixtureComponent.Y;
                assemblyComponent.Z = srcFixtureComponent.Z;
                if (moveToWorldPos)
                {
                    assemblyComponent.X = srcFixtureComponent.MetaWorldX.Value;
                    assemblyComponent.Y = srcFixtureComponent.MetaWorldY.Value;
                    assemblyComponent.Z = srcFixtureComponent.MetaWorldZ.Value;
                }
                assemblyComponent.Slope = srcFixtureComponent.Slope;
                assemblyComponent.Angle = srcFixtureComponent.Angle;
                assemblyComponent.Roll = srcFixtureComponent.Roll;
                this.AssemblyModel.Components.Add(assemblyComponent);
            }
            else
            {
                PlanogramAssemblyComponent assemblyComponentCopy = src.AssemblyComponentModel.Copy();
                assemblyComponentCopy.PlanogramComponentId = componentCopy.Id;
                if (moveToWorldPos)
                {
                    assemblyComponentCopy.X = src.FixtureComponentModel.MetaWorldX.Value;
                    assemblyComponentCopy.Y = src.FixtureComponentModel.MetaWorldY.Value;
                    assemblyComponentCopy.Z = src.FixtureComponentModel.MetaWorldZ.Value;
                }
                this.AssemblyModel.Components.Add(assemblyComponentCopy);
            }

            PlanogramComponentView view = this.Components.Last();
            view.IsSelectable = src.IsSelectable;

            Int32 componentNum = view.Planogram.Model.Components.Where(c => c.ComponentType == src.ComponentType).Count() + 1;
            view.Name = String.Format("{0} {1}", PlanogramComponentTypeHelper.FriendlyNames[src.ComponentType], componentNum);


            //copy subcomponents
            foreach (PlanogramSubComponentView sub in src.SubComponents)
            {
                view.AddSubComponentCopy(sub, copyPositions);
            }


            return view;
        }

        /// <summary>
        /// Removes the component from this assembly
        /// </summary>
        /// <param name="componentView"></param>
        public void RemoveComponent(PlanogramComponentView componentView)
        {
            if (this.Components.Contains(componentView))
            {
                //completely remove this component from the plan
                Boolean update = !this.Planogram.IsRecordingUndoableAction;
                if(update) this.Planogram.BeginUndoableAction();

                this.Planogram.Model.Components.RemoveComponentAndDependants(componentView.ComponentModel);
                //AssemblyModel.Components.Remove(componentView.AssemblyComponentModel);

                if (update) this.Planogram.EndUndoableAction();
            }
        }

        /// <summary>
        /// Ungroups this assembly moving components back up to the fixture
        /// </summary>
        /// <returns>The list of newly created fixture components.</returns>
        public List<PlanogramComponentView> Ungroup()
        {
            List<PlanogramComponentView> components = new List<PlanogramComponentView>(this.Components.Count);

            PlanogramFixtureView parentFixture = this.Fixture;

            //copy the components to the parent fixture
            foreach (PlanogramComponentView oldComponent in this.Components.ToList())
            {
                PlanogramComponentView newComp = parentFixture.AddComponentCopy(oldComponent);

                //reset the position
                newComp.X += this.X;
                newComp.Y += this.Y;
                newComp.Z += this.Z;

                newComp.IsSelectable = true;

                components.Add(newComp);
            }

            //remove the assembly.
            parentFixture.RemoveAssembly(this);

            return components;
        }

        /// <summary>
        /// Resolves the given expression for this item
        /// </summary>
        public Object GetCalculatedValue(String expressionText)
        {
            return PlanogramFieldHelper.ResolveFieldExpression(expressionText,
                PlanogramItemPlacement.NewPlanogramItemPlacement(
                assembly: this.AssemblyModel,
                fixtureAssembly: this.FixtureAssemblyModel,
                fixture: this.Fixture.FixtureModel,
                fixtureItem: this.Fixture.FixtureItemModel,
                planogram: this.Planogram.Model
                ));
        }

        /// <summary>
        /// Called by the parent planogram to notify that processing has completed
        /// </summary>
        internal void NotifyPlanProcessCompleting()
        {
            //if we have calculated values cached then update them now.
            if (_calculatedValueResolver != null) _calculatedValueResolver.RefreshValues();

            //notify all child views
            foreach (var component in this.Components)
            {
                component.NotifyPlanProcessCompleting();
            }
        }

        #endregion

        #region IPlanItem Members

        public PlanogramView Planogram
        {
            get { return _parentFixtureView.Planogram; }
        }

        public PlanogramFixtureView Fixture
        {
            get { return _parentFixtureView; }
        }

        PlanogramAssemblyView IPlanItem.Assembly
        {
            get { return this; }
        }

        PlanogramComponentView IPlanItem.Component
        {
            get { return null; }
        }

        PlanogramSubComponentView IPlanItem.SubComponent
        {
            get { return null; }
        }

        PlanogramPositionView IPlanItem.Position
        {
            get { return null; }
        }

        PlanogramProductView IPlanItem.Product
        {
            get { return null; }
        }

        PlanogramAnnotationView IPlanItem.Annotation
        {
            get { return null; }
        }

        PlanItemType IPlanItem.PlanItemType
        {
            get { return Common.PlanItemType.Assembly; }
        }

        public Boolean IsSelectable { get; set; }

        #endregion

        #region Static Helpers

        /// <summary>
        /// Returns the list of plan item fields for this plan item.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDefaultPlanItemFields()
        {
            return EnumerateDisplayableFields();
        }

        #endregion

        #region IDisposable

        protected override void OnDisposing(bool disposing)
        {
            if (_calculatedValueResolver != null) _calculatedValueResolver.Dispose();
            base.OnDisposing(disposing);
        }

        #endregion
    }

}
