﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion
#endregion

using System;
using System.ComponentModel;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Simple base class to allow for use of model and model changed concepts with non csla objects.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class ViewModelObjectView<T> : INotifyPropertyChanged
    {
        #region Fields
        private T _model;
        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the model.
        /// </summary>
        public T Model
        {
            get { return _model; }
            set
            {
                SetModel(value, ViewStateObjectModelChangeReason.Direct);
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Called when the model changes.
        /// </summary>
        public event EventHandler<ViewStateObjectModelChangedEventArgs<T>> ModelChanged;

        #endregion

        #region Constructor

        public ViewModelObjectView()
        {

        }

        #endregion

        #region Methods

        protected void SetModel(T newModel, ViewStateObjectModelChangeReason reason)
        {
            T oldModel = _model;

            _model = newModel;
            OnPropertyChanged("Model");

            if (ModelChanged != null)
            {
                ModelChanged(this, new ViewStateObjectModelChangedEventArgs<T>(reason, newModel, oldModel, null));
            }
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
