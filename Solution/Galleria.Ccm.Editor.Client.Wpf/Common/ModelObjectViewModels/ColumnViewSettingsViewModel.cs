﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Windows;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.Common.ModelObjectViewModels
{
    public class CustomColumnLayoutViewModel : ViewModelAttachedControlObject<CustomColumnLayoutDialog>
    {
        #region Properties

        #region AvailableColumns property

        public static readonly PropertyPath AvailableColumnsProperty = GetPropertyPath(o => o.AvailableColumns);

        private ObservableCollection<CustomColumn> _availableColumns;

        /// <summary>
        ///     Gets or sets the available columns observable collection.
        /// </summary>
        public ObservableCollection<CustomColumn> AvailableColumns
        {
            get { return _availableColumns; }
            set
            {
                _availableColumns = value;
                OnPropertyChanged(AvailableColumnsProperty);
            }
        }

        #endregion

        #region AvailableFilterByColumns property

        public static readonly PropertyPath AvailableFilterByColumnsProperty =
            GetPropertyPath(o => o.AvailableFilterByColumns);

        private ObservableCollection<CustomColumn> _availableFilterByColumns;

        /// <summary>
        ///     Gets or sets the available filter by columns collection.
        /// </summary>
        public ObservableCollection<CustomColumn> AvailableFilterByColumns
        {
            get { return _availableFilterByColumns; }
            set
            {
                _availableFilterByColumns = value;
                OnPropertyChanged(AvailableFilterByColumnsProperty);
            }
        }

        #endregion

        #region CurrentView property

        public static readonly PropertyPath CurrentViewSettingsProperty = GetPropertyPath(p => p.CurrentLayout);

        private CustomColumnLayout _currentLayout;

        /// <summary>
        ///     Gets or sets the current view settings.
        /// </summary>
        public CustomColumnLayout CurrentLayout
        {
            get { return _currentLayout; }
            set
            {
                _currentLayout = value;
                OnPropertyChanged(CurrentViewSettingsProperty);
                _isViewChanged = false;
            }
        }

        #endregion

        #region FilterBy properties

        #region IsFilterRemoved property

        public static readonly PropertyPath IsFilterRemovedProperty = GetPropertyPath(o => o.IsFilterRemoved);

        private Boolean _isFilterRemoved;

        public Boolean IsFilterRemoved
        {
            get { return _isFilterRemoved; }
            set
            {
                _isFilterRemoved = value;
                OnPropertyChanged(IsFilterRemovedProperty);
            }
        }

        #endregion

        #region SelectedNewFilterColumn property

        public static readonly PropertyPath SelectedNewFilterColumnProperty =
            GetPropertyPath(o => o.SelectedNewFilterColumn);

        private CustomColumn _selectedFilterColumn;

        /// <summary>
        ///     Gets or sets the selected filter column.
        /// </summary>
        public CustomColumn SelectedNewFilterColumn
        {
            get { return _selectedFilterColumn; }
            set
            {
                _selectedFilterColumn = value;
                OnPropertyChanged(SelectedNewFilterColumnProperty);
            }
        }

        #endregion

        #endregion

        #region GroupBy properties

        #region LevelOne properties

        #region AvailableColumns property

        public static readonly PropertyPath GroupByLevelOneAvailableColumnsProperty =
            GetPropertyPath(o => o.GroupBylevelOneAvailableColumns);

        private ObservableCollection<CustomColumn> _groupBylevelOneAvailableColumns;

        /// <summary>
        ///     Gets or sets the collection containing the available group by level four columns.
        /// </summary>
        public ObservableCollection<CustomColumn> GroupBylevelOneAvailableColumns
        {
            get { return _groupBylevelOneAvailableColumns; }
            set
            {
                _groupBylevelOneAvailableColumns = value;
                OnPropertyChanged(GroupByLevelOneAvailableColumnsProperty);
            }
        }

        #endregion

        #region SelectedColumn property

        public static readonly PropertyPath GroupByLevelOneSelectedColumnProperty =
            GetPropertyPath(o => o.GroupByLevelOneSelectedColumn);

        private String _groupByLevelOneSelectedColumn;

        /// <summary>
        ///     Gets or sets the name of the group by level four selected column.
        /// </summary>
        public String GroupByLevelOneSelectedColumn
        {
            get { return _groupByLevelOneSelectedColumn; }
            set
            {
                _groupByLevelOneSelectedColumn = value;
                OnPropertyChanged(GroupByLevelOneSelectedColumnProperty);
            }
        }

        #endregion

        #endregion

        #region LevelTwo properties

        #region AvailableColumns property

        public static readonly PropertyPath GroupByLevelTwoAvailableColumnsProperty =
            GetPropertyPath(o => o.GroupByLevelTwoAvailableColumns);

        private ObservableCollection<CustomColumn> _groupByLevelTwoAvailableColumns;

        /// <summary>
        ///     Gets or sets the collection containing the available group by level two columns.
        /// </summary>
        public ObservableCollection<CustomColumn> GroupByLevelTwoAvailableColumns
        {
            get { return _groupByLevelTwoAvailableColumns; }
            set
            {
                _groupByLevelTwoAvailableColumns = value;
                OnPropertyChanged(GroupByLevelThreeAvailableColumnsProperty);
            }
        }

        #endregion

        #region SelectedColumn property

        public static readonly PropertyPath GroupByLevelTwoSelectedColumnProperty =
            GetPropertyPath(o => o.GroupByLevelTwoSelectedColumn);

        private string _groupByLevelTwoSelectedColumn;

        /// <summary>
        ///     Gets or sets the name of the group by level two selected column.
        /// </summary>
        public String GroupByLevelTwoSelectedColumn
        {
            get { return _groupByLevelTwoSelectedColumn; }
            set
            {
                _groupByLevelTwoSelectedColumn = value;
                OnPropertyChanged(GroupByLevelTwoSelectedColumnProperty);
            }
        }

        #endregion

        #endregion

        #region LevelThree properties

        #region AvailableColumns property

        public static readonly PropertyPath GroupByLevelThreeAvailableColumnsProperty =
            GetPropertyPath(o => o.GroupByLevelThreeAvailableColumns);

        private ObservableCollection<CustomColumn> _groupByLevelThreeAvailableColumns;

        /// <summary>
        ///     Gets or sets the collection containing the available group by level three columns.
        /// </summary>
        public ObservableCollection<CustomColumn> GroupByLevelThreeAvailableColumns
        {
            get { return _groupByLevelThreeAvailableColumns; }
            set
            {
                _groupByLevelThreeAvailableColumns = value;
                OnPropertyChanged(GroupByLevelThreeAvailableColumnsProperty);
            }
        }

        #endregion

        #region SelectedColumn property

        public static readonly PropertyPath GroupBylevelThreeSelectedColumnProperty =
            GetPropertyPath(o => o.GroupByLevelThreeSelectedColumn);

        private String _groupByLevelThreeSelectedColumn;

        /// <summary>
        ///     Gets or sets the name of the group by level three selected column.
        /// </summary>
        public String GroupByLevelThreeSelectedColumn
        {
            get { return _groupByLevelThreeSelectedColumn; }
            set
            {
                _groupByLevelThreeSelectedColumn = value;
                OnPropertyChanged(GroupBylevelThreeSelectedColumnProperty);
            }
        }

        #endregion

        #endregion

        #region LevelFour properties

        #region AvailableColumns property

        public static readonly PropertyPath GroupByLevelFourAvailableColumnsProperty =
            GetPropertyPath(o => o.GroupByLevelFourAvailableColumns);

        private ObservableCollection<CustomColumn> _groupByLevelFourAvailableColumns;

        /// <summary>
        ///     Gets or sets the name of the group by level four selected column.
        /// </summary>
        public ObservableCollection<CustomColumn> GroupByLevelFourAvailableColumns
        {
            get { return _groupByLevelFourAvailableColumns; }
            set
            {
                _groupByLevelFourAvailableColumns = value;
                OnPropertyChanged(GroupByLevelFourAvailableColumnsProperty);
            }
        }

        #endregion

        #region SelectedColumn property

        public static readonly PropertyPath GroupBylevelFourSelectedColumnProperty =
            GetPropertyPath(o => o.GroupByLevelFourSelectedColumn);

        private String _groupByLevelFourSelectedColumn;

        /// <summary>
        ///     Gets or sets the name of the group by level four selected column.
        /// </summary>
        public String GroupByLevelFourSelectedColumn
        {
            get { return _groupByLevelFourSelectedColumn; }
            set
            {
                _groupByLevelFourSelectedColumn = value;
                OnPropertyChanged(GroupBylevelFourSelectedColumnProperty);
            }
        }

        #endregion

        #endregion

        #endregion

        #region OriginalViewSettings property

        public static readonly PropertyPath OriginalViewSettingsProperty = GetPropertyPath(p => p.OriginalLayout);

        private CustomColumnLayout _originalLayout;

        /// <summary>
        ///     Gets or sets the original view settings.
        /// </summary>
        public CustomColumnLayout OriginalLayout
        {
            get { return _originalLayout; }
            set
            {
                _originalLayout = value;
                OnPropertyChanged(OriginalViewSettingsProperty);
                _isViewChanged = false;
            }
        }

        #endregion

        #region SortBy properties

        #region LevelOne properties

        #region AvailableColumns property

        public static readonly PropertyPath SortByLevelOneAvailableColumnsProperty =
            GetPropertyPath(o => o.SortBylevelOneAvailableColumns);

        private ObservableCollection<CustomColumn> _sortBylevelOneAvailableColumns;

        /// <summary>
        ///     Gets or sets the collection containing the available group by level four columns.
        /// </summary>
        public ObservableCollection<CustomColumn> SortBylevelOneAvailableColumns
        {
            get { return _sortBylevelOneAvailableColumns; }
            set
            {
                _sortBylevelOneAvailableColumns = value;
                OnPropertyChanged(SortByLevelOneAvailableColumnsProperty);
            }
        }

        #endregion

        #region SelectedColumn property

        public static readonly PropertyPath SortByLevelOneSelectedColumnProperty =
            GetPropertyPath(o => o.SortByLevelOneSelectedColumn);

        private String _sortByLevelOneSelectedColumn;

        /// <summary>
        ///     Gets or sets the name of the group by level four selected column.
        /// </summary>
        public String SortByLevelOneSelectedColumn
        {
            get { return _sortByLevelOneSelectedColumn; }
            set
            {
                _sortByLevelOneSelectedColumn = value;
                OnPropertyChanged(SortByLevelOneSelectedColumnProperty);
            }
        }

        #endregion

        #region Ascending property

        public static readonly PropertyPath SortByLevelOneAscendingProperty =
            GetPropertyPath(o => o.SortByLevelOneAscending);

        private bool _sortByLevelOneAscending;

        /// <summary>
        ///     Gets or sets whether level four is sorted in ascending order.
        /// </summary>
        public Boolean SortByLevelOneAscending
        {
            get { return _sortByLevelOneAscending; }
            set
            {
                _sortByLevelOneAscending = value;
                _isViewChanged = true;
                OnPropertyChanged(SortByLevelOneAscendingProperty);
            }
        }

        #endregion

        #region Descending Property

        public static readonly PropertyPath SortByLevelOneDescendingProperty =
            GetPropertyPath(o => o.SortByLevelOneDescending);

        private bool _sortByLevelOneDescending;

        /// <summary>
        ///     Gets or sets whether level four is sorted in descending order.
        /// </summary>
        public Boolean SortByLevelOneDescending
        {
            get { return _sortByLevelOneDescending; }
            set
            {
                _sortByLevelOneDescending = value;
                _isViewChanged = true;
                OnPropertyChanged(SortByLevelOneDescendingProperty);
            }
        }

        #endregion

        #endregion

        #region LevelTwo properties

        #region AvailableColumns property

        public static readonly PropertyPath SortByLevelTwoAvailableColumnsProperty =
            GetPropertyPath(o => o.SortByLevelTwoAvailableColumns);

        private ObservableCollection<CustomColumn> _sortByLevelTwoAvailableColumns;

        /// <summary>
        ///     Gets or sets the collection containing the available group by level two columns.
        /// </summary>
        public ObservableCollection<CustomColumn> SortByLevelTwoAvailableColumns
        {
            get { return _sortByLevelTwoAvailableColumns; }
            set
            {
                _sortByLevelTwoAvailableColumns = value;
                OnPropertyChanged(SortByLevelThreeAvailableColumnsProperty);
            }
        }

        #endregion

        #region SelectedColumn property

        public static readonly PropertyPath SortByLevelTwoSelectedColumnProperty =
            GetPropertyPath(o => o.SortByLevelTwoSelectedColumn);

        private string _sortByLevelTwoSelectedColumn;

        /// <summary>
        ///     Gets or sets the name of the group by level two selected column.
        /// </summary>
        public String SortByLevelTwoSelectedColumn
        {
            get { return _sortByLevelTwoSelectedColumn; }
            set
            {
                _sortByLevelTwoSelectedColumn = value;
                OnPropertyChanged(SortByLevelTwoSelectedColumnProperty);
            }
        }

        #endregion

        #region Ascending property

        public static readonly PropertyPath SortByLevelTwoAscendingProperty =
            GetPropertyPath(o => o.SortByLevelTwoAscending);

        private bool _sortByLevelTwoAscending;

        /// <summary>
        ///     Gets or sets whether level two is sorted in ascending order.
        /// </summary>
        public Boolean SortByLevelTwoAscending
        {
            get { return _sortByLevelTwoAscending; }
            set
            {
                _sortByLevelTwoAscending = value;
                _isViewChanged = true;
                OnPropertyChanged(SortByLevelTwoAscendingProperty);
            }
        }

        #endregion

        #region Descending Property

        public static readonly PropertyPath SortByLevelTwoDescendingProperty =
            GetPropertyPath(o => o.SortByLevelTwoDescending);

        private bool _sortByLevelTwoDescending;

        /// <summary>
        ///     Gets or sets whether level two is sorted in descending order.
        /// </summary>
        public Boolean SortByLevelTwoDescending
        {
            get { return _sortByLevelTwoDescending; }
            set
            {
                _sortByLevelTwoDescending = value;
                _isViewChanged = true;
                OnPropertyChanged(SortByLevelTwoDescendingProperty);
            }
        }

        #endregion

        #endregion

        #region LevelThree properties

        #region AvailableColumns property

        public static readonly PropertyPath SortByLevelThreeAvailableColumnsProperty =
            GetPropertyPath(o => o.SortByLevelThreeAvailableColumns);

        private ObservableCollection<CustomColumn> _sortByLevelThreeAvailableColumns;

        /// <summary>
        ///     Gets or sets the collection containing the available group by level three columns.
        /// </summary>
        public ObservableCollection<CustomColumn> SortByLevelThreeAvailableColumns
        {
            get { return _sortByLevelThreeAvailableColumns; }
            set
            {
                _sortByLevelThreeAvailableColumns = value;
                OnPropertyChanged(SortByLevelThreeAvailableColumnsProperty);
            }
        }

        #endregion

        #region SelectedColumn property

        public static readonly PropertyPath SortByLevelThreeSelectedColumnProperty =
            GetPropertyPath(o => o.SortByLevelThreeSelectedColumn);

        private String _sortByLevelThreeSelectedColumn;

        /// <summary>
        ///     Gets or sets the name of the group by level three selected column.
        /// </summary>
        public String SortByLevelThreeSelectedColumn
        {
            get { return _sortByLevelThreeSelectedColumn; }
            set
            {
                _sortByLevelThreeSelectedColumn = value;
                OnPropertyChanged(SortByLevelThreeSelectedColumnProperty);
            }
        }

        #endregion

        #region Ascending property

        public static readonly PropertyPath SortByLevelThreeAscendingProperty =
            GetPropertyPath(o => o.SortByLevelThreeAscending);

        private bool _sortByLevelThreeAscending;

        /// <summary>
        ///     Gets or sets whether level three is sorted in ascending order.
        /// </summary>
        public Boolean SortByLevelThreeAscending
        {
            get { return _sortByLevelThreeAscending; }
            set
            {
                _sortByLevelThreeAscending = value;
                _isViewChanged = true;
                OnPropertyChanged(SortByLevelThreeAscendingProperty);
            }
        }

        #endregion

        #region Descending Property

        public static readonly PropertyPath SortByLevelThreeDescendingProperty =
            GetPropertyPath(o => o.SortByLevelThreeDescending);

        private bool _sortByLevelThreeDescending;

        /// <summary>
        ///     Gets or sets whether level three is sorted in descending order.
        /// </summary>
        public Boolean SortByLevelThreeDescending
        {
            get { return _sortByLevelThreeDescending; }
            set
            {
                _sortByLevelThreeDescending = value;
                _isViewChanged = true;
                OnPropertyChanged(SortByLevelThreeDescendingProperty);
            }
        }

        #endregion

        #endregion

        #region LevelFour properties

        #region AvailableColumns property

        public static readonly PropertyPath SortByLevelFourAvailableColumnsProperty =
            GetPropertyPath(o => o.SortByLevelFourAvailableColumns);

        private ObservableCollection<CustomColumn> _sortByLevelFourAvailableColumns;

        /// <summary>
        ///     Gets or sets the name of the group by level four selected column.
        /// </summary>
        public ObservableCollection<CustomColumn> SortByLevelFourAvailableColumns
        {
            get { return _sortByLevelFourAvailableColumns; }
            set
            {
                _sortByLevelFourAvailableColumns = value;
                OnPropertyChanged(SortByLevelFourAvailableColumnsProperty);
            }
        }

        #endregion

        #region SelectedColumn property

        public static readonly PropertyPath SortByLevelFourSelectedColumnProperty =
            GetPropertyPath(o => o.SortByLevelFourSelectedColumn);

        private String _sortByLevelFourSelectedColumn;

        /// <summary>
        ///     Gets or sets the name of the group by level four selected column.
        /// </summary>
        public String SortByLevelFourSelectedColumn
        {
            get { return _sortByLevelFourSelectedColumn; }
            set
            {
                _sortByLevelFourSelectedColumn = value;
                OnPropertyChanged(SortByLevelFourSelectedColumnProperty);
            }
        }

        #endregion

        #region Ascending property

        public static readonly PropertyPath SortByLevelFourAscendingProperty =
            GetPropertyPath(o => o.SortByLevelFourAscending);

        private bool _sortByLevelFourAscending;

        /// <summary>
        ///     Gets or sets whether level four is sorted in ascending order.
        /// </summary>
        public Boolean SortByLevelFourAscending
        {
            get { return _sortByLevelFourAscending; }
            set
            {
                _sortByLevelFourAscending = value;
                _isViewChanged = true;
                OnPropertyChanged(SortByLevelFourAscendingProperty);
            }
        }

        #endregion

        #region Descending Property

        public static readonly PropertyPath SortByLevelFourDescendingProperty =
            GetPropertyPath(o => o.SortByLevelFourDescending);

        private bool _sortByLevelFourDescending;

        /// <summary>
        ///     Gets or sets whether level four is sorted in descending order.
        /// </summary>
        public Boolean SortByLevelFourDescending
        {
            get { return _sortByLevelFourDescending; }
            set
            {
                _sortByLevelFourDescending = value;
                _isViewChanged = true;
                OnPropertyChanged(SortByLevelFourDescendingProperty);
            }
        }

        #endregion

        #endregion

        #endregion

        #endregion

        #region Commands

        #region AddAllColumns command

        public static readonly PropertyPath AddAllColumnsCommandPath =
            WpfHelper.GetPropertyPath<CustomColumnLayoutViewModel>(o => o.AddAllColumnsCommand);

        private RelayCommand _addAllColumnsCommand;

        /// <summary>
        ///     Adds all available columns to the current view column collection.
        /// </summary>
        public RelayCommand AddAllColumnsCommand
        {
            get
            {
                if (_addAllColumnsCommand != null) return _addAllColumnsCommand;

                _addAllColumnsCommand = new RelayCommand(o => AddAllColumns_Executed(), o => AddAllColumns_CanExecute())
                {
                    FriendlyDescription = Message.GridViewSettings_AddAllColumns,
                    SmallIcon = ImageResources.TODO,
                    DisabledReason = Message.GridViewSettings_AddAllColumns_DisabledReason
                };
                ViewModelCommands.Add(_addAllColumnsCommand);

                return _addAllColumnsCommand;
            }
        }

        private Boolean AddAllColumns_CanExecute()
        {
            return AvailableColumns.Count > 0;
        }

        private void AddAllColumns_Executed()
        {
            SelectedColumns.AddList(AvailableColumns.ToList());
            ReorderSelectedColumns();
        }

        #endregion

        #region AddSelectedColumns command

        public static readonly PropertyPath AddSelectedColumnsCommandPath =
            WpfHelper.GetPropertyPath<CustomColumnLayoutViewModel>(o => o.AddSelectedColumnsCommand);

        private RelayCommand _addSelectedColumnsCommand;

        /// <summary>
        ///     Adds the selected columns from the Available Columns collection to the Selected Columns collection.
        /// </summary>
        public RelayCommand AddSelectedColumnsCommand
        {
            get
            {
                if (_addSelectedColumnsCommand != null) return _addSelectedColumnsCommand;

                _addSelectedColumnsCommand = new RelayCommand(o => AddSelectedColumns_Executed(),
                    o => AddSelectedColumns_CanExecute())
                {
                    FriendlyDescription = Message.GridViewSettings_AddSelectedColumns,
                    SmallIcon = ImageResources.TODO,
                    DisabledReason = Message.GridViewSettings_AddSelectedColumns_DisabledReason
                };

                ViewModelCommands.Add(_addSelectedColumnsCommand);
                return _addSelectedColumnsCommand;
            }
        }

        private Boolean AddSelectedColumns_CanExecute()
        {
            return SelectedAvailableColumns.Count > 0;
        }

        private void AddSelectedColumns_Executed()
        {
            SelectedColumns.AddList(SelectedAvailableColumns.ToList());
            ReorderSelectedColumns();
        }

        #endregion

        #region MoveColumnDown command

        public static readonly PropertyPath MoveColumnDownCommandPath = GetPropertyPath(o => o.MoveColumnDownCommand);

        private RelayCommand _moveColumnDownCommand;

        /// <summary>
        ///     Moves the selected column down one position in the selected columns rank.
        /// </summary>
        public RelayCommand MoveColumnDownCommand
        {
            get
            {
                if (_moveColumnDownCommand != null) return _moveColumnDownCommand;

                _moveColumnDownCommand = new RelayCommand(o => MoveColumnDown_Executed(),
                    o => MoveColumnDown_CanExecute())
                {
                    FriendlyDescription = Message.GridViewSettings_MoveColumnDown,
                    SmallIcon = ImageResources.TODO,
                    DisabledReason = Message.GridViewSettings_MoveColumnDown_DisabledReason
                };
                ViewModelCommands.Add(_moveColumnDownCommand);

                return _moveColumnDownCommand;
            }
        }

        private Boolean MoveColumnDown_CanExecute()
        {
            return IsValidMoveInList(SelectedViewColumns, MoveColumnDownCommand, SelectedColumns, false);
        }

        private void MoveColumnDown_Executed()
        {
            MoveInList(SelectedViewColumns, SelectedColumns, false);
        }

        #endregion

        #region MoveColumnUp command

        public static readonly PropertyPath MoveColumnUpCommandPath = GetPropertyPath(o => o.MoveColumnUpCommand);

        private RelayCommand _moveColumnUpCommand;

        /// <summary>
        ///     Moves the selected column up one position in the selected columns rank.
        /// </summary>
        public RelayCommand MoveColumnUpCommand
        {
            get
            {
                if (_moveColumnUpCommand != null) return _moveColumnUpCommand;

                _moveColumnUpCommand = new RelayCommand(o => MoveColumnUp_Executed(), o => MoveColumnUp_CanExecute())
                {
                    FriendlyDescription = Message.GridViewSettings_MoveColumnUp,
                    SmallIcon = ImageResources.TODO,
                    DisabledReason = Message.GridViewSettings_MoveColumnUp_DisabledReason
                };
                ViewModelCommands.Add(_moveColumnUpCommand);

                return _moveColumnUpCommand;
            }
        }

        private Boolean MoveColumnUp_CanExecute()
        {
            return IsValidMoveInList(SelectedViewColumns, MoveColumnDownCommand, SelectedColumns, true);
        }

        private void MoveColumnUp_Executed()
        {
            MoveInList(SelectedViewColumns, SelectedColumns, true);
        }

        #endregion

        #region RemoveAllColumns command

        public static readonly PropertyPath RemoveAllColumnsCommandPath = GetPropertyPath(o => o.RemoveAllColumnsCommand);

        private RelayCommand _removeAllColumnsCommand;

        /// <summary>
        ///     Removes all columns from the current view column list.
        /// </summary>
        public RelayCommand RemoveAllColumnsCommand
        {
            get
            {
                if (_removeAllColumnsCommand != null) return _removeAllColumnsCommand;

                _removeAllColumnsCommand = new RelayCommand(o => RemoveAllColumns_Executed(),
                    o => RemoveAllColumns_CanExecute())
                {
                    FriendlyDescription = Message.GridViewSettings_RemoveAllColumns,
                    SmallIcon = ImageResources.TODO,
                    DisabledReason = Message.GridViewSettings_RemoveAllColumns_DisabledReason
                };
                ViewModelCommands.Add(_removeAllColumnsCommand);

                return _removeAllColumnsCommand;
            }
        }

        private Boolean RemoveAllColumns_CanExecute()
        {
            return SelectedColumns.Count > 0;
        }

        private void RemoveAllColumns_Executed()
        {
            SelectedColumns.RemoveList(SelectedColumns.ToList());
            ReorderSelectedColumns();
        }

        #endregion

        #region RemoveFilter command

        public static readonly PropertyPath RemoveFilterCommandPath = GetPropertyPath(o => o.RemoveFilterCommand);

        private RelayCommand _removeFilterCommand;

        /// <summary>
        ///     Removes an existing filter from a view
        /// </summary>
        public RelayCommand RemoveFilterCommand
        {
            get
            {
                if (_removeFilterCommand == null) return _removeFilterCommand;

                _removeFilterCommand = new RelayCommand(p => RemoveFilter_Executed(), p => RemoveFilter_CanExecute())
                {
                    FriendlyName = Message.GridViewSettings_RemoveFilter,
                    FriendlyDescription = Message.GridViewSettings_RemoveFilter_Tooltip,
                    SmallIcon = ImageResources.ViewSettings_RemoveFilter_16
                };
                ViewModelCommands.Add(_removeFilterCommand);

                return _removeFilterCommand;
            }
        }

        private bool RemoveFilter_CanExecute()
        {
            return (SelectedFilterItem != null);
        }

        private void RemoveFilter_Executed()
        {
            //Take copy of the selected filter
            var selectedFilter = SelectedFilterItem;

            //Remove filter
            CurrentLayout.FilterList.Remove(selectedFilter);

            //Get corresponding column
            var column = CurrentLayout.Columns.FirstOrDefault(p => p.Path == selectedFilter.Path);

            if (column != null)
            {
                //Add back to available list
                //if (column.Path != "NotificationDataUpdate." + NotificationDataUpdate.FollowItemProperty.Name) // ensure that this doesn not show in filter options
                //{
                AvailableFilterByColumns.Add(column);
                //}
                SelectedNewFilterColumn = AvailableFilterByColumns.FirstOrDefault();
            }
            _isFilterRemoved = true;
        }

        #endregion

        #region RemoveSelectedColumns command

        public static readonly PropertyPath RemoveSelectedColumnsCommandPath =
            GetPropertyPath(o => o.RemoveSelectedColumnsCommand);

        private RelayCommand _removeSelectedColumnsCommand;

        public RelayCommand RemoveSelectedColumnsCommand
        {
            get
            {
                if (_removeSelectedColumnsCommand == null) return _removeSelectedColumnsCommand;

                _removeSelectedColumnsCommand = new RelayCommand(o => RemoveSelectedColumns_Execute(),
                    o => RemoveSelectedColumns_CanExecute())
                {
                    FriendlyDescription = Message.GridViewSettings_RemoveSelectedColumns,
                    SmallIcon = ImageResources.TODO,
                    DisabledReason = Message.GridViewSettings_RemoveSelectedColumns_DisabledReason
                };
                ViewModelCommands.Add(_removeSelectedColumnsCommand);

                return _removeSelectedColumnsCommand;
            }
        }

        private Boolean RemoveSelectedColumns_CanExecute()
        {
            return SelectedColumns.Count > 0;
        }

        private void RemoveSelectedColumns_Execute()
        {
            SelectedColumns.RemoveList(SelectedViewColumns.ToList());
            ReorderSelectedColumns();
        }

        #endregion

        #region Save command

        public static readonly PropertyPath SaveCommandPath = GetPropertyPath(o => o.SaveCommand);

        private RelayCommand _saveCommand;

        /// <summary>
        ///     Saves the current grid view settings
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null) return _saveCommand;

                _saveCommand = new RelayCommand(p => Save_Executed(), p => Save_CanExecute())
                {
                    FriendlyName = Message.Generic_Save,
                    FriendlyDescription = Message.Generic_Save_Tooltip,
                    SmallIcon = ImageResources.Save_32,
                    DisabledReason = Message.Generic_Save_DisabledReasonInvalidData
                };
                ViewModelCommands.Add(_saveCommand);

                return _saveCommand;
            }
        }

        private void BuildGroupByList()
        {
            CurrentLayout.GroupList.Clear();
            CurrentLayout.GroupList.AddList(CustomColumnGroupList.NewCustomColumnGroupList());
        }

        /// <summary>
        ///     Build the list of sort by columns from the user's selection.
        /// </summary>
        /// <remarks>Once an empty SelectedColumn is found, no more columns in the list are processed.</remarks>
        private void BuildSortByList()
        {
            CurrentLayout.SortList.Clear();
            CurrentLayout.SortList.AddList(CustomColumnSortList.NewCustomColumnSortList());
        }

        private bool Save_CanExecute()
        {
            return (CurrentLayout != null) &&
                   (CurrentLayout.IsDirty || _isViewChanged) &&
                   (CurrentLayout.IsValid);
        }

        private void Save_Executed()
        {
            if (!ValidateName()) return;
            BuildGroupByList();
            BuildSortByList();

            try
            {
                OriginalLayout = CurrentLayout.Save();
                CurrentLayout = OriginalLayout.Clone();
                _isViewChanged = false;
            }
            catch (Exception)
            {
                //TODO Catch errors and handle them
                throw;
            }
        }

        private bool ValidateName()
        {
            throw new NotImplementedException();
        }

        #endregion

        #endregion

        public static PropertyPath SelectedColumnsProperty = GetPropertyPath(o => o.SelectedColumns);

        public static readonly PropertyPath SelectedFilterItemProperty = GetPropertyPath(p => p.SelectedFilterItem);

        public static PropertyPath SelectedFiltersProperty;

        public static readonly PropertyPath SelectedViewColumnsProperty = GetPropertyPath(o => o.SelectedViewColumns);

        /// <summary>
        ///     Whether any content of the view has been changed.
        /// </summary>
        private Boolean _isViewChanged;

        private ObservableCollection<CustomColumn> _selectedAvailableColumns;
        private CustomColumnList _selectedColumns;
        private CustomColumnFilter _selectedFilterItem;
        private ObservableCollection<CustomColumn> _selectedViewColumns;

        public CustomColumnLayoutViewModel(CustomColumnLayout layout,
            ReadOnlyObservableCollection<CustomColumnLayout> availableGridViewSettings,
            Boolean isEdit)
        {
        }

        public ObservableCollection<CustomColumn> SelectedAvailableColumns
        {
            get { return _selectedAvailableColumns; }
            set
            {
                _selectedAvailableColumns = value;
                OnPropertyChanged(SelectedAvailableColumnsProperty);
            }
        }

        /// <summary>
        ///     Gets or Sets the selected coulumns collection.
        /// </summary>
        public CustomColumnList SelectedColumns
        {
            get { return _selectedColumns; }
            set
            {
                _selectedColumns = value;
                OnPropertyChanged(SelectedColumnsProperty);
            }
        }

        public ObservableCollection<CustomColumn> SelectedViewColumns
        {
            get { return _selectedViewColumns; }
            set
            {
                _selectedViewColumns = value;
                OnPropertyChanged(SelectedViewColumnsProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets the selected filter item
        /// </summary>
        public CustomColumnFilter SelectedFilterItem
        {
            get { return _selectedFilterItem; }
            set
            {
                _selectedFilterItem = value;
                OnPropertyChanged(SelectedFilterItemProperty);
            }
        }

        public static PropertyPath SelectedAvailableColumnsProperty
        {
            get { throw new NotImplementedException(); }
        }

        protected override void Dispose(bool disposing)
        {
            if (IsDisposed)
            {
                return;
            }

            if (disposing)
            {
                //TODO dispose resources.
            }
            IsDisposed = true;
        }

        #region Methods

        /// <summary>
        ///     Reassign the sorting number for each column in the selected columns collection.
        /// </summary>
        private void ReorderSelectedColumns()
        {
            var colNumber = 1;
            foreach (var column in SelectedColumns)
            {
                column.Number = colNumber;
                colNumber++;
            }
        }

        #endregion

        #region Helpers

        private static PropertyPath GetPropertyPath(Expression<Func<CustomColumnLayoutViewModel, object>> expression)
        {
            return WpfHelper.GetPropertyPath(expression);
        }

        /// <summary>
        ///     Checks if the selection can be moved inside the containing list.
        /// </summary>
        /// <param name="selection"></param>
        /// <param name="command"></param>
        /// <param name="list"></param>
        /// <param name="isMoveUp"></param>
        /// <returns></returns>
        private static bool IsValidMoveInList(ObservableCollection<CustomColumn> selection, RelayCommand command,
            IList<CustomColumn> list, Boolean isMoveUp)
        {
            var disabledReason = String.Empty;

            if (selection.Count == 0)
                disabledReason = Message.TODO; // Can't move without a selection.

            if (selection.Count != 1)
                disabledReason = Message.TODO; // Can't move more than one column at a time.

            if (list.IndexOf(selection[0]) == (isMoveUp ? 0 : list.Count - 1))
                disabledReason = Message.TODO; // Can't move if already the limit row.

            if (String.IsNullOrEmpty(disabledReason))
                return true;

            command.DisabledReason = disabledReason;
            return false;
        }

        /// <summary>
        ///     Moves the selected item in the list.
        /// </summary>
        /// <param name="selection"></param>
        /// <param name="list"></param>
        /// <param name="isMoveUp"></param>
        private void MoveInList(IList<CustomColumn> selection, ObservableCollection<CustomColumn> list,
            Boolean isMoveUp)
        {
            var currentSelection = selection[0];
            var currentIndex = list.IndexOf(currentSelection);
            var offset = isMoveUp ? -1 : 1;
            list.Move(currentIndex, currentIndex + offset);

            ReorderSelectedColumns();

            // Find the current selection in the reordered list and if found make sure it stays selected.
            currentSelection = list.FirstOrDefault(column => column.Path == currentSelection.Path);
            if (currentSelection == null) return;

            selection.Clear();
            selection.Add(currentSelection);
        }

        #endregion
    }

    public class ColumnViewSettingsFilterByItem
    {
        public static PropertyPath HeaderProperty;
        public static PropertyPath TextProperty;
    }

}