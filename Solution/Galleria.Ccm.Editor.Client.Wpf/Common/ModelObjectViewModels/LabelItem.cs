﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-24265 J.Pickup
//  Created
// CCM-24265 : N.Haywood
//  Added more properties
// V8-25734 : L.Ineson
//  Moved out rendering controls to framework.
// CCM-25897 : N.Haywood
//  Overrided equals method
// V8-26248 : L.Luong
//  Rename ToLabelItem to ToLabel for clarification
#endregion
#endregion

using Galleria.Ccm.Editor.Client.Wpf.Common.Rendering;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Interfaces;
using System;
using System.Collections.Generic;
using System.Windows;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    //TODO: Tidy up.

    /// <summary>
    /// UI view of a Label
    /// </summary>
    public sealed class LabelItem : IPlanogramLabel
    {
        #region Properties

        public Object Id { get; private set; }
        public String Name { get; private set; }
        public LabelType Type { get; private set; }
        public Int32 BackgroundColour { get; private set; }
        public Int32 BorderColour { get; private set; }
        public Int32 BorderThickness { get; private set; }
        public String Text { get; private set; }
        public Int32 TextColour { get; private set; }
        public String Font { get; private set; }
        public Int32 FontSize { get; private set; }
        public Boolean IsRotateToFitOn { get; private set; }
        public Boolean IsShrinkToFitOn { get; private set; }
        public Boolean TextBoxFontBold { get; private set; }
        public Boolean TextBoxFontItalic { get; private set; }
        public Boolean TextBoxFontUnderlined { get; private set; }
        public LabelHorizontalPlacementType LabelHorizontalPlacement { get; private set; }
        public LabelVerticalPlacementType LabelVerticalPlacement { get; private set; }
        public TextAlignment TextBoxTextAlignment { get; private set; }
        public LabelTextDirectionType TextDirection { get; private set; }
        public LabelTextWrapping TextWrapping { get; private set; }
        public Single BackgroundTransparency { get; private set; }
        public Boolean ShowOverImages { get; private set; }
        public Boolean ShowLabelPerFacing { get; private set; }
        public Boolean HasImage { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type from the given model.
        /// </summary>
        /// <param name="model"></param>
        public LabelItem(Label model)
            :this(model, model.Id)
        {
            
        }


        /// <summary>
        /// Creates a new instance of this type from the given model.
        /// </summary>
        /// <param name="model"></param>
        public LabelItem(Label model, Object itemId)
        {
            this.Id = itemId;
            this.Name = model.Name;
            this.Type = model.Type;
            this.BackgroundColour = model.BackgroundColour;
            this.BorderColour = model.BorderColour;
            this.BorderThickness = model.BorderThickness;
            this.Text = model.Text;
            this.TextColour = model.TextColour;
            this.Font = model.Font;
            this.FontSize = model.FontSize;
            this.IsRotateToFitOn = model.IsRotateToFitOn;
            this.IsShrinkToFitOn = model.IsShrinkToFitOn;
            this.TextBoxFontBold = model.TextBoxFontBold;
            this.TextBoxFontItalic = model.TextBoxFontItalic;
            this.TextBoxFontUnderlined = model.TextBoxFontUnderlined;
            this.LabelHorizontalPlacement = model.LabelHorizontalPlacement;
            this.LabelVerticalPlacement = model.LabelVerticalPlacement;
            this.TextBoxTextAlignment = model.TextBoxTextAlignment;
            this.TextDirection = model.TextDirection;
            this.TextWrapping = model.TextWrapping;
            this.BackgroundTransparency = model.BackgroundTransparency;
            this.ShowOverImages = model.ShowOverImages;
            this.ShowLabelPerFacing = model.ShowLabelPerFacing;
            this.HasImage = false;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a new Label model from this item.
        /// </summary>
        /// <returns></returns>
        public Label ToLabel()
        {
            Label model = Label.NewLabel(this.Type);

            model.Name = this.Name;
            model.Type = this.Type;
            model.BackgroundColour = this.BackgroundColour;
            model.BorderColour = this.BorderColour;
            model.BorderThickness = this.BorderThickness;
            model.Text = this.Text;
            model.IsRotateToFitOn = this.IsRotateToFitOn;
            model.TextColour = this.TextColour;
            model.Font = this.Font;
            model.FontSize = this.FontSize;
            model.IsShrinkToFitOn = this.IsShrinkToFitOn;
            model.TextBoxFontBold = this.TextBoxFontBold;
            model.TextBoxFontItalic = this.TextBoxFontItalic;
            model.TextBoxFontUnderlined = this.TextBoxFontUnderlined;
            model.LabelHorizontalPlacement = this.LabelHorizontalPlacement;
            model.LabelVerticalPlacement = this.LabelVerticalPlacement;
            model.TextBoxTextAlignment = this.TextBoxTextAlignment;
            model.TextDirection = this.TextDirection;
            model.TextWrapping = this.TextWrapping;
            model.BackgroundTransparency = this.BackgroundTransparency;
            model.ShowOverImages = this.ShowOverImages;
            model.ShowLabelPerFacing = this.ShowLabelPerFacing;

            return model;
        }

        public override bool Equals(object obj)
        {

            LabelItem otherLabel = obj as LabelItem;

            if (otherLabel == null)
            {
                return false;
            }
            if (otherLabel.Name == this.Name &&
            otherLabel.Type == this.Type &&
            otherLabel.BackgroundColour == this.BackgroundColour &&
            otherLabel.BorderColour == this.BorderColour &&
            otherLabel.BorderThickness == this.BorderThickness &&
            otherLabel.Text == this.Text &&
            otherLabel.IsRotateToFitOn == this.IsRotateToFitOn &&
            otherLabel.TextColour == this.TextColour &&
            otherLabel.Font == this.Font &&
            otherLabel.FontSize == this.FontSize &&
            otherLabel.IsShrinkToFitOn == this.IsShrinkToFitOn &&
            otherLabel.TextBoxFontBold == this.TextBoxFontBold &&
            otherLabel.TextBoxFontItalic == this.TextBoxFontItalic &&
            otherLabel.TextBoxFontUnderlined == this.TextBoxFontUnderlined &&
            otherLabel.LabelHorizontalPlacement == this.LabelHorizontalPlacement &&
            otherLabel.LabelVerticalPlacement == this.LabelVerticalPlacement &&
            otherLabel.TextBoxTextAlignment == this.TextBoxTextAlignment &&
            otherLabel.TextDirection == this.TextDirection &&
            otherLabel.TextWrapping == this.TextWrapping &&
            otherLabel.BackgroundTransparency == this.BackgroundTransparency &&
            otherLabel.ShowOverImages == this.ShowOverImages &&
            otherLabel.ShowLabelPerFacing == this.ShowLabelPerFacing)
            {
                return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }

        #endregion

        #region Static Helpers

        public static List<LabelItem> CreateViews(IEnumerable<Label> settings)
        {
            List<LabelItem> ItemList = new List<LabelItem>();

            foreach (Label setting in settings)
            {
                ItemList.Add(new LabelItem(setting));
            }

            return ItemList;
        }

        public static Boolean AreMatching(LabelItem view, Label setting)
        {
            if (view == null && setting == null) { return true; }
            if (view == null || setting == null) { return false; }

            if (view.Name != setting.Name) { return false; }
            //if (view.Type != setting.Type) { return false; }
            if (view.BackgroundColour != setting.BackgroundColour) { return false; }
            if (view.BorderColour != setting.BorderColour) { return false; }
            if (view.TextColour != setting.TextColour) { return false; }
            if (view.Text != setting.Text) { return false; }
            if (view.Font != setting.Font) { return false; }
            if (view.FontSize != setting.FontSize) { return false; }
            if (view.IsRotateToFitOn != setting.IsRotateToFitOn) { return false; }
            if (view.IsShrinkToFitOn != setting.IsShrinkToFitOn) { return false; }

            if (view.TextBoxFontBold != setting.TextBoxFontBold) { return false; }
            if (view.TextBoxFontItalic != setting.TextBoxFontItalic) { return false; }
            if (view.TextBoxFontUnderlined != setting.TextBoxFontUnderlined) { return false; }
            if (view.LabelHorizontalPlacement != setting.LabelHorizontalPlacement) { return false; }
            if (view.LabelVerticalPlacement != setting.LabelVerticalPlacement) { return false; }
            if (view.TextBoxTextAlignment != setting.TextBoxTextAlignment) { return false; }
            if (view.TextDirection != setting.TextDirection) { return false; }
            if (view.TextWrapping != setting.TextWrapping) { return false; }
            if (view.BackgroundTransparency != setting.BackgroundTransparency) { return false; }
            if (view.ShowOverImages != setting.ShowOverImages) { return false; }
            if (view.ShowLabelPerFacing != setting.ShowLabelPerFacing) { return false; }

            return true;
        }

        #endregion

        #region IPlanogramLabel Members

        PlanogramLabelHorizontalAlignment IPlanogramLabel.LabelHorizontalPlacement
        {
            get { return RenderingHelper.GetModelHorizontalAlignment(this.LabelHorizontalPlacement); }
        }

        PlanogramLabelVerticalAlignment IPlanogramLabel.LabelVerticalPlacement
        {
            get { return RenderingHelper.GetModelVerticalAlignment(this.LabelVerticalPlacement); }
        }

        PlanogramLabelTextAlignment IPlanogramLabel.TextBoxTextAlignment
        {
            get { return RenderingHelper.GetModelTextAlignment(this.TextBoxTextAlignment); }
        }

        PlanogramLabelTextDirection IPlanogramLabel.TextDirection
        {
            get { return RenderingHelper.GetModelTextDirection(this.TextDirection); }
        }

        PlanogramLabelTextWrapping IPlanogramLabel.TextWrapping
        {
            get { return RenderingHelper.GetModelTextWrapping(this.TextWrapping); }
        }

        #endregion
    }
}
