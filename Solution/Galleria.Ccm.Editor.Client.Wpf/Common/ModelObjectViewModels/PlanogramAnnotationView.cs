﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
// V8-25734 : L.Ineson
//  Moved out rendering controls to framework.
//V8-26787 : L.Ineson
//  Changes to planitemfield
#endregion
#region Version History: (CCM 8.3)
//V8-31832 : L.Ineson
// Implemented ICalculatedValueResolver
//V8-32523 : L.Ineson
//  Added support for component level annotations.
#endregion
#endregion

using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Planograms.ViewModel;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Windows;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Represents a view of a PlanogramAnnotation.
    /// </summary>
    public sealed class PlanogramAnnotationView : PlanogramAnnotationViewModelBase, IPlanItem, IPlanAnnotationRenderable
    {
        #region Fields

        private readonly PlanogramSubComponentView _parentSubComponentView;
        private readonly PlanogramComponentView _parentComponentView;
        private readonly PlanogramFixtureView _parentFixtureView;
        private CalculatedValueResolver _calculatedValueResolver;

        #endregion

        #region Binding Property Paths
        public static readonly PropertyPath WorldXProperty = WpfHelper.GetPropertyPath<PlanogramAnnotationView>(p => p.WorldX);
        public static readonly PropertyPath WorldYProperty = WpfHelper.GetPropertyPath<PlanogramAnnotationView>(p => p.WorldY);
        public static readonly PropertyPath WorldZProperty = WpfHelper.GetPropertyPath<PlanogramAnnotationView>(p => p.WorldZ);
        #endregion

        #region Properties

        /// <summary>
        /// Returns true if this is a fixture level annotation.
        /// </summary>
        public Boolean IsFixtureAnnotation
        {
            get { return this.SubComponent == null && this.Component == null; }
        }

        /// <summary>
        /// Returns true if this is a subcomponent level annotation.
        /// </summary>
        public Boolean IsComponentAnnotation
        {
            get { return this.Component != null && this.SubComponent == null; }
        }

        /// <summary>
        /// Returns true if this is a subcomponent level annotation.
        /// </summary>
        public Boolean IsSubComponentAnnotation
        {
            get { return this.SubComponent != null; }
        }

        /// <summary>
        /// Returns the resolver used for binding to calculated values.
        /// </summary>
        public CalculatedValueResolver CalculatedValueResolver
        {
            get
            {
                if (_calculatedValueResolver == null)
                {
                    _calculatedValueResolver = 
                        new CalculatedValueResolver(GetCalculatedValue, /*enableCaching*/true);
                }
                return _calculatedValueResolver;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for a fixture annotation
        /// </summary>
        /// <param name="parentFixtureView"></param>
        /// <param name="annotation"></param>
        public PlanogramAnnotationView(PlanogramFixtureView parentFixtureView, PlanogramAnnotation annotation)
            :base(annotation)
        {
            _parentFixtureView = parentFixtureView;
            this.IsSelectable = true; //annotations are always selectable.
        }

        /// <summary>
        /// Constructor for a subcomponent annotation
        /// </summary>
        /// <param name="parentComponentView"></param>
        /// <param name="annotation"></param>
        public PlanogramAnnotationView(PlanogramComponentView parentComponentView, PlanogramAnnotation annotation)
            : base(annotation)
        {
            _parentComponentView = parentComponentView;
            this.IsSelectable = true; //annotations are always selectable.
        }

        /// <summary>
        /// Constructor for a subcomponent annotation
        /// </summary>
        /// <param name="parentSubComponentView"></param>
        /// <param name="annotation"></param>
        public PlanogramAnnotationView(PlanogramSubComponentView parentSubComponentView, PlanogramAnnotation annotation)
            : base(annotation)
        {
            _parentSubComponentView = parentSubComponentView;
            this.IsSelectable = true; //annotations are always selectable.
        }

        #endregion

        #region Methods

        /// <summary>
        /// Called by the parent planogram to notify that processing has completed
        /// </summary>
        internal void NotifyPlanProcessCompleting()
        {
            //if we have calculated values cached then update them now.
            if (_calculatedValueResolver != null) _calculatedValueResolver.RefreshValues();
        }

        /// <summary>
        /// Returns a list of default fields to display for this type.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDefaultPlanItemFields()
        {
            //just return them all for this
            return EnumerateDisplayableFields();
        }


        /// <summary>
        /// Resolves the given expression for this item
        /// </summary>
        public Object GetCalculatedValue(String expressionText)
        {
            return PlanogramFieldHelper.ResolveFieldExpression(expressionText,
                PlanogramItemPlacement.NewPlanogramItemPlacement(
                subComponent: this.SubComponent != null? this.SubComponent.Model:null,
                component: this.Component != null?this.Component.ComponentModel:null,
                fixtureComponent: this.Component != null ? this.Component.FixtureComponentModel : null,
                assemblyComponent: this.Component != null ? this.Component.AssemblyComponentModel : null,
                assembly: (this.Assembly != null) ? this.Assembly.AssemblyModel : null,
                fixtureAssembly: (this.Assembly != null) ? this.Assembly.FixtureAssemblyModel : null,
                fixture: this.Fixture.FixtureModel,
                fixtureItem: this.Fixture.FixtureItemModel,
                planogram: this.Planogram.Model,
                annotation:this.Model
                ));
        }

        #endregion

        #region IPlanItem Members

        Single IPlanItem.X
        {
            get { return (Model.X.HasValue) ? Model.X.Value : 0; }
            set { Model.X = value; }
        }
        Single IPlanItem.Y
        {
            get { return (Model.Y.HasValue) ? Model.Y.Value : 0; }
            set { Model.Y = value; }
        }
        Single IPlanItem.Z
        {
            get { return (Model.Z.HasValue) ? Model.Z.Value : 0; }
            set { Model.Z = value; }
        }

        public PlanogramView Planogram
        {
            get
            {
                if (_parentSubComponentView != null)return _parentSubComponentView.Planogram;
                else if (_parentComponentView != null)return _parentComponentView.Planogram;
                else if (_parentFixtureView != null) return _parentFixtureView.Planogram;
                return null;
            }
        }

        public PlanogramFixtureView Fixture
        {
            get
            {
                if (_parentComponentView != null) return _parentComponentView.Fixture;
                if (_parentSubComponentView != null) return _parentSubComponentView.Fixture;
                else if (_parentFixtureView != null) return _parentFixtureView;
                return null;
            }
        }

        public PlanogramAssemblyView Assembly
        {
            get
            {
                if (_parentComponentView != null) return _parentComponentView.Assembly;
                if (_parentSubComponentView != null) return _parentSubComponentView.Assembly;
                return null;
            }
        }

        public PlanogramComponentView Component
        {
            get
            {
                if (_parentComponentView != null) return _parentComponentView.Component;
                if (_parentSubComponentView != null) return _parentSubComponentView.Component;
                return null;
            }
        }

        public PlanogramSubComponentView SubComponent
        {
            get
            {
                return _parentSubComponentView;
            }
        }

        PlanogramAnnotationView IPlanItem.Annotation
        {
            get { return this; }
        }

        PlanogramPositionView IPlanItem.Position
        {
            get { return null; }
        }

        PlanogramProductView IPlanItem.Product
        {
            get { return null; }
        }

        PlanItemType IPlanItem.PlanItemType
        {
            get { return PlanItemType.Annotation; }
        }

        public Boolean IsSelectable { get; set; }

        public Single Angle { get { return 0; } set { } }
        public Single Slope { get { return 0; } set { } }
        public Single Roll { get { return 0; } set { } }

        #endregion

        #region IDisposable

        protected override void OnDisposing(bool disposing)
        {
            if (_calculatedValueResolver != null) _calculatedValueResolver.Dispose();
            base.OnDisposing(disposing);
        }

        #endregion
    }

}
