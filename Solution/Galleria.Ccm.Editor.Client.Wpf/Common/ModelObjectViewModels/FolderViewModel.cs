﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Provides a view of a folder model object.
    /// </summary>
    public sealed class FolderViewModel : ViewStateObject<Folder>
    {
        #region Constructor

        public FolderViewModel() { }

        #endregion

        #region Methods

        /// <summary>
        /// Fetch the folder at the given directory path.
        /// </summary>
        /// <param name="directoryName"></param>
        public void FetchByDirectoryName(String directoryName)
        {
            Fetch(new Folder.FetchByIdCriteria(Folder.FolderDalFactoryType.FileSystem, directoryName));
        }

        /// <summary>
        /// Saves the current model.
        /// </summary>
        public void Save()
        {
            if (this.Model != null)
            {
                if (!String.IsNullOrEmpty(this.Model.FileSystemPath))
                {
                    //save and immediately refetch as this may have changed externally.
                    Folder savedFolder = this.Model.Save();
                    FetchByDirectoryName(savedFolder.FileSystemPath);
                }
                else
                {
                    Update();
                }
            }
        }

        #endregion
    }
}
