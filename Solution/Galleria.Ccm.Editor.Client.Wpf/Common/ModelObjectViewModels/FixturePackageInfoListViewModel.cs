﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using System.Globalization;
using System.IO;
using System.Linq;
using Galleria.Ccm.Common.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// ViewModel for FixturePackageInfoList
    /// </summary>
    public sealed class FixturePackageInfoListViewModel : ViewStateListObject<FixturePackageInfoList, FixturePackageInfo>
    {
        #region Constructor
        public FixturePackageInfoListViewModel() { }
        #endregion

        #region Methods

        #region FetchAll

        /// <summary>
        /// Populates the model with a list of infos for all
        /// fixtures held in the currently connected to location
        /// </summary>
        public void FetchAll()
        {
            Fetch(GetFetchAllCriteria());
        }

        /// <summary>
        /// Populates the model with a list of infos for all
        /// fixtures held in the currently connected to location
        /// </summary>
        public void FetchAllAsync()
        {
            var criteria = GetFetchAllCriteria();

            if (!CCMClient.IsUnitTesting)
            {
                BeginFetch(criteria);
            }
            else
            {
                Fetch(criteria);
            }
        }

        private FixturePackageInfoList.FetchByIdsCriteria GetFetchAllCriteria()
        {
            FixturePackageInfoList.FetchByIdsCriteria criteria = null;

            switch (FixtureLibraryHelper.GetConnectedFixturePackageType())
            {
                case FixturePackageType.FileSystem:
                    {
                        String folderName = App.ViewState.GetSessionDirectory(SessionDirectory.FixtureLibrary);
                        String searchFilter = String.Format(CultureInfo.InvariantCulture, "*{0}", FixturePackage.FileExtension);

                        criteria =
                            new FixturePackageInfoList.FetchByIdsCriteria(FixturePackageType.FileSystem,
                                Directory.GetFiles(folderName, searchFilter, SearchOption.AllDirectories).Cast<Object>().ToList());
                    }
                    break;

                case FixturePackageType.Unknown:
                    criteria = new FixturePackageInfoList.FetchByIdsCriteria(FixturePackageType.Unknown, null);
                    break;

                default:
                    throw new NotImplementedException();
            }

            return criteria;
        }

        #endregion

        /// <summary>
        /// Moves the given package
        /// </summary>
        /// <param name="info"></param>
        /// <param name="newFolder"></param>
        public void MoveFixturePackage(FixturePackageInfo info, Folder newFolder)
        {
            switch (info.PackageType)
            {
                case FixturePackageType.FileSystem:
                    FixturePackageInfo.MoveFixturePackage((String)info.Id, newFolder.FileSystemPath);
                    break;
                    
                default:
                    throw new NotImplementedException();
                    
            }

            //refetch.
            FetchAll();
        }

        /// <summary>
        /// Renames the given fixture package and reloads
        /// </summary>
        /// <param name="info"></param>
        /// <param name="newName"></param>
        public void RenameFixturePackage(FixturePackageInfo info, String newName)
        {
            switch (info.PackageType)
            {
                case FixturePackageType.FileSystem:
                    using (FixturePackage package = FixturePackage.FetchByFileName((String)info.Id))
                    {
                        package.Name = newName;
                        package.Save();
                    }
                    break;

                default:
                    throw new NotImplementedException();

            }

            //refetch.
            FetchAll();
        }

        #endregion

        #region Overrides

        /// <summary>
        /// Called whenever this viewmodel throws a data portal exception.
        /// </summary>
        /// <param name="error"></param>
        protected override void OnDataPortalException(Exception error)
        {
            Exception rootError = error.GetBaseException();
            LocalHelper.RecordException(rootError);

            Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage
                (Message.FixtureLibrary_FixturePackageInfoFetchError, Galleria.Framework.Controls.Wpf.OperationType.Open);
        }

        #endregion
    }

    
}
