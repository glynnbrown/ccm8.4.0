﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Ineson
//  Created
#endregion

#region Version History: (CCM 8.3)
//V8-31832 : L.Ineson
// Implemented ICalculatedValueResolver
#endregion

#endregion

using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// A flattened out planogram fixture view.
    /// </summary>
    public sealed class PlanogramFixtureDesignView : IPlanFixtureRenderable, IPlanItem, IDisposable
    {
        #region Fields

        private PlanogramFixtureView _fixture;
        private Single _x;
        private CalculatedValueResolver _calculatedValueResolver;

        #endregion

        #region Properties

        public PlanogramFixtureView Fixture
        {
            get { return _fixture; }
        }

        public Single X
        {
            get { return _x; }
            set
            {
                _x = value;
                OnPropertyChanged("X");
            }

        }

        /// <summary>
        /// Returns the resolver used for binding to calculated values.
        /// </summary>
        public CalculatedValueResolver CalculatedValueResolver
        {
            get
            {
                if (_calculatedValueResolver == null)
                    _calculatedValueResolver = new CalculatedValueResolver(GetCalculatedValue);

                return _calculatedValueResolver;
            }
        }

        #endregion

        #region Constructor

        public PlanogramFixtureDesignView(PlanogramFixtureView fixture)
        {
            _fixture = fixture;
            _fixture.PropertyChanged += Fixture_PropertyChanged;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Passes through property changes from the original fixture.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fixture_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(e.PropertyName);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Resolves the given expression for this item
        /// </summary>
        public Object GetCalculatedValue(String expressionText)
        {
            return PlanogramFieldHelper.ResolveFieldExpression(expressionText,
                PlanogramItemPlacement.NewPlanogramItemPlacement(
                fixture: this.Fixture.FixtureModel,
                fixtureItem: this.Fixture.FixtureItemModel,
                planogram: this.Fixture.Planogram.Model
                ));
        }


        #region Equals Override

        public override Int32 GetHashCode()
        {
            return Fixture.GetHashCode();
        }

        public override Boolean Equals(object obj)
        {
            if (obj.GetType() == typeof(PlanogramFixtureDesignView))
            {
                return base.Equals(obj);
            }
            else if (obj.GetType() == typeof(PlanogramFixtureView))
            {
                return Fixture.Equals(obj);
            }
            return false;
        }

        #endregion

        #endregion

        #region IPlanogramFixture Members

        Single IPlanFixtureRenderable.Y
        {
            get { return 0; }
        }
        Single IPlanFixtureRenderable.Z
        {
            get { return 0; }
        }

        Single IPlanFixtureRenderable.Angle
        {
            get { return 0; }
        }
        Single IPlanFixtureRenderable.Slope
        {
            get { return 0; }
        }
        Single IPlanFixtureRenderable.Roll
        {
            get { return 0; }
        }

        public Single Height
        {
            get { return _fixture.Height; }
        }
        public Single Width
        {
            get { return _fixture.Width; }
        }
        public Single Depth
        {
            get { return _fixture.Depth; }
        }

        event NotifyCollectionChangedEventHandler IPlanFixtureRenderable.AssembliesCollectionChanged
        {
            add { ((IPlanFixtureRenderable)_fixture).AssembliesCollectionChanged += value; }
            remove { ((IPlanFixtureRenderable)_fixture).AssembliesCollectionChanged -= value; }
        }

        IEnumerable<IPlanAssemblyRenderable> IPlanFixtureRenderable.Assemblies
        {
            get { return _fixture.Assemblies; }
        }

        event NotifyCollectionChangedEventHandler IPlanFixtureRenderable.ComponentsCollectionChanged
        {
            add { ((IPlanFixtureRenderable)_fixture).ComponentsCollectionChanged += value; }
            remove { ((IPlanFixtureRenderable)_fixture).ComponentsCollectionChanged -= value; }
        }


        IEnumerable<IPlanComponentRenderable> IPlanFixtureRenderable.Components
        {
            get { return _fixture.Components; }
        }

        event NotifyCollectionChangedEventHandler IPlanFixtureRenderable.AnnotationsCollectionChanged
        {
            add { ((IPlanFixtureRenderable)_fixture).AnnotationsCollectionChanged += value; }
            remove { ((IPlanFixtureRenderable)_fixture).AnnotationsCollectionChanged -= value; }
        }

        IEnumerable<IPlanAnnotationRenderable> IPlanFixtureRenderable.Annotations
        {
            get { return _fixture.Annotations; }
        }

        Object IPlanFixtureRenderable.PlanogramFixtureItemId
        {
            get { return this.Fixture.FixtureItemModel.Id; }
        }
        #endregion

        #region IPlanItem Members

        PlanogramView IPlanItem.Planogram
        {
            get { return _fixture.Planogram; }
        }

        PlanogramAssemblyView IPlanItem.Assembly
        {
            get { return null; }
        }

        PlanogramComponentView IPlanItem.Component
        {
            get { return null; }
        }

        PlanogramSubComponentView IPlanItem.SubComponent
        {
            get { return null; }
        }

        PlanogramPositionView IPlanItem.Position
        {
            get { return null; }
        }

        PlanogramProductView IPlanItem.Product
        {
            get { return null; }
        }

        PlanogramAnnotationView IPlanItem.Annotation
        {
            get { return null; }
        }

        public PlanItemType PlanItemType
        {
            get { return _fixture.PlanItemType; }
        }

        Boolean IPlanItem.IsSelectable
        {
            get { return _fixture.IsSelectable; }
            set { _fixture.IsSelectable = value; }
        }

        Single IPlanItem.Y
        {
            get { return _fixture.Y; }
            set { _fixture.Y = value; }
        }

        Single IPlanItem.Z
        {
            get { return _fixture.Z; }
            set { _fixture.Z = value; }
        }

        Single IPlanItem.Angle
        {
            get { return _fixture.Angle; }
            set { _fixture.Angle = value; }
        }

        Single IPlanItem.Slope
        {
            get { return _fixture.Slope; }
            set { _fixture.Slope = value; }
        }

        Single IPlanItem.Roll
        {
            get { return _fixture.Roll; }
            set { _fixture.Roll = value; }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            if (!_isDisposed)
            {
                _fixture.PropertyChanged -= Fixture_PropertyChanged;

                GC.SuppressFinalize(this);
                _isDisposed = true;
            }
        }

        #endregion

    }
}
