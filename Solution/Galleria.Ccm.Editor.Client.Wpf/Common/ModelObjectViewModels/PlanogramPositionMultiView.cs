﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// CCM-26149 : L.Ineson
//  Created
// V8-26569 : L.Ineson
//  Added world space coordinates
// V8-25543 : L.Ineson
//  Orientation type values now use position enum.
//V8-26787 : L.Ineson
//  Changes to planitemfield
// V8-27570 : A.Silva
//      Added PositionSequenceNumber.

#endregion

#region Version History: (CCM 8.02)

// CCM-28766 : J.Pickup
//  IsManuallyPlaced property introuced
// CCM-29028 : L.Ineson
//  Added squeeze properties.

#endregion

#region Version History: (CCM 8.03)

// V8-29427 : L.Luong
//  Added Meta Data properties

#endregion

#region Version History: CCM811

// V8-30220 : A.Silva
//  Added properties to control when edition is allows for High, Wide and Deep facing values when AutoFill could end up overriding the values anyway.
// V8-30395 : A.Silva
//  Amended AutoFillEnabled properties to take into account manually merchandised sub components as autofill does not affect them.
// V8-30381 : A.Silva
//  Added code to react to changes to the AutoFill settings in the planogram.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Attributes;
using Galleria.Framework.Controls.Wpf.PropertyEditor;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using System.Windows.Media;
using Galleria.Framework.Controls.Wpf.HelperClasses;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Presents a merged view of multiple PlanogramPositionViews
    /// </summary>
    public sealed class PlanogramPositionMultiView : ViewModelObject, IDataErrorInfo
    {
        #region Fields

        private IEnumerable<PlanogramPositionView> _items;

        private PlanogramView _plan;
        private Boolean _isLoggingUndoableActions = true;
        private Boolean _canUserEditSequenceNumber;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath MetaWorldXProperty = GetPropertyPath(p => p.MetaWorldX);
        public static readonly PropertyPath MetaWorldYProperty = GetPropertyPath(p => p.MetaWorldY);
        public static readonly PropertyPath MetaWorldZProperty = GetPropertyPath(p => p.MetaWorldZ);

        public static readonly PropertyPath FacingsHighProperty = GetPropertyPath(p => p.FacingsHigh);
        public static readonly PropertyPath FacingsWideProperty = GetPropertyPath(p => p.FacingsWide);
        public static readonly PropertyPath FacingsDeepProperty = GetPropertyPath(p => p.FacingsDeep);
        public static readonly PropertyPath OrientationTypeProperty = GetPropertyPath(p => p.OrientationType);
        public static readonly PropertyPath MerchandisingStyleProperty = GetPropertyPath(p => p.MerchandisingStyle);

        public static readonly PropertyPath FacingsXHighProperty = GetPropertyPath(p => p.FacingsXHigh);
        public static readonly PropertyPath FacingsXWideProperty = GetPropertyPath(p => p.FacingsXWide);
        public static readonly PropertyPath FacingsXDeepProperty = GetPropertyPath(p => p.FacingsXDeep);
        public static readonly PropertyPath OrientationTypeXProperty = GetPropertyPath(p => p.OrientationTypeX);
        public static readonly PropertyPath MerchandisingStyleXProperty = GetPropertyPath(p => p.MerchandisingStyleX);
        public static readonly PropertyPath IsXPlacedLeftProperty = GetPropertyPath(p => p.IsXPlacedLeft);

        public static readonly PropertyPath FacingsYHighProperty = GetPropertyPath(p => p.FacingsYHigh);
        public static readonly PropertyPath FacingsYWideProperty = GetPropertyPath(p => p.FacingsYWide);
        public static readonly PropertyPath FacingsYDeepProperty = GetPropertyPath(p => p.FacingsYDeep);
        public static readonly PropertyPath OrientationTypeYProperty = GetPropertyPath(p => p.OrientationTypeY);
        public static readonly PropertyPath MerchandisingStyleYProperty = GetPropertyPath(p => p.MerchandisingStyleY);
        public static readonly PropertyPath IsYPlacedBottomProperty = GetPropertyPath(p => p.IsYPlacedBottom);

        public static readonly PropertyPath FacingsZHighProperty = GetPropertyPath(p => p.FacingsZHigh);
        public static readonly PropertyPath FacingsZWideProperty = GetPropertyPath(p => p.FacingsZWide);
        public static readonly PropertyPath FacingsZDeepProperty = GetPropertyPath(p => p.FacingsZDeep);
        public static readonly PropertyPath OrientationTypeZProperty = GetPropertyPath(p => p.OrientationTypeZ);
        public static readonly PropertyPath MerchandisingStyleZProperty = GetPropertyPath(p => p.MerchandisingStyleZ);
        public static readonly PropertyPath IsZPlacedFrontProperty = GetPropertyPath(p => p.IsZPlacedFront);

        public static readonly PropertyPath UnitsHighProperty = GetPropertyPath(p => p.UnitsHigh);
        public static readonly PropertyPath UnitsWideProperty = GetPropertyPath(p => p.UnitsWide);
        public static readonly PropertyPath UnitsDeepProperty = GetPropertyPath(p => p.UnitsDeep);
        public static readonly PropertyPath TotalUnitsProperty = GetPropertyPath(p => p.TotalUnits);

        public static readonly PropertyPath IsMerchandisedAsTraysProperty = GetPropertyPath(p => p.IsMerchandisedAsTrays);

        public static readonly PropertyPath PositionSequenceNumberProperty = GetPropertyPath(p => p.PositionSequenceNumber);

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="IsManuallyPlaced"/> property.
        /// </summary>
        public static readonly PropertyPath IsManuallyPlacedProperty = GetPropertyPath(p => p.IsManuallyPlaced);

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="IsAnyAutoFillEnabled"/> property.
        /// </summary>
        public static readonly PropertyPath IsAnyAutoFillEnabledProperty = GetPropertyPath(p => p.IsAnyAutoFillEnabled);

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="IsAutoFillHighEnabled"/> property.
        /// </summary>
        public static readonly PropertyPath IsAutoFillHighEnabledProperty = GetPropertyPath(p => p.IsAutoFillHighEnabled);

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="IsAutoFillWideEnabled"/> property.
        /// </summary>
        public static readonly PropertyPath IsAutoFillWideEnabledProperty = GetPropertyPath(p => p.IsAutoFillWideEnabled);

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="IsAutoFillDeepEnabled"/> property.
        /// </summary>
        public static readonly PropertyPath IsAutoFillDeepEnabledProperty = GetPropertyPath(p => p.IsAutoFillDeepEnabled);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets whether changes should be logged as undoable actions.
        /// Note - this requires that a plan was specified on create.
        /// </summary>
        public Boolean IsLoggingUndoableActions
        {
            get { return _plan != null && _isLoggingUndoableActions; }
            set { _isLoggingUndoableActions = value; }
        }

        public IEnumerable<PlanogramPositionView> Items
        {
            get
            {
                return _items;
            }
        }

        public Int32 Count
        {
            get { return Items.Count(); }
        }

        /// <summary>
        /// Returns true if the position sequence number property
        /// may be edited by the user.
        /// </summary>
        public Boolean CanUserEditSequenceNumber
        {
            get { return _canUserEditSequenceNumber; }
            private set
            {
                _canUserEditSequenceNumber = value;
                OnPropertyChanged("CanUserEditSequenceNumber");
            }
        }


        /// <summary>
        ///     Get whether any <c>Auto Fill</c> setting is enabled in the <c>Planogram</c> that contains this instance's
        ///     <c>Positions</c>.
        /// </summary>
        /// <remarks>
        ///     If <see cref="IsManuallyPlaced" /> is <c>True</c> for all <c>Positions</c> in this instance Any <c>Auto Fill</c>
        ///     settings will be ignored and this property will return <c>False</c>.
        /// </remarks>
        public Boolean IsAnyAutoFillEnabled
        {
            get
            {
                //  Manually placed positions never are affected by any Auto Fill.
                if (IsManuallyPlaced.HasValue &&
                    IsManuallyPlaced.Value) return false;

                //  Positions with any Auto Fill settings AND no Manual Merchandising Strategy subcomponents are affected.
                Planogram planogram = _plan.Model;
                List<PlanogramSubComponentView> subComponentViews = Items.GroupBy(item => item.SubComponent).Select(g => g.Key).ToList();
                return (planogram.ProductPlacementX != PlanogramProductPlacementXType.Manual &&
                        subComponentViews.Any(s => s.MerchandisingStrategyX != PlanogramSubComponentXMerchStrategyType.Manual)) ||
                       (planogram.ProductPlacementY != PlanogramProductPlacementYType.Manual &&
                        subComponentViews.Any(s => s.MerchandisingStrategyY != PlanogramSubComponentYMerchStrategyType.Manual)) ||
                       (planogram.ProductPlacementZ != PlanogramProductPlacementZType.Manual &&
                        subComponentViews.Any(s => s.MerchandisingStrategyZ != PlanogramSubComponentZMerchStrategyType.Manual));
            }
        }

        /// <summary>
        ///     Get whether the <c>Auto Fill High</c> setting is enabled in the <c>Planogram</c> that contains this instance's
        ///     <c>Positions</c>.
        /// </summary>
        /// <remarks>
        ///     If <see cref="IsManuallyPlaced" /> is <c>True</c> for all <c>Positions</c> in this instance Any <c>Auto Fill</c>
        ///     settings will be ignored and this property will return <c>False</c>.
        /// </remarks>
        public Boolean IsAutoFillHighEnabled
        {
            get
            {
                //  Manually placed positions never are affected by any Auto Fill.
                if (IsManuallyPlaced.HasValue &&
                    IsManuallyPlaced.Value) return false;

                //  Positions with Auto Fill setting AND no Manual Merchandising Strategy subcomponents are affected.
                return (_plan.Model.ProductPlacementY != PlanogramProductPlacementYType.Manual &&
                        Items.GroupBy(item => item.SubComponent)
                             .Select(g => g.Key)
                             .Any(s => s.MerchandisingStrategyY != PlanogramSubComponentYMerchStrategyType.Manual));
            }
        }

        /// <summary>
        ///     Get whether the <c>Auto Fill Wide</c> setting is enabled in the <c>Planogram</c> that contains this instance's
        ///     <c>Positions</c>.
        /// </summary>
        /// <remarks>
        ///     If <see cref="IsManuallyPlaced" /> is <c>True</c> for all <c>Positions</c> in this instance Any <c>Auto Fill</c>
        ///     settings will be ignored and this property will return <c>False</c>.
        /// </remarks>
        public Boolean IsAutoFillWideEnabled
        {
            get
            {
                //  Manually placed positions never are affected by any Auto Fill.
                if (IsManuallyPlaced.HasValue &&
                    IsManuallyPlaced.Value) return false;

                //  Positions with Auto Fill setting AND no Manual Merchandising Strategy subcomponents are affected.
                return (_plan.Model.ProductPlacementX != PlanogramProductPlacementXType.Manual &&
                        Items.GroupBy(item => item.SubComponent)
                             .Select(g => g.Key)
                             .Any(s => s.MerchandisingStrategyX != PlanogramSubComponentXMerchStrategyType.Manual));
            }
        }

        /// <summary>
        ///     Get whether the <c>Auto Fill Deep</c> setting is enabled in the <c>Planogram</c> that contains this instance's
        ///     <c>Positions</c>.
        /// </summary>
        /// <remarks>
        ///     If <see cref="IsManuallyPlaced" /> is <c>True</c> for all <c>Positions</c> in this instance Any <c>Auto Fill</c>
        ///     settings will be ignored and this property will return <c>False</c>.
        /// </remarks>
        public Boolean IsAutoFillDeepEnabled
        {
            get
            {   
                //  Manually placed positions never are affected by any Auto Fill.
                if (IsManuallyPlaced.HasValue &&
                    IsManuallyPlaced.Value) return false;

                //  Positions with Auto Fill setting AND no Manual Merchandising Strategy subcomponents are affected.
                return (_plan.Model.ProductPlacementZ != PlanogramProductPlacementZType.Manual &&
                        Items.GroupBy(item => item.SubComponent)
                             .Select(g => g.Key)
                             .Any(s => s.MerchandisingStrategyZ != PlanogramSubComponentZMerchStrategyType.Manual));
            }
        }

        #region Model properties

        //[ModelObjectDisplayName(typeof(PlanogramPosition), "XProperty")]
        //public Single? X
        //{
        //    get
        //    {
        //        return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.XProperty.Name);
        //    }
        //    set
        //    {
        //        if (value.HasValue)
        //        {
        //            SetPropertyValue(Items, PlanogramPosition.XProperty.Name, value.Value);
        //        }
        //    }
        //}

        //[ModelObjectDisplayName(typeof(PlanogramPosition), "YProperty")]
        //public Single? Y
        //{
        //    get
        //    {
        //        return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.YProperty.Name);
        //    }
        //    set
        //    {
        //        if (value.HasValue)
        //        {
        //            SetPropertyValue(Items, PlanogramPosition.YProperty.Name, value.Value);
        //        }
        //    }
        //}

        //[ModelObjectDisplayName(typeof(PlanogramPosition), "ZProperty")]
        //public Single? Z
        //{
        //    get
        //    {
        //        return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.ZProperty.Name);
        //    }
        //    set
        //    {
        //        if (value.HasValue)
        //        {
        //            SetPropertyValue(Items, PlanogramPosition.ZProperty.Name, value.Value);
        //        }
        //    }
        //}

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaWorldXProperty")]
        public Single? MetaWorldX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.MetaWorldXProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.MetaWorldXProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaWorldYProperty")]
        public Single? MetaWorldY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.MetaWorldYProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.MetaWorldYProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaWorldZProperty")]
        public Single? MetaWorldZ
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.MetaWorldZProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.MetaWorldZProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "FacingsHighProperty")]
        public Int16? FacingsHigh
        {
            get { return GetCommonPropertyValue<Int16?>(Items, PlanogramPosition.FacingsHighProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.FacingsHighProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "FacingsWideProperty")]
        public Int16? FacingsWide
        {
            get { return GetCommonPropertyValue<Int16?>(Items, PlanogramPosition.FacingsWideProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.FacingsWideProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "FacingsDeepProperty")]
        public Int16? FacingsDeep
        {
            get { return GetCommonPropertyValue<Int16?>(Items, PlanogramPosition.FacingsDeepProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.FacingsDeepProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "OrientationTypeProperty")]
        [PropertyEditorItemsSource(typeof(PropertyEditorItemsSource<PlanogramPositionOrientationType>))]
        public PlanogramPositionOrientationType? OrientationType
        {
            get { return GetCommonPropertyValue<PlanogramPositionOrientationType?>(Items, PlanogramPosition.OrientationTypeProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.OrientationTypeProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MerchandisingStyleProperty")]
        [PropertyEditorItemsSource(typeof(PropertyEditorItemsSource<PlanogramPositionMerchandisingStyle>))]
        public PlanogramPositionMerchandisingStyle? MerchandisingStyle
        {
            get { return GetCommonPropertyValue<PlanogramPositionMerchandisingStyle?>(Items, PlanogramPosition.MerchandisingStyleProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.MerchandisingStyleProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramPosition), "FacingsXHighProperty")]
        public Int16? FacingsXHigh
        {
            get { return GetCommonPropertyValue<Int16?>(Items, PlanogramPosition.FacingsXHighProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.FacingsXHighProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "FacingsXWideProperty")]
        public Int16? FacingsXWide
        {
            get { return GetCommonPropertyValue<Int16?>(Items, PlanogramPosition.FacingsXWideProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.FacingsXWideProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "FacingsXDeepProperty")]
        public Int16? FacingsXDeep
        {
            get { return GetCommonPropertyValue<Int16?>(Items, PlanogramPosition.FacingsXDeepProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.FacingsXDeepProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "OrientationTypeXProperty")]
        [PropertyEditorItemsSource(typeof(PropertyEditorItemsSource<PlanogramPositionOrientationType>))]
        public PlanogramPositionOrientationType? OrientationTypeX
        {
            get { return GetCommonPropertyValue<PlanogramPositionOrientationType?>(Items, PlanogramPosition.OrientationTypeXProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.OrientationTypeXProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MerchandisingStyleXProperty")]
        public PlanogramPositionMerchandisingStyle? MerchandisingStyleX
        {
            get { return GetCommonPropertyValue<PlanogramPositionMerchandisingStyle?>(Items, PlanogramPosition.MerchandisingStyleXProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.MerchandisingStyleXProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "IsXPlacedLeftProperty")]
        public Boolean? IsXPlacedLeft
        {
            get { return GetCommonPropertyValue<Boolean?>(Items, PlanogramPosition.IsXPlacedLeftProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.IsXPlacedLeftProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramPosition), "FacingsYHighProperty")]
        public Int16? FacingsYHigh
        {
            get { return GetCommonPropertyValue<Int16?>(Items, PlanogramPosition.FacingsYHighProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.FacingsYHighProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "FacingsYWideProperty")]
        public Int16? FacingsYWide
        {
            get { return GetCommonPropertyValue<Int16?>(Items, PlanogramPosition.FacingsYWideProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.FacingsYWideProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "FacingsYDeepProperty")]
        public Int16? FacingsYDeep
        {
            get { return GetCommonPropertyValue<Int16?>(Items, PlanogramPosition.FacingsYDeepProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.FacingsYDeepProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "OrientationTypeYProperty")]
        [PropertyEditorItemsSource(typeof(PropertyEditorItemsSource<PlanogramPositionOrientationType>))]
        public PlanogramPositionOrientationType? OrientationTypeY
        {
            get { return GetCommonPropertyValue<PlanogramPositionOrientationType?>(Items, PlanogramPosition.OrientationTypeYProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.OrientationTypeYProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MerchandisingStyleYProperty")]
        public PlanogramPositionMerchandisingStyle? MerchandisingStyleY
        {
            get { return GetCommonPropertyValue<PlanogramPositionMerchandisingStyle?>(Items, PlanogramPosition.MerchandisingStyleYProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.MerchandisingStyleYProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "IsYPlacedBottomProperty")]
        public Boolean? IsYPlacedBottom
        {
            get { return GetCommonPropertyValue<Boolean?>(Items, PlanogramPosition.IsYPlacedBottomProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.IsYPlacedBottomProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramPosition), "FacingsZHighProperty")]
        public Int16? FacingsZHigh
        {
            get { return GetCommonPropertyValue<Int16?>(Items, PlanogramPosition.FacingsZHighProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.FacingsZHighProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "FacingsZWideProperty")]
        public Int16? FacingsZWide
        {
            get { return GetCommonPropertyValue<Int16?>(Items, PlanogramPosition.FacingsZWideProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.FacingsZWideProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "FacingsZDeepProperty")]
        public Int16? FacingsZDeep
        {
            get { return GetCommonPropertyValue<Int16?>(Items, PlanogramPosition.FacingsZDeepProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.FacingsZDeepProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "OrientationTypeZProperty")]
        [PropertyEditorItemsSource(typeof(PropertyEditorItemsSource<PlanogramPositionOrientationType>))]
        public PlanogramPositionOrientationType? OrientationTypeZ
        {
            get { return GetCommonPropertyValue<PlanogramPositionOrientationType?>(Items, PlanogramPosition.OrientationTypeZProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.OrientationTypeZProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MerchandisingStyleZProperty")]
        public PlanogramPositionMerchandisingStyle? MerchandisingStyleZ
        {
            get { return GetCommonPropertyValue<PlanogramPositionMerchandisingStyle?>(Items, PlanogramPosition.MerchandisingStyleZProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.MerchandisingStyleZProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "IsZPlacedFrontProperty")]
        public Boolean? IsZPlacedFront
        {
            get { return GetCommonPropertyValue<Boolean?>(Items, PlanogramPosition.IsZPlacedFrontProperty.Name); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPosition.IsZPlacedFrontProperty.Name, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramPosition), "UnitsHighProperty")]
        public Int16? UnitsHigh
        {
            get
            {
                return GetCommonPropertyValue<Int16?>(Items, PlanogramPosition.UnitsHighProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "UnitsWideProperty")]
        public Int16? UnitsWide
        {
            get
            {
                return GetCommonPropertyValue<Int16?>(Items, PlanogramPosition.UnitsWideProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "UnitsDeepProperty")]
        public Int16? UnitsDeep
        {
            get
            {
                return GetCommonPropertyValue<Int16?>(Items, PlanogramPosition.UnitsDeepProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "TotalUnitsProperty")]
        public Int32? TotalUnits
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPosition.TotalUnitsProperty.Name);
            }
        }


        [LocalisedDisplayName(typeof(Message), "PlanogramPositionView_IsMerchandisedAsTrays")]
        public Boolean? IsMerchandisedAsTrays
        {
            get { return GetCommonPropertyValue<Boolean?>(Items, "IsMerchandisedAsTrays"); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, "IsMerchandisedAsTrays", value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramProduct), "HeightProperty")]
        public Single? Height
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPositionView.HeightProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "WidthProperty")]
        public Single? Width
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPositionView.WidthProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramProduct), "DepthProperty")]
        public Single? Depth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPositionView.DepthProperty.Path);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "PositionSequenceNumberProperty")]
        public Int16? PositionSequenceNumber
        {
            get
            {
                return GetCommonPropertyValue<Int16?>(Items, PlanogramPositionView.PositionSequenceNumberProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramPositionView.PositionSequenceNumberProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "IsManuallyPlacedProperty")]
        public Boolean? IsManuallyPlaced
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramPosition.IsManuallyPlacedProperty.Name);
            }
            set
            {
                if (!value.HasValue) return;
                SetPropertyValue(Items, PlanogramPositionView.IsManuallyPlacedProperty.Path, value);
                OnPropertyChanged(IsAnyAutoFillEnabledProperty);
                OnPropertyChanged(IsAutoFillWideEnabledProperty);
                OnPropertyChanged(IsAutoFillHighEnabledProperty);
                OnPropertyChanged(IsAutoFillDeepEnabledProperty);
            }
        }


        [ModelObjectDisplayName(typeof(PlanogramPosition), "VerticalSqueezeProperty")]
        public Single? VerticalSqueeze
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.VerticalSqueezeProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "HorizontalSqueezeProperty")]
        public Single? HorizontalSqueeze
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.HorizontalSqueezeProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "DepthSqueezeProperty")]
        public Single? DepthSqueeze
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.DepthSqueezeProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "VerticalSqueezeXProperty")]
        public Single? VerticalSqueezeX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.VerticalSqueezeXProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "HorizontalSqueezeXProperty")]
        public Single? HorizontalSqueezeX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.HorizontalSqueezeXProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "DepthSqueezeXProperty")]
        public Single? DepthSqueezeX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.DepthSqueezeXProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "VerticalSqueezeYProperty")]
        public Single? VerticalSqueezeY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.VerticalSqueezeYProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "HorizontalSqueezeYProperty")]
        public Single? HorizontalSqueezeY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.HorizontalSqueezeYProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "DepthSqueezeYProperty")]
        public Single? DepthSqueezeY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.DepthSqueezeYProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "VerticalSqueezeZProperty")]
        public Single? VerticalSqueezeZ
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.VerticalSqueezeZProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "HorizontalSqueezeZProperty")]
        public Single? HorizontalSqueezeZ
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.HorizontalSqueezeZProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "DepthSqueezeZProperty")]
        public Single? DepthSqueezeZ
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.DepthSqueezeZProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "SequenceNumberProperty")]
        public Int32? SequenceNumber
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPosition.SequenceNumberProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "SequenceColourProperty")]
        public Color SequenceColour
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, PlanogramProduct.FillColourProperty.Name);

                if (val.HasValue)
                {
                    return ColorHelper.IntToColor(val.Value);
                }
                else
                {
                    return Colors.White;
                }
            }
        }

        #region Meta Data Properties

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaAchievedCasesProperty")]
        public Single? MetaAchievedCases
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.MetaAchievedCasesProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaIsPositionCollisionsProperty")]
        public Boolean? MetaIsPositionCollisions
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramPosition.MetaIsPositionCollisionsProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaFrontFacingsWideProperty")]
        public Int16? MetaFrontFacingsWide
        {
            get
            {
                return GetCommonPropertyValue<Int16?>(Items, PlanogramPosition.MetaFrontFacingsWideProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaIsUnderMinDeepProperty")]
        public Boolean? MetaIsUnderMinDeep
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramPosition.MetaIsUnderMinDeepProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaIsOverMaxDeepProperty")]
        public Boolean? MetaIsOverMaxDeep
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramPosition.MetaIsOverMaxDeepProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaIsOverMaxRightCapProperty")]
        public Boolean? MetaIsOverMaxRightCap
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramPosition.MetaIsOverMaxRightCapProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaIsOverMaxStackProperty")]
        public Boolean? MetaIsOverMaxStack
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramPosition.MetaIsOverMaxStackProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaIsOverMaxTopCapProperty")]
        public Boolean? MetaIsOverMaxTopCap
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramPosition.MetaIsOverMaxTopCapProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaIsInvalidMerchandisingStyleProperty")]
        public Boolean? MetaIsInvalidMerchandisingStyle
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramPosition.MetaIsInvalidMerchandisingStyleProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaIsHangingTrayProperty")]
        public Boolean? MetaIsHangingTray
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramPosition.MetaIsHangingTrayProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaIsPegOverfilledProperty")]
        public Boolean? MetaIsPegOverfilled
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramPosition.MetaIsPegOverfilledProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaIsOutsideMerchandisingSpaceProperty")]
        public Boolean? MetaIsOutsideMerchandisingSpace
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramPosition.MetaIsOutsideMerchandisingSpaceProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaIsOutsideOfBlockSpaceProperty")]
        public Boolean? MetaIsOutsideOfBlockSpace
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramPosition.MetaIsOutsideOfBlockSpaceProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaTotalLinearSpaceProperty")]
        public Single? MetaTotalLinearSpace
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.MetaTotalLinearSpaceProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaTotalAreaSpaceProperty")]
        public Single? MetaTotalAreaSpace
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.MetaTotalAreaSpaceProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaTotalVolumetricSpaceProperty")]
        public Single? MetaTotalVolumetricSpace
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.MetaTotalVolumetricSpaceProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaPlanogramLinearSpacePercentageProperty")]
        public Single? MetaPlanogramLinearSpacePercentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.MetaPlanogramLinearSpacePercentageProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaPlanogramAreaSpacePercentageProperty")]
        public Single? MetaPlanogramAreaSpacePercentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.MetaPlanogramAreaSpacePercentageProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaPlanogramVolumetricSpacePercentageProperty")]
        public Single? MetaPlanogramVolumetricSpacePercentage
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramPosition.MetaPlanogramVolumetricSpacePercentageProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaPegRowNumberProperty")]
        public Int32? MetaPegRowNumber
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPosition.MetaPegRowNumberProperty.Name);
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramPosition), "MetaPegColumnNumberProperty")]
        public Int32? MetaPegColumnNumber
        {
            get
            {
                return GetCommonPropertyValue<Int32?>(Items, PlanogramPosition.MetaPegColumnNumberProperty.Name);
            }
        }

        #endregion

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="items"></param>
        public PlanogramPositionMultiView(IEnumerable<PlanogramPositionView> items)
        {
            _items = items.ToList();
            _plan = _items.First().Planogram;

            foreach (PlanogramPositionView i in _items)
            {
                i.PropertyChanged += PlanItem_PropertyChanged;
            }
            _plan.PropertyChanged += PlanModelOnPropertyChanged;

            this.CanUserEditSequenceNumber = (_plan != null) ? !_plan.Model.RenumberingStrategy.IsEnabled : false;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on any linked plan item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(e.PropertyName);
        }

        /// <summary>
        ///     Called whenever a property changes in the planogram associated with this instance.
        /// </summary>
        private void PlanModelOnPropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != Planogram.ProductPlacementXProperty.Name &&
                e.PropertyName != Planogram.ProductPlacementYProperty.Name &&
                e.PropertyName != Planogram.ProductPlacementZProperty.Name) return;
            //  Notify there were changes to AutoFill settings.
            OnPropertyChanged(IsAnyAutoFillEnabledProperty);
            OnPropertyChanged(IsAutoFillWideEnabledProperty);
            OnPropertyChanged(IsAutoFillHighEnabledProperty);
            OnPropertyChanged(IsAutoFillDeepEnabledProperty);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the common value of the given property for all the items
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        private static T GetCommonPropertyValue<T>(IEnumerable<Object> items, String propertyName)
        {
            T value = default(T);

            if (items.Any())
            {
                Type classType = items.First().GetType();
                System.Reflection.PropertyInfo pInfo = classType.GetProperty(propertyName);
                if (pInfo != null)
                {
                    Boolean isFirst = true;
                    foreach (Object item in items)
                    {
                        T itemValue = (T)pInfo.GetValue(item, null);
                        if (isFirst)
                        {
                            value = itemValue;
                        }
                        else if (!Object.Equals(itemValue, value))
                        {
                            return default(T);
                        }

                        isFirst = false;
                    }
                }
            }

            return value;

        }

        /// <summary>
        /// Sets the given value against
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        private void SetPropertyValue<T>(IEnumerable<Object> items, String propertyName, T value)
        {
            if (items.Any())
            {

                //Start logging the change
                Boolean isProcessingUndoable = false;
                if (IsLoggingUndoableActions)
                {
                    if (!_plan.IsRecordingUndoableAction)
                    {
                        isProcessingUndoable = true;
                        _plan.BeginUndoableAction(propertyName);
                    }
                }

                //make the property change
                Type classType = items.First().GetType();
                System.Reflection.PropertyInfo pInfo = classType.GetProperty(propertyName);

                if (pInfo != null)
                {
                    foreach (Object item in items)
                    {
                        pInfo.SetValue(item, value, null);
                    }
                }

                //Commit the log.
                if (isProcessingUndoable)
                {
                    _plan.EndUndoableAction();
                }
            }
        }


        #endregion

        #region IDataErrorInfo Members

        public String Error
        {
            get { throw new NotImplementedException(); }
        }

        public String this[String columnName]
        {
            get
            {
                return ((IDataErrorInfo)_items.First())[columnName];
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                foreach (var i in _items)
                {
                    i.PropertyChanged -= PlanItem_PropertyChanged;
                }

                _plan.PropertyChanged -= PlanModelOnPropertyChanged;

                base.IsDisposed = true;
            }
        }

        #endregion

        #region Static Helpers

        private static PropertyPath GetPropertyPath(System.Linq.Expressions.Expression<Func<PlanogramPositionMultiView, Object>> expression)
        {
            return WpfHelper.GetPropertyPath<PlanogramPositionMultiView>(expression);
        }

        #endregion
    }
}
