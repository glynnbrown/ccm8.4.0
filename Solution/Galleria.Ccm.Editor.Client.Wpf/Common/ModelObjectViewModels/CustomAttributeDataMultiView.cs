﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26377 : L.Ineson
//  Created
#endregion

#region Version History: (CCM 8.4)
// V8-19069 : J.Mendes
//  Five new Note field attributes for the Custom Attribute Data
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Galleria.Framework.Attributes;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Presents a merged view of multiple custom attribute data model objects
    /// </summary>
    public sealed class CustomAttributeDataMultiView : ViewModelObject, IDataErrorInfo
    {
        #region Fields

        private IEnumerable<CustomAttributeData> _items;

        private PlanogramView _plan;
        private Boolean _isLoggingUndoableActions = true;

        #endregion

        #region Binding Property Paths

        
        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets whether changes should be logged as undoable actions.
        /// Note - this requires that a plan was specified on create.
        /// </summary>
        public Boolean IsLoggingUndoableActions
        {
            get { return _plan != null && _isLoggingUndoableActions; }
            set { _isLoggingUndoableActions = value; }
        }

        public IEnumerable<CustomAttributeData> Items
        {
            get
            {
                return _items;
            }
        }

        #region Custom Fields

        #region Custom Text Fields

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text1Property")]
        public String Text1
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text1Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text1Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text2Property")]
        public String Text2
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text2Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text2Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text3Property")]
        public String Text3
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text3Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text3Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text4Property")]
        public String Text4
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text4Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text4Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text5Property")]
        public String Text5
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text5Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text5Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text6Property")]
        public String Text6
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text6Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text6Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text7Property")]
        public String Text7
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text7Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text7Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text8Property")]
        public String Text8
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text8Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text8Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text9Property")]
        public String Text9
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text9Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text9Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text10Property")]
        public String Text10
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text10Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text10Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text11Property")]
        public String Text11
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text11Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text11Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text12Property")]
        public String Text12
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text12Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text12Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text13Property")]
        public String Text13
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text13Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text13Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text14Property")]
        public String Text14
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text14Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text14Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text15Property")]
        public String Text15
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text15Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text15Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text16Property")]
        public String Text16
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text16Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text16Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text17Property")]
        public String Text17
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text17Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text17Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text18Property")]
        public String Text18
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text18Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text18Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text19Property")]
        public String Text19
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text19Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text19Property.Name, value);
                }
            }
        }



        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text20Property")]
        public String Text20
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text20Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text20Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text21Property")]
        public String Text21
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text21Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text21Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text22Property")]
        public String Text22
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text22Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text22Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text23Property")]
        public String Text23
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text23Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text23Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text24Property")]
        public String Text24
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text24Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text24Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text25Property")]
        public String Text25
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text25Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text25Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text26Property")]
        public String Text26
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text26Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text26Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text27Property")]
        public String Text27
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text27Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text27Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text28Property")]
        public String Text28
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text28Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text28Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text29Property")]
        public String Text29
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text29Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text29Property.Name, value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text30Property")]
        public String Text30
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text30Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text30Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text31Property")]
        public String Text31
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text31Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text31Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text32Property")]
        public String Text32
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text32Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text32Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text33Property")]
        public String Text33
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text33Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text33Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text34Property")]
        public String Text34
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text34Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text34Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text35Property")]
        public String Text35
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text35Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text35Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text36Property")]
        public String Text36
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text36Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text36Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text37Property")]
        public String Text37
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text37Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text37Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text38Property")]
        public String Text38
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text38Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text38Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text39Property")]
        public String Text39
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text39Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text39Property.Name, value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text40Property")]
        public String Text40
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text40Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text40Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text41Property")]
        public String Text41
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text41Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text41Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text42Property")]
        public String Text42
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text42Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text42Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text43Property")]
        public String Text43
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text43Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text43Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text44Property")]
        public String Text44
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text44Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text44Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text45Property")]
        public String Text45
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text45Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text45Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text46Property")]
        public String Text46
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text46Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text46Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text47Property")]
        public String Text47
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text47Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text47Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text48Property")]
        public String Text48
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text48Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text48Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text49Property")]
        public String Text49
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text49Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text49Property.Name, value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(CustomAttributeData), "Text50Property")]
        public String Text50
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Text50Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Text50Property.Name, value);
                }
            }
        }

        #endregion

        #region Custom Value Fields

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value1Property")]
        public Single? Value1
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value1Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value1Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value2Property")]
        public Single? Value2
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value2Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value2Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value3Property")]
        public Single? Value3
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value3Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value3Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value4Property")]
        public Single? Value4
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value4Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value4Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value5Property")]
        public Single? Value5
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value5Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value5Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value6Property")]
        public Single? Value6
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value6Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value6Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value7Property")]
        public Single? Value7
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value7Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value7Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value8Property")]
        public Single? Value8
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value8Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value8Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value9Property")]
        public Single? Value9
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value9Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value9Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value10Property")]
        public Single? Value10
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value10Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value10Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value11Property")]
        public Single? Value11
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value11Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value11Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value12Property")]
        public Single? Value12
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value12Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value12Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value13Property")]
        public Single? Value13
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value13Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value13Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value14Property")]
        public Single? Value14
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value14Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value14Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value15Property")]
        public Single? Value15
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value15Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value15Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value16Property")]
        public Single? Value16
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value16Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value16Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value17Property")]
        public Single? Value17
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value17Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value17Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value18Property")]
        public Single? Value18
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value18Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value18Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value19Property")]
        public Single? Value19
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value19Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value19Property.Name, value);
                }
            }
        }



        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value20Property")]
        public Single? Value20
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value20Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value20Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value21Property")]
        public Single? Value21
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value21Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value21Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value22Property")]
        public Single? Value22
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value22Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value22Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value23Property")]
        public Single? Value23
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value23Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value23Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value24Property")]
        public Single? Value24
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value24Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value24Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value25Property")]
        public Single? Value25
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value25Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value25Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value26Property")]
        public Single? Value26
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value26Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value26Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value27Property")]
        public Single? Value27
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value27Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value27Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value28Property")]
        public Single? Value28
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value28Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value28Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value29Property")]
        public Single? Value29
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value29Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value29Property.Name, value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value30Property")]
        public Single? Value30
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value30Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value30Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value31Property")]
        public Single? Value31
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value31Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value31Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value32Property")]
        public Single? Value32
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value32Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value32Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value33Property")]
        public Single? Value33
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value33Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value33Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value34Property")]
        public Single? Value34
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value34Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value34Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value35Property")]
        public Single? Value35
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value35Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value35Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value36Property")]
        public Single? Value36
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value36Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value36Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value37Property")]
        public Single? Value37
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value37Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value37Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value38Property")]
        public Single? Value38
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value38Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value38Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value39Property")]
        public Single? Value39
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value39Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value39Property.Name, value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value40Property")]
        public Single? Value40
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value40Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value40Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value41Property")]
        public Single? Value41
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value41Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value41Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value42Property")]
        public Single? Value42
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value42Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value42Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value43Property")]
        public Single? Value43
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value43Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value43Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value44Property")]
        public Single? Value44
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value44Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value44Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value45Property")]
        public Single? Value45
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value45Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value45Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value46Property")]
        public Single? Value46
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value46Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value46Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value47Property")]
        public Single? Value47
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value47Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value47Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value48Property")]
        public Single? Value48
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value48Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value48Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value49Property")]
        public Single? Value49
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value49Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value49Property.Name, value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(CustomAttributeData), "Value50Property")]
        public Single? Value50
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, CustomAttributeData.Value50Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Value50Property.Name, value);
                }
            }
        }

        #endregion

        #region Product Custom Flag

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Flag1Property")]
        public Boolean? Flag1
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, CustomAttributeData.Flag1Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Flag1Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Flag2Property")]
        public Boolean? Flag2
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, CustomAttributeData.Flag2Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Flag2Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Flag3Property")]
        public Boolean? Flag3
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, CustomAttributeData.Flag3Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Flag3Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Flag4Property")]
        public Boolean? Flag4
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, CustomAttributeData.Flag4Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Flag4Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Flag5Property")]
        public Boolean? Flag5
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, CustomAttributeData.Flag5Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Flag5Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Flag6Property")]
        public Boolean? Flag6
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, CustomAttributeData.Flag6Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Flag6Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Flag7Property")]
        public Boolean? Flag7
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, CustomAttributeData.Flag7Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Flag7Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Flag8Property")]
        public Boolean? Flag8
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, CustomAttributeData.Flag8Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Flag8Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Flag9Property")]
        public Boolean? Flag9
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, CustomAttributeData.Flag9Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Flag9Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Flag10Property")]
        public Boolean? Flag10
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, CustomAttributeData.Flag10Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Flag10Property.Name, value);
                }
            }
        }

        #endregion

        #region Product Custom Date

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Date1Property")]
        public DateTime? Date1
        {
            get
            {
                return GetCommonPropertyValue<DateTime?>(Items, CustomAttributeData.Date1Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Date1Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Date2Property")]
        public DateTime? Date2
        {
            get
            {
                return GetCommonPropertyValue<DateTime?>(Items, CustomAttributeData.Date2Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Date2Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Date3Property")]
        public DateTime? Date3
        {
            get
            {
                return GetCommonPropertyValue<DateTime?>(Items, CustomAttributeData.Date3Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Date3Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Date4Property")]
        public DateTime? Date4
        {
            get
            {
                return GetCommonPropertyValue<DateTime?>(Items, CustomAttributeData.Date4Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Date4Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Date5Property")]
        public DateTime? Date5
        {
            get
            {
                return GetCommonPropertyValue<DateTime?>(Items, CustomAttributeData.Date5Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Date5Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Date6Property")]
        public DateTime? Date6
        {
            get
            {
                return GetCommonPropertyValue<DateTime?>(Items, CustomAttributeData.Date6Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Date6Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Date7Property")]
        public DateTime? Date7
        {
            get
            {
                return GetCommonPropertyValue<DateTime?>(Items, CustomAttributeData.Date7Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Date7Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Date8Property")]
        public DateTime? Date8
        {
            get
            {
                return GetCommonPropertyValue<DateTime?>(Items, CustomAttributeData.Date8Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Date8Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Date9Property")]
        public DateTime? Date9
        {
            get
            {
                return GetCommonPropertyValue<DateTime?>(Items, CustomAttributeData.Date9Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Date9Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Date10Property")]
        public DateTime? Date10
        {
            get
            {
                return GetCommonPropertyValue<DateTime?>(Items, CustomAttributeData.Date10Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Date10Property.Name, value);
                }
            }
        }

        #endregion

        #region Custom Note Fields

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Note1Property")]
        public String Note1
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Note1Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Note1Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Note2Property")]
        public String Note2
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Note2Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Note2Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Note3Property")]
        public String Note3
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Note3Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Note3Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Note4Property")]
        public String Note4
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Note4Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Note4Property.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(CustomAttributeData), "Note5Property")]
        public String Note5
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, CustomAttributeData.Note5Property.Name);
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, CustomAttributeData.Note5Property.Name, value);
                }
            }
        }

        #endregion

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="items"></param>
        public CustomAttributeDataMultiView(IEnumerable<CustomAttributeData> items, PlanogramView planogramView)
        {
            _items = items.ToList();
            _plan = planogramView;

            foreach (var i in _items)
            {
                i.PropertyChanged += PlanItem_PropertyChanged;
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on any linked plan item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(e.PropertyName);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the common value of the given property for all the items
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        private static T GetCommonPropertyValue<T>(IEnumerable<Object> items, String propertyName)
        {
            return PlanItemHelper.GetCommonPropertyValue<T>(items, propertyName);
        }

        /// <summary>
        /// Sets the given value against
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        private void SetPropertyValue<T>(IEnumerable<Object> items, String propertyName, T value)
        {
            PlanItemHelper.SetPropertyValue<T>(items, propertyName, value, IsLoggingUndoableActions, _plan);
        }


        #endregion

        #region IDataErrorInfo Members

        public String Error
        {
            get { throw new NotImplementedException(); }
        }

        public String this[String columnName]
        {
            get
            {
                return ((IDataErrorInfo)_items.First())[columnName];
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (_items != null)
                    foreach (var i in _items)
                    {
                        i.PropertyChanged -= PlanItem_PropertyChanged;
                    }
                base.IsDisposed = true;
            }
        }

        /// <summary>
        /// Finalizer, just in case
        /// </summary>
        ~CustomAttributeDataMultiView()
        {
            Dispose(false);
        }

        #endregion

        #region Static Helpers

        private static PropertyPath GetPropertyPath(System.Linq.Expressions.Expression<Func<CustomAttributeDataMultiView, Object>> expression)
        {
            return WpfHelper.GetPropertyPath<CustomAttributeDataMultiView>(expression);
        }

        #endregion
    }
}
