﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24261: L.Hodson
//  Created
#endregion

#region Version History : CCM 802
// V8-27433 : A.Kuszyk
//  Added Path for repository plans.
#endregion

#region Version History : CCM 810
// V8-30224 : M.Pettit
//  SaveAs removes the original list entry
#endregion

#region Version History : CCM 820
// V8-31140 : L.Ineson
//  Added additional repository location detail.
#endregion

#endregion

using System;
using System.Diagnostics;
using System.Linq;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;


namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// View of RecentPlanogramList
    /// </summary>
    public sealed class RecentPlanogramListView : ViewStateListObject<RecentPlanogramList, RecentPlanogram>
    {
        #region Fields
        const Int32 _maxItemCount = 20;
        #endregion

        #region Properties

        /// <summary>
        /// Returns the maximum number of items that may be held by the model list.
        /// If 0 then there is no limit.
        /// </summary>
        public Int32 MaxItemCount
        {
            get { return _maxItemCount; }
        }

        #endregion

        #region Constructor
        public RecentPlanogramListView()
        {
            App.ViewState.EntityIdChanged += ViewState_EntityIdChanged;
        }
        #endregion

        #region Methods

        /// <summary>
        /// Fetches a model containing all items.
        /// </summary>
        public void FetchAll()
        {
            Fetch(new RecentPlanogramList.FetchAllCriteria());
        }

        /// <summary>
        /// Saves the model synchronously
        /// </summary>
        public void Save()
        {
            if (Model == null) return;

            Update();
        }

        /// <summary>
        /// Adds a new mru entry for the given package or brings the current one to the top.
        /// </summary>
        /// <param name="package"></param>
        /// <param name="filePath"></param>
        public void AddFromFileSystem(String packageName, String fileName)
        {
            Debug.Assert(Model != null, "No model loaded");
            if (Model == null) return;

            //add the item to the model list
            this.Model.AddOrUpdateFileItem(packageName, fileName);

            //ensure the max item count is correct
            this.Model.EnforceMaxItemCount(_maxItemCount);
        }

        /// <summary>
        /// Adds a new entry for a repository planogram.
        /// </summary>
        /// <param name="planId"></param>
        /// <param name="planName"></param>
        public void AddFromRepository(Object planId, String planName, String repositoryFolderPath)
        {
            Debug.Assert(Model != null, "No model loaded");
            if (Model == null) return;


            // Get the plan hierarchy path of the the plan's plan group.
            String path = repositoryFolderPath;
            if (String.IsNullOrWhiteSpace(repositoryFolderPath))
            {
                try
                {
                    PlanogramInfo planInfo = PlanogramInfoList.FetchByIds(new[] { Convert.ToInt32(planId) }).FirstOrDefault();
                    if (planInfo == null || !planInfo.PlanogramGroupId.HasValue) return;

                    PlanogramGroup planGroup = PlanogramHierarchy.FetchByEntityId(App.ViewState.EntityId).
                    EnumerateAllGroups().FirstOrDefault(g => g.Id == planInfo.PlanogramGroupId.Value);

                    path = planGroup.GetFriendlyFullPath();
                }
                catch (DataPortalException ex)
                {
                    CommonHelper.RecordException(ex);
                }
            }


            //add the item to the model list
            this.Model.AddOrUpdateRepositoryItem(
                App.ViewState.ServerName, App.ViewState.DatabaseName, App.ViewState.EntityId, planId,
                planName, path);

            //ensure the max item count is correct
            this.Model.EnforceMaxItemCount(_maxItemCount);

        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the id of the connected entity changes.
        /// </summary>
        private void ViewState_EntityIdChanged(object sender, EventArgs e)
        {
            if (this.Model == null) return;

            //update the repository status flags.
            this.Model.UpdateIsCurrentRepositoryStatus(
                App.ViewState.ServerName, App.ViewState.DatabaseName, App.ViewState.EntityId);
        }

        /// <summary>
        /// Called whenever the model changes.
        /// </summary>
        protected override void RaiseModelChangedEvent(ViewStateObjectModelChangedEventArgs<RecentPlanogramList> e)
        {
            //update the repository status flags.
            if (e.NewModel != null)
            {
                e.NewModel.UpdateIsCurrentRepositoryStatus(App.ViewState.ServerName, App.ViewState.DatabaseName, App.ViewState.EntityId);
            }

            base.RaiseModelChangedEvent(e);
        }

        protected override void OnDataPortalException(Exception error)
        {
            //just log and dont worry.
            CommonHelper.RecordException(error);
            this.Model = null;
        }

        #endregion

        #region IDisposable

        protected override void Dispose(bool isDisposing)
        {
            App.ViewState.EntityIdChanged -= ViewState_EntityIdChanged;
            base.Dispose(isDisposing);
        }

        #endregion
    }
}
