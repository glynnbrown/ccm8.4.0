﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
//V8-26787 : L.Ineson
//  Changes to planitemfield
// V8-25905 : A.Probyn
//  Updated MoveAssembly to relink products and annotations.
#endregion

#region Version History : CCM 802
// V8-28932 : A.Kuszyk
//  Added copy/restore to SetBase() so that fixture bases are not deleted when showing/hiding them.
#endregion

#region Version History : CCM 803
// V8-29726 : L.Ineson
//  Corrected SetBase to also take a copy of the planogram component.
//  Amended SetBackboard to behave the same way
#endregion

#region Version History : CCM 820
// V8-31384 : M.Shelley
//  Add collision detection so when a base is added to a fixture, any components that might collide
//  with the base are moved to prevent this. 
#endregion

#region Version History: (CCM 8.3.0)
// V8-29527 : M.Shelley
//  Added support for multiple bay editing
//V8-31832 : L.Ineson
// Implemented ICalculatedValueResolver
// V8-32518 : N.Haywood
//  Added moveToWorldPos for addcomponentcopy
//V8-32592 : L.Ineson
//  Made sure that addcomponent copy also takes annotations.
// V8-32782 : L.Ineson
// Plan fixture now gets passed into constructor
#endregion

#endregion

using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Represents a view of a PlanogramFixture.
    /// </summary>
    public sealed class PlanogramFixtureView : PlanogramFixtureViewModelBase, IPlanItem
    {
        #region Fields

        private readonly PlanogramView _parentPlanogramView;
        private CastedObservableCollection<PlanogramAssemblyView, PlanogramAssemblyViewModelBase> _assembliesRO;
        private CastedObservableCollection<PlanogramComponentView, PlanogramComponentViewModelBase> _componentsRO;
        private CastedObservableCollection<PlanogramAnnotationView, PlanogramAnnotationViewModelBase> _annotationsRO;

        private PlanogramFixtureComponent _oldBaseFixtureComponent;
        private PlanogramComponent _oldBaseComponent;

        private PlanogramFixtureComponent _oldBackboardFixtureComponent;
        private PlanogramComponent _oldBackboardComponent;

        private Boolean _isSelected = false;
        private CalculatedValueResolver _calculatedValueResolver;

        #endregion

        #region Property Paths

        public static readonly PropertyPath HasBaseProperty = WpfHelper.GetPropertyPath<PlanogramFixtureView>(p => p.HasBase);
        public static readonly PropertyPath BaseComponentProperty = WpfHelper.GetPropertyPath<PlanogramFixtureView>(p => p.BaseComponent);
        public static readonly PropertyPath HasBackboardProperty = WpfHelper.GetPropertyPath<PlanogramFixtureView>(p => p.HasBackboard);
        public static readonly PropertyPath BackboardComponentProperty = WpfHelper.GetPropertyPath<PlanogramFixtureView>(p => p.BackboardComponent);

        public static readonly PropertyPath IsSelectedProperty = WpfHelper.GetPropertyPath<PlanogramFixtureView>(p => p.IsSelected);

        #endregion

        #region Properties

        /// <summary>
        /// Returns direct assembly children of this fixture.
        /// </summary>
        public CastedObservableCollection<PlanogramAssemblyView, PlanogramAssemblyViewModelBase> Assemblies
        {
            get
            {
                if (_assembliesRO == null)
                {
                    _assembliesRO =
                        new CastedObservableCollection<PlanogramAssemblyView, PlanogramAssemblyViewModelBase>
                            (base.AssemblyViews, true);
                }
                return _assembliesRO;
            }
        }

        /// <summary>
        /// Returns direct component children of this fixture.
        /// </summary>
        public CastedObservableCollection<PlanogramComponentView, PlanogramComponentViewModelBase> Components
        {
            get
            {
                if (_componentsRO == null)
                {
                    _componentsRO =
                        new CastedObservableCollection<PlanogramComponentView, PlanogramComponentViewModelBase>
                            (base.ComponentViews, true);
                }
                return _componentsRO;
            }
        }

        /// <summary>
        /// Returns the collection of child annotation views.
        /// </summary>
        public CastedObservableCollection<PlanogramAnnotationView, PlanogramAnnotationViewModelBase> Annotations
        {
            get
            {
                if (_annotationsRO == null)
                {
                    _annotationsRO =
                        new CastedObservableCollection<PlanogramAnnotationView, PlanogramAnnotationViewModelBase>
                            (base.AnnotationViews, true);
                }
                return _annotationsRO;
            }
        }


        /// <summary>
        /// Gets/Sets whether this bay has a backboard.
        /// </summary>
        public Boolean HasBase
        {
            get { return this.BaseComponent != null; }
            set { SetBase(value); }
        }

        /// <summary>
        /// Returns the base component for this bay
        /// or null if it does not have one set.
        /// </summary>
        public PlanogramComponentView BaseComponent
        {
            get
            {
                return this.EnumerateAllComponents()
                .FirstOrDefault(c => c.ComponentType == PlanogramComponentType.Base);
            }
        }


        /// <summary>
        /// Gets/Sets whether this bay has a backboard.
        /// </summary>
        public Boolean HasBackboard
        {
            get { return this.BackboardComponent != null; }
            set { SetBackboard(value); }
        }

        /// <summary>
        /// Returns the backboard component for this bay
        /// or null if it does not have one set.
        /// </summary>
        public PlanogramComponentView BackboardComponent
        {
            get
            {
                return this.EnumerateAllComponents()
               .FirstOrDefault(c => c.ComponentType == PlanogramComponentType.Backboard);
            }
        }

        public Boolean IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                OnPropertyChanged(IsSelectedProperty.Path);
            }
        }

        /// <summary>
        /// Returns the resolver used for binding to calculated values.
        /// </summary>
        public CalculatedValueResolver CalculatedValueResolver
        {
            get
            {
                if (_calculatedValueResolver == null)
                {
                    _calculatedValueResolver =
                        new CalculatedValueResolver(GetCalculatedValue, /*enableCaching*/true);
                }
                return _calculatedValueResolver;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="parentPlanogramView"></param>
        /// <param name="fixtureItem"></param>
        public PlanogramFixtureView(PlanogramView parentPlanogramView, PlanogramFixtureItem fixtureItem, PlanogramFixture fixture)
            : base(parentPlanogramView, fixtureItem, fixture)
        {
            _parentPlanogramView = parentPlanogramView;

            InitializeChildViews();
        }

        #endregion

        #region Methods

        #region Child Assembly Methods

        /// <summary>
        /// Creates a new viewmodel for the given fixture assembly.
        /// </summary>
        /// <param name="fixtureAssembly"></param>
        /// <returns></returns>
        protected override PlanogramAssemblyViewModelBase CreateAssemblyView(PlanogramFixtureAssembly fixtureAssembly)
        {
            PlanogramAssembly assembly = fixtureAssembly.GetPlanogramAssembly();
            if (assembly == null) return null;
            return new PlanogramAssemblyView(this, fixtureAssembly, assembly);
        }

        /// <summary>
        /// Adds a new child assembly
        /// </summary>
        public PlanogramAssemblyView AddAssembly()
        {
            PlanogramFixtureAssembly fixtureAssembly = this.FixtureModel.Assemblies.Add();
            return this.Assemblies.LastOrDefault(a => a.FixtureAssemblyModel == fixtureAssembly);
        }

        /// <summary>
        /// Adds a copy of the given assembly
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        public PlanogramAssemblyView AddAssemblyCopy(PlanogramAssemblyView src, Boolean copyPositions = true)
        {
            Planogram planModel = this.FixtureModel.Parent;

            //create shallow model copies
            PlanogramAssembly assemblyCopy = src.AssemblyModel.Copy();
            assemblyCopy.Components.Clear();
            assemblyCopy.Name = String.Format(CultureInfo.CurrentCulture, Message.NewPlanogramAssemblyName, this.Planogram.Model.Assemblies.Count);
            planModel.Assemblies.Add(assemblyCopy);

            PlanogramFixtureAssembly fixtureAssemblyCopy = src.FixtureAssemblyModel.Copy();
            fixtureAssemblyCopy.PlanogramAssemblyId = assemblyCopy.Id;
            this.FixtureModel.Assemblies.Add(fixtureAssemblyCopy);

            PlanogramAssemblyView view = this.Assemblies.LastOrDefault();
            view.IsSelectable = src.IsSelectable;


            //copy components
            foreach (var childSrc in src.Components)
            {
                view.AddComponentCopy(childSrc, copyPositions);
            }

            return view;
        }

        /// <summary>
        /// Adds a new assembly comprised of the given components.
        /// </summary>
        /// <param name="fixtureComponents"></param>
        /// <returns></returns>
        public PlanogramAssemblyView AddAssembly(List<PlanogramComponentView> fixtureComponents)
        {
            PlanogramFixtureAssembly fixtureAssembly =
            this.FixtureModel.Assemblies.Add(
                fixtureComponents.Select(c => 
                new Tuple<PlanogramFixtureComponent, PlanogramFixtureItem>(c.FixtureComponentModel,c.Fixture.FixtureItemModel)),
                this.FixtureItemModel);

            return this.Assemblies.LastOrDefault(a => a.FixtureAssemblyModel == fixtureAssembly);
        }

        /// <summary>
        /// Removes the given assembly
        /// </summary>
        /// <param name="assemblyView"></param>
        public void RemoveAssembly(PlanogramAssemblyView assemblyView)
        {
            Debug.Assert(this.Assemblies.Contains(assemblyView), "Deleting assembly from wrong fixture");
            if (!this.Assemblies.Contains(assemblyView)) return;

            using (var undoAction = this.Planogram.NewUndoableAction(supressPlanUpdates:true))
            {
                //remove this assembly and all of its dependents from the planogram.
                this.Planogram.Model.Assemblies.RemoveAssemblyAndDependants(assemblyView.AssemblyModel);
            }
        }

        /// <summary>
        /// Moves the given assembly from its current fixture to this one.
        /// </summary>
        public PlanogramAssemblyView MoveAssembly(PlanogramAssemblyView assemblyView)
        {
            Boolean isUpdatingPlan = !this.ParentPlanogramView.IsUpdating;
            if (isUpdatingPlan) this.ParentPlanogramView.BeginUpdate();

            //Move the component and all its dependants
            PlanogramFixtureAssembly newFixtureAssembly =
                this.FixtureItemModel.MoveAssembly(
                assemblyView.FixtureAssemblyModel,
                assemblyView.Fixture.FixtureItemModel);

            if (isUpdatingPlan) this.ParentPlanogramView.EndUpdate();


            //refind the view
            PlanogramAssemblyView newView =
                this.Assemblies.FirstOrDefault(c => c.FixtureAssemblyModel == newFixtureAssembly);
            if (newView != null) newView.IsSelectable = assemblyView.IsSelectable;

            return newView;
        }

        #endregion

        #region Child Component Methods

        /// <summary>
        /// Creates  a new view of the given component.
        /// </summary>
        /// <param name="fixtureComponent"></param>
        /// <returns></returns>
        protected override PlanogramComponentViewModelBase CreateComponentView(PlanogramFixtureComponent fixtureComponent)
        {
            PlanogramComponent component = fixtureComponent.GetPlanogramComponent();
            if (component == null) return null;
            return new PlanogramComponentView(this, fixtureComponent, component);
        }

        /// <summary>
        /// Adds a new component setup as per defaults of the given type.
        /// </summary>
        public PlanogramComponentView AddEmptyCustomComponent(Single height, Single width, Single depth)
        {
            // get the new component number
            Int32 componentNo = this.Planogram.Model.Components.Where(c => c.ComponentType == PlanogramComponentType.Custom).Count();

            PlanogramComponent component = PlanogramComponent.NewPlanogramComponent();
            component.Name = String.Format("{0} {1}", PlanogramComponentTypeHelper.FriendlyNames[PlanogramComponentType.Custom], componentNo);
            component.Height = height;
            component.Width = width;
            component.Depth = depth;
            this.Planogram.Model.Components.Add(component);

            PlanogramFixtureComponent fc = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);
            this.FixtureModel.Components.Add(fc);

            PlanogramComponentView view = this.Components.First(c => c.FixtureComponentModel == fc);
            view.IsSelectable = true;
            return view;
        }

        /// <summary>
        /// Adds a new component setup as per defaults of the given type.
        /// </summary>
        public PlanogramComponentView AddComponent(PlanogramComponentType componentType)
        {
            PlanogramFixtureComponent fc = this.FixtureModel.Components.Add(componentType, App.ViewState.Settings.Model);
            PlanogramComponentView view = this.Components.First(c => c.FixtureComponentModel == fc);
            PlanogramView.AdjustDefaultsForUOM(App.ViewState.Settings.Model.LengthUnitOfMeasure, view.ComponentModel);
            view.IsSelectable = true;
            return view;
        }

        /// <summary>
        /// Adds a new component based on the existing component
        /// </summary>
        public PlanogramComponentView AddComponentCopy(PlanogramComponentView src, Boolean copyPositions = true, Boolean moveToWorldPos = false)
        {
            Planogram planModel = this.FixtureModel.Parent;

            //create shallow model copies
            PlanogramComponent componentCopy = src.ComponentModel.Copy();
            componentCopy.Name = String.Format(CultureInfo.CurrentCulture, Message.NewPlanogramComponentName, (this.Planogram.Model.Components.Count));
            componentCopy.SubComponents.Clear();
            planModel.Components.Add(componentCopy);

            if (src.FixtureComponentModel != null)
            {
                PlanogramFixtureComponent fixtureComponentCopy = src.FixtureComponentModel.Copy();
                fixtureComponentCopy.PlanogramComponentId = componentCopy.Id;
                if (moveToWorldPos)
                {
                    fixtureComponentCopy.X = fixtureComponentCopy.MetaWorldX.Value;
                    fixtureComponentCopy.Y = fixtureComponentCopy.MetaWorldY.Value;
                    fixtureComponentCopy.Z = fixtureComponentCopy.MetaWorldZ.Value;
                }
                this.FixtureModel.Components.Add(fixtureComponentCopy);
            }
            else
            {
                PlanogramAssemblyComponent srcAssemblyComponent = src.AssemblyComponentModel;

                PlanogramFixtureComponent fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent();
                fixtureComponent.PlanogramComponentId = componentCopy.Id;
                fixtureComponent.X = srcAssemblyComponent.X;
                fixtureComponent.Y = srcAssemblyComponent.Y;
                fixtureComponent.Z = srcAssemblyComponent.Z;
                if (moveToWorldPos)
                {
                    fixtureComponent.X = srcAssemblyComponent.MetaWorldX.Value;
                    fixtureComponent.Y = srcAssemblyComponent.MetaWorldY.Value;
                    fixtureComponent.Z = srcAssemblyComponent.MetaWorldZ.Value;
                }
                fixtureComponent.Slope = srcAssemblyComponent.Slope;
                fixtureComponent.Angle = srcAssemblyComponent.Angle;
                fixtureComponent.Roll = srcAssemblyComponent.Roll;
                this.FixtureModel.Components.Add(fixtureComponent);
            }

            PlanogramComponentView view = this.Components.Last();
            view.IsSelectable = src.IsSelectable;

            Int32 componentNum = view.Planogram.Model.Components.Where(c => c.ComponentType == src.ComponentType).Count();
            view.Name = String.Format("{0} {1}", PlanogramComponentTypeHelper.FriendlyNames[src.ComponentType], componentNum);


            //copy subcomponents
            foreach (PlanogramSubComponentView sub in src.SubComponents)
            {
                view.AddSubComponentCopy(sub, copyPositions);
            }


            //copy annotations
            foreach (PlanogramAnnotationView anno in src.Annotations.ToArray())
            {
                view.AddAnnotationCopy(anno);
            }


            return view;
        }

        /// <summary>
        /// Adds a new FixtureComponent linked to the given component
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        private PlanogramComponentView AddComponent(PlanogramComponent component)
        {
            PlanogramFixtureComponent fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent();
            fixtureComponent.PlanogramComponentId = component.Id;
            FixtureModel.Components.Add(fixtureComponent);

            return this.Components.LastOrDefault();
        }

        /// <summary>
        /// Removes the given component from this fixture
        /// </summary>
        /// <param name="componentView"></param>
        public void RemoveComponent(PlanogramComponentView componentView)
        {
            Debug.Assert(this.Components.Contains(componentView), "Deleting component from wrong fixture");
            if (!this.Components.Contains(componentView)) return;

            using (var undoAction = this.Planogram.NewUndoableAction(supressPlanUpdates: true))
            {
                //remove the component and all of its dependants from the plan
                this.Planogram.Model.Components.RemoveComponentAndDependants(componentView.ComponentModel);
            }
        }

        /// <summary>
        /// Moves the given component from its current fixture to this one.
        /// </summary>
        /// <param name="component"></param>
        public PlanogramComponentView MoveComponent(PlanogramComponentView componentView)
        {
            Boolean isUpdatingPlan = !this.ParentPlanogramView.IsUpdating;
            if (isUpdatingPlan) this.ParentPlanogramView.BeginUpdate();

            //Move the component and all its dependants
            PlanogramFixtureComponent newFixtureComponent = null;
            if (componentView.FixtureComponentModel != null)
            {
                newFixtureComponent = 
                    this.FixtureItemModel.MoveComponent(
                    componentView.FixtureComponentModel,
                    componentView.Fixture.FixtureItemModel);
            }
            else if (componentView.AssemblyComponentModel != null)
            {
                newFixtureComponent =
                this.FixtureItemModel.MoveComponent(
                    componentView.AssemblyComponentModel,
                    componentView.Assembly.FixtureAssemblyModel,
                    componentView.Fixture.FixtureItemModel);
            }

            if (isUpdatingPlan) this.ParentPlanogramView.EndUpdate();


            //refind the component view
            PlanogramComponentView newView = 
                this.Components.FirstOrDefault(c => c.FixtureComponentModel == newFixtureComponent);
            if(newView != null)  newView.IsSelectable = componentView.IsSelectable;

            return newView;
        }

        /// <summary>
        /// Returns all components under this fixture.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanogramComponentView> EnumerateAllComponents()
        {
            foreach (PlanogramAssemblyView assembly in this.Assemblies)
            {
                foreach (PlanogramComponentView c in assembly.Components)
                {
                    yield return c;
                }
            }

            foreach (PlanogramComponentView c in this.Components)
            {
                yield return c;
            }
        }

        /// <summary>
        /// Returns all subcomponents under this fixture.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanogramSubComponentView> EnumerateAllSubcomponents()
        {
            foreach (PlanogramAssemblyView assembly in this.Assemblies)
            {
                foreach (PlanogramComponentView c in assembly.Components)
                {
                    foreach (PlanogramSubComponentView s in c.SubComponents)
                    {
                        yield return s;
                    }
                }
            }

            foreach (PlanogramComponentView c in this.Components)
            {
                foreach (PlanogramSubComponentView s in c.SubComponents)
                {
                    yield return s;
                }
            }
        }

        /// <summary>
        /// Adds or removes the backboard according to the param
        /// </summary>
        /// <param name="hasBackboard">if true this fixture should have a backboard</param>
        /// <returns></returns>
        public PlanogramComponentView SetBackboard(Boolean hasBackboard)
        {
            PlanogramComponentView backboard =
                this.EnumerateAllComponents().FirstOrDefault(c => c.ComponentType == PlanogramComponentType.Backboard);

            if (hasBackboard && backboard == null)
            {
                //we should have a backboard but don't, so add one.
                BeginUpdate();

                if (_oldBackboardFixtureComponent == null)
                {
                    //add a backboard
                    backboard = AddComponent(PlanogramComponentType.Backboard);
                    PlanogramView.AdjustDefaultsForUOM(this.Planogram.LengthUnitsOfMeasure, backboard.ComponentModel);
                    backboard.IsSelectable = false;
                    backboard.Width = this.Width;
                    backboard.Height = this.Height;

                    //offset by the the depth
                    backboard.Z = -backboard.Depth;
                }
                else
                {
                    //restore the old one.
                    //add the component back into the planogram
                    this.FixtureItemModel.Parent.Components.Add(_oldBackboardComponent);
                    _oldBackboardFixtureComponent.PlanogramComponentId = _oldBackboardComponent.Id;

                    //add the fixture component back
                    this.FixtureModel.Components.Add(_oldBackboardFixtureComponent);
                    backboard = this.Components.FirstOrDefault(c => c.FixtureComponentModel == _oldBackboardFixtureComponent);
                    if (backboard != null) backboard.IsSelectable = true;

                    _oldBackboardComponent = null;
                    _oldBackboardFixtureComponent = null;
                }

                EndUpdate();
            }
            else if (!hasBackboard && backboard != null)
            {

                // Take a copy of the current fixture backboard to restore later.
                _oldBackboardComponent = backboard.ComponentModel.Copy();
                _oldBackboardFixtureComponent = backboard.FixtureComponentModel.Copy();


                //we have a backboard but shouldn't so remove it
                RemoveComponent(backboard);
                backboard = null;
            }

            OnPropertyChanged(HasBackboardProperty);
            OnPropertyChanged(BackboardComponentProperty);

            return backboard;
        }

        /// <summary>
        /// Adds or removes the fixture base according to the param
        /// </summary>
        /// <param name="hasBackboard">if true this fixture should have a base</param>
        /// <returns></returns>
        public PlanogramComponentView SetBase(Boolean hasBase)
        {
            PlanogramComponentView fixtureBase =
                this.EnumerateAllComponents().FirstOrDefault(c => c.ComponentType == PlanogramComponentType.Base);

            if (hasBase && fixtureBase == null)
            {
                //we should have a base but don't, so add one.
                BeginUpdate();

                // If no previous fixture base has been saved, add a new one.
                if (_oldBaseFixtureComponent == null)
                {
                    fixtureBase = AddComponent(PlanogramComponentType.Base);
                    PlanogramView.AdjustDefaultsForUOM(this.Planogram.LengthUnitsOfMeasure, fixtureBase.ComponentModel);
                    fixtureBase.IsSelectable = false;
                    fixtureBase.Width = this.Width;
                }
                else
                {
                    //add the component back into the planogram
                    this.FixtureItemModel.Parent.Components.Add(_oldBaseComponent);
                    _oldBaseFixtureComponent.PlanogramComponentId = _oldBaseComponent.Id;

                    //add the fixture component back
                    this.FixtureModel.Components.Add(_oldBaseFixtureComponent);
                    fixtureBase = this.Components.FirstOrDefault(c => c.FixtureComponentModel == _oldBaseFixtureComponent);
                    if (fixtureBase != null) fixtureBase.IsSelectable = true;

                    _oldBaseComponent = null;
                    _oldBaseFixtureComponent = null;
                }

                float maxTranslate = 0F;
                var fixtureBaseRect = fixtureBase.GetWorldSpaceBounds();

                // Check if there are any collisions between existing components and the "new" base.
                var allComps = this.EnumerateAllComponents().ToList();

                foreach (var subComp in allComps)
                {
                    // Check we are not testing for an intersection with the fixture base itself
                    if (subComp.Equals(fixtureBase))
                    {
                        continue;
                    }

                    // Get the bounding values
                    var subCompRect = subComp.GetWorldSpaceBounds();
                    var doesCollide = fixtureBaseRect.CollidesWith(subCompRect);

                    if (doesCollide)
                    {
                        // If there is a collision, translate the sub-component so there is 
                        // no longer a collision
                        var intersectRect = fixtureBaseRect.Intersect(subCompRect);

                        if (intersectRect != null)
                        {
                            // Get the top of the base
                            float baseTop = fixtureBaseRect.Y + fixtureBaseRect.Height;

                            // and caluclate how much to offset the sub-component
                            float subCompOffset = baseTop + (subComp.Y - fixtureBaseRect.Y);

                            subComp.Y = subCompOffset;
                        }
                    }
                }

                //snap the base to the backboard
                if (fixtureBase != null)
                {
                    PlanogramComponentView backboard =
                        this.EnumerateAllComponents().FirstOrDefault(c => c.ComponentType == PlanogramComponentType.Backboard);
                    if (backboard != null)
                    {
                        fixtureBase.X = backboard.X;
                        fixtureBase.Y = backboard.Y;
                        fixtureBase.Z = backboard.Z + backboard.Depth;
                    }
                }

                EndUpdate();
            }
            else if (!hasBase && fixtureBase != null)
            {
                // Take a copy of the current fixture base to restore later.
                _oldBaseComponent = fixtureBase.ComponentModel.Copy();
                _oldBaseFixtureComponent = fixtureBase.FixtureComponentModel.Copy();

                //we have a base but shouldn't so remove it
                RemoveComponent(fixtureBase);
                fixtureBase = null;
            }

            OnPropertyChanged(HasBaseProperty);
            OnPropertyChanged(BaseComponentProperty);

            return fixtureBase;
        }

        /// <summary>
        /// Utility method to determine which component types are allowed to be resized when the fixture width is changed.
        /// </summary>
        /// <param name="componentItem">The planogram component view object to check</param>
        /// <returns>True if the component can be resized, false otherwise</returns>
        private Boolean IsResizeable(PlanogramComponentView componentItem)
        {
            if (componentItem == null) return false;

            switch (componentItem.ComponentType)
            {
                // These items are not resizeable here but may be resized elsewhere
                case PlanogramComponentType.Backboard:
                case PlanogramComponentType.Base:
                case PlanogramComponentType.Custom:
                    return false;

                default:
                    return true;
            }
        }

        /// <summary>
        /// This method iterates through the components of a fixture and any component that has
        /// the width specified in oldWidth is changed so that it matches the current width of
        /// the fixture (either enlarging or shrinking the widths).
        /// </summary>
        /// <returns></returns>
        public Boolean ResizeComponents()
        {
            Boolean resultStatus = true;

            foreach (var compItem in this.Components)
            {
                if (compItem.Width.EqualTo(this.PreviousWidth, 3) && IsResizeable(compItem))
                {
                    compItem.Width = this.Width;
                }
            }

            return resultStatus;
        }

        public Boolean ResizeAssemblies()
        {
            Boolean resultStatus = true;

            // For each component in each assembly, check if the component width matches the 
            // "old" width and if so, resize accordingly
            foreach (var assemblyItem in this.Assemblies)
            {
                foreach (var assemblyCompItem in assemblyItem.Components)
                {
                    if (assemblyCompItem.Width.EqualTo(this.PreviousWidth, 3) && IsResizeable(assemblyCompItem))
                    {
                        assemblyCompItem.Width = this.Width;
                    }
                }
            }

            return resultStatus;
        }

        #endregion

        #region Child Annotation Methods

        /// <summary>
        /// Creates a new view of the given annotation
        /// </summary>
        /// <param name="fixtureAnnotation"></param>
        /// <returns></returns>
        protected override PlanogramAnnotationViewModelBase CreateAnnotationView(PlanogramAnnotation fixtureAnnotation)
        {
            return new PlanogramAnnotationView(this, fixtureAnnotation);
        }

        /// <summary>
        /// Adds a new annotation to this fixture
        /// </summary>
        /// <returns></returns>
        public PlanogramAnnotationView AddAnnotation()
        {
            PlanogramAnnotation anno = PlanogramAnnotation.NewPlanogramAnnotation(App.ViewState.Settings.Model);
            if (this.Planogram != null && this.Planogram.LengthUnitsOfMeasure == PlanogramLengthUnitOfMeasureType.Inches)
            {
                //temp - correct uom for inches.
                anno.BorderThickness = 0.2F;
            }
            AssociateAnnotation(anno);

            this.Planogram.Model.Annotations.Add(anno);

            return this.Annotations.Last();
        }

        /// <summary>
        /// Adds a copy of the given annotation to this fixture.
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        public PlanogramAnnotationView AddAnnotationCopy(PlanogramAnnotationView src)
        {
            PlanogramAnnotation annoModel = src.Model.Copy();
            AssociateAnnotation(annoModel);

            this.Planogram.Model.Annotations.Add(annoModel);

            return this.Annotations.Last();
        }

        /// <summary>
        /// Removes the given annotation model from this fixture
        /// </summary>
        /// <param name="annoView"></param>
        public void RemoveAnnotation(PlanogramAnnotationView annoView)
        {
            if (this.Annotations.Contains(annoView))
            {
                this.Planogram.Model.Annotations.Remove(annoView.Model);
            }
        }

        /// <summary>
        /// Links the given annotation model to this fixture.
        /// </summary>
        /// <param name="annoModel"></param>
        private void AssociateAnnotation(PlanogramAnnotation annoModel)
        {
            annoModel.PlanogramFixtureItemId = this.FixtureItemModel.Id;
            annoModel.PlanogramFixtureAssemblyId = null;
            annoModel.PlanogramFixtureComponentId = null;
            annoModel.PlanogramAssemblyComponentId = null;
            annoModel.PlanogramSubComponentId = null;
            annoModel.PlanogramPositionId = null;
        }

        /// <summary>
        /// Moves the given annotation from its current fixture to this one.
        /// If this was a component level annotation then it will be unlinked.
        /// </summary>
        public PlanogramAnnotationView MoveAnnotation(PlanogramAnnotationView annoView)
        {
            PlanogramAnnotation annotation = annoView.Model;

            //remove the view from the original fixture
            //annoView.Fixture.AnnotationViews.Remove(annoView);

            PointValue worldPos = new PointValue(annoView.WorldX, annoView.WorldY, annoView.WorldZ);

            //relink the model to this.
            AssociateAnnotation(annotation);

            //correct its world position
            annoView.WorldX = worldPos.X;
            annoView.WorldY = worldPos.Y;
            annoView.WorldZ = worldPos.Z;

            //get the new view from this.
            PlanogramAnnotationView newView = this.Annotations.FirstOrDefault(f => f.Model == annotation);
            newView.IsSelectable = annoView.IsSelectable;


            ////add a new anno view to this.
            //PlanogramAnnotationView newView = new PlanogramAnnotationView(this, annotation);
            //AnnotationViews.Add(newView);

            //newView.IsSelectable = annoView.IsSelectable;

            return newView;

        }

        #endregion


        /// <summary>
        /// Resolves the given expression for this item
        /// </summary>
        public Object GetCalculatedValue(String expressionText)
        {
            return PlanogramFieldHelper.ResolveFieldExpression(expressionText,
                PlanogramItemPlacement.NewPlanogramItemPlacement(
                fixture: this.FixtureModel,
                fixtureItem: this.FixtureItemModel,
                planogram: this.Planogram.Model
                ));
        }

        /// <summary>
        /// Called by the parent planogram to notify that processing has completed
        /// </summary>
        internal void NotifyPlanProcessCompleting()
        {
            //if we have calculated values cached then update them now.
            if (_calculatedValueResolver != null) _calculatedValueResolver.RefreshValues();

            //notify all child views
            foreach (var component in this.Components)
            {
                component.NotifyPlanProcessCompleting();
            }

            foreach (var assembly in this.Assemblies)
            {
                assembly.NotifyPlanProcessCompleting();
            }

            foreach (var anno in this.Annotations)
            {
                anno.NotifyPlanProcessCompleting();
            }
        }

        /// <summary>
        /// Raises the property changed event for the given property
        /// </summary>
        /// <param name="property"></param>
        private void OnPropertyChanged(PropertyPath property)
        {
            OnPropertyChanged(property.Path);
        }

        #endregion

        #region IPlanItem Members

        public PlanogramView Planogram
        {
            get { return _parentPlanogramView; }
        }

        PlanogramFixtureView IPlanItem.Fixture
        {
            get { return this; }
        }

        PlanogramAssemblyView IPlanItem.Assembly
        {
            get { return null; }
        }

        PlanogramComponentView IPlanItem.Component
        {
            get { return null; }
        }

        PlanogramSubComponentView IPlanItem.SubComponent
        {
            get { return null; }
        }

        PlanogramPositionView IPlanItem.Position
        {
            get { return null; }
        }

        PlanogramProductView IPlanItem.Product
        {
            get { return null; }
        }

        PlanogramAnnotationView IPlanItem.Annotation
        {
            get { return null; }
        }

        public PlanItemType PlanItemType
        {
            get { return Common.PlanItemType.Fixture; }
        }

        public Boolean IsSelectable
        {
            get { return true; } //always true.
            set { }
        }

        #endregion

        #region IDisposable

        protected override void OnDisposing(bool disposing)
        {
            if (_calculatedValueResolver != null) _calculatedValueResolver.Dispose();
            base.OnDisposing(disposing);
        }

        #endregion
    }

}
