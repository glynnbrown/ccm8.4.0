﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26149 : L.Hodson
//  Created
#endregion
#region Version History: (CCM 8.3)
// V8-32523 : L.Ineson
//  Added property for the linked component name
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Converters;
using Galleria.Framework.Attributes;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Provides a merged view of multiple PlanogramAnnotationViews
    /// </summary>
    public sealed class PlanogramAnnotationMultiView : ViewModelObject, IDataErrorInfo
    {
        #region Fields

        private IEnumerable<PlanogramAnnotationView> _items;

        private PlanogramView _plan;
        private Boolean _isLoggingUndoableActions = true;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath TextProperty = GetPropertyPath(p => p.Text);
        public static readonly PropertyPath WorldXProperty = GetPropertyPath(p => p.WorldX);
        public static readonly PropertyPath WorldYProperty = GetPropertyPath(p => p.WorldY);
        public static readonly PropertyPath WorldZProperty = GetPropertyPath(p => p.WorldZ);
        public static readonly PropertyPath HeightProperty = GetPropertyPath(p => p.Height);
        public static readonly PropertyPath WidthProperty = GetPropertyPath(p => p.Width);
        public static readonly PropertyPath DepthProperty = GetPropertyPath(p => p.Depth);
        public static readonly PropertyPath BackgroundColourProperty = GetPropertyPath(p => p.BackgroundColour);
        public static readonly PropertyPath BorderColourProperty = GetPropertyPath(p => p.BorderColour);
        public static readonly PropertyPath BorderThicknessProperty = GetPropertyPath(p => p.BorderThickness);
        public static readonly PropertyPath FontColourProperty = GetPropertyPath(p => p.FontColour);
        public static readonly PropertyPath FontSizeProperty = GetPropertyPath(p => p.FontSize);
        public static readonly PropertyPath FontNameProperty = GetPropertyPath(p => p.FontName);
        public static readonly PropertyPath CanReduceFontToFitProperty = GetPropertyPath(p => p.CanReduceFontToFit);
        public static readonly PropertyPath LinkedComponentNameProperty = GetPropertyPath(p => p.LinkedComponentName);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets whether changes should be logged as undoable actions.
        /// Note - this requires that a plan was specified on create.
        /// </summary>
        public Boolean IsLoggingUndoableActions
        {
            get { return _plan != null && _isLoggingUndoableActions; }
            set { _isLoggingUndoableActions = value; }
        }

        public IEnumerable<PlanogramAnnotationView> Items
        {
            get
            {
                return _items;
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramAnnotation), "XProperty")]
        public Single? WorldX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAnnotationView.WorldXProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramAnnotationView.WorldXProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramAnnotation), "YProperty")]
        public Single? WorldY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAnnotationView.WorldYProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramAnnotationView.WorldYProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramAnnotation), "ZProperty")]
        public Single? WorldZ
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAnnotationView.WorldZProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramAnnotationView.WorldZProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramAnnotation), "HeightProperty")]
        public Single? Height
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAnnotation.HeightProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramAnnotation.HeightProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramAnnotation), "WidthProperty")]
        public Single? Width
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAnnotation.WidthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramAnnotation.WidthProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramAnnotation), "DepthProperty")]
        public Single? Depth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAnnotation.DepthProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramAnnotation.DepthProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramAnnotation), "TextProperty")]
        public String Text
        {
            get { return GetCommonPropertyValue<String>(Items, PlanogramAnnotation.TextProperty.Name); }
            set { SetPropertyValue(Items, PlanogramAnnotation.TextProperty.Name, value); }
        }

        [ModelObjectDisplayName(typeof(PlanogramAnnotation), "BackgroundColourProperty")]
        public Color BackgroundColour
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, PlanogramAnnotation.BackgroundColourProperty.Name);

                if (val.HasValue)
                {
                    return CommonHelper.IntToColor(val.Value);
                }
                else
                {
                    return Colors.White;
                }
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramAnnotation.BackgroundColourProperty.Name, CommonHelper.ColorToInt(value));
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramAnnotation), "BorderColourProperty")]
        public Color BorderColour
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, PlanogramAnnotation.BorderColourProperty.Name);

                if (val.HasValue)
                {
                    return CommonHelper.IntToColor(val.Value);
                }
                else
                {
                    return Colors.White;
                }
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramAnnotation.BorderColourProperty.Name, CommonHelper.ColorToInt(value));
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramAnnotation), "BorderThicknessProperty")]
        public Single? BorderThickness
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAnnotation.BorderThicknessProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramAnnotation.BorderThicknessProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramAnnotation), "FontColourProperty")]
        public Color FontColour
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, PlanogramAnnotation.FontColourProperty.Name);

                if (val.HasValue)
                {
                    return CommonHelper.IntToColor(val.Value);
                }
                else
                {
                    return Colors.White;
                }
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, PlanogramAnnotation.FontColourProperty.Name, CommonHelper.ColorToInt(value));
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramAnnotation), "FontSizeProperty")]
        public Single? FontSize
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, PlanogramAnnotation.FontSizeProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramAnnotation.FontSizeProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramAnnotation), "FontNameProperty")]
        public String FontName
        {
            get
            {
                return GetCommonPropertyValue<String>(Items, PlanogramAnnotation.FontNameProperty.Name);
            }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    SetPropertyValue(Items, PlanogramAnnotation.FontNameProperty.Name, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramAnnotation), "CanReduceFontToFitProperty")]
        public Boolean? CanReduceFontToFit
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, PlanogramAnnotation.CanReduceFontToFitProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, PlanogramAnnotation.CanReduceFontToFitProperty.Name, value.Value);
                }
            }
        }

        /// <summary>
        /// Returns the name of the linked component.
        /// </summary>
        public String LinkedComponentName
        {
            get 
            {
                if (!Items.Any(a=> a.IsComponentAnnotation)) return null;

                PlanogramComponentView component = 
                    Items.Where(c=> c.IsComponentAnnotation).First().Component;

                if(!Items.Where(c=> c.IsComponentAnnotation).All(a=> a.Component == component))
                {
                    return  Message.PlanogramAnnotationMultiView_MultipleLinkedComponents;
                }
                else 
                {
                    return component.Name;
                }
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="items"></param>
        public PlanogramAnnotationMultiView(IEnumerable<PlanogramAnnotationView> items)
        {
            _items = items.ToList();
            _plan = items.First().Planogram;

            foreach (var i in items)
            {
                i.PropertyChanged += PlanItem_PropertyChanged;
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on any linked plan item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(e.PropertyName);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the common value of the given property for all the items
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        private static T GetCommonPropertyValue<T>(IEnumerable<Object> items, String propertyName)
        {
            T value = default(T);

            if (items.Any())
            {
                Type classType = items.First().GetType();
                System.Reflection.PropertyInfo pInfo = classType.GetProperty(propertyName);
                if (pInfo != null)
                {
                    Boolean isFirst = true;
                    foreach (Object item in items)
                    {
                        T itemValue = (T)pInfo.GetValue(item, null);
                        if (isFirst)
                        {
                            value = itemValue;
                        }
                        else if (!Object.Equals(itemValue, value))
                        {
                            return default(T);
                        }

                        isFirst = false;
                    }
                }
            }

            return value;

        }

        /// <summary>
        /// Sets the given value against
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        private void SetPropertyValue<T>(IEnumerable<Object> items, String propertyName, T value)
        {
            if (items.Any())
            {

                //Start logging the change
                Boolean isProcessingUndoable = false;
                if (IsLoggingUndoableActions)
                {
                    if (!_plan.IsRecordingUndoableAction)
                    {
                        isProcessingUndoable = true;
                        _plan.BeginUndoableAction(propertyName);
                    }
                }

                //make the property change
                Type classType = items.First().GetType();
                System.Reflection.PropertyInfo pInfo = classType.GetProperty(propertyName);

                if (pInfo != null)
                {
                    foreach (Object item in items)
                    {
                        pInfo.SetValue(item, value, null);
                    }
                }

                //Commit the log.
                if (isProcessingUndoable)
                {
                    _plan.EndUndoableAction();
                }
            }
        }


        #endregion

        #region IDataErrorInfo Members

        public String Error
        {
            get { throw new NotImplementedException(); }
        }

        public String this[String columnName]
        {
            get
            {
                return ((IDataErrorInfo)_items.First())[columnName];
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                foreach (var i in _items)
                {
                    i.PropertyChanged -= PlanItem_PropertyChanged;
                }
                base.IsDisposed = true;
            }
        }

        #endregion

        #region Static Helpers

        private static PropertyPath GetPropertyPath(System.Linq.Expressions.Expression<Func<PlanogramAnnotationMultiView, Object>> expression)
        {
            return WpfHelper.GetPropertyPath<PlanogramAnnotationMultiView>(expression);
        }

        #endregion
    }
}
