﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-29527 : M.Shelley
//  Created to allow editing of properties of multiple bays in the planogram propeties window
// V8-32161 : M.Brumby
//  Removed convertion from degrees/radians in the angle get/set as they were already being
//  convered in the binding converter.
// V8-32059 : N.Haywood
//  Added AddFixture method
// V8-32513 : M.Brumby
//  Don't add fixture if it is already added
// V8-32162 : M.Brumby
//  Update all details if bay hight, width or depth changes as it alters other things as well
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using System.ComponentModel;
using Galleria.Framework.Attributes;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Common.Wpf.Helpers;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
	/// <summary>
	/// Presents a merged view of multiple PlanogramFixtureView objects
	/// </summary>
	public sealed class PlanogramFixturePropertiesMultiView : ViewModelObject, IDataErrorInfo
	{
		#region Fields

		private Boolean _isLoggingUndoableActions = true;
		private Boolean _isValid = true;

		private Int32 _oldItemsCount = 0;

		private Boolean _isBackboardRequired;
		private PlanogramComponentView _backboardComponent;
		private Boolean _isBaseRequired;
		private PlanogramComponentView _baseComponent;

		/// <summary>
		/// Stores the collection of <see cref="PlanogramProductView"/> items that are part of this <c>Product Multi View</c>.
		/// </summary>
        private readonly ICollection<PlanogramFixtureView> _items = null;

		/// <summary>
		///     Stores the <see cref="PlanogramView"/> that the items of this instance belong to.
		/// </summary>
		private readonly PlanogramView _planView = null;

		#endregion

		#region Binding Property Paths

		public static readonly PropertyPath SelectedItemsProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.SelectedItems);

		public static readonly PropertyPath NameProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.Name);
		public static readonly PropertyPath XProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.X);
		public static readonly PropertyPath YProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.Y);
		public static readonly PropertyPath ZProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.Z);
		public static readonly PropertyPath AngleProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.Angle);
		public static readonly PropertyPath BaySequenceNumberProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.BaySequenceNumber);

		public static readonly PropertyPath WidthProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.Width);
		public static readonly PropertyPath HeightProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.Height);
		public static readonly PropertyPath DepthProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.Depth);

		public static readonly PropertyPath IsBackboardRequiredProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.IsBackboardRequired);
		public static readonly PropertyPath BackboardHeightProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.BackboardHeight);
		public static readonly PropertyPath BackboardWidthProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.BackboardWidth);
		public static readonly PropertyPath BackboardDepthProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.BackboardDepth);
		public static readonly PropertyPath BackboardFillColourProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.BackboardFillColour);
		public static readonly PropertyPath BackboardIsNotchPlacedOnFrontProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.BackboardIsNotchPlacedOnFront);
		public static readonly PropertyPath BackboardNotchStyleTypeProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.BackboardNotchStyleType);
		public static readonly PropertyPath BackboardNotchHeightProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.BackboardNotchHeight);
		public static readonly PropertyPath BackboardNotchWidthProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.BackboardNotchWidth);
		public static readonly PropertyPath BackboardNotchStartXProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.BackboardNotchStartX);
		public static readonly PropertyPath BackboardNotchSpacingXProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.BackboardNotchSpacingX);
		public static readonly PropertyPath BackboardNotchStartYProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.BackboardNotchStartY);
		public static readonly PropertyPath BackboardNotchSpacingYProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.BackboardNotchSpacingY);

        public static readonly PropertyPath IsBaseRequiredProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.IsBaseRequired);
		public static readonly PropertyPath BaseHeightProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.BaseHeight);
		public static readonly PropertyPath BaseWidthProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.BaseWidth);
		public static readonly PropertyPath BaseDepthProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.BaseDepth);
		public static readonly PropertyPath BaseFillColourProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.BaseFillColour);
        public static readonly PropertyPath BayFieldsLockedProperty = WpfHelper.GetPropertyPath<PlanogramFixturePropertiesMultiView>(p => p.BayFieldsLocked);

		#endregion

		#region Properties

		/// <summary>
		/// Gets/Sets whether changes should be logged as undoable actions.
		/// Note - this requires that a plan was specified on create.
		/// </summary>
		public Boolean IsLoggingUndoableActions
		{
			get { return _planView != null && _isLoggingUndoableActions; }
			set { _isLoggingUndoableActions = value; }
		}

        /// <summary>
        // to add use the AddFixture Method
        /// </summary>
		public ICollection<PlanogramFixtureView> Items
		{
			get
			{
				return _items;
			}
		}

		public ICollection<PlanogramFixtureView> SelectedItems
		{
			get
			{
				return _items.Where(x => x.IsSelected).ToList();
			}
		}

		public String SelectedItemsFriendly
		{
			get
			{
				var itemsList = _items.Where(x => x.IsSelected).Select(y => y.Name).ToList();
				return String.Join(", ", itemsList);
			}
		}

		public Int32 Count
		{
			get { return Items.Count(); }
		}

		public PlanogramFixtureView this[Int32 i]
		{
			get { return _items.ElementAt(i); }
		}

		public Boolean IsValid
		{
			get { return _isValid; }
		}

		/// <summary>
		/// Set when multiple bays are selected and some fields needs to be made read-only to prevent editing
		/// </summary>
		public Boolean BayFieldsLocked
		{
			get { return SelectedItems != null && SelectedItems.Count != 1; }
		}

		#region Planogram Fixture Properties

		[ModelObjectDisplayName(typeof(PlanogramFixture), "NameProperty")]
		public String Name
		{
			get
			{
				if (SelectedItems.Count() == 0)
				{
					return "No Bay Selected";
				}
				if (SelectedItems.Count() == 1)
				{
					return SelectedItems.First().Name;
				}
				else
				{
					return Message.PlanogramProperties_MultipleBaysSelected_Desc;
				}
			}
			set
			{
				// Ensure that "<Multiple bays selected>" is not written back to the model
				if (!value.Equals(Message.PlanogramProperties_MultipleBaysSelected_Desc))
				{
					SetPropertyValue(SelectedItems, PlanogramFixture.NameProperty.Name, value);
				}
			}
		}

		[ModelObjectDisplayName(typeof(PlanogramFixture), "HeightProperty")]
		public Single? Height
		{
			get
			{
				return GetCommonPropertyValue<Single?>(SelectedItems, PlanogramFixture.HeightProperty.Name);
			}
			set
			{
				if (value.HasValue)
				{
					SetPropertyValue(SelectedItems, PlanogramFixture.HeightProperty.Name, value);
                    UpdateProperties();
				}
			}
		}

		[ModelObjectDisplayName(typeof(PlanogramFixture), "WidthProperty")]
		public Single? Width
		{
			get
			{
				return GetCommonPropertyValue<Single?>(SelectedItems, PlanogramFixture.WidthProperty.Name);
			}
			set
			{
				if (value.HasValue)
				{
					SetPropertyValue(SelectedItems, PlanogramFixture.WidthProperty.Name, value);
                    UpdateProperties();
				}
			}
		}

		[ModelObjectDisplayName(typeof(PlanogramFixture), "DepthProperty")]
		public Single? Depth
		{
			get
			{
				return GetCommonPropertyValue<Single?>(SelectedItems, PlanogramFixture.DepthProperty.Name);
			}
			set
			{
				if (value.HasValue)
				{
					SetPropertyValue(SelectedItems, PlanogramFixture.DepthProperty.Name, value);
                    UpdateProperties();
				}
			}
		}

		[ModelObjectDisplayName(typeof(PlanogramFixtureItem), "XProperty")]
		public Single? X
		{
			get
			{
				return GetCommonPropertyValue<Single?>(SelectedItems, PlanogramFixtureItem.XProperty.Name);
			}
			set
			{
				if (value.HasValue)
				{
					SetPropertyValue(SelectedItems, PlanogramFixtureItem.XProperty.Name, value);
				}
			}
		}

		[ModelObjectDisplayName(typeof(PlanogramFixtureItem), "YProperty")]
		public Single? Y
		{
			get
			{
				return GetCommonPropertyValue<Single?>(SelectedItems, PlanogramFixtureItem.YProperty.Name);
			}
			set
			{
				if (value.HasValue)
				{
					SetPropertyValue(SelectedItems, PlanogramFixtureItem.YProperty.Name, value);
				}
			}
		}

		[ModelObjectDisplayName(typeof(PlanogramFixtureItem), "ZProperty")]
		public Single? Z
		{
			get
			{
				return GetCommonPropertyValue<Single?>(SelectedItems, PlanogramFixtureItem.ZProperty.Name);
			}
			set
			{
				if (value.HasValue)
				{
					SetPropertyValue(SelectedItems, PlanogramFixtureItem.ZProperty.Name, value);
				}
			}
		}

		[ModelObjectDisplayName(typeof(PlanogramFixtureItem), "AngleProperty")]
		public Single? Angle
		{
			get
			{
				Single? value = GetCommonPropertyValue<Single?>(SelectedItems, PlanogramFixtureItem.AngleProperty.Name);

                //Value is converted to degrees in the binding converter
				return value;
			}
			set
			{
				if (value.HasValue)
				{
                    //Value is converted to radians in the binding converter
					SetPropertyValue(SelectedItems, PlanogramFixtureItem.AngleProperty.Name,
						Convert.ToSingle(value.Value));
				}
			}
		}

		[ModelObjectDisplayName(typeof(PlanogramFixtureItem), "BaySequenceNumberProperty")]
		public Int16? BaySequenceNumber
		{
			get
			{
				return GetCommonPropertyValue<Int16?>(SelectedItems, PlanogramFixtureItem.BaySequenceNumberProperty.Name);
			}
			// ToDo: Add code to allow this to be set - what to do when multiple fixtures  
			set
			{
				if (value.HasValue)
				{
					SetPropertyValue(SelectedItems, PlanogramFixtureItem.BaySequenceNumberProperty.Name, value);
				}
			}
		}

		#region Backboard Properties

		/// <summary>
		/// Gets/Sets whether the selected fixture should have a backboard.
		/// </summary>
		public Boolean IsBackboardRequired
		{
			get
			{
                var itemsHaveBackboard = GetCommonPropertyValue<Boolean?>(SelectedItems, PlanogramFixtureView.HasBackboardProperty.Path);
                _isBackboardRequired = (itemsHaveBackboard == null ? false : (Boolean) itemsHaveBackboard);

                return _isBackboardRequired;
			}
			set
			{
				_isBackboardRequired = value;

				OnIsBackboardRequiredChanged(value);
                OnPropertyChanged(IsBackboardRequiredProperty);
			}
		}

		/// <summary>
		/// Returns the backboard component for the selected fixture
		/// </summary>
        //public PlanogramComponentView Backboard
        //{
        //    get { return _backboardComponent; }
        //    internal set
        //    {
        //        PlanogramComponentView oldValue = _backboardComponent;

        //        _backboardComponent = value;
        //        OnPropertyChanged(BackboardProperty);

        //        //update backboard required property
        //        _isBackboardRequired = (value != null);
        //        OnPropertyChanged(IsBackboardRequiredProperty);
        //    }
        //}

        [ModelObjectDisplayName(typeof(PlanogramComponentView), "HeightProperty")]
        public Single? BackboardHeight
        {
            get
            {
                return GetCommonFixtureComponentPropertyValue<Single?>(SelectedItems, PlanogramComponent.HeightProperty.Name, PlanogramComponentType.Backboard);
            }
            set
            {
                // Check that a backboard is required 
                if (_isBackboardRequired && value.HasValue)
                {
                    SetCommonFixtureComponentPropertyValue(SelectedItems, PlanogramComponent.HeightProperty.Name, value, PlanogramComponentType.Backboard);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponentView), "WidthProperty")]
        public Single? BackboardWidth
        {
            get
            {
                return GetCommonFixtureComponentPropertyValue<Single?>(SelectedItems, PlanogramComponent.WidthProperty.Name, PlanogramComponentType.Backboard);
            }
            set
            {
                // Check that a backboard is required 
                if (_isBackboardRequired && value.HasValue)
                {
                    SetCommonFixtureComponentPropertyValue(SelectedItems, PlanogramComponent.WidthProperty.Name, value, PlanogramComponentType.Backboard);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponentView), "DepthProperty")]
        public Single? BackboardDepth
        {
            get
            {
                return GetCommonFixtureComponentPropertyValue<Single?>(SelectedItems, PlanogramComponent.DepthProperty.Name, PlanogramComponentType.Backboard);
            }
            set
            {
                // Check that a backboard is required 
                if (_isBackboardRequired && value.HasValue)
                {
                    SetCommonFixtureComponentPropertyValue(SelectedItems, PlanogramComponent.DepthProperty.Name, value, PlanogramComponentType.Backboard);
                }
            }
        }

        [ModelObjectDisplayName(typeof(PlanogramComponentView), "FillColourProperty")]
        public Int32? BackboardFillColour
        {
            get
            {
                return GetCommonFixtureComponentPropertyValue<Int32?>(SelectedItems, PlanogramComponentView.FillColourProperty.Path);
            }
            set
            {
                // Check that a backboard is required 
                if (_isBackboardRequired && value.HasValue)
                {
                    SetCommonFixtureComponentPropertyValue(SelectedItems, PlanogramComponentView.FillColourProperty.Path, value, PlanogramComponentType.Backboard);
                }
            }
        }

		[ModelObjectDisplayName(typeof(PlanogramSubComponent), "IsNotchPlacedOnFrontProperty")]
		public Boolean? BackboardIsNotchPlacedOnFront
		{
			get
			{
				return GetCommonBackboardSubComponentPropertyValue<Boolean?>(SelectedItems, PlanogramSubComponent.IsNotchPlacedOnFrontProperty.Name);
			}
			set
			{
                if (_isBackboardRequired && value.HasValue)
                {
                    SetCommonBackboardSubComponentPropertyValue(SelectedItems, PlanogramSubComponent.IsNotchPlacedOnFrontProperty.Name, value);
                }
			}
		}

		// Notch Style
		[ModelObjectDisplayName(typeof(PlanogramSubComponent), "NotchStyleTypeProperty")]
		public PlanogramSubComponentNotchStyleType? BackboardNotchStyleType
		{
			get
			{
				return GetCommonBackboardSubComponentPropertyValue<PlanogramSubComponentNotchStyleType?>(SelectedItems, PlanogramSubComponent.NotchStyleTypeProperty.Name);
			}
			set
			{
                if (_isBackboardRequired && value.HasValue)
                {
                    SetCommonBackboardSubComponentPropertyValue(SelectedItems, PlanogramSubComponent.NotchStyleTypeProperty.Name, value);
                }
			}
		}

		// Notch Height
		[ModelObjectDisplayName(typeof(PlanogramSubComponent), "NotchHeightProperty")]
		public Single? BackboardNotchHeight
		{
			get
			{
				return GetCommonBackboardSubComponentPropertyValue<Single?>(SelectedItems, PlanogramSubComponent.NotchHeightProperty.Name);
			}
			set
			{
                if (_isBackboardRequired && value.HasValue)
                {
                    SetCommonBackboardSubComponentPropertyValue(SelectedItems, PlanogramSubComponent.NotchHeightProperty.Name, value);
                }
			}
		}

		// Notch Width
		[ModelObjectDisplayName(typeof(PlanogramSubComponent), "NotchWidthProperty")]
		public Single? BackboardNotchWidth
		{
			get
			{
				return GetCommonBackboardSubComponentPropertyValue<Single?>(SelectedItems, PlanogramSubComponent.NotchWidthProperty.Name);
			}
			set
			{
                if (_isBackboardRequired && value.HasValue)
                {
                    SetCommonBackboardSubComponentPropertyValue(SelectedItems, PlanogramSubComponent.NotchWidthProperty.Name, value);
                }
			}
		}

		// Notch Start X
		[ModelObjectDisplayName(typeof(PlanogramSubComponent), "NotchStartXProperty")]
		public Single? BackboardNotchStartX
		{
			get
			{
				return GetCommonBackboardSubComponentPropertyValue<Single?>(SelectedItems, PlanogramSubComponent.NotchStartXProperty.Name);
			}
			set
			{
                if (_isBackboardRequired && value.HasValue)
                {
                    SetCommonBackboardSubComponentPropertyValue(SelectedItems, PlanogramSubComponent.NotchStartXProperty.Name, value);
                }
			}
		}

		// Notch Spacing X
		[ModelObjectDisplayName(typeof(PlanogramSubComponent), "NotchSpacingXProperty")]
		public Single? BackboardNotchSpacingX
		{
			get
			{
				return GetCommonBackboardSubComponentPropertyValue<Single?>(SelectedItems, PlanogramSubComponent.NotchSpacingXProperty.Name);
			}
			set
			{
                if (_isBackboardRequired && value.HasValue)
                {
                    SetCommonBackboardSubComponentPropertyValue(SelectedItems, PlanogramSubComponent.NotchSpacingXProperty.Name, value);
                }
			}
		}

		// Notch Start Y
		[ModelObjectDisplayName(typeof(PlanogramSubComponent), "NotchStartYProperty")]
		public Single? BackboardNotchStartY
		{
			get
			{
				return GetCommonBackboardSubComponentPropertyValue<Single?>(SelectedItems, PlanogramSubComponent.NotchStartYProperty.Name);
			}
			set
			{
                if (_isBackboardRequired && value.HasValue)
                {
                    SetCommonBackboardSubComponentPropertyValue(SelectedItems, PlanogramSubComponent.NotchStartYProperty.Name, value);
                }
			}
		}

		// Notch Spacing Y
		[ModelObjectDisplayName(typeof(PlanogramSubComponent), "NotchSpacingYProperty")]
		public Single? BackboardNotchSpacingY
		{
			get
			{
				return GetCommonBackboardSubComponentPropertyValue<Single?>(SelectedItems, PlanogramSubComponent.NotchSpacingYProperty.Name);
			}
			set
			{
                if (_isBackboardRequired && value.HasValue)
                {
                    SetCommonBackboardSubComponentPropertyValue(SelectedItems, PlanogramSubComponent.NotchSpacingYProperty.Name, value);
                }
			}
		}

		#endregion

		#region Base Properties

		/// <summary>
		/// Gets/Sets whether the selected fixture should have a base.
		/// </summary>
		public Boolean IsBaseRequired
		{
            get
            {
                var itemsHaveBase = GetCommonPropertyValue<Boolean?>(SelectedItems, PlanogramFixtureView.HasBaseProperty.Path);
                _isBaseRequired = (itemsHaveBase == null ? false : (Boolean)itemsHaveBase);

                return _isBaseRequired;
            }
			set
			{
				_isBaseRequired = value;

                OnIsBaseRequiredChanged(value);
                OnPropertyChanged(IsBaseRequiredProperty);
            }
		}

		/// <summary>
		/// Gets the base component for the selected fixture
		/// </summary>
        //public PlanogramComponentView Base
        //{
        //    get { return _baseComponent; }
        //    private set
        //    {
        //        PlanogramComponentView oldValue = _baseComponent;

        //        _baseComponent = value;
        //        OnPropertyChanged(BaseProperty);

        //        //update base required property
        //        _isBaseRequired = (value != null);
        //        OnPropertyChanged(IsBaseRequiredProperty);
        //    }
        //}

		[ModelObjectDisplayName(typeof(PlanogramComponentView), "HeightProperty")]
		public Single? BaseHeight
		{
			get
			{
				return GetCommonFixtureComponentPropertyValue<Single?>(SelectedItems, PlanogramComponent.HeightProperty.Name, PlanogramComponentType.Base);
			}
			set
			{
                // Check that a base is required 
                if (_isBaseRequired && value.HasValue)
                {
                    SetCommonFixtureComponentPropertyValue(SelectedItems, PlanogramComponent.HeightProperty.Name, value, PlanogramComponentType.Base);
                }
			}
		}

		[ModelObjectDisplayName(typeof(PlanogramComponentView), "WidthProperty")]
		public Single? BaseWidth
		{
			get
			{
				return GetCommonFixtureComponentPropertyValue<Single?>(SelectedItems, PlanogramComponent.WidthProperty.Name, PlanogramComponentType.Base);
			}
			set
			{
                // Check that a base is required 
                if (_isBaseRequired && value.HasValue)
                {
                    SetCommonFixtureComponentPropertyValue(SelectedItems, PlanogramComponent.WidthProperty.Name, value, PlanogramComponentType.Base);
                }
            }
		}

		[ModelObjectDisplayName(typeof(PlanogramComponentView), "DepthProperty")]
		public Single? BaseDepth
		{
			get
			{
				return GetCommonFixtureComponentPropertyValue<Single?>(SelectedItems, PlanogramComponent.DepthProperty.Name, PlanogramComponentType.Base);
			}
			set
			{
                // Check that a base is required 
                if (_isBaseRequired && value.HasValue)
                {
                    SetCommonFixtureComponentPropertyValue(SelectedItems, PlanogramComponent.DepthProperty.Name, value, PlanogramComponentType.Base);
                }
            }
		}

		// Base Colour
		[ModelObjectDisplayName(typeof(PlanogramComponentView), "FillColourProperty")]
		public Int32? BaseFillColour
		{
			get
			{
				return GetCommonFixtureComponentPropertyValue<Int32?>(SelectedItems, PlanogramComponentView.FillColourProperty.Path, PlanogramComponentType.Base);
			}
			set
			{
                // Check that a base is required 
                if (_isBaseRequired && value.HasValue)
                {
                    SetCommonFixtureComponentPropertyValue(SelectedItems, PlanogramComponentView.FillColourProperty.Path, value, PlanogramComponentType.Base);
                }
            }
		}


		#endregion

		#endregion

		#region IDataErrorInfo properties

		public String Error
		{
			get { return ""; }
		}

		public String this[String columnName]
		{
			// ToDo: implement this
			get { return ""; }
		}

		#endregion

		#endregion

		#region Constructor

		public PlanogramFixturePropertiesMultiView(IEnumerable<PlanogramFixtureView> items)
		{
			if (items == null || items.Count() == 0)
			{
				return;
			}

			// ToDo: Check what to do if the user selects plans in the main editor view 
			// and then opens the planogram properties window
            var itemsList = items.ToList();
            foreach (var itemItem in itemsList)
            {
                itemItem.IsSelected = false;
            }

			_items = items.ToList();
			_planView = items.First().Planogram;

			foreach (var i in _items)
			{
				i.PropertyChanged += PlanItem_PropertyChanged;
			}

			UpdateProperties();
		}

		#endregion

		#region Event Handlers

		/// <summary>
		/// Called whenever a property changes on any linked plan item.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void PlanItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			OnPropertyChanged(e.PropertyName);

			if (e.PropertyName == PlanogramFixtureView.IsSelectedProperty.Path)
			{
				UpdateProperties();
			}
		}

		#endregion

		#region Methods

        public void AddFixture(PlanogramFixtureView fixture)
        {
            if (!this.Items.Contains(fixture))
            {
                fixture.PropertyChanged += PlanItem_PropertyChanged;
                this.Items.Add(fixture);
            }
        }

        public void ClearAllSelections()
        {
            var selItemsList = this.SelectedItems.ToList();
            foreach (var itemItem in selItemsList)
            {
                itemItem.IsSelected = false;
            }

            UpdateProperties();
        }

		private void UpdateProperties()
		{
			UpdateBackboard();
			UpdateBase();

			OnPropertyChanged(NameProperty);
			OnPropertyChanged(XProperty);
			OnPropertyChanged(YProperty);
			OnPropertyChanged(ZProperty);
			OnPropertyChanged(AngleProperty);
			OnPropertyChanged(BaySequenceNumberProperty);

			OnPropertyChanged(WidthProperty);
			OnPropertyChanged(HeightProperty);
			OnPropertyChanged(DepthProperty);

            OnPropertyChanged(BayFieldsLockedProperty);

			_oldItemsCount = this.SelectedItems.Count();
		}

		/// <summary>
		/// Gets the common value of the given property for all the items
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="items"></param>
		/// <param name="propertyName"></param>
		/// <returns></returns>
		private static T GetCommonPropertyValue<T>(IEnumerable<Object> items, String propertyName)
		{
			T value = default(T);

			if (items.Any())
			{
				Type classType = items.First().GetType();
				System.Reflection.PropertyInfo pInfo = classType.GetProperty(propertyName);
				if (pInfo != null)
				{
					Boolean isFirst = true;
					foreach (Object item in items)
					{
						T itemValue = (T)pInfo.GetValue(item, null);
						if (isFirst)
						{
							value = itemValue;
						}
						else if (!Object.Equals(itemValue, value))
						{
							return default(T);
						}

						isFirst = false;
					}
				}
			}

			return value;
		}

		private static T GetCommonFixtureComponentPropertyValue<T>(IEnumerable<PlanogramFixtureView> fixtureItems, String propertyName, 
																   PlanogramComponentType fixtureComponentType = PlanogramComponentType.Backboard)
		{
			T value = default(T);

			// First get a list of all the components of the requested type
			IEnumerable<PlanogramComponentView> allTypeComponents = null;

			if (fixtureComponentType == PlanogramComponentType.Backboard)
			{
				allTypeComponents = fixtureItems.Select(x => x.BackboardComponent);
			}
			else if (fixtureComponentType == PlanogramComponentType.Base)
			{
				allTypeComponents = fixtureItems.Select(x => x.BaseComponent);
			}

			// Check if there are some null values (i.e. one of the fixtures does not have a backboard or base), 
			// in which case we return the default value anyway
			Boolean nullPresent = allTypeComponents.Where(x => x == null).Any();

			// And if there are some component values, get the common property from that collection
			if (allTypeComponents != null && !nullPresent)
			{
				value = GetCommonPropertyValue<T>(allTypeComponents, propertyName);
			}

			return value;
		}

        private void SetCommonFixtureComponentPropertyValue<T>(IEnumerable<PlanogramFixtureView> fixtureItems, String propertyName, T value, 
                                                                PlanogramComponentType fixtureComponentType = PlanogramComponentType.Backboard)
        {
            // First get a list of all the components of the requested type
            IEnumerable<PlanogramComponentView> allTypeComponents = null;

            if (fixtureComponentType == PlanogramComponentType.Backboard)
            {
                allTypeComponents = fixtureItems.Select(x => x.BackboardComponent);
            }
            else if (fixtureComponentType == PlanogramComponentType.Base)
            {
                allTypeComponents = fixtureItems.Select(x => x.BaseComponent);
            }

            // Check if there are some null values (i.e. one of the fixtures does not have a backboard or base), 
            // in which case we return the default value anyway
            Boolean nullPresent = allTypeComponents.Where(x => x == null).Any();

            if (allTypeComponents != null && !nullPresent)
            {
                SetPropertyValue(allTypeComponents, propertyName, value);
            }
        }

		private static T GetCommonBackboardSubComponentPropertyValue<T>(IEnumerable<PlanogramFixtureView> fixtureItems, String propertyName)
		{
			T value = default(T);

			// First get a list of all the backboard components

			IEnumerable<PlanogramComponentView> allTypeComponents = fixtureItems.Select(x => x.BackboardComponent);

			if (allTypeComponents != null)
			{
                // First check if any of the fixtures do not have a SubComponent entry in which case
                // we return the default value
                Boolean nullPresent = allTypeComponents.Where(x => x == null).Any();

                // Check if any of the fixtures do not have a backboard
                if (!nullPresent)
                {
                    // Get the first sub component of these backboards to get the notch values
                    var firstSubComponents = allTypeComponents.Select(x => x.SubComponents[0]);
                    value = GetCommonPropertyValue<T>(firstSubComponents, propertyName);
                }
			}

			return value;
		}

        private void SetCommonBackboardSubComponentPropertyValue<T>(IEnumerable<PlanogramFixtureView> fixtureItems, String propertyName, T value)
        {
            // First get a list of all the backboard components
            IEnumerable<PlanogramComponentView> allTypeComponents = fixtureItems.Select(x => x.BackboardComponent);

            if (allTypeComponents != null)
            {
                // First check if any of the fixtures do not have a SubComponent entry in which case
                // we return the default value
                Boolean nullPresent = allTypeComponents.Where(x => x == null).Any();

                if (allTypeComponents != null && !nullPresent)
                {
                    // Get the first sub component of these backboards to get the notch values
                    var firstSubComponents = allTypeComponents.Select(x => x.SubComponents[0]);

                    // Check there are no null subcomponents (not sure what to do if there are...)
                    SetPropertyValue(firstSubComponents, propertyName, value);
                }
            }
        }

		/// <summary>
		/// Sets the given value against
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="items"></param>
		/// <param name="propertyName"></param>
		/// <param name="value"></param>
		private void SetPropertyValue<T>(IEnumerable<Object> items, String propertyName, T value)
		{
			if (items.Any())
			{

				// Start logging the change
				Boolean isProcessingUndoable = false;
				if (IsLoggingUndoableActions)
				{
					if (!_planView.IsRecordingUndoableAction)
					{
						isProcessingUndoable = true;
						_planView.BeginUndoableAction(propertyName);
					}
				}

				// Make the property change
				Type classType = items.First().GetType();
				System.Reflection.PropertyInfo pInfo = classType.GetProperty(propertyName);

				if (pInfo != null)
				{
					foreach (Object item in items)
					{
                        pInfo.SetValue(item, value, null);
					}
				}

				// Commit the log.
				if (isProcessingUndoable)
				{
					_planView.EndUndoableAction();
				}
			}
		}

		public void UpdateBackboard()
		{
			// ToDo: setup the backboard depending on whether a single bay or multiple bays are selected
            //if (this.SelectedItems.Count() > 1)
            //{
            //    //var things = GetCommonPropertyValue<PlanogramComponentView>(SelectedItems, PlanogramFixtureView.BackboardComponentProperty.Path);

            //    // Loop through all the back board components of the fixtures to check if any are null
            //    Boolean foundEmptyBackboard = false;
            //    Boolean foundBackboard = false;
            //    PlanogramComponentView useBackboard = null;
            //    foreach (var selItem in SelectedItems)
            //    {
            //        var itemBackboard = selItem.EnumerateAllComponents().FirstOrDefault(f => f.ComponentType == PlanogramComponentType.Backboard);
            //        if (itemBackboard == null)
            //        {
            //            foundEmptyBackboard = true;
            //            break;
            //        }
            //        else if (!foundBackboard)
            //        {
            //            useBackboard = itemBackboard;
            //            foundBackboard = true;
            //        }
            //    }

            //    if (!foundEmptyBackboard)
            //    {
            //        this.Backboard = useBackboard;
            //    }
            //}
            //else
            //{
            //    var selectedFixture = SelectedItems.FirstOrDefault();
            //    if (selectedFixture != null)
            //    {
            //        this.Backboard = selectedFixture.EnumerateAllComponents().FirstOrDefault(f => f.ComponentType == PlanogramComponentType.Backboard);
            //    }
            //}

            OnPropertyChanged(IsBackboardRequiredProperty);
            OnPropertyChanged(BackboardHeightProperty);
			OnPropertyChanged(BackboardWidthProperty);
			OnPropertyChanged(BackboardDepthProperty);
			OnPropertyChanged(BackboardFillColourProperty);
			OnPropertyChanged(BackboardIsNotchPlacedOnFrontProperty);
			OnPropertyChanged(BackboardNotchStyleTypeProperty);
			OnPropertyChanged(BackboardNotchHeightProperty);
			OnPropertyChanged(BackboardNotchWidthProperty);
			OnPropertyChanged(BackboardNotchStartXProperty);
			OnPropertyChanged(BackboardNotchSpacingXProperty);
			OnPropertyChanged(BackboardNotchStartYProperty);
			OnPropertyChanged(BackboardNotchSpacingYProperty);
		}

		/// <summary>
		/// Called whenever the value of IsBackboardRequired changes
		/// </summary>
		/// <param name="newValue"></param>
		private void OnIsBackboardRequiredChanged(Boolean newValue)
        {
            PlanogramComponentView newBackboard = null;

            // Loop through all the selected fixtures and set the base IsRequired value for each
            foreach (var selItem in this.SelectedItems)
            {
                newBackboard = selItem.SetBackboard(newValue);
            }

            UpdateBackboard();
        }

		public void UpdateBase()
		{
			OnPropertyChanged(IsBaseRequiredProperty);
			OnPropertyChanged(BaseHeightProperty);
			OnPropertyChanged(BaseWidthProperty);
			OnPropertyChanged(BaseDepthProperty);
			OnPropertyChanged(BaseFillColourProperty);
		}

        		/// <summary>
		/// Called whenever the value of IsBackboardRequired changes
		/// </summary>
		/// <param name="newValue"></param>
        private void OnIsBaseRequiredChanged(Boolean newValue)
        {
            PlanogramComponentView newBase = null;

            // Loop through all the selected fixtures and set the base IsRequired value for each
            foreach (var selItem in this.SelectedItems)
            {
                newBase = selItem.SetBase(newValue);
            }

            UpdateBase();
        }

		#endregion

		#region IDisposable Members

		protected override void Dispose(bool disposing)
		{
			foreach (var i in _items)
			{
				i.PropertyChanged -= PlanItem_PropertyChanged;
			}
		}

		#endregion
	}
}
