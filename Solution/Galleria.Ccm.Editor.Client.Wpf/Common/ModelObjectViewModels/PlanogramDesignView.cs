﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// L.Ineson
//  Created
#endregion

#region Version History: (CCM 8.10)
//  V8-29662 : M.Brumby
//      Changed ordering of fixtures to take into account
//      the numbering strategy.
#endregion
#endregion

using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Planograms.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Represents a flattened out view of a planogram.
    /// </summary>
    public sealed class PlanogramDesignView : IPlanRenderable, IDisposable
    {
        #region Fields

        private PlanogramView _planogram;

        private readonly ObservableCollection<PlanogramFixtureDesignView> _flattenedFixtures = new ObservableCollection<PlanogramFixtureDesignView>();
        private ReadOnlyObservableCollection<PlanogramFixtureDesignView> _flattenedFixturesRO;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath HeightProperty = WpfHelper.GetPropertyPath<PlanogramDesignView>(p => p.Height);
        public static readonly PropertyPath WidthProperty = WpfHelper.GetPropertyPath<PlanogramDesignView>(p => p.Width);
        public static readonly PropertyPath DepthProperty = WpfHelper.GetPropertyPath<PlanogramDesignView>(p => p.Depth);

        #endregion

        #region Properties

        public Single Height
        {
            get { return _planogram.Height; }
        }

        public Single Width
        {
            get { return _planogram.Width; }
        }

        public Single Depth
        {
            get { return _planogram.Depth; }
        }

        /// <summary>
        /// Returns a readonly collection of the fixtures of this planogram flattened out.
        /// </summary>
        public ReadOnlyObservableCollection<PlanogramFixtureDesignView> FlattenedFixtures
        {
            get
            {
                if (_flattenedFixturesRO == null)
                {
                    _flattenedFixturesRO = new ReadOnlyObservableCollection<PlanogramFixtureDesignView>(_flattenedFixtures);
                }
                return _flattenedFixturesRO;
            }
        }

        #endregion

        #region Constructor

        public PlanogramDesignView(PlanogramView plan)
        {
            _planogram = plan;
            plan.PropertyChanged += Plan_PropertyChanged;
            plan.ChildChanged += Plan_ChildChanged;
            plan.Fixtures.CollectionChanged += Plan_FixturesCollectionChanged;

            //add in existing fixtures
            foreach (PlanogramFixtureView child in plan.Fixtures)
            {
                _flattenedFixtures.Add(new PlanogramFixtureDesignView(child));
            }
            UpdateFlattenedFixtures();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on the source planogram.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Plan_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == HeightProperty.Path
                || e.PropertyName == WidthProperty.Path
                || e.PropertyName == DepthProperty.Path)
            {
                OnPropertyChanged(e.PropertyName);
            }
        }

        /// <summary>
        /// Called whenever a child property changes on the source planogram
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Plan_ChildChanged(object sender, ViewModelChildChangedEventArgs e)
        {
            if (e.ChildObject is PlanogramFixtureView)
            {
                UpdateFlattenedFixtures();
            }
        }

        /// <summary>
        /// Called whenever the planogram fixtures collection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Plan_FixturesCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (PlanogramFixtureView o in e.NewItems)
                    {
                        _flattenedFixtures.Add(new PlanogramFixtureDesignView(o));
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        List<PlanogramFixtureView> oldItems = e.OldItems.Cast<PlanogramFixtureView>().ToList();

                        foreach (PlanogramFixtureDesignView flattenedView in _flattenedFixtures.ToList())
                        {
                            if (oldItems.Contains(flattenedView.Fixture))
                            {
                                _flattenedFixtures.Remove(flattenedView);
                                flattenedView.Dispose();
                            }
                        }

                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        foreach (var f in _flattenedFixtures)
                        {
                            f.Dispose();
                        }
                        _flattenedFixtures.Clear();

                        foreach (PlanogramFixtureView child in (IEnumerable)sender)
                        {
                            _flattenedFixtures.Add(new PlanogramFixtureDesignView(child));
                        }

                    }
                    break;
            }

            UpdateFlattenedFixtures();
        }

        #endregion

        #region Methods

        private void UpdateFlattenedFixtures()
        {
            Single currX = 0;

            foreach (PlanogramFixtureDesignView view in EnumerateAllFixtures())
            {
                view.X = currX;
                currX += view.Width;
            }
        }

        /// <summary>
        /// Enumerates through all fixtures in this plan in sequence order.
        /// </summary>
        /// <returns></returns>        
        public IEnumerable<PlanogramFixtureDesignView> EnumerateAllFixtures()
        {
            //If our numbering strategy isn't left to right then we need to get the fixtures in reverse
            //order. As the numbering strategy should not effect the order of the bays.
            if (this._planogram != null &&
                this._planogram.RenumberingStrategy != null &&
                this._planogram.RenumberingStrategy.BayComponentXRenumberStrategy == Framework.Planograms.Model.PlanogramRenumberingStrategyXRenumberType.RightToLeft)
            {
                foreach (var fixture in _flattenedFixtures.OrderByDescending(f => f.Fixture.BaySequenceNumber))
                {
                    yield return fixture;
                }
            }
            else
            {
                foreach (var fixture in _flattenedFixtures.OrderBy(f => f.Fixture.BaySequenceNumber))
                {
                    yield return fixture;
                }
            }
        }
        #endregion

        #region IPlanogram Members

        event NotifyCollectionChangedEventHandler IPlanRenderable.FixturesCollectionChanged
        {
            add { _flattenedFixtures.CollectionChanged += value; }
            remove { _flattenedFixtures.CollectionChanged -= value; }
        }

        IEnumerable<IPlanFixtureRenderable> IPlanRenderable.Fixtures
        {
            get { return this.FlattenedFixtures; }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            OnPropertyChanged(property.Path);
        }
        private void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            if (!_isDisposed)
            {
                _planogram.PropertyChanged -= Plan_PropertyChanged;
                _planogram.ChildChanged -= Plan_ChildChanged;
                _planogram.Fixtures.CollectionChanged -= Plan_FixturesCollectionChanged;

                foreach (var f in _flattenedFixtures)
                {
                    f.Dispose();
                }

                GC.SuppressFinalize(this);
                _isDisposed = true;
            }
        }

        #endregion
    }
}
