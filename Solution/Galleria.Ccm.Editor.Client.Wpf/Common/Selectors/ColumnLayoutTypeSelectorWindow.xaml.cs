﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)

// V8-26076 : A.Silva ~ Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.Common.Selectors
{
    /// <summary>
    ///     Interaction logic for ColumnLayoutTypeSelectorWindow.xaml
    /// </summary>
    public partial class ColumnLayoutTypeSelectorWindow
    {
        #region Constructor

        /// <summary>
        ///     Initializes a new instance of the <see cref="ColumnLayoutTypeSelectorWindow" /> class.
        ///     Shows all existing values in <see cref="CustomColumnLayoutType"/> to the user.
        ///     By default uses Single selection mode.
        /// </summary>
        public ColumnLayoutTypeSelectorWindow() 
        {
            Mouse.OverrideCursor = Cursors.Wait;

            Selection = new List<CustomColumnLayoutType>();

            var allTypes = Enum.GetValues(typeof(CustomColumnLayoutType)).Cast<CustomColumnLayoutType>();
            var excludedTypes = new List<CustomColumnLayoutType> { CustomColumnLayoutType.Undefined};
         
            Items = allTypes.Except(excludedTypes).ToList();
            SelectionMode = DataGridSelectionMode.Single;

            if (Application.Current != null) InitializeComponent(); // do not initialize for unit testing.

            Loaded += ColumnLayoutTypeSelectorWindow_Loaded;
        }

        #endregion

        #region Properties

        #region Items Property

        private static readonly DependencyProperty ItemsProperty = DependencyProperty.Register("Items",
            typeof(List<CustomColumnLayoutType>), typeof(ColumnLayoutTypeSelectorWindow),
            new PropertyMetadata(new List<CustomColumnLayoutType>()));

        private List<CustomColumnLayoutType> Items
        {
            get { return (List<CustomColumnLayoutType>)GetValue(ItemsProperty); }
            set { SetValue(ItemsProperty, value); }
        }

        #endregion

        #region SelectedItems Property

        private static readonly DependencyProperty SelectedItemsProperty = DependencyProperty.Register("SelectedItems",
            typeof (List<CustomColumnLayoutType>), typeof (ColumnLayoutTypeSelectorWindow),
            new PropertyMetadata(new List<CustomColumnLayoutType>()));

        private List<CustomColumnLayoutType> SelectedItems
        {
            get { return (List<CustomColumnLayoutType>) GetValue(SelectedItemsProperty); }
            set { SetValue(SelectedItemsProperty, value); }
        }

        #endregion

        #region SelectionMode Property

        /// <summary>
        ///     <see cref="DependencyProperty" /> for the <see cref="SelectionMode" /> property.
        /// </summary>
        public static readonly DependencyProperty SelectionModeProperty = DependencyProperty.Register("SelectionMode",
            typeof (DataGridSelectionMode), typeof (ColumnLayoutTypeSelectorWindow),
            new PropertyMetadata(DataGridSelectionMode.Single));

        /// <summary>
        ///     Gets or set the <see cref="DataGridSelectionMode" /> for <see cref="xSelectionGrid" />.
        /// </summary>
        public DataGridSelectionMode SelectionMode
        {
            get { return (DataGridSelectionMode) GetValue(SelectionModeProperty); }
            set { SetValue(SelectionModeProperty, value); }
        }

        #endregion

        public List<CustomColumnLayoutType> Selection { get; private set; }

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Whenever the current instance finishes loading.
        /// </summary>
        private void ColumnLayoutTypeSelectorWindow_Loaded(Object sender, RoutedEventArgs e)
        {
            Loaded -= ColumnLayoutTypeSelectorWindow_Loaded;

            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        /// <summary>
        ///     Whenever the user presses a key, and before inner controls get the event.
        /// </summary>
        private void XSelectionGrid_OnPreviewKeyDown(Object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter || !SelectedItems.Any()) return; // Either the pressed key is not Enter or there are no items selected.

            e.Handled = true;
            OnSelectionComplete();
        }

        /// <summary>
        ///     Whenever the user double clicks on a given row item.
        /// </summary>
        private void XSelectionGrid_OnRowItemMouseDoubleClick(Object sender, ExtendedDataGridItemEventArgs e)
        {
            OnSelectionComplete();
        }

        /// <summary>
        ///     Whenever the user clicks on the Accept Button.
        /// </summary>
        private void xAcceptButton_Click(Object sender, RoutedEventArgs e)
        {
            OnSelectionComplete();
        }

        /// <summary>
        ///     Whenever the user click on the Cancel Button.
        /// </summary>
        private void xCancelButton_Click(Object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        #endregion

        #region Methods

        private void OnSelectionComplete()
        {
            Selection = SelectionMode == DataGridSelectionMode.Single 
                ? new List<CustomColumnLayoutType>{(CustomColumnLayoutType) xSelectionGrid.SelectedItem}
                : SelectedItems;
            DialogResult = true;
        }

        #endregion
    }
}