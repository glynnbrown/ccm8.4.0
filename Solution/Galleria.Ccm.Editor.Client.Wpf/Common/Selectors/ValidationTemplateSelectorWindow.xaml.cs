﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)

// V8-26815 : A.Silva ~ Created.

#endregion

#endregion

using System;
using System.Collections.ObjectModel;
using System.Linq.Expressions;
using System.Windows;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.Common.Selectors
{
    /// <summary>
    /// Interaction logic for ValidationTemplateSelectorWindow.xaml
    /// </summary>
    public partial class ValidationTemplateSelectorWindow
    {
        #region Fields

        private ReadOnlyObservableCollection<ValidationTemplateInfo> _itemSourceRo;

        private readonly ObservableCollection<ValidationTemplateInfo> _itemSource =
            new ObservableCollection<ValidationTemplateInfo>();

        #endregion
      
        #region Property path bindings

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="ItemSource"/>.
        /// </summary>
        public static readonly PropertyPath ItemSourceProperty = GetPropertyPath(o => o.ItemSource);

        #region Commands

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="SelectCommand"/>.
        /// </summary>
        public static readonly PropertyPath SelectCommandProperty = GetPropertyPath(o => o.SelectCommand);

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="CancelCommand"/>.
        /// </summary>
        public static readonly PropertyPath CancelCommandProperty = GetPropertyPath(o => o.CancelCommand);

        #endregion

        #endregion

        #region Properties

        public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register(
            "SelectedItem", typeof (ValidationTemplateInfo), typeof (ValidationTemplateSelectorWindow));

        public ValidationTemplateInfo SelectedItem
        {
            get { return (ValidationTemplateInfo) GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public ReadOnlyObservableCollection<ValidationTemplateInfo> ItemSource
        {
            get
            {
                return _itemSourceRo ??
                       (_itemSourceRo = new ReadOnlyObservableCollection<ValidationTemplateInfo>(_itemSource));
            }
        }

        #endregion

        #region Constructor

        public ValidationTemplateSelectorWindow()
        {
            InitializeComponent();

            InitializeItemSource();
        }

        #endregion
      
        #region Commands

        #region Select

        /// <summary>
        ///     Reference to the current instance for <see cref="SelectCommand"/>.
        /// </summary>
        private RelayCommand _selectCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="SelectCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand SelectCommand
        {
            get
            {
                if (_selectCommand != null) return _selectCommand;

                _selectCommand = new RelayCommand(o => Select_Executed(), o => Select_CanExecute())
                {
                    FriendlyName = Message.Generic_Select
                };
                return _selectCommand;
            }
        }

        /// <summary>
        ///		Invoked whenever the UI needs to assess the availability of the <see cref="SelectCommand"/>.
        /// </summary>
        private Boolean Select_CanExecute()
        {
            return SelectedItem != null;
        }

        /// <summary>
        ///		Invoked whenever the <see cref="SelectCommand"/> is executed.
        /// </summary>
        private void Select_Executed()
        {
            DialogResult = true;
            Close();
        }

        #endregion

        #region Cancel

        /// <summary>
        ///     Reference to the current instance for <see cref="CancelCommand"/>.
        /// </summary>
        private RelayCommand _cancelCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="CancelCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand != null) return _cancelCommand;

                _cancelCommand = new RelayCommand(o => Cancel_Executed())
                {
                    FriendlyName = Message.Generic_Cancel
                };
                return _cancelCommand;
            }
        }

        /// <summary>
        ///		Invoked whenever the <see cref="CancelCommand"/> is executed.
        /// </summary>
        private void Cancel_Executed()
        {
            DialogResult = false;
            Close();
        }

        #endregion

        #endregion

        #region Event Handlers

        private void XItemDataGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (SelectCommand.CanExecute()) SelectCommand.Execute();
        }

        #endregion

        #region Methods

        private void InitializeItemSource()
        {
            _itemSource.Clear();
            var list = ValidationTemplateInfoList.FetchByEntityId(App.ViewState.EntityId);
            foreach (var item in list) _itemSource.Add(item);
        }

        #endregion

        #region Private Static Helpers

        /// <summary>
        ///     Helper method to get the property path for the <see cref="ValidationTemplateSelectorWindow" /> property indicated by the
        ///     <paramref name="expression" />.
        /// </summary>
        /// <param name="expression">Lambda expression to access the property.</param>
        /// <returns>
        ///     Returns a <see cref="PropertyPath" /> Object with the path to the property in the
        ///     <paramref name="expression" />.
        /// </returns>
        private static PropertyPath GetPropertyPath(
            Expression<Func<ValidationTemplateSelectorWindow, Object>> expression)
        {
            return WpfHelper.GetPropertyPath(expression);
        }

        #endregion
    }

}
