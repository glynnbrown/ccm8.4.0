﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// V8-26704 : A.Kuszyk
//  Created.
#endregion

#region Version History: (CCM 8.1.0)
// V8-30196 : M.Pettit
//  Added Help link
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using System.ComponentModel;
using Galleria.Ccm.Model;
using System.Collections.ObjectModel;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Editor.Client.Wpf.Common.Selectors
{
    /// <summary>
    /// Interaction logic for AssortmentSelectionWindow.xaml
    /// </summary>
    public partial class AssortmentSelectionWindow : ExtendedRibbonWindow
    {
        #region Fields

        private AssortmentInfoList _availableAssortmentInfos;
        private ObservableCollection<AssortmentInfo> _searchResults = new ObservableCollection<AssortmentInfo>();
        private ReadOnlyObservableCollection<AssortmentInfo> _searchResultsRO;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath SearchResultsProperty = WpfHelper.GetPropertyPath<AssortmentSelectionWindow>(p => p.SearchResults);
        public static readonly PropertyPath SelectCommandProperty = WpfHelper.GetPropertyPath<AssortmentSelectionWindow>(p => p.SelectCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<AssortmentSelectionWindow>(p => p.CancelCommand);

        #endregion

        #region Properties

        public static readonly DependencyProperty SearchCriteriaProperty = 
            DependencyProperty.Register("SearchCriteria", typeof(String), typeof(AssortmentSelectionWindow),
            new PropertyMetadata(OnSearchCriteriaChanged));
        public String SearchCriteria
        {
            get { return (String)GetValue(SearchCriteriaProperty); }
            set { SetValue(SearchCriteriaProperty, value);}
        }

        private static void OnSearchCriteriaChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var win = obj as AssortmentSelectionWindow;
            if(win==null) return;
            win.FilterResults();
        }

        public static readonly DependencyProperty SelectedAssortmentProperty =
            DependencyProperty.Register("SelectedAssortment", typeof(AssortmentInfo), typeof(AssortmentSelectionWindow));
        public AssortmentInfo SelectedAssortment
        {
            get { return (AssortmentInfo)GetValue(SelectedAssortmentProperty); }
            set { SetValue(SelectedAssortmentProperty, value); }
        }

        public ReadOnlyObservableCollection<AssortmentInfo> SearchResults
        {
            get 
            {
                if (_searchResultsRO == null)
                {
                    _searchResultsRO = new ReadOnlyObservableCollection<AssortmentInfo>(_searchResults);
                }
                return _searchResultsRO;
            }
        }
        
        #endregion

        #region Constructor
        public AssortmentSelectionWindow()
        {
            InitializeComponent();
            _availableAssortmentInfos = AssortmentInfoList.FetchByEntityId(App.ViewState.EntityId);

            //set the help file key for this window.
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.AssortmentSelection);

            foreach (var item in _availableAssortmentInfos)
            {
                _searchResults.Add(item);
            }
        } 
        #endregion

        #region Event Handlers

        private void ExtendedDataGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (SelectCommand.CanExecute()) SelectCommand.Execute();
        }

        #endregion

        #region Commands

        #region Select
        private RelayCommand _selectCommand;

        /// <summary>
        /// Selects the window.
        /// </summary>
        public RelayCommand SelectCommand
        {
            get
            {
                if (_selectCommand == null)
                {
                    _selectCommand = new RelayCommand(
                        p => Select_Executed(), p=>Select_CanExecute())
                    {
                        FriendlyName = Message.Generic_Select
                    };
                }
                return _selectCommand;
            }
        }

        private Boolean Select_CanExecute()
        {
            return SelectedAssortment != null;
        }

        private void Select_Executed()
        {
            this.DialogResult = true;
            this.Close();
        }
        #endregion

        #region Cancel
        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            this.DialogResult = false;
            this.Close();
        }
        #endregion

        #endregion

        #region Methods

        public void FilterResults()
        {
            _searchResults.Clear();
            if (String.IsNullOrEmpty(SearchCriteria))
            {
                foreach (var item in _availableAssortmentInfos)
                {
                    _searchResults.Add(item);
                }
                return;
            }
            var matchingAssortments = _availableAssortmentInfos.Where(o => CultureInfo.InvariantCulture.CompareInfo.IndexOf(o.Name,SearchCriteria, CompareOptions.IgnoreCase) >= 0 ||
                CultureInfo.InvariantCulture.CompareInfo.IndexOf(o.ProductGroupName, SearchCriteria, CompareOptions.IgnoreCase) >= 0);
            foreach(var item in matchingAssortments)
            {
                _searchResults.Add(item);
            }
        }

        #endregion

        private void datagrid_PrefilterItem(object sender, ExtendedDataGridFilterEventArgs e)
        {

        }
    }
}
