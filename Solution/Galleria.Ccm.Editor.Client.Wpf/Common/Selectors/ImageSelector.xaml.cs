﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using System.Windows.Controls;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    public partial class ImageSelector : UserControl
    {
        #region Properties

        #region Header Property

        public static readonly DependencyProperty HeaderProperty =
            DependencyProperty.Register("Header", typeof(String), typeof(ImageSelector),
            new PropertyMetadata(String.Empty));

        /// <summary>
        /// Gets/Sets the image header text.
        /// </summary>
        public String Header
        {
            get { return (String)GetValue(HeaderProperty); }
            set { SetValue(HeaderProperty, value); }
        }

        #endregion

        #region ImageName

        public static readonly DependencyProperty ImageNameProperty =
            DependencyProperty.Register("ImageName", typeof(String), typeof(ImageSelector),
            new PropertyMetadata(String.Empty));

        /// <summary>
        /// Gets/Sets the name of the image this represents.
        /// </summary>
        public String ImageName
        {
            get { return (String)GetValue(ImageNameProperty); }
            set { SetValue(ImageNameProperty, value); }
        }

        #endregion

        #region SourceProperty

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(ImageSource), typeof(ImageSelector),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the image source
        /// </summary>
        public ImageSource Source
        {
            get { return (ImageSource)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }

        #endregion

        #region IsLoading Property

        public static readonly DependencyProperty IsLoadingProperty =
            DependencyProperty.Register("IsLoading", typeof(Boolean), typeof(ImageSelector),
            new PropertyMetadata(true));

        /// <summary>
        /// Gets/Sets whether the image loading busy should be displayed.
        /// </summary>
        public Boolean IsLoading
        {
            get { return (Boolean)GetValue(IsLoadingProperty); }
            set { SetValue(IsLoadingProperty, value); }
        }

        #endregion

        #endregion

        #region Events

        #region EditRequested

        public static readonly RoutedEvent EditRequestedEvent =
            EventManager.RegisterRoutedEvent("EditRequested", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ImageSelector));

        public event RoutedEventHandler EditRequested
        {
            add { AddHandler(EditRequestedEvent, value); }
            remove { RemoveHandler(EditRequestedEvent, value); }
        }

        private void OnEditRequested()
        {
            //raise out the selected event
            RaiseEvent(new RoutedEventArgs(ImageSelector.EditRequestedEvent));
        }

        #endregion

        #region ClearRequested

        public static readonly RoutedEvent ClearRequestedEvent =
            EventManager.RegisterRoutedEvent("ClearRequested", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ImageSelector));

        public event RoutedEventHandler ClearRequested
        {
            add { AddHandler(ClearRequestedEvent, value); }
            remove { RemoveHandler(ClearRequestedEvent, value); }
        }

        private void OnClearRequested()
        {
            //raise out the selected event
            RaiseEvent(new RoutedEventArgs(ImageSelector.ClearRequestedEvent));
        }

        #endregion

        #endregion

        #region Constructor

        public ImageSelector() 
        {
            InitializeComponent();
        }

        #endregion

        #region Commands

        #region EditCommand

        private RelayCommand _editCommand;

        /// <summary>
        /// Triggers the event to allow the image to be set.
        /// </summary>
        public RelayCommand EditCommand
        {
            get
            {
                if (_editCommand == null)
                {
                    _editCommand = new RelayCommand(
                        p => Edit_Executed(),
                        p => Edit_CanExecute())
                        {
                            FriendlyName = Message.PlanItemProperties_EditProductImage,
                            SmallIcon = ImageResources.PlanItemProperties_EditProductImage_16
                        };
                }
                return _editCommand;
            }
        }

        private Boolean Edit_CanExecute()
        {
            if (this.IsLoading) { return false; }
            return true;
        }

        private void Edit_Executed()
        {
            OnEditRequested();
        }

        #endregion

        #region ClearCommand

        private RelayCommand _clearCommand;

        public RelayCommand ClearCommand
        {
            get
            {
                if (_clearCommand == null)
                {
                    _clearCommand = new RelayCommand(
                        p => Clear_Executed(),
                        p => Clear_CanExecute())
                    {
                        FriendlyName = Message.PlanItemProperties_ClearProductImage,
                        SmallIcon = ImageResources.PlanItemProperties_ClearProductImage_16
                    };
                }
                return _clearCommand;
            }
        }

        private Boolean Clear_CanExecute()
        {
            if (this.IsLoading) { return false; }
            if (this.Source == null) { return false; }
            return true;
        }

        private void Clear_Executed()
        {
            OnClearRequested();
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out actions on mouse double click.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnMouseDoubleClick(e);

            OnEditRequested();
        }

        #endregion
    }





    ///// <summary>
    ///// Interaction logic for ImageSelector.xaml
    ///// </summary>
    //public sealed partial class ImageSelector : ExtendedRibbonWindow
    //{
    //    #region Fields 
    //    private static String _lastDirectory;
    //    private readonly ObservableCollection<BrowsingImageSelectorImage> _browsingImages = new ObservableCollection<BrowsingImageSelectorImage>();
    //    #endregion

    //    #region Properties

    //    #region ItemSource property

    //    public static readonly DependencyProperty ItemSourceProperty =
    //        DependencyProperty.Register("ItemSource", typeof(IEnumerable<IImageSelectorImage>), typeof(ImageSelector),
    //        new PropertyMetadata(null));

    //    public IEnumerable<IImageSelectorImage> ItemSource
    //    {
    //        get { return (IEnumerable<IImageSelectorImage>)GetValue(ItemSourceProperty); }
    //        set { SetValue(ItemSourceProperty, value); }
    //    }

    //    #endregion

    //    #region SelectedItem property

    //    public static readonly DependencyProperty SelectedItemProperty =
    //        DependencyProperty.Register("SelectedItem", typeof(IImageSelectorImage), typeof(ImageSelector),
    //        new PropertyMetadata(null));

    //    public IImageSelectorImage SelectedItem
    //    {
    //        get { return (IImageSelectorImage)GetValue(SelectedItemProperty); }
    //        set { SetValue(SelectedItemProperty, value); }
    //    }

    //    #endregion

    //    #region Browsing Directory

    //    public static readonly DependencyProperty BrowsingDirectoryProperty =
    //        DependencyProperty.Register("BrowsingDirectory", typeof(String), typeof(ImageSelector),
    //        new PropertyMetadata(Environment.GetFolderPath(Environment.SpecialFolder.MyPictures), OnBrowsingDirectoryPropertyChanged));

    //    private static void OnBrowsingDirectoryPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
    //    {
    //        ImageSelector senderControl = (ImageSelector)obj;

    //        _lastDirectory = e.NewValue as String;

    //        senderControl.ReloadBrowsingImages();
    //    }

    //    public String BrowsingDirectory
    //    {
    //        get { return (String)GetValue(BrowsingDirectoryProperty); }
    //        set { SetValue(BrowsingDirectoryProperty, value); }
    //    }

    //    #endregion

    //    #region BrowsingItemSource

    //    public static readonly DependencyProperty BrowsingItemSourceProperty = 
    //        DependencyProperty.Register("BrowsingItemSource", typeof(ObservableCollection<BrowsingImageSelectorImage>),
    //        typeof(ImageSelector));

    //    public ObservableCollection<BrowsingImageSelectorImage> BrowsingItemSource
    //    {
    //        get { return (ObservableCollection<BrowsingImageSelectorImage>)GetValue(BrowsingItemSourceProperty); }
    //    }

    //    #endregion

    //    #endregion

    //    #region Constructor

    //    /// <summary>
    //    /// Constructor
    //    /// </summary>
    //    public ImageSelector()
    //    {
    //        Mouse.OverrideCursor = Cursors.Wait;

    //        SetValue(BrowsingItemSourceProperty, _browsingImages);

    //        if (!String.IsNullOrEmpty(_lastDirectory))
    //        {
    //            this.BrowsingDirectory = _lastDirectory;
    //        }
    //        else if (!String.IsNullOrEmpty(App.ViewState.Settings.Model.ImageLocation))
    //        {
    //            this.BrowsingDirectory = App.ViewState.Settings.Model.ImageLocation;
    //        }


    //        InitializeComponent();

    //        this.Loaded += This_Loaded;
    //    }

    //    /// <summary>
    //    /// Called on initial load of this window.
    //    /// </summary>
    //    /// <param name="sender"></param>
    //    /// <param name="e"></param>
    //    private void This_Loaded(object sender, RoutedEventArgs e)
    //    {
    //        this.Loaded -= This_Loaded;

    //        ReloadBrowsingImages();

    //        //cancel the busy cursor
    //        Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
    //    }

    //    #endregion

    //    #region Commands

    //    #region SetBrowsingDirectory Command

    //    private RelayCommand _setBrowsingDirectoryCommand;

    //    public RelayCommand SetBrowsingDirectoryCommand
    //    {
    //        get
    //        {
    //            if (_setBrowsingDirectoryCommand == null)
    //            {
    //                _setBrowsingDirectoryCommand = new RelayCommand(
    //                    p => SetBrowsingDirectory_Executed())
    //                {
    //                  FriendlyName = ".."  
    //                };
    //            }
    //            return _setBrowsingDirectoryCommand;
    //        }

    //    }

    //    private void SetBrowsingDirectory_Executed()
    //    {
    //        var dia = new System.Windows.Forms.FolderBrowserDialog();
    //        dia.ShowNewFolderButton = false;
            
    //        if (dia.ShowDialog() == System.Windows.Forms.DialogResult.OK)
    //        {
    //            this.BrowsingDirectory = dia.SelectedPath;
                
    //        }
    //    }

    //    #endregion

    //    #region Select Command

    //    private RelayCommand _selectCommand;

    //    /// <summary>
    //    /// Commits the selection
    //    /// </summary>
    //    public RelayCommand SelectCommand
    //    {
    //        get
    //        {
    //            if (_selectCommand == null)
    //            {
    //                _selectCommand = new RelayCommand(
    //                   P => Select_Executed(),
    //                   p => Select_CanExecute())
    //                   {
    //                       FriendlyName = Message.Generic_OK
    //                   };
    //            }
    //            return _selectCommand;
    //        }
    //    }

    //    private Boolean Select_CanExecute()
    //    {
    //        if (this.SelectedItem == null)
    //        {
    //            return false;
    //        }

    //        return true;
    //    }

    //    private void Select_Executed()
    //    {
    //        this.DialogResult = true;
    //    }

    //    #endregion

    //    #region Cancel Command

    //    private RelayCommand _cancelCommand;

    //    /// <summary>
    //    /// Cancels the selection change.
    //    /// </summary>
    //    public RelayCommand CancelCommand
    //    {
    //        get
    //        {
    //            if (_cancelCommand == null)
    //            {
    //                _cancelCommand = new RelayCommand(
    //                    p => Cancel_Executed())
    //                    {
    //                        FriendlyName = Message.Generic_Cancel
    //                    };
    //            }
    //            return _cancelCommand;
    //        }
    //    }

    //    private void Cancel_Executed()
    //    {
    //        this.DialogResult = false;
    //    }

    //    #endregion

    //    #endregion

    //    #region Methods

    //    /// <summary>
    //    /// Reloads the browsing images collection.
    //    /// </summary>
    //    private void ReloadBrowsingImages()
    //    {
    //        _browsingImages.Clear();

    //        if (!String.IsNullOrEmpty(this.BrowsingDirectory)
    //            && Directory.Exists(this.BrowsingDirectory))
    //        {
    //            String[] fileNames = Directory.GetFiles(this.BrowsingDirectory);

    //            foreach (String fileName in fileNames)
    //            {
    //                if (fileName.EndsWith(".png", StringComparison.OrdinalIgnoreCase)
    //                    || fileName.EndsWith(".jpg", StringComparison.OrdinalIgnoreCase)
    //                    || fileName.EndsWith(".jpeg", StringComparison.OrdinalIgnoreCase)
    //                    || fileName.EndsWith(".bmp", StringComparison.OrdinalIgnoreCase))
    //                {
    //                    _browsingImages.Add(new BrowsingImageSelectorImage(fileName));
    //                }
    //            }   
    //        }

    //    }

    //    #endregion

    //    #region Window Close

    //    protected override void OnClosed(EventArgs e)
    //    {
    //        base.OnClosed(e);

    //        //Dispatcher.BeginInvoke(
    //        //(Action)(() =>
    //        //{
    //        //    if (this.ViewModel != null)
    //        //    {
    //        //        IDisposable dis = this.ViewModel;
    //        //        this.ViewModel = null;

    //        //        if (dis != null)
    //        //        {
    //        //            dis.Dispose();
    //        //        }
    //        //    }

    //        //}), priority: DispatcherPriority.Background);
    //    }

    //    #endregion
    //}

    //public interface IImageSelectorImage
    //{
    //    String Name { get; }
    //    ImageSource ThumbnailSource { get; }
    //}

    //public sealed class BrowsingImageSelectorImage : IImageSelectorImage, INotifyPropertyChanged
    //{
    //    #region Fields
    //    private String _filePath;
    //    private ImageSource _thumbSource;
    //    private Boolean _isLoading = false;
    //    #endregion

    //    #region Binding Property Paths

    //    public static readonly PropertyPath ThumbnailSourceProperty = WpfHelper.GetPropertyPath<BrowsingImageSelectorImage>(p => p.ThumbnailSource);
    //    public static readonly PropertyPath IsLoadingProperty = WpfHelper.GetPropertyPath<BrowsingImageSelectorImage>(p => p.IsLoading);


    //    #endregion

    //    #region Properties

    //    public String Name
    //    {
    //        get { return _filePath; }
    //    }

    //    public ImageSource ThumbnailSource
    //    {
    //        get 
    //        { 
    //            if(_thumbSource ==  null && !_isLoading)
    //            {
    //                LoadThumbnail();
    //            }
                
    //            return _thumbSource; 
    //        }
    //        private set
    //        {
    //            _thumbSource = value;
    //            OnPropertyChanged(ThumbnailSourceProperty);
    //        }
    //    }

    //    public Boolean IsLoading
    //    {
    //        get { return _isLoading; }
    //        private set
    //        {
    //            _isLoading = value;
    //            OnPropertyChanged(IsLoadingProperty);
    //        }
                
    //    }

    //    #endregion

    //    #region Constructor

    //    public BrowsingImageSelectorImage(String filePath)
    //    {
    //        _filePath = filePath;
    //    }

    //    #endregion

    //    #region Methods

    //    private void LoadThumbnail()
    //    {
    //        this.IsLoading = true;

    //        Csla.Threading.BackgroundWorker worker =
    //            new Csla.Threading.BackgroundWorker();

    //        worker.DoWork +=
    //            (s, e) =>
    //            {
    //                ImageSource returnSource = null;

    //                String fileName = e.Argument as String;
    //                if(!String.IsNullOrEmpty(fileName))
    //                {
    //                    if (File.Exists(fileName))
    //                    {
    //                        BitmapImage fullSrc = LocalHelper.GetBitmapImage(new MemoryStream(File.ReadAllBytes(fileName)));
    //                        Double newWidth = 80;
    //                        var scale = newWidth / fullSrc.PixelWidth;
    //                        ScaleTransform transform = new ScaleTransform(scale, scale);
    //                        TransformedBitmap thumb = new TransformedBitmap(fullSrc, transform);
    //                        WriteableBitmap newBitmap = new WriteableBitmap(thumb);
    //                        newBitmap.Freeze();

    //                        returnSource = newBitmap;
    //                    }
    //                }

    //                e.Result = returnSource;
    //            };
            
    //        worker.RunWorkerCompleted +=
    //            (s, e) =>
    //                {
    //                    this.ThumbnailSource = e.Result as ImageSource;
                        
    //                    if(this.ThumbnailSource == null)
    //                    {
    //                        this.ThumbnailSource = ImageResources.ImageSelector_NoImage;
    //                    }

    //                    this.IsLoading = false;

    //                };

    //        worker.RunWorkerAsync(_filePath);
    //    }

    //    #endregion

    //    #region INotifyPropertyChanged Members

    //    public event PropertyChangedEventHandler PropertyChanged;

    //    private void OnPropertyChanged(PropertyPath property)
    //    {
    //        if (PropertyChanged != null)
    //        {
    //            PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
    //        }
    //    }

    //    #endregion
    //}

    //public sealed class PlanogramImageSelectorImage : IImageSelectorImage
    //{
    //    #region Fields
    //    private PlanogramImage _planogramImage;
    //    private ImageSource _thumbSource;
    //    #endregion

    //    #region Properties

    //    public PlanogramImage PlanImage
    //    {
    //        get { return _planogramImage; }
    //    }
    
    //    public String Name
    //    {
    //        get
    //        {
    //            if (_planogramImage != null)
    //            {
    //                return _planogramImage.FileName;
    //            }
    //            return Message.Generic_None;
    //        }
    //    }

    //    public ImageSource ThumbnailSource
    //    {
    //        get
    //        {
    //            if(_thumbSource == null)
    //            {
    //                if (_planogramImage != null)
    //                {
    //                    _thumbSource = LocalHelper.GetBitmapImage(
    //                        new MemoryStream(_planogramImage.ImageData));
    //                }
    //                else
    //                {
    //                    _thumbSource = ImageResources.ImageSelector_NoImage;
    //                }
    //            }
    //            return _thumbSource;
    //        }
    //    }

    //    #endregion

    //    #region Constructor

    //    public PlanogramImageSelectorImage(PlanogramImage planImage)
    //    {
    //        _planogramImage = planImage;
    //    }

    //    #endregion
    //}

    //public sealed class FixtureImageSelectorImage : IImageSelectorImage
    //{
    //    #region Fields
    //    private FixtureImage _image;
    //    private ImageSource _thumbSource;
    //    #endregion

    //    #region Properties

    //    public FixtureImage FixtureImage
    //    {
    //        get { return _image; }
    //    }

    //    public String Name
    //    {
    //        get
    //        {
    //            if (_image != null)
    //            {
    //                return _image.FileName;
    //            }
    //            return Message.Generic_None;
    //        }
    //    }

    //    public ImageSource ThumbnailSource
    //    {
    //        get
    //        {
    //            if (_thumbSource == null)
    //            {
    //                if (_image != null)
    //                {
    //                    _thumbSource = LocalHelper.GetBitmapImage(
    //                        new MemoryStream(_image.ImageData));
    //                }
    //                else
    //                {
    //                    _thumbSource = ImageResources.ImageSelector_NoImage;
    //                }
    //            }
    //            return _thumbSource;
    //        }
    //    }

    //    #endregion

    //    #region Constructor

    //    public FixtureImageSelectorImage(FixtureImage planImage)
    //    {
    //        _image = planImage;
    //    }

    //    #endregion
    //}
}
