﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26705 : A.Kuszyk
//  Created.
// V8-27120 : A.Kuszyk
//  Added option in constructor to remove products from list when selected.
// V8-27255 : A.Kuszyk
//  Added loading cursor when window loads.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using System.Collections.ObjectModel;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Model;
using System.ComponentModel;
using Galleria.Framework.Helpers;
using System.Windows.Threading;

namespace Galleria.Ccm.Editor.Client.Wpf.Common.Selectors
{
    /// <summary>
    /// Interaction logic for ProductLibraryProductSelectionWindow.xaml
    /// </summary>
    public partial class ProductLibraryProductSelectionWindow : ExtendedRibbonWindow
    {
        #region Properties

        public static readonly DependencyProperty ViewModelProperty =
                    DependencyProperty.Register("ViewModel", typeof(ProductLibraryProductSelectionViewModel), typeof(ProductLibraryProductSelectionWindow),
                    new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets/Sets the viewmodel context
        /// </summary>
        public ProductLibraryProductSelectionViewModel ViewModel
        {
            get { return (ProductLibraryProductSelectionViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ProductLibraryProductSelectionWindow senderControl = (ProductLibraryProductSelectionWindow)obj;

            if (e.OldValue != null)
            {
                ProductLibraryProductSelectionViewModel oldModel = (ProductLibraryProductSelectionViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                ProductLibraryProductSelectionViewModel newModel = (ProductLibraryProductSelectionViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }        

        #endregion

        #region Constructor

        public ProductLibraryProductSelectionWindow(IEnumerable<Product> products)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            this.Loaded += ProductLibraryProductSelectionWindow_Loaded;
            InitializeComponent();
            ViewModel = new ProductLibraryProductSelectionViewModel(products);
        }

        public ProductLibraryProductSelectionWindow(IEnumerable<Product> products, Boolean isAddToSelectedVisible, Boolean removeFromListWhenAddToSelected=false)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            this.Loaded += ProductLibraryProductSelectionWindow_Loaded;
            InitializeComponent();
            ViewModel = new ProductLibraryProductSelectionViewModel(products, isAddToSelectedVisible, removeFromListWhenAddToSelected);
        }

        private void ProductLibraryProductSelectionWindow_Loaded(Object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
            this.Loaded -= ProductLibraryProductSelectionWindow_Loaded;
        }
        
        #endregion

        #region Event Handlers

        private void resultsGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.AddToSelectionCommand.Execute();
            }
        }

        /// <summary>
        /// When enter is pressed return selected items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void resultsGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    if (resultsGrid.SelectedItems.Count > 0)
                    {
                        e.Handled = true;
                        if (this.ViewModel != null)
                        {
                            this.ViewModel.SelectAndCloseCommand.Execute();
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region Window Close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }
            }), priority: DispatcherPriority.Background);
        }

        #endregion
    }
}
