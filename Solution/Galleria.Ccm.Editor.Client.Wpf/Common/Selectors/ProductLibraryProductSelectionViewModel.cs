﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26705 : A.Kuszyk
//  Created.
// V8-27120 : A.Kuszyk
//  Added option in constructor to remove products from list when selected.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using System.Collections.ObjectModel;
using Galleria.Ccm.Model;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Framework.Collections;
using Galleria.Framework.Model;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Editor.Client.Wpf.Common.Selectors
{
    public sealed class ProductLibraryProductSelectionViewModel : ViewModelAttachedControlObject<ProductLibraryProductSelectionWindow>
    {
        #region Fields

        private Boolean? _dialogResult;
        private ObservableCollection<Product> _selectedProducts = new ObservableCollection<Product>();
        private String _searchCriteria;
        private Boolean _isSearching;
        private Boolean _containsRecords;
        private readonly BulkObservableCollection<Product> _searchResults = new BulkObservableCollection<Product>();
        private ReadOnlyBulkObservableCollection<Product> _searchResultsRO;
        private IEnumerable<Product> _availableProducts;
        private Boolean _isSearchQueued;
        private Boolean _isAddToSelectionVisible;
        private Boolean _removeFromListWhenAddToSelected = false;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath IsSearchingProperty = WpfHelper.GetPropertyPath<ProductLibraryProductSelectionViewModel>(p => p.IsSearching);
        public static readonly PropertyPath ContainsRecordsProperty = WpfHelper.GetPropertyPath<ProductLibraryProductSelectionViewModel>(p => p.ContainsRecords);
        public static readonly PropertyPath SearchResultsProperty = WpfHelper.GetPropertyPath<ProductLibraryProductSelectionViewModel>(p => p.SearchResults);
        public static readonly PropertyPath SelectedProductsProperty = WpfHelper.GetPropertyPath<ProductLibraryProductSelectionViewModel>(p => p.SelectedProducts);
        public static readonly PropertyPath SearchCriteriaProperty = WpfHelper.GetPropertyPath<ProductLibraryProductSelectionViewModel>(p => p.SearchCriteria);
        public static readonly PropertyPath IsAddToSelectedVisibleProperty = WpfHelper.GetPropertyPath<ProductLibraryProductSelectionViewModel>(p => p.IsAddSelectedVisible);
        public static readonly PropertyPath SelectAndCloseCommandProperty = WpfHelper.GetPropertyPath<ProductLibraryProductSelectionViewModel>(p => p.SelectAndCloseCommand);
        public static readonly PropertyPath AddToSelectionCommandProperty = WpfHelper.GetPropertyPath<ProductLibraryProductSelectionViewModel>(p => p.AddToSelectionCommand);
        public static readonly PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<ProductLibraryProductSelectionViewModel>(p => p.CloseCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;

                if (this.AttachedControl != null)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }

        /// <summary>
        /// Returns the readonly collection of search results
        /// </summary>
        public ReadOnlyBulkObservableCollection<Product> SearchResults
        {
            get
            {
                if (_searchResultsRO == null)
                {
                    _searchResultsRO = new ReadOnlyBulkObservableCollection<Product>(_searchResults);
                }
                return _searchResultsRO;
            }
        }

        /// <summary>
        /// Returns the editable collection of selected products
        /// </summary>
        public ObservableCollection<Product> SelectedProducts
        {
            get { return _selectedProducts; }
        }

        /// <summary>
        /// Gets/Sets the search criteria value.
        /// </summary>
        public String SearchCriteria
        {
            get { return _searchCriteria; }
            set
            {
                _searchCriteria = value;
                OnPropertyChanged(SearchCriteriaProperty);
                PerformSearch();
            }
        }

        /// <summary>
        /// Returns true if a search is currently being performed
        /// </summary>
        public Boolean IsSearching
        {
            get { return _isSearching; }
            private set
            {
                _isSearching = value;
                OnPropertyChanged(IsSearchingProperty);
            }
        }

        /// <summary>
        /// Returns true if the search results grid contains records
        /// </summary>
        public Boolean ContainsRecords
        {
            get { return _containsRecords; }
            private set
            {
                _containsRecords = value;
                OnPropertyChanged(ContainsRecordsProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether the add to selected option should be shown.
        /// </summary>
        public Boolean IsAddSelectedVisible
        {
            get { return _isAddToSelectionVisible; }
            set
            {
                _isAddToSelectionVisible = value;
                OnPropertyChanged(IsAddToSelectedVisibleProperty);
            }
        }

        #endregion

        #region Constructor
        public ProductLibraryProductSelectionViewModel(IEnumerable<Product> products)
        {
            _availableProducts = products;
            _searchResults.AddRange(products);
            ContainsRecords = _searchResults.Count > 0;
        }

        public ProductLibraryProductSelectionViewModel(IEnumerable<Product> products, Boolean isAddToSelectedVisible, Boolean removeFromListWhenAddToSelected=false)
        {
            _availableProducts = products;
            _searchResults.AddRange(products);
            IsAddSelectedVisible = isAddToSelectedVisible;
            ContainsRecords = _searchResults.Count > 0;
            _removeFromListWhenAddToSelected = removeFromListWhenAddToSelected;
        }
        #endregion

        #region Events
        public event EventHandler<EventArgs<IEnumerable<Product>>> AddToSelectionRequested;

        private void RaiseAddToSelection()
        {
            if (this.AddToSelectionRequested != null)
            {
                this.AddToSelectionRequested(this, new EventArgs<IEnumerable<Product>>(this.SelectedProducts.ToList()));
            }
        }
        #endregion

        #region Commands

        #region AddToSelection

        private RelayCommand _addToSelectionCommand;

        /// <summary>
        /// Selects and closes.
        /// </summary>
        public RelayCommand AddToSelectionCommand
        {
            get
            {
                if (_addToSelectionCommand == null)
                {
                    _addToSelectionCommand = new RelayCommand(
                        p => AddToSelection_Executed(),
                        p => AddToSelection_CanExecute())
                    {
                        FriendlyName = Message.ProductLibrarySelection_AddSelected
                    };

                    base.ViewModelCommands.Add(_addToSelectionCommand);
                }
                return _addToSelectionCommand;
            }
        }

        private Boolean AddToSelection_CanExecute()
        {
            return this.SelectedProducts.Count > 0;
        }

        private void AddToSelection_Executed()
        {
            RaiseAddToSelection();
            _availableProducts = _availableProducts.Where(p=>!SelectedProducts.Any(sp=>sp.Id==p.Id));
            _searchResults.RemoveRange(SelectedProducts);
        }

        #endregion

        #region SelectAndClose
        private RelayCommand _selectAndCloseCommand;

        /// <summary>
        /// Selects and closes.
        /// </summary>
        public RelayCommand SelectAndCloseCommand
        {
            get
            {
                if (_selectAndCloseCommand == null)
                {
                    _selectAndCloseCommand = new RelayCommand(
                        p => SelectAndClose_Executed(),
                        p => SelectAndClose_CanExecute())
                    {
                        FriendlyName = Message.ProductLibraryProductSelection_SelectAndClose
                    };
                }
                return _selectAndCloseCommand;
            }
        }

        private Boolean SelectAndClose_CanExecute()
        {
            return SelectedProducts!=null && SelectedProducts.Count > 0;
        }

        private void SelectAndClose_Executed()
        {
            RaiseAddToSelection();
            this.DialogResult = true;
        }
        #endregion

        #region Close
        private RelayCommand _CloseCommand;

        /// <summary>
        /// Closes the window.
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new RelayCommand(
                        p => Close_Executed())
                    {
                        FriendlyName = Message.Generic_Close
                    };
                }
                return _CloseCommand;
            }
        }

        private void Close_Executed()
        {
            this.DialogResult = false;
            this.AttachedControl.DialogResult = false;
            this.AttachedControl.Close();
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Performs the search operation
        /// </summary>
        private void PerformSearch()
        {
            if (!this.IsSearching)
            {
                _isSearchQueued = false;
                this.IsSearching = true;
                FilterResults();
            }
            else
            {
                _isSearchQueued = true;
            }
        }

        private void FilterResults()
        {
            IsSearching = false;
            _searchResults.Clear();
            if (String.IsNullOrEmpty(SearchCriteria))
            {
                _searchResults.AddRange(_availableProducts);
            }
            else
            {
                _searchResults.AddRange(_availableProducts.Where(prod =>
                        prod.Gtin.Contains(SearchCriteria) || 
                        CultureInfo.InvariantCulture.CompareInfo.IndexOf(prod.Name,SearchCriteria, CompareOptions.IgnoreCase) >= 0));
            }

            ContainsRecords = _searchResults.Count > 0;
            if (_isSearchQueued) PerformSearch();
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
