﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Denotes the available zoom actions
    /// </summary>
    public enum ZoomType
    {
        ZoomToFit,
        ZoomToFitHeight,
        ZoomToFitWidth,
        ZoomIn,
        ZoomOut,
        ZoomItems
    }


    /// <summary>
    /// Event Args for sending a zoom request
    /// </summary>
    public sealed class ZoomEventArgs : EventArgs
    {
        #region Properties

        public ZoomType Mode { get; set; }
        public Object Parameter { get; set; }

        #endregion

        #region Constructor

        public ZoomEventArgs(ZoomType mode)
            : this(mode, null) { }

        public ZoomEventArgs(ZoomType mode, Object parameter)
        {
            this.Mode = mode;
            this.Parameter = parameter;
        }

        #endregion
    }


}
