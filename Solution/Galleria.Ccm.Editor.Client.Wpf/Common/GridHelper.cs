﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf.Diagram;
using Galleria.Framework.Controls.Wpf.HelperClasses;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Model;
using Gibraltar.Agent;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Provides helpers for use with the grid control
    /// </summary>
    public static class GridHelper
    {
        #region RowCount property

        /// <summary>
        /// Gets/Sets the number of auto rows in the grid.
        /// </summary>
        public static readonly DependencyProperty RowCountProperty =
            DependencyProperty.RegisterAttached("RowCount", typeof(Int32), typeof(GridHelper),
            new PropertyMetadata(-1, OnRowCountPropertyChanged));

        private static void OnRowCountPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            Grid senderControl = obj as Grid;
            if (senderControl != null)
            {
                SetGridRows(senderControl, (Int32)e.NewValue);
            }
        }

        public static Int32 GetRowCount(Grid grid)
        {
            return (Int32)grid.GetValue(RowCountProperty);
        }

        public static void SetRowCount(Grid grid, Int32 value)
        {
            grid.SetValue(RowCountProperty, value);
        }

        private static void SetGridRows(Grid grd, Int32 rowCount)
        {
            if (rowCount != -1)
            {
                grd.RowDefinitions.Clear();

                for (Int32 i = 0; i < rowCount; i++)
                {
                    grd.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Auto) });
                }

            }
        }

        #endregion

        #region ColumnCount property

        /// <summary>
        /// Gets/Sets the number of auto rows in the grid.
        /// </summary>
        public static readonly DependencyProperty ColumnCountProperty =
            DependencyProperty.RegisterAttached("ColumnCount", typeof(Int32), typeof(GridHelper),
            new PropertyMetadata(-1, OnColumnCountPropertyChanged));

        private static void OnColumnCountPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            Grid senderControl = obj as Grid;
            if (senderControl != null)
            {
                SetGridColumns(senderControl, (Int32)e.NewValue);
            }
        }

        public static Int32 GetColumnCount(Grid grid)
        {
            return (Int32)grid.GetValue(ColumnCountProperty);
        }

        public static void SetColumnCount(Grid grid, Int32 value)
        {
            grid.SetValue(ColumnCountProperty, value);
        }

        private static void SetGridColumns(Grid grd, Int32 columnCount)
        {
            if (columnCount != -1)
            {
                grd.ColumnDefinitions.Clear();

                for (Int32 i = 0; i < columnCount; i++)
                {
                    grd.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) });
                }

            }
        }

        #endregion
    }
}
