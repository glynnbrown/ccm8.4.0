﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0)

// L.Hodson ~ Created.
// V8-26686 : A.Silva ~ Added Planogram value to PlanItemType

#endregion

#region Version History: (CCM 8.3)
//V8-31832 : L.Ineson
//  Added support for ICalculatedValueResolver to IPlanItem
#endregion

#endregion

using System;
using System.ComponentModel;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    public enum PlanItemType
    {
        Fixture,
        Assembly,
        Component,
        SubComponent,
        Position,
        Annotation,
        Product,
        Planogram
    }

    /// <summary>
    ///     Defines a plan item.
    /// </summary>
    public interface IPlanItem : INotifyPropertyChanged
    {
        Boolean IsSelectable { get; set; }
        PlanItemType PlanItemType { get; }

        PlanogramView Planogram { get; }
        PlanogramFixtureView Fixture { get; }
        PlanogramAssemblyView Assembly { get; }
        PlanogramComponentView Component { get; }
        PlanogramSubComponentView SubComponent { get; }
        PlanogramAnnotationView Annotation { get; }
        PlanogramPositionView Position { get; }
        PlanogramProductView Product { get; }


        Single Height { get; }
        Single Width { get; }
        Single Depth { get; }

        Single Slope { get; set; }
        Single Angle { get; set; }
        Single Roll { get; set; }

        Single X { get; set; }
        Single Y { get; set; }
        Single Z { get; set; }
    }
}