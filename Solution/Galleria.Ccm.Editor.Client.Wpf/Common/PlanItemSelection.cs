﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
// V8-24971 : L.Hodson
//  Changes to position structure
// V8-26062 : A.Kuszyk
//  Added additional Product properties from PlanogramProduct model.
//V8-26787 : L.Ineson
//  Changes to planitemfield
#endregion
#region Version History: (CCM 8.1.1)
// V8-30358 : J.Pickup
//  Introduced a limit to the frequency the bulkCollectionChanged event is raised (0.1ms) to handle a serious performance issue.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using sysReflect = System.Reflection;
using System.Windows;
using System.Windows.Media;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Converters;
using Galleria.Framework.Attributes;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using System.Windows.Data;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf.PropertyEditor;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Helpers;
using System.Diagnostics;
using System.Windows.Threading;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{

    /// <summary>
    /// Container class for handling actions against multiple plan items.
    /// </summary>
    public sealed class PlanItemSelection : BulkObservableCollection<IPlanItem>, IDisposable
    {

        #region Fields

        private PlanogramView _plan;
        private Boolean _isLoggingUndoableActions;

        private PlanogramAnnotationMultiView _annotationView;
        private PlanogramComponentMultiView _componentView;
        private PlanogramAssemblyMultiView _assemblyView;
        private PlanogramFixtureMultiView _fixtureView;
        private PlanogramSubComponentMultiView _childSubcomponentView;
        private PlanogramProductMultiView _productView;
        private PlanogramPositionMultiView _positionView;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath PlanogramProperty = GetPropertyPath(p => p.Planogram);
        public static readonly PropertyPath HasComponentsProperty = GetPropertyPath(p => p.HasComponents);
        public static readonly PropertyPath HasPositionsProperty = GetPropertyPath(p => p.HasPositions);

        public static readonly PropertyPath AnnotationViewProperty = GetPropertyPath(p => p.AnnotationView);
        public static readonly PropertyPath ComponentViewProperty = GetPropertyPath(p => p.ComponentView);
        public static readonly PropertyPath AssemblyViewProperty = GetPropertyPath(p => p.AssemblyView);
        public static readonly PropertyPath FixtureViewProperty = GetPropertyPath(p => p.FixtureView);
        public static readonly PropertyPath ChildSubComponentViewProperty = GetPropertyPath(p => p.ChildSubComponentView);
        public static readonly PropertyPath ProductViewProperty = GetPropertyPath(p => p.ProductView);
        public static readonly PropertyPath PositionViewProperty = GetPropertyPath(p => p.PositionView);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets whether changes should be logged as undoable actions.
        /// Note - this requires that a plan was specified on create.
        /// </summary>
        public Boolean IsLoggingUndoableActions
        {
            get { return _plan != null && _isLoggingUndoableActions; }
            set
            {
                _isLoggingUndoableActions = value;

                if (_annotationView != null) _annotationView.IsLoggingUndoableActions = value;
                if (_componentView != null) _componentView.IsLoggingUndoableActions = value;
                if (_assemblyView != null) _assemblyView.IsLoggingUndoableActions = value;
                if (_childSubcomponentView != null) _childSubcomponentView.IsLoggingUndoableActions = value;
            }
        }

        /// <summary>
        /// Returns the planogram parent for this selection.
        /// </summary>
        public PlanogramView Planogram
        {
            get { return _plan; }
        }

        public DisplayUnitOfMeasureCollection DisplayUOMs
        {
            get
            {
                if (_plan != null)
                {
                    return DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(_plan.Model);
                }
                else return DisplayUnitOfMeasureCollection.Empty;
            }
        }



        public Boolean HasComponents
        {
            get { return ComponentView != null; }
        }

        public Boolean HasPositions
        {
            get { return PositionView != null; }
        }


        public PlanogramAnnotationMultiView AnnotationView
        {
            get
            {
                if (_annotationView == null && !_isDisposed)
                {
                    var annotationItems = Items.Where(i => i.Annotation != null).Select(i => i.Annotation).Distinct();

                    if (annotationItems.Any())
                    {
                        _annotationView = new PlanogramAnnotationMultiView(annotationItems);
                        _annotationView.IsLoggingUndoableActions = IsLoggingUndoableActions;
                    }
                }
                return _annotationView;
            }
        }

        public PlanogramComponentMultiView ComponentView
        {
            get
            {
                if (_componentView == null && !_isDisposed)
                {
                    var componentItems = Items.Where(i => i.Component != null).Select(i => i.Component).Distinct();

                    if (componentItems.Any())
                    {
                        _componentView = new PlanogramComponentMultiView(componentItems);
                        _componentView.IsLoggingUndoableActions = IsLoggingUndoableActions;
                    }
                }
                return _componentView;
            }
        }

        public PlanogramAssemblyMultiView AssemblyView
        {
            get
            {
                if (_assemblyView == null && !_isDisposed)
                {
                    var assemblyItems = Items.Where(p => p.Assembly != null).Select(i => i.Assembly);

                    if (assemblyItems.Any())
                    {
                        _assemblyView = new PlanogramAssemblyMultiView(assemblyItems);
                        _assemblyView.IsLoggingUndoableActions = IsLoggingUndoableActions;
                    }
                }
                return _assemblyView;
            }
        }

        public PlanogramFixtureMultiView FixtureView
        {
            get
            {
                if (_fixtureView == null && !_isDisposed)
                {
                    var fixtureItems = Items.Where(p => p.Fixture != null).Select(i => i.Fixture).Distinct();

                    if (fixtureItems.Any())
                    {
                        _fixtureView = new PlanogramFixtureMultiView(fixtureItems);
                        _fixtureView.IsLoggingUndoableActions = IsLoggingUndoableActions;
                    }
                }
                return _fixtureView;
            }
        }

        public PlanogramSubComponentMultiView ChildSubComponentView
        {
            get
            {
                if (_childSubcomponentView == null && !_isDisposed)
                {
                    var componentItems = Items.Where(i => i.Component != null).Select(i => i.Component).Distinct();
                    var childSubComponentItems = componentItems.SelectMany(c => c.SubComponents);

                    if (childSubComponentItems.Any())
                    {
                        _childSubcomponentView = new PlanogramSubComponentMultiView(childSubComponentItems);
                        _childSubcomponentView.IsLoggingUndoableActions = IsLoggingUndoableActions;
                    }
                }
                return _childSubcomponentView;
            }
        }

        public PlanogramProductMultiView ProductView
        {
            get
            {
                if (_productView == null && !_isDisposed)
                {
                    var productItems = Items.Where(p => p.Product != null).Select(i => i.Product).Distinct();

                    if (productItems.Any())
                    {
                        _productView = new PlanogramProductMultiView(productItems);
                        _productView.IsLoggingUndoableActions = IsLoggingUndoableActions;
                    }
                }
                return _productView;
            }
        }

        public PlanogramPositionMultiView PositionView
        {
            get
            {
                if (_positionView == null && !_isDisposed)
                {
                    var positionItems = Items.Where(p => p.Position != null).Select(i => i.Position);

                    if (positionItems.Any())
                    {
                        _positionView = new PlanogramPositionMultiView(positionItems);
                        _positionView.IsLoggingUndoableActions = IsLoggingUndoableActions;
                    }
                }
                return _positionView;
            }
        }

        #endregion

        #region Event

        public event EventHandler SelectionChanged;
        private void RaiseSelectionChanged()
        {
            if (SelectionChanged != null) 
                SelectionChanged(this, EventArgs.Empty);
        }
        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public PlanItemSelection()
        {
            IsLoggingUndoableActions = false;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="planView">The planogram to log edits against.</param>
        public PlanItemSelection(PlanogramView planView)
        {
            _plan = planView;
            IsLoggingUndoableActions = true;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out actions whenever a bulk collection change is performed
        /// </summary>
        /// <param name="e"></param>
        protected override void OnBulkCollectionChanged(BulkCollectionChangedEventArgs e)
        {
            ClearViews();
            base.OnBulkCollectionChanged(e);

            //[V8-30358] - Performance issue with constantly polling property changed, no need to update this for less than every 0.1ms.
            OnBulkCollectionChangedDelayed();
        }

        private Boolean _propertyChangeDispatcherInProgress = false;
        /// <summary>
        /// Calls a property change event for all reflected items but at a maximum of once per tenth of a second for performance.
        /// </summary>
        private void OnBulkCollectionChangedDelayed()
        {
            if (_propertyChangeDispatcherInProgress) return;

            _propertyChangeDispatcherInProgress = true;

            var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(0.15) };
            timer.Start();
            timer.Tick += (senderr, args) =>
            {
                timer.Stop();

                foreach (sysReflect.PropertyInfo propertyInfo in this.GetType().GetProperties())
                {
                    OnPropertyChanged(propertyInfo.Name);
                }
                RaiseSelectionChanged();
                _propertyChangeDispatcherInProgress = false;
            };
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a list of property descriptions for the current
        /// selection.
        /// </summary>
        /// <returns></returns>
        public List<PropertyItemDescription> GetPropertyDefinitions(UserEditorSettings defaultSettings)
        {
            List<PropertyItemDescription> propertyList = new List<PropertyItemDescription>();

            var uomConverter = Application.Current.FindResource(ResourceKeys.ConverterUnitDisplay) as IValueConverter;

            if (Items.Count > 0)
            {
                IEnumerable<ObjectFieldInfo> masterFields = PlanItemHelper.EnumerateAllFields((_plan != null)? _plan.Model : null);
                List<PlanItemType> itemTypes = Items.Select(p => p.PlanItemType).Distinct().ToList();

                Boolean incAnnotationProperties = itemTypes.Contains(PlanItemType.Annotation);
                Boolean incPositionProperties = itemTypes.Contains(PlanItemType.Position);
                Boolean incComponentProperties = itemTypes.Contains(PlanItemType.Component);
                Boolean incAssemblyProperties = itemTypes.Contains(PlanItemType.Assembly);
                Boolean incFixtureProperties = itemTypes.Count == 1 && Items[0] is PlanogramFixtureView;

                #region Annotation
                if (incAnnotationProperties)
                {
                    foreach (var field in PlanogramAnnotationView.EnumerateDefaultPlanItemFields())
                    {
                        propertyList.Add(PlanItemHelper.GetPropertyDescription(field, uomConverter, this.DisplayUOMs, AnnotationViewProperty.Path, Message.Generic_Annotation));
                    }
                }
                #endregion

                #region Position
                if (incPositionProperties)
                {
                    String prodPrefix = ProductViewProperty.Path + ".";
                    String posPrefix = PositionViewProperty.Path + ".";

                    List<ObjectFieldInfo> fields =
                    ObjectFieldInfo.ExtractFieldsFromText(defaultSettings.PositionProperties, masterFields);

                    if (fields.Count > 0)
                    {
                        foreach (ObjectFieldInfo field in fields)
                        {
                            String groupName = String.Empty;
                            String pathPrefix = String.Empty;

                            switch (PlanItemHelper.GetPlanItemType(field.OwnerType))
                            {
                                case PlanItemType.Position:
                                    groupName = Message.Generic_Position;
                                    pathPrefix = PositionViewProperty.Path;
                                    break;

                                case PlanItemType.Product:
                                    groupName = Message.Generic_Product;
                                    pathPrefix = ProductViewProperty.Path;
                                    break;

                                case PlanItemType.SubComponent:
                                    groupName = Message.Generic_SubComponent;
                                    pathPrefix = ChildSubComponentViewProperty.Path;
                                    break;

                                case PlanItemType.Component:
                                     groupName = Message.Generic_Component;
                                     pathPrefix = ComponentViewProperty.Path;
                                    break;

                                case PlanItemType.Fixture:
                                    groupName = Message.Generic_Fixture;
                                     pathPrefix = FixtureViewProperty.Path;
                                    break;

                                default:
                                    Debug.Fail("Not handled");
                                    continue;
                            }

                            propertyList.Add(PlanItemHelper.GetPropertyDescription(field, uomConverter, this.DisplayUOMs, pathPrefix, groupName));

                           
                        }
                    }
                    else
                    {
                        foreach (var field in PlanogramProductView.EnumerateDefaultPlanItemFields())
                        {
                            propertyList.Add(PlanItemHelper.GetPropertyDescription(field, uomConverter, this.DisplayUOMs, ProductViewProperty.Path, Message.Generic_Product));
                        }

                        foreach (var field in PlanogramPositionView.EnumerateDefaultPlanItemFields())
                        {
                            propertyList.Add(PlanItemHelper.GetPropertyDescription(field, uomConverter, this.DisplayUOMs, PositionViewProperty.Path, Message.Generic_Position));
                        }
                    }
                }
                #endregion

                #region Component
                if (incComponentProperties)
                {
                    List<ObjectFieldInfo> fields =  ObjectFieldInfo.ExtractFieldsFromText(defaultSettings.ComponentProperties, masterFields);

                    if (fields.Count > 0)
                    {
                        foreach (ObjectFieldInfo field in fields)
                        {
                            String groupName = String.Empty;
                            String pathPrefix = String.Empty;

                            switch (PlanItemHelper.GetPlanItemType(field.OwnerType))
                            {
                                case PlanItemType.SubComponent:
                                    groupName = Message.Generic_SubComponent;
                                    pathPrefix = ChildSubComponentViewProperty.Path;
                                    break;

                                case PlanItemType.Component:
                                    groupName = Message.Generic_Component;
                                    pathPrefix = ComponentViewProperty.Path;
                                    break;

                                case PlanItemType.Fixture:
                                    groupName = Message.Generic_Fixture;
                                    pathPrefix = FixtureViewProperty.Path;
                                    break;

                                default:
                                    Debug.Fail("Not handled");
                                    continue;
                            }

                            propertyList.Add(PlanItemHelper.GetPropertyDescription(field, uomConverter, this.DisplayUOMs, pathPrefix, groupName));
                        }
                    }
                    else
                    {
                        foreach (var field in PlanogramComponentView.EnumerateDefaultPlanItemFields())
                        {
                            propertyList.Add(PlanItemHelper.GetPropertyDescription(field, uomConverter, this.DisplayUOMs, ComponentViewProperty.Path, Message.Generic_Component));
                        }

                        foreach (var field in PlanogramSubComponentView.EnumerateDefaultPlanItemFields())
                        {
                            if (ChildSubComponentView.ShowProperty(field.PropertyName))
                            {
                                propertyList.Add(PlanItemHelper.GetPropertyDescription(field, uomConverter, this.DisplayUOMs, ChildSubComponentViewProperty.Path, Message.Generic_Component));
                            }
                        }

                    }
                }
                #endregion

                #region Assembly
                if (incAssemblyProperties)
                {
                    foreach (var field in PlanogramAssemblyView.EnumerateDefaultPlanItemFields())
                    {
                        propertyList.Add(PlanItemHelper.GetPropertyDescription(field, uomConverter, this.DisplayUOMs, AssemblyViewProperty.Path, Message.Generic_Assembly));
                    }
                }
                #endregion

                #region Fixture
                if (incFixtureProperties)
                {
                    foreach (var field in PlanogramFixtureMultiView.GetPlanItemFields())
                    {
                        propertyList.Add(PlanItemHelper.GetPropertyDescription(field, uomConverter, this.DisplayUOMs, FixtureViewProperty.Path, Message.Generic_Fixture));
                    }
                }
                #endregion
            }
            else
            {
                //show the properties for the planogram.
                foreach (var field in PlanogramView.EnumerateDefaultPlanItemFields())
                {
                    propertyList.Add(PlanItemHelper.GetPropertyDescription(field, uomConverter, this.DisplayUOMs, PlanogramProperty.Path, Message.Generic_Planogram));
                }
            }


            return propertyList;
        }

        /// <summary>
        /// Clears down and disposes of the current views.
        /// </summary>
        private void ClearViews()
        {
            //clear off old annotation view.
            if (_annotationView != null)
            {
                _annotationView.Dispose();
                _annotationView = null;
            }

            if (_componentView != null)
            {
                _componentView.Dispose();
                _componentView = null;
            }

            if (_assemblyView != null)
            {
                _assemblyView.Dispose();
                _assemblyView = null;
            }

            if (_fixtureView != null)
            {
                _fixtureView.Dispose();
                _fixtureView = null;
            }

            if (_childSubcomponentView != null)
            {
                _childSubcomponentView.Dispose();
                _childSubcomponentView = null;
            }

            if (_positionView != null)
            {
                _positionView.Dispose();
                _positionView = null;
            }

            if (_productView != null)
            {
                _productView.Dispose();
                _productView = null;
            }
        }

        /// <summary>
        /// Updates the selected plan items collection to those given.
        /// </summary>
        public void SetSelection(IPlanItem itemToSelect)
        {
            //clear if not empty
            if (this.Count > 0)
            {
                this.Clear();
            }

            this.Add(itemToSelect);
        }
        /// <summary>
        /// Updates the selected plan items collection to those given.
        /// </summary>
        public void SetSelection(IEnumerable<IPlanItem> itemsToSelect)
        {
            //clear if not empty
            if (this.Count > 0)
            {
                this.Clear();
            }

            this.AddRange(itemsToSelect.Where(i=> i != null).Distinct());
        }

        /// <summary>
        /// Checks that the selection is valid.
        /// If any item is for a deleted model this will try to select the correct view instead.
        /// </summary>
        public void ValidateCurrentSelection()
        {
            if(_plan == null) return;

            foreach (IPlanItem item in this.Items.ToArray())
            {
                if (!item.IsSelectable)
                {
                    this.Items.Remove(item);
                    continue;
                }

                try
                {
                    switch (item.PlanItemType)
                    {
                        case PlanItemType.Component:
                            if (item.Component.IsInvalid)
                            {
                                //refind it
                                IPlanItem newItem = _plan.EnumerateAllFixtureComponents().FirstOrDefault(c => c.ComponentModel == item.Component.ComponentModel);
                                this.Items.Remove(item);
                                if (newItem != null) this.Items.Add(newItem);
                            }
                            break;
                    }
                }
                catch (Exception) { }
            }
        }

        public void UpdateOnSelectionTypeChanged()
        {

        }

        #region Overrides

        /// <summary>
        /// Override of AddRange method to ensure that duplicates cannot
        /// be added.
        /// </summary>
        public new void AddRange(IEnumerable<IPlanItem> range)
        {
            base.AddRange(range.Distinct());
        }

        /// <summary>
        /// Override of Add method to ensure that
        /// duplicates cannot be added.
        /// </summary>
        /// <param name="item"></param>
        public new void Add(IPlanItem item)
        {
            if (!this.Contains(item)) base.Add(item);
        }

        #endregion

        #endregion

        #region INotifyPropertyChanged Members

        private void OnPropertyChanged(String propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        #endregion


        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                ClearViews();

                _isDisposed = true;
                GC.SuppressFinalize(this);
            }
        }

        #endregion

        #region Static Helpers

        private static PropertyPath GetPropertyPath(System.Linq.Expressions.Expression<Func<PlanItemSelection, Object>> expression)
        {
            return WpfHelper.GetPropertyPath<PlanItemSelection>(expression);
        }

        #endregion

    }

}
