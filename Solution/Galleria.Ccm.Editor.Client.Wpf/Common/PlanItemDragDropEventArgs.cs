﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// L.Hodson ~ Created
// V8-25734 : L.Ineson
//  Moved out rendering controls to framework.

#endregion

#region Version History: (CCM 8.3.0)

// V8-29333 : M.Shelley
//  Added the DragSource property
//  V8-31788 : A.Silva
//      Moved CreatePreviewModels() and GetPlanItemsWithPositions() from PlanVisualDocumentView.xaml.cs.
//      Added RemoveOrphanedDragDropPositions()

#endregion

#endregion

using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Framework.Controls.Wpf.Rendering;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    public sealed class PlanItemDragDropArgs
    {
        public FrameworkElement DragSource { get; set; }
        public List<PlanDragDropItem> DragItems { get; private set; }

        public Boolean IsCreateCopy { get; set; }

        public PlanItemDragDropArgs()
        {
            this.DragItems = new List<PlanDragDropItem>();
        }

        /// <summary>
        ///     Create <see cref="ModelConstruct3D"/> instances from the collection of <see cref="PlanDragDropItem"/> in this Drag Drop operation.
        /// </summary>
        /// <param name="isDynamicTextScalingEnabled">Whether dynamic text scaling is to be used or not.</param>
        /// <returns>This method returns an array of all the <c>Preview Models</c> for the <see cref="DragItems"/>.</returns>
        public ModelConstruct3D[] CreatePreviewModels(Boolean isDynamicTextScalingEnabled)
        {
            var models = new ModelConstruct3D[DragItems.Count];
            //  Get the Dragged Plan Items that contain Positions that are being dragged as well.
            //  These will need their positions to be hidden depending on whether they are currently being dragged or not.
            List<IPlanItem> planItemsWithPositions = GetPlanItemsWithPositions();

            var idx = 0;
            foreach (IPlanItem planItem in DragItems.Select(item => item.PlanItem))
            {
                ModelConstruct3D itemDragModel = PlanDocumentHelper.CreateModel(planItem);
                if (itemDragModel == null) return null;

                itemDragModel.IsDynamicTextScalingEnabled = isDynamicTextScalingEnabled;
                itemDragModel.ModelData.IsWireframe = true;

                //  When creating copies, components may be dragged without some or all of their positions.
                //  Check whether each one needs to be hidden.
                if (IsCreateCopy && (App.ViewState.SelectionMode == PlanItemSelectionType.OnlyComponents ||
                                     planItemsWithPositions.Contains(planItem)))
                {
                    foreach (PlanPosition3DData posData in
                        itemDragModel.ModelData.GetAllChildModels().OfType<PlanPosition3DData>())
                    {
                        //  When positions could be hidden their visibility depends on whether they are also being dragged or not.
                        posData.IsVisibleOverride = DragItems.Any(item => Equals(item.PlanItem, (IPlanItem)posData.PlanPosition));
                    }
                }

                //  Any position being dragged needs to be hidden if there are any other types being dragged too.
                if (planItem.PlanItemType == PlanItemType.Position &&
                    DragItems.Any(item => item.PlanItem.PlanItemType != PlanItemType.Position))
                {
                    itemDragModel.ModelData.IsVisible = false;
                }

                models[idx] = itemDragModel;
                idx++;
            }

            return models;
        }

        /// <summary>
        ///     Remove any drag drop position that is not being dragged with their parent.
        /// </summary>
        /// <remarks>If nothing but positions are being drag dropped nothing will be removed.</remarks>
        public void RemoveOrphanedDragDropPositions()
        {
            //  Checke whether we are drag dropping a mix of types, or just positions.
            if (DragItems.All(item => item.PlanItem.PlanItemType == PlanItemType.Position)) return;

            List<IPlanItem> nonPositionDragDropItems =
                DragItems.Select(item => item.PlanItem).Where(item => item.PlanItemType != PlanItemType.Position).ToList();

            //  Remove any position whose parent is not being dragged too.
            DragItems.RemoveAll(item =>
                                item.PlanItem.PlanItemType == PlanItemType.Position &&
                                !nonPositionDragDropItems.Contains((IPlanItem)item.PlanItem.Component ?? item.PlanItem.Assembly));
        }

        /// <summary>
        ///     Get a list of <see cref="IPlanItem"/> instances that are being dragged 
        ///     and contains positions that are being dragged as well.
        /// </summary>
        /// <returns></returns>
        public List<IPlanItem> GetPlanItemsWithPositions()
        {
            return
                DragItems.Select(dragDropItem => dragDropItem.PlanItem)
                         .Where(planItem => planItem.PlanItemType == PlanItemType.Position)
                         .Select(planItem => planItem.Component ?? (IPlanItem) planItem.Assembly)
                         .ToList();
        }
    }

    public sealed class PlanDragDropItem
    {
        public IPlanItem PlanItem { get; set; }// the item.
        public ModelConstruct3D Model3DVis { get; set; }// the actual models that were moved.
        public PointValue DragOffset { get; set; } //the dragging position offset from the mouse point.

        public Object LastHitTestResult { get; set; }
    }

}