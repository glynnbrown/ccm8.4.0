﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
// V8-25734 : L.Ineson
//  Moved out rendering controls to framework.
// CCM-25917 : N.Haywood
//  Changed odd behaviour of resizing
// V8-26684 : A.Silva ~ Added Friendly name for PlanItemType.Planogram.
//V8-26787 : L.Ineson
//  Changes to planitemfield
// V8-27477 : L.Ineson
//  Removed old world space related methods.
// V8-28175 : A.Kuszyk
//  Added optional Planogram parameter to GetPlanogramFieldSelectorGroups.
// V8-28607 : N.Haywood
//  Added defensive code to ResolveField
// V8-28407 : L.Ineson
//  Added subcomponent to resolve field.
#endregion
#region Version History: (CCM 8.10)
// V8-29028 : L.Ineson
//  Resolve field to text now applies uom converter.
// V8-28662 : L.Ineson
//  Moved most of field methods to PlanogramHelper.
#endregion
#region Version History: (CCM 8.20)
// V8-31383 : J.Pickup
//  Using keyboard Up / Down now moves to next notch. Unless there is no notch then it moves 1f;
// V8-31399 : A.Probyn
//  Added defensive code to RepositionItems for the fix added under 31383. Only components were being considered.
//  Updated RepositionItems so that plan items get reposition, even if the parent bay is selected.
#endregion
#region Version History: (CCM 8.30)
// V8-31541 : L.Ineson
//  GetPlanItems now returns for all filter types.
// V8-31883 : A.Kuszyk
//  Corrected issue in RepositionItems relating to subcomponents being moved.
// V8-31626 : A.Heathcote
//  Changed the Percentage Switch Case in PropertyItemDescription to convert in the same way as the others,
//  as percentages did not display "%" in the UI.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf.PropertyEditor;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Planograms.Merchandising;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    public static class PlanItemHelper
    {
        #region Binding Property Paths

        public static readonly PropertyPath FixtureProperty = WpfHelper.GetPropertyPath<IPlanItem>(p => p.Fixture);
        public static readonly PropertyPath AssemblyProperty = WpfHelper.GetPropertyPath<IPlanItem>(p => p.Assembly);
        public static readonly PropertyPath ComponentProperty = WpfHelper.GetPropertyPath<IPlanItem>(p => p.Component);
        public static readonly PropertyPath SubComponentProperty = WpfHelper.GetPropertyPath<IPlanItem>(p => p.SubComponent);
        public static readonly PropertyPath PositionProperty = WpfHelper.GetPropertyPath<IPlanItem>(p => p.Position);
        public static readonly PropertyPath PlanProductProperty = WpfHelper.GetPropertyPath<IPlanItem>(p => p.Product);
        public static readonly PropertyPath AnnotationProperty = WpfHelper.GetPropertyPath<IPlanItem>(p => p.Annotation);
        public static readonly PropertyPath XProperty = WpfHelper.GetPropertyPath<IPlanItem>(p => p.X);
        public static readonly PropertyPath YProperty = WpfHelper.GetPropertyPath<IPlanItem>(p => p.Y);
        public static readonly PropertyPath ZProperty = WpfHelper.GetPropertyPath<IPlanItem>(p => p.Z);
        public static readonly PropertyPath AngleProperty = WpfHelper.GetPropertyPath<IPlanItem>(p => p.Angle);
        public static readonly PropertyPath SlopeProperty = WpfHelper.GetPropertyPath<IPlanItem>(p => p.Slope);
        public static readonly PropertyPath RollProperty = WpfHelper.GetPropertyPath<IPlanItem>(p => p.Roll);
        public static readonly PropertyPath HeightProperty = WpfHelper.GetPropertyPath<IPlanItem>(p => p.Height);
        public static readonly PropertyPath WidthProperty = WpfHelper.GetPropertyPath<IPlanItem>(p => p.Width);
        public static readonly PropertyPath DepthProperty = WpfHelper.GetPropertyPath<IPlanItem>(p => p.Depth);
        #endregion

        #region General

        public static readonly Dictionary<PlanItemType, String> FriendlyNames =
            new Dictionary<PlanItemType, String>()
            {
                {PlanItemType.Fixture, Message.Generic_Fixture },
                {PlanItemType.Assembly, Message.Generic_Assembly },
                {PlanItemType.Component, Message.Generic_Component},
                {PlanItemType.SubComponent, Message.Generic_SubComponent},
                {PlanItemType.Position, Message.Generic_Position},
                {PlanItemType.Product, Message.Generic_Product},
                {PlanItemType.Annotation, Message.Generic_Annotation},
                {PlanItemType.Planogram, Message.Generic_Planogram}
            };

        /// <summary>
        /// Returns all items from the plan based on the given filter type.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="filterType"></param>
        /// <returns></returns>
        public static List<IPlanItem> GetPlanItems(PlanogramView plan, PlanItemType? filterType)
        {
            if (plan == null) return new List<IPlanItem>();

            if (filterType.HasValue)
            {
                switch (filterType.Value)
                {
                    case PlanItemType.Planogram: return new List<IPlanItem> { plan };
                    case PlanItemType.Fixture: return plan.Fixtures.Cast<IPlanItem>().ToList();
                    case PlanItemType.Assembly: return plan.EnumerateAllAssemblies().Cast<IPlanItem>().ToList();
                    case PlanItemType.Component: return plan.EnumerateAllComponents().Cast<IPlanItem>().ToList();
                    case PlanItemType.SubComponent: return plan.EnumerateAllSubComponents().Cast<IPlanItem>().ToList();
                    case PlanItemType.Position: return plan.EnumerateAllPositions().Cast<IPlanItem>().ToList();
                    case PlanItemType.Product: return plan.Products.Cast<IPlanItem>().ToList();
                    case PlanItemType.Annotation: return plan.EnumerateAllAnnotations().Cast<IPlanItem>().ToList();
                    default: throw new NotImplementedException();
                }
            }
            else
            {
                List<IPlanItem> items = new List<IPlanItem>();

                foreach (var fixture in plan.Fixtures)
                {
                    items.Add(fixture);

                    foreach (var assembly in fixture.Assemblies)
                    {
                        items.Add(assembly);
                    }

                    foreach (var component in
                        fixture.Assemblies.SelectMany(c => c.Components)
                        .Union(fixture.Components))
                    {
                        items.Add(component);

                        foreach (var subComponent in component.SubComponents)
                        {
                            items.Add(subComponent);

                            foreach (var position in subComponent.Positions)
                            {
                                items.Add(position);
                            }
                        }
                    }
                }
                items.AddRange(plan.EnumerateAllAnnotations());
                items.AddRange(plan.Products);

                return items;
            }

        }

        /// <summary>
        /// Returns a list of all child subcomponents of the given item.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static List<IPlanItem> GetAllChildItemsOfType(IPlanItem item, PlanItemType childItemType)
        {
            List<IPlanItem> returnList = new List<IPlanItem>();

            switch (item.PlanItemType)
            {
                #region Fixture
                case PlanItemType.Fixture:
                    if (childItemType == PlanItemType.Fixture)
                    {
                        returnList.Add(item);
                    }
                    else
                    {
                        if (childItemType == PlanItemType.Assembly)
                        {
                            foreach (var assemblyView in item.Fixture.Assemblies)
                            {
                                returnList.Add((IPlanItem)assemblyView);
                            }
                        }
                        else
                        {
                            List<PlanogramComponentView> childComponents = new List<PlanogramComponentView>();

                            foreach (var assemblyView in item.Fixture.Assemblies)
                            {
                                foreach (var componentView in assemblyView.Components)
                                {
                                    childComponents.Add(componentView);
                                }
                            }
                            childComponents.AddRange(item.Fixture.Components);

                            if (childItemType == PlanItemType.Component)
                            {
                                returnList.AddRange(childComponents);
                            }
                            else if (childItemType == PlanItemType.SubComponent)
                            {
                                foreach (var componentView in childComponents)
                                {
                                    foreach (var subcomponentView in componentView.SubComponents)
                                    {
                                        returnList.Add((IPlanItem)subcomponentView);
                                    }
                                }
                            }
                        }

                    }
                    break;
                #endregion

                #region Assembly
                case PlanItemType.Assembly:
                    if (childItemType == PlanItemType.Assembly)
                    {
                        returnList.Add(item);
                    }
                    else
                    {
                        foreach (var componentView in item.Assembly.Components)
                        {
                            if (childItemType == PlanItemType.Component)
                            {
                                returnList.Add((IPlanItem)componentView);
                            }
                            else
                            {
                                foreach (var subcomponentView in componentView.SubComponents)
                                {
                                    if (childItemType == PlanItemType.SubComponent)
                                    {
                                        returnList.Add((IPlanItem)subcomponentView);
                                    }
                                }
                            }
                        }
                    }
                    break;
                #endregion

                #region Component
                case PlanItemType.Component:
                    if (childItemType == PlanItemType.Component)
                    {
                        returnList.Add(item);
                    }
                    else
                    {
                        foreach (var subcomponentView in item.Component.SubComponents)
                        {
                            if (childItemType == PlanItemType.SubComponent)
                            {
                                returnList.Add((IPlanItem)subcomponentView);
                            }
                        }
                    }
                    break;
                #endregion

                case PlanItemType.SubComponent:
                    if (childItemType == PlanItemType.SubComponent)
                    {
                        returnList.Add(item);
                    }
                    break;

            }



            return returnList;
        }

        /// <summary>
        /// Returns all plan items which are a child of that given
        /// This does not include the item itself.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static List<IPlanItem> GetAllChildItems(IPlanItem item)
        {
            List<IPlanItem> returnList = new List<IPlanItem>();

            switch (item.PlanItemType)
            {
                #region Fixture
                case PlanItemType.Fixture:
                    {
                        foreach (var assembly in item.Fixture.Assemblies)
                        {
                            returnList.Add(assembly);

                            foreach (var component in assembly.Components)
                            {
                                returnList.Add(component);

                                foreach (var sub in component.SubComponents)
                                {
                                    returnList.Add(sub);

                                    foreach (var pos in sub.Positions)
                                    {
                                        returnList.Add(pos);
                                    }
                                    foreach (var anno in sub.Annotations)
                                    {
                                        returnList.Add(anno);
                                    }
                                }
                            }
                        }
                        foreach (var component in item.Fixture.Components)
                        {
                            returnList.Add(component);

                            foreach (var sub in component.SubComponents)
                            {
                                returnList.Add(sub);

                                foreach (var pos in sub.Positions)
                                {
                                    returnList.Add(pos);
                                }
                                foreach (var anno in sub.Annotations)
                                {
                                    returnList.Add(anno);
                                }
                            }
                        }
                        foreach (var anno in item.Fixture.Annotations)
                        {
                            returnList.Add(anno);
                        }

                    }
                    break;
                #endregion

                case PlanItemType.Assembly:
                    {
                        foreach (var component in item.Assembly.Components)
                        {
                            returnList.Add(component);

                            foreach (var sub in component.SubComponents)
                            {
                                returnList.Add(sub);

                                foreach (var pos in sub.Positions)
                                {
                                    returnList.Add(pos);
                                }
                                foreach (var anno in sub.Annotations)
                                {
                                    returnList.Add(anno);
                                }
                            }
                        }
                    }
                    break;

                #region Component
                case PlanItemType.Component:
                    foreach (var sub in item.Component.SubComponents)
                    {
                        returnList.Add(sub);

                        foreach (var pos in sub.Positions)
                        {
                            returnList.Add(pos);
                        }
                        foreach (var anno in sub.Annotations)
                        {
                            returnList.Add(anno);
                        }
                    }
                    break;
                #endregion

                case PlanItemType.SubComponent:
                    foreach (var pos in item.SubComponent.Positions)
                    {
                        returnList.Add(pos);
                    }
                    foreach (var anno in item.SubComponent.Annotations)
                    {
                        returnList.Add(anno);
                    }
                    break;

                case PlanItemType.Position:
                case PlanItemType.Annotation:
                case PlanItemType.Product:
                    //no child items.
                    break;


            }



            return returnList;
        }

        /// <summary>
        /// Returns the value for given plan item at the given level
        /// </summary>
        /// <param name="item"></param>
        /// <param name="levelType"></param>
        /// <returns></returns>
        public static IPlanItem GetItemValue(IPlanItem item, PlanItemType levelType)
        {
            switch (levelType)
            {
                case PlanItemType.Annotation: return item.Annotation;
                case PlanItemType.Assembly: return item.Assembly;
                case PlanItemType.Component: return item.Component;
                case PlanItemType.Fixture: return item.Fixture;
                case PlanItemType.Position: return item.Position;
                case PlanItemType.Product: return item.Product;
                case PlanItemType.SubComponent: return item.SubComponent;
                case PlanItemType.Planogram: return item.Planogram;
                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Returns the order of plan item types fixture first
        /// </summary>
        /// <returns></returns>
        public static List<PlanItemType> GetTypeOrderDesc()
        {
            return
                new List<PlanItemType>()
                {
                    PlanItemType.Fixture,
                    PlanItemType.Assembly,
                    PlanItemType.Component,
                    PlanItemType.SubComponent,
                    PlanItemType.Position,
                    PlanItemType.Annotation,
                    PlanItemType.Product,
                };
        }

        /// <summary>
        /// Returns the order of plan item types product first
        /// </summary>
        /// <returns></returns>
        public static List<PlanItemType> GetTypeOrderAsc()
        {
            List<PlanItemType> order = GetTypeOrderDesc();
            order.Reverse();
            return order;
        }

        /// <summary>
        /// Returns true if the given plan item type is a fixture type.
        /// </summary>
        /// <param name="itemType"></param>
        /// <returns></returns>
        public static Boolean IsFixtureType(PlanItemType itemType)
        {
            switch (itemType)
            {
                case PlanItemType.Fixture:
                case PlanItemType.Assembly:
                case PlanItemType.Component:
                case PlanItemType.SubComponent:
                    return true;

                default: return false;
            }
        }

        /// <summary>
        /// Returns the first selectable parent of the given item
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static IPlanItem GetSelectableItem(IPlanItem item)
        {
            if (item.Position != null && item.Position.IsSelectable) return item.Position;
            if (item.Annotation != null && item.Annotation.IsSelectable) return item.Annotation;
            if (item.SubComponent != null && item.SubComponent.IsSelectable) return item.SubComponent;
            if (item.Component != null && item.Component.IsSelectable) return item.Component;
            if (item.Assembly != null && item.Assembly.IsSelectable) return item.Assembly;
            if (item.Fixture != null && item.Fixture.IsSelectable) return item.Fixture;
            return null;
        }

        /// <summary>
        /// Returns a list of the given items resolved to their
        /// highest distinct values.
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public static List<IPlanItem> GetHighestDistinctItems(IEnumerable<IPlanItem> items)
        {
            List<IPlanItem> returnList = items.Distinct().ToList();

            //positions to subcomponents
            //foreach (var subGroup in returnList.Where(p => p.PlanItemType == PlanItemType.Position).GroupBy(p => p.SubComponent))
            //{
            //    PlanogramSubComponentView subView = subGroup.Key;
            //    if (subGroup.Count() == subView.Positions.Count)
            //    {
            //        foreach (var pos in subGroup)
            //        {
            //            returnList.Remove(pos);
            //        }
            //        returnList.Add(subView);
            //    }
            //}

            //subcomponents -> components
            foreach (var compGroup in
                returnList.Where(p => p.PlanItemType == PlanItemType.SubComponent).GroupBy(p => p.Component))
            {
                PlanogramComponentView compView = compGroup.Key;
                if (compGroup.Count() == compView.SubComponents.Count)
                {
                    foreach (var sub in compGroup)
                    {
                        returnList.Remove(sub);
                    }
                    returnList.Add(compView);
                }
            }

            //components -> assemblies
            foreach (var assemblyGroup in
                returnList.Where(p => p.PlanItemType == PlanItemType.Component).GroupBy(p => p.Assembly))
            {
                if (assemblyGroup.Key != null)
                {
                    PlanogramAssemblyView assemblyView = assemblyGroup.Key;
                    if (assemblyGroup.Count() == assemblyView.Components.Count)
                    {
                        foreach (var comp in assemblyGroup)
                        {
                            returnList.Remove(comp);
                        }
                        returnList.Add(assemblyView);
                    }
                }
            }

            //components & assemblies to fixture.
            foreach (var fixtureGroup in returnList.Where(p => p.PlanItemType == PlanItemType.Assembly
                || p.PlanItemType == PlanItemType.Component).GroupBy(p => p.Fixture))
            {
                PlanogramFixtureView fixture = fixtureGroup.Key;
                if (fixtureGroup.Where(c => c.PlanItemType == PlanItemType.Assembly).Count() == fixture.Assemblies.Count
                    && fixtureGroup.Where(c => c.PlanItemType == PlanItemType.Component).Count() == fixture.Components.Count)
                {
                    foreach (var item in fixtureGroup)
                    {
                        returnList.Remove(item);
                    }
                    returnList.Add(fixture);
                }
            }



            return returnList;
        }

        public static PlanItemType GetPlanItemType(Type ownerType)
        {
            if (ownerType == typeof(PlanogramFixtureItem)
                || ownerType == typeof(PlanogramFixture))
            {
                return PlanItemType.Fixture;
            }
            else if (ownerType == typeof(PlanogramFixtureAssembly)
                || ownerType == typeof(PlanogramAssembly))
            {
                return PlanItemType.Assembly;
            }
            else if (ownerType == typeof(PlanogramFixtureComponent)
                || ownerType == typeof(PlanogramAssemblyComponent)
                || ownerType == typeof(PlanogramComponent))
            {
                return PlanItemType.Component;
            }
            else if (ownerType == typeof(PlanogramSubComponent))
            {
                return PlanItemType.SubComponent;
            }
            else if (ownerType == typeof(PlanogramProduct))
            {
                return PlanItemType.Product;
            }
            else if (ownerType == typeof(PlanogramPosition))
            {
                return PlanItemType.Position;
            }
            else if (ownerType == typeof(PlanogramAnnotation))
            {
                return PlanItemType.Annotation;
            }
            else if (ownerType == typeof(Planogram))
            {
                return PlanItemType.Planogram;
            }

            return PlanItemType.Planogram;
        }

        public static PlanItemType ToPlanItemType(PlanogramItemType partType)
        {
            switch (partType)
            {
                case PlanogramItemType.Annotation: return PlanItemType.Annotation;
                case PlanogramItemType.Assembly: return PlanItemType.Assembly;
                case PlanogramItemType.Component: return PlanItemType.Component;
                case PlanogramItemType.Fixture: return PlanItemType.Fixture;
                case PlanogramItemType.Planogram: return PlanItemType.Planogram;
                case PlanogramItemType.Position: return PlanItemType.Position;
                case PlanogramItemType.Product: return PlanItemType.Product;
                case PlanogramItemType.SubComponent: return PlanItemType.SubComponent;
                default: return PlanItemType.Position;
            }
        }


        public static PlanogramItemPlacement ToPlanogramItemPlacement(this IPlanItem planItem)
        {
            return PlanogramItemPlacement.NewPlanogramItemPlacement(
                (planItem.Position != null)?planItem.Position.Model : null,
                (planItem.Product != null)?planItem.Product.Model : null,
                (planItem.SubComponent != null)?planItem.SubComponent.Model : null,
                (planItem.Component != null)?planItem.Component.ComponentModel : null,
                (planItem.Component != null)?planItem.Component.FixtureComponentModel : null,
                (planItem.Component != null)?planItem.Component.AssemblyComponentModel : null,
                (planItem.Assembly != null)?planItem.Assembly.AssemblyModel : null,
                (planItem.Assembly != null)?planItem.Assembly.FixtureAssemblyModel : null,
                (planItem.Fixture != null)?planItem.Fixture.FixtureModel : null,
                (planItem.Fixture != null)?planItem.Fixture.FixtureItemModel : null,
                (planItem.Planogram != null)?planItem.Planogram.Model : null);

        }

        /// <summary>
        /// Strips out all child items that are already accounted for by a parent.
        /// </summary>
        /// <param name="sourceList"></param>
        /// <returns></returns>
        public static List<IPlanItem> StripOutCommonChildren(IEnumerable<IPlanItem> sourceList)
        {
            List<IPlanItem> returnList = new List<IPlanItem>();

            //extract all fixtures
            returnList.AddRange(sourceList.Where(p => p.PlanItemType == PlanItemType.Fixture).Distinct());

            //assemblies
            foreach (IPlanItem assembly in sourceList.Where(p => p.PlanItemType == PlanItemType.Assembly))
            {
                if (!returnList.Contains(assembly.Fixture)) returnList.Add(assembly);
            }

            //components
            foreach (IPlanItem component in sourceList.Where(p => p.PlanItemType == PlanItemType.Component))
            {
                if (!returnList.Contains(component.Fixture)
                    && !returnList.Contains(component.Assembly))
                {
                    returnList.Add(component);
                }
            }

            //positions
            foreach (IPlanItem position in sourceList.Where(p => p.PlanItemType == PlanItemType.Position))
            {
                if (!returnList.Contains(position.Fixture)
                    && (position.Assembly == null || !returnList.Contains(position.Assembly))
                    && !returnList.Contains(position.Component))
                {
                    returnList.Add(position);
                }
            }

            //products
            foreach (IPlanItem product in sourceList.Where(p => p.PlanItemType == PlanItemType.Product))
            {
                if (!returnList.Any(p=> p.Product == product))
                {
                    returnList.Add(product);
                }
            }

            //Annotations
            foreach (IPlanItem annotation in sourceList.Where(p => p.PlanItemType == PlanItemType.Annotation))
            {
                if (annotation.Annotation.IsFixtureAnnotation && !returnList.Contains(annotation.Fixture))
                {
                    returnList.Add(annotation);
                }
            }

            return returnList;
        }


        #endregion

        #region Transformations

        public static void SetPosition(IPlanItem item, Double x, Double y, Double z)
        {
            item.X = Convert.ToSingle(x);
            item.Y = Convert.ToSingle(y);
            item.Z = Convert.ToSingle(z);
        }
        public static void SetPosition(IPlanItem item, PointValue position)
        {
            item.X = position.X;
            item.Y = position.Y;
            item.Z = position.Z;
        }

        public static void SetRotation(IPlanItem item, RotationValue rotation)
        {
            item.Angle = rotation.Angle;
            item.Slope = rotation.Slope;
            item.Roll = rotation.Roll;
        }

        public static void SetSize(IPlanItem item, WidthHeightDepthValue size)
        {
            switch (item.PlanItemType)
            {
                case PlanItemType.Component:
                    item.Component.Height = size.Height;
                    item.Component.Width = size.Width;
                    item.Component.Depth = size.Depth;
                    break;

                case PlanItemType.Annotation:
                    item.Annotation.Height = size.Height;
                    item.Annotation.Width = size.Width;
                    item.Annotation.Depth = size.Depth;
                    break;

                case PlanItemType.Fixture:
                    item.Fixture.Height = size.Height;
                    item.Fixture.Width = size.Width;
                    item.Fixture.Depth = size.Depth;
                    break;

                case PlanItemType.SubComponent:
                    item.SubComponent.Height = size.Height;
                    item.SubComponent.Width = size.Width;
                    item.SubComponent.Depth = size.Depth;
                    break;

                default:
                    throw new ArgumentException();

            }
        }

        /// <summary>
        /// Resizes the given plan items by the provided change vector.
        /// </summary>
        public static Boolean ResizeItems(IEnumerable<IPlanItem> items, Double changeX, Double changeY, Double changeZ)
        {
            Boolean valueChanged = false;
            foreach (IPlanItem item in items)
            {
                PlanogramView plan = item.Planogram;

                if (item.PlanItemType == PlanItemType.Annotation)
                {
                    PlanogramAnnotationView anno = item.Annotation;

                    //Height
                    Single newHeight = Convert.ToSingle(anno.Height + changeY);
                    if (newHeight != anno.Height)
                    {
                        if (newHeight > 0.01f)
                        {
                            anno.Height = newHeight;
                            valueChanged = true;
                        }
                        else
                        {
                            anno.Height = 0.01f;
                        }
                    }

                    //Width
                    Single newWidth = Convert.ToSingle(anno.Width + changeX);
                    if (newWidth != anno.Width)
                    {
                        if (newWidth > 0.01f)
                        {
                            anno.Width = newWidth;
                            valueChanged = true;
                        }
                        else
                        {
                            anno.Width = 0.01f;
                        }
                    }

                    //Depth
                    Single newDepth = Convert.ToSingle(anno.Depth + changeZ);
                    if (newDepth != anno.Depth)
                    {
                        if (newDepth > 0.01f)
                        {
                            anno.Depth = newDepth;
                            valueChanged = true;
                        }
                        else
                        {
                            anno.Depth = 0.01f;
                        }
                    }
                }
                else if (PlanItemHelper.IsFixtureType(item.PlanItemType))
                {
                    //we always resize the subcomponents
                    List<IPlanItem> resizeItems = PlanItemHelper.GetAllChildItemsOfType(item, PlanItemType.SubComponent);

                    foreach (IPlanItem ri in resizeItems)
                    {
                        if (ri.Component.ComponentType == PlanogramComponentType.Backboard ||
                            ri.Component.ComponentType == PlanogramComponentType.Base) continue;

                        PlanogramSubComponentView sub = ri.SubComponent;

                        //Height
                        Single newHeight = Convert.ToSingle(sub.Height + changeY);
                        if (newHeight != sub.Height)
                        {
                            if (newHeight > 0.01f)
                            {
                                sub.Height = newHeight;
                                valueChanged = true;
                            }
                            else
                            {
                                sub.Height = 0.01f;
                            }
                        }

                        //Width
                        Single newWidth = Convert.ToSingle(sub.Width + changeX);
                        if (newWidth != sub.Width)
                        {
                            if (newWidth > 0.01f)
                            {
                                sub.Width = newWidth;
                                valueChanged = true;
                            }
                            else
                            {
                                sub.Width = 0.01f;
                            }
                        }

                        //Depth
                        Single newDepth = Convert.ToSingle(sub.Depth + changeZ);
                        if (newDepth != sub.Depth)
                        {
                            if (newDepth > 0.01f)
                            {
                                sub.Depth = newDepth;
                                valueChanged = true;
                            }
                            else
                            {
                                sub.Depth = 0.01f;
                            }
                        }
                    }
                }
            }

            return valueChanged;

        }

        /// <summary>
        /// Repositions the given plan items according to the provided change vector.
        /// Items will be moved at the highest possible level based on the provided selection.
        /// </summary>
        public static void RepositionItems(IEnumerable<IPlanItem> items, Double changeX, Double changeY, Double changeZ)
        {
            List<IPlanItem> moveItems = GetHighestDistinctItems(items);

            foreach (IPlanItem moveItem in moveItems)
            {
                // If the item is a component, snap it to notches.
                Int32? currentNotchNumber = null;
                if (moveItem.PlanItemType == PlanItemType.Component)
                {
                    currentNotchNumber = moveItem.Component.NotchNumber;

                    if (changeY > 0)
                    {
                        moveItem.Component.NotchNumber++;
                    }
                    else if (changeY < 0)
                    {
                        moveItem.Component.NotchNumber--;
                    }

                    if (currentNotchNumber == moveItem.Component.NotchNumber && changeY != 0) moveItem.Y = moveItem.Y + Convert.ToSingle(changeY);
                    if (changeX != 0) moveItem.X = moveItem.X + Convert.ToSingle(changeX);
                    if (changeZ != 0) moveItem.Z = moveItem.Z + Convert.ToSingle(changeZ);
                }

                //If fixture type, check for child items
                if (PlanItemHelper.IsFixtureType(moveItem.PlanItemType))
                {

                    List<IPlanItem> moveChildItems = PlanItemHelper.GetAllChildItemsOfType(moveItem, PlanItemType.SubComponent);

                    foreach (IPlanItem item in moveChildItems)
                    {
                        if (item.Component.ComponentType == PlanogramComponentType.Backboard ||
                            item.Component.ComponentType == PlanogramComponentType.Base ||
                            item.PlanItemType == PlanItemType.SubComponent) continue;

                        currentNotchNumber = null;

                        // If the child is a component, snap it to notches.
                        if (item.PlanItemType == PlanItemType.Component)
                        {
                            currentNotchNumber = item.Component.NotchNumber;

                            if (changeY > 0)
                            {
                                item.Component.NotchNumber++;
                            }
                            else if (changeY < 0)
                            {
                                item.Component.NotchNumber--;
                            }

                            //If component type, apply notch logic of If no change occured then we werent on a notch so should adjust by 1f. 
                            if (currentNotchNumber == item.Component.NotchNumber && changeY != 0) item.Y = item.Y + Convert.ToSingle(changeY);
                        }

                        //If not a component, ignore notch logic and default back to original 
                        if ((item.PlanItemType != PlanItemType.Component && item.PlanItemType != PlanItemType.SubComponent) && changeY != 0) item.Y = item.Y + Convert.ToSingle(changeY);
                        if (changeX != 0) item.X = item.X + Convert.ToSingle(changeX);
                        if (changeZ != 0) item.Z = item.Z + Convert.ToSingle(changeZ);
                    }
                }
                else
                {
                    //Move not fixture type item
                    if (changeY != 0) moveItem.Y = moveItem.Y + Convert.ToSingle(changeY);
                    if (changeX != 0) moveItem.X = moveItem.X + Convert.ToSingle(changeX);
                    if (changeZ != 0) moveItem.Z = moveItem.Z + Convert.ToSingle(changeZ);
                }
            }
        }

        public static void FinalizeItemPositions(IEnumerable<IPlanItem> items)
        {
            if (items.Count() == 0) return;

            PlanogramView plan = items.First().Planogram;
            plan.BeginUpdate();

            foreach (IPlanItem item in items)
            {
                if (item.PlanItemType == PlanItemType.Position)
                {
                    item.Position.SetWorldCoordinates(item.Position.MetaWorldX, item.Position.MetaWorldY, item.Position.MetaWorldZ);
                }
                else if (item.PlanItemType == PlanItemType.Component)
                {
                    item.Component.SetWorldCoordinates(item.Component.MetaWorldX, item.Component.MetaWorldY, item.Component.MetaWorldZ);
                }
                else if (item.PlanItemType == PlanItemType.Assembly)
                {
                    item.Assembly.SetWorldCoordinates(item.Assembly.WorldX, item.Assembly.WorldY, item.Assembly.WorldZ);
                }
            }

            plan.EndUpdate();
        }

        /// <summary>
        /// Rotates the given plan items according to the provided change vector.
        /// Items will be moved at the highest possible level based on the provided selection.
        /// </summary>
        /// <param name="items"></param>
        /// <param name="slopeChange">the slope rotation change in radians</param>
        /// <param name="angleChange">the angle rotation change in radians </param>
        /// <param name="rollChange"> the roll rotation change in radians</param>
        public static void RotateItems(IEnumerable<IPlanItem> items, Double slopeChange, Double angleChange, Double rollChange)
        {
            List<IPlanItem> moveItems = GetHighestDistinctItems(items);

            foreach (IPlanItem item in moveItems)
            {
                PlanogramView plan = item.Planogram;

                Double newDegrees;

                //Angle
                if (angleChange != 0)
                {
                    newDegrees = CommonHelper.ToDegrees(item.Angle + angleChange);
                    item.Angle = Convert.ToSingle(CommonHelper.ToRadians(newDegrees));
                }

                //Slope
                if (slopeChange != 0)
                {
                    newDegrees = CommonHelper.ToDegrees(item.Slope + slopeChange);
                    item.Slope = Convert.ToSingle(CommonHelper.ToRadians(newDegrees));
                }

                //Roll
                if (rollChange != 0)
                {
                    newDegrees = CommonHelper.ToDegrees(item.Roll + rollChange);
                    item.Roll = Convert.ToSingle(CommonHelper.ToRadians(newDegrees));
                }

            }
        }

        #endregion

        #region Field Values

        /// <summary>
        /// Returns a list of all avialable plan item fields.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateAllFields(Planogram context = null)
        {
            foreach (var f in PlanogramFieldHelper.EnumerateAllFields()) yield return f;
        }

        /// <summary>
        /// Returns the property description for this field.
        /// </summary>
        public static PropertyItemDescription GetPropertyDescription(ObjectFieldInfo field,
            IValueConverter uomConverter, DisplayUnitOfMeasureCollection uomValues,
            String pathPrefix = null, String category = null)
        {
            PropertyItemDescription propertyDesc = new PropertyItemDescription()
            {
                DisplayName = field.PropertyFriendlyName,
                PropertyName = (pathPrefix == null) ? field.PropertyName : pathPrefix + "." + field.PropertyName,
                CategoryName = category,
            };

            //set the converter
            switch (field.PropertyDisplayType)
            {
                case ModelPropertyDisplayType.None:
                case ModelPropertyDisplayType.Angle:
                    // do nothing
                    break;

                case ModelPropertyDisplayType.Percentage:
                    propertyDesc.Converter = uomConverter;
                    propertyDesc.ConverterParameter = uomValues.Percentage;
                    break;

                case ModelPropertyDisplayType.Length:
                    propertyDesc.Converter = uomConverter;
                    propertyDesc.ConverterParameter = uomValues.Length;
                    break;

                case ModelPropertyDisplayType.Currency:
                    propertyDesc.Converter = uomConverter;
                    propertyDesc.ConverterParameter = uomValues.Currency;
                    break;

                case ModelPropertyDisplayType.Area:
                    propertyDesc.Converter = uomConverter;
                    propertyDesc.ConverterParameter = uomValues.Area;
                    break;

                case ModelPropertyDisplayType.Volume:
                    propertyDesc.Converter = uomConverter;
                    propertyDesc.ConverterParameter = uomValues.Volume;
                    break;
            }



            return propertyDesc;
        }

        [Obsolete("use CommonHelper.GetPlanogramFieldSelectorGroups")]
        public static List<FieldSelectorGroup> GetPlanogramFieldSelectorGroups(Planogram planogram = null)
        {
            return CommonHelper.GetAllPlanogramFieldSelectorGroups(planogram);
            //List<FieldSelectorGroup> groups = new List<FieldSelectorGroup>();

            //groups.Add(new FieldSelectorGroup(FriendlyNames[PlanItemType.Product],
            //    PlanogramProductView.EnumerateDisplayableFields(planogram)));

            //groups.Add(new FieldSelectorGroup(FriendlyNames[PlanItemType.Position],
            //    PlanogramPositionView.EnumerateDisplayableFields()));

            //groups.Add(new FieldSelectorGroup(FriendlyNames[PlanItemType.SubComponent],
            //    PlanogramSubComponentView.EnumerateDisplayableFields()));

            //groups.Add(new FieldSelectorGroup(FriendlyNames[PlanItemType.Component],
            //    PlanogramComponentView.EnumerateDisplayableFields()));

            //groups.Add(new FieldSelectorGroup(FriendlyNames[PlanItemType.Fixture],
            //    PlanogramFixtureView.EnumerateDisplayableFields()));

            //groups.Add(new FieldSelectorGroup(FriendlyNames[PlanItemType.Planogram],
            //   PlanogramView.EnumerateDisplayableFields()));

            //return groups;
        }

        public static List<FieldSelectorGroup> GetPlanFixtureFieldSelectorGroups()
        {
            List<FieldSelectorGroup> groups = new List<FieldSelectorGroup>();

            groups.Add(new FieldSelectorGroup(FriendlyNames[PlanItemType.SubComponent],
                PlanogramSubComponentView.EnumerateDisplayableFields()));

            groups.Add(new FieldSelectorGroup(FriendlyNames[PlanItemType.Component],
                PlanogramComponentView.EnumerateDisplayableFields()));

            groups.Add(new FieldSelectorGroup(FriendlyNames[PlanItemType.Fixture],
                PlanogramFixtureView.EnumerateDisplayableFields()));

            groups.Add(new FieldSelectorGroup(FriendlyNames[PlanItemType.Planogram],
               PlanogramView.EnumerateDisplayableFields()));

            return groups;
        }

        #endregion

        #region Property Multiviews

        /// <summary>
        /// Gets the common value of the given property for all the items
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static T GetCommonPropertyValue<T>(IEnumerable<Object> items, String propertyName)
        {
            T value = default(T);

            if (items.Any())
            {
                Type classType = items.First().GetType();
                System.Reflection.PropertyInfo pInfo = classType.GetProperty(propertyName);
                if (pInfo != null)
                {
                    Boolean isFirst = true;
                    foreach (Object item in items)
                    {
                        T itemValue = (T)pInfo.GetValue(item, null);
                        if (isFirst)
                        {
                            value = itemValue;
                        }
                        else if (!Object.Equals(itemValue, value))
                        {
                            return default(T);
                        }

                        isFirst = false;
                    }
                }
            }

            return value;

        }

        /// <summary>
        /// Sets the given value against
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        public static void SetPropertyValue<T>(IEnumerable<Object> items, String propertyName, T value,
            Boolean logUndoableActions, PlanogramView plan)
        {
            if (items.Any())
            {

                //Start logging the change
                Boolean isProcessingUndoable = false;
                if (logUndoableActions)
                {
                    if (!plan.IsRecordingUndoableAction)
                    {
                        isProcessingUndoable = true;
                        plan.BeginUndoableAction(propertyName);
                    }
                }

                //make the property change
                Type classType = items.First().GetType();
                System.Reflection.PropertyInfo pInfo = classType.GetProperty(propertyName);

                if (pInfo != null)
                {
                    foreach (Object item in items)
                    {
                        pInfo.SetValue(item, value, null);
                    }
                }

                //Commit the log.
                if (isProcessingUndoable)
                {
                    plan.EndUndoableAction();
                }
            }
        }

        #endregion

    }
}
