﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Ineson ~ Created.
// V8-25734 : L.Ineson
//  Moved out rendering controls to framework.
// CCM-25854 : N.Haywood
//  Added logic to handle formulas
// CCM-25897 : N.Haywood
//  Changed labels to display no data when there's no data
// V8-25921 : A.Silva ~ Moved the ToObservableCollection extension from a separate file.
// V8-26686 : A.Silva ~ Added extension IsAggregable() for PlanItemTypes.
//V8-26787 : L.Ineson
//  Changes to planitemfield
#endregion

#region Version History: (CCM 8.2)
// V8-30936 : M.Brumby
//  Added slotwall
// V8-31093 : A.Silva
//  Added helper method to get the name of the temp file import folder.
// V8-31490 : D.Pleasance
//  Amended MouseToolTypeToComponentType() to include Slot Wall implementation
#endregion

#region Version History: CCM830

// V8-32327 : A.Silva
//  Added SortPositionsToMoveByDirection extension method.
// V8-31788 : A.Silva
//  Amended AddPlanItemCopies so that duplicate positions are not copied.

#endregion

#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf.Diagram;
using Galleria.Framework.Controls.Wpf.HelperClasses;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Model;
using Gibraltar.Agent;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    #region Local Helper

    public static class LocalHelper
    {
        #region Extensions

        /// <summary>
        ///     Checks wether the <paramref name="type"/> of <see cref="PlanItemType"/> can be aggregated or not in a summary column or metric.
        /// </summary>
        /// <param name="type">The value to have its aggregability checked.</param>
        /// <returns><c>True</c> if the <paramref name="type"/> is aggregable, <c>false</c> otherwise.</returns>
        public static Boolean IsAggregable(this PlanItemType type)
        {
            return type == PlanItemType.Product 
                || type == PlanItemType.Position 
                || type == PlanItemType.SubComponent 
                || type == PlanItemType.Component 
                || type == PlanItemType.Fixture;
        }

        public static IEnumerable<DependencyObject> EnumerateVisualChildren(this DependencyObject dep)
        {
            for (Int32 i = 0; i < VisualTreeHelper.GetChildrenCount(dep); i++)
            {
                yield return VisualTreeHelper.GetChild(dep, i);
            }
        }

        public static IEnumerable<DependencyObject> EnumerateVisualDescendents(this DependencyObject dep)
        {
            yield return dep;

            foreach (DependencyObject child in dep.EnumerateVisualChildren())
            {
                yield return child;

                foreach (DependencyObject descendent in child.EnumerateVisualDescendents())
                {
                    yield return descendent;
                }
            }
        }

        /// <summary>
        /// Updates all binding sources on the given dependency object
        /// </summary>
        /// <param name="dependencyObject"></param>
        public static void UpdateBindingSources(this DependencyObject dependencyObject)
        {
            if (dependencyObject == null) return;

            foreach (DependencyObject element in dependencyObject.EnumerateVisualDescendents())
            {
                LocalValueEnumerator localValueEnumerator = element.GetLocalValueEnumerator();
                while (localValueEnumerator.MoveNext())
                {
                    BindingExpressionBase bindingExpression = BindingOperations.GetBindingExpressionBase(element, localValueEnumerator.Current.Property);
                    if (bindingExpression != null)
                    {
                        bindingExpression.UpdateSource();
                    }
                }
            }
        }

        /// <summary>
        /// Updates the binding source of the currently focused element on
        /// the given dependency object.
        /// </summary>
        /// <param name="obj"></param>
        public static void UpdateFocusedBindingSource(DependencyObject obj)
        {
            if (obj != null)
            {
                DependencyObject focusedElement = FocusManager.GetFocusedElement(obj) as DependencyObject;
                if (focusedElement != null)
                {
                    LocalValueEnumerator localValueEnumerator = focusedElement.GetLocalValueEnumerator();
                    while (localValueEnumerator.MoveNext())
                    {
                        BindingExpressionBase bindingExpression = BindingOperations.GetBindingExpressionBase(focusedElement, localValueEnumerator.Current.Property);
                        if (bindingExpression != null)
                        {
                            bindingExpression.UpdateSource();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Returns true if there are no binding validation errors
        /// </summary>
        /// <param name="dep"></param>
        /// <returns></returns>
        public static Boolean AreBindingsValid(this DependencyObject dep)
        {
            if (dep != null)
            {
                return !Validation.GetHasError(dep) &&
                    LogicalTreeHelper.GetChildren(dep).OfType<DependencyObject>().All(child => AreBindingsValid(child));
            }
            else
            {
                //unit testing so just return true
                return true;
            }
        }

        #endregion

       
        /// <summary>
        /// Records that the given exception has occurred
        /// </summary>
        /// <param name="ex"></param>
        public static void RecordException(Exception ex, String exceptionCategory = "Handled")
        {
            Debug.WriteLine(ex.Message);

            //notify gibraltar
            Log.RecordException(ex, exceptionCategory, /*canContinue*/true);

            //[TODO] place the exception message in some sort of local event log?
        }

        /// <summary>
        /// Converts the given tooltype to a component type.
        /// </summary>
        /// <param name="toolType"></param>
        /// <returns></returns>
        public static FixtureComponentType? MouseToolTypeToComponentType(MouseToolType toolType)
        {
            switch (toolType)
            {
                //case MouseToolType.CreateBackboard: return FixtureComponentType.Backboard;
                case MouseToolType.CreateBar: return FixtureComponentType.Bar;
                case MouseToolType.CreateChest: return FixtureComponentType.Chest;
                case MouseToolType.CreatePallet: return FixtureComponentType.Pallet;
                case MouseToolType.CreatePanel: return FixtureComponentType.Panel;
                case MouseToolType.CreatePegboard: return FixtureComponentType.Peg;
                case MouseToolType.CreateShelf: return FixtureComponentType.Shelf;
                case MouseToolType.CreateRod: return FixtureComponentType.Rod;
                case MouseToolType.CreateClipstrip: return FixtureComponentType.ClipStrip;
                case MouseToolType.CreateCustom: return FixtureComponentType.Custom;
                case MouseToolType.CreateSlotWall: return FixtureComponentType.SlotWall;
                    
                default: return null;
            }
        }

        /// <summary>
        /// Converts the given tooltype to a component type.
        /// </summary>
        /// <param name="toolType"></param>
        /// <returns></returns>
        public static PlanogramComponentType? MouseToolTypeToPlanogramComponentType(MouseToolType toolType)
        {
            switch (toolType)
            {
                //case MouseToolType.CreateBackboard: return PlanogramComponentType.Backboard;
                case MouseToolType.CreateBar: return PlanogramComponentType.Bar;
                case MouseToolType.CreateChest: return PlanogramComponentType.Chest;
                case MouseToolType.CreatePallet: return PlanogramComponentType.Pallet;
                case MouseToolType.CreatePanel: return PlanogramComponentType.Panel;
                case MouseToolType.CreatePegboard: return PlanogramComponentType.Peg;
                case MouseToolType.CreateShelf: return PlanogramComponentType.Shelf;
                case MouseToolType.CreateRod: return PlanogramComponentType.Rod;
                case MouseToolType.CreateClipstrip: return PlanogramComponentType.ClipStrip;
                case MouseToolType.CreateCustom: return PlanogramComponentType.Custom;
                case MouseToolType.CreateSlotWall: return PlanogramComponentType.SlotWall;
                default: return null;
            }
        }

        /// <summary>
        /// Returns the default size for the given component type.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        public static WidthHeightDepthValue GetDefaultComponentSize(FixtureComponentType type, UserEditorSettings settings)
        {
            Single height;
            Single width;
            Single depth;

            GetDefaultComponentSize(type, settings, out height, out width, out depth);

            return new WidthHeightDepthValue(width, height, depth);
        }


        public static void GetDefaultComponentSize(FixtureComponentType type, UserEditorSettings settings,
            out Single height, out Single width, out Single depth)
        {
            switch (type)
            {
                case FixtureComponentType.Backboard:
                    {
                        width = settings.FixtureWidth;
                        height = settings.FixtureHeight;
                        depth = settings.BackboardDepth;
                    }
                    break;

                case FixtureComponentType.Base:
                    {
                        width = settings.FixtureWidth;
                        height = settings.BaseHeight;
                        depth = settings.FixtureDepth - settings.BackboardDepth;
                    }
                    break;

                case FixtureComponentType.Shelf:
                    {
                        width = settings.ShelfWidth;
                        height = settings.ShelfHeight;
                        depth = settings.ShelfDepth;
                    }
                    break;

                case FixtureComponentType.Chest:
                    {
                        width = settings.ChestWidth;
                        height = settings.ChestHeight;
                        depth = settings.ChestDepth;
                    }
                    break;

                case FixtureComponentType.Bar:
                    {
                        width = settings.BarWidth;
                        height = settings.BarHeight;
                        depth = settings.BarDepth;
                    }
                    break;

                case FixtureComponentType.Rod:
                    {
                        width = settings.RodWidth;
                        height = settings.RodHeight;
                        depth = settings.RodDepth;
                    }
                    break;

                case FixtureComponentType.ClipStrip:
                    {
                        width = settings.ClipStripWidth;
                        height = settings.ClipStripHeight;
                        depth = settings.ClipStripDepth;
                    }
                    break;

                case FixtureComponentType.Peg:
                    {
                        width = settings.PegboardWidth;
                        height = settings.PegboardHeight;
                        depth = settings.PegboardDepth;
                    }
                    break;

                case FixtureComponentType.Pallet:
                    {
                        width = settings.PalletWidth;
                        height = settings.PalletHeight;
                        depth = settings.PalletDepth;
                    }
                    break;

                case FixtureComponentType.Panel:
                    {
                        width = 10;
                        height = 10;
                        depth = 10;

                    }
                    break;


                case FixtureComponentType.Custom:
                    height = 10;
                    width = 10;
                    depth = 10;
                    break;

                case FixtureComponentType.SlotWall:
                    {
                        width = settings.SlotwallWidth;
                        height = settings.SlotwallHeight;
                        depth = settings.SlotwallDepth;
                    }
                    break;
                default:
                    Debug.Fail("TODO");
                    height = 0;
                    width = 0;
                    depth = 0;
                    break;

            }
        }

        /// <summary>
        /// Returns the set of colour picker colours.
        /// </summary>
        public static Color[] ColourPickerColours
        {
            get
            {
                return new Color[]
                {
                    Color.FromArgb(255, 255, 255, 255),
                    Color.FromArgb(255, 0, 0, 0),
                    Color.FromArgb(255, 24, 73, 123),
                    Color.FromArgb(255, 84, 130, 189),
                    Color.FromArgb(255, 198, 81, 74),
                    Color.FromArgb(255, 156, 186, 90),
                    Color.FromArgb(255, 132, 101, 165),
                    Color.FromArgb(255, 74, 174, 198),
                    Color.FromArgb(255, 247, 150, 66)
                };
            }
        }

        /// <summary>
        ///     Sorts a list of positions to be moved, so that they are readded in the correct order.
        /// </summary>
        /// <param name="positionsToMove"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        public static IEnumerable<PlanogramPositionView> SortPositionsToMoveByDirection(this IEnumerable<PlanogramPositionView> positionsToMove, PlanogramPositionAnchorDirection direction)
        {
            //  Order positions to move depending on the strategy and the direction of insertion.
            if (direction == PlanogramPositionAnchorDirection.ToLeft)
            {
                return positionsToMove
                    .OrderBy(view => view.Component.Fixture.BaySequenceNumber)
                    .ThenBy(view =>
                    {
                        var subPlacement = view.GetParentSubComponentPlacement();
                        if (subPlacement.FixtureComponent != null) return subPlacement.FixtureComponent.ComponentSequenceNumber;
                        else return subPlacement.AssemblyComponent.ComponentSequenceNumber;
                    })
                    .ThenBy(view => view.SequenceX);
            }


            if (direction == PlanogramPositionAnchorDirection.ToRight)
            {
                return positionsToMove
                    .OrderByDescending(view => view.Component.Fixture.BaySequenceNumber)
                    .ThenByDescending(view =>
                    {
                        var subPlacement = view.GetParentSubComponentPlacement();
                        if (subPlacement.FixtureComponent != null) return subPlacement.FixtureComponent.ComponentSequenceNumber;
                        else return subPlacement.AssemblyComponent.ComponentSequenceNumber;
                    })
                    .ThenByDescending(view => view.SequenceX);
            }


            return positionsToMove;
        }



        /// <summary>
        /// Clears all nodes that are decendents of the given node
        /// </summary>
        /// <param name="node">The parent node to clear beneath</param>
        public static void ClearDescendantVisuals(Diagram diagram, FrameworkElement node)
        {
            if (diagram != null && node != null)
            {
                List<FrameworkElement> childList =
                    diagram.Links.Where(l => l.StartItem == node)
                    .Select(l => l.EndItem).ToList();

                foreach (FrameworkElement child in childList)
                {
                    // clear all the children recursively
                    ClearDescendantVisuals(diagram, child);
                    //now it is safe to remove 
                    diagram.Items.Remove(child);
                }

            }
        }



        public static Dictionary<PlanogramProductFillPatternType, Brush> ProductFillPatternBrushes
        {
            get
            {
                return new Dictionary<PlanogramProductFillPatternType, Brush>()
                {
                    {PlanogramProductFillPatternType.Solid, 
                        WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Solid, 5)},

                     {PlanogramProductFillPatternType.Border,
                        WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Border, 5)},

                     {PlanogramProductFillPatternType.Crosshatch,
                        WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Crosshatch, 5)},

                      {PlanogramProductFillPatternType.DiagonalDown,
                        WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.DiagonalDown, 5)},

                        {PlanogramProductFillPatternType.DiagonalUp,
                        WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.DiagonalUp, 5)},

                        {PlanogramProductFillPatternType.Dotted,
                        WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Dotted, 5)},

                        {PlanogramProductFillPatternType.Horizontal,
                        WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Horizontal, 5)},

                        {PlanogramProductFillPatternType.Vertical,
                        WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Vertical, 5)},
                };
            }
        }


        public static String GetTempImportFilesFolder()
        {
            //The default location of documents will be defined by the type of install chosen.
            String myDocsFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), Constants.MyDocsAppFolderName);
            String myPublicDocsFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments), Constants.MyDocsAppFolderName);
            String calculatedDefaultDocsFolder;

            if (Directory.Exists(myPublicDocsFolder))
            {
                //Sample files were installed in public... (All users)
                //Set default folder to public
                calculatedDefaultDocsFolder = myPublicDocsFolder;
            }
            else
            {
                if (Directory.Exists(myDocsFolder))
                {
                    //Sample files were installed in MyDocuments... (Just current user)
                    //Set default folder to documents
                    calculatedDefaultDocsFolder = myDocsFolder;
                }
                else
                {
                    //No Sample files were installed... (User elected not to install sample files).
                    //We choose documents as default location anyway
                    calculatedDefaultDocsFolder = myDocsFolder;
                }
            }
            return Path.Combine(calculatedDefaultDocsFolder, "ImportTempFiles");
        }

        
    }

    #endregion


}
