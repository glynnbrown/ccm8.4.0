﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: (CCM CCM800)
//// CCM-24979 : L.Hodson
////		Created
//#endregion
//#endregion

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Galleria.Framework.Planograms.Model;
//using Galleria.Ccm.Model;
//using System.IO;
//using Galleria.Ccm.Editor.Client.Wpf.PlanRender.Interfaces;
//using Galleria.Ccm.Editor.Client.Wpf.Framework.RenderControls;
//using Galleria.Ccm.Editor.Client.Wpf.PlanRender.ModelData;
//using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;
//using System.Windows.Controls;
//using System.Windows.Media.Media3D;
//using Galleria.Ccm.Editor.Client.Wpf.Resources.Converters;
//using System.Windows.Media;
//using System.Windows;
//using System.Windows.Media.Imaging;

//namespace Galleria.Ccm.Editor.Client.Wpf.Common
//{
//    /// <summary>
//    /// Provides helper methods for the FixturePackage model object 
//    /// and its children.
//    /// </summary>
//    public static class FixturePackageHelper
//    {
//        public const String FileExtension = ".fix";

//        #region Create Helpers

//        /// <summary>
//        /// Creates a new fixture package from the given plan items.
//        /// </summary>
//        /// <param name="planItems"></param>
//        /// <returns></returns>
//        public static FixturePackage CreateFixturePackage(List<IPlanItem> planItems)
//        {
//            FixturePackage fixturePackage = null;

//            switch (planItems[0].PlanItemType)
//            {
//                case PlanItemType.Component:
//                    fixturePackage = FixturePackageHelper.CreateFixturePackage(planItems[0].Component.ComponentModel);
//                    fixturePackage.ThumbnailImageData = CreateFixtureThumbnail(planItems[0].Component);
//                    break;

//                case PlanItemType.Assembly:
//                    fixturePackage = FixturePackageHelper.CreateFixturePackage(planItems[0].Assembly.AssemblyModel);
//                    fixturePackage.ThumbnailImageData = CreateFixtureThumbnail(planItems[0].Assembly);
//                    break;

//                case PlanItemType.Fixture:
//                    if (planItems.Count > 1 && planItems.All(i => i.PlanItemType == PlanItemType.Fixture))
//                    {
//                        fixturePackage = FixturePackageHelper.CreateFixturePackage(planItems.Select(i => i.Fixture.FixtureItemModel));
//                        fixturePackage.ThumbnailImageData = CreateFixtureThumbnail(planItems[0].Fixture);
//                    }
//                    else
//                    {
//                        fixturePackage = FixturePackageHelper.CreateFixturePackage(planItems[0].Fixture.FixtureModel);
//                    }
//                    break;
//            }

//            return fixturePackage;
//        }

//        /// <summary>
//        /// Creates a new fixture package from the given planogram component
//        /// </summary>
//        /// <param name="planComponent"></param>
//        /// <returns></returns>
//        public static FixturePackage CreateFixturePackage(PlanogramComponent planComponent)
//        {
//            FixturePackage package = FixturePackage.NewFixturePackage();
//            package.Height = planComponent.Height;
//            package.Width = planComponent.Width;
//            package.Depth = planComponent.Depth;

//            Dictionary<Object, FixtureImage> planImageIdDict = new Dictionary<Object, FixtureImage>();

//            FixtureComponent fixtureComponent = FixtureComponent.NewFixtureComponent();
//            CopyValues(fixtureComponent, planComponent, planImageIdDict);
//            package.Components.Add(fixtureComponent);

//            //create subcomponents
//            foreach (PlanogramSubComponent planSub in planComponent.SubComponents)
//            {
//                FixtureSubComponent fixtureSub = FixtureSubComponent.NewFixtureSubComponent();
//                CopyValues(fixtureSub, planSub, planImageIdDict);

//                fixtureComponent.SubComponents.Add(fixtureSub);
//            }



//            //add images
//            package.Images.AddRange(planImageIdDict.Values);

//            return package;
//        }

//        /// <summary>
//        /// Creates a new fixture package from the given planogram assembly
//        /// </summary>
//        /// <param name="planAssembly"></param>
//        /// <returns></returns>
//        public static FixturePackage CreateFixturePackage(PlanogramAssembly planAssembly)
//        {
//            FixturePackage package = FixturePackage.NewFixturePackage();
//            package.Height = planAssembly.Height;
//            package.Width = planAssembly.Width;
//            package.Depth = planAssembly.Depth;

//            Dictionary<Object, FixtureComponent> planCompIdDict = new Dictionary<Object, FixtureComponent>();
//            Dictionary<Object, FixtureImage> planImageIdDict = new Dictionary<Object, FixtureImage>();

//            //add a new assembly.
//            FixtureAssembly fixtureAssembly = FixtureAssembly.NewFixtureAssembly();
//            CopyValues(fixtureAssembly, planAssembly);
//            package.Assemblies.Add(fixtureAssembly);

//            foreach (PlanogramAssemblyComponent planAssemblyComponent in planAssembly.Components)
//            {
//                FixtureAssemblyComponent fixtureAssemblyComponent = FixtureAssemblyComponent.NewFixtureAssemblyComponent();
//                CopyValues(fixtureAssemblyComponent, planAssemblyComponent);

//                //create and link the fixture component
//                FixtureComponent fixtureComponent;
//                if (!planCompIdDict.TryGetValue(planAssemblyComponent.PlanogramComponentId, out fixtureComponent))
//                {
//                    PlanogramComponent planComponent = planAssembly.Parent.Components.FindById(planAssemblyComponent.PlanogramComponentId);

//                    fixtureComponent = FixtureComponent.NewFixtureComponent();
//                    CopyValues(fixtureComponent, planComponent, planImageIdDict);

//                    //create subcomponents
//                    foreach (PlanogramSubComponent planSub in planComponent.SubComponents)
//                    {
//                        FixtureSubComponent fixtureSub = FixtureSubComponent.NewFixtureSubComponent();
//                        CopyValues(fixtureSub, planSub, planImageIdDict);

//                        fixtureComponent.SubComponents.Add(fixtureSub);
//                    }

//                    planCompIdDict.Add(planAssemblyComponent.PlanogramComponentId, fixtureComponent);
//                    package.Components.Add(fixtureComponent);
//                }

//                fixtureAssemblyComponent.FixtureComponentId = fixtureComponent.Id;
//                fixtureAssembly.Components.Add(fixtureAssemblyComponent);
//            }

//            //add images
//            package.Images.AddRange(planImageIdDict.Values);

//            return package;
//        }

//        /// <summary>
//        /// Creates a new fixture package from the given planogram fixture
//        /// </summary>
//        /// <param name="planFixture"></param>
//        /// <returns></returns>
//        public static FixturePackage CreateFixturePackage(PlanogramFixture planFixture)
//        {
//            FixturePackage package = FixturePackage.NewFixturePackage();
//            package.Height = planFixture.Height;
//            package.Width = planFixture.Width;
//            package.Depth = planFixture.Depth;
            

//            Dictionary<Object, FixtureAssembly> planAssemblyIdDict = new Dictionary<Object, FixtureAssembly>();
//            Dictionary<Object, FixtureComponent> planCompIdDict = new Dictionary<Object, FixtureComponent>();
//            Dictionary<Object, FixtureImage> planImageIdDict = new Dictionary<Object, FixtureImage>();

//            //Add a new fixture
//            Fixture fixture = Fixture.NewFixture();
//            CopyValues(fixture, planFixture);
//            package.Fixtures.Add(fixture);

//            foreach (PlanogramFixtureAssembly planFixtureAssembly in planFixture.Assemblies)
//            {
//                PlanogramAssembly planAssembly = planFixture.Parent.Assemblies.FindById(planFixtureAssembly.PlanogramAssemblyId);

//                FixtureAssemblyItem assemblyItem = FixtureAssemblyItem.NewFixtureAssemblyItem();
//                CopyValues(assemblyItem, planFixtureAssembly);
//                fixture.Assemblies.Add(assemblyItem);

//                FixtureAssembly fixtureAssembly;
//                if(!planAssemblyIdDict.TryGetValue(planAssembly.Id, out fixtureAssembly))
//                {
//                    fixtureAssembly = FixtureAssembly.NewFixtureAssembly();
//                    CopyValues(fixtureAssembly, planAssembly);
                
//                    package.Assemblies.Add(fixtureAssembly);
//                    planAssemblyIdDict.Add(planAssembly.Id, fixtureAssembly);


//                    foreach (PlanogramAssemblyComponent planAssemblyComponent in planAssembly.Components)
//                    {
//                        FixtureAssemblyComponent fixtureAssemblyComponent = FixtureAssemblyComponent.NewFixtureAssemblyComponent();
//                        CopyValues(fixtureAssemblyComponent, planAssemblyComponent);

//                        //create and link the fixture component
//                        FixtureComponent fixtureComponent;
//                        if (!planCompIdDict.TryGetValue(planAssemblyComponent.PlanogramComponentId, out fixtureComponent))
//                        {
//                            PlanogramComponent planComponent = planAssembly.Parent.Components.FindById(planAssemblyComponent.PlanogramComponentId);

//                            fixtureComponent = FixtureComponent.NewFixtureComponent();
//                            CopyValues(fixtureComponent, planComponent, planImageIdDict);

//                            //create subcomponents
//                            foreach (PlanogramSubComponent planSub in planComponent.SubComponents)
//                            {
//                                FixtureSubComponent fixtureSub = FixtureSubComponent.NewFixtureSubComponent();
//                                CopyValues(fixtureSub, planSub, planImageIdDict);

//                                fixtureComponent.SubComponents.Add(fixtureSub);
//                            }

//                            planCompIdDict.Add(planAssemblyComponent.PlanogramComponentId, fixtureComponent);
//                            package.Components.Add(fixtureComponent);
//                        }

//                        fixtureAssemblyComponent.FixtureComponentId = fixtureComponent.Id;
//                        fixtureAssembly.Components.Add(fixtureAssemblyComponent);
//                    }

//                }

//                assemblyItem.FixtureAssemblyId = fixtureAssembly.Id;

//            }

//            foreach(PlanogramFixtureComponent planFixtureComponent in planFixture.Components)
//            {
//                FixtureComponentItem fixtureComponentItem = FixtureComponentItem.NewFixtureComponentItem();
//                CopyValues(fixtureComponentItem, planFixtureComponent);

//                //create and link the fixture component
//                FixtureComponent fixtureComponent;
//                if (!planCompIdDict.TryGetValue(planFixtureComponent.PlanogramComponentId, out fixtureComponent))
//                {
//                    PlanogramComponent planComponent = planFixture.Parent.Components.FindById(planFixtureComponent.PlanogramComponentId);

//                    fixtureComponent = FixtureComponent.NewFixtureComponent();
//                    CopyValues(fixtureComponent, planComponent, planImageIdDict);

//                    //create subcomponents
//                    foreach (PlanogramSubComponent planSub in planComponent.SubComponents)
//                    {
//                        FixtureSubComponent fixtureSub = FixtureSubComponent.NewFixtureSubComponent();
//                        CopyValues(fixtureSub, planSub, planImageIdDict);

//                        fixtureComponent.SubComponents.Add(fixtureSub);
//                    }

//                    planCompIdDict.Add(planFixtureComponent.PlanogramComponentId, fixtureComponent);
//                    package.Components.Add(fixtureComponent);
//                }

//                fixtureComponentItem.FixtureComponentId = fixtureComponent.Id;
//                fixture.Components.Add(fixtureComponentItem);
//            }

//            return package;
//        }

//        /// <summary>
//        /// Creates a new fixture package from the given fixture items.
//        /// </summary>
//        /// <param name="planFixtureItems"></param>
//        /// <returns></returns>
//        public static FixturePackage CreateFixturePackage(IEnumerable<PlanogramFixtureItem> planFixtureItems)
//        {
//            throw new NotImplementedException();
//        }

//        /// <summary>
//        /// Creates the data for the fixture thumbnail.
//        /// </summary>
//        /// <param name="item"></param>
//        /// <returns></returns>
//        private static Byte[] CreateFixtureThumbnail(IPlanItem item)
//        {
//            const Double imgLength = 150D;

//            Plan3DDataSettings settings = new Plan3DDataSettings()
//            {
//                ShowShelfRisers = true,
//                ShowChestsTopDown = false,
//                ShowChestWalls = true,
//                ShowPositions = false,
//                ShowPegHoles = true,
//                ShowNotches = true,
//                ShowFixtureImages = true
//            };


//            ModelConstruct3D model = null;
//            switch (item.PlanItemType)
//            {
//                case PlanItemType.Component:
//                    model = new ModelConstruct3D(new PlanComponent3DData(item.Component, settings));
//                    break;
//            }

//            //tilt the model slightly for the image.
//            model.ModelData.Rotation = new ObjectRotation3D(0, RadiansToDegreesConverter.ToRadians(10), 0);



//            Grid render = new Grid();
//            render.Height = imgLength;
//            render.Width = imgLength;

//            PerspectiveCamera cam = new PerspectiveCamera();
//            cam.UpDirection = new Vector3D(0, 1, 0);
//            cam.LookDirection = new Vector3D(0, 0, -244);
//            cam.FieldOfView = 60;

//            HelixToolkit.Wpf.HelixViewport3D viewport = new HelixToolkit.Wpf.HelixViewport3D()
//            {
//                Background = Brushes.White,
//                HorizontalAlignment = HorizontalAlignment.Stretch,
//                VerticalAlignment = VerticalAlignment.Stretch,
//                ModelUpDirection = new Vector3D(0, 1, 0),
//                ShowCoordinateSystem = false,
//                ShowViewCube = false,
//                Camera = cam
//            };
//            render.Children.Add(viewport);
//            viewport.Children.Add(new HelixToolkit.Wpf.SunLight());
//            viewport.Children.Add(model);

//            render.Arrange(new Rect(new Size(imgLength, imgLength)));
//            viewport.ZoomExtents();

//            viewport.CameraController.Zoom(0.2);

//            // Scale dimensions from 96 dpi to get better quality
//            Double scale = 200 / 96;

//            //perform a straight render.
//            RenderTargetBitmap renderTargetBitmap =
//                new RenderTargetBitmap(
//                (Int32)(scale * imgLength),
//                (Int32)(scale * imgLength),
//                scale * 96,
//                scale * 96,
//                PixelFormats.Default);
//            renderTargetBitmap.Render(render);



//            // encode
//            using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
//            {
//                PngBitmapEncoder encoder = new PngBitmapEncoder();
//                encoder.Frames.Add(BitmapFrame.Create(renderTargetBitmap));
//                encoder.Save(memoryStream);

//                // Export the stream to a byte array
//                Byte[] renderBytes = LocalHelper.CreateImageBlob(memoryStream, true);

//                return renderBytes;
//            }
//        }


//        #endregion

//        #region Fetch Helpers

//        /// <summary>
//        /// Returns the type of fixture library we are connected to.
//        /// </summary>
//        /// <returns></returns>
//        public static FixturePackageType GetConnectedFixturePackageType()
//        {
//            //TODO: Work out how to determine if we are connected to a db.
//            return FixturePackageType.FileSystem;
//        }

//        /// <summary>
//        /// Returns the path to the currently connected fixture directory.
//        /// </summary>
//        /// <returns></returns>
//        public static String GetFixtureDirectoryPath()
//        {
//            String path = App.ViewState.Settings.Model.FixtureLibraryLocation;

//            if (String.IsNullOrEmpty(path))
//            {
//                path =
//                     Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
//                     "Customer Centric Merchandising", "Fixtures");
//            }

//            if (!Directory.Exists(path))
//            {
//                Directory.CreateDirectory(path);
//            }

//            return path;
//        }

//        #endregion

//        #region Add To Planogram Helpers

//        /// <summary>
//        /// Adds the given package to the given planogram.
//        /// </summary>
//        /// <param name="package"></param>
//        /// <param name="nearestFixture"></param>
//        /// <returns></returns>
//        public static List<IPlanItem> AddToPlanogram(FixturePackage package, PlanogramView plan, PlanogramFixtureView nearestFixture)
//        {
//            List<IPlanItem> addedItems = new List<IPlanItem>();

//            PlanogramFixtureItem nearestFixtureModel = (nearestFixture != null) ? nearestFixture.FixtureItemModel : null;

//            if (package.FixtureItems.Count > 0)
//            {
//                List<PlanogramFixtureItem> fixtureItems = AddToPlanogram(package.FixtureItems, plan.Model, nearestFixtureModel);
//                foreach (PlanogramFixtureItem addedFixtureItem in fixtureItems)
//                {
//                    addedItems.Add(plan.Fixtures.FirstOrDefault(f => Object.Equals(f.FixtureItemModel.Id, addedFixtureItem.Id)));
//                }
//            }
//            else if (package.Fixtures.Count > 0)
//            {
//                PlanogramFixtureItem addedFixtureItem =
//                 AddToPlanogram(package.Fixtures[0], plan.Model, nearestFixtureModel);
//                if (addedFixtureItem != null)
//                {
//                    addedItems.Add(plan.Fixtures.FirstOrDefault(f => Object.Equals(f.FixtureItemModel.Id, addedFixtureItem.Id)));
//                }
//            }
//            else if (package.Assemblies.Count > 0)
//            {
//                PlanogramFixtureAssembly fixtureAssembly =
//                    AddToPlanogram(package.Assemblies[0], plan.Model, nearestFixtureModel);
//                if (fixtureAssembly != null)
//                {
//                    addedItems.Add(plan.EnumerateAllAssemblies().FirstOrDefault(f => Object.Equals(f.FixtureAssemblyModel.Id, fixtureAssembly.Id)));
//                }
//            }
//            else if (package.Components.Count > 0)
//            {
//                PlanogramFixtureComponent fixtureComponent =
//                    AddToPlanogram(package.Components[0], plan.Model, nearestFixtureModel);
//                if (fixtureComponent != null)
//                {
//                    addedItems.Add(plan.EnumerateAllComponents().FirstOrDefault(f => Object.Equals(f.FixtureComponentModel.Id, fixtureComponent.Id)));
//                }

//            }

//            return addedItems;
//        }

//        /// <summary>
//        /// Adds the given package to the given planogram.
//        /// </summary>
//        /// <param name="package"></param>
//        /// <param name="nearestFixture"></param>
//        /// <returns></returns>
//        public static Object AddToPlanogram(FixturePackage package, Planogram plan, PlanogramFixtureItem nearestFixture)
//        {
//            Object addedItem = null;

//            if (package.FixtureItems.Count > 0)
//            {
//                addedItem = AddToPlanogram(package.FixtureItems, plan, nearestFixture);
//            }
//            else if (package.Fixtures.Count > 1)
//            {

//                addedItem = AddToPlanogram(package.Fixtures[0], plan, nearestFixture);
//            }
//            else if (package.Assemblies.Count > 1)
//            {
//                addedItem = AddToPlanogram(package.Assemblies[0], plan, nearestFixture);
//            }
//            else if (package.Components.Count > 1)
//            {
//                addedItem = AddToPlanogram(package.Components[0], plan, nearestFixture);
//            }


//            return addedItem;
//        }

//        private static PlanogramFixtureComponent AddToPlanogram(FixtureComponent fixtureComponent, Planogram plan, PlanogramFixtureItem nearestFixtureItem)
//        {
//            FixturePackage package = fixtureComponent.Parent;

//            //copy all images
//            Dictionary<Object, PlanogramImage> imagesDict = new Dictionary<Object, PlanogramImage>();
//            foreach (FixtureImage fixtureImage in package.Images)
//            {
//                PlanogramImage planImage = PlanogramImage.NewPlanogramImage();
//                CopyValues(planImage, fixtureImage);
//                plan.Images.Add(planImage);

//                imagesDict.Add(fixtureImage.Id, planImage);
//            }

//            //get the fixture to add to.
//            PlanogramFixture addToFixture = (nearestFixtureItem != null) ? plan.Fixtures.FindById(nearestFixtureItem.PlanogramFixtureId) : null;
//            if (addToFixture == null)
//            {
//                if (plan.Fixtures.Count == 0)
//                {
//                    addToFixture = PlanogramFixture.NewPlanogramFixture();
//                    plan.Fixtures.Add(addToFixture);
//                }
//                else
//                {
//                    addToFixture = plan.Fixtures.Last();
//                }
//            }


//            //copy the component and subcomponents
//            PlanogramComponent planComponent = PlanogramComponent.NewPlanogramComponent();
//            CopyValues(planComponent, fixtureComponent, imagesDict);
//            plan.Components.Add(planComponent);

//            foreach (FixtureSubComponent fixtureSub in fixtureComponent.SubComponents)
//            {
//                PlanogramSubComponent planSub = PlanogramSubComponent.NewPlanogramSubComponent();
//                CopyValues(planSub, fixtureSub, imagesDict);
//                planComponent.SubComponents.Add(planSub);
//            }

//            //hook the new component to the fixture
//            PlanogramFixtureComponent planFixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent();
//            planFixtureComponent.PlanogramComponentId = planComponent.Id;
//            addToFixture.Components.Add(planFixtureComponent);

//            //place in the center of the fixture 
//            planFixtureComponent.X = (addToFixture.Width / 2) - (planComponent.Width / 2);
//            planFixtureComponent.Y = (addToFixture.Height / 2) - (planComponent.Height / 2);

//            return planFixtureComponent;
//        }

//        private static PlanogramFixtureAssembly AddToPlanogram(FixtureAssembly fixtureAssembly, Planogram plan, PlanogramFixtureItem nearestFixtureItem)
//        {
//            FixturePackage package = fixtureAssembly.Parent;

//            //copy all images
//            Dictionary<Object, PlanogramImage> imagesDict = new Dictionary<Object, PlanogramImage>();
//            foreach (FixtureImage fixtureImage in package.Images)
//            {
//                PlanogramImage planImage = PlanogramImage.NewPlanogramImage();
//                CopyValues(planImage, fixtureImage);
//                plan.Images.Add(planImage);

//                imagesDict.Add(fixtureImage.Id, planImage);
//            }

//            //get the fixture to add to.
//            PlanogramFixture addToFixture = (nearestFixtureItem != null) ? plan.Fixtures.FindById(nearestFixtureItem.PlanogramFixtureId) : null;
//            if (addToFixture == null)
//            {
//                if (plan.Fixtures.Count == 0)
//                {
//                    addToFixture = PlanogramFixture.NewPlanogramFixture();
//                    plan.Fixtures.Add(addToFixture);
//                }
//                else
//                {
//                    addToFixture = plan.Fixtures.Last();
//                }
//            }


//            //copy components
//            Dictionary<Object, PlanogramComponent> componentsDict = new Dictionary<Object, PlanogramComponent>();
//            foreach (FixtureComponent fixtureComponent in package.Components)
//            {
//                PlanogramComponent planComponent = PlanogramComponent.NewPlanogramComponent();
//                CopyValues(planComponent, fixtureComponent, imagesDict);
//                plan.Components.Add(planComponent);
//                componentsDict.Add(fixtureComponent.Id, planComponent);

//                foreach (FixtureSubComponent fixtureSub in fixtureComponent.SubComponents)
//                {
//                    PlanogramSubComponent planSub = PlanogramSubComponent.NewPlanogramSubComponent();
//                    CopyValues(planSub, fixtureSub, imagesDict);
//                    planComponent.SubComponents.Add(planSub);
//                }
//            }

//            //copy the assembly
//            PlanogramAssembly planAssembly = PlanogramAssembly.NewPlanogramAssembly();
//            CopyValues(planAssembly, fixtureAssembly);
//            plan.Assemblies.Add(planAssembly);

//            foreach (FixtureAssemblyComponent fixtureAssemblyComponent in fixtureAssembly.Components)
//            {
//                PlanogramAssemblyComponent planAc = PlanogramAssemblyComponent.NewPlanogramAssemblyComponent();
//                CopyValues(planAc, fixtureAssemblyComponent);

//                planAc.PlanogramComponentId = componentsDict[fixtureAssemblyComponent.FixtureComponentId].Id;

//                planAssembly.Components.Add(planAc);
//            }


//            //hook the assembly to the fixture
//            PlanogramFixtureAssembly planFixtureAssembly = PlanogramFixtureAssembly.NewPlanogramFixtureAssembly();
//            planFixtureAssembly.PlanogramAssemblyId = planAssembly.Id;
//            addToFixture.Assemblies.Add(planFixtureAssembly);

//            return planFixtureAssembly;
//        }

//        private static PlanogramFixtureItem AddToPlanogram(Fixture fixture, Planogram plan, PlanogramFixtureItem nearestFixtureItem)
//        {
//            FixturePackage package = fixture.Parent;

//            //get the fixture to add to.
//            PlanogramFixtureItem addToFixture = nearestFixtureItem;
//            if (addToFixture == null && plan.FixtureItems.Count > 0)
//            {
//                addToFixture = plan.FixtureItems.Last();
//            }


//            //copy all images
//            Dictionary<Object, PlanogramImage> imagesDict = new Dictionary<Object, PlanogramImage>();
//            foreach (FixtureImage fixtureImage in package.Images)
//            {
//                PlanogramImage planImage = PlanogramImage.NewPlanogramImage();
//                CopyValues(planImage, fixtureImage);
//                plan.Images.Add(planImage);

//                imagesDict.Add(fixtureImage.Id, planImage);
//            }


//            //copy components
//            Dictionary<Object, PlanogramComponent> componentsDict = new Dictionary<Object, PlanogramComponent>();
//            foreach (FixtureComponent fixtureComponent in package.Components)
//            {
//                PlanogramComponent planComponent = PlanogramComponent.NewPlanogramComponent();
//                CopyValues(planComponent, fixtureComponent, imagesDict);
//                plan.Components.Add(planComponent);
//                componentsDict.Add(fixtureComponent.Id, planComponent);

//                foreach (FixtureSubComponent fixtureSub in fixtureComponent.SubComponents)
//                {
//                    PlanogramSubComponent planSub = PlanogramSubComponent.NewPlanogramSubComponent();
//                    CopyValues(planSub, fixtureSub, imagesDict);
//                    planComponent.SubComponents.Add(planSub);
//                }
//            }

//            //copy assemblies
//            Dictionary<Object, PlanogramAssembly> assembliesDict = new Dictionary<Object, PlanogramAssembly>();
//            foreach (FixtureAssembly fixtureAssembly in package.Assemblies)
//            {
//                PlanogramAssembly planAssembly = PlanogramAssembly.NewPlanogramAssembly();
//                CopyValues(planAssembly, fixtureAssembly);
//                plan.Assemblies.Add(planAssembly);
//                assembliesDict.Add(fixtureAssembly.Id, planAssembly);

//                foreach (FixtureAssemblyComponent fixtureAssemblyComponent in fixtureAssembly.Components)
//                {
//                    PlanogramAssemblyComponent planAc = PlanogramAssemblyComponent.NewPlanogramAssemblyComponent();
//                    CopyValues(planAc, fixtureAssemblyComponent);

//                    planAc.PlanogramComponentId = componentsDict[fixtureAssemblyComponent.FixtureComponentId].Id;

//                    planAssembly.Components.Add(planAc);
//                }
//            }

//            //copy fixture
//            PlanogramFixture planFixture = PlanogramFixture.NewPlanogramFixture();
//            CopyValues(planFixture, fixture);
//            plan.Fixtures.Add(planFixture);

//            foreach (FixtureAssemblyItem fixtureAI in fixture.Assemblies)
//            {
//                PlanogramFixtureAssembly planFA = PlanogramFixtureAssembly.NewPlanogramFixtureAssembly();
//                CopyValues(planFA, fixtureAI);

//                planFA.PlanogramAssemblyId = assembliesDict[fixtureAI.FixtureAssemblyId].Id;

//                planFixture.Assemblies.Add(planFA);
//            }

//            foreach (FixtureComponentItem fixtureCI in fixture.Components)
//            {
//                PlanogramFixtureComponent planFC = PlanogramFixtureComponent.NewPlanogramFixtureComponent();
//                CopyValues(planFC, fixtureCI);

//                planFC.PlanogramComponentId = componentsDict[fixtureCI.FixtureComponentId].Id;

//                planFixture.Components.Add(planFC);
//            }

//            //add fixture to plan
//            PlanogramFixtureItem fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem();
//            fixtureItem.PlanogramFixtureId = planFixture.Id;

//            if (addToFixture != null)
//            {
//                fixtureItem.X = addToFixture.X +
//                    plan.Fixtures.FindById(addToFixture.PlanogramFixtureId).Width;
//            }

//            plan.FixtureItems.Add(fixtureItem);


//            return fixtureItem;
//        }

//        private static List<PlanogramFixtureItem> AddToPlanogram(IEnumerable<FixtureItem> fixtureItems, Planogram plan, PlanogramFixtureItem nearestFixtureItem)
//        {
//            List<PlanogramFixtureItem> addedItems = new List<PlanogramFixtureItem>();

//            if (fixtureItems.Count() > 0)
//            {

//                FixturePackage package = fixtureItems.First().Parent;

//                //get the fixture to add to.
//                PlanogramFixtureItem addToFixture = nearestFixtureItem;
//                if (addToFixture == null && plan.FixtureItems.Count > 0)
//                {
//                    addToFixture = plan.FixtureItems.Last();
//                }


//                //copy all images
//                Dictionary<Object, PlanogramImage> imagesDict = new Dictionary<Object, PlanogramImage>();
//                foreach (FixtureImage fixtureImage in package.Images)
//                {
//                    PlanogramImage planImage = PlanogramImage.NewPlanogramImage();
//                    CopyValues(planImage, fixtureImage);
//                    plan.Images.Add(planImage);

//                    imagesDict.Add(fixtureImage.Id, planImage);
//                }



//                //copy components
//                Dictionary<Object, PlanogramComponent> componentsDict = new Dictionary<Object, PlanogramComponent>();
//                foreach (FixtureComponent fixtureComponent in package.Components)
//                {
//                    PlanogramComponent planComponent = PlanogramComponent.NewPlanogramComponent();
//                    CopyValues(planComponent, fixtureComponent, imagesDict);
//                    plan.Components.Add(planComponent);
//                    componentsDict.Add(fixtureComponent.Id, planComponent);

//                    foreach (FixtureSubComponent fixtureSub in fixtureComponent.SubComponents)
//                    {
//                        PlanogramSubComponent planSub = PlanogramSubComponent.NewPlanogramSubComponent();
//                        CopyValues(planSub, fixtureSub, imagesDict);
//                        planComponent.SubComponents.Add(planSub);
//                    }
//                }

//                //copy assemblies
//                Dictionary<Object, PlanogramAssembly> assembliesDict = new Dictionary<Object, PlanogramAssembly>();
//                foreach (FixtureAssembly fixtureAssembly in package.Assemblies)
//                {
//                    PlanogramAssembly planAssembly = PlanogramAssembly.NewPlanogramAssembly();
//                    CopyValues(planAssembly, fixtureAssembly);
//                    plan.Assemblies.Add(planAssembly);
//                    assembliesDict.Add(fixtureAssembly.Id, planAssembly);

//                    foreach (FixtureAssemblyComponent fixtureAssemblyComponent in fixtureAssembly.Components)
//                    {
//                        PlanogramAssemblyComponent planAc = PlanogramAssemblyComponent.NewPlanogramAssemblyComponent();
//                        CopyValues(planAc, fixtureAssemblyComponent);

//                        planAc.PlanogramComponentId = componentsDict[fixtureAssemblyComponent.FixtureComponentId].Id;

//                        planAssembly.Components.Add(planAc);
//                    }
//                }

//                //copy fixtures
//                Dictionary<Object, PlanogramFixture> fixturesDict = new Dictionary<Object, PlanogramFixture>();
//                foreach (Fixture fixture in package.Fixtures)
//                {
//                    PlanogramFixture planFixture = PlanogramFixture.NewPlanogramFixture();
//                    CopyValues(planFixture, fixture);
//                    plan.Fixtures.Add(planFixture);
//                    fixturesDict.Add(fixture.Id, planFixture);

//                    foreach (FixtureAssemblyItem fixtureAI in fixture.Assemblies)
//                    {
//                        PlanogramFixtureAssembly planFA = PlanogramFixtureAssembly.NewPlanogramFixtureAssembly();
//                        CopyValues(planFA, fixtureAI);

//                        planFA.PlanogramAssemblyId = assembliesDict[fixtureAI.FixtureAssemblyId].Id;

//                        planFixture.Assemblies.Add(planFA);
//                    }

//                    foreach (FixtureComponentItem fixtureCI in fixture.Components)
//                    {
//                        PlanogramFixtureComponent planFC = PlanogramFixtureComponent.NewPlanogramFixtureComponent();
//                        CopyValues(planFC, fixtureCI);

//                        planFC.PlanogramComponentId = componentsDict[fixtureCI.FixtureComponentId].Id;

//                        planFixture.Components.Add(planFC);
//                    }
//                }

//                //copy fixture items
//                Single nextX = 0;
//                if (addToFixture != null)
//                {
//                    nextX = addToFixture.X + plan.Fixtures.FindById(addToFixture.PlanogramFixtureId).Width;
//                }

//                foreach (FixtureItem fixtureItem in fixtureItems)
//                {
//                    PlanogramFixtureItem planFixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem();
//                    CopyValues(planFixtureItem, fixtureItem);

//                    PlanogramFixture planFixture = fixturesDict[fixtureItem.FixtureId];
//                    planFixtureItem.PlanogramFixtureId = planFixture.Id;

//                    planFixtureItem.X = nextX;
//                    nextX += planFixture.Width;

//                    plan.FixtureItems.Add(planFixtureItem);
//                    addedItems.Add(planFixtureItem);
//                }

//            }

//            return addedItems;
//        }

//        #endregion

//        #region CopyValues Methods

//        public static void CopyValues(PlanogramFixtureItem target, FixtureItem source)
//        {
//            target.X = source.X;
//            target.Y = source.Y;
//            target.Z = source.Z;
//            target.Slope = source.Slope;
//            target.Angle = source.Angle;
//            target.Roll = source.Roll;
//            target.IsBay = source.IsBay;
//        }

//        public static void CopyValues(PlanogramFixture target, Fixture source)
//        {
//            target.Name = source.Name;
//            target.Height = source.Height;
//            target.Width = source.Width;
//            target.Depth = source.Depth;
//            //target.NumberOfAssemblies = source.NumberOfAssemblies;
//            //target.NumberOfMerchandisedSubComponents = source.NumberOfMerchandisedSubComponents;
//            target.TotalFixtureCost = source.TotalFixtureCost;
//        }

//        public static void CopyValues(Fixture target, PlanogramFixture source)
//        {
//            target.Name = source.Name;
//            target.Height = source.Height;
//            target.Width = source.Width;
//            target.Depth = source.Depth;
//            //target.NumberOfAssemblies = source.NumberOfAssemblies;
//            //target.NumberOfMerchandisedSubComponents = source.NumberOfMerchandisedSubComponents;
//            target.TotalFixtureCost = source.TotalFixtureCost;
//        }

//        private static void CopyValues(PlanogramImage target, FixtureImage source)
//        {
//            target.FileName = source.FileName;
//            target.Description = source.Description;
//            target.ImageData = source.ImageData;
//        }

//        private static void CopyValues(FixtureImage target, PlanogramImage source)
//        {
//            target.FileName = source.FileName;
//            target.Description = source.Description;
//            target.ImageData = source.ImageData;
//        }

//        public static void CopyValues(PlanogramFixtureAssembly target, FixtureAssemblyItem source)
//        {
//            target.X = source.X;
//            target.Y = source.Y;
//            target.Z = source.Z;
//            target.Slope = source.Slope;
//            target.Angle = source.Angle;
//            target.Roll = source.Roll;
//        }

//        public static void CopyValues(FixtureAssemblyItem target, PlanogramFixtureAssembly source)
//        {
//            target.X = source.X;
//            target.Y = source.Y;
//            target.Z = source.Z;
//            target.Slope = source.Slope;
//            target.Angle = source.Angle;
//            target.Roll = source.Roll;
//        }

//        public static void CopyValues(PlanogramFixtureComponent target, FixtureComponentItem source)
//        {
//            target.X = source.X;
//            target.Y = source.Y;
//            target.Z = source.Z;
//            target.Slope = source.Slope;
//            target.Angle = source.Angle;
//            target.Roll = source.Roll;
//        }

//        public static void CopyValues(FixtureComponentItem target, PlanogramFixtureComponent source)
//        {
//            target.X = source.X;
//            target.Y = source.Y;
//            target.Z = source.Z;
//            target.Slope = source.Slope;
//            target.Angle = source.Angle;
//            target.Roll = source.Roll;
//        }

//        public static void CopyValues(FixtureAssemblyComponent target, PlanogramAssemblyComponent source)
//        {
//            target.X = source.X;
//            target.Y = source.Y;
//            target.Z = source.Z;
//            target.Slope = source.Slope;
//            target.Angle = source.Angle;
//            target.Roll = source.Roll;
//        }

//        public static void CopyValues(PlanogramAssemblyComponent target, FixtureAssemblyComponent source)
//        {
//            target.X = source.X;
//            target.Y = source.Y;
//            target.Z = source.Z;
//            target.Slope = source.Slope;
//            target.Angle = source.Angle;
//            target.Roll = source.Roll;
//        }

//        public static void CopyValues(FixtureAssembly target, PlanogramAssembly source)
//        {
//            target.Name = source.Name;
//            target.Height = source.Height;
//            target.Width = source.Width;
//            target.Depth = source.Depth;
//            target.NumberOfComponents = source.NumberOfComponents;
//            target.TotalComponentCost = source.TotalComponentCost;
//        }

//        public static void CopyValues(PlanogramAssembly target, FixtureAssembly source)
//        {
//            target.Name = source.Name;
//            target.Height = source.Height;
//            target.Width = source.Width;
//            target.Depth = source.Depth;
//            //target.NumberOfComponents = source.NumberOfComponents;
//            target.TotalComponentCost = source.TotalComponentCost;
//        }

//        public static void CopyValues(PlanogramComponent target, FixtureComponent source, Dictionary<Object, PlanogramImage> fixtureImageIdDict)
//        {
//            target.Name = source.Name;
//            target.Height = source.Height;
//            target.Width = source.Width;
//            target.Depth = source.Depth;
//            //target.NumberOfSubComponents = source.NumberOfSubComponents;
//            //target.NumberOfMerchandisedSubComponents = source.NumberOfMerchandisedSubComponents;
//            target.NumberOfShelfEdgeLabels = source.NumberOfShelfEdgeLabels;
//            target.IsMoveable = source.IsMoveable;
//            target.IsDisplayOnly = source.IsDisplayOnly;
//            target.CanAttachShelfEdgeLabel = source.CanAttachShelfEdgeLabel;
//            target.RetailerReferenceCode = source.RetailerReferenceCode;
//            target.BarCode = source.BarCode;
//            target.Manufacturer = source.Manufacturer;
//            target.ManufacturerPartName = source.ManufacturerPartName;
//            target.ManufacturerPartNumber = source.ManufacturerPartNumber;
//            target.SupplierName = source.SupplierName;
//            target.SupplierPartNumber = source.SupplierPartNumber;
//            target.SupplierCostPrice = source.SupplierCostPrice;
//            target.SupplierDiscount = source.SupplierDiscount;
//            target.SupplierLeadTime = source.SupplierLeadTime;
//            target.MinPurchaseQty = source.MinPurchaseQty;
//            target.WeightLimit = source.WeightLimit;
//            target.Weight = source.Weight;
//            target.Volume = source.Volume;
//            target.Diameter = source.Diameter;
//            target.Capacity = source.Capacity;
//            target.ComponentType = (PlanogramComponentType)source.ComponentType;


//            PlanogramImage fixtureImage = null;

//            //front image
//            if (source.ImageIdFront != null)
//            {
//                if (!fixtureImageIdDict.TryGetValue(source.ImageIdFront, out fixtureImage))
//                {
//                    FixtureImage planImage = source.Parent.Images.FindById(source.ImageIdFront);
//                    if (planImage != null)
//                    {
//                        fixtureImage = PlanogramImage.NewPlanogramImage();
//                        CopyValues(fixtureImage, planImage);

//                        fixtureImageIdDict.Add(source.ImageIdFront, fixtureImage);
//                    }
//                }
//                if (fixtureImage != null)
//                {
//                    target.ImageIdFront = fixtureImage.Id;
//                }
//            }

//            //back image
//            if (source.ImageIdBack != null)
//            {
//                if (!fixtureImageIdDict.TryGetValue(source.ImageIdBack, out fixtureImage))
//                {
//                    FixtureImage planImage = source.Parent.Images.FindById(source.ImageIdBack);
//                    if (planImage != null)
//                    {
//                        fixtureImage = PlanogramImage.NewPlanogramImage();
//                        CopyValues(fixtureImage, planImage);

//                        fixtureImageIdDict.Add(source.ImageIdBack, fixtureImage);
//                    }
//                }
//                if (fixtureImage != null)
//                {
//                    target.ImageIdBack = fixtureImage.Id;
//                }
//            }

//            //top
//            if (source.ImageIdTop != null)
//            {
//                if (!fixtureImageIdDict.TryGetValue(source.ImageIdTop, out fixtureImage))
//                {
//                    FixtureImage planImage = source.Parent.Images.FindById(source.ImageIdTop);
//                    if (planImage != null)
//                    {
//                        fixtureImage = PlanogramImage.NewPlanogramImage();
//                        CopyValues(fixtureImage, planImage);

//                        fixtureImageIdDict.Add(source.ImageIdTop, fixtureImage);
//                    }
//                }
//                if (fixtureImage != null)
//                {
//                    target.ImageIdTop = fixtureImage.Id;
//                }
//            }

//            //bottom
//            if (source.ImageIdBottom != null)
//            {
//                if (!fixtureImageIdDict.TryGetValue(source.ImageIdBottom, out fixtureImage))
//                {
//                    FixtureImage planImage = source.Parent.Images.FindById(source.ImageIdBottom);
//                    if (planImage != null)
//                    {
//                        fixtureImage = PlanogramImage.NewPlanogramImage();
//                        CopyValues(fixtureImage, planImage);

//                        fixtureImageIdDict.Add(source.ImageIdBottom, fixtureImage);
//                    }
//                }
//                if (fixtureImage != null)
//                {
//                    target.ImageIdBottom = fixtureImage.Id;
//                }
//            }

//            //left
//            if (source.ImageIdLeft != null)
//            {
//                if (!fixtureImageIdDict.TryGetValue(source.ImageIdLeft, out fixtureImage))
//                {
//                    FixtureImage planImage = source.Parent.Images.FindById(source.ImageIdLeft);
//                    if (planImage != null)
//                    {
//                        fixtureImage = PlanogramImage.NewPlanogramImage();
//                        CopyValues(fixtureImage, planImage);

//                        fixtureImageIdDict.Add(source.ImageIdLeft, fixtureImage);
//                    }
//                }
//                if (fixtureImage != null)
//                {
//                    target.ImageIdLeft = fixtureImage.Id;
//                }
//            }

//            //right
//            if (source.ImageIdRight != null)
//            {
//                if (!fixtureImageIdDict.TryGetValue(source.ImageIdRight, out fixtureImage))
//                {
//                    FixtureImage planImage = source.Parent.Images.FindById(source.ImageIdRight);
//                    if (planImage != null)
//                    {
//                        fixtureImage = PlanogramImage.NewPlanogramImage();
//                        CopyValues(fixtureImage, planImage);

//                        fixtureImageIdDict.Add(source.ImageIdRight, fixtureImage);
//                    }
//                }
//                if (fixtureImage != null)
//                {
//                    target.ImageIdRight = fixtureImage.Id;
//                }
//            }
//        }

//        public static void CopyValues(FixtureComponent target, PlanogramComponent source, Dictionary<Object, FixtureImage> planImageIdDict)
//        {
//            target.Name = source.Name;
//            target.Height = source.Height;
//            target.Width = source.Width;
//            target.Depth = source.Depth;
//            target.NumberOfSubComponents = source.NumberOfSubComponents;
//            target.NumberOfMerchandisedSubComponents = source.NumberOfMerchandisedSubComponents;
//            target.NumberOfShelfEdgeLabels = source.NumberOfShelfEdgeLabels;
//            target.IsMoveable = source.IsMoveable;
//            target.IsDisplayOnly = source.IsDisplayOnly;
//            target.CanAttachShelfEdgeLabel = source.CanAttachShelfEdgeLabel;
//            target.RetailerReferenceCode = source.RetailerReferenceCode;
//            target.BarCode = source.BarCode;
//            target.Manufacturer = source.Manufacturer;
//            target.ManufacturerPartName = source.ManufacturerPartName;
//            target.ManufacturerPartNumber = source.ManufacturerPartNumber;
//            target.SupplierName = source.SupplierName;
//            target.SupplierPartNumber = source.SupplierPartNumber;
//            target.SupplierCostPrice = source.SupplierCostPrice;
//            target.SupplierDiscount = source.SupplierDiscount;
//            target.SupplierLeadTime = source.SupplierLeadTime;
//            target.MinPurchaseQty = source.MinPurchaseQty;
//            target.WeightLimit = source.WeightLimit;
//            target.Weight = source.Weight;
//            target.Volume = source.Volume;
//            target.Diameter = source.Diameter;
//            target.Capacity = source.Capacity;
//            target.ComponentType = (FixtureComponentType)Enum.Parse(typeof(FixtureComponentType), source.ComponentType.ToString());


//            FixtureImage fixtureImage;

//            //front image
//            if (source.ImageIdFront != null)
//            {
//                if (!planImageIdDict.TryGetValue(source.ImageIdFront, out fixtureImage))
//                {
//                    PlanogramImage planImage = source.Parent.Images.FindById(source.ImageIdFront);
//                    if (planImage != null)
//                    {
//                        fixtureImage = FixtureImage.NewFixtureImage();
//                        CopyValues(fixtureImage, planImage);

//                        planImageIdDict.Add(source.ImageIdFront, fixtureImage);
//                    }
//                }
//                if (fixtureImage != null)
//                {
//                    target.ImageIdFront = fixtureImage.Id;
//                }
//            }

//            //back image
//            if (source.ImageIdBack != null)
//            {
//                if (!planImageIdDict.TryGetValue(source.ImageIdBack, out fixtureImage))
//                {
//                    PlanogramImage planImage = source.Parent.Images.FindById(source.ImageIdBack);
//                    if (planImage != null)
//                    {
//                        fixtureImage = FixtureImage.NewFixtureImage();
//                        CopyValues(fixtureImage, planImage);

//                        planImageIdDict.Add(source.ImageIdBack, fixtureImage);
//                    }
//                }
//                if (fixtureImage != null)
//                {
//                    target.ImageIdBack = fixtureImage.Id;
//                }
//            }

//            //top
//            if (source.ImageIdTop != null)
//            {
//                if (!planImageIdDict.TryGetValue(source.ImageIdTop, out fixtureImage))
//                {
//                    PlanogramImage planImage = source.Parent.Images.FindById(source.ImageIdTop);
//                    if (planImage != null)
//                    {
//                        fixtureImage = FixtureImage.NewFixtureImage();
//                        CopyValues(fixtureImage, planImage);

//                        planImageIdDict.Add(source.ImageIdTop, fixtureImage);
//                    }
//                }
//                if (fixtureImage != null)
//                {
//                    target.ImageIdTop = fixtureImage.Id;
//                }
//            }

//            //bottom
//            if (source.ImageIdBottom != null)
//            {
//                if (!planImageIdDict.TryGetValue(source.ImageIdBottom, out fixtureImage))
//                {
//                    PlanogramImage planImage = source.Parent.Images.FindById(source.ImageIdBottom);
//                    if (planImage != null)
//                    {
//                        fixtureImage = FixtureImage.NewFixtureImage();
//                        CopyValues(fixtureImage, planImage);

//                        planImageIdDict.Add(source.ImageIdBottom, fixtureImage);
//                    }
//                }
//                if (fixtureImage != null)
//                {
//                    target.ImageIdBottom = fixtureImage.Id;
//                }
//            }

//            //left
//            if (source.ImageIdLeft != null)
//            {
//                if (!planImageIdDict.TryGetValue(source.ImageIdLeft, out fixtureImage))
//                {
//                    PlanogramImage planImage = source.Parent.Images.FindById(source.ImageIdLeft);
//                    if (planImage != null)
//                    {
//                        fixtureImage = FixtureImage.NewFixtureImage();
//                        CopyValues(fixtureImage, planImage);

//                        planImageIdDict.Add(source.ImageIdLeft, fixtureImage);
//                    }
//                }
//                if (fixtureImage != null)
//                {
//                    target.ImageIdLeft = fixtureImage.Id;
//                }
//            }

//            //right
//            if (source.ImageIdRight != null)
//            {
//                if (!planImageIdDict.TryGetValue(source.ImageIdRight, out fixtureImage))
//                {
//                    PlanogramImage planImage = source.Parent.Images.FindById(source.ImageIdRight);
//                    if (planImage != null)
//                    {
//                        fixtureImage = FixtureImage.NewFixtureImage();
//                        CopyValues(fixtureImage, planImage);

//                        planImageIdDict.Add(source.ImageIdRight, fixtureImage);
//                    }
//                }
//                if (fixtureImage != null)
//                {
//                    target.ImageIdRight = fixtureImage.Id;
//                }
//            }
//        }

//        public static void CopyValues(FixtureSubComponent target, PlanogramSubComponent source, Dictionary<Object, FixtureImage> planImageIdDict)
//        {
//            target.Angle = source.Angle;
//            target.Depth = source.Depth;
//            target.DividerObstructionHeight = source.DividerObstructionHeight;
//            target.DividerObstructionSpacingX = source.DividerObstructionSpacingX;
//            target.DividerObstructionSpacingY = source.DividerObstructionSpacingY;
//            target.DividerObstructionStartX = source.DividerObstructionStartX;
//            target.DividerObstructionStartY = source.DividerObstructionStartY;
//            target.DividerObstructionWidth = source.DividerObstructionWidth;
//            target.FaceThickness = source.FaceThickness;
//            target.FaceThicknessTop = source.FaceThicknessTop;
//            target.FaceTopOffset = source.FaceTopOffset;
//            target.FillColourBack = source.FillColourBack;
//            target.FillColourBottom = source.FillColourBottom;
//            target.FillColourFront = source.FillColourFront;
//            target.FillColourLeft = source.FillColourLeft;
//            target.FillColourRight = source.FillColourRight;
//            target.FillColourTop = source.FillColourTop;
//            target.FillPatternTypeBack = (FixtureSubComponentFillPatternType)source.FillPatternTypeBack;
//            target.FillPatternTypeBottom = (FixtureSubComponentFillPatternType)source.FillPatternTypeBottom;
//            target.FillPatternTypeFront = (FixtureSubComponentFillPatternType)source.FillPatternTypeFront;
//            target.FillPatternTypeLeft = (FixtureSubComponentFillPatternType)source.FillPatternTypeLeft;
//            target.FillPatternTypeRight = (FixtureSubComponentFillPatternType)source.FillPatternTypeRight;
//            target.FillPatternTypeTop = (FixtureSubComponentFillPatternType)source.FillPatternTypeTop;
//            target.HasCollisionDetection = source.HasCollisionDetection;
//            target.Height = source.Height;
//            target.IsNotchPlacedOnBack = source.IsNotchPlacedOnBack;
//            target.IsNotchPlacedOnFront = source.IsNotchPlacedOnFront;
//            target.IsNotchPlacedOnLeft = source.IsNotchPlacedOnLeft;
//            target.IsNotchPlacedOnRight = source.IsNotchPlacedOnRight;
//            target.IsRiserPlacedOnBack = source.IsRiserPlacedOnBack;
//            target.IsRiserPlacedOnFront = source.IsRiserPlacedOnFront;
//            target.IsRiserPlacedOnLeft = source.IsRiserPlacedOnLeft;
//            target.IsRiserPlacedOnRight = source.IsRiserPlacedOnRight;
//            target.IsVisible = source.IsVisible;
//            target.LineColour = source.LineColour;
//            target.LineThickness = source.LineThickness;
//            target.MerchandisableHeight = source.MerchandisableHeight;
//            target.MerchConstraintRow1SpacingX = source.MerchConstraintRow1SpacingX;
//            target.MerchConstraintRow1SpacingY = source.MerchConstraintRow1SpacingY;
//            target.MerchConstraintRow1StartX = source.MerchConstraintRow1StartX;
//            target.MerchConstraintRow1StartY = source.MerchConstraintRow1StartY;
//            target.MerchConstraintRow2SpacingX = source.MerchConstraintRow2SpacingX;
//            target.MerchConstraintRow2SpacingY = source.MerchConstraintRow2SpacingY;
//            target.MerchConstraintRow2StartX = source.MerchConstraintRow2StartX;
//            target.MerchConstraintRow2StartY = source.MerchConstraintRow2StartY;
//            target.Name = source.Name;
//            target.NotchHeight = source.NotchHeight;
//            target.NotchSpacingX = source.NotchSpacingX;
//            target.NotchSpacingY = source.NotchSpacingY;
//            target.NotchStartX = source.NotchStartX;
//            target.NotchStartY = source.NotchStartY;
//            target.NotchStyleType = (FixtureSubComponentNotchStyleType)source.NotchStyleType;
//            target.NotchWidth = source.NotchWidth;
//            target.RiserColour = source.RiserColour;
//            target.RiserFillPatternType = (FixtureSubComponentFillPatternType)source.RiserFillPatternType;
//            target.RiserHeight = source.RiserHeight;
//            target.RiserThickness = source.RiserThickness;
//            target.RiserTransparencyPercent = source.RiserTransparencyPercent;
//            target.Roll = source.Roll;
//            target.ShapeType = (FixtureSubComponentShapeType)source.ShapeType;
//            target.Slope = source.Slope;
//            target.TransparencyPercentBack = source.TransparencyPercentBack;
//            target.TransparencyPercentBottom = source.TransparencyPercentBottom;
//            target.TransparencyPercentFront = source.TransparencyPercentFront;
//            target.TransparencyPercentLeft = source.TransparencyPercentLeft;
//            target.TransparencyPercentRight = source.TransparencyPercentRight;
//            target.TransparencyPercentTop = source.TransparencyPercentTop;
//            target.Width = source.Width;
//            target.X = source.X;
//            target.Y = source.Y;
//            target.Z = source.Z;
//            target.LineThickness = source.LineThickness;
//            target.MerchandisingType = (FixtureSubComponentMerchandisingType)source.MerchandisingType;
//            target.CombineType = (FixtureSubComponentCombineType)source.CombineType;
//            target.MerchandisingStrategyX = (FixtureSubComponentXMerchStrategyType)source.MerchandisingStrategyX;
//            target.MerchandisingStrategyY = (FixtureSubComponentYMerchStrategyType)source.MerchandisingStrategyY;
//            target.MerchandisingStrategyZ = (FixtureSubComponentZMerchStrategyType)source.MerchandisingStrategyZ;
//            target.LeftOverhang = source.LeftOverhang;
//            target.RightOverhang = source.RightOverhang;
//            target.FrontOverhang = source.FrontOverhang;
//            target.BackOverhang = source.BackOverhang;
//            target.TopOverhang = source.TopOverhang;
//            target.BottomOverhang = source.BottomOverhang;

//            if (target.Parent != null
//                && target.Parent.Parent != null)
//            {

//                FixtureImage fixtureImage;

//                //front image
//                if (source.ImageIdFront != null)
//                {
//                    if (!planImageIdDict.TryGetValue(source.ImageIdFront, out fixtureImage))
//                    {
//                        PlanogramImage planImage = source.Parent.Parent.Images.FindById(source.ImageIdFront);

//                        fixtureImage = FixtureImage.NewFixtureImage();
//                        CopyValues(fixtureImage, planImage);

//                        planImageIdDict.Add(source.ImageIdFront, fixtureImage);
//                    }
//                    target.ImageIdFront = fixtureImage.Id;
//                }

//                //back image
//                if (source.ImageIdBack != null)
//                {
//                    if (!planImageIdDict.TryGetValue(source.ImageIdBack, out fixtureImage))
//                    {
//                        PlanogramImage planImage = source.Parent.Parent.Images.FindById(source.ImageIdBack);

//                        fixtureImage = FixtureImage.NewFixtureImage();
//                        CopyValues(fixtureImage, planImage);

//                        planImageIdDict.Add(source.ImageIdBack, fixtureImage);
//                    }
//                    target.ImageIdBack = fixtureImage.Id;
//                }

//                //top
//                if (source.ImageIdTop != null)
//                {
//                    if (!planImageIdDict.TryGetValue(source.ImageIdTop, out fixtureImage))
//                    {
//                        PlanogramImage planImage = source.Parent.Parent.Images.FindById(source.ImageIdTop);

//                        fixtureImage = FixtureImage.NewFixtureImage();
//                        CopyValues(fixtureImage, planImage);

//                        planImageIdDict.Add(source.ImageIdTop, fixtureImage);
//                    }
//                    target.ImageIdTop = fixtureImage.Id;
//                }

//                //bottom
//                if (source.ImageIdBottom != null)
//                {
//                    if (!planImageIdDict.TryGetValue(source.ImageIdBottom, out fixtureImage))
//                    {
//                        PlanogramImage planImage = source.Parent.Parent.Images.FindById(source.ImageIdBottom);

//                        fixtureImage = FixtureImage.NewFixtureImage();
//                        CopyValues(fixtureImage, planImage);

//                        planImageIdDict.Add(source.ImageIdBottom, fixtureImage);
//                    }
//                    target.ImageIdBottom = fixtureImage.Id;
//                }

//                //left
//                if (source.ImageIdLeft != null)
//                {
//                    if (!planImageIdDict.TryGetValue(source.ImageIdLeft, out fixtureImage))
//                    {
//                        PlanogramImage planImage = source.Parent.Parent.Images.FindById(source.ImageIdLeft);

//                        fixtureImage = FixtureImage.NewFixtureImage();
//                        CopyValues(fixtureImage, planImage);

//                        planImageIdDict.Add(source.ImageIdLeft, fixtureImage);
//                    }
//                    target.ImageIdLeft = fixtureImage.Id;
//                }

//                //right
//                if (source.ImageIdRight != null)
//                {
//                    if (!planImageIdDict.TryGetValue(source.ImageIdRight, out fixtureImage))
//                    {
//                        PlanogramImage planImage = source.Parent.Parent.Images.FindById(source.ImageIdRight);

//                        fixtureImage = FixtureImage.NewFixtureImage();
//                        CopyValues(fixtureImage, planImage);

//                        planImageIdDict.Add(source.ImageIdRight, fixtureImage);
//                    }
//                    target.ImageIdRight = fixtureImage.Id;
//                }
//            }
//        }

//        public static void CopyValues(PlanogramSubComponent target, FixtureSubComponent source, Dictionary<Object, PlanogramImage> fixtureImageIdDict)
//        {
//            target.Angle = source.Angle;
//            target.Depth = source.Depth;
//            target.DividerObstructionHeight = source.DividerObstructionHeight;
//            target.DividerObstructionSpacingX = source.DividerObstructionSpacingX;
//            target.DividerObstructionSpacingY = source.DividerObstructionSpacingY;
//            target.DividerObstructionStartX = source.DividerObstructionStartX;
//            target.DividerObstructionStartY = source.DividerObstructionStartY;
//            target.DividerObstructionWidth = source.DividerObstructionWidth;
//            target.FaceThickness = source.FaceThickness;
//            target.FaceThicknessTop = source.FaceThicknessTop;
//            target.FaceTopOffset = source.FaceTopOffset;
//            target.FillColourBack = source.FillColourBack;
//            target.FillColourBottom = source.FillColourBottom;
//            target.FillColourFront = source.FillColourFront;
//            target.FillColourLeft = source.FillColourLeft;
//            target.FillColourRight = source.FillColourRight;
//            target.FillColourTop = source.FillColourTop;
//            target.FillPatternTypeBack = (PlanogramSubComponentFillPatternType)source.FillPatternTypeBack;
//            target.FillPatternTypeBottom = (PlanogramSubComponentFillPatternType)source.FillPatternTypeBottom;
//            target.FillPatternTypeFront = (PlanogramSubComponentFillPatternType)source.FillPatternTypeFront;
//            target.FillPatternTypeLeft = (PlanogramSubComponentFillPatternType)source.FillPatternTypeLeft;
//            target.FillPatternTypeRight = (PlanogramSubComponentFillPatternType)source.FillPatternTypeRight;
//            target.FillPatternTypeTop = (PlanogramSubComponentFillPatternType)source.FillPatternTypeTop;
//            target.HasCollisionDetection = source.HasCollisionDetection;
//            target.Height = source.Height;
//            target.IsNotchPlacedOnBack = source.IsNotchPlacedOnBack;
//            target.IsNotchPlacedOnFront = source.IsNotchPlacedOnFront;
//            target.IsNotchPlacedOnLeft = source.IsNotchPlacedOnLeft;
//            target.IsNotchPlacedOnRight = source.IsNotchPlacedOnRight;
//            target.IsRiserPlacedOnBack = source.IsRiserPlacedOnBack;
//            target.IsRiserPlacedOnFront = source.IsRiserPlacedOnFront;
//            target.IsRiserPlacedOnLeft = source.IsRiserPlacedOnLeft;
//            target.IsRiserPlacedOnRight = source.IsRiserPlacedOnRight;
//            target.IsVisible = source.IsVisible;
//            target.LineColour = source.LineColour;
//            target.LineThickness = source.LineThickness;
//            target.MerchandisableHeight = source.MerchandisableHeight;
//            target.MerchConstraintRow1SpacingX = source.MerchConstraintRow1SpacingX;
//            target.MerchConstraintRow1SpacingY = source.MerchConstraintRow1SpacingY;
//            target.MerchConstraintRow1StartX = source.MerchConstraintRow1StartX;
//            target.MerchConstraintRow1StartY = source.MerchConstraintRow1StartY;
//            target.MerchConstraintRow2SpacingX = source.MerchConstraintRow2SpacingX;
//            target.MerchConstraintRow2SpacingY = source.MerchConstraintRow2SpacingY;
//            target.MerchConstraintRow2StartX = source.MerchConstraintRow2StartX;
//            target.MerchConstraintRow2StartY = source.MerchConstraintRow2StartY;
//            target.Name = source.Name;
//            target.NotchHeight = source.NotchHeight;
//            target.NotchSpacingX = source.NotchSpacingX;
//            target.NotchSpacingY = source.NotchSpacingY;
//            target.NotchStartX = source.NotchStartX;
//            target.NotchStartY = source.NotchStartY;
//            target.NotchStyleType = (PlanogramSubComponentNotchStyleType)source.NotchStyleType;
//            target.NotchWidth = source.NotchWidth;
//            target.RiserColour = source.RiserColour;
//            target.RiserFillPatternType = (PlanogramSubComponentFillPatternType)source.RiserFillPatternType;
//            target.RiserHeight = source.RiserHeight;
//            target.RiserThickness = source.RiserThickness;
//            target.RiserTransparencyPercent = source.RiserTransparencyPercent;
//            target.Roll = source.Roll;
//            target.ShapeType = (PlanogramSubComponentShapeType)source.ShapeType;
//            target.Slope = source.Slope;
//            target.TransparencyPercentBack = source.TransparencyPercentBack;
//            target.TransparencyPercentBottom = source.TransparencyPercentBottom;
//            target.TransparencyPercentFront = source.TransparencyPercentFront;
//            target.TransparencyPercentLeft = source.TransparencyPercentLeft;
//            target.TransparencyPercentRight = source.TransparencyPercentRight;
//            target.TransparencyPercentTop = source.TransparencyPercentTop;
//            target.Width = source.Width;
//            target.X = source.X;
//            target.Y = source.Y;
//            target.Z = source.Z;
//            target.LineThickness = source.LineThickness;
//            target.MerchandisingType = (PlanogramSubComponentMerchandisingType)source.MerchandisingType;
//            target.CombineType = (PlanogramSubComponentCombineType)source.CombineType;
//            target.MerchandisingStrategyX = (PlanogramSubComponentXMerchStrategyType)source.MerchandisingStrategyX;
//            target.MerchandisingStrategyY = (PlanogramSubComponentYMerchStrategyType)source.MerchandisingStrategyY;
//            target.MerchandisingStrategyZ = (PlanogramSubComponentZMerchStrategyType)source.MerchandisingStrategyZ;
//            target.LeftOverhang = source.LeftOverhang;
//            target.RightOverhang = source.RightOverhang;
//            target.FrontOverhang = source.FrontOverhang;
//            target.BackOverhang = source.BackOverhang;
//            target.TopOverhang = source.TopOverhang;
//            target.BottomOverhang = source.BottomOverhang;

//            if (target.Parent != null
//                && target.Parent.Parent != null)
//            {

//                PlanogramImage fixtureImage;

//                //front image
//                if (source.ImageIdFront != null)
//                {
//                    if (!fixtureImageIdDict.TryGetValue(source.ImageIdFront, out fixtureImage))
//                    {
//                        FixtureImage planImage = source.Parent.Parent.Images.FindById(source.ImageIdFront);

//                        fixtureImage = PlanogramImage.NewPlanogramImage();
//                        CopyValues(fixtureImage, planImage);

//                        fixtureImageIdDict.Add(source.ImageIdFront, fixtureImage);
//                    }
//                    target.ImageIdFront = fixtureImage.Id;
//                }

//                //back image
//                if (source.ImageIdBack != null)
//                {
//                    if (!fixtureImageIdDict.TryGetValue(source.ImageIdBack, out fixtureImage))
//                    {
//                        FixtureImage planImage = source.Parent.Parent.Images.FindById(source.ImageIdBack);

//                        fixtureImage = PlanogramImage.NewPlanogramImage();
//                        CopyValues(fixtureImage, planImage);

//                        fixtureImageIdDict.Add(source.ImageIdBack, fixtureImage);
//                    }
//                    target.ImageIdBack = fixtureImage.Id;
//                }

//                //top
//                if (source.ImageIdTop != null)
//                {
//                    if (!fixtureImageIdDict.TryGetValue(source.ImageIdTop, out fixtureImage))
//                    {
//                        FixtureImage planImage = source.Parent.Parent.Images.FindById(source.ImageIdTop);

//                        fixtureImage = PlanogramImage.NewPlanogramImage();
//                        CopyValues(fixtureImage, planImage);

//                        fixtureImageIdDict.Add(source.ImageIdTop, fixtureImage);
//                    }
//                    target.ImageIdTop = fixtureImage.Id;
//                }

//                //bottom
//                if (source.ImageIdBottom != null)
//                {
//                    if (!fixtureImageIdDict.TryGetValue(source.ImageIdBottom, out fixtureImage))
//                    {
//                        FixtureImage planImage = source.Parent.Parent.Images.FindById(source.ImageIdBottom);

//                        fixtureImage = PlanogramImage.NewPlanogramImage();
//                        CopyValues(fixtureImage, planImage);

//                        fixtureImageIdDict.Add(source.ImageIdBottom, fixtureImage);
//                    }
//                    target.ImageIdBottom = fixtureImage.Id;
//                }

//                //left
//                if (source.ImageIdLeft != null)
//                {
//                    if (!fixtureImageIdDict.TryGetValue(source.ImageIdLeft, out fixtureImage))
//                    {
//                        FixtureImage planImage = source.Parent.Parent.Images.FindById(source.ImageIdLeft);

//                        fixtureImage = PlanogramImage.NewPlanogramImage();
//                        CopyValues(fixtureImage, planImage);

//                        fixtureImageIdDict.Add(source.ImageIdLeft, fixtureImage);
//                    }
//                    target.ImageIdLeft = fixtureImage.Id;
//                }

//                //right
//                if (source.ImageIdRight != null)
//                {
//                    if (!fixtureImageIdDict.TryGetValue(source.ImageIdRight, out fixtureImage))
//                    {
//                        FixtureImage planImage = source.Parent.Parent.Images.FindById(source.ImageIdRight);

//                        fixtureImage = PlanogramImage.NewPlanogramImage();
//                        CopyValues(fixtureImage, planImage);

//                        fixtureImageIdDict.Add(source.ImageIdRight, fixtureImage);
//                    }
//                    target.ImageIdRight = fixtureImage.Id;
//                }
//            }
//        }

//        public static void CopyValues(FixtureSubComponent target, PlanogramSubComponent source)
//        {
//            target.Angle = source.Angle;
//            target.Depth = source.Depth;
//            target.DividerObstructionHeight = source.DividerObstructionHeight;
//            target.DividerObstructionSpacingX = source.DividerObstructionSpacingX;
//            target.DividerObstructionSpacingY = source.DividerObstructionSpacingY;
//            target.DividerObstructionStartX = source.DividerObstructionStartX;
//            target.DividerObstructionStartY = source.DividerObstructionStartY;
//            target.DividerObstructionWidth = source.DividerObstructionWidth;
//            target.FaceThickness = source.FaceThickness;
//            target.FaceThicknessTop = source.FaceThicknessTop;
//            target.FaceTopOffset = source.FaceTopOffset;
//            target.FillColourBack = source.FillColourBack;
//            target.FillColourBottom = source.FillColourBottom;
//            target.FillColourFront = source.FillColourFront;
//            target.FillColourLeft = source.FillColourLeft;
//            target.FillColourRight = source.FillColourRight;
//            target.FillColourTop = source.FillColourTop;
//            target.FillPatternTypeBack = (FixtureSubComponentFillPatternType)source.FillPatternTypeBack;
//            target.FillPatternTypeBottom = (FixtureSubComponentFillPatternType)source.FillPatternTypeBottom;
//            target.FillPatternTypeFront = (FixtureSubComponentFillPatternType)source.FillPatternTypeFront;
//            target.FillPatternTypeLeft = (FixtureSubComponentFillPatternType)source.FillPatternTypeLeft;
//            target.FillPatternTypeRight = (FixtureSubComponentFillPatternType)source.FillPatternTypeRight;
//            target.FillPatternTypeTop = (FixtureSubComponentFillPatternType)source.FillPatternTypeTop;
//            target.HasCollisionDetection = source.HasCollisionDetection;
//            target.Height = source.Height;
//            target.IsNotchPlacedOnBack = source.IsNotchPlacedOnBack;
//            target.IsNotchPlacedOnFront = source.IsNotchPlacedOnFront;
//            target.IsNotchPlacedOnLeft = source.IsNotchPlacedOnLeft;
//            target.IsNotchPlacedOnRight = source.IsNotchPlacedOnRight;
//            target.IsRiserPlacedOnBack = source.IsRiserPlacedOnBack;
//            target.IsRiserPlacedOnFront = source.IsRiserPlacedOnFront;
//            target.IsRiserPlacedOnLeft = source.IsRiserPlacedOnLeft;
//            target.IsRiserPlacedOnRight = source.IsRiserPlacedOnRight;
//            target.IsVisible = source.IsVisible;
//            target.LineColour = source.LineColour;
//            target.LineThickness = source.LineThickness;
//            target.MerchandisableHeight = source.MerchandisableHeight;
//            target.MerchConstraintRow1SpacingX = source.MerchConstraintRow1SpacingX;
//            target.MerchConstraintRow1SpacingY = source.MerchConstraintRow1SpacingY;
//            target.MerchConstraintRow1StartX = source.MerchConstraintRow1StartX;
//            target.MerchConstraintRow1StartY = source.MerchConstraintRow1StartY;
//            target.MerchConstraintRow2SpacingX = source.MerchConstraintRow2SpacingX;
//            target.MerchConstraintRow2SpacingY = source.MerchConstraintRow2SpacingY;
//            target.MerchConstraintRow2StartX = source.MerchConstraintRow2StartX;
//            target.MerchConstraintRow2StartY = source.MerchConstraintRow2StartY;
//            target.Name = source.Name;
//            target.NotchHeight = source.NotchHeight;
//            target.NotchSpacingX = source.NotchSpacingX;
//            target.NotchSpacingY = source.NotchSpacingY;
//            target.NotchStartX = source.NotchStartX;
//            target.NotchStartY = source.NotchStartY;
//            target.NotchStyleType = (FixtureSubComponentNotchStyleType)source.NotchStyleType;
//            target.NotchWidth = source.NotchWidth;
//            target.RiserColour = source.RiserColour;
//            target.RiserFillPatternType = (FixtureSubComponentFillPatternType)source.RiserFillPatternType;
//            target.RiserHeight = source.RiserHeight;
//            target.RiserThickness = source.RiserThickness;
//            target.RiserTransparencyPercent = source.RiserTransparencyPercent;
//            target.Roll = source.Roll;
//            target.ShapeType = (FixtureSubComponentShapeType)source.ShapeType;
//            target.Slope = source.Slope;
//            target.TransparencyPercentBack = source.TransparencyPercentBack;
//            target.TransparencyPercentBottom = source.TransparencyPercentBottom;
//            target.TransparencyPercentFront = source.TransparencyPercentFront;
//            target.TransparencyPercentLeft = source.TransparencyPercentLeft;
//            target.TransparencyPercentRight = source.TransparencyPercentRight;
//            target.TransparencyPercentTop = source.TransparencyPercentTop;
//            target.Width = source.Width;
//            target.X = source.X;
//            target.Y = source.Y;
//            target.Z = source.Z;
//            target.LineThickness = source.LineThickness;
//            target.MerchandisingType = (FixtureSubComponentMerchandisingType)source.MerchandisingType;
//            target.CombineType = (FixtureSubComponentCombineType)source.CombineType;
//            target.MerchandisingStrategyX = (FixtureSubComponentXMerchStrategyType)source.MerchandisingStrategyX;
//            target.MerchandisingStrategyY = (FixtureSubComponentYMerchStrategyType)source.MerchandisingStrategyY;
//            target.MerchandisingStrategyZ = (FixtureSubComponentZMerchStrategyType)source.MerchandisingStrategyZ;
//            target.LeftOverhang = source.LeftOverhang;
//            target.RightOverhang = source.RightOverhang;
//            target.FrontOverhang = source.FrontOverhang;
//            target.BackOverhang = source.BackOverhang;
//            target.TopOverhang = source.TopOverhang;
//            target.BottomOverhang = source.BottomOverhang;


//        }

//        #endregion

//        #region Delete Methods

       

//        #endregion
//    }
//}
