﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Controls;
using Xceed.Wpf.AvalonDock.Layout;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Base control for LayoutAnchorable content.
    /// </summary>
    public class DockingPanelControl : UserControl
    {
        #region Title Property

        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register("Title", typeof(String), typeof(DockingPanelControl),
            new PropertyMetadata(String.Empty));

        public String Title
        {
            get { return (String)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        #endregion

        #region InitialPlacement

        public static readonly DependencyProperty PlacementStrategyProperty =
            DependencyProperty.Register("InitialPlacement", typeof(AnchorableShowStrategy), typeof(DockingPanelControl),
            new PropertyMetadata(AnchorableShowStrategy.Left));

        /// <summary>
        /// Gets/Sets the placement of this panel 
        /// when initially loaded into the dock.
        /// </summary>
        public AnchorableShowStrategy PlacementStrategy
        {
            get { return (AnchorableShowStrategy)GetValue(PlacementStrategyProperty); }
            set { SetValue(PlacementStrategyProperty, value); }
        }

        #endregion

        #region GroupName

        public static readonly DependencyProperty GroupNameProperty =
            DependencyProperty.Register("GroupName", typeof(String), typeof(DockingPanelControl),
            new PropertyMetadata(null));

        public String GroupName
        {
            get { return (String)GetValue(GroupNameProperty); }
            set { SetValue(GroupNameProperty, value); }
        }

        #endregion

        #region DockedWidth

        public static readonly DependencyProperty DockedWidthProperty =
            DependencyProperty.Register("DockedWidth", typeof(Double), typeof(DockingPanelControl),
            new PropertyMetadata(200D));

        /// <summary>
        /// Gets/Sets the initial width this control should be docked at.
        /// </summary>
        public Double DockedWidth
        {
            get { return (Double)GetValue(DockedWidthProperty); }
            set { SetValue(DockedWidthProperty, value); }
        }

        #endregion
    }
}
