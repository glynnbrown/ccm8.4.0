﻿using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Planograms.ViewModel;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Contains helper methods for reprocessing planogram merch groups.
    /// </summary>
    internal static class MerchGroupProcessingHelper
    {
        /// <summary>
        /// Returns true if merchandising groups should be processed after
        /// a change the given property.
        /// </summary>
        internal static Boolean IsMerchandisingGroupProcessRequired(PlanogramPositionView positionView, String propertyName)
        {
            if (String.IsNullOrEmpty(propertyName)) return false;
            if (propertyName.StartsWith("Meta")) return false;
            if (propertyName.Contains("Image")) return false;


            //specific property exclusions:
            if (propertyName.Equals(PlanogramPosition.ExtendedDataProperty.Name)) return false;
            if (propertyName.Equals(PlanogramPosition.SequenceNumberProperty.Name)) return false;
            if (propertyName.Equals(PlanogramPosition.SequenceColourProperty.Name)) return false;
            if (propertyName.Equals(PlanogramPosition.PositionSequenceNumberProperty.Name)) return false;
            if (propertyName.Equals(PlanogramPosition.TotalUnitsProperty.Name)) return false;
            if (propertyName.Equals(PlanogramPosition.UnitsHighProperty.Name)) return false;
            if (propertyName.Equals(PlanogramPosition.UnitsWideProperty.Name)) return false;
            if (propertyName.Equals(PlanogramPosition.UnitsDeepProperty.Name)) return false;

            return true;
        }

        /// <summary>
        /// Returns true if merchandising groups should be processed after
        /// a change the given property.
        /// </summary>
        internal static Boolean IsMerchandisingGroupProcessRequired(PlanogramProductView productView, String propertyName)
        {
            if (String.IsNullOrEmpty(propertyName)) return false;
            if (propertyName.StartsWith("Meta")) return false;
            if (propertyName.EndsWith("Async")) return false;


            //specific exclusions:
            if (propertyName.Equals(PlanogramProduct.CustomAttributesProperty.Name)) return false;
            if (propertyName.StartsWith("PlanogramImageId")) return false;
            if (propertyName.Equals(PlanogramProduct.GtinProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.NameProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.BrandProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.IsActiveProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.ShapeTypeProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.FillPatternTypeProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.FillColourProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.PointOfPurchaseDescriptionProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.ShortDescriptionProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.SubcategoryProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.CustomerStatusProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.ColourProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.FlavourProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.PackagingShapeProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.PackagingTypeProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.CountryOfOriginProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.CountryOfProcessingProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.ShelfLifeProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.DeliveryFrequencyDaysProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.DeliveryMethodProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.VendorCodeProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.VendorProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.ManufacturerCodeProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.ManufacturerProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.SizeProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.UnitOfMeasureProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.DateIntroducedProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.DateDiscontinuedProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.DateEffectiveProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.HealthProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.CorporateCodeProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.BarcodeProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.SellPriceProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.SellPackCountProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.SellPackDescriptionProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.RecommendedRetailPriceProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.ManufacturerRecommendedRetailPriceProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.CostPriceProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.CaseCostProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.TaxRateProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.ConsumerInformationProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.TextureProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.StyleNumberProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.PatternProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.ModelProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.GarmentTypeProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.IsPrivateLabelProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.IsNewProductProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.ShapeProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.FinancialGroupCodeProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.FinancialGroupNameProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.StatusTypeProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.IsPlaceHolderProductProperty.Name)) return false;
            if (propertyName.Equals(PlanogramProduct.ColourGroupValueProperty.Name)) return false;


            return true;
        }

        /// <summary>
        /// Returns true if merchandising groups should be processed after
        /// a change the given property.
        /// </summary>
        internal static Boolean IsMerchandisingGroupProcessRequired(PlanogramSubComponentView subComponentView, String propertyName)
        {
            //if (!this.IsMerchandisable) return false;
            if (String.IsNullOrEmpty(propertyName)) return false;
            if (propertyName.StartsWith("Meta")) return false;

            //Specific exclusions:
            //These properties should never trigger merchandising groups to be reprocessed
            // as they do not affect the layout in any way.
            if (propertyName.StartsWith("ImageId")) return false;
            if (propertyName.Equals(PlanogramSubComponent.NameProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.ShapeTypeProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.IsRiserPlacedOnBackProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.IsRiserPlacedOnFrontProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.IsRiserPlacedOnLeftProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.IsRiserPlacedOnRightProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.RiserColourProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.RiserHeightProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.RiserThicknessProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.RiserTransparencyPercentProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.IsNotchPlacedOnBackProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.IsNotchPlacedOnFrontProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.IsNotchPlacedOnLeftProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.IsNotchPlacedOnRightProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.NotchHeightProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.NotchSpacingXProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.NotchSpacingYProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.NotchStartXProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.NotchStartYProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.NotchStyleTypeProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.NotchWidthProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.FillColourBackProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.FillColourBottomProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.FillColourFrontProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.FillColourLeftProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.FillColourRightProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.FillColourTopProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.FillPatternTypeBackProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.FillPatternTypeBottomProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.FillPatternTypeFrontProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.FillPatternTypeLeftProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.FillPatternTypeRightProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.FillPatternTypeTopProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.LineColourProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.LineThicknessProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.TransparencyPercentBackProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.TransparencyPercentBottomProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.TransparencyPercentFrontProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.TransparencyPercentLeftProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.TransparencyPercentRightProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.TransparencyPercentTopProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.DividerObstructionFillColourProperty.Name)) return false;
            if (propertyName.Equals(PlanogramSubComponent.DividerObstructionFillPatternProperty.Name)) return false;

            return true;
        }

        /// <summary>
        /// Returns true if merchandising groups should be processed after
        /// a change the given property.
        /// </summary>
        internal static Boolean IsMerchandisingGroupProcessRequired(PlanogramFixtureView fixtureView, String propertyName)
        {
            if (String.IsNullOrEmpty(propertyName)) return false;
            if (propertyName.StartsWith("Meta")) return false;
            if (propertyName.EndsWith("Async")) return false;


            if (propertyName == PlanogramComponent.NameProperty.Name) return false;

            return true;
        }


        /// <summary>
        /// Returns true if merchandising groups should be processed after
        /// a change the given property.
        /// </summary>
        internal static Boolean IsMerchandisingGroupProcessRequired(PlanogramComponentView componentView, String propertyName)
        {
            if (String.IsNullOrEmpty(propertyName)) return false;
            if (propertyName.StartsWith("Meta")) return false;
            if (propertyName.EndsWith("Async")) return false;

            if (propertyName == PlanogramComponent.CustomAttributesProperty.Name) return false;
            if (propertyName == PlanogramComponent.NameProperty.Name) return false;
            if (propertyName == PlanogramComponent.BarCodeProperty.Name) return false;
            if (propertyName == PlanogramComponent.CanAttachShelfEdgeLabelProperty.Name) return false;
            if (propertyName == PlanogramComponent.CapacityProperty.Name) return false;
            if (propertyName == PlanogramComponent.ComponentTypeProperty.Name) return false;
            if (propertyName == PlanogramComponent.DiameterProperty.Name) return false;
            if (propertyName == PlanogramComponent.IsDisplayOnlyProperty.Name) return false;
            if (propertyName == PlanogramComponent.IsMerchandisedTopDownProperty.Name) return false;
            if (propertyName == PlanogramComponent.IsMoveableProperty.Name) return false;
            if (propertyName == PlanogramComponent.ManufacturerPartNameProperty.Name) return false;
            if (propertyName == PlanogramComponent.ManufacturerPartNumberProperty.Name) return false;
            if (propertyName == PlanogramComponent.ManufacturerProperty.Name) return false;
            if (propertyName == PlanogramComponent.MinPurchaseQtyProperty.Name) return false;
            if (propertyName == PlanogramComponent.RetailerReferenceCodeProperty.Name) return false;
            if (propertyName == PlanogramComponent.SupplierCostPriceProperty.Name) return false;
            if (propertyName == PlanogramComponent.SupplierDiscountProperty.Name) return false;
            if (propertyName == PlanogramComponent.SupplierLeadTimeProperty.Name) return false;
            if (propertyName == PlanogramComponent.SupplierNameProperty.Name) return false;
            if (propertyName == PlanogramComponent.SupplierPartNumberProperty.Name) return false;
            if (propertyName == PlanogramComponent.VolumeProperty.Name) return false;
            if (propertyName == PlanogramComponent.WeightLimitProperty.Name) return false;
            if (propertyName == PlanogramComponent.WeightProperty.Name) return false;

            return true;
        }

    }
}
