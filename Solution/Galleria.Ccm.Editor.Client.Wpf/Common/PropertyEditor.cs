﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Galleria.Framework.Collections;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    //TODO: Consider moving into framework.

    /// <summary>
    /// Control used to edit properties on a given object
    /// </summary>
    public class PropertyEditor : Control
    {
        #region Fields

        private readonly BulkObservableCollection<PropertyItemDescription> _propertyDescriptions = new BulkObservableCollection<PropertyItemDescription>();

        private readonly List<PropertyEditorItem> _masterPropertyItems = new List<PropertyEditorItem>();
        private readonly ObservableCollection<PropertyEditorItem> _propertyItems = new ObservableCollection<PropertyEditorItem>();

        private readonly Collection<DataTemplate> _editorTemplates = new Collection<DataTemplate>();

        #endregion

        #region Properties

        #region SourceObject Property

        public static readonly DependencyProperty SourceObjectProperty =
            DependencyProperty.Register("SourceObject", typeof(Object), typeof(PropertyEditor),
            new PropertyMetadata(null, OnSourceObjectPropertyChanged));

        private static void OnSourceObjectPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PropertyEditor senderControl = (PropertyEditor)obj;
            senderControl.RefreshPropertyItems();
        }

        /// <summary>
        /// Gets/Sets the source object to which properties should bind.
        /// </summary>
        public Object SourceObject
        {
            get { return GetValue(SourceObjectProperty); }
            set { SetValue(SourceObjectProperty, value); }
        }

        #endregion

        #region PropertyDescriptions

        /// <summary>
        /// Returns the collection of property descriptions to display
        /// </summary>
        public BulkObservableCollection<PropertyItemDescription> PropertyDescriptions
        {
            get { return _propertyDescriptions; }
        }

        #endregion

        #region PropertyItems Property

        public static readonly DependencyProperty PropertyItemsProperty =
            DependencyProperty.Register("PropertyItems", typeof(ReadOnlyObservableCollection<PropertyEditorItem>), typeof(PropertyEditor));

        public ReadOnlyObservableCollection<PropertyEditorItem> PropertyItems
        {
            get { return (ReadOnlyObservableCollection<PropertyEditorItem>)GetValue(PropertyItemsProperty); }
        }

        #endregion

        #region DisplayNameWidth Property

        public static readonly DependencyProperty DisplayNameWidthProperty =
            DependencyProperty.Register("DisplayNameWidth", typeof(GridLength), typeof(PropertyEditor),
            new UIPropertyMetadata(new GridLength(120, GridUnitType.Pixel)));

        /// <summary>
        /// Gets/Sets the width of the display name column.
        /// </summary>
        public GridLength DisplayNameWidth
        {
            get { return (GridLength)GetValue(DisplayNameWidthProperty); }
            set { SetValue(DisplayNameWidthProperty, value); }
        }

        #endregion

        #region SearchCriteria Property

        public static readonly DependencyProperty SearchCriteriaProperty =
            DependencyProperty.Register("SearchCriteria", typeof(String), typeof(PropertyEditor),
            new UIPropertyMetadata(String.Empty, OnSearchCriteriaPropertyChanged));

        private static void OnSearchCriteriaPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((PropertyEditor)obj).UpdateDisplayedPropertyItems();
        }

        /// <summary>
        /// Gets/Sets the search criteria to use for filtering the displayed
        /// properties.
        /// </summary>
        public String SearchCriteria
        {
            get { return (String)GetValue(SearchCriteriaProperty); }
            set { SetValue(SearchCriteriaProperty, value); }
        }

        #endregion

        #region ItemTemplate Property

        public static readonly DependencyProperty ItemTemplateProperty =
            DependencyProperty.Register("ItemTemplate", typeof(DataTemplate), typeof(PropertyEditor));

        /// <summary>
        /// Gets/Sets the style to use for the item containers.
        /// Target type: PropertyEditorItem
        /// </summary>
        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }

        #endregion

        #region EditorTemplates Property

        public static readonly DependencyProperty EditorTemplatesProperty =
            DependencyProperty.Register("EditorTemplates", typeof(EditorTemplateCollection), typeof(PropertyEditor));

        /// <summary>
        /// Returns the collection of available editor templates
        /// </summary>
        public EditorTemplateCollection EditorTemplates
        {
            get { return (EditorTemplateCollection)GetValue(EditorTemplatesProperty); }
            set { SetValue(EditorTemplatesProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Style key override")]
        static PropertyEditor()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
               typeof(PropertyEditor), new FrameworkPropertyMetadata(typeof(PropertyEditor)));
        }

        public PropertyEditor()
        {
            SetValue(PropertyItemsProperty, new ReadOnlyObservableCollection<PropertyEditorItem>(_propertyItems));

            _propertyDescriptions.BulkCollectionChanged += PropertyDescriptions_BulkCollectionChanged;

        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the property descriptions change.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PropertyDescriptions_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            RefreshPropertyItems();
        }

        #endregion

        #region Methods

        private void RefreshPropertyItems()
        {
            if (_masterPropertyItems.Count > 0)
            {
                _masterPropertyItems.Clear();
            }

            if (this.SourceObject != null)
            {

                Type sourceType = this.SourceObject.GetType();

                foreach (PropertyItemDescription p in this.PropertyDescriptions)
                {
                    _masterPropertyItems.Add(GeneratePropertyItem(p, sourceType));
                }
            }

            UpdateDisplayedPropertyItems();
        }

        private PropertyEditorItem GeneratePropertyItem(PropertyItemDescription description, Type sourceType)
        {
            PropertyEditorItem item = new PropertyEditorItem(description);

            PropertyInfo pInfo = sourceType.GetProperty(description.PropertyName);
            if (pInfo != null)
            {
                //Display name
                DisplayNameAttribute displayNameAtt = pInfo.GetCustomAttributes(typeof(DisplayNameAttribute), false).SingleOrDefault() as DisplayNameAttribute;
                item.DisplayName = (displayNameAtt != null) ? displayNameAtt.DisplayName : pInfo.Name;

                //ItemsSource
                PropertyEditorItemsSourceAttribute itemsSourceAtt =
                    pInfo.GetCustomAttributes(typeof(PropertyEditorItemsSourceAttribute), false).SingleOrDefault() as PropertyEditorItemsSourceAttribute;
                if (itemsSourceAtt != null)
                {
                    IPropertyEditorItemsSource sourceProvider = Activator.CreateInstance(itemsSourceAtt.Type) as IPropertyEditorItemsSource;
                    if (sourceProvider != null)
                    {
                        item.ItemsSource = sourceProvider.GetValues();
                    }
                }

                //Set the binding
                Binding itemBinding;
                if (pInfo.CanWrite)
                {
                    item.IsReadOnly = false;

                    itemBinding =
                        new Binding(pInfo.Name)
                        {
                            Source = this.SourceObject,
                            Mode = BindingMode.TwoWay
                        };
                }
                else
                {
                    item.IsReadOnly = true;

                    itemBinding =
                        new Binding(pInfo.Name)
                        {
                            Source = this.SourceObject,
                            Mode = BindingMode.OneWay
                        };
                }
                BindingOperations.SetBinding(item, PropertyEditorItem.ValueProperty, itemBinding);

                Type propertyType = pInfo.PropertyType;
                if (propertyType.IsGenericType
                    && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    propertyType = Nullable.GetUnderlyingType(propertyType);
                }

                //find the editor template
                EditorTemplateDefinition templateDef = null;
                foreach (EditorTemplateDefinition editorDef in this.EditorTemplates)
                {
                    if (editorDef.PropertyNames.Contains(description.PropertyName))
                    {
                        templateDef = editorDef;
                        break;
                    }
                    else if (editorDef.TargetType as Type == typeof(IPropertyEditorItemsSource) && item.ItemsSource != null
                        && !item.IsReadOnly)
                    {
                        templateDef = editorDef;
                        break;
                    }
                    else if (editorDef.TargetType as Type == propertyType)
                    {
                        templateDef = editorDef;
                    }
                    else if (templateDef == null && editorDef.TargetType == null)
                    {
                        templateDef = editorDef;
                    }
                }
                if (templateDef != null)
                {
                    item.EditorTemplate = templateDef.Template;
                }


            }
            else
            {
                item.DisplayName = description.PropertyName;
                item.IsReadOnly = true;
            }


            return item;
        }

        /// <summary>
        /// Updates the property items collection
        /// </summary>
        private void UpdateDisplayedPropertyItems()
        {
            if (_propertyItems.Count > 0)
            {
                _propertyItems.Clear();
            }

            String criteria = this.SearchCriteria;
            if (!String.IsNullOrEmpty(criteria))
            {
                criteria = criteria.ToLowerInvariant();
            }

            foreach (PropertyEditorItem item in _masterPropertyItems)
            {
                if (String.IsNullOrEmpty(criteria)
                    || item.DisplayName.ToLowerInvariant().Contains(criteria))
                {
                    _propertyItems.Add(item);
                }
            }

        }

        #endregion

    }

    /// <summary>
    /// Defines a property item to be displayed by the property editor.
    /// </summary>
    public class PropertyItemDescription
    {
        public String PropertyName { get; set; }
    }

    /// <summary>
    /// Actual item displayed by the property editor.
    /// </summary>
    public class PropertyEditorItem : DependencyObject
    {
        public PropertyItemDescription Description { get; private set; }

        #region DisplayName Property

        public static readonly DependencyProperty DisplayNameProperty =
            DependencyProperty.Register("DisplayName", typeof(String), typeof(PropertyEditorItem));

        /// <summary>
        /// Gets/Sets the property item friendly display name.
        /// </summary>
        public String DisplayName
        {
            get { return (String)GetValue(DisplayNameProperty); }
            set { SetValue(DisplayNameProperty, value); }
        }

        #endregion

        #region Editor Template Property

        public static readonly DependencyProperty EditorTemplateProperty =
            DependencyProperty.Register("EditorTemplate", typeof(DataTemplate), typeof(PropertyEditorItem));

        /// <summary>
        /// Gets/Sets the template used to display this item.
        /// </summary>
        public DataTemplate EditorTemplate
        {
            get { return (DataTemplate)GetValue(EditorTemplateProperty); }
            set { SetValue(EditorTemplateProperty, value); }
        }

        #endregion

        #region IsReadOnly Property

        public static readonly DependencyProperty IsReadOnlyProperty =
            DependencyProperty.Register("IsReadOnly", typeof(Boolean), typeof(PropertyEditorItem),
            new PropertyMetadata(false));

        /// <summary>
        /// Gets/Sets whether this item is readonly.
        /// </summary>
        public Boolean IsReadOnly
        {
            get { return (Boolean)GetValue(IsReadOnlyProperty); }
            set { SetValue(IsReadOnlyProperty, value); }
        }

        #endregion

        #region ItemsSource Property

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(PropertyEditorItem));

        /// <summary>
        /// Gets/Sets the itemsource to use.
        /// </summary>
        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        #endregion

        #region Value Property

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(Object), typeof(PropertyEditorItem));

        public Object Value
        {
            get { return GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        #endregion

        #region Constructor

        public PropertyEditorItem(PropertyItemDescription propertyDesc)
        {
            Description = propertyDesc;
        }

        #endregion

    }

    /// <summary>
    /// Defines a template which can be used to display an item in the property editor.
    /// </summary>
    public class EditorTemplateDefinition : DependencyObject
    {
        #region TargetType Property

        public static readonly DependencyProperty TargetTypeProperty =
            DependencyProperty.Register("TargetType", typeof(Object), typeof(EditorTemplateDefinition),
            new PropertyMetadata());

        public Object TargetType
        {
            get { return (Object)GetValue(TargetTypeProperty); }
            set { SetValue(TargetTypeProperty, value); }
        }

        #endregion

        #region Template Property

        public static readonly DependencyProperty TemplateProperty =
            DependencyProperty.Register("Template", typeof(DataTemplate), typeof(EditorTemplateDefinition),
            new PropertyMetadata(null));

        public DataTemplate Template
        {
            get { return (DataTemplate)GetValue(TemplateProperty); }
            set { SetValue(TemplateProperty, value); }
        }

        #endregion

        #region PropertyNames

        public static readonly DependencyProperty PropertyNamesProperty =
            DependencyProperty.Register("PropertyNames", typeof(StringCollection), typeof(EditorTemplateDefinition));

        /// <summary>
        /// Returns the collection of specific property names
        /// this template should be used for.
        /// </summary>
        public StringCollection PropertyNames
        {
            get { return (StringCollection)GetValue(PropertyNamesProperty); }
        }

        #endregion

        public EditorTemplateDefinition()
        {
            SetValue(PropertyNamesProperty, new StringCollection());
        }
    }

    /// <summary>
    /// Attribute used to declare an itemsource.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class PropertyEditorItemsSourceAttribute : Attribute
    {
        public PropertyEditorItemsSourceAttribute(Type type)
        {
            Type = type;
        }

        public Type Type { get; set; }
    }

    public interface IPropertyEditorItemsSource
    {
        List<PropertyEditorSourceItem> GetValues();
    }

    public sealed class PropertyEditorItemsSource<T> : IPropertyEditorItemsSource
    {
        public List<PropertyEditorSourceItem> GetValues()
        {
            //find helper
            String enumTypeName = typeof(T).AssemblyQualifiedName;
            String helperTypeName = enumTypeName.Replace(typeof(T).Name, typeof(T).Name + "Helper");

            List<PropertyEditorSourceItem> values = new List<PropertyEditorSourceItem>();

            Type helperType = Type.GetType(helperTypeName);
            if (helperType != null)
            {
                FieldInfo fInfo = helperType.GetField("FriendlyNames");
                if (fInfo != null)
                {
                    Dictionary<T, String> dict = fInfo.GetValue(null) as Dictionary<T, String>;
                    if (dict != null)
                    {
                        foreach (var entry in dict)
                        {
                            values.Add(new PropertyEditorSourceItem(entry.Key, entry.Value));
                        }
                    }
                }
            }
            else
            {
                foreach (var key in Enum.GetValues(typeof(T)))
                {
                    values.Add(new PropertyEditorSourceItem(key, key.ToString()));
                }
            }

            return values;
        }
    }

    public class PropertyEditorSourceItem
    {
        public String DisplayName { get; set; }
        public Object Value { get; set; }

        public PropertyEditorSourceItem(Object val, String displayName)
        {
            Value = val;
            DisplayName = displayName;
        }
    }

    public class EditorTemplateCollection : List<EditorTemplateDefinition>
    {

    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class PropertyOrderAttribute : Attribute
    {
        public PropertyOrderAttribute(Int32 order)
        {
            Order = order;
        }

        public Int32 Order { get; set; }
    }
}
