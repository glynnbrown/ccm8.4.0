﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion
#endregion

using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Shapes;
using System;
using System.Windows.Controls;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    public sealed class CursorAdorner: Adorner
    {
        #region Fields

        private UIElement _child;
        private Double XCenter;
        private Double YCenter;

        #endregion

        #region Properties

        public static readonly DependencyProperty LeftOffsetProperty =
            DependencyProperty.Register("LeftOffset", typeof(Double), typeof(CursorAdorner),
            new FrameworkPropertyMetadata(0D, FrameworkPropertyMetadataOptions.AffectsRender, OnOffsetPropertyChanged));

        public Double LeftOffset
        {
            get{return (Double)GetValue(LeftOffsetProperty);}
            set {SetValue(LeftOffsetProperty, value);}
        }

        private static void OnOffsetPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((CursorAdorner)obj).UpdatePosition();
        }



        public static readonly DependencyProperty TopOffsetProperty =
           DependencyProperty.Register("TopOffset", typeof(Double), typeof(CursorAdorner),
           new FrameworkPropertyMetadata(0D, FrameworkPropertyMetadataOptions.AffectsRender, OnOffsetPropertyChanged));

        public Double TopOffset
        {
            get { return (Double)GetValue(TopOffsetProperty); }
            set { SetValue(TopOffsetProperty, value); }
        }


        #endregion

        #region Constructor

        public CursorAdorner(UIElement owner, ImageSource adornElementImage)
            : base(owner)
        {
            Image adornerImage = new Image();
            adornerImage.Width = 16.0;
            adornerImage.Height = 16.0;
            adornerImage.Source = adornElementImage;

            Initialize(adornerImage, false, 1);
           
        }

        /// <summary>
        /// Creates a new drag adorner
        /// </summary>
        /// <param name="owner">the element to use the adornerlayer of</param>
        /// <param name="adornElement">the visual to use as the adorner</param>
        /// <param name="useVisualBrush">toggle - switch if item fails to show</param>
        /// <param name="opacity">the opacity of the adorner</param>
        public CursorAdorner(UIElement owner, UIElement adornElement, Boolean useVisualBrush, Double opacity)
            : base(owner)
        {
            Initialize(adornElement, useVisualBrush, opacity);
        }

        #endregion


        #region Methods

        private void Initialize(UIElement adornElement, Boolean useVisualBrush, Double opacity)
        {
            UIElement childElement = null;

            if (adornElement == null)
            {
                childElement = new Rectangle();
            }
            else
            {
                if (useVisualBrush)
                {
                    VisualBrush brush = new VisualBrush(adornElement);
                    brush.Opacity = opacity;

                    //set the visual 
                    Rectangle r = new Rectangle();
                    r.Width = adornElement.RenderSize.Width;
                    r.Height = adornElement.RenderSize.Height;
                    XCenter = r.Width / 2;
                    YCenter = r.Height / 2;

                    //set as the cursor image
                    r.Fill = brush;
                    childElement = r;

                }
                else
                {
                    Double width = (Double)adornElement.GetValue(WidthProperty);
                    Double height = (Double)adornElement.GetValue(HeightProperty);

                    //correct the width and height if they have not been set
                    if (width == 0 && height == 0)
                    {
                        width = 50;
                        height = 50;
                    }

                    XCenter = width / 2;
                    YCenter = height / 2;
                    childElement = adornElement;
                }
            }

            _child = childElement;
        }

        /// <summary>
        /// Updates the position of the cursor adorner
        /// </summary>
        private void UpdatePosition()
        {
            AdornerLayer adorner = (AdornerLayer)this.Parent;
            if (adorner != null)
            {
                 adorner.Update(this.AdornedElement);
            }
        }

        protected override Visual GetVisualChild(int index)
        {
            return _child;
        }

        protected override Int32 VisualChildrenCount
        {
            get { return 1; }
        }

        protected override Size MeasureOverride(Size finalSize)
        {
            _child.Measure(finalSize);
            return _child.DesiredSize;
        }
        
        protected override Size ArrangeOverride(Size finalSize)
        {
            _child.Arrange(new Rect(_child.DesiredSize));
            return finalSize;
        }

        public override GeneralTransform GetDesiredTransform(GeneralTransform transform)
        {
            GeneralTransformGroup result = new GeneralTransformGroup();

            result.Children.Add(base.GetDesiredTransform(transform));
            result.Children.Add(new TranslateTransform(this.LeftOffset, this.TopOffset));
            return result;
        }

        #endregion
    }
}
