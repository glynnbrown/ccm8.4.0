﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26495 : L.Ineson 
//  Added create custom. Removed create backboard.
#endregion
#region Version History: (CCM 8.2)
// V8-30936 : M.Brumby
//  Added slotwall
#endregion

#endregion

using System;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Denotes the available mouse tools
    /// </summary>
    public enum MouseToolType
    {
        Pointer,
        Transform,
        Rotate,
        CreatePart,
        CreateText,
        CreateShelf,
        CreateChest,
        CreatePegboard,
        CreateBar,
        CreateRod,
        CreateClipstrip,
        CreatePallet,
        CreatePanel,
        CreateCustom,
        CreateSlotWall
    }

    public static class MouseToolTypeHelper
    {
        public static Boolean IsCreateType(MouseToolType tooltype)
        {
            switch (tooltype)
            {
                case MouseToolType.CreatePart:
                case MouseToolType.CreateText:
                case MouseToolType.CreateShelf:
                case MouseToolType.CreateChest:
                case MouseToolType.CreatePegboard:
                case MouseToolType.CreateBar:
                case MouseToolType.CreateRod:
                case MouseToolType.CreateClipstrip:
                case MouseToolType.CreatePallet:
                case MouseToolType.CreatePanel:
                case MouseToolType.CreateCustom:
                case MouseToolType.CreateSlotWall:
                    return true;

                default: return false;
            }
        }
    }
}
