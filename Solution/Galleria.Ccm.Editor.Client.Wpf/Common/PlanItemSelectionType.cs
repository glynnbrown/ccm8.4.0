﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820
// V8-30932 : L.Ineson
// Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Denotes the available types of selection mode.
    /// </summary>
    public enum PlanItemSelectionType
    {
        /// <summary>
        /// Allows the user to only select positions on the planogram
        /// </summary>
        OnlyPositions,

        /// <summary>
        /// allows the user to select a position and the system automatically selects the related product positions
        /// </summary>
        AllProductPositions,

        /// <summary>
        /// select only components on the planogram, if a position is selected then the related component should be selected
        /// </summary>
        OnlyComponents,

        /// <summary>
        /// allows for the positions and components to be selected (DEFAULT)
        /// </summary>
        PositionsAndComponents,

        /// <summary>
        /// Allows for the product positions and components to be selected

        /// </summary>
        ProductsAndComponents
    }

    /// <summary>
    /// Provides helpers for the PlanItemSelectionType enum
    /// </summary>
    public static class PlanItemSelectionTypeHelper
    {
        public static readonly Dictionary<PlanItemSelectionType, String> FriendlyNames =
           new Dictionary<PlanItemSelectionType, String>()
            {
                {PlanItemSelectionType.OnlyPositions, Message.Enum_PlanItemSelectionType_OnlyPositions},
                {PlanItemSelectionType.AllProductPositions, Message.Enum_PlanItemSelectionType_AllProductPositions},
                {PlanItemSelectionType.OnlyComponents, Message.Enum_PlanItemSelectionType_OnlyComponents},
                {PlanItemSelectionType.PositionsAndComponents, Message.PlanItemSelectionType_PositionsAndComponents},
                {PlanItemSelectionType.ProductsAndComponents, Message.Enum_PlanItemSelectionType_ProductsAndComponents}
            };

         public static readonly Dictionary<PlanItemSelectionType, String> FriendlyDescriptions =
           new Dictionary<PlanItemSelectionType, String>()
            {
                {PlanItemSelectionType.OnlyPositions, Message.Enum_PlanItemSelectionType_OnlyPositions_Desc},
                {PlanItemSelectionType.AllProductPositions, Message.Enum_PlanItemSelectionType_AllProductPositions_Desc},
                {PlanItemSelectionType.OnlyComponents, Message.Enum_PlanItemSelectionType_OnlyComponents_Desc},
                {PlanItemSelectionType.PositionsAndComponents, Message.PlanItemSelectionType_PositionsAndComponents_Desc},
                {PlanItemSelectionType.ProductsAndComponents, Message.Enum_PlanItemSelectionType_ProductsAndComponents_Desc}
            };


    }
}
