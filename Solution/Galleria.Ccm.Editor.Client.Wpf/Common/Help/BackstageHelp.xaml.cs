﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Interaction logic for BackstageHelp.xaml
    /// </summary>
    public sealed partial class BackstageHelp : UserControl
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(BackstageHelpViewModel), typeof(BackstageHelp),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public BackstageHelpViewModel ViewModel
        {
            get { return (BackstageHelpViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            BackstageHelp senderControl = (BackstageHelp)obj;

            if (e.OldValue != null)
            {
                BackstageHelpViewModel oldModel = (BackstageHelpViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                BackstageHelpViewModel newModel = (BackstageHelpViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public BackstageHelp()
        {
            InitializeComponent();

            this.ViewModel = new BackstageHelpViewModel();
        }

        #endregion
    }
}
