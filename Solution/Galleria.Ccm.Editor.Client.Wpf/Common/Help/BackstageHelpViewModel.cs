﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using System.Windows;
using Galleria.Framework.Helpers;
using System.Reflection;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    public sealed class BackstageHelpViewModel : ViewModelAttachedControlObject<BackstageHelp>
    {
        #region Help File Path Constants
        private const String CCMHelpFileName = "Resources\\HelpFiles\\CCM_Help.chm";
        #endregion

        #region Fields
        private String _buildVersion = String.Empty;
        private String _keyword = String.Empty;
        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath BuildVersionProperty = WpfHelper.GetPropertyPath<BackstageHelpViewModel>(p => p.BuildVersion);
        
        //Commands
        public static readonly PropertyPath OnlineHelpCommandProperty = WpfHelper.GetPropertyPath<BackstageHelpViewModel>(p => p.OnlineHelpCommand);
        public static readonly PropertyPath ContactUsCommandProperty = WpfHelper.GetPropertyPath<BackstageHelpViewModel>(p => p.ContactUsCommand);

        #endregion

        #region Properties

        /// <summary>
        /// The version of the current build
        /// </summary>
        public String BuildVersion
        {
            get { return _buildVersion; }
            private set
            {
                _buildVersion = value;
                OnPropertyChanged(BuildVersionProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Testing Constructor
        /// </summary>
        /// <param name="state"></param>
        public BackstageHelpViewModel()
        {
            //Get Version Information
            this.BuildVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        #endregion

        #region Commands

        #region OnlineHelpCommand

        private RelayCommand _onlineHelpCommand;

        public RelayCommand OnlineHelpCommand
        {
            get
            {
                if (_onlineHelpCommand == null)
                {
                    this._onlineHelpCommand = new RelayCommand(p => OnlineHelp_Executed())
                    {
                        FriendlyName = Message.BackstageHelp_OnlineHelp_Button,
                        Icon = ImageResources.BackstageHelp_Help_32
                    };
                    this.ViewModelCommands.Add(_onlineHelpCommand);
                }
                return _onlineHelpCommand;
            }
        }

        private void OnlineHelp_Executed()
        {
            //if (this.AttachedControl != null)
            //{
            //    String loc = Assembly.GetExecutingAssembly().Location;
            //    String path = System.IO.Path.GetDirectoryName(loc);
            //    String fileToOpen = System.IO.Path.Combine(path, App.ViewState.HelpFilePath);
            //    DependencyObject parent = Helpers.FindVisualAncestor<ExtendedRibbonWindow>(this.AttachedControl);

            //    if (parent != null)
            //    {
            //        _keyword = Help.GetKeyword(parent);
            //    }

            //    if (System.IO.File.Exists(fileToOpen))
            //    {
            //        if (!String.IsNullOrEmpty(_keyword))
            //        {
            //            form.Help.ShowHelp(null, fileToOpen, form.HelpNavigator.TopicId, _keyword);
            //        }
            //        else
            //        {
            //            form.Help.ShowHelp(null, fileToOpen);
            //        }
            //    }
            //    else
            //    {
            //        //if debugging, need to also look in this location
            //        fileToOpen = System.IO.Path.Combine(path, CCMHelpFileName);


            //        if (System.IO.File.Exists(fileToOpen))
            //        {
            //            if (!String.IsNullOrEmpty(_keyword))
            //            {
            //                form.Help.ShowHelp(null, fileToOpen, form.HelpNavigator.TopicId, _keyword);
            //            }
            //            else
            //            {
            //                form.Help.ShowHelp(null, fileToOpen);
            //            }
            //        }
            //        else
            //        {
            //            #region Inform User
            //            ModalMessage msg = new ModalMessage();
            //            msg.Title = System.Windows.Application.Current.MainWindow.GetType().Assembly.GetName().Name;
            //            msg.Header = Galleria.Ccm.Client.Wpf.Resources.Language.Message.BackstageHelp_CannotFindFile_Header;
            //            msg.Description = String.Format(Galleria.Ccm.Client.Wpf.Resources.Language.Message.BackstageHelp_CannotFindHelpFile_Message, fileToOpen);
            //            msg.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            //            msg.ButtonCount = 1;
            //            msg.Button1Content = Galleria.Ccm.Client.Wpf.Resources.Language.Message.Generic_Ok;
            //            msg.MessageIcon = ImageResources.Warning_32;

            //            App.ShowWindow(msg, true);
            //            #endregion
            //        }
            //    }
            //}
        }

        #endregion

        #region ContactUsCommand

        private RelayCommand _contactUsCommand;

        public RelayCommand ContactUsCommand
        {
            get
            {
                if (_contactUsCommand == null)
                {
                    this._contactUsCommand = new RelayCommand(p => ContactUs_Executed())
                    {
                        FriendlyName = Message.BackstageHelp_ContactUs_Button,
                        Icon = ImageResources.BackstageHelp_ContactUs_32
                    };
                    this.ViewModelCommands.Add(_contactUsCommand);
                }
                return _contactUsCommand;
            }
        }

        private void ContactUs_Executed()
        {
            //if (this.AttachedControl != null)
            //{
            //    App.ShowWindow(new ContactUsWindow(), true);
            //}
        }

        #endregion

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    //nothing to dispose
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
