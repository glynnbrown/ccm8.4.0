﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25734 : L.Ineson ~ Created.
#endregion
#endregion

using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Rendering;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace Galleria.Ccm.Editor.Client.Wpf.Common.Rendering
{
    public sealed class FixtureComponent3DData : ModelConstruct3DData, IPlanPart3DData
    {
        #region Fields

        private readonly IFixtureRenderSettings _settingsController;
        private readonly IFixtureComponentRenderable _component;
        private readonly Dictionary<IFixtureSubComponentRenderable, FixtureSubComponent3DData> _modelsDict = new Dictionary<IFixtureSubComponentRenderable, FixtureSubComponent3DData>();

        #endregion

        #region Properties

        /// <summary>
        /// Returns the source component view.
        /// </summary>
        public IFixtureComponentRenderable Component
        {
            get { return _component; }
        }

        /// <summary>
        /// Returns the settings controller
        /// </summary>
        private IFixtureRenderSettings SettingsController
        {
            get { return _settingsController; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="componentView"></param>
        /// <param name="settingsController"></param>
        public FixtureComponent3DData(IFixtureComponentRenderable componentView, IFixtureRenderSettings settingsController)
        {
            _settingsController = settingsController;

            _component = componentView;
            componentView.PropertyChanged += Component_PropertyChanged;
            componentView.SubComponentsCollectionChanged += Component_SubComponentsCollectionChanged;

            this.Size = new WidthHeightDepthValue
            {
                Width = componentView.Width,
                Height = componentView.Height,
                Depth = componentView.Depth
            };

            UpdateModelPlacement();

            //Add child Subcomponents
            foreach (var subComponent in componentView.SubComponents)
            {
                AddSubComponentModel(subComponent);
            }

        }

        

        #endregion

        #region Event Handlers

        /// <summary>
        /// Reponds to property changes on the main component view.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Component_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "X":
                case "Y":
                case "Z":
                case "Slope":
                case "Angle":
                case "Roll":
                    UpdateModelPlacement();
                    break;

                case "Height":
                case "Width":
                case "Depth":
                    this.Size = new WidthHeightDepthValue
                    {
                        Width = this.Component.Width,
                        Height = this.Component.Height,
                        Depth = this.Component.Depth
                    };
                    break;
            }
        }

        /// <summary>
        /// Called when the subcomponents collection on the linked component changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Component_SubComponentsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (IFixtureSubComponentRenderable view in e.NewItems)
                        {
                            AddSubComponentModel(view);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        foreach (IFixtureSubComponentRenderable view in e.OldItems)
                        {
                            RemoveSubComponentModel(view);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        List<IFixtureSubComponentRenderable> views = this.Component.SubComponents.ToList();

                        //remove as required
                        foreach (IFixtureSubComponentRenderable modelledView in _modelsDict.Keys.ToList())
                        {
                            if (!views.Contains(modelledView))
                            {
                                RemoveSubComponentModel(modelledView);
                            }
                            else
                            {
                                views.Remove(modelledView);
                            }
                        }

                        //add remaining
                        foreach (IFixtureSubComponentRenderable v in views)
                        {
                            AddSubComponentModel(v);
                        }
                    }
                    break;
            }
        }

        #endregion

        #region Methods

        #region Child Methods

        /// <summary>
        /// Adds a new child for the given Subcomponent view
        /// </summary>
        /// <param name="assemblyView"></param>
        private void AddSubComponentModel(IFixtureSubComponentRenderable view)
        {
            FixtureSubComponent3DData model = new FixtureSubComponent3DData(view, SettingsController);
            AddChild(model);
            _modelsDict.Add(view, model);
        }

        /// <summary>
        /// Removes the child model for the given Subcomponent view.
        /// </summary>
        /// <param name="assemblyView"></param>
        private void RemoveSubComponentModel(IFixtureSubComponentRenderable view)
        {
            FixtureSubComponent3DData model;

            if (_modelsDict.TryGetValue(view, out model))
            {
                RemoveChild(model);
                _modelsDict.Remove(view);
            }
        }

        #endregion

        private void UpdateModelPlacement()
        {
            var part = this.Component;

            this.Position =
                new PointValue(part.X, part.Y, part.Z);

            this.Rotation =
                new RotationValue(part.Angle, part.Slope, part.Roll);
        }

        #endregion

        #region IPlanPart3DData Members

        public object PlanPart
        {
            get { return _component; }
        }

        void IPlanPart3DData.PauseUpdates()
        {
            
        }

        void IPlanPart3DData.UnpauseUpdates()
        {
            
        }

        #endregion


        #region IDisposable Members

        public override void Dispose()
        {
            if (!IsDisposed)
            {
                this.Component.PropertyChanged -= Component_PropertyChanged;
                this.Component.SubComponentsCollectionChanged -= Component_SubComponentsCollectionChanged;

                DisposeBase();

                GC.SuppressFinalize(this);
                IsDisposed = true;
            }
        }

        #endregion

       
    }
}
