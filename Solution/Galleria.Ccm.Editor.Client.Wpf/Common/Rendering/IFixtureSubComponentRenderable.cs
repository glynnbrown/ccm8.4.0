﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25734 : L.Ineson 
//  Created.
// V8-26091 : L.Ineson
//  Added more face thick properties
#endregion
#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.DataStructures;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Galleria.Ccm.Editor.Client.Wpf.Common.Rendering
{
    /// <summary>
    /// Defines basic members required to render a subcomponent.
    /// </summary>
    public interface IFixtureSubComponentRenderable : INotifyPropertyChanged
    {
        Boolean IsUpdating { get; }

        /// <summary>
        /// The x position of the part (front bottom left)
        /// </summary>
        Single X { get; }
        /// <summary>
        /// The y position of the part (front bottom left)
        /// </summary>
        Single Y { get; }
        /// <summary>
        /// The z position of the part (front bottom left)
        /// </summary>
        Single Z { get; }

        /// <summary>
        /// The rotation angle
        /// </summary>
        Single Angle { get; }
        /// <summary>
        /// The rotation slope
        /// </summary>
        Single Slope { get; }
        /// <summary>
        /// The rotation roll
        /// </summary>
        Single Roll { get; }

        /// <summary>
        /// The height of the part
        /// </summary>
        Single Height { get; }
        /// <summary>
        /// The width of the part
        /// </summary>
        Single Width { get; }
        /// <summary>
        /// The depth of the part
        /// </summary>
        Single Depth { get; }


        /// <summary>
        /// Returns true if the subcomponent is merchandisable
        /// </summary>
        Boolean IsMerchandisable { get; }

        /// <summary>
        /// Gets the shape that this subcomponent should be rendered as.
        /// </summary>
        FixtureSubComponentShapeType ShapeType { get; }

        /// <summary>
        /// Flag denoting it this is part of a chest (helper)
        /// </summary>
        Boolean IsChest { get; }

        //******************************
        // Face Fill Fields
        //*******************************

        /// <summary>
        /// Fill colour for the front facing
        /// </summary>
        Int32 FillColourFront { get; }

        /// <summary>
        /// Fill colour for the back facing
        /// </summary>
        Int32 FillColourBack { get; }

        /// <summary>
        /// Fill colour for the top facing
        /// </summary>
        Int32 FillColourTop { get; }

        /// <summary>
        /// Fill colour for the bottom facing
        /// </summary>
        Int32 FillColourBottom { get; }

        /// <summary>
        /// Fill colour for the left facing
        /// </summary>
        Int32 FillColourLeft { get; }

        /// <summary>
        /// Fill colour for the right facing
        /// </summary>
        Int32 FillColourRight { get; }

        /// <summary>
        /// The fill pattern type to use on the front facing
        /// </summary>
        FixtureSubComponentFillPatternType FillPatternTypeFront { get; }

        /// <summary>
        /// The fill pattern type to use on the back facing
        /// </summary>
        FixtureSubComponentFillPatternType FillPatternTypeBack { get; }

        /// <summary>
        /// The fill pattern type to use on the top facing
        /// </summary>
        FixtureSubComponentFillPatternType FillPatternTypeTop { get; }

        /// <summary>
        /// The fill pattern type to use on the bottom facing
        /// </summary>
        FixtureSubComponentFillPatternType FillPatternTypeBottom { get; }

        /// <summary>
        /// The fill pattern type to use on the letf facing
        /// </summary>
        FixtureSubComponentFillPatternType FillPatternTypeLeft { get; }

        /// <summary>
        /// The fill pattern type to use on the right facing
        /// </summary>
        FixtureSubComponentFillPatternType FillPatternTypeRight { get; }

        /// <summary>
        /// The colour of lines on the subcomponent
        /// </summary>
        Int32 LineColour { get; }

        /// <summary>
        /// The thickness of lines on the subcomponent.
        /// </summary>
        Single LineThickness { get; }

        /// <summary>
        /// The transparency percentage of the front facing
        /// </summary>
        Int32 TransparencyPercentFront { get; }

        /// <summary>
        /// The transparency percentage of the back facing
        /// </summary>
        Int32 TransparencyPercentBack { get; }

        /// <summary>
        /// The transparency percentage of the top facing
        /// </summary>
        Int32 TransparencyPercentTop { get; }

        /// <summary>
        /// The transparency percentage of the bottom facing
        /// </summary>
        Int32 TransparencyPercentBottom { get; }

        /// <summary>
        /// The transparency percentage of the left facing
        /// </summary>
        Int32 TransparencyPercentLeft { get; }

        /// <summary>
        /// The transparency percentage of the right facing
        /// </summary>
        Int32 TransparencyPercentRight { get; }

        Byte[] FrontImageData { get; }
        Byte[] BackImageData { get; }
        Byte[] TopImageData { get; }
        Byte[] BottomImageData { get; }
        Byte[] LeftImageData { get; }
        Byte[] RightImageData { get; }

        //******************************
        // Face Thickness Fields
        //*******************************

        Single FaceThicknessFront { get; }
        Single FaceThicknessBack { get; }
        Single FaceThicknessTop { get; }
        Single FaceThicknessBottom { get; }
        Single FaceThicknessLeft { get; }
        Single FaceThicknessRight { get; }

        //******************************
        // Riser Fields
        //*******************************

        /// <summary>
        /// The height of risers
        /// </summary>
        Single RiserHeight { get; }

        /// <summary>
        /// The thickness of risers.
        /// </summary>
        Single RiserThickness { get; }

        /// <summary>
        /// The colour of the riser
        /// </summary>
        Int32 RiserColour { get; }

        /// <summary>
        /// The transparency percentage of the riser colour.
        /// </summary>
        Int32 RiserTransparencyPercent { get; }

        /// <summary>
        /// Returns true if the front riser should be drawn
        /// </summary>
        Boolean IsRiserPlacedOnFront { get; }

        /// <summary>
        /// Returns true if the back riser should be drawn
        /// </summary>
        Boolean IsRiserPlacedOnBack { get; }

        /// <summary>
        /// Returns true if the left riser should be drawn
        /// </summary>
        Boolean IsRiserPlacedOnLeft { get; }

        /// <summary>
        /// Returns true if the right riser should be drawn
        /// </summary>
        Boolean IsRiserPlacedOnRight { get; }



        //******************************
        // Notch Fields
        //*******************************

        /// <summary>
        /// The x start position of backboard notches
        /// </summary>
        Single NotchStartX { get; }

        /// <summary>
        /// The x spacing of backboard notches
        /// </summary>
        Single NotchSpacingX { get; }

        /// <summary>
        /// The y start position of backboard notches
        /// </summary>
        Single NotchStartY { get; }

        /// <summary>
        /// The y spacing of backboard notches
        /// </summary>
        Single NotchSpacingY { get; }

        /// <summary>
        /// The height of the backboard notches
        /// </summary>
        Single NotchHeight { get; }

        /// <summary>
        /// The width of backboard notches
        /// </summary>
        Single NotchWidth { get; }

        /// <summary>
        /// Returns true if notches should be placed on the front facing
        /// </summary>
        Boolean IsNotchPlacedOnFront { get; }

        /// <summary>
        /// The style type of the notches
        /// </summary>
        FixtureSubComponentNotchStyleType NotchStyleType { get; }



        //******************************
        // Slot lines
        //*******************************

        /// <summary>
        /// The start x position of the divider obstruction
        /// </summary>
        Single DividerObstructionStartX { get; }

        /// <summary>
        /// The x spacing between each divider
        /// </summary>
        Single DividerObstructionSpacingX { get; }

        /// <summary>
        /// The start y position of the divider obstruction
        /// </summary>
        Single DividerObstructionStartY { get; }

        /// <summary>
        /// The y spacing between each divider
        /// </summary>
        Single DividerObstructionSpacingY { get; }

        /// <summary>
        /// The start z position of the divider obstruction
        /// </summary>
        Single DividerObstructionStartZ { get; }

        /// <summary>
        /// The z spacing between each divider.
        /// </summary>
        Single DividerObstructionSpacingZ { get; }

        /// <summary>
        /// The width of the divider lines
        /// </summary>
        Single DividerObstructionWidth { get; }

        //******************************
        // Peg Hole Fields
        //*******************************

        /// <summary>
        /// The start x position of the first merch constraint row.
        /// Used by pegboards
        /// </summary>
        Single MerchConstraintRow1StartX { get; }

        /// <summary>
        /// The x spacing of the first merch constraint row.
        ///  Used by pegboards
        /// </summary>
        Single MerchConstraintRow1SpacingX { get; }

        /// <summary>
        /// The start y position of the first merch constraint row.
        ///  Used by pegboards
        /// </summary>
        Single MerchConstraintRow1StartY { get; }

        /// <summary>
        /// The y spacing of the first merch constraint row.
        ///  Used by pegboards
        /// </summary>
        Single MerchConstraintRow1SpacingY { get; }

        /// <summary>
        /// The height of holes on row 1
        /// </summary>
        Single MerchConstraintRow1Height { get; }

        /// <summary>
        /// The width of holes on row 1
        /// </summary>
        Single MerchConstraintRow1Width { get; }

        /// <summary>
        /// The start x position of the second merch constraint row.
        /// Used by pegboards
        /// </summary>
        Single MerchConstraintRow2StartX { get; }

        /// <summary>
        /// The x spacing of the second merch constraint row.
        ///  Used by pegboards
        /// </summary>
        Single MerchConstraintRow2SpacingX { get; }

        /// <summary>
        /// The start y position of the second merch constraint row.
        ///  Used by pegboards
        /// </summary>
        Single MerchConstraintRow2StartY { get; }

        /// <summary>
        /// The y spacing of the second merch constraint row.
        ///  Used by pegboards
        /// </summary>
        Single MerchConstraintRow2SpacingY { get; }

        /// <summary>
        /// The height of holes on row 2
        /// </summary>
        Single MerchConstraintRow2Height { get; }

        /// <summary>
        /// The width of holes on row 2
        /// </summary>
        Single MerchConstraintRow2Width { get; }




        //TODO -

        ///// <summary>
        ///// Returns true if notches should be placed on the back facing
        ///// </summary>
        //Boolean IsNotchPlacedOnBack { get; }

        ///// <summary>
        ///// Return true if notches should be placed on the left facing.
        ///// </summary>
        //Boolean IsNotchPlacedOnLeft { get; }

        ///// <summary>
        ///// Returns true if notches should be placed on the right facing.
        ///// </summary>
        //Boolean IsNotchPlacedOnRight { get; }

        ///// <summary>
        ///// The fill pattern to be used on the riser.
        ///// </summary>
        //SubComponentFillPatternType RiserFillPatternType { get; }

        ///// <summary>
        ///// The thickness of the top face
        ///// </summary>
        //Single FaceThicknessTop { get; }

        ///// <summary>
        ///// The offset of the top face
        ///// </summary>
        //Single FaceTopOffset { get; }

        ///// <summary>
        ///// The shape type of the subcomponent
        ///// </summary>
        //SubComponentShapeType ShapeType { get; set; }


    }

    /// <summary>
    /// Helper class for ISubComponentRenderable
    /// </summary>
    public static class FixtureRenderingHelper
    {
        

        /// <summary>
        /// Returns the center points for all pegholes to be drawn for the given sub.
        /// </summary>
        /// <param name="sub"></param>
        /// <param name="row1Holes"></param>
        /// <param name="row2Holes"></param>
        public static void GetPegHoles(IFixtureSubComponentRenderable sub, out List<PointValue> row1Holes, out List<PointValue> row2Holes, out Single pegWidth1, out Single pegWidth2)
        {
            pegWidth1 = 0.2f;
            pegWidth2 = 0.2f;
            List<PointValue> row1 = new List<PointValue>();
            List<PointValue> row2 = new List<PointValue>();

            Boolean hasRow1 = (sub.MerchConstraintRow1SpacingY > 0 && sub.MerchConstraintRow1Height > 0 && sub.MerchConstraintRow1Width > 0);
            Boolean hasRow2 = (sub.MerchConstraintRow2SpacingY > 0 && sub.MerchConstraintRow2Height > 0 && sub.MerchConstraintRow2Width > 0);

            if (hasRow1)
            {
                Single minX = 0;
                Single maxX = sub.Width;
                Single minY = 0;
                Single maxY = sub.Height;

                for (Int32 i = 0; i < 2; i++)
                {
                    if ((i == 0 && hasRow1) || (i == 1 && hasRow2))
                    {
                        List<PointValue> points;
                        Single startX;
                        Single startY;
                        Single spacingX;
                        Single spacingY;
                        Boolean isSlots = false;
                        if (i == 0)
                        {
                            points = row1;
                            startX = sub.MerchConstraintRow1StartX;
                            startY = sub.MerchConstraintRow1StartY;
                            spacingX = sub.MerchConstraintRow1SpacingX;
                            spacingY = sub.MerchConstraintRow1SpacingY;

                            if (spacingX == 0)
                            {
                                pegWidth1 = (sub.Width - startX);
                                isSlots = true;
                            }
                            else
                            {
                                pegWidth1 = sub.MerchConstraintRow1Width;
                            }
                        }
                        else
                        {
                            points = row2;
                            startX = sub.MerchConstraintRow2StartX;
                            startY = sub.MerchConstraintRow2StartY;
                            spacingX = sub.MerchConstraintRow2SpacingX;
                            spacingY = sub.MerchConstraintRow2SpacingY;

                            if (spacingX == 0)
                            {
                                pegWidth2 = (sub.Width - startX);
                                isSlots = true;
                            }
                            else
                            {
                                pegWidth2 = sub.MerchConstraintRow2Width;
                            }
                        }
                        

                        //draw down.
                        Single curY = maxY - startY;
                        while (curY > minY)
                        {
                            Single curX = minX + startX;
                            if (!isSlots)
                            {
                                while (curX < maxX)
                                {
                                    points.Add(new PointValue(curX, curY, sub.Depth));
                                    curX += spacingX;
                                }
                            }
                            else
                            {
                                points.Add(new PointValue(curX + ((sub.Width - startX) / 2.0f), curY, sub.Depth));                                
                            }
                            
                            curY -= spacingY;
                        }

                    }

                }
            }


            row1Holes = row1;
            row2Holes = row2;
        }

        
    }
}
