﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25734 : L.Ineson ~ Created.
#endregion
#endregion

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.Specialized;
using Galleria.Ccm.Editor.Client.Wpf.Common;

namespace Galleria.Ccm.Editor.Client.Wpf.Common.Rendering
{
    /// <summary>
    /// Defines members required to render a fixture component.
    /// </summary>
    public interface IFixtureComponentRenderable : INotifyPropertyChanged
    {
        /// <summary>
        /// The x position of the part (front bottom left)
        /// </summary>
        Single X { get; }
        /// <summary>
        /// The y position of the part (front bottom left)
        /// </summary>
        Single Y { get; }
        /// <summary>
        /// The z position of the part (front bottom left)
        /// </summary>
        Single Z { get; }

        /// <summary>
        /// The rotation angle
        /// </summary>
        Single Angle { get; }
        /// <summary>
        /// The rotation slope
        /// </summary>
        Single Slope { get; }
        /// <summary>
        /// The rotation roll
        /// </summary>
        Single Roll { get; }

        /// <summary>
        /// The height of the part
        /// </summary>
        Single Height { get; }
        /// <summary>
        /// The width of the part
        /// </summary>
        Single Width { get; }
        /// <summary>
        /// The depth of the part
        /// </summary>
        Single Depth { get; }


        event NotifyCollectionChangedEventHandler SubComponentsCollectionChanged;
        IEnumerable<IFixtureSubComponentRenderable> SubComponents { get; }
    }
}
