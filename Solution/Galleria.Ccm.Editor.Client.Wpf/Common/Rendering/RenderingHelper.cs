﻿using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Rendering;
using System;
using System.Collections.Generic;
using System.Windows;

namespace Galleria.Ccm.Editor.Client.Wpf.Common.Rendering
{
    public static class RenderingHelper
    {
        #region Enums

        public static PlanogramLabelHorizontalAlignment GetModelHorizontalAlignment(Model.LabelHorizontalPlacementType type)
        {
            switch (type)
            {
                default:
                case Model.LabelHorizontalPlacementType.Center: return PlanogramLabelHorizontalAlignment.Center;
                case Model.LabelHorizontalPlacementType.Left: return PlanogramLabelHorizontalAlignment.Left;
                case Model.LabelHorizontalPlacementType.Right: return PlanogramLabelHorizontalAlignment.Right;
            }
        }

        public static PlanogramLabelVerticalAlignment GetModelVerticalAlignment(Model.LabelVerticalPlacementType type)
        {
            switch (type)
            {
                case Model.LabelVerticalPlacementType.Bottom: return PlanogramLabelVerticalAlignment.Bottom;
                default:
                case Model.LabelVerticalPlacementType.Center: return PlanogramLabelVerticalAlignment.Center;
                case Model.LabelVerticalPlacementType.Top: return PlanogramLabelVerticalAlignment.Top;
            }
        }

        public static PlanogramLabelTextDirection GetModelTextDirection(Model.LabelTextDirectionType type)
        {
            switch (type)
            {
                default:
                case Model.LabelTextDirectionType.Horizontal: return PlanogramLabelTextDirection.Horizontal;
                case Model.LabelTextDirectionType.Vertical: return PlanogramLabelTextDirection.Vertical;
            }
        }

        public static PlanogramLabelTextWrapping GetModelTextWrapping(Model.LabelTextWrapping type)
        {
            switch (type)
            {
                case Model.LabelTextWrapping.Letter: return PlanogramLabelTextWrapping.Letter;
                case Model.LabelTextWrapping.None: return PlanogramLabelTextWrapping.None;

                default:
                case Model.LabelTextWrapping.Word: return PlanogramLabelTextWrapping.Word;
            }
        }

        public static PlanogramLabelTextAlignment GetModelTextAlignment(TextAlignment type)
        {
            switch (type)
            {
                case TextAlignment.Center: return PlanogramLabelTextAlignment.Center;
                case TextAlignment.Justify: return PlanogramLabelTextAlignment.Justified;
                default:
                case TextAlignment.Left: return PlanogramLabelTextAlignment.Left;
                case TextAlignment.Right: return PlanogramLabelTextAlignment.Right;
            }

        }

        #endregion


    }

    public static class CameraViewTypeHelper
    {
        public static readonly Dictionary<CameraViewType, String> FriendlyNames =
           new Dictionary<CameraViewType, String>()
            {
                {CameraViewType.Perspective, DocumentTypeHelper.FriendlyNames[DocumentType.Perspective]},
                {CameraViewType.Front, DocumentTypeHelper.FriendlyNames[DocumentType.Front]},
                {CameraViewType.Left, DocumentTypeHelper.FriendlyNames[DocumentType.Left]},
                {CameraViewType.Right, DocumentTypeHelper.FriendlyNames[DocumentType.Right]},
                {CameraViewType.Top, DocumentTypeHelper.FriendlyNames[DocumentType.Top]},
                {CameraViewType.Bottom, DocumentTypeHelper.FriendlyNames[DocumentType.Bottom]},
                {CameraViewType.Back, DocumentTypeHelper.FriendlyNames[DocumentType.Back]},
                {CameraViewType.Design, DocumentTypeHelper.FriendlyNames[DocumentType.Design]}
            };
    }
}
