﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25734 : L.Ineson ~ Created.
#endregion
#endregion

using Galleria.Framework.DataStructures;
using System;
using System.ComponentModel;

namespace Galleria.Ccm.Editor.Client.Wpf.Common.Rendering
{
    public interface IFixtureRenderSettings : INotifyPropertyChanged
    {
        Boolean SuppressPerformanceIntensiveUpdates { get; set; }
        Double SelectionLineThickness { get; }
        ModelColour SelectionColour { get; }

        Boolean ShowShelfRisers { get; set; }
        Boolean ShowChestWalls { get; set; }
        Boolean ShowFixtureImages { get; set; }
        Boolean ShowFixtureFillPatterns { get; set; }
        Boolean ShowWireframeOnly { get; set; }
        Boolean ShowNotches { get; set; }
        Boolean ShowPegHoles { get; set; }
        Boolean ShowDividerLines { get; set; }
    }


    /// <summary>
    /// A default implementation of IFixture3DDataSettings
    /// </summary>
    public class FixtureRenderSettings : IFixtureRenderSettings
    {
        #region Fields

        private Boolean _suppressPerformanceIntensiveUpdates;

        private Boolean _showChestWalls = true;
        private Boolean _showShelfRisers = true;
        private Boolean _showFixtureImages = false;
        private Boolean _showFixtureFillPatterns = false;
        private Boolean _showWireframeOnly = false;
        private Boolean _showNotches = false;
        private Boolean _showPegHoles = false;
        private Boolean _showSlotLines = false;

        private Double _selectionLineThickness = 0.5;
        private ModelColour _selectionColour;
        #endregion

        #region PropertyNames

        //Not using property paths here because they are not supported in silverlight.
        public static readonly String SuppressPerformanceIntensiveUpdatesPropertyName = "SuppressPerformanceIntensiveUpdates";
        public static readonly String ShowChestWallsPropertyName = "ShowChestWalls";
        public static readonly String ShowShelfRisersPropertyName = "ShowShelfRisers";
        public static readonly String ShowFixtureImagesPropertyName = "ShowFixtureImages";
        public static readonly String ShowFixtureFillPatternsPropertyName = "ShowFixtureFillPatterns";
        public static readonly String ShowWireframeOnlyPropertyName = "ShowWireframeOnly";
        public static readonly String SelectionColourPropertyName = "SelectionColour";
        public static readonly String ShowNotchesPropertyName = "ShowNotches";
        public static readonly String ShowPegHolesPropertyName = "ShowPegHoles";
        public static readonly String ShowSlotLinesPropertyName = "ShowSlotLines";
        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets whether heavy updates should be suppressed.
        /// This is used while the plan is in an update mode.
        /// </summary>
        public Boolean SuppressPerformanceIntensiveUpdates
        {
            get { return _suppressPerformanceIntensiveUpdates; }
            set
            {
                if (value != _suppressPerformanceIntensiveUpdates)
                {
                    Object oldValue = _suppressPerformanceIntensiveUpdates;
                    _suppressPerformanceIntensiveUpdates = value;
                    OnPropertyChanged(SuppressPerformanceIntensiveUpdatesPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether chest walls should be visible
        /// </summary>
        public Boolean ShowChestWalls
        {
            get { return _showChestWalls; }
            set
            {
                if (value != _showChestWalls)
                {
                    Object oldValue = _showChestWalls;
                    _showChestWalls = value;
                    OnPropertyChanged(ShowChestWallsPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether shelf risers should be drawn
        /// </summary>
        public Boolean ShowShelfRisers
        {
            get { return _showShelfRisers; }
            set
            {
                if (value != _showShelfRisers)
                {
                    Object oldValue = _showShelfRisers;

                    _showShelfRisers = value;
                    OnPropertyChanged(ShowShelfRisersPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether fixture images should be shown.
        /// </summary>
        public Boolean ShowFixtureImages
        {
            get { return _showFixtureImages; }
            set
            {
                if (value != _showFixtureImages)
                {
                    _showFixtureImages = value;
                    OnPropertyChanged(ShowFixtureImagesPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether fixture fill patterns should be shown.
        /// </summary>
        public Boolean ShowFixtureFillPatterns
        {
            get { return _showFixtureFillPatterns; }
            set
            {
                if (value != _showFixtureFillPatterns)
                {
                    _showFixtureFillPatterns = value;
                    OnPropertyChanged(ShowFixtureFillPatternsPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether the model should be shown as a wireframe.
        /// </summary>
        public Boolean ShowWireframeOnly
        {
            get { return _showWireframeOnly; }
            set
            {
                if (value != _showWireframeOnly)
                {
                    Object oldValue = _showWireframeOnly;

                    _showWireframeOnly = value;
                    OnPropertyChanged(ShowWireframeOnlyPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets the thickness of selection lines.
        /// </summary>
        public Double SelectionLineThickness
        {
            get { return _selectionLineThickness; }
        }

        /// <summary>
        /// Gets/Sets the colour used for selection
        /// </summary>
        public ModelColour SelectionColour
        {
            get { return _selectionColour; }
            set
            {
                _selectionColour = value;
                OnPropertyChanged(SelectionColourPropertyName);
            }
        }

        /// <summary>
        /// Gets/Sets whether notches should be shown
        /// </summary>
        public Boolean ShowNotches
        {
            get { return _showNotches; }
            set
            {
                if (value != _showNotches)
                {
                    Object oldValue = _showNotches;

                    _showNotches = value;
                    OnPropertyChanged(ShowNotchesPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether peg holes should be shown.
        /// </summary>
        public Boolean ShowPegHoles
        {
            get { return _showPegHoles; }
            set
            {
                if (value != _showPegHoles)
                {
                    Object oldValue = _showPegHoles;

                    _showPegHoles = value;
                    OnPropertyChanged(ShowPegHolesPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether slot lines should be shown.
        /// </summary>
        public Boolean ShowDividerLines
        {
            get { return _showSlotLines; }
            set
            {
                if (value != _showSlotLines)
                {
                    Object oldValue = _showSlotLines;

                    _showSlotLines = value;
                    OnPropertyChanged(ShowSlotLinesPropertyName);
                }
            }
        }

        #endregion

        #region Constructor
        public FixtureRenderSettings()
        {
            _selectionColour =
            ModelColour.NewColour(System.Windows.Media.Colors.Orange.R,
            System.Windows.Media.Colors.Orange.G, System.Windows.Media.
            Colors.Orange.B, System.Windows.Media.Colors.Orange.A);
        }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }
}
