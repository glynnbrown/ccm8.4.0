﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion
#region Version History: (CCM 8.3)
// V8-32523 : L.Ineson
//  Added support for component level annotations.
#endregion
#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Planograms.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;

namespace Galleria.Ccm.Editor.Client.Wpf.Common.Rendering
{
    /// <summary>
    /// Simple class to facilitate the preview of components
    /// </summary>
    public sealed class PreviewComponent : IPlanComponentRenderable, IUpdateable
    {
        #region Fields

        private readonly ObservableCollection<PreviewSubComponent> _subComponents = new ObservableCollection<PreviewSubComponent>();
        private readonly ObservableCollection<IPlanAnnotationRenderable> _annotations = new ObservableCollection<IPlanAnnotationRenderable>();

        private Single _x;
        private Single _y;
        private Single _z;
        private Single _angle;
        private Single _slope;
        private Single _roll;
        private Single _height;
        private Single _width;
        private Single _depth;

        #endregion

        #region Properties

        public ObservableCollection<PreviewSubComponent> SubComponents
        {
            get { return _subComponents; }
        }


        public Single X
        {
            get { return _x; }
            set
            {
                _x = value;
                OnPropertyChanged("X");
            }
        }
        public Single Y
        {
            get { return _y; }
            set
            {
                _y = value;
                OnPropertyChanged("Y");
            }
        }
        public Single Z
        {
            get { return _z; }
            set
            {
                _z = value;
                OnPropertyChanged("Z");
            }
        }

        public Single Angle
        {
            get { return _angle; }
            set
            {
                _angle = value;
                OnPropertyChanged("Angle");
            }
        }
        public Single Slope
        {
            get { return _slope; }
            set
            {
                _slope = value;
                OnPropertyChanged("Slope");
            }
        }
        public Single Roll
        {
            get { return _roll; }
            set
            {
                _roll = value;
                OnPropertyChanged("Roll");
            }
        }

        public Single Height
        {
            get { return _height; }
            set
            {
                //resize the subcomponents. The component height will update automatically.
                Double heightChange = value - _height;
                foreach (var sub in this.SubComponents)
                {
                    Single newHeight = Convert.ToSingle(sub.Height + heightChange);
                    if (newHeight > 0 && newHeight != sub.Height)
                    {
                        sub.Height = newHeight;
                    }
                }
                _height = value;

                OnPropertyChanged("Height");
            }
        }
        public Single Width
        {
            get { return _width; }
            set
            {
                //resize the subcomponents. The component width will update automatically.
                Double widthChange = value - _width;
                foreach (var sub in this.SubComponents)
                {
                    Single newWidth = Convert.ToSingle(sub.Width + widthChange);
                    if (newWidth > 0 && newWidth != sub.Width)
                    {
                        sub.Width = newWidth;
                    }
                }

                _width = value;
                OnPropertyChanged("Width");
            }
        }
        public Single Depth
        {
            get { return _depth; }
            set
            {
                //resize the subcomponents. The component width will update automatically.
                Double depthChange = _depth - value;
                foreach (var sub in this.SubComponents)
                {
                    Single newDepth = Convert.ToSingle(sub.Depth - depthChange);
                    if (newDepth > 0 && newDepth != sub.Depth)
                    {
                        sub.Depth = newDepth;
                    }
                }

                _depth = value;
                OnPropertyChanged("Depth");
            }
        }

        public Boolean IsMerchandisedTopDown
        {
            get { return false; }
        }

        #endregion

        #region Methods

        public void SetSize(WidthHeightDepthValue size)
        {
            this.Width = Convert.ToSingle(size.Width);
            this.Height = Convert.ToSingle(size.Height);
            this.Depth = Convert.ToSingle(size.Depth);
        }

        public void SetRotation(RotationValue rotation)
        {
            this.Angle = Convert.ToSingle(rotation.Angle);
            this.Slope = Convert.ToSingle(rotation.Slope);
            this.Roll = Convert.ToSingle(rotation.Roll);
        }

        public void SetPosition(PointValue position)
        {
            this.X = Convert.ToSingle(position.X);
            this.Y = Convert.ToSingle(position.Y);
            this.Z = Convert.ToSingle(position.Z);
        }


        /// <summary>
        /// Returns the text to be displayed by the position label based on
        /// the given format string.
        /// </summary>
        /// <param name="formatString"></param>
        /// <returns></returns>
        public String GetLabelText(String formatString)
        {
            return String.Empty;
        }

        #endregion

        #region IPlanogramComponent Members

        event NotifyCollectionChangedEventHandler IPlanComponentRenderable.SubComponentsCollectionChanged
        {
            add { _subComponents.CollectionChanged += value; }
            remove { _subComponents.CollectionChanged -= value; }
        }

        IEnumerable<IPlanSubComponentRenderable> IPlanComponentRenderable.SubComponents
        {
            get { return this.SubComponents; }
        }

        event NotifyCollectionChangedEventHandler IPlanComponentRenderable.AnnotationsCollectionChanged
        {
            add { _annotations.CollectionChanged += value; }
            remove { _annotations.CollectionChanged -= value; }
        }

        IEnumerable<IPlanAnnotationRenderable> IPlanComponentRenderable.Annotations
        {
            get { return _annotations; }
        }

        object IPlanComponentRenderable.Id
        {
            get { return null; }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IUpdatable

        private Boolean _isUpdating;

        public Boolean IsUpdating
        {
            get { return _isUpdating; }
            private set
            {
                _isUpdating = value;
                OnPropertyChanged("IsUpdating");
            }
        }

        /// <summary>
        /// Notifies this object that multiple properties are about to be updated
        /// UI should not be updated until EndUpdate has been called.
        /// </summary>
        /// <returns>true if an update actually began</returns>
        public void BeginUpdate()
        {
            if (!IsUpdating)
            {
                IsUpdating = true;

                //ripple down.
                foreach (var child in SubComponents)
                {
                    child.BeginUpdate();
                }
            }
        }

        /// <summary>
        /// Ends the ongoing update state.
        /// </summary>
        public void EndUpdate()
        {
            if (IsUpdating)
            {
                IsUpdating = false;

                //ripple down.
                foreach (var child in SubComponents)
                {
                    child.EndUpdate();
                }
            }
        }

        #endregion

        #region Static Helpers

        public static PreviewComponent CreatePreview(PlanogramComponentType type, UserEditorSettings settings)
        {
            return CreatePreview((FixtureComponentType)type, settings);
        }

        public static PreviewComponent CreatePreview(FixtureComponentType type, UserEditorSettings settings)
        {
            PreviewComponent component = new PreviewComponent();
            component.SubComponents.Clear();

            PreviewSubComponent subcomponent = new PreviewSubComponent();
            component.SubComponents.Add(subcomponent);

            component.SetSize(LocalHelper.GetDefaultComponentSize(type, settings));

            if (type == FixtureComponentType.Chest)
            {
                //TODO: Set thickness values
                component.SubComponents[0].FaceThicknessFront = 2;
                component.SubComponents[0].FaceThicknessBack = 2;
                component.SubComponents[0].FaceThicknessTop = 0;
                component.SubComponents[0].FaceThicknessBottom = 2;
                component.SubComponents[0].FaceThicknessLeft = 2;
                component.SubComponents[0].FaceThicknessRight = 2;
            }

            return component;
        }

        public static PreviewComponent CreateAnnotationPreview()
        {
            PreviewComponent component = new PreviewComponent();

            PreviewSubComponent subcomponent = new PreviewSubComponent();
            component.SubComponents.Add(subcomponent);

            component.Height = 20;
            component.Width = 40;
            component.Depth = 1;

            return component;
        }

        #endregion


        
    }
}
