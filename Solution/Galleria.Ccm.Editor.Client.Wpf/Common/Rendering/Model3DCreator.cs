﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Media3D;
using System.Windows.Media;
using HelixToolkit.Wpf;
using System.Windows.Controls;
using System.Windows.Input;


namespace Galleria.Ccm.Editor.Client.Wpf.Common.Rendering
{
    /// <summary>
    /// A class to facilitate defining the size of a 3d model
    /// </summary>
    public class Model3DCreator : UIElement3D
    {
        #region Fields

        private GeometryModel3D _model;
        private Viewport3D _parentViewport;
        private Viewport3D _hitTestViewport;
        private Point3D _lastPoint;
        private Vector3D _currDirection;

        #endregion

        #region Properties

        #region Position Property

        public static readonly DependencyProperty PositionProperty =
            DependencyProperty.Register("Position", typeof(Point3D), typeof(Model3DCreator),
            new UIPropertyMetadata(new Point3D(), OnPositionPropertyChanged));

        private static void OnPositionPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Model3DCreator)obj).OnPositionChanged();
        }

        /// <summary>
        /// Gets/Sets the origin position for the model
        /// (Front Lower Left point of the bounding cube)
        /// </summary>
        public Point3D Position
        {
            get { return (Point3D)GetValue(PositionProperty); }
            set { SetValue(PositionProperty, value); }
        }

        private void OnPositionChanged()
        {
            Point3D pos = this.Position;
            this.Transform = new TranslateTransform3D(pos.X, pos.Y, pos.Z);
        }

        #endregion

        #region Size Property

        public static readonly DependencyProperty SizeProperty =
            DependencyProperty.Register("Size", typeof(Size3D), typeof(Model3DCreator),
            new UIPropertyMetadata(new Size3D(10, 10, 10), OnSizePropertyChanged));

        private static void OnSizePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Model3DCreator)obj).Tesselate();
        }

        /// <summary>
        /// Gets/Sets the size of the model.
        /// </summary>
        public Size3D Size
        {
            get { return (Size3D)GetValue(SizeProperty); }
            set { SetValue(SizeProperty, value); }
        }

        #endregion

        #region Colour Property

        public static readonly DependencyProperty ColourProperty =
            DependencyProperty.Register("Colour", typeof(Color), typeof(Model3DCreator),
            new UIPropertyMetadata(Colors.DarkRed, OnColourPropertyChanged));

        private static void OnColourPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Model3DCreator)obj).OnColourChanged();
        }

        /// <summary>
        /// Gets/Sets the colour to create the material model in.
        /// </summary>
        public Color Colour
        {
            get { return (Color)GetValue(ColourProperty); }
            set { SetValue(ColourProperty, value); }
        }

        private void OnColourChanged()
        {
            SolidColorBrush brush = new SolidColorBrush(this.Colour);
            brush.Freeze();

            Material mat = new DiffuseMaterial(brush);
            mat.Freeze();

            _model.Material = mat;
            _model.BackMaterial = mat;
        }

        #endregion

        #endregion

        #region Events

        public event EventHandler PartDefinitionCompleted;

        private void OnPartDefinitionCompleted()
        {
            if (PartDefinitionCompleted != null)
            {
                PartDefinitionCompleted(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Constructor

        public Model3DCreator()
        {
            _model = new GeometryModel3D();
            this.Visual3DModel = _model;

            OnColourChanged();
        }

        #endregion

        #region Methods

        public void StartMouseCapture(MouseEventArgs e, Viewport3D hitTestViewport)
        {

            _currDirection = new Vector3D(1, 0, 0);
            _parentViewport = Visual3DHelper.GetViewport3D(this);
            _hitTestViewport = hitTestViewport;

            //_camera = _parentViewport.Camera as ProjectionCamera;

            //var projectionCamera = _camera;
            //if (projectionCamera != null)
            //{
            //    _hitPlaneNormal = projectionCamera.LookDirection;
            //}

            Point hitLoc = e.GetPosition(_parentViewport);
            Point3D position = GetPoint3D(hitLoc, hitTestViewport);

            //Point3D position;

            //Point3D? nrPos = Viewport3DHelper.FindNearestPoint(hitTestViewport, hitLoc);
            //if (!nrPos.HasValue)
            //{
            //    Ray3D posRay = Viewport3DHelper.Point2DtoRay3D(_parentViewport, hitLoc);
            //    Vector3D rayDirection = posRay.Direction;
            //    rayDirection.Normalize();

            //    position = posRay.GetNearest(new Point3D());
            //}
            //else { position = nrPos.Value; }




            //floor the y axis to the nearest model or 0.
            Double maxY = 0;
            var hits = Viewport3DHelper.FindHits(hitTestViewport, hitLoc);
            if (hits.Count > 0)
            {
                maxY = hits.Max(s => s.Position.Y);
            }
            position.Y = Math.Max(0, maxY);

            this.Position = position;


            var lp = ToLocal(position);
            _lastPoint = lp;

            //this.CaptureMouse();

            Tesselate();
        }

        public void ProcessMouseMove(MouseEventArgs e)
        {
            Point hitLoc = e.GetPosition(_parentViewport);
            //Point3D nearestPoint = GetPoint3D(hitLoc, _hitTestViewport);

            Ray3D posRay = Viewport3DHelper.Point2DtoRay3D(_parentViewport, hitLoc);
            Vector3D rayDirection = posRay.Direction;
            rayDirection.Normalize();

            Point3D nearestPoint = posRay.GetNearest(new Point3D());



            Vector3D direction = _currDirection;

            Vector3D delta = ToLocal(nearestPoint) - _lastPoint;

            Double changeVal = Vector3D.DotProduct(delta, direction);
            Vector3D changeVector = Vector3D.Multiply(Math.Round(changeVal, 2), direction);

            Size3D oldSize = this.Size;

            Double newWidth = Math.Round(oldSize.X + changeVector.X, 2);
            if (newWidth < 1) newWidth = 1;

            Double newHeight = Math.Round(oldSize.Y + changeVector.Y, 2);
            if (newHeight < 1) newHeight = 1;

            Double newDepth = Math.Round(oldSize.Z + changeVector.Z, 2);
            if (newDepth < 1) newDepth = 1;

            //set new size
            this.Size =
                new Size3D()
                {
                    X = newWidth,
                    Y = newHeight,
                    Z = newDepth
                };

            _lastPoint = ToLocal(nearestPoint);
        }

        public void ProcessMouseUp(MouseButtonEventArgs e)
        {
            if (_currDirection == new Vector3D(0, 0, 0))
            {
                _currDirection = new Vector3D(1, 0, 0);
            }
            else if (_currDirection == new Vector3D(1, 0, 0))
            {
                //switch to manipulate depth
                _currDirection = new Vector3D(0, 0, 1);
            }
            else if (_currDirection == new Vector3D(0, 0, 1))
            {
                //switch to manipulate height.
                _currDirection = new Vector3D(0, 1, 0);
            }
            else
            {
                //we are done.
                //ReleaseMouseCapture();
                OnPartDefinitionCompleted();
            }
        }

        /// <summary>
        /// Creates the model mesh.
        /// </summary>
        private void Tesselate()
        {
            Double h = this.Size.Y;
            Double w = this.Size.X;
            Double d = this.Size.Z;

            Point3D p0 = new Point3D(0, 0, 0);//bll
            Point3D p1 = new Point3D(w, 0, 0);//blr
            Point3D p2 = new Point3D(w, 0, d);//flr
            Point3D p3 = new Point3D(0, 0, d);//fll
            Point3D p4 = new Point3D(0, h, 0);//bul
            Point3D p5 = new Point3D(w, h, 0);//bur
            Point3D p6 = new Point3D(w, h, d);//fur
            Point3D p7 = new Point3D(0, h, d);//ful


            MeshBuilder mb = new MeshBuilder();

            Double diameter = 0.1;
            Int32 thetaDiv = 5;

            mb.AddCylinder(p7, p6, diameter, thetaDiv);//ful to fur
            mb.AddCylinder(p4, p5, diameter, thetaDiv);//bul to bur
            mb.AddCylinder(p3, p2, diameter, thetaDiv);//fll to flr
            mb.AddCylinder(p0, p1, diameter, thetaDiv);//bll to blr
            mb.AddCylinder(p7, p4, diameter, thetaDiv);//ful to bul
            mb.AddCylinder(p6, p5, diameter, thetaDiv);//fur to bur
            mb.AddCylinder(p3, p0, diameter, thetaDiv);//fll to bll
            mb.AddCylinder(p2, p1, diameter, thetaDiv);//flr to blr
            mb.AddCylinder(p6, p2, diameter, thetaDiv);//fur to flr
            mb.AddCylinder(p7, p3, diameter, thetaDiv);//ful to fll
            mb.AddCylinder(p5, p1, diameter, thetaDiv);//bur to blr
            mb.AddCylinder(p4, p0, diameter, thetaDiv);//bul to bll

            _model.Geometry = mb.ToMesh();
        }

        /// <summary>
        /// Transforms from world to local coordinates.
        /// </summary>
        /// <param name="worldPoint">
        /// The point (world coordinates).
        /// </param>
        /// <returns>
        /// Transformed vector (local coordinates).
        /// </returns>
        private Point3D ToLocal(Point3D worldPoint)
        {
            var mat = Visual3DHelper.GetTransform(this);
            mat.Invert();
            var t = new MatrixTransform3D(mat);
            return t.Transform(worldPoint);
        }

        /// <summary>
        /// Transforms from local to world coordinates.
        /// </summary>
        /// <param name="point">
        /// The point (local coordinates).
        /// </param>
        /// <returns>
        /// Transformed point (world coordinates).
        /// </returns>
        private Point3D ToWorld(Point3D point)
        {
            var mat = Visual3DHelper.GetTransform(this);
            var t = new MatrixTransform3D(mat);
            return t.Transform(point);
        }

        /// <summary>
        /// Transforms from local to world coordinates.
        /// </summary>
        /// <param name="vector">
        /// The vector (local coordinates).
        /// </param>
        /// <returns>
        /// Transformed vector (world coordinates).
        /// </returns>
        private Vector3D ToWorld(Vector3D vector)
        {
            var mat = Visual3DHelper.GetTransform(this);
            var t = new MatrixTransform3D(mat);
            return t.Transform(vector);
        }

        /// <summary>
        /// Gets the nearest point on the translation axis.
        /// </summary>
        /// <param name="position">
        /// The position (in screen coordinates).
        /// </param>
        /// <param name="hitPlaneOrigin">
        /// The hit plane origin (world coordinate system).
        /// </param>
        /// <param name="hitPlaneNormal">
        /// The hit plane normal (world coordinate system).
        /// </param>
        /// <returns>
        /// The nearest point (world coordinates) or null if no point could be found.
        /// </returns>
        private Point3D? GetNearestPoint(Point position, Point3D hitPlaneOrigin, Vector3D hitPlaneNormal)
        {
            var hpp = GetHitPlanePoint(position, hitPlaneOrigin, hitPlaneNormal);
            if (hpp == null)
            {
                return null;
            }

            var ray = new Ray3D(this.ToWorld(this.Position), this.ToWorld(_currDirection));
            return ray.GetNearest(hpp.Value);
        }

        /// <summary>
        /// Projects the point on the hit plane.
        /// </summary>
        /// <param name="p">
        /// The p.
        /// </param>
        /// <param name="hitPlaneOrigin">
        /// The hit Plane Origin.
        /// </param>
        /// <param name="hitPlaneNormal">
        /// The hit plane normal (world coordinate system).
        /// </param>
        /// <returns>
        /// The point in world coordinates.
        /// </returns>
        private Point3D? GetHitPlanePoint(Point p, Point3D hitPlaneOrigin, Vector3D hitPlaneNormal)
        {
            return Viewport3DHelper.UnProject(_parentViewport, p, hitPlaneOrigin, hitPlaneNormal);
        }

        public String GetStatusText()
        {
            //TODO: localise
            return String.Format("Creating part: h{0} x w{1} x d{2}",
                Math.Round(this.Size.Y), Math.Round(this.Size.X), Math.Round(this.Size.Z));
        }

        private static Point3D GetPoint3D(Point hitLoc, Viewport3D viewport)
        {
            Point3D position = new Point3D();

            Point3D? nrPos = Viewport3DHelper.FindNearestPoint(viewport, hitLoc);
            if (!nrPos.HasValue)
            {
                Ray3D posRay = Viewport3DHelper.Point2DtoRay3D(viewport, hitLoc);
                Vector3D rayDirection = posRay.Direction;
                rayDirection.Normalize();

                position = posRay.GetNearest(new Point3D());
            }
            else { position = nrPos.Value; }


            //floor the y axis to 0.
            position.Y = Math.Max(0, position.Y);

            return position;
        }

        #endregion
    }
}
