﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion
#endregion

using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Planograms.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;

namespace Galleria.Ccm.Editor.Client.Wpf.Common.Rendering
{
    /// <summary>
    /// Simple class to facilitate the preview of subcomponents
    /// </summary>
    public sealed class PreviewSubComponent : IPlanSubComponentRenderable, IUpdateable
    {
        #region Fields

        //private Boolean _isEditing;
        private Single _x;
        private Single _y;
        private Single _z;
        private Single _height;
        private Single _width;
        private Single _depth;
        //private Boolean _isVisible = true;
        private Single _faceThicknessFront = 0;
        private Single _faceThicknessBack = 0;
        private Single _faceThicknessTop = 0;
        private Single _faceThicknessBottom = 0;
        private Single _faceThicknessLeft = 0;
        private Single _faceThicknessRight = 0;

        #endregion

        #region Properties

        public Single X
        {
            get { return _x; }
            set
            {
                _x = value;
                OnPropertyChanged("X");
            }
        }

        public Single Y
        {
            get { return _y; }
            set
            {
                _y = value;
                OnPropertyChanged("Y");
            }
        }

        public Single Z
        {
            get { return _z; }
            set
            {
                _z = value;
                OnPropertyChanged("Z");
            }
        }

        public Single Height
        {
            get { return _height; }
            set
            {
                _height = value;
                OnPropertyChanged("Height");
            }
        }

        public Single Width
        {
            get { return _width; }
            set
            {
                _width = value;
                OnPropertyChanged("Width");
            }
        }

        public Single Depth
        {
            get { return _depth; }
            set
            {
                _depth = value;
                OnPropertyChanged("Depth");
            }
        }

        public Single FaceThicknessFront
        {
            get { return _faceThicknessFront; }
            set
            {
                _faceThicknessFront = value;
                OnPropertyChanged("FaceThicknessFront");
            }
        }

        public Single FaceThicknessBack
        {
            get { return _faceThicknessBack; }
            set
            {
                _faceThicknessBack = value;
                OnPropertyChanged("FaceThicknessBack");
            }
        }

        public Single FaceThicknessTop
        {
            get { return _faceThicknessTop; }
            set
            {
                _faceThicknessTop = value;
                OnPropertyChanged("FaceThicknessTop");
            }
        }

        public Single FaceThicknessBottom
        {
            get { return _faceThicknessBottom; }
            set
            {
                _faceThicknessBottom = value;
                OnPropertyChanged("FaceThicknessBottom");
            }
        }

        public Single FaceThicknessLeft
        {
            get { return _faceThicknessLeft; }
            set
            {
                _faceThicknessLeft = value;
                OnPropertyChanged("FaceThicknessLeft");
            }
        }

        public Single FaceThicknessRight
        {
            get { return _faceThicknessRight; }
            set
            {
                _faceThicknessRight = value;
                OnPropertyChanged("FaceThicknessRight");
            }
        }

        #endregion

        #region IUpdatable

        private Boolean _isUpdating;

        public Boolean IsUpdating
        {
            get { return _isUpdating; }
            private set
            {
                _isUpdating = value;
                OnPropertyChanged("IsUpdating");
            }
        }

        /// <summary>
        /// Notifies this object that multiple properties are about to be updated
        /// UI should not be updated until EndUpdate has been called.
        /// </summary>
        public void BeginUpdate()
        {
            if (!IsUpdating)
            {
                IsUpdating = true;

                //ripple down.
                //foreach (var child in SubComponents)
                //{
                //    child.BeginUpdate();
                //}

            }
        }

        /// <summary>
        /// Ends the ongoing update state.
        /// </summary>
        public void EndUpdate()
        {
            if (IsUpdating)
            {
                IsUpdating = false;

                //ripple down.
                //foreach (var child in SubComponents)
                //{
                //    child.EndUpdate();
                //}
            }
        }

        #endregion

        #region IPlanogramSubComponent Members

        event NotifyCollectionChangedEventHandler IPlanSubComponentRenderable.PositionsCollectionChanged
        {
            add { }
            remove { }
        }
        IEnumerable<IPlanPositionRenderable> IPlanSubComponentRenderable.Positions
        {
            get { return new List<IPlanPositionRenderable>(); }
        }

        event NotifyCollectionChangedEventHandler IPlanSubComponentRenderable.AnnotationsCollectionChanged
        {
            add { }
            remove { }
        }
        IEnumerable<IPlanAnnotationRenderable> IPlanSubComponentRenderable.Annotations
        {
            get { return new List<IPlanAnnotationRenderable>(); }
        }

        event NotifyCollectionChangedEventHandler IPlanSubComponentRenderable.DividersCollectionChanged
        {
            add { }
            remove { }
        }

        IEnumerable<ISubComponentDividerRenderable> IPlanSubComponentRenderable.Dividers
        {
            get { return null; }
        }

        PlanogramSubComponentShapeType IPlanSubComponentRenderable.ShapeType { get { return PlanogramSubComponentShapeType.Box; } }
        Boolean IPlanSubComponentRenderable.IsMerchandisable { get { return true; } }
        Int32 IPlanSubComponentRenderable.FillColourFront { get { return 0; } }
        Int32 IPlanSubComponentRenderable.FillColourBack { get { return 0; } }
        Int32 IPlanSubComponentRenderable.FillColourTop { get { return 0; } }
        Int32 IPlanSubComponentRenderable.FillColourBottom { get { return 0; } }
        Int32 IPlanSubComponentRenderable.FillColourLeft { get { return 0; } }
        Int32 IPlanSubComponentRenderable.FillColourRight { get { return 0; } }
        PlanogramSubComponentFillPatternType IPlanSubComponentRenderable.FillPatternTypeFront { get { return PlanogramSubComponentFillPatternType.Solid; } }
        PlanogramSubComponentFillPatternType IPlanSubComponentRenderable.FillPatternTypeBack { get { return PlanogramSubComponentFillPatternType.Solid; } }
        PlanogramSubComponentFillPatternType IPlanSubComponentRenderable.FillPatternTypeTop { get { return PlanogramSubComponentFillPatternType.Solid; } }
        PlanogramSubComponentFillPatternType IPlanSubComponentRenderable.FillPatternTypeBottom { get { return PlanogramSubComponentFillPatternType.Solid; } }
        PlanogramSubComponentFillPatternType IPlanSubComponentRenderable.FillPatternTypeLeft { get { return PlanogramSubComponentFillPatternType.Solid; } }
        PlanogramSubComponentFillPatternType IPlanSubComponentRenderable.FillPatternTypeRight { get { return PlanogramSubComponentFillPatternType.Solid; } }
        Int32 IPlanSubComponentRenderable.LineColour { get { return 0; } }
        Single IPlanSubComponentRenderable.LineThickness { get { return 0; } }
        Int32 IPlanSubComponentRenderable.TransparencyPercentFront { get { return 0; } }
        Int32 IPlanSubComponentRenderable.TransparencyPercentBack { get { return 0; } }
        Int32 IPlanSubComponentRenderable.TransparencyPercentTop { get { return 0; } }
        Int32 IPlanSubComponentRenderable.TransparencyPercentBottom { get { return 0; } }
        Int32 IPlanSubComponentRenderable.TransparencyPercentLeft { get { return 0; } }
        Int32 IPlanSubComponentRenderable.TransparencyPercentRight { get { return 0; } }
        Byte[] IPlanSubComponentRenderable.FrontImageData { get { return null; } }
        Byte[] IPlanSubComponentRenderable.BackImageData { get { return null; } }
        Byte[] IPlanSubComponentRenderable.TopImageData { get { return null; } }
        Byte[] IPlanSubComponentRenderable.BottomImageData { get { return null; } }
        Byte[] IPlanSubComponentRenderable.LeftImageData { get { return null; } }
        Byte[] IPlanSubComponentRenderable.RightImageData { get { return null; } }

        Single IPlanSubComponentRenderable.RiserHeight { get { return 0; } }
        Single IPlanSubComponentRenderable.RiserThickness { get { return 0; } }
        Int32 IPlanSubComponentRenderable.RiserColour { get { return 0; } }
        Int32 IPlanSubComponentRenderable.RiserTransparencyPercent { get { return 0; } }
        Boolean IPlanSubComponentRenderable.IsRiserPlacedOnFront { get { return false; } }
        Boolean IPlanSubComponentRenderable.IsRiserPlacedOnBack { get { return false; } }
        Boolean IPlanSubComponentRenderable.IsRiserPlacedOnLeft { get { return false; } }
        Boolean IPlanSubComponentRenderable.IsRiserPlacedOnRight { get { return false; } }
        Single IPlanSubComponentRenderable.NotchStartX { get { return 0; } }
        Single IPlanSubComponentRenderable.NotchSpacingX { get { return 0; } }
        Single IPlanSubComponentRenderable.NotchStartY { get { return 0; } }
        Single IPlanSubComponentRenderable.NotchSpacingY { get { return 0; } }
        Single IPlanSubComponentRenderable.NotchHeight { get { return 0; } }
        Single IPlanSubComponentRenderable.NotchWidth { get { return 0; } }
        Boolean IPlanSubComponentRenderable.IsNotchPlacedOnFront { get { return false; } }
        PlanogramSubComponentNotchStyleType IPlanSubComponentRenderable.NotchStyleType { get { return PlanogramSubComponentNotchStyleType.SingleRectangle; } }
        //Single IPlanSubComponentRenderable.DividerObstructionStartX { get { return 0; } }
        //Single IPlanSubComponentRenderable.DividerObstructionSpacingX { get { return 0; } }
        //Single IPlanSubComponentRenderable.DividerObstructionStartY { get { return 0; } }
        //Single IPlanSubComponentRenderable.DividerObstructionSpacingY { get { return 0; } }
        //Single IPlanSubComponentRenderable.DividerObstructionStartZ { get { return 0; } }
        //Single IPlanSubComponentRenderable.DividerObstructionSpacingZ { get { return 0; } }
        //Single IPlanSubComponentRenderable.DividerObstructionWidth { get { return 0; } }
        //Single IPlanSubComponentRenderable.MerchConstraintRow1StartX { get { return 0; } }
        //Single IPlanSubComponentRenderable.MerchConstraintRow1SpacingX { get { return 0; } }
        //Single IPlanSubComponentRenderable.MerchConstraintRow1StartY { get { return 0; } }
        //Single IPlanSubComponentRenderable.MerchConstraintRow1SpacingY { get { return 0; } }
        Single IPlanSubComponentRenderable.MerchConstraintRow1Height { get { return 0; } }
        Single IPlanSubComponentRenderable.MerchConstraintRow1Width { get { return 0; } }
        //Single IPlanSubComponentRenderable.MerchConstraintRow2StartX { get { return 0; } }
        //Single IPlanSubComponentRenderable.MerchConstraintRow2SpacingX { get { return 0; } }
        //Single IPlanSubComponentRenderable.MerchConstraintRow2StartY { get { return 0; } }
        //Single IPlanSubComponentRenderable.MerchConstraintRow2SpacingY { get { return 0; } }
        Single IPlanSubComponentRenderable.MerchConstraintRow2Height { get { return 0; } }
        Single IPlanSubComponentRenderable.MerchConstraintRow2Width { get { return 0; } }
        Boolean IPlanSubComponentRenderable.IsChest { get { return false; } }
        Single IPlanSubComponentRenderable.Angle { get { return 0; } }
        Single IPlanSubComponentRenderable.Slope { get { return 0; } }
        Single IPlanSubComponentRenderable.Roll { get { return 0; } }

        Int32 IPlanSubComponentRenderable.DividerObstructionFillColour { get { return 0; } }
        PlanogramSubComponentFillPatternType IPlanSubComponentRenderable.DividerObstructionFillPattern { get { return PlanogramSubComponentFillPatternType.Solid; } }

        PlanogramPegHoles IPlanSubComponentRenderable.GetPegHoles()
        {
            return new PlanogramPegHoles(new List<PointValue>(), new List<PointValue>());
        }

        List<RectValue> IPlanSubComponentRenderable.GetDividerLines()
        {
            return new List<RectValue>();
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }
}
