﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25734 : L.Ineson ~ Created.
// V8-26141 : L.Ineson
//  Separated out peghole and notch rendering to instead use meshpartdata.
// V8-27795 : L.Ineson
//  Divider lines now render at the divider width.
// V8-28098 : L.Ineson
//  Made use of new part creation static helpers on PlanSubComponent3DData for
//  notches and divider lines.
// V8-28464 : N.Haywood
//  Changed divider name in SettingsController_PropertyChanged
#endregion

#region Version History : CCM 802
// V8-27233 : A.Kuszyk
//  Added FaceThickness properties to SubComponent_PropertyChanged().
#endregion
#region Version History : CCM820
// V8-30683 : L.Ineson
//  Due to issues with rendering transparency in wpf, riser alpha is ignored for now.
#endregion
#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Rendering;
using Galleria.Framework.Rendering.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

namespace Galleria.Ccm.Editor.Client.Wpf.Common.Rendering
{
    /// <summary>
    /// Implementation of ModelConstruct3DData for a FixtureSubComponent.
    /// </summary>
    public sealed class FixtureSubComponent3DData : ModelConstruct3DData, IPlanPart3DData
    {
        #region Fields

        const Boolean genNormals = false;
        const Boolean genTextures = true;

        private readonly IFixtureRenderSettings _settingsController;
        private readonly IFixtureSubComponentRenderable _subcomponent;

        private ModelConstructPart3DData _subComponentPart;
        private MeshPart3DData _riserPart;
        private MeshPart3DData _dividerLinePart;

        private Boolean _isSelected = false;

        private Boolean _isPartChanged; //flag to indicate that the parts need updating
        private Boolean _isMaterialChanged; // flag to indicate the material needs updating.
        private Boolean _isRiserPartChanged; //flag to indicate that risers need updating;
        private Boolean _isRiserMaterialChanged; //flag to indicate that the riser material needs updating;
        private Boolean _isSlotLinesChanged; // flag to indicate that slotlines need updating.
        private Boolean _isPegholesChanged; // flag to indicate that pegholes need updating.
        private Boolean _isNotchesChanged; //flag to indicate that notches need updating.
        private Boolean _isPlacementChanged; //flag to indicate that the placement needs updating.
        private Boolean _isVisibilityChanged; //flag to indicate that the visibility needs updating.

        private MeshPart3DData _pegHolesPart;
        private MeshPart3DData _notchesPart;

        #endregion

        #region Properties

        private IFixtureRenderSettings SettingsController
        {
            get { return _settingsController; }
        }

        /// <summary>
        /// Returns the source component view.
        /// </summary>
        public IFixtureSubComponentRenderable SubComponent
        {
            get { return _subcomponent; }
        }

        /// <summary>
        /// Returns true if performance intensive updates should be supressed.
        /// </summary>
        public Boolean SupressPerformanceIntensiveUpdates
        {
            get { return _settingsController.SuppressPerformanceIntensiveUpdates; }
        }

        /// <summary>
        /// Gets/Sets whether this model is selected.
        /// </summary>
        public Boolean IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (value != _isSelected)
                {
                    _isSelected = value;
                    OnPropertyChanged("IsSelected");

                    OnMaterialChanged();
                    OnRiserMaterialChanged();
                }
            }
        }

        /// <summary>
        /// Returns the actual subcomponent part data.
        /// </summary>
        public ModelConstructPart3DData SubComponentPart
        {
            get { return _subComponentPart; }
        }

        /// <summary>
        /// Returns an enumerable of the slotline currently loaded for this part.
        /// </summary>
        public MeshPart3DData SlotLinePart
        {
            get { return _dividerLinePart; }
        }

        /// <summary>
        /// Returns an enumerable of the risers currently loaded for this part.
        /// </summary>
        public MeshPart3DData RiserPart
        {
            get { return _riserPart; }
        }

        /// <summary>
        /// Returns true if chest walls should be visible.
        /// (Inherited from the parent Plan3DData)
        /// </summary>
        private Boolean ShowChestWalls
        {
            get { return _settingsController.ShowChestWalls; }
        }

        /// <summary>
        /// Returns true if shelf risers should be visible
        /// (Inherited from the parent Plan3DData)
        /// </summary>
        private Boolean ShowShelfRisers
        {
            get { return _settingsController.ShowShelfRisers; }
        }

        /// <summary>
        /// Returns true if images should be used
        /// </summary>
        private Boolean ShowImages
        {
            get
            {
                if (_settingsController != null)
                {
                    return _settingsController.ShowFixtureImages;
                }
                return true;
            }
        }

        /// <summary>
        /// Returns true if fill patterns will be shown.
        /// </summary>
        private Boolean ShowFillPatterns
        {
            get { return _settingsController.ShowFixtureFillPatterns; }
        }

        /// <summary>
        /// Returns true if peg holes should be shown.
        /// </summary>
        private Boolean ShowPegHoles
        {
            get { return _settingsController.ShowPegHoles; }
        }

        /// <summary>
        /// Returns true if notches should be shown.
        /// </summary>
        private Boolean ShowNotches
        {
            get { return _settingsController.ShowNotches; }
        }

        /// <summary>
        /// Returns true if slot lines should be shown
        /// </summary>
        private Boolean ShowDividerLines
        {
            get { return _settingsController.ShowDividerLines; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="subComponent"></param>
        /// <param name="settingsController"></param>
        public FixtureSubComponent3DData(IFixtureSubComponentRenderable subComponent, IFixtureRenderSettings settingsController)
        {
            _subcomponent = subComponent;
            subComponent.PropertyChanged += SubComponent_PropertyChanged;

            _settingsController = settingsController;
            settingsController.PropertyChanged += SettingsController_PropertyChanged;

            //update position and create parts
            OnPartChanged();

            OnPlacementChanged();

            OnIsUpdatingChanged();
        }

        #endregion

        #region Event Handler

        /// <summary>
        /// Called whenever a property changes on the source IPlanogramSubComponent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SubComponent_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "IsUpdating":
                    OnIsUpdatingChanged();
                    break;

                case "X":
                case "Y":
                case "Z":
                case "Slope":
                case "Angle":
                case "Roll":
                    OnPlacementChanged();
                    break;

                case "Height":
                case "Width":
                case "Depth":
                case "FaceThickness":
                case "FaceThicknessTop":
                case "FaceThicknessBottom":
                case "FaceThicknessLeft":
                case "FaceThicknessRight":
                case "FaceThicknessFront":
                case "FaceThicknessBack":
                case "ShapeType":
                    OnPartChanged();
                    break;

                case "FillColourFront":
                case "FillColourBack":
                case "FillColourTop":
                case "FillColourBottom":
                case "FillColourLeft":
                case "FillColourRight":
                case "TransparencyPercentBack":
                case "TransparencyPercentBottom":
                case "TransparencyPercentFront":
                case "TransparencyPercentLeft":
                case "TransparencyPercentRight":
                case "TransparencyPercentTop":
                    OnMaterialChanged();
                    break;

                case "FillPatternTypeFront":
                case "FillPatternTypeBack":
                case "FillPatternTypeTop":
                case "FillPatternTypeBottom":
                case "FillPatternTypeLeft":
                case "FillPatternTypeRight":
                    if (this.ShowFillPatterns)
                    {
                        OnMaterialChanged();
                    }
                    break;

                case "FrontImageData":
                case "BackImageData":
                case "TopImageData":
                case "BottomImageData":
                case "LeftImageData":
                case "RightImageData":
                    if (this.ShowImages) OnMaterialChanged();
                    break;

                case "RiserColour":
                case "RiserTransparencyPercent":
                    OnRiserMaterialChanged();
                    break;

                case "RiserHeight":
                case "RiserThickness":
                case "IsRiserPlacedOnFront":
                case "IsRiserPlacedOnBack":
                case "IsRiserPlacedOnLeft":
                case "IsRiserPlacedOnRight":
                    OnRiserPartChanged();
                    break;

                case "IsNotchPlacedOnFront":
                case "NotchHeight":
                case "NotchSpacingX":
                case "NotchSpacingY":
                case "NotchStartX":
                case "NotchStartY":
                case "NotchStyleType":
                case "NotchWidth":
                    OnNotchesChanged();
                    break;

                case "MerchConstraintRow1StartX":
                case "MerchConstraintRow1SpacingX":
                case "MerchConstraintRow1StartY":
                case "MerchConstraintRow1SpacingY":
                case "MerchConstraintRow1Height":
                case "MerchConstraintRow1Width":
                case "MerchConstraintRow2StartX":
                case "MerchConstraintRow2SpacingX":
                case "MerchConstraintRow2StartY":
                case "MerchConstraintRow2SpacingY":
                case "MerchConstraintRow2Height":
                case "MerchConstraintRow2Width":
                    OnPegholesChanged();
                    break;

                case "DividerObstructionStartX":
                case "DividerObstructionSpacingX":
                case "DividerObstructionStartY":
                case "DividerObstructionSpacingY":
                case "DividerObstructionStartZ":
                case "DividerObstructionSpacingZ":
                    OnSlotLinesChanged();
                    break;
            }
        }

        /// <summary>
        /// Called when a property changes on the parent plangram 3d data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SettingsController_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "ShowChestWalls":
                case "ShowShelfRisers":
                    OnPartChanged();
                    break;

                case "ShowFixtureImages":
                case "ShowFixtureFillPatterns":
                    OnMaterialChanged();
                    break;

                case "ShowNotches":
                    OnNotchesChanged();
                    break;

                case "ShowPegHoles":
                    OnPegholesChanged();
                    break;

                case "ShowDividerLines":
                    OnSlotLinesChanged();
                    break;

                case "SuppressPerformanceIntensiveUpdates":
                    OnSupressPerformanceIntensiveUpdatesChanged();
                    break;
            }
        }

        /// <summary>
        /// Called whenever the settings controller property value changes
        /// </summary>
        private void OnSupressPerformanceIntensiveUpdatesChanged()
        {
            if (!this.SupressPerformanceIntensiveUpdates)
            {
                if (_isNotchesChanged)
                {
                    OnNotchesChanged();
                }

                if (_isPegholesChanged)
                {
                    OnPegholesChanged();
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the position and rotation of this model.
        /// </summary>
        private void UpdateModelPlacement()
        {
            var subcomponentView = this.SubComponent;

            this.Position = new PointValue
            {
                X = subcomponentView.X,
                Y = subcomponentView.Y,
                Z = subcomponentView.Z
            };

            this.Rotation = new RotationValue
            {
                Angle = subcomponentView.Angle,
                Slope = subcomponentView.Slope,
                Roll = subcomponentView.Roll
            };
        }

        /// <summary>
        /// Updates the visiblity of this model.
        /// </summary>
        private void UpdateVisibility()
        {
            Boolean setIsVisible = true;

            foreach (IModelConstructPart3DData part in this.ModelParts)
            {
                part.IsVisible = setIsVisible;
            }
        }

        /// <summary>
        /// Updates the material used by the parts of this model.
        /// </summary>
        private void UpdateMaterial()
        {
            //if we have no model to colour then just return.
            if (_subComponentPart == null) { return; }

            var subComponent = this.SubComponent;
            var settings = _settingsController;
            ModelConstructPart3DData subPart = _subComponentPart;

            subPart.BeginEdit();
            {
                if (this.IsSelected)
                {
                    subPart.LineThickness = settings.SelectionLineThickness;
                    subPart.LineMaterial = ModelMaterial3D.CreateMaterial(settings.SelectionColour);
                }
                else
                {
                    subPart.LineMaterial = ModelMaterial3D.CreateMaterial(ModelColour.IntToColor(subComponent.LineColour));
                    subPart.LineThickness = subComponent.LineThickness;
                }

                switch (subComponent.ShapeType)
                {
                    default:
                    case Model.FixtureSubComponentShapeType.Box:
                        ((BoxModelPart3DData)subPart).SetMaterials(GetBoxFaceMaterials());
                        break;

                    //case Model.FixtureSubComponentShapeType.Mesh:
                    //    subPart.Material = GetBoxFaceMaterials()[BoxFaceType.Front];
                    //    break;
                }
            }
            subPart.EndEdit();
        }

        /// <summary>
        /// Reloads the parts of this model.
        /// </summary>
        private void UpdateParts()
        {
            var subComponent = this.SubComponent;

            //update the size
            this.Size =
                new WidthHeightDepthValue()
                {
                    Width = subComponent.Width,
                    Height = subComponent.Height,
                    Depth = subComponent.Depth
                };

            ModelConstructPart3DData subPart = null;

            switch (subComponent.ShapeType)
            {
                #region Default / Box

                default:
                case Model.FixtureSubComponentShapeType.Box:
                    {
                        //check if we need to create the model
                        if (_subComponentPart != null && !(_subComponentPart is BoxModelPart3DData))
                        {
                            RemovePart(_subComponentPart);
                            _subComponentPart = null;
                        }
                        if (_subComponentPart == null)
                        {
                            _subComponentPart = new BoxModelPart3DData(this);
                            AddPart(_subComponentPart);
                        }

                        //Begin editing
                        subPart = _subComponentPart;
                        subPart.BeginEdit();



                        BoxModelPart3DData boxSubPart = (BoxModelPart3DData)_subComponentPart;

                        //set face thicknesses.
                        boxSubPart.FrontFaceThickness = subComponent.FaceThicknessFront;
                        boxSubPart.BackFaceThickness = subComponent.FaceThicknessBack;
                        boxSubPart.TopFaceThickness = subComponent.FaceThicknessTop;
                        boxSubPart.BottomFaceThickness = subComponent.FaceThicknessBottom;
                        boxSubPart.LeftFaceThickness = subComponent.FaceThicknessLeft;
                        boxSubPart.RightFaceThickness = subComponent.FaceThicknessRight;


                        //if this is a chest and the settings are set to hide walls
                        // then set all wall values to 0.
                        if (subComponent.IsChest)
                        {
                            if (!this.ShowChestWalls)
                            {
                                boxSubPart.FrontFaceThickness = 0;
                                boxSubPart.BackFaceThickness = 0;
                                boxSubPart.LeftFaceThickness = 0;
                                boxSubPart.RightFaceThickness = 0;
                            }
                        }
                    }
                    break;

                #endregion

                #region Mesh

                //case Model.FixtureSubComponentShapeType.Mesh:
                //    {
                //        //check if we need to create the model
                //        if (_subComponentPart != null && !(_subComponentPart is FileModelPart3DData))
                //        {
                //            RemovePart(_subComponentPart);
                //            _subComponentPart = null;
                //        }
                //        if (_subComponentPart == null)
                //        {
                //            _subComponentPart = new FileModelPart3DData(this);
                //            AddPart(_subComponentPart);
                //        }

                //        //Begin editing
                //        subPart = _subComponentPart;
                //        subPart.BeginEdit();

                //        FileModelPart3DData meshSubPart = (FileModelPart3DData)subPart;
                //        meshSubPart.FileType = FileModelPartType.studio3ds;
                //        meshSubPart.FileData = File.ReadAllBytes(@".\UserData\VWBus.3ds");

                //    }
                //break;

                #endregion
            }

            subPart.Size = this.Size;

            //update related items.
            OnRiserPartChanged();
            OnNotchesChanged();
            OnPegholesChanged();
            OnSlotLinesChanged();
            OnMaterialChanged();
            OnVisibilityChanged();


            subPart.EndEdit();
        }

        #region Risers

        /// <summary>
        /// Updates the riser parts of this subcomponent
        /// </summary>
        private void UpdateRiserModel()
        {
            if (_subComponentPart == null) { return; }

            var subComponent = this.SubComponent;
            WidthHeightDepthValue subComponentSize = this.Size;


            Single riserHeight = subComponent.RiserHeight;
            Single riserThickness = subComponent.RiserThickness;
            if (riserThickness == 0) riserThickness = 1;

            if (this.ShowShelfRisers && riserHeight > 0)
            {
                //NB- Risers are placed outside the size of the subcomponent.
                Boolean generateNormals = false;
                Boolean generateTextures = false;

                MeshBuilderInstructions mb = new MeshBuilderInstructions(generateNormals, generateTextures);

                if (subComponent.IsRiserPlacedOnFront)
                {
                    MeshBuilderInstructions riser = new MeshBuilderInstructions(generateNormals, generateTextures);
                    riser.AddBox(subComponentSize.Width, riserHeight, riserThickness);
                    mb.Append(riser, new PointValue(0, subComponentSize.Height, subComponentSize.Depth));
                }

                if (subComponent.IsRiserPlacedOnBack)
                {
                    MeshBuilderInstructions riser = new MeshBuilderInstructions(generateNormals, generateTextures);
                    riser.AddBox(subComponentSize.Width, riserHeight, riserThickness);
                    mb.Append(riser, new PointValue(0, subComponentSize.Height, -riserThickness));
                }

                if (subComponent.IsRiserPlacedOnLeft)
                {
                    MeshBuilderInstructions riser = new MeshBuilderInstructions(generateNormals, generateTextures);
                    riser.AddBox(riserThickness, riserHeight, subComponentSize.Depth);
                    mb.Append(riser, new PointValue(-riserThickness, subComponentSize.Height, 0));
                }

                if (subComponent.IsRiserPlacedOnRight)
                {
                    MeshBuilderInstructions riser = new MeshBuilderInstructions(generateNormals, generateTextures);
                    riser.AddBox(riserThickness, riserHeight, subComponentSize.Depth);
                    mb.Append(riser, new PointValue(subComponentSize.Width, subComponentSize.Height, 0));
                }


                //add the part in if required.
                if (_riserPart == null)
                {
                    _riserPart = new MeshPart3DData(this);
                    OnRiserMaterialChanged();
                    AddPart(_riserPart);
                }


                MeshPart3DData riserPart = _riserPart;
                riserPart.Builder = mb;
            }
            else if (_riserPart != null)
            {
                RemovePart(_riserPart);
                _riserPart = null;
            }


        }

        /// <summary>
        /// Updates the material of the riser model.
        /// </summary>
        private void UpdateRiserMaterial()
        {
            MeshPart3DData riserPart = _riserPart;
            if (riserPart != null)
            {
                var subComponent = this.SubComponent;

                riserPart.BeginEdit();
                {
                    ModelColour riserColour = new ModelColour() { Alpha = 255/*100*/, Red = 255, Blue = 255, Green = 255 };
                    if (subComponent.RiserColour != 0) riserColour = ModelColour.IntToColor(subComponent.RiserColour);

                    //set transparency
                    if (riserColour != ModelColour.Transparent)
                    {
                        riserPart.IsWireframe = false;

                        riserColour.Alpha = 255;
                        //riserColour.Alpha = Convert.ToByte((subComponent.RiserTransparencyPercent > 0) ?
                        //    255 - Math.Round((255 / 100.0) * subComponent.RiserTransparencyPercent) : 255);
                    }
                    else
                    {
                        //if the part is transparent then draw as wireframe only.
                        riserPart.IsWireframe = true;
                    }

                    riserPart.SetMaterial(riserColour);

                    if (this.IsSelected)
                    {
                        riserPart.LineThickness = _settingsController.SelectionLineThickness;
                        riserPart.LineMaterial = ModelMaterial3D.CreateMaterial(_settingsController.SelectionColour);
                    }
                }
                riserPart.EndEdit();
            }
        }

        #endregion

        #region Dividers

        /// <summary>
        /// Updates the slot line parts of this subcomponent
        /// </summary>
        private void UpdateDividerLines()
        {
            if (_subComponentPart == null) { return; }
            var subComponent = this.SubComponent;

            MeshBuilderInstructions mb = null;

            Boolean hasDividerLines = false;
            if (this.ShowDividerLines)
            {
                mb = PlanSubComponent3DData.CreateDividerLineInstructions(
                    GetFaceThicknesses(),
                     subComponent.DividerObstructionWidth,
                     subComponent.DividerObstructionStartX, subComponent.DividerObstructionSpacingX,
                     subComponent.DividerObstructionStartY, subComponent.DividerObstructionSpacingY,
                     subComponent.DividerObstructionStartZ, subComponent.DividerObstructionSpacingZ,
                     new WidthHeightDepthValue(subComponent.Width, subComponent.Height, subComponent.Depth));

                hasDividerLines = (mb != null);
            }


            if (hasDividerLines)
            {
                //create the part if req
                if (_dividerLinePart == null)
                {
                    MeshPart3DData slotLinesPart = new MeshPart3DData(this);
                    slotLinesPart.CanWireframe = false;
                    slotLinesPart.Builder = mb;
                    slotLinesPart.SetMaterial(ModelColour.White);

                    _dividerLinePart = slotLinesPart;
                    AddPart(_dividerLinePart);
                }
                else
                {
                    _dividerLinePart.Builder = mb;
                }

            }
            else if (_dividerLinePart != null)
            {
                RemovePart(_dividerLinePart);
                _dividerLinePart = null;
            }

        }

        #endregion

        #region Pegholes

        /// <summary>
        /// Updates the peghole model for this subcomponent.
        /// </summary>
        private void UpdatePegholes()
        {
            if (_subComponentPart == null) { return; }
            var subComponent = this.SubComponent;

            //NB: Peg holes are on front facing only.

            List<PointValue> row1PegHoles = new List<PointValue>();
            List<PointValue> row2PegHoles = new List<PointValue>();
            Single pegWidth1 = 0.2f, pegWidth2 = 0.2f;

            Boolean hasPegholes = false;
            if (this.ShowPegHoles && !this.SupressPerformanceIntensiveUpdates)
            {
                FixtureRenderingHelper.GetPegHoles(subComponent, out row1PegHoles, out row2PegHoles, out pegWidth1, out pegWidth2);

                if (row1PegHoles.Count > 0 || row2PegHoles.Count > 0)
                {
                    hasPegholes = true;
                }
            }

            if (hasPegholes)
            {
                MeshPart3DData modelPart = _pegHolesPart;

                //if the part does not exist then create it.
                if (modelPart == null)
                {
                    modelPart = new MeshPart3DData(this);
                    modelPart.Material = new ModelMaterial3D(ModelColour.Black, ModelMaterial3D.FillPatternType.Solid, null);
                    modelPart.Size = _subComponentPart.Size;
                    AddPart(modelPart);
                    _pegHolesPart = modelPart;
                }


                MeshBuilderInstructions meshInstructions = new MeshBuilderInstructions(genNormals, genTextures);

                foreach (var p in row1PegHoles)
                {
                    meshInstructions.AddBoxFaces(new PointValue(p.X, p.Y, modelPart.Size.Depth),
                        pegWidth1, subComponent.MerchConstraintRow1Height, 0.1, BoxFaceType.All);
                }

                foreach (var p in row2PegHoles)
                {
                    meshInstructions.AddBoxFaces(new PointValue(p.X, p.Y, modelPart.Size.Depth),
                        pegWidth2, subComponent.MerchConstraintRow2Height, 0.1, BoxFaceType.All);
                }


                modelPart.Builder = meshInstructions;
            }
            else
            {
                //remove any existing part.
                if (_pegHolesPart != null)
                {
                    RemovePart(_pegHolesPart);
                    _pegHolesPart = null;
                }

                if (this.ShowPegHoles && this.SupressPerformanceIntensiveUpdates)
                {
                    //flag the change.
                    _isPegholesChanged = true;
                }
            }

        }

        #endregion

        #region Notches

        /// <summary>
        /// Updates the notch part for this subcomponent.
        /// </summary>
        private void UpdateNotches()
        {
            if (_subComponentPart == null) { return; }
            var subComponent = this.SubComponent;
            ModelConstructPart3DData subPart = _subComponentPart;


            MeshBuilderInstructions meshInstructions = null;

            Boolean hasNotches = (this.ShowNotches && !this.SupressPerformanceIntensiveUpdates);
            if (hasNotches)
            {
                meshInstructions =
                    PlanSubComponent3DData.CreateNotchPartInstructions(
                    genNormals, genTextures,
                    ToPlanogramSubComponentNotchStyleType(subComponent.NotchStyleType), subComponent.IsNotchPlacedOnFront,
                    subComponent.NotchStartX, subComponent.NotchStartY,
                    subComponent.NotchSpacingX, subComponent.NotchSpacingY,
                    subComponent.NotchWidth, subComponent.NotchHeight,
                    subPart.Size);

                hasNotches = (meshInstructions != null);
            }

            if (hasNotches)
            {
                
                MeshPart3DData modelPart = _notchesPart;

                //if the part does not exist then create it.
                if (modelPart == null)
                {
                    modelPart = new MeshPart3DData(this);
                    modelPart.Material = new ModelMaterial3D(ModelColour.Black, ModelMaterial3D.FillPatternType.Solid, null);
                    modelPart.Size = _subComponentPart.Size;
                    AddPart(modelPart);
                    _notchesPart = modelPart;
                }

                modelPart.Builder = meshInstructions;
            }
            else
            {
                //remove any existing part
                if (_notchesPart != null)
                {
                    RemovePart(_notchesPart);
                    _notchesPart = null;
                }

                if (this.ShowNotches && this.SupressPerformanceIntensiveUpdates)
                {
                    //flag the change
                    _isNotchesChanged = true;
                }
            }
        }

        private static PlanogramSubComponentNotchStyleType ToPlanogramSubComponentNotchStyleType(FixtureSubComponentNotchStyleType value)
        {
            switch (value)
            {
                case FixtureSubComponentNotchStyleType.DoubleCircle: return PlanogramSubComponentNotchStyleType.DoubleCircle;
                case FixtureSubComponentNotchStyleType.DoubleRectangle: return PlanogramSubComponentNotchStyleType.DoubleRectangle;
                case FixtureSubComponentNotchStyleType.DoubleSquare: return PlanogramSubComponentNotchStyleType.DoubleSquare;
                case FixtureSubComponentNotchStyleType.SingleCircle: return PlanogramSubComponentNotchStyleType.SingleCircle;
                case FixtureSubComponentNotchStyleType.SingleH: return PlanogramSubComponentNotchStyleType.SingleH;
                case FixtureSubComponentNotchStyleType.SingleRectangle: return PlanogramSubComponentNotchStyleType.SingleRectangle;
                case FixtureSubComponentNotchStyleType.SingleSquare: return PlanogramSubComponentNotchStyleType.SingleSquare;

                default:
                    Debug.Fail("FixtureSubComponentNotchStyleType not handled");
                    return PlanogramSubComponentNotchStyleType.SingleRectangle;
            }
        }

        #endregion

        /// <summary>
        /// Returns an array of facing materials for this subcomponent.
        /// </summary>
        /// <returns></returns>
        private Dictionary<BoxFaceType, ModelMaterial3D> GetBoxFaceMaterials()
        {
            IFixtureSubComponentRenderable subComponent = this.SubComponent;

            Byte[] frontImageData = null;
            Byte[] backImageData = null;
            Byte[] topImageData = null;
            Byte[] bottomImageData = null;
            Byte[] leftImageData = null;
            Byte[] rightImageData = null;

            if (this.ShowImages)
            {
                frontImageData = subComponent.FrontImageData;
                backImageData = subComponent.BackImageData;
                topImageData = subComponent.TopImageData;
                bottomImageData = subComponent.BottomImageData;
                leftImageData = subComponent.LeftImageData;
                rightImageData = subComponent.RightImageData;
            }

            var frontPattern = ModelMaterial3D.FillPatternType.Solid;
            var backPattern = ModelMaterial3D.FillPatternType.Solid;
            var topPattern = ModelMaterial3D.FillPatternType.Solid;
            var bottomPattern = ModelMaterial3D.FillPatternType.Solid;
            var leftPattern = ModelMaterial3D.FillPatternType.Solid;
            var rightPattern = ModelMaterial3D.FillPatternType.Solid;

            if (this.ShowFillPatterns)
            {
                frontPattern = ToFillPatternType(subComponent.FillPatternTypeFront);
                backPattern = ToFillPatternType(subComponent.FillPatternTypeBack);
                topPattern = ToFillPatternType(subComponent.FillPatternTypeTop);
                bottomPattern = ToFillPatternType(subComponent.FillPatternTypeBottom);
                leftPattern = ToFillPatternType(subComponent.FillPatternTypeLeft);
                rightPattern = ToFillPatternType(subComponent.FillPatternTypeRight);
            }


            Int32 transparency;

            ModelColour frontColour = ModelColour.IntToColor(subComponent.FillColourFront);
            transparency = subComponent.TransparencyPercentFront;
            frontColour.Alpha = Convert.ToByte((transparency > 0) ? 255 - Math.Round((255 / 100.0) * transparency) : 255);

            ModelColour backColour = ModelColour.IntToColor(subComponent.FillColourBack);
            transparency = subComponent.TransparencyPercentBack;
            backColour.Alpha = Convert.ToByte((transparency > 0) ? 255 - Math.Round((255 / 100.0) * transparency) : 255);

            ModelColour topColour = ModelColour.IntToColor(subComponent.FillColourTop);
            transparency = subComponent.TransparencyPercentTop;
            topColour.Alpha = Convert.ToByte((transparency > 0) ? 255 - Math.Round((255 / 100.0) * transparency) : 255);

            ModelColour bottomColour = ModelColour.IntToColor(subComponent.FillColourBottom);
            transparency = subComponent.TransparencyPercentBottom;
            bottomColour.Alpha = Convert.ToByte((transparency > 0) ? 255 - Math.Round((255 / 100.0) * transparency) : 255);

            ModelColour leftColour = ModelColour.IntToColor(subComponent.FillColourLeft);
            transparency = subComponent.TransparencyPercentLeft;
            leftColour.Alpha = Convert.ToByte((transparency > 0) ? 255 - Math.Round((255 / 100.0) * transparency) : 255);

            ModelColour rightColour = ModelColour.IntToColor(subComponent.FillColourRight);
            transparency = subComponent.TransparencyPercentRight;
            rightColour.Alpha = Convert.ToByte((transparency > 0) ? 255 - Math.Round((255 / 100.0) * transparency) : 255);


            Dictionary<BoxFaceType, ModelMaterial3D> faceMaterials = new Dictionary<BoxFaceType, ModelMaterial3D>()
                {
                    {BoxFaceType.Front, new ModelMaterial3D(frontColour, frontPattern, frontImageData)},
                    {BoxFaceType.Back, new ModelMaterial3D(backColour, backPattern, backImageData)},
                    {BoxFaceType.Top, new ModelMaterial3D(topColour, topPattern, topImageData)},
                    {BoxFaceType.Bottom, new ModelMaterial3D(bottomColour, bottomPattern, bottomImageData)},
                    {BoxFaceType.Left, new ModelMaterial3D(leftColour, leftPattern, leftImageData)},
                    {BoxFaceType.Right, new ModelMaterial3D(rightColour, rightPattern, rightImageData)}
                };



            return faceMaterials;
        }

        private static ModelMaterial3D.FillPatternType ToFillPatternType(FixtureSubComponentFillPatternType prodFillPattern)
        {
            switch (prodFillPattern)
            {
                case FixtureSubComponentFillPatternType.Solid: return ModelMaterial3D.FillPatternType.Solid;
                case FixtureSubComponentFillPatternType.Border: return ModelMaterial3D.FillPatternType.Border;
                case FixtureSubComponentFillPatternType.Crosshatch: return ModelMaterial3D.FillPatternType.Crosshatch;
                case FixtureSubComponentFillPatternType.DiagonalDown: return ModelMaterial3D.FillPatternType.DiagonalDown;
                case FixtureSubComponentFillPatternType.DiagonalUp: return ModelMaterial3D.FillPatternType.DiagonalUp;
                case FixtureSubComponentFillPatternType.Horizontal: return ModelMaterial3D.FillPatternType.Horizontal;
                case FixtureSubComponentFillPatternType.Vertical: return ModelMaterial3D.FillPatternType.Vertical;
                case FixtureSubComponentFillPatternType.Dotted: return ModelMaterial3D.FillPatternType.Dotted;

                default:
                    Debug.Fail("Product Fill Pattern type not handled");
                    return ModelMaterial3D.FillPatternType.Solid;
            }
        }

        /// <summary>
        /// Returns a dictionary of resolved face thicknesses.
        /// </summary>
        /// <returns></returns>
        private Dictionary<BoxFaceType, Single> GetFaceThicknesses()
        {
            var sub = this.SubComponent;

            var faceThicknesses = new Dictionary<BoxFaceType, Single>()
                {
                    {BoxFaceType.Front, Math.Max(0, sub.FaceThicknessFront)},
                    {BoxFaceType.Back, Math.Max(0,sub.FaceThicknessBack)},
                    {BoxFaceType.Top, Math.Max(0,sub.FaceThicknessTop)},
                    {BoxFaceType.Bottom, Math.Max(0,sub.FaceThicknessBottom)},
                    {BoxFaceType.Left, Math.Max(0,sub.FaceThicknessLeft)},
                    {BoxFaceType.Right, Math.Max(0,sub.FaceThicknessRight)}
                };


            //if all values are over 0 then none will be applied so just reset them.
            if (faceThicknesses.Values.All(f => f > 0))
            {
                foreach (var key in faceThicknesses.Keys.ToList())
                {
                    faceThicknesses[key] = 0;
                }
            }

            return faceThicknesses;
        }

        #endregion

        #region IUpdatable Members

        private Boolean _isUpdating;

        private void BeginUpdate()
        {
            _isUpdating = true;
        }

        private void EndUpdate()
        {
            _isUpdating = false;

            if (_isPartChanged)
            {
                OnPartChanged();
            }
            if (_isMaterialChanged)
            {
                OnMaterialChanged();
            }

            if (_isRiserPartChanged)
            {
                OnRiserPartChanged();
            }
            if (_isRiserMaterialChanged)
            {
                OnRiserMaterialChanged();
            }

            if (_isNotchesChanged)
            {
                OnNotchesChanged();
            }

            if (_isPegholesChanged)
            {
                OnPegholesChanged();
            }

            if (_isSlotLinesChanged)
            {
                OnSlotLinesChanged();
            }

            if (_isPlacementChanged)
            {
                OnPlacementChanged();
            }

            if (_isVisibilityChanged)
            {
                OnVisibilityChanged();
            }

        }

        private void OnIsUpdatingChanged()
        {
            if (this.SubComponent.IsUpdating && !_isUpdating)
            {
                BeginUpdate();
            }
            else if (!this.SubComponent.IsUpdating && _isUpdating)
            {
                EndUpdate();
            }
        }

        #region OnChanged Methods

        private void OnPartChanged()
        {
            if (!_isUpdating)
            {
                _isPartChanged = false;
                UpdateParts();
            }
            else
            {
                //flag the change
                _isPartChanged = true;
            }
        }

        private void OnMaterialChanged()
        {
            if (!_isUpdating)
            {
                _isMaterialChanged = false;
                UpdateMaterial();
            }
            else
            {
                //flag the change
                _isMaterialChanged = true;
            }
        }

        private void OnRiserPartChanged()
        {
            if (!_isUpdating)
            {
                _isRiserPartChanged = false;
                UpdateRiserModel();
            }
            else
            {
                //flag the change
                _isRiserPartChanged = true;
            }
        }

        private void OnRiserMaterialChanged()
        {
            if (!_isUpdating)
            {
                _isRiserMaterialChanged = false;
                UpdateRiserMaterial();
            }
            else
            {
                //flag the change
                _isRiserMaterialChanged = true;
            }
        }

        private void OnNotchesChanged()
        {
            if (!_isUpdating)
            {
                _isNotchesChanged = false;
                UpdateNotches();
            }
            else
            {
                //flag the change
                _isNotchesChanged = true;
            }
        }

        private void OnPegholesChanged()
        {
            if (!_isUpdating)
            {
                _isPegholesChanged = false;
                UpdatePegholes();
            }
            else
            {
                //flag the change
                _isPegholesChanged = true;
            }
        }

        private void OnSlotLinesChanged()
        {
            if (!_isUpdating)
            {
                _isSlotLinesChanged = false;
                UpdateDividerLines();
            }
            else
            {
                //flag the change
                _isSlotLinesChanged = true;
            }
        }

        private void OnPlacementChanged()
        {
            if (!_isUpdating)
            {
                _isPlacementChanged = false;
                UpdateModelPlacement();
            }
            else
            {
                //flag the change
                _isPlacementChanged = true;
            }
        }

        private void OnVisibilityChanged()
        {
            if (!_isUpdating)
            {
                _isVisibilityChanged = false;
                UpdateVisibility();
            }
            else
            {
                //flag the change
                _isVisibilityChanged = true;
            }
        }

        #endregion

        #endregion

        #region IPlanPart3DData Members

        public Object PlanPart
        {
            get { return _subcomponent; }
        }

        void IPlanPart3DData.PauseUpdates()
        {

        }

        void IPlanPart3DData.UnpauseUpdates()
        {

        }


        #endregion

        #region IDisposable Members

        public override void Dispose()
        {
            if (!IsDisposed)
            {
                _subcomponent.PropertyChanged -= SubComponent_PropertyChanged;
                _settingsController.PropertyChanged -= SettingsController_PropertyChanged;


                GC.SuppressFinalize(this);
                IsDisposed = true;
            }
        }

        #endregion
    }
}
