#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26368 : A.Silva ~ Created.
// V8-26633 : A.Silva ~ Added localization strings to PlacementFilterTypeHelper.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    ///     Enumeration of possible values to perform placement count based filtering.
    /// </summary>
    public enum PlacementFilterType
    {
        /// <summary>
        ///     Do not filter by placement count and show all products.
        /// </summary>
        All,

        /// <summary>
        ///     Filter by placement count and show only placed products.
        /// </summary>
        Placed,

        /// <summary>
        ///     Filter by placement count and show only unplaced products.
        /// </summary>
        Unplaced
    }

    /// <summary>
    ///     Helper class for the <see cref="PlacementFilterType"/> enumeration.
    /// </summary>
    public static class PlacementFilterTypeHelper
    {
        /// <summary>
        ///     A dictionary of <see cref="PlacementFilterType"/> values and their Friendly Name <see cref="String"/>.
        /// </summary>
        public static readonly Dictionary<PlacementFilterType, String> FriendlyNames = new Dictionary
            <PlacementFilterType, String>
        {
            {PlacementFilterType.All, Message.Enum_PlacementFilterType_All},
            {PlacementFilterType.Placed, Message.Enum_PlacementFilterType_Placed},
            {PlacementFilterType.Unplaced, Message.Enum_PlacementFilterType_Unplaced}
        };
    }
}