﻿using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Interaction logic for QuickItemsSelectDialog.xaml
    /// </summary>
    public partial class QuickItemsSelectDialog : ExtendedRibbonWindow
    {
        #region Properties

        #region ItemSource property

        public static readonly DependencyProperty ItemSourceProperty =
            DependencyProperty.Register("ItemSource", typeof(IEnumerable), typeof(QuickItemsSelectDialog));

        /// <summary>
        /// Gets/Sets the item source for the available items
        /// </summary>
        public IEnumerable ItemSource
        {
            get { return (IEnumerable)GetValue(ItemSourceProperty); }
            set { SetValue(ItemSourceProperty, value); }
        }

        #endregion

        #region SelectedItems property

        public static readonly DependencyProperty SelectedItemsProperty =
            DependencyProperty.Register("SelectedItems", typeof(IEnumerable), typeof(QuickItemsSelectDialog));

        /// <summary>
        /// Gets/Sets the selected item
        /// </summary>
        public IEnumerable SelectedItems
        {
            get { return (IEnumerable)GetValue(SelectedItemsProperty); }
            private set { SetValue(SelectedItemsProperty, value); }
        }

        #endregion

        #region ColumnSet property

        public static readonly DependencyProperty ColumnSetProperty =
            DependencyProperty.Register("ColumnSet", typeof(DataGridColumnCollection), typeof(QuickItemsSelectDialog));

        /// <summary>
        /// Gets/Sets the columnset to use to display the items
        /// </summary>
        public DataGridColumnCollection ColumnSet
        {
            get { return (DataGridColumnCollection)GetValue(ColumnSetProperty); }
            set { SetValue(ColumnSetProperty, value); }
        }


        #endregion

        #region SelectionMode property

        public static readonly DependencyProperty SelectionModeProperty =
            DependencyProperty.Register("SelectionMode", typeof(DataGridSelectionMode), typeof(QuickItemsSelectDialog),
            new PropertyMetadata(DataGridSelectionMode.Extended));

        /// <summary>
        /// Gets/Sets the selection mode
        /// </summary>
        public DataGridSelectionMode SelectionMode
        {
            get { return (DataGridSelectionMode)GetValue(SelectionModeProperty); }
            set { SetValue(SelectionModeProperty, value); }
        }

        #endregion

        #region ExtendColumn property

        public static readonly DependencyProperty ExtendColumnProperty =
            DependencyProperty.Register("ExtendColumn", typeof(Int32), typeof(QuickItemsSelectDialog),
            new PropertyMetadata(0));

        /// <summary>
        /// Gets/Sets the extend column
        /// </summary>
        public Int32 ExtendColumn
        {
            get { return (Int32)GetValue(ExtendColumnProperty); }
            set { SetValue(ExtendColumnProperty, value); }
        }

        #endregion

        #region DefaultGroupBy property

        public static readonly DependencyProperty DefaultGroupByProperty =
            DependencyProperty.Register("DefaultGroupBy", typeof(String), typeof(QuickItemsSelectDialog));

        /// <summary>
        /// Gets/Sets the default column to group by
        /// </summary>
        public String DefaultGroupBy
        {
            get { return (String)GetValue(DefaultGroupByProperty); }
            set { SetValue(DefaultGroupByProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public QuickItemsSelectDialog()
        {
            InitializeComponent();

            this.Loaded += QuickItemsSelectDialog_Loaded;
        }

        #endregion

        #region Event Handlers

        private void QuickItemsSelectDialog_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= QuickItemsSelectDialog_Loaded;

            CreateDefaultGroupByDescription();
        }

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            CommitSelection();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void itemsGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            CommitSelection();
        }


        protected override void OnPreviewKeyDown(System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                CommitSelection();
                e.Handled = true;
            }

            base.OnPreviewKeyUp(e);
        }

        #endregion

        #region Methods

        private void CommitSelection()
        {
            this.SelectedItems = this.itemsGrid.SelectedItems;
            this.DialogResult = true;
        }

        /// <summary>
        /// Creates a default group by on the items grid
        /// </summary>
        private void CreateDefaultGroupByDescription()
        {
            if (!String.IsNullOrEmpty(this.DefaultGroupBy))
            {
                itemsGrid.GroupByDescriptions.Add(new PropertyGroupDescription(this.DefaultGroupBy));
            }
        }

        #endregion
    }
}
