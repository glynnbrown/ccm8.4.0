﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Contains helper methods for dealing with product images
    /// </summary>
    internal static class ProductImageHelper
    {
        /// <summary>
        ///     Stores the images in the given collection of <paramref name="productViews"/> showing a modal busy window.
        /// </summary>
        /// <param name="productViews">The collection of product views to store images for.</param>
        internal static void StoreExternalImages(IEnumerable<PlanogramProductView> productViews)
        {
            IRealImageProvider realImageProvider = PlanogramImagesHelper.RealImageProvider;
            if (realImageProvider == null) return;

            Boolean isRunning = realImageProvider.IsRunning;
            realImageProvider.PauseFetchAsync();

            try
            {
                StoreExternalImagesWork work = new StoreExternalImagesWork(productViews);
                CommonHelper.GetModalBusyWorkerService().RunWorker(work);
            }
            finally
            {
                realImageProvider.ContinueFetchAsync();
            }
        }

        /// <summary>
        ///     Fetches an external image and returns a callback to execute in the UI thread so that the image is assigned.
        /// </summary>
        /// <param name="view">The view that the external image is for.</param>
        /// <param name="facingType">The facing type of the required external image.</param>
        /// <param name="imageType">The image type of the required external image.</param>
        /// <returns>A callback action to be invoked from the UI thread.</returns>
        private static Action FetchExternalImage(PlanogramProductViewModelBase view, PlanogramImageFacingType facingType,
            PlanogramImageType imageType)
        {
            String imageTypeName = (imageType == PlanogramImageType.None
                                        ? String.Empty
                                        : imageType.ToString());
            String facingTypeName = facingType.ToString();
            String propertyName = String.Format("PlanogramImageId{0}{1}", imageTypeName, facingTypeName);
            String imagePropertyName = String.Format("{0}{1}Image", facingTypeName, imageTypeName);

            //  Get the PropertyInfo for the PlanogramImageId reference in the PlanogramProduct.
            //  If there is none, there is no UI action required.
            PropertyInfo idPropertyInfo =
                typeof(PlanogramProduct).GetProperties().FirstOrDefault(p => p.Name == propertyName);
            if (idPropertyInfo == null) return null;

            //  Get the PropertyInfo for the PlanogramImage referece in the PlanogramProductView.
            //  If there is none, there is no UI action required.
            PropertyInfo imagePropertyInfo =
                typeof(PlanogramProductView).GetProperties().FirstOrDefault(p => p.Name == imagePropertyName);
            if (imagePropertyInfo == null) return null;

            //  Get the current PlanogramImageId referenced in the Planogram Product.
            //  If there is one the image is already stored, there is no UI action required.
            Object propertyValue = idPropertyInfo.GetValue(view.Model, null);
            if (propertyValue != null) return null;

            //  Get the current PlanogramImage.
            PlanogramImage image = imagePropertyInfo.GetValue(view, null) as PlanogramImage;

            //  If there is a placeholder PlanogramImage try to get the image right away.
            if (image != null &&
                image.ImageData == null)
                image = PlanogramImagesHelper.GetRealImage(view.Model, facingType, imageType);

            //  If there is no real image, there is no UI action required.
            if (image == null ||
                image.ImageData == null)
                return null;

            //  Return the action to perform on the UI thread.
            return () =>
            {
                PlanogramImageList images = view.Model.Parent.Images;
                //  Get a copy of the PlanogramImage, we do not want any particular 
                //  PlanogramProductView grabbing a hold of the cache instance.
                PlanogramImage planogramImage = image.Copy();
                if (images != null &&
                    images.All(o => o.FileName != planogramImage.FileName))
                    images.Add(planogramImage);
                imagePropertyInfo.SetValue(view, planogramImage, null);
            };
        }


        /// <summary>
        ///     Enumerates the pending Fetch External Image Actions for the product view.
        /// </summary>
        /// <returns></returns>
        internal static IEnumerable<Func<Action>> EnumerateExternalImageFetchActions(PlanogramProductView productView)
        {
            if (productView.Model == null) yield break;

            foreach (PlanogramImageFacingType currentFacingType in Enum.GetValues(typeof(PlanogramImageFacingType)))
            {
                PlanogramImageFacingType thisFacingType = currentFacingType;
                foreach (PlanogramImageType currentImageType in Enum.GetValues(typeof(PlanogramImageType)))
                {
                    PlanogramImageType thisImageType = currentImageType;
                    yield return (() => FetchExternalImage(productView, thisFacingType, thisImageType));
                }
            }
        }


        #region Nested Classes

        /// <summary>
        ///     Worker to store external images whilst showing a modal busy.
        /// </summary>
        private sealed class StoreExternalImagesWork : IModalBusyWork
        {
            private readonly Queue<Func<Action>> _fetchExternalImageActions;
            private Int32 _itemCount;

            #region Properties

            public Boolean ReportsProgress
            {
                get { return true; }
            }

            public Boolean SupportsCancellation
            {
                get { return true; }
            }

            #endregion

            #region Constructor

            public StoreExternalImagesWork(IEnumerable<PlanogramProductView> productViews)
            {
                _fetchExternalImageActions =
                    new Queue<Func<Action>>(productViews.SelectMany(pv => ProductImageHelper.EnumerateExternalImageFetchActions(pv)));
            }

            #endregion

            #region Methods

            public Object GetWorkerArgs()
            {
                // No worker args to pass on.
                return null;
            }

            public void OnDoWork(IModalBusyWorkerService sender, DoWorkEventArgs e)
            {
                if (_fetchExternalImageActions == null ||
                    _fetchExternalImageActions.Count == 0) return;

                // Determine the percentage per item.
                Double itemPercent = 100 / (Double)(_fetchExternalImageActions.Count);
                Double curPecent = 0;

                while (!sender.CancellationPending &&
                       _fetchExternalImageActions.Count > 0)
                {
                    Func<Action> fetchExternaImageAction = _fetchExternalImageActions.Dequeue();

                    //  Fetch the image.
                    Action uiCallBack = fetchExternaImageAction.Invoke();

                    //  Report progress.
                    curPecent += itemPercent;
                    if (uiCallBack != null) _itemCount++;
                    sender.ReportProgress((Int32)curPecent, uiCallBack);
                }
                if (sender.CancellationPending) e.Cancel = true;
            }


            public void OnProgressChanged(IModalBusyWorkerService sender, ProgressChangedEventArgs e)
            {
                //  Update the busy description with the current values.
                CommonHelper.GetWindowService().UpdateBusy(
                    String.Format(Message.StoreExternalImagesWork_ModalBusy_ProgressDescription, Environment.NewLine,
                        _itemCount), e.ProgressPercentage);

                //  Invoke the UI callback if there is one pending.
                //  NB: This ensures that code that needs to run on the UI thread is run there.
                Action uiCallBack = e.UserState as Action;
                if (uiCallBack != null) uiCallBack.Invoke();
            }

            public void OnWorkCompleted(IModalBusyWorkerService sender, RunWorkerCompletedEventArgs e)
            {
                if (e.Cancelled)
                {
                    //  Show the cancelled message to the user.
                    CommonHelper.GetWindowService().ShowOkMessage(MessageWindowType.Warning,
                        Message.StoreExternalImagesWork_ModalBusy_CancelledHeader,
                        String.Format(Message.StoreExternalImagesWork_ModalBusy_CancelledDescription, _itemCount));
                }
                else if (e.Error != null)
                {
                    //  Show the error message to the user.
                    CommonHelper.GetWindowService().ShowErrorMessage(
                        Message.StoreExternalImagesWork_ModalBusy_ErrorHeader, e.Error.Message);
                }
                else
                {
                    //  Show the completed message to the user.
                    CommonHelper.GetWindowService().ShowOkMessage(MessageWindowType.Info,
                        Message.StoreExternalImagesWork_ModalBusy_OkHeader,
                        String.Format(Message.StoreExternalImagesWork_ModalBusy_OkDescription, _itemCount));
                }
            }

            /// <summary>
            ///     Invoked after the work has started asynchronously so that the modal busy dialog is displayed.
            /// </summary>
            public void ShowModalBusy(IModalBusyWorkerService sender, EventHandler cancelEventHandler)
            {
                //  Setup the modal busy dialog values.
                IWindowService service = CommonHelper.GetWindowService();
                String header = String.Format(Message.StoreExternalImagesWork_ModalBusy_Header, Environment.NewLine);
                String desc = String.Format(Message.StoreExternalImagesWork_ModalBusy_Description);

                //  Show the dialog.
                service.ShowDeterminateBusy(header, desc, cancelEventHandler);
            }

            #endregion
        }

        #endregion
    }
}
