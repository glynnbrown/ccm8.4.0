﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.3.0)
//V8-32600 : L.Ineson
//  Created.
// V8-32955 : L.Ineson
//  Corrected CanAddToFixtureLibrary to allow multiple fixtures.
#endregion

#endregion

using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.FixtureLibrary;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Controls.Wpf.Rendering;
using Galleria.Framework.Planograms.Controls.Wpf.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Rendering;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    /// Provides helper methods for operations related to the fixture library
    /// </summary>
    public static class FixtureLibraryHelper
    {
        /// <summary>
        /// Creates a new fixture package from the given plan items.
        /// </summary>
        /// <param name="planItems"></param>
        /// <returns></returns>
        public static FixturePackage CreateFixturePackage(IEnumerable<IPlanItem> planItems)
        {
            FixturePackage fixturePackage = null;

            switch (planItems.ElementAt(0).PlanItemType)
            {
                case PlanItemType.Component:
                    fixturePackage = FixturePackage.NewFixturePackage(planItems.ElementAt(0).Component.FixtureComponentModel);
                    break;

                case PlanItemType.Assembly:
                    fixturePackage = FixturePackage.NewFixturePackage(planItems.ElementAt(0).Assembly.FixtureAssemblyModel);
                    break;

                case PlanItemType.Fixture:
                    if (planItems.Count() > 1 && planItems.All(i => i.PlanItemType == PlanItemType.Fixture))
                    {
                        fixturePackage = FixturePackage.NewFixturePackage(planItems.Select(i => i.Fixture.FixtureItemModel));
                    }
                    else
                    {
                        fixturePackage = FixturePackage.NewFixturePackage(planItems.ElementAt(0).Fixture.FixtureItemModel);
                    }
                    break;
            }

            fixturePackage.ThumbnailImageData = CreateFixtureThumbnail(planItems);

            return fixturePackage;
        }

        /// <summary>
        /// Creates the data for the fixture thumbnail.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private static Byte[] CreateFixtureThumbnail(IEnumerable<IPlanItem> items)
        {
            const Double imgLength = 150D;

            PlanRenderSettings settings = new PlanRenderSettings()
            {
                ShowShelfRisers = true,
                RotateTopDownComponents = false,
                ShowChestWalls = true,
                ShowPositions = false,
                ShowPegHoles = true,
                ShowNotches = true,
                ShowFixtureImages = true,
                ShowAnnotations = true
            };


            ModelVisual3D model = null;
            switch (items.First().PlanItemType)
            {
                case PlanItemType.Component:
                    model = new ModelConstruct3D(new PlanComponent3DData(items.First().Component, settings));
                    break;

                case PlanItemType.Assembly:
                    model = new ModelConstruct3D(new PlanAssembly3DData(items.First().Assembly, settings));
                    break;

                case PlanItemType.Fixture:
                    if (items.Count() > 1)
                    {
                        ModelVisual3D parentModel = new ModelVisual3D();
                        foreach (IPlanItem item in items)
                        {
                            parentModel.Children.Add(new ModelConstruct3D(new PlanFixture3DData(item.Fixture, settings)));
                        }
                        model = parentModel;
                    }
                    else
                    {
                        model = new ModelConstruct3D(new PlanFixture3DData(items.First().Fixture, settings));
                    }
                    break;

            }

            if (!App.IsUnitTesting && model != null)
            {
                Grid render = new Grid();
                render.Height = imgLength;
                render.Width = imgLength;

                PerspectiveCamera cam = new PerspectiveCamera();
                cam.UpDirection = new Vector3D(0, 1, 0);
                cam.LookDirection = new Vector3D(0, 0, -244);
                cam.FieldOfView = 60;

                HelixToolkit.Wpf.HelixViewport3D viewport = new HelixToolkit.Wpf.HelixViewport3D()
                {
                    Background = Brushes.White,
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                    VerticalAlignment = VerticalAlignment.Stretch,
                    ModelUpDirection = new Vector3D(0, 1, 0),
                    ShowCoordinateSystem = false,
                    ShowViewCube = false,
                    Camera = cam
                };
                render.Children.Add(viewport);
                viewport.Children.Add(new HelixToolkit.Wpf.SunLight());
                viewport.Children.Add(model);

                render.Arrange(new Rect(new Size(imgLength, imgLength)));

                PlanCameraHelper.SetupForCameraType(viewport, CameraViewType.Front, false, false);

                viewport.ZoomExtents();

                // Scale dimensions from 96 dpi to get better quality
                Double scale = 200 / 96;

                //perform a straight render.
                RenderTargetBitmap renderTargetBitmap =
                    new RenderTargetBitmap(
                    (Int32)(scale * imgLength),
                    (Int32)(scale * imgLength),
                    scale * 96,
                    scale * 96,
                    PixelFormats.Default);
                renderTargetBitmap.Render(render);



                // encode
                using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
                {
                    PngBitmapEncoder encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(renderTargetBitmap));
                    encoder.Save(memoryStream);

                    // Export the stream to a byte array
                    Byte[] renderBytes = CommonHelper.CreateImageBlob(memoryStream, true);

                    return renderBytes;
                }
            }
            else
            {
                return new Byte[0];
            }
        }

        /// <summary>
        /// Adds the given package to the given planogram.
        /// </summary>
        /// <param name="package"></param>
        /// <param name="nearestFixture"></param>
        /// <returns></returns>
        public static List<IPlanItem> AddToPlanogram(FixturePackage package, PlanogramView plan, PlanogramFixtureView nearestFixture)
        {
            List<IPlanItem> addedItems = new List<IPlanItem>();

            PlanogramFixtureItem nearestFixtureModel = (nearestFixture != null) ? nearestFixture.FixtureItemModel : null;


            FixturePackageHelper.AddToPlanogramResult result = 
                FixturePackageHelper.AddToPlanogram(package, plan.Model, nearestFixtureModel);

            if (result.AddedFixtureItems != null)
            {
                foreach (PlanogramFixtureItem addedFixtureItem in result.AddedFixtureItems)
                {
                    PlanogramFixtureView addedFixtureView = plan.Fixtures.FirstOrDefault(f => Object.Equals(f.FixtureItemModel, addedFixtureItem));
                    if (addedFixtureView != null)
                    {
                        addedItems.Add(addedFixtureView);
                    }
                }
            }
            else if (result.AddedFixtureAssembly != null)
            {
                PlanogramAssemblyView addedAssemblyView = plan.EnumerateAllAssemblies().FirstOrDefault(f => Object.Equals(f.FixtureAssemblyModel, result.AddedFixtureAssembly));
                if (addedAssemblyView != null)
                {
                    addedItems.Add(addedAssemblyView);
                }
            }
            else if (result.AddedFixtureComponent != null)
            {
                PlanogramComponentView addedComponentView = plan.EnumerateAllComponents().FirstOrDefault(f => Object.Equals(f.FixtureComponentModel, result.AddedFixtureComponent));
                if (addedComponentView != null)
                {
                    addedItems.Add(addedComponentView);
                }
            }

            return addedItems;
        }

        /// <summary>
        /// Deletes the fixture package for the given info.
        /// </summary>
        /// <param name="info"></param>
        public static void DeleteFixturePackage(FixturePackageInfo info)
        {
            try
            {
                switch (info.PackageType)
                {
                    case FixturePackageType.FileSystem:
                        {
                            FixturePackage package = FixturePackage.FetchByFileName(info.FileName);
                            package.Delete();
                            package = package.Save();
                            package.Dispose();
                        }
                        break;

                    default:
                    case FixturePackageType.Unknown:
                        {
                            FixturePackage package = FixturePackage.FetchById(info.Id);
                            package.Delete();
                            package = package.Save();
                            package.Dispose();
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                CommonHelper.RecordException(ex);
                CommonHelper.GetWindowService().ShowErrorOccurredMessage(info.FileName, OperationType.Delete);
            }
        }

        /// <summary>
        /// Deletes the fixture package for the given info.
        /// </summary>
        /// <param name="info"></param>
        public static void DeleteFixturePackage(String fileName)
        {
            try
            {
                FixturePackage package = FixturePackage.FetchByFileName(fileName);
                package.Delete();
                package = package.Save();
                package.Dispose();
            }
            catch (Exception ex)
            {
                CommonHelper.RecordException(ex);
                CommonHelper.GetWindowService().ShowErrorOccurredMessage(fileName, OperationType.Delete);
            }
        }

        /// <summary>
        /// Returns the type of fixture library we are connected to.
        /// </summary>
        /// <returns></returns>
        public static FixturePackageType GetConnectedFixturePackageType()
        {
            //Always save to file system atm.
            return FixturePackageType.FileSystem;
        }

        public static Boolean AddFixtureLibraryPromptUserForValues(String title, String forceFirstShowDescription,
           String notUniqueDescription, Boolean forceFirstShow,
           Predicate<String> isUniqueCheck, String proposedValue, String proposedFolderPath, out String selectedNameValue, out String selectedFolderPath,
           FixturePackage package)
        {
            Boolean wasCancelled = false;
            String currentValue = proposedValue;
            String newFolderPath = proposedFolderPath;

            Boolean isFirstCheck = true;
            while (true)
            {
                //check if the name is unique
                Boolean isUnique = isUniqueCheck(currentValue);

                if ((isFirstCheck && forceFirstShow) || !isUnique || String.IsNullOrEmpty(currentValue))
                {
                    //TODO: Change over to use window service and add to unit tests.

                    //prompt the user to enter a new name
                    FixtureLibraryAddEditWindow dia = new FixtureLibraryAddEditWindow(package, true, newFolderPath);
                    dia.Title = title;
                    dia.ViewModel.FixtureName = currentValue;
                    dia.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                    dia.Owner = App.MainPageViewModel.AttachedControl;

                    //if this is the first check show the first show description
                    if (isFirstCheck && forceFirstShow && !String.IsNullOrEmpty(forceFirstShowDescription))
                    {
                        dia.Description = forceFirstShowDescription;
                    }
                    else
                    {
                        dia.Description = notUniqueDescription;
                    }

                    //show the dialog
                    if (dia.ShowDialog() == true)
                    {
                        currentValue = dia.ViewModel.FixtureName;
                        newFolderPath = dia.ViewModel.FolderPath;
                    }
                    else
                    {
                        wasCancelled = true;
                        break;
                    }
                }
                else
                {
                    //is unique so break
                    break;
                }

                isFirstCheck = false;
            }


            //if a name was accepted so return
            selectedNameValue = currentValue;
            selectedFolderPath = newFolderPath;
            return !wasCancelled;
        }



        /// <summary>
        /// Returns true if the selection is valid to be added to fixture library.
        /// </summary>
        /// <param name="selectedItems"></param>
        /// <returns></returns>
        public static Boolean CanAddToFixtureLibrary(IEnumerable<IPlanItem> selectedItems, out List<IPlanItem> validatedSelection)
        {
            validatedSelection = null;

            if (!selectedItems.Any()) return false;

            //get the highest items from the selection
            List<IPlanItem> highestDistinctItems = PlanItemHelper.StripOutCommonChildren(selectedItems);

            //remove all invalid types.
            highestDistinctItems.RemoveAll(p =>
                p.PlanItemType != PlanItemType.Fixture
                && p.PlanItemType != PlanItemType.Assembly
                && p.PlanItemType != PlanItemType.Component);

            //check only 1 item is actually selected
            //or they are all fixtures
            if(highestDistinctItems.Count > 1  && !highestDistinctItems.All(p=> p.PlanItemType == PlanItemType.Fixture))
             return false;

            validatedSelection = highestDistinctItems;
            return validatedSelection.Any();
        }

        /// <summary>
        /// Interprets the selected items and adds them to the fixture library if possible.
        /// Prompting the user for input as required.
        /// </summary>
        /// <param name="selectedItems">the currently selected plan items.</param>
        /// <returns>True</returns>
        public static Boolean AddToFixtureLibrary(List<IPlanItem> selectedItems)
        {
            return AddToFixtureLibrary(selectedItems, null);
        }

        /// <summary>
        /// Interprets the selected items and adds them to the fixture library if possible.
        /// Prompting the user for input as required.
        /// </summary>
        /// <param name="itemsToAdd">the currently selected plan items.</param>
        /// <returns></returns>
        public static Boolean AddToFixtureLibrary(List<IPlanItem> selectedItems, String newPackageFilePath)
        {
            List<IPlanItem> itemsToAdd;
            if (!CanAddToFixtureLibrary(selectedItems, out itemsToAdd)) return false;

            //Replaced by selection validate
            //V8:25514 - if a fixture is selected, ensure its first to cause fixture to be added, not components of
            //if (itemsToAdd.Any(p => p.PlanItemType.Equals(PlanItemType.Fixture)))
            //{
            //    //If element 0 not fixture
            //    if (itemsToAdd.ElementAt(0).PlanItemType != PlanItemType.Fixture)
            //    {
            //        //Move first fixture to element index 0
            //        IPlanItem fixtureItem = itemsToAdd.FirstOrDefault(p => p.PlanItemType == PlanItemType.Fixture);
            //        Int32 oldIndex = itemsToAdd.IndexOf(fixtureItem);
            //        itemsToAdd.RemoveAt(oldIndex);
            //        itemsToAdd.Insert(0, fixtureItem);
            //    }
            //}

            //create the package
            Boolean operationCancelled = false;

            using (FixturePackageViewModel packageView = new FixturePackageViewModel())
            {
                packageView.CreateNew(itemsToAdd);

                if (packageView.Model != null)
                {
                    switch (FixtureLibraryHelper.GetConnectedFixturePackageType())
                    {
                        case FixturePackageType.FileSystem:
                            {
                                String folderPath;
                                String packageName = packageView.Model.Name;

                                Boolean nameAccepted = true;
                                if (String.IsNullOrEmpty(newPackageFilePath))
                                {
                                    //get the fixture directory.
                                    String fixtureDirectory = packageView.GetFileSystemDirectory();
                                    if (String.IsNullOrEmpty(fixtureDirectory))
                                    {
                                        CommonHelper.GetWindowService().ShowErrorOccurredMessage(packageView.Model.Name, OperationType.Save);
                                        operationCancelled = true;
                                        break;
                                    }

                                    String[] takenNames = new String[] { };
                                    if (Directory.Exists(fixtureDirectory))
                                    {
                                        takenNames = Directory.GetFiles(fixtureDirectory, "*" + FixturePackage.FileExtension, SearchOption.AllDirectories)
                                            .Select(f => Path.GetFileNameWithoutExtension(f)).ToArray();
                                    }


                                    if (App.Current != null && App.Current.MainWindow != null)
                                    {
                                        nameAccepted =
                                            AddFixtureLibraryPromptUserForValues(
                                            Message.FixtureLibrary_AddToLibraryTitle,
                                            Message.FixtureLibraryAddEditWindow_AddToLibraryText,
                                            Message.FixtureLibrary_AddToLibraryNameNotUnique, true,
                                            (s) => !takenNames.Select(t => t.ToUpperInvariant()).Contains(s.Trim().ToUpperInvariant()),
                                            packageName, App.ViewState.GetSessionDirectory(SessionDirectory.FixtureLibrary),
                                            out packageName,
                                            out folderPath,
                                            packageView.Model);
                                    }
                                    else
                                    {
                                        //Default for unit tests - TODO: Get rid..
                                        folderPath = App.ViewState.GetSessionDirectory(SessionDirectory.FixtureLibrary);
                                    }
                                }
                                else
                                {
                                    packageName = System.IO.Path.GetFileNameWithoutExtension(newPackageFilePath);
                                    folderPath = System.IO.Path.GetDirectoryName(newPackageFilePath);
                                }

                                //If name is valid, and the fixture package created is valid
                                if (nameAccepted && (packageView.Model != null && packageView.Model.IsValid))
                                {
                                    packageView.Model.Name = packageName;

                                    try
                                    {
                                        packageView.SaveAsFile(packageView.GetModelFileSystemPath(folderPath));
                                    }
                                    catch (Exception ex)
                                    {
                                        Exception rootException = ex.GetBaseException();
                                        LocalHelper.RecordException(rootException);

                                        CommonHelper.GetWindowService().
                                            ShowErrorOccurredMessage(packageView.Model.Name, OperationType.Save);
                                    }
                                }
                                else
                                {
                                    operationCancelled = true;
                                }
                            }
                            break;

                        default:
                            Debug.Fail("File type not handled");
                            operationCancelled = true;
                            break;
                    }
                }

                if (operationCancelled) return false;

                //show the library panel if it is not already visible.
                App.ViewState.RaiseFixtureLibraryChanged();
                if (!App.MainPageViewModel.IsLibraryPanelVisible)
                {
                    App.MainPageViewModel.IsLibraryPanelVisible = true;
                }
            }

            return true;
        }

        /// <summary>
        /// Returns a fixture package file path from the given parts.
        /// </summary>
        public static String CreateFilePath(String directory, String fileName)
        {
            return System.IO.Path.ChangeExtension(System.IO.Path.Combine(directory, fileName), FixturePackage.FileExtension);
        }
    }
}
