﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
#endregion
#region Version History: (CCM 8.1.1)
// V8-30277 : A.Probyn
//  ~ Updated so that property set action items are performed first in undo, incase object instance changes also change
//  within the same action.
//  ~ Added missing defensive code to Redo on ModelUndoActionItem
#endregion
#region Version History: (CCM 8.3)
//V8-32115 : L.Ineson
//  Updated following framework changes
#endregion
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    public enum UndoActionType
    {
        SetProperty,
        Add,
        Remove
    }

    /// <summary>
    /// Represents a single action which may be undone.
    /// </summary>
    public sealed class ModelUndoAction
    {
        #region Fields
        private readonly List<IModelUndoActionItem> _actionItems = new List<IModelUndoActionItem>();
        private Boolean _isUndoable = true;
        private String _descriptionText;
        #endregion

        #region Properties

        public List<IModelUndoActionItem> ActionItems
        {
            get { return _actionItems; }
        }

        /// <summary>
        /// Gets/Sets the description text to use when displaying this action.
        /// </summary>
        public String DescriptionText
        {
            get
            {
                if (!String.IsNullOrEmpty(_descriptionText))
                    return _descriptionText;

                if (this.ActionItems.Any())
                    return this.ActionItems.First().Description;

                return null;
            }
            set { _descriptionText = value; }
        }

        #endregion

        #region Constructor

        public ModelUndoAction() { }

        #endregion

        #region Methods

        /// <summary>
        /// Removes this action.
        /// </summary>
        /// <returns> True if the undo was successful.</returns>
        public Boolean Undo()
        {
            if (!_isUndoable) throw new ArgumentException("Has already been undone!");

            _isUndoable = false;

            List<IModelUndoActionItem> actions = new List<IModelUndoActionItem>(this.ActionItems);
            actions.Reverse();

            //Perform set properties first, incase instances of objects change
            try
            {
                foreach (IModelUndoActionItem item in actions)
                {
                    if (!item.Undo()) return false;
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Reapplies this action
        /// </summary>
        /// <returns></returns>
        public Boolean Redo()
        {
            if (_isUndoable) throw new ArgumentException();

            _isUndoable = true;

            try
            {
                foreach (IModelUndoActionItem item in this.ActionItems)
                {
                    if (!item.Redo()) return false;
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.DescriptionText;
        }

        #endregion
    }

    public interface IModelUndoActionItem
    {
        Boolean IsBusy { get; set; }
        String Description { get; }

        Boolean Undo();
        Boolean Redo();
    }

    public sealed class ModelUndoPropertyActionItem : IModelUndoActionItem
    {
        public Boolean IsBusy { get; set; }
        public String Description
        {
            get { return Message.UndoActionType_SetProperty; }
        }

        public Object TargetObject { get; set; }
        public String PropertyName { get; set; }
        public Object OldValue { get; set; }
        public Object NewValue { get; set; }

        public Boolean Undo()
        {
            if (this.TargetObject == null) return false;

            try
            {
                PropertyInfo pInfo = TargetObject.GetType().GetProperty(this.PropertyName);
                if (pInfo == null || !pInfo.CanWrite) return false;

                pInfo.SetValue(this.TargetObject, this.OldValue, null);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public Boolean Redo()
        {
            if (this.TargetObject == null) return false;

            try
            {
                PropertyInfo pInfo = TargetObject.GetType().GetProperty(this.PropertyName);
                if (pInfo == null || !pInfo.CanWrite) return false;

                pInfo.SetValue(this.TargetObject, this.NewValue, null);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
    }

    public sealed class ModelUndoCollectionActionItem : IModelUndoActionItem
    {
        public Boolean IsBusy { get; set; }
        public NotifyCollectionChangedAction ActionType { get; set; }
        public String Description
        {
            get { return Message.UndoActionType_Add; }
        }

        public IList ModelList { get; set; }
        public IModelObject ModelObject { get; set; }

        public Boolean Undo()
        {
            if (ModelList == null || ModelObject == null) return false;
            

            switch(ActionType)
            {
                case NotifyCollectionChangedAction.Add:
                    {   
                        if (!ModelList.Contains(ModelObject)) return false;

                        try
                        {
                            MethodInfo undoAddMethod = ModelList.GetType().GetMethod("UndoAdd");
                            if (undoAddMethod == null) return false;
                            undoAddMethod.Invoke(ModelList, new Object[] { ModelObject });
                        }
                        catch (Exception)
                        {
                            return false;
                        }

                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        try
                        {
                            MethodInfo undoRemoveMethod = ModelList.GetType().GetMethod("UndoRemove");
                            if (undoRemoveMethod == null) return false;
                            undoRemoveMethod.Invoke(ModelList, new Object[] { ModelObject });
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    }
                    break;

            }

            return true;
        }

        public Boolean Redo()
        {
            if (ModelList == null || ModelObject == null) return false;

            switch (ActionType)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        try
                        {
                            MethodInfo undoRemoveMethod = ModelList.GetType().GetMethod("UndoRemove");
                            if (undoRemoveMethod == null) return false;
                            undoRemoveMethod.Invoke(ModelList, new Object[] { ModelObject });
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        if (!ModelList.Contains(ModelObject)) return false;
                        try
                        {
                            ModelList.Remove(ModelObject);
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    }
                    break;
            }

            return true;
        }

    }


}
