﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.

#endregion

#endregion

using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Threading;
using Galleria.Ccm.Editor.Client.Wpf.Common.ModelObjectViewModels;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.Common
{
    /// <summary>
    ///     Dialog to configure a <see cref="CustomColumnLayout"/>.
    /// </summary>
    public partial class CustomColumnLayoutDialog
    {
        #region Constants

        private const String RemoveFilterCommandKey = "RemoveFilterCommand";

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof(CustomColumnLayoutViewModel), typeof(CustomColumnLayout),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        ///     Gets or sets the ViewModel controller for the dialog.
        /// </summary>
        public CustomColumnLayoutViewModel ViewModel
        {
            get { return (CustomColumnLayoutViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = (CustomColumnLayoutDialog)obj;

            if (e.OldValue != null)
            {
                var oldModel = (CustomColumnLayoutViewModel)e.OldValue;
                senderControl.Resources.Remove(RemoveFilterCommandKey);
                oldModel.AttachedControl = null;
            }

            if (e.NewValue == null)
            {
                return;
            }

            var newModel = (CustomColumnLayoutViewModel)e.NewValue;
            senderControl.Resources.Add(RemoveFilterCommandKey, newModel.RemoveFilterCommand);
            newModel.AttachedControl = senderControl;
        }

        #endregion

        #endregion

        #region Constructor

        public CustomColumnLayoutDialog(CustomColumnLayout layout,
            ReadOnlyObservableCollection<CustomColumnLayout> availableLayouts,
            Boolean isEdit = true)
        {
            InitializeComponent();
            ViewModel = new CustomColumnLayoutViewModel(layout, availableLayouts, isEdit);
            ViewModel.SaveCommand.Executed += OnSaveCommandExecuted;
        }

        #endregion

        #region Event Handlers

        private void OnSaveCommandExecuted(Object sender, EventArgs e)
        {
            ViewModel.SaveCommand.Executed -= OnSaveCommandExecuted;

            DialogResult = true; // this will cause the window to close.
        }

        /// <summary>
        ///     Whenever the Close button is clicked the window is closed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_OnClick(Object sender, RoutedEventArgs e)
        {
            DialogResult = false; // this will cause the window to close.
        }

        /// <summary>
        ///     Whenever a row is dropped back into the Available Columns grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAvailableColumns_RowDropCaught(Object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (Equals(e.SourceGrid, xSelectedColumnsGrid) && ViewModel != null)
            {
                ViewModel.RemoveSelectedColumnsCommand.Execute();
            }
        }

        /// <summary>
        ///     Whenever a row is dropped into the Selected Columns grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xSelectedColumns_RowDropCaught(Object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (Equals(e.SourceGrid, xAvailableColumnsGrid) && ViewModel != null)
            {
                ViewModel.AddSelectedColumnsCommand.Execute();
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Window.Closed"/> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnClosed(EventArgs e)
        {
            Dispatcher.BeginInvoke((Action) (() =>
            {
                var disposableViewModel = ViewModel as IDisposable;
                ViewModel = null;

                if (disposableViewModel != null)
                {
                    disposableViewModel.Dispose();
                }
            }), DispatcherPriority.Background);

            base.OnClosed(e); // continue with the normal eventhandler.
        }

        #endregion
    }
}
