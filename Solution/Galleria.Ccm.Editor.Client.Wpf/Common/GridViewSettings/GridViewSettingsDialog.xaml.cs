﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Threading;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.Common.GridViewSettings
{
    /// <summary>
    /// Interaction logic for GridViewSettingsDialog.xaml
    /// </summary>
    public partial class GridViewSettingsDialog
    {
        #region Constants

        private const String RemoveFilterCommandKey = "RemoveFilterCommand";

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof(GridViewSettingsViewModel), typeof(GridViewSettings),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public GridViewSettingsViewModel ViewModel
        {
            get { return (GridViewSettingsViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = (GridViewSettingsDialog)obj;

            if (e.OldValue != null)
            {
                var oldModel = (GridViewSettingsViewModel)e.OldValue;
                senderControl.Resources.Remove(RemoveFilterCommandKey);
                oldModel.AttachedControl = null;
            }

            if (e.NewValue == null)
            {
                return;
            }

            var newModel = (GridViewSettingsViewModel)e.NewValue;
            senderControl.Resources.Add(RemoveFilterCommandKey, newModel.RemoveFilterCommand);
            newModel.AttachedControl = senderControl;
        }

        #endregion

        #endregion

        #region Constructor

        public GridViewSettingsDialog(GridViewSettings gridViewSettings,
            ObservableCollection<GridViewSettings> availableGridViewSettings,
            Boolean isEdit = true)
        {
            InitializeComponent();
            ViewModel = new GridViewSettingsViewModel(gridViewSettings, availableGridViewSettings, isEdit);
            ViewModel.SaveCommand.Executed += OnSaveCommandExecuted;
        }

        #endregion

        #region Event Handlers

        private void OnSaveCommandExecuted(Object sender, EventArgs e)
        {
            ViewModel.SaveCommand.Executed -= OnSaveCommandExecuted;

            DialogResult = true; // this will cause the window to close.
        }

        /// <summary>
        ///     Whenever the Close button is clicked the window is closed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_OnClick(Object sender, RoutedEventArgs e)
        {
            DialogResult = false; // this will cause the window to close.
        }

        /// <summary>
        ///     Whenever a row is dropped back into the Available Columns grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAvailableColumns_RowDropCaught(Object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid == xSelectedColumnsGrid && ViewModel != null)
            {
                ViewModel.RemoveSelectedColumnsCommand.Execute();
            }
        }

        /// <summary>
        ///     Whenever a row is dropped into the Selected Columns grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xSelectedColumns_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid == xAvailableColumnsGrid && ViewModel != null)
            {
                ViewModel.AddSelectedColumnsCommand.Execute();
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Window.Closed"/> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnClosed(EventArgs e)
        {
            Dispatcher.BeginInvoke((Action) (() =>
            {
                var disposableViewModel = ViewModel as IDisposable;
                ViewModel = null;

                if (disposableViewModel != null)
                {
                    disposableViewModel.Dispose();
                }
            }), DispatcherPriority.Background);

            base.OnClosed(e); // continue with the normal eventhandler.
        }

        #endregion
    }
}
