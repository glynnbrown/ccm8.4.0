﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva
//  Created.

#endregion

#endregion

namespace Galleria.Ccm.Editor.Client.Wpf.Common.GridViewSettings
{
    /// <summary>
    /// Server side implementation for the model for settings of a grid view.
    /// </summary>
    public sealed partial class GridViewSettings
    {
        #region Constructors
     
        /// <summary>
        ///     Private constructor. Obtain a new instance through the factory methods.
        /// </summary>
        private GridViewSettings() { }

        #endregion
    }
}