﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva
//  Created.

#endregion

#endregion

using System;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Editor.Client.Wpf.Common.GridViewSettings
{
    /// <summary>
    /// Model for settings configuring a grid view.
    /// </summary>
    [Serializable]
    public sealed partial class GridViewSettings : ModelObject<GridViewSettings>
    {
        #region Properties

        #region Name Property

        public static readonly ModelPropertyInfo<String> NameProperty = RegisterModelProperty<String>(o => o.Name);

        public String Name
        {
            get { return GetProperty(NameProperty); }
            set { SetProperty(NameProperty, value); }
        }

        #endregion

        #region Description Property

        public static readonly ModelPropertyInfo<String> DescriptionProperty = RegisterModelProperty<String>(o => o.Description);

        public String Description
        {
            get { return GetProperty(DescriptionProperty); }
            set { SetProperty(DescriptionProperty, value); }
        }

        #endregion

        #endregion

        #region Factory pattern support

        /// <summary>
        ///     Create new user defined grid view settings.
        /// </summary>
        /// <returns></returns>
        public static GridViewSettings New()
        {
            var newInstance = new GridViewSettings();
            newInstance.Create();
            return newInstance;
        }

        #endregion

        #region Methods

        #region Overrides of ModelObject<GridViewSettings>

        protected override void Create()
        {
            LoadProperty(NameProperty,"Custom View");
            LoadProperty(DescriptionProperty, "New user defined view.");

            base.Create();
        }

        #endregion

        #endregion
    }
}