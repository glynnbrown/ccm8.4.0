﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.Common.GridViewSettings
{
    public class GridViewSettingsViewModel : ViewModelAttachedControlObject<GridViewSettingsDialog>
    {
        #region Fields

        private GridViewSettings _currentView;

        /// <summary>
        ///     Whether any content of the view has been changed.
        /// </summary>
        private Boolean _isViewChanged;

        private GridViewSettingsFilter _selectedFilterItem;

        #endregion

        #region Property Paths

        public static readonly PropertyPath CurrentViewProperty =
            WpfHelper.GetPropertyPath<GridViewSettingsViewModel>(p => p.CurrentView);

        public static readonly PropertyPath SelectedFilterItemProperty =
            WpfHelper.GetPropertyPath<GridViewSettingsViewModel>(p => p.SelectedFilterItem);

        public static PropertyPath SelectedGroupByLevelOneColumns;
        public static PropertyPath AvailableGroupBylevelOneColumnsProperty;
        public static PropertyPath SelectedGroupByLevelOneProperty;
        public static PropertyPath AvailableGroupBylevelTwoColumnsProperty;
        public static PropertyPath SelectedGroupByLevelTwoProperty;
        public static PropertyPath SelectedGroupByLevelTwoColumns;
        public static PropertyPath SelectedGroupByLevelFourProperty;
        public static PropertyPath AvailableGroupBylevelFourColumnsProperty;
        public static PropertyPath SelectedGroupByLevelFourColumns;
        public static PropertyPath AvailableSortBylevelOneColumnsProperty;
        public static PropertyPath SelectedSortByLevelOneProperty;
        public static PropertyPath SelectedSortByLevelTwoColumns;
        public static PropertyPath SortByLevelOneAscendingProperty;
        public static PropertyPath SortByLevelOneDescendingProperty;
        public static PropertyPath SelectedSortByLevelOneColumns;
        public static PropertyPath SelectedSortByLevelTwoProperty;
        public static PropertyPath AvailableSortBylevelTwoColumnsProperty;
        public static PropertyPath SortByLevelTwoAscendingProperty;
        public static PropertyPath SortByLevelTwoDescendingProperty;
        public static PropertyPath SelectedSortByLevelFourProperty;
        public static PropertyPath AvailableSortBylevelFourColumnsProperty;
        public static PropertyPath SortByLevelFourAscendingProperty;
        public static PropertyPath SortByLevelFourDescendingProperty;
        public static PropertyPath SelectedSortByLevelFourColumns;
        public static PropertyPath AvailableFilterByColumnsProperty;
        public static PropertyPath SelectedNewFilterColumnProperty;
        public static PropertyPath SelectedFiltersProperty;
        public static PropertyPath RemoveFilterCommandProperty;
        public static PropertyPath SaveCommandPath;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets/Sets the current view
        /// </summary>
        public GridViewSettings CurrentView
        {
            get { return _currentView; }
            set
            {
                _currentView = value;
                OnPropertyChanged(CurrentViewProperty);
                _isViewChanged = false;
            }
        }

        #region AvailableColumns Property

        private ObservableCollection<GridViewSettingsColumn> _availableColumns;

        public static PropertyPath AvailableColumnsProperty =
    WpfHelper.GetPropertyPath<GridViewSettingsViewModel>(o => o.AvailableColumns);

        public ObservableCollection<GridViewSettingsColumn> AvailableColumns
        {
            get { return _availableColumns; }
            set
            {
                _availableColumns = value;
                OnPropertyChanged(AvailableColumnsProperty);
            }
        }

        #endregion

        #region SelectedAvailableColumns Property

        private ObservableCollection<GridViewSettingsColumn> _selectedAvailableColumns;

        public static PropertyPath SelectedAvailableColumnsProperty =
    WpfHelper.GetPropertyPath<GridViewSettingsViewModel>(o => o.SelectedAvailableColumns);

        public ObservableCollection<GridViewSettingsColumn> SelectedAvailableColumns
        {
            get { return _selectedAvailableColumns; }
            set
            {
                _selectedAvailableColumns = value;
                OnPropertyChanged(SelectedAvailableColumnsProperty);
            }
        }

        #endregion

        #region SelectedColumns Property

        private ObservableCollection<GridViewSettingsColumn> _selectedColumns;

        public static PropertyPath SelectedColumnsProperty =
            WpfHelper.GetPropertyPath<GridViewSettingsViewModel>(o => o.SelectedColumns);

        public ObservableCollection<GridViewSettingsColumn> SelectedColumns
        {
            get { return _selectedColumns; }
            set
            {
                _selectedColumns = value;
                OnPropertyChanged(SelectedColumnsProperty);
            }
        }

        #endregion

        #region SelectedViewColumns Property

        private ObservableCollection<GridViewSettingsColumn> _selectedViewColumns;

        public static PropertyPath SelectedViewColumnsProperty =
            WpfHelper.GetPropertyPath<GridViewSettingsViewModel>(o => o.SelectedViewColumns);

        public ObservableCollection<GridViewSettingsColumn> SelectedViewColumns
        {
            get { return _selectedViewColumns; }
            set
            {
                _selectedViewColumns = value;
                OnPropertyChanged(SelectedViewColumnsProperty);
            }
        }

        #endregion

        #region FilterBy Properties

        /// <summary>
        ///     Gets/Sets the selected filter item
        /// </summary>
        public GridViewSettingsFilter SelectedFilterItem
        {
            get { return _selectedFilterItem; }
            set
            {
                _selectedFilterItem = value;
                OnPropertyChanged(SelectedFilterItemProperty);
            }
        }

        #endregion

        #endregion

        #region Constructor

        public GridViewSettingsViewModel(GridViewSettings gridViewSettings,
            ObservableCollection<GridViewSettings> availableGridViewSettings,
            Boolean isEdit)
        {
        }

        #endregion

        #region Commands

        #region SaveCommand

        private RelayCommand _saveCommand;

        /// <summary>
        ///     Saves the current grid view settings
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null) return _saveCommand;

                _saveCommand = new RelayCommand(p => Save_Executed(), p => Save_CanExecute())
                {
                    FriendlyName = Message.Generic_Save,
                    FriendlyDescription = Message.Generic_Save_Tooltip,
                    SmallIcon = ImageResources.Save_32,
                    DisabledReason = Message.Generic_Save_DisabledReasonInvalidData
                };
                ViewModelCommands.Add(_saveCommand);

                return _saveCommand;
            }
        }

        private bool Save_CanExecute()
        {
            throw new NotImplementedException();
        }

        private void Save_Executed()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region RemoveFilter Command

        private RelayCommand _removeFilterCommand;

        /// <summary>
        ///     Removes an existing filter from a view
        /// </summary>
        public RelayCommand RemoveFilterCommand
        {
            get
            {
                if (_removeFilterCommand == null) return _removeFilterCommand;

                _removeFilterCommand = new RelayCommand(p => RemoveFilter_Executed(), p => RemoveFilter_CanExecute())
                {
                    FriendlyName = Message.GridViewSettings_RemoveFilter,
                    FriendlyDescription = Message.GridViewSettings_RemoveFilter_Tooltip,
                    SmallIcon = ImageResources.GridViewSettings_RemoveFilter_16
                };
                ViewModelCommands.Add(_removeFilterCommand);

                return _removeFilterCommand;
            }
        }

        private bool RemoveFilter_CanExecute()
        {
            return (SelectedFilterItem != null);
        }

        private void RemoveFilter_Executed()
        {
            ////Take copy of the selected filter
            //ProductsViewFilter selectedFilter = this.SelectedFilterItem;

            ////Remove filter
            //this.CurrentView.Filters.Remove(selectedFilter);

            ////Get corresponding column
            //ProductsViewColumn column = this.CurrentView.Columns.Where(p => p.Path == selectedFilter.Path).FirstOrDefault();

            //if (column != null)
            //{
            //    //Add back to available list
            //    if (column.Path != "NotificationDataUpdate." + NotificationDataUpdate.FollowItemProperty.Name) // ensure that this doesn not show in filter options
            //    {
            //        this.AvailableFilterByColumns.Add(column);
            //    }
            //    this.SelectedNewFilterColumn = this.AvailableFilterByColumns.FirstOrDefault();
            //}
            //_isFilterRemoved = true;
        }

        #endregion

        #region RemoveSelectedColumns Command

        private RelayCommand _removeSelectedColumnsCommand;

        public static PropertyPath RemoveSelectedColumnsCommandPath =
            WpfHelper.GetPropertyPath<GridViewSettingsViewModel>(o => o.RemoveSelectedColumnsCommand);

        public RelayCommand RemoveSelectedColumnsCommand
        {
            get
            {
                if (_removeSelectedColumnsCommand == null) return _removeSelectedColumnsCommand;

                _removeSelectedColumnsCommand = new RelayCommand(o => RemoveSelectedColumns_Execute(),
                    o => RemoveSelectedColumns_CanExecute())
                {
                    FriendlyDescription = Message.GridViewSettings_RemoveSelectedColumns,
                    SmallIcon = ImageResources.TODO,
                    DisabledReason = Message.GridViewSettings_RemoveSelectedColumns_DisabledReason
                };
                ViewModelCommands.Add(_removeSelectedColumnsCommand);

                return _removeSelectedColumnsCommand;
            }
        }

        private Boolean RemoveSelectedColumns_CanExecute()
        {
            throw new NotImplementedException();
        }

        private void RemoveSelectedColumns_Execute()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region AddSelectedColumns Command

        private RelayCommand _addSelectedColumnsCommand;

        public static PropertyPath AddSelectedColumnsCommandPath =
            WpfHelper.GetPropertyPath<GridViewSettingsViewModel>(o => o.AddSelectedColumnsCommand);

        public RelayCommand AddSelectedColumnsCommand
        {
            get
            {
                if (_addSelectedColumnsCommand != null) return _addSelectedColumnsCommand;

                _addSelectedColumnsCommand = new RelayCommand(o => AddSelectedColumns_Executed(), o => AddSelectedColumns_CanExecute())
                {
                    FriendlyDescription = Message.GridViewSettings_AddSelectedColumns,
                    SmallIcon = ImageResources.TODO,
                    DisabledReason = Message.GridViewSettings_AddSelectedColumns_DisabledReason
                };
                return _addSelectedColumnsCommand;
            }
        }

        private Boolean AddSelectedColumns_CanExecute()
        {
            throw new NotImplementedException();
        }

        private void AddSelectedColumns_Executed()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region AddAllColumns Command

        private RelayCommand _addAllColumnsCommand;

        public static PropertyPath AddAllColumnsCommandPath =
            WpfHelper.GetPropertyPath<GridViewSettingsViewModel>(o => o.AddAllColumnsCommand);

        public RelayCommand AddAllColumnsCommand
        {
            get
            {
                if (_addAllColumnsCommand != null) return _addAllColumnsCommand;

                _addAllColumnsCommand = new RelayCommand(o => AddAllColumns_Executed(), o => AddAllColumns_CanExecute())
                {
                    FriendlyDescription = Message.GridViewSettings_AddAllColumns,
                    SmallIcon = ImageResources.TODO,
                    DisabledReason = Message.GridViewSettings_AddAllColumns_DisabledReason
                };
                return _addAllColumnsCommand;
            }
        }

        private Boolean AddAllColumns_CanExecute()
        {
            throw new NotImplementedException();
        }

        private void AddAllColumns_Executed()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region RemoveAllColumns Command

        private RelayCommand _removeAllColumnsCommand;

        public static PropertyPath RemoveAllColumnsCommandPath =
            WpfHelper.GetPropertyPath<GridViewSettingsViewModel>(o => o.RemoveAllColumnsCommand);

        public RelayCommand RemoveAllColumnsCommand
        {
            get
            {
                if (_removeAllColumnsCommand != null) return _removeAllColumnsCommand;

                _removeAllColumnsCommand = new RelayCommand(o => RemoveAllColumns_Executed(), o => RemoveAllColumns_CanExecute())
                {
                    FriendlyDescription = Message.GridViewSettings_RemoveAllColumns,
                    SmallIcon = ImageResources.TODO,
                    DisabledReason = Message.GridViewSettings_RemoveAllColumns_DisabledReason
                };
                return _removeAllColumnsCommand;
            }
        }

        private Boolean RemoveAllColumns_CanExecute()
        {
            throw new NotImplementedException();
        }

        private void RemoveAllColumns_Executed()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region MoveColumnUp Command

        private RelayCommand _moveColumnUpCommand;

        public static PropertyPath MoveColumnUpCommandPath =
            WpfHelper.GetPropertyPath<GridViewSettingsViewModel>(o => o.MoveColumnUpCommand);

        public RelayCommand MoveColumnUpCommand
        {
            get
            {
                if (_moveColumnUpCommand != null) return _moveColumnUpCommand;

                _moveColumnUpCommand = new RelayCommand(o => MoveColumnUp_Executed(), o => MoveColumnUp_CanExecute())
                {
                    FriendlyDescription = Message.GridViewSettings_MoveColumnUp,
                    SmallIcon = ImageResources.TODO,
                    DisabledReason = Message.GridViewSettings_MoveColumnUp_DisabledReason
                };
                return _moveColumnUpCommand;
            }
        }

        private Boolean MoveColumnUp_CanExecute()
        {
            throw new NotImplementedException();
        }

        private void MoveColumnUp_Executed()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region MoveColumnDown Command

        private RelayCommand _moveColumnDownCommand;

        public static PropertyPath MoveColumnDownCommandPath =
            WpfHelper.GetPropertyPath<GridViewSettingsViewModel>(o => o.MoveColumnDownCommand);



        public RelayCommand MoveColumnDownCommand
        {
            get
            {
                if (_moveColumnDownCommand != null) return _moveColumnDownCommand;

                _moveColumnDownCommand = new RelayCommand(o => MoveColumnDown_Executed(), o => MoveColumnDown_CanExecute())
                {
                    FriendlyDescription = Message.GridViewSettings_MoveColumnDown,
                    SmallIcon = ImageResources.TODO,
                    DisabledReason = Message.GridViewSettings_MoveColumnDown_DisabledReason
                };
                return _moveColumnDownCommand;
            }
        }

        private Boolean MoveColumnDown_CanExecute()
        {
            throw new NotImplementedException();
        }

        private void MoveColumnDown_Executed()
        {
            throw new NotImplementedException();
        }

        #endregion

        #endregion

        #region IDisposable Support

        protected override void Dispose(bool disposing)
        {
            if (IsDisposed)
            {
                return;
            }

            if (disposing)
            {
                //TODO dispose resources.
            }
            IsDisposed = true;
        }

        #endregion
    }

    public class GridViewSettingsColumn
    {
        //TODO implement GridViewSettingsColumn
    }

    public class GridViewFilter
    {
        public static PropertyPath HeaderProperty;
        public static PropertyPath TextProperty;
    }
}