﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;
using System.Windows;

//TODO: Classes under this namespace will eventually move into the framework.
namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData
{
    /// <summary>
    /// Interface defining base data used to create a ModelConstructPart3D
    /// </summary>
    public interface IModelConstructPart3DData : INotifyPropertyChanged, IDisposable
    {
        IModelConstruct3DData ParentModelData { get; }

        /// <summary>
        /// Gets/Sets whether this part should be visible
        /// </summary>
        Boolean IsVisible { get; set; }

        /// <summary>
        /// Gets/Sets whether this part should be rendered as wireframe only.
        /// </summary>
        Boolean IsWireframe { get; set; }

        /// <summary>
        /// Gets/Sets the position of the model
        /// </summary>
        ObjectPoint3D Position { get; set; }

        /// <summary>
        /// Gets/Sets the rotation of the model
        /// </summary>
        ObjectRotation3D Rotation { get; set; }

        /// <summary>
        /// Gets/Sets the size of the model.
        /// </summary>
        ObjectSize3D Size { get; set; }

        /// <summary>
        /// Gets/Sets the main material used to paint the model.
        /// </summary>
        ObjectMaterial3D Material { get; set; }

        /// <summary>
        /// Gets/Sets the thickness of the model lines
        /// </summary>
        Double LineThickness { get; set; }

        /// <summary>
        /// Gets/Sets the colour of the model lines
        /// </summary>
        ObjectColour LineColour { get; set; }

        /// <summary>
        /// Gets/Sets an optional tag for this part. 
        /// Not used by the render.
        /// </summary>
        Object Tag { get; set; }

        /// <summary>
        /// Returns true if the part data is undergoing a mass edit.
        /// </summary>
        Boolean IsEditing { get; }

        /// <summary>
        /// Begins an edit state on this data
        /// </summary>
        void BeginEdit();

        /// <summary>
        /// Ends any existing edit state on this data.
        /// </summary>
        void EndEdit();
    }

}
