﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;

//TODO: Classes under this namespace will eventually move into the framework.
namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData
{
    /// <summary>
    /// Enum denoting the box faces.
    /// </summary>
    [Flags]
    public enum BoxFaceType
    {
        Front = 0x1,
        Back = 0x2,
        Top = 0x4,
        Bottom = 0x8,
        Left = 0x10,
        Right = 0x20,
        All = Front | Back | Top | Bottom | Left | Right
    }


    /// <summary>
    /// Interface defining members required to create the data for
    /// a BoxModelConstructPart3D
    /// </summary>
    public interface IBoxModelPart3DData : IModelConstructPart3DData
    {
        BoxFaceType BoxFaces { get; set; }
        Double BoxFaceThickness { get; set; }

        ObjectMaterial3D BackFaceMaterial { get; set; }
        ObjectMaterial3D TopFaceMaterial { get; set; }
        ObjectMaterial3D BottomFaceMaterial { get; set; }
        ObjectMaterial3D LeftFaceMaterial { get; set; }
        ObjectMaterial3D RightFaceMaterial { get; set; }

        /// <summary>
        /// Gets/Sets optional tag data
        /// </summary>
        Object Data { get; set; }

        /// <summary>
        /// Gets the collection of cutout definitions
        /// </summary>
        /// <remarks>TODO: Get rid</remarks>
        IEnumerable<IModelConstructPart3DCutout> CutoutDefinitions { get; }

        /// <summary>
        /// Gets/Sets whether cutout definitions should be rendered.
        /// </summary>
        /// <remarks>TODO: Get rid.</remarks>
        Boolean RenderCutouts { get; set; }
    }


    ///// <summary>
    ///// Interface defining members required to create the data for
    ///// a BoxModelConstructPart3D
    ///// </summary>
    //public interface IBoxModelPart3DData : IModelConstructPart3DData
    //{
    //    BoxFaceType BoxFaces { get; set; }
    //    Double BoxFaceThickness { get; set; }

    //    ObjectMaterial3D BackFaceMaterial { get; set; }
    //    ObjectMaterial3D TopFaceMaterial { get; set; }
    //    ObjectMaterial3D BottomFaceMaterial { get; set; }
    //    ObjectMaterial3D LeftFaceMaterial { get; set; }
    //    ObjectMaterial3D RightFaceMaterial { get; set; }
    //}
}
