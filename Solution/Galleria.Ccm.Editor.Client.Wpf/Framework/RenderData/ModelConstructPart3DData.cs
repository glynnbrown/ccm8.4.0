﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;

//TODO: Classes under this namespace will eventually move into the framework.
namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData
{
    /// <summary>
    /// Base class implementation of IModelConstructPart3DData
    /// </summary>
    public abstract class ModelConstructPart3DData : IModelConstructPart3DData
    {
        #region Fields

        private readonly IModelConstruct3DData _parentModelData;

        private ObjectPoint3D _position = new ObjectPoint3D();
        private ObjectRotation3D _rotation = new ObjectRotation3D();
        private ObjectSize3D _size = new ObjectSize3D();
        

        private Double _lineThickness = 0;
        private ObjectColour _lineColour = ObjectColour.Black;
        private ObjectMaterial3D _material;


        private Boolean _isVisible = true;
        private Boolean _canWireframe = true;
        private Boolean _isWireframe = false;

        private Object _tag;

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets whether the model should be visible
        /// </summary>
        public Boolean IsVisible
        {
            get { return _isVisible; }
            set
            {
                if (_isVisible != value)
                {
                    _isVisible = value;
                    OnPropertyChanged("IsVisible");
                }
            }
        }

        public Boolean CanWireframe
        {
            get { return _canWireframe; }
            set 
            { 
                _canWireframe = value;

                //force iswireframe update
                if(_isWireframe && !value)
                {
                    OnPropertyChanged("IsWireframe");
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether only a wireframe should be rendered.
        /// </summary>
        public Boolean IsWireframe
        {
            get { return _isWireframe && _canWireframe; }
            set
            {
                if (_isWireframe != value)
                {
                    _isWireframe = value;
                    OnPropertyChanged("IsWireframe");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the position of the bottom left back corner.
        /// </summary>
        public ObjectPoint3D Position
        {
            get { return _position; }
            set
            {
                if (_position != value)
                {
                    _position = value;
                    OnPropertyChanged("Position");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the rotation to be applied.
        /// Rotation origin is the model position value
        /// </summary>
        public ObjectRotation3D Rotation
        {
            get { return _rotation; }
            set
            {
                if (_rotation != value)
                {
                    _rotation = value;
                    OnPropertyChanged("Rotation");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the size of the model.
        /// </summary>
        public ObjectSize3D Size
        {
            get { return _size; }
            set
            {
                if (_size != value)
                {
                    _size = value;
                    OnPropertyChanged("Size");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the main material of the model.
        /// </summary>
        public ObjectMaterial3D Material
        {
            get { return _material; }
            set
            {
                _material = value;
                OnPropertyChanged("Material");
            }
        }


        /// <summary>
        /// Returns a reference to the parent model data.
        /// </summary>
        public IModelConstruct3DData ParentModelData
        {
            get { return _parentModelData; }
        }

        /// <summary>
        /// Gets/Sets the model line thickness
        /// </summary>
        public Double LineThickness
        {
            get
            {
                return _lineThickness;
            }
            set
            {
                if (_lineThickness != value)
                {
                    _lineThickness = value;
                    OnPropertyChanged("LineThickness");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the model line colour.
        /// </summary>
        public ObjectColour LineColour
        {
            get { return _lineColour; }
            set
            {
                if (_lineColour != value)
                {
                    _lineColour = value;
                    OnPropertyChanged("LineColour");
                }
            }
        }

        /// <summary>
        /// Gets/Sets a tag value
        /// This is not used by the render at all
        /// and may simply be used to identify the part.
        /// </summary>
        public Object Tag
        {
            get { return _tag; }
            set
            {
                _tag = value;
                OnPropertyChanged("Tag");
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="parentData"></param>
        public ModelConstructPart3DData(ModelConstruct3DData parentData)
        {
            _parentModelData = parentData;
            _material = ObjectMaterial3D.CreateMaterial(ObjectColour.White);
        }

        #endregion

        #region Methods

        public virtual void SetMaterial(ObjectMaterial3D mat)
        {
            this.Material = mat;
        }

        public virtual void SetMaterial(ObjectColour newColour)
        {
            ObjectMaterial3D mat = new ObjectMaterial3D();
            mat.FillColour = newColour;
            this.Material = mat;
        }

        #endregion

        #region IEditableObject Members

        private Boolean _isEditing;
        public Boolean IsEditing
        {
            get { return _isEditing; }
            private set
            {
                _isEditing = value;
                OnPropertyChanged("IsEditing");
            }

        }

        public void BeginEdit()
        {
            if (!this.IsEditing)
            {
                this.IsEditing = true;
            }
        }

        public void EndEdit()
        {
            if (this.IsEditing)
            {
                this.IsEditing = false;
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDisposable Members

        protected Boolean IsDisposed;

        public virtual void Dispose()
        {
            if (!IsDisposed)
            {
                DisposeBase();

                IsDisposed = true;
                GC.SuppressFinalize(this);
            }
        }

        protected virtual void DisposeBase()
        {

        }

        #endregion

    }


    #region OLD

    //public class ModelConstructPart3DData : IModelConstructPart3DData
    //{
    //    #region Fields

    //    private readonly IModelConstruct3DData _parentModelData;

    //    private ModelPartType _partType = ModelPartType.Box;
    //    private Boolean _isVisible = true;
    //    private Boolean _isWireframe = false;

    //    private ObjectPoint3D _position = new ObjectPoint3D();
    //    private ObjectRotation3D _rotation = new ObjectRotation3D();
    //    private ObjectSize3D _size = new ObjectSize3D();
    //    private Object _data = null;

    //    private Double _lineThickness = 0;
    //    private ObjectColour _lineColour = ObjectColour.Black;
    //    private ObjectMaterial3DCollection _materials = new ObjectMaterial3DCollection(new ObjectMaterial3D());

    //    private Boolean _renderCutouts = false;
    //    private readonly ObservableCollection<IModelConstructPart3DCutout> _cutoutDefinitions = new ObservableCollection<IModelConstructPart3DCutout>();

    //    private BoxFaceType _boxFaces = BoxFaceType.All;
    //    private Double _boxFaceThickness = 0;

    //    #endregion

    //    #region Properties

    //    public ModelPartType PartType
    //    {
    //        get { return _partType; }
    //    }

    //    public Boolean IsVisible
    //    {
    //        get { return _isVisible; }
    //        set
    //        {
    //            if (_isVisible != value)
    //            {
    //                _isVisible = value;
    //                OnPropertyChanged("IsVisible");
    //            }
    //        }
    //    }

    //    public Boolean IsWireframe
    //    {
    //        get { return _isWireframe; }
    //        set
    //        {
    //            if (_isWireframe != value)
    //            {
    //                _isWireframe = value;
    //                OnPropertyChanged("IsWireframe");
    //            }
    //        }
    //    }

    //    public ObjectPoint3D Position
    //    {
    //        get { return _position; }
    //        set
    //        {
    //            if (_position != value)
    //            {
    //                _position = value;
    //                OnPropertyChanged("Position");
    //            }
    //        }
    //    }

    //    public ObjectRotation3D Rotation
    //    {
    //        get { return _rotation; }
    //        set
    //        {
    //            if (_rotation != value)
    //            {
    //                _rotation = value;
    //                OnPropertyChanged("Rotation");
    //            }
    //        }
    //    }

    //    public ObjectSize3D Size
    //    {
    //        get { return _size; }
    //        set
    //        {
    //            if (_size != value)
    //            {
    //                _size = value;
    //                OnPropertyChanged("Size");
    //            }
    //        }
    //    }

    //    public ObjectMaterial3DCollection Materials
    //    {
    //        get { return _materials; }
    //        set
    //        {
    //            _materials = value;
    //            OnPropertyChanged("Materials");
    //        }
    //    }

    //    public ObjectMaterial3D Material
    //    {
    //        get { return _materials.ElementAt(0); }
    //        set { this.Materials = new ObjectMaterial3DCollection(value); }
    //    }

    //    public Object Data
    //    {
    //        get { return _data; }
    //        set
    //        {
    //            _data = value;
    //            OnPropertyChanged("Data");
    //        }
    //    }

    //    public IModelConstruct3DData ParentModelData
    //    {
    //        get { return _parentModelData; }
    //    }

    //    public ObservableCollection<IModelConstructPart3DCutout> CutoutDefinitions
    //    {
    //        get { return _cutoutDefinitions; }
    //    }

    //    public Boolean RenderCutouts
    //    {
    //        get { return _renderCutouts; }
    //        set
    //        {
    //            if (_renderCutouts != value)
    //            {
    //                _renderCutouts = value;
    //                OnPropertyChanged("RenderCutouts");
    //            }
    //        }
    //    }

    //    public Double LineThickness
    //    {
    //        get
    //        {
    //            return _lineThickness;
    //        }
    //        set
    //        {
    //            if (_lineThickness != value)
    //            {
    //                _lineThickness = value;
    //                OnPropertyChanged("LineThickness");
    //            }
    //        }
    //    }

    //    public ObjectColour LineColour
    //    {
    //        get { return _lineColour; }
    //        set
    //        {
    //            if (_lineColour != value)
    //            {
    //                _lineColour = value;
    //                OnPropertyChanged("LineColour");
    //            }
    //        }
    //    }

    //    public BoxFaceType BoxFaces
    //    {
    //        get
    //        {
    //            return _boxFaces;
    //        }
    //        set
    //        {
    //            if (_boxFaces != value)
    //            {
    //                _boxFaces = value;
    //                OnPropertyChanged("BoxFaces");
    //            }
    //        }
    //    }

    //    public Double BoxFaceThickness
    //    {
    //        get
    //        {
    //            return _boxFaceThickness;
    //        }
    //        set
    //        {
    //            if (_boxFaceThickness != value)
    //            {
    //                _boxFaceThickness = value;
    //                OnPropertyChanged("BoxFaceThickness");
    //            }
    //        }
    //    }

    //    #endregion

    //    #region Constructor

    //    public ModelConstructPart3DData(ModelConstruct3DData parentData)
    //        :this(parentData, ModelPartType.Box)
    //    {}
    //    public ModelConstructPart3DData(ModelConstruct3DData parentData, ModelPartType partType)
    //    {
    //        _parentModelData = parentData;
    //        _partType = partType;
    //    }

    //    #endregion

    //    #region Methods

    //    public void SetMaterial(ObjectColour newColour, Boolean isTiled)
    //    {
    //        ObjectMaterial3D mat = new ObjectMaterial3D(); 
    //        mat.FillColour = newColour;

    //        if (isTiled || this.Data is ITiled)
    //        {
    //            this.LineThickness = 1;
    //            this.LineColour = ObjectColour.Black;
    //        }

    //        this.Material = mat;

    //    }

    //    #endregion

    //    #region IModelConstructPart3DData Members

    //    IEnumerable<IModelConstructPart3DCutout> IModelConstructPart3DData.CutoutDefinitions
    //    {
    //        get { return _cutoutDefinitions; }
    //    }

    //    #endregion

    //    #region IEditableObject Members

    //    private Boolean _isEditing;
    //    public Boolean IsEditing
    //    {
    //        get { return _isEditing; }
    //        private set
    //        {
    //            _isEditing = value;
    //            OnPropertyChanged("IsEditing");
    //        }

    //    }

    //    public void BeginEdit()
    //    {
    //        if (!this.IsEditing)
    //        {
    //            this.IsEditing = true;
    //        }
    //    }

    //    public void EndEdit()
    //    {
    //        if (this.IsEditing)
    //        {
    //            this.IsEditing = false;
    //        }
    //    }

    //    #endregion

    //    #region INotifyPropertyChanged Members

    //    public event PropertyChangedEventHandler PropertyChanged;

    //    protected virtual void OnPropertyChanged(String propertyName)
    //    {
    //        if (PropertyChanged != null)
    //        {
    //            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
    //        }
    //    }

    //    #endregion

    //    #region IDisposable Members

    //    protected Boolean IsDisposed;

    //    public virtual void Dispose()
    //    {
    //        if (!IsDisposed)
    //        {
    //            DisposeBase();

    //            IsDisposed = true;
    //            GC.SuppressFinalize(this);
    //        }
    //    }

    //    protected virtual void DisposeBase()
    //    {

    //    }

    //    #endregion

    //}

    #endregion
}
