﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion

using System;
using System.ComponentModel;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;
using Galleria.Ccm.Model;
using System.Windows;

//TODO: Classes under this namespace will eventually move into the framework.
namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData
{
    /// <summary>
    /// Denotes the faces that a label may attach to.
    /// </summary>
    public enum LabelAttachFace
    {
        Front,
        Top
    }

    /// <summary>
    /// Interface defining data to be used to create a ModelLabel3D
    /// </summary>
    public interface IModelLabel3DData : INotifyPropertyChanged, IDisposable
    {
        /// <summary>
        /// Gets/Sets whether this model is visible.
        /// </summary>
        Boolean IsVisible { get; set; }

        /// <summary>
        /// Gets/Sets the size of the model
        /// </summary>
        ObjectSize3D MaxSize { get; set; }

        /// <summary>
        /// Gets/Sets the label text to display
        /// </summary>
        String Text { get; set; }

        /// <summary>
        /// Gets/Sets the background colour of the label
        /// </summary>
        ObjectColour BackgroundColour { get; set; }

        float BackgroundTransparency { get; set; }

        /// <summary>
        /// Gets/Sets the colour of the background
        /// </summary>
        ObjectColour BorderColour { get; set; }

        /// <summary>
        /// Gets/Sets the thickness of the border.
        /// </summary>
        Double BorderThickness { get; set; }

        /// <summary>
        /// Gets/Sets the size of the font.
        /// </summary>
        Double FontSize { get; set; }

        /// <summary>
        /// Gets/Sets the colour of the text
        /// </summary>
        ObjectColour TextColour { get; set; }

        /// <summary>
        /// Gets/Sets the name of the font to use.
        /// </summary>
        String FontName { get; set; }

        /// <summary>
        /// Gets/Sets whether the label font may be shrunk to fit
        /// into the available space.
        /// </summary>
        Boolean ShrinkFontToFit { get; set; }

        /// <summary>
        /// Gets/Sets the minimum font side that the label
        /// text may shrink to.
        /// </summary>
        Double MinFontSize { get; set; }

        /// <summary>
        /// Gets/Sets whether the label may be rotated to
        /// fit into the available space.
        /// </summary>
        Boolean RotateFontToFit { get; set; }


        /// <summary>
        /// Gets/Sets how the label text should wrap
        /// accross lines.
        /// </summary>
        LabelTextWrapping TextWrapping { get; set; }


        /// <summary>
        /// Gets/Sets the offset of the label
        /// </summary>
        ObjectPoint3D Offset { get; set; }

        /// <summary>
        /// The label margin
        /// </summary>
        Double Margin { get; set; }


        /// <summary>
        /// Gets/Sets the alignment of the label.
        /// </summary>
        TextAlignment Alignment { get; set; }

        /// <summary>
        /// Gets/Sets which face of the parent model the
        /// label should attach to.
        /// </summary>
        LabelAttachFace AttachFace { get; set; }

        Boolean TextBoxFontBold { get; set; }

        Boolean TextBoxFontItalic { get; set; }

        Boolean TextBoxFontUnderlined { get; set; }

        LabelHorizontalPlacementType LabelHorizontalPlacement { get; set; }

        LabelVerticalPlacementType LabelVerticalPlacement { get; set; }

        LabelTextDirectionType TextDirection { get; set; }

        Boolean ShowOverImages { get; set; }

        Boolean ShowLabelPerFacing { get; set; }

        Boolean HasImage { get; set; }

        Boolean IsEditing { get; }
        void BeginEdit();
        void EndEdit();
    }
}
