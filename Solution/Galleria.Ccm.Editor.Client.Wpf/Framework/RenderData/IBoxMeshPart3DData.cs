﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
#endregion
#endregion

using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;

namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData
{
    public interface IBoxMeshPart3DData : IModelConstructPart3DData
    {
        MeshBuilderInstructions Builder { get; }

        ObjectMaterial3D BackFaceMaterial { get; set; }
        ObjectMaterial3D TopFaceMaterial { get; set; }
        ObjectMaterial3D BottomFaceMaterial { get; set; }
        ObjectMaterial3D LeftFaceMaterial { get; set; }
        ObjectMaterial3D RightFaceMaterial { get; set; }

        ObjectSize3D MaterialTileSize { get; set; }
    }
}
