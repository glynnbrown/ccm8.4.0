﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
#endregion
#endregion


namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData
{
    public interface IMeshPart3DData : IModelConstructPart3DData
    {
        MeshBuilderInstructions Builder { get; }
    }
}
