﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;

//TODO: Classes under this namespace will eventually move into the framework.
namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData
{
    /// <summary>
    /// Enum denoting the different file types
    /// supported by a FileModelConstructPart3D
    /// </summary>
    public enum FileModelPartType
    {
        studio3ds,
        obj,
        lwo,
        stl,
        off
    }

    /// <summary>
    /// Interface defining members required to create the data for
    /// a FileModelConstructPart3D
    /// </summary>
    public interface IFileModelPart3DData : IModelConstructPart3DData
    {
        FileModelPartType FileType { get; set; }
        Byte[] FileData { get; set; }
    }
}
