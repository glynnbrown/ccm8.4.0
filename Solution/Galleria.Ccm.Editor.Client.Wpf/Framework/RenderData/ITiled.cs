﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion

using System;

//TODO: Classes under this namespace will eventually move into the framework.
namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData
{
    /// <summary>
    /// Interface defining a class containing tiled data
    /// </summary>
    public interface ITiled
    {
        Int32 Wide { get; set; } //Number of Objects wide to tile
        Int32 High { get; set; } //Number of Objects high to tile
        Int32 Deep { get; set; } //Number of Objects deep to tile
        Int32 HollowStart { get; set; } //Number of layers to draw internaly before stopping (making the centre hollow)

        Double FirstRowReduction { get; set; }

        Double SpacingX { get; set; } //x spacing between each tile.
        Double SpacingY { get; set; } //Y spacing between each tile.
        Double SpacingZ { get; set; } //z spacing betweem each tile
        Boolean IsSingleUnitRotation { get; set; } //If true, the part rotation will be applied to an individual unit
    }

    public class TileData : ITiled
    {
        #region Fields

        private Int32 _wide;
        private Int32 _high;
        private Int32 _deep;
        private Int32 _hollowStart;
        private Double _spacingX;
        private Double _spacingY;
        private Double _spacingZ;
        private Boolean _isSingleUnitRotation = true;
        private Double _firstRowReduction = 0;

        #endregion

        #region Properties

        /// <summary>
        /// Number of Objects wide to tile
        /// </summary>
        public Int32 Wide
        {
            get { return _wide; }
            set { _wide = value; }
        }

        /// <summary>
        /// Number of Objects high to tile
        /// </summary>
        public Int32 High
        {
            get { return _high; }
            set { _high = value; }
        }

        /// <summary>
        /// Number of Objects deep to tile
        /// </summary>
        public Int32 Deep
        {
            get { return _deep; }
            set { _deep = value; }
        }

        /// <summary>
        /// Number of layers to draw internaly before stopping (making the centre hollow)
        /// </summary>
        public Int32 HollowStart
        {
            get { return _hollowStart; }
            set { _hollowStart = value; }
        }

        /// <summary>
        /// x spacing between each tile.
        /// </summary>
        public Double SpacingX
        {
            get { return _spacingX; }
            set { _spacingX = value; }
        }

        /// <summary>
        /// Y spacing between each tile.
        /// </summary>
        public Double SpacingY
        {
            get { return _spacingY; }
            set { _spacingY = value; }
        }

        /// <summary>
        /// Z spacing between each tile
        /// </summary>
        public Double SpacingZ
        {
            get { return _spacingZ; }
            set { _spacingZ = value; }
        }

        /// <summary>
        /// If true, the part rotation will be applied to an individual unit
        /// </summary>
        public Boolean IsSingleUnitRotation
        {
            get { return _isSingleUnitRotation; }
            set { _isSingleUnitRotation = value; }
        }

        public Double FirstRowReduction
        {
            get { return _firstRowReduction; }
            set { _firstRowReduction = value; }
        }

        #endregion

        #region Constructor

        public TileData()
        { }

        #endregion
    }
}
