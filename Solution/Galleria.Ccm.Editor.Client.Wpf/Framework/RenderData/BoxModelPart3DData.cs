﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;
using System.Collections.ObjectModel;

//TODO: Classes under this namespace will eventually move into the framework.
namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData
{
    /// <summary>
    /// Implementation of IBoxModelPart3DData
    /// </summary>
    public class BoxModelPart3DData : ModelConstructPart3DData, IBoxModelPart3DData
    {
        #region Fields

        private BoxFaceType _boxFaces = BoxFaceType.All;
        private Double _boxFaceThickness = 0;
        private ObjectMaterial3D _backFaceMaterial;
        private ObjectMaterial3D _topFaceMaterial;
        private ObjectMaterial3D _bottomFaceMaterial;
        private ObjectMaterial3D _leftFaceMaterial;
        private ObjectMaterial3D _rightFaceMaterial;

        private Boolean _renderCutouts = false;
        private readonly ObservableCollection<IModelConstructPart3DCutout> _cutoutDefinitions = new ObservableCollection<IModelConstructPart3DCutout>();
        private Object _data = null;

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets which faces of the box should be rendered
        /// </summary>
        public BoxFaceType BoxFaces
        {
            get
            {
                return _boxFaces;
            }
            set
            {
                if (_boxFaces != value)
                {
                    _boxFaces = value;
                    OnPropertyChanged("BoxFaces");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the thickness of the rendered faces.
        /// </summary>
        public Double BoxFaceThickness
        {
            get
            {
                return _boxFaceThickness;
            }
            set
            {
                if (_boxFaceThickness != value)
                {
                    _boxFaceThickness = value;
                    OnPropertyChanged("BoxFaceThickness");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the material to use for the back face
        /// </summary>
        public ObjectMaterial3D BackFaceMaterial
        {
            get { return _backFaceMaterial; }
            set
            {
                if (_backFaceMaterial != value)
                {
                    _backFaceMaterial = value;
                    OnPropertyChanged("BackFaceMaterial");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the material to use for the top face
        /// </summary>
        public ObjectMaterial3D TopFaceMaterial
        {
            get { return _topFaceMaterial; }
            set
            {
                if (_topFaceMaterial != value)
                {
                    _topFaceMaterial = value;
                    OnPropertyChanged("TopFaceMaterial");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the material to use for the bottom face
        /// </summary>
        public ObjectMaterial3D BottomFaceMaterial
        {
            get { return _bottomFaceMaterial; }
            set
            {
                if (_bottomFaceMaterial != value)
                {
                    _bottomFaceMaterial = value;
                    OnPropertyChanged("BottomFaceMaterial");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the material to use for the left face
        /// </summary>
        public ObjectMaterial3D LeftFaceMaterial
        {
            get { return _leftFaceMaterial; }
            set
            {
                if (_leftFaceMaterial != value)
                {
                    _leftFaceMaterial = value;
                    OnPropertyChanged("LeftFaceMaterial");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the material to use for the right face.
        /// </summary>
        public ObjectMaterial3D RightFaceMaterial
        {
            get { return _rightFaceMaterial; }
            set
            {
                if (_rightFaceMaterial != value)
                {
                    _rightFaceMaterial = value;
                    OnPropertyChanged("RightFaceMaterial");
                }
            }
        }

        /// <summary>
        /// Returns the editable collection of cutout parts
        /// </summary>
        public ObservableCollection<IModelConstructPart3DCutout> CutoutDefinitions
        {
            get { return _cutoutDefinitions; }
        }

        /// <summary>
        /// Gets/Sets whether cutouts should be applied
        /// </summary>
        public Boolean RenderCutouts
        {
            get { return _renderCutouts; }
            set
            {
                if (_renderCutouts != value)
                {
                    _renderCutouts = value;
                    OnPropertyChanged("RenderCutouts");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the model data
        /// </summary>
        /// <remarks>TODO: Aim to get rid of this.</remarks>
        public Object Data
        {
            get { return _data; }
            set
            {
                _data = value;
                OnPropertyChanged("Data");
            }
        }

        #endregion

        #region Constructor

        public BoxModelPart3DData(ModelConstruct3DData parentData)
            : base(parentData)
        {

            ObjectMaterial3D mat = this.Material;
            _backFaceMaterial = mat;
            _topFaceMaterial = mat;
            _bottomFaceMaterial = mat;
            _leftFaceMaterial = mat;
            _rightFaceMaterial = mat;
        }

        #endregion

        #region Methods

        public override void SetMaterial(ObjectMaterial3D mat)
        {
            Boolean wasEditing = this.IsEditing;
            if (!wasEditing)
            {
                BeginEdit();
            }

            this.Material = mat;
            this.BackFaceMaterial = mat;
            this.TopFaceMaterial = mat;
            this.BottomFaceMaterial = mat;
            this.LeftFaceMaterial = mat;
            this.RightFaceMaterial = mat;


            if (!wasEditing)
            {
                EndEdit();
            }
        }

        public override void SetMaterial(ObjectColour newColour)
        {
            SetMaterial(newColour, false);
        }

        public void SetMaterial(ObjectColour newColour, Boolean isTiled)
        {
            Boolean wasEditing = this.IsEditing;
            if (!wasEditing)
            {
                BeginEdit();
            }

            ObjectMaterial3D mat = new ObjectMaterial3D();
            mat.FillColour = newColour;
            this.Material = mat;

            //update all materials to match the main
            this.BackFaceMaterial = mat;
            this.TopFaceMaterial = mat;
            this.BottomFaceMaterial = mat;
            this.LeftFaceMaterial = mat;
            this.RightFaceMaterial = mat;

            if (isTiled || this.Data is ITiled)
            {
                this.LineThickness = 1;
                this.LineColour = ObjectColour.Black;
            }


            if (!wasEditing)
            {
                EndEdit();
            }
        }

        public void SetMaterials(Dictionary<BoxFaceType, ObjectMaterial3D> materials)
        {
            Boolean wasEditing = this.IsEditing;
            if (!wasEditing)
            {
                BeginEdit();
            }

            this.Material = materials[BoxFaceType.Front];
            this.BackFaceMaterial = materials[BoxFaceType.Back];
            this.TopFaceMaterial = materials[BoxFaceType.Top];
            this.BottomFaceMaterial = materials[BoxFaceType.Bottom];
            this.LeftFaceMaterial = materials[BoxFaceType.Left];
            this.RightFaceMaterial = materials[BoxFaceType.Right];

            if (!wasEditing)
            {
                EndEdit();
            }
        }

        #endregion

        #region IModelConstructPart3DData Members

        IEnumerable<IModelConstructPart3DCutout> IBoxModelPart3DData.CutoutDefinitions
        {
            get { return _cutoutDefinitions; }
        }

        #endregion
    }
}
