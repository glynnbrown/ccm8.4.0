﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion

using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Collections.Generic;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;

//TODO: Classes under this namespace will eventually move into the framework.
namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData
{
    /// <summary>
    /// Interface defining data to be used to create a ModelConstruct3D
    /// </summary>
    public interface IModelConstruct3DData : INotifyPropertyChanged, IDisposable
    {
        IModelConstruct3DData ParentModelData { get; }

        /// <summary>
        /// Gets/Sets whether the model should be visible
        /// </summary>
        Boolean IsVisible { get; set; }

        /// <summary>
        /// Get/Sets whether the model should be rendered as wireframe
        /// </summary>
        Boolean IsWireframe { get; set; }

        /// <summary>
        /// Gets/Sets the position of the model
        /// </summary>
        ObjectPoint3D Position { get; set; }

        /// <summary>
        /// Gets/Sets the rotation of the model
        /// </summary>
        ObjectRotation3D Rotation { get; set; }

        ///// <summary>
        ///// Gets/Sets whether this model should use a center 
        ///// pivot point. If false, the origin will be used.
        ///// </summary>
        //Boolean IsCenterPivot { get; set; }

        /// <summary>
        /// Gets/Sets the size of the model
        /// </summary>
        ObjectSize3D Size { get; set; }

        /// <summary>
        /// Gets/Sets an optional data tag.
        /// </summary>
        Object Data { get; set; }

        ReadOnlyObservableCollection<IModelConstruct3DData> ModelChildren { get; }
        ReadOnlyObservableCollection<IModelConstructPart3DData> ModelParts { get; }
        ReadOnlyObservableCollection<IModelLabel3DData> ModelLabels { get; }

        Boolean RemoveChild(IModelConstruct3DData child);
        List<IModelConstruct3DData> GetAllChildModels();
        List<IModelConstructPart3DData> GetAllParts();
    }
}
