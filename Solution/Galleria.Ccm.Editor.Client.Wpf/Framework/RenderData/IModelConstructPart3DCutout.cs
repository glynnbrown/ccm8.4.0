﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;

//TODO: Classes under this namespace will eventually move into the framework.
namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData
{
    /// <summary>
    /// Enum denoting the available cutout shape types.
    /// </summary>
    public enum CutoutShapeType
    {
        Circle,
        Rectangle,
        H
    }

    /// <summary>
    /// Interface defining memebers required to create a 
    /// cutout on a ModelConstructPart3D
    /// </summary>
    public interface IModelConstructPart3DCutout
    {
        CutoutShapeType ShapeType { get; set; }
        ObjectPoint3D Center { get; set; }
        Double Height { get; set; }
        Double Width { get; set; }
        Double Depth { get; set; }
    }

    /// <summary>
    /// Implementation of IModelConstructPart3DCutout
    /// </summary>
    public class ModelConstructPart3DCutout : IModelConstructPart3DCutout
    {
        public CutoutShapeType ShapeType { get; set; }
        public ObjectPoint3D Center { get; set; }
        public Double Height { get; set; }
        public Double Width { get; set; }
        public Double Depth { get; set; }
    }
}
