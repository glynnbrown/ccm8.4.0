﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;

//TODO: Classes under this namespace will eventually move into the framework.
namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData
{
    /// <summary>
    /// Base class implementation of IModelConstruct3DData
    /// </summary>
    public class ModelConstruct3DData : IModelConstruct3DData
    {
        #region Fields
        private IModelConstruct3DData _parentModelData;
        private Boolean _isVisible = true;
        private Boolean _isWireframe = false;
        private ObjectPoint3D _position = new ObjectPoint3D();
        private ObjectRotation3D _rotation = new ObjectRotation3D();
        //private Boolean _isCenterPivot = false;
        private ObjectSize3D _size = new ObjectSize3D();
        private Object _data = null;

        private readonly ObservableCollection<IModelConstruct3DData> _modelChildren = new ObservableCollection<IModelConstruct3DData>();
        private readonly ReadOnlyObservableCollection<IModelConstruct3DData> _modelChildrenRO;

        private readonly ObservableCollection<IModelConstructPart3DData> _modelParts = new ObservableCollection<IModelConstructPart3DData>();
        private readonly ReadOnlyObservableCollection<IModelConstructPart3DData> _modelPartsRO;

        private readonly ObservableCollection<IModelLabel3DData> _modelLabels = new ObservableCollection<IModelLabel3DData>();
        private readonly ReadOnlyObservableCollection<IModelLabel3DData> _modelLabelsRO;

        #endregion

        #region Properties

        public IModelConstruct3DData ParentModelData
        {
            get { return _parentModelData; }
            private set
            {
                _parentModelData = value;
            }
        }

        /// <summary>
        /// Gets/Sets whether the model should be visible
        /// </summary>
        public Boolean IsVisible
        {
            get { return _isVisible; }
            set
            {
                if (_isVisible != value)
                {
                    _isVisible = value;
                    OnPropertyChanged("IsVisible");
                }
            }
        }

        /// <summary>
        /// Get/Sets whether the model should be rendered as wireframe
        /// </summary>
        public Boolean IsWireframe
        {
            get { return _isWireframe; }
            set
            {
                if (_isWireframe != value)
                {
                    _isWireframe = value;
                    OnPropertyChanged("IsWireframe");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the position of the model
        /// </summary>
        public ObjectPoint3D Position
        {
            get { return _position; }
            set
            {
                if (_position != value)
                {
                    _position = value;
                    OnPropertyChanged("Position");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the rotation of the model
        /// </summary>
        public ObjectRotation3D Rotation
        {
            get { return _rotation; }
            set
            {
                if (_rotation != value)
                {
                    _rotation = value;
                    OnPropertyChanged("Rotation");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the size of the model
        /// </summary>
        public ObjectSize3D Size
        {
            get { return _size; }
            set
            {
                if (_size != value)
                {
                    _size = value;
                    OnPropertyChanged("Size");
                }
            }
        }

        /// <summary>
        /// Gets/Sets an optional data tag.
        /// </summary>
        public Object Data
        {
            get { return _data; }
            set
            {
                _data = value;
                OnPropertyChanged("Data");
            }

        }

        public ReadOnlyObservableCollection<IModelConstruct3DData> ModelChildren
        {
            get { return _modelChildrenRO; }
        }

        public ReadOnlyObservableCollection<IModelConstructPart3DData> ModelParts
        {
            get { return _modelPartsRO; }
        }

        public ReadOnlyObservableCollection<IModelLabel3DData> ModelLabels
        {
            get { return _modelLabelsRO; }
        }

        #endregion

        #region Constructor

        public ModelConstruct3DData()
        {
            _modelChildrenRO = new ReadOnlyObservableCollection<IModelConstruct3DData>(_modelChildren);
            _modelPartsRO = new ReadOnlyObservableCollection<IModelConstructPart3DData>(_modelParts);
            _modelLabelsRO = new ReadOnlyObservableCollection<IModelLabel3DData>(_modelLabels);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns all child models underneath this one.
        /// </summary>
        /// <returns></returns>
        public List<IModelConstruct3DData> GetAllChildModels()
        {
            return GetAllChildModels(this);
        }

        /// <summary>
        /// Enumerates through all child models.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IModelConstruct3DData> EnumerateChildModels()
        {
            foreach (var i in EnumerateChildModels(this))
            {
                yield return i;
            }
        }

        /// <summary>
        /// Returns all child parts underneath this one.
        /// </summary>
        /// <returns></returns>
        public List<IModelConstructPart3DData> GetAllParts()
        {
            return GetAllModelParts(this);
        }

        /// <summary>
        /// Adds a child model
        /// </summary>
        /// <param name="child"></param>
        public void AddChild(ModelConstruct3DData child)
        {
            child.ParentModelData = this;
            _modelChildren.Add(child);
        }

        /// <summary>
        /// Removes the requested child model
        /// </summary>
        /// <param name="child"></param>
        public Boolean RemoveChild(IModelConstruct3DData child)
        {
            Boolean childFound = false;

            if (this.ModelChildren.Contains(child))
            {
                _modelChildren.Remove(child);

                if (child is IDisposable)
                {
                    ((IDisposable)child).Dispose();
                }

                childFound = true;
            }
            else
            {
                //pass the request down
                foreach (var modelChild in this.ModelChildren)
                {
                    if (modelChild is IModelConstruct3DData)
                    {
                        childFound = ((IModelConstruct3DData)modelChild).RemoveChild(child);
                    }

                    if (childFound) break;
                }
            }

            return childFound;
        }

        /// <summary>
        /// Adds a child part
        /// </summary>
        /// <param name="part"></param>
        public void AddPart(IModelConstructPart3DData part)
        {
            _modelParts.Add(part);
        }

        /// <summary>
        /// Removes the requested part
        /// </summary>
        /// <param name="part"></param>
        public void RemovePart(IModelConstructPart3DData part)
        {
            _modelParts.Remove(part);

            if (part is IDisposable)
            {
                ((IDisposable)part).Dispose();
            }
        }

        /// <summary>
        /// Adds the given label.
        /// </summary>
        /// <param name="label"></param>
        public void AddLabel(IModelLabel3DData label)
        {
            _modelLabels.Add(label);
        }

        /// <summary>
        /// Removes the requested label.
        /// </summary>
        /// <param name="label"></param>
        public void RemoveLabel(IModelLabel3DData label)
        {
            _modelLabels.Remove(label);

            if (label is IDisposable)
            {
                ((IDisposable)label).Dispose();
            }
        }

        /// <summary>
        /// Clears out all child models and parts
        /// ensuring that each one is correctly disposed.
        /// </summary>
        protected void ClearAllChildren()
        {
            //call dispose on all children
            List<IDisposable> disposables = this.ModelChildren.OfType<IDisposable>().ToList();
            disposables.AddRange(this.ModelParts.OfType<IDisposable>());
            disposables.AddRange(this.ModelLabels.OfType<IDisposable>());

            _modelChildren.Clear();
            _modelParts.Clear();
            _modelLabels.Clear();

            foreach (var child in disposables)
            {
                child.Dispose();
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDisposable Members

        public Boolean IsDisposed { get; protected set; }

        public virtual void Dispose()
        {
            if (!IsDisposed)
            {
                DisposeBase();

                IsDisposed = true;
                GC.SuppressFinalize(this);
            }
        }

        protected virtual void DisposeBase()
        {
            ClearAllChildren();
            _parentModelData = null;
        }

        #endregion

        #region Static Helpers

        /// <summary>
        /// Returns all parts for the given model including those belonging to child models
        /// </summary>
        /// <returns></returns>
        public static List<IModelConstruct3DData> GetAllChildModels(IModelConstruct3DData model)
        {
            List<IModelConstruct3DData> allChildren = new List<IModelConstruct3DData>();
            allChildren.Add(model);

            foreach (IModelConstruct3DData childModel in model.ModelChildren)
            {
                allChildren.AddRange(GetAllChildModels(childModel));
            }

            return allChildren;
        }

        /// <summary>
        /// Returns all parts for the given model including those belonging to child models
        /// </summary>
        /// <returns></returns>
        public static List<IModelConstructPart3DData> GetAllModelParts(IModelConstruct3DData model)
        {
            List<IModelConstructPart3DData> allParts = new List<IModelConstructPart3DData>();
            allParts.AddRange(model.ModelParts);

            foreach (IModelConstruct3DData childModel in model.ModelChildren)
            {
                allParts.AddRange(GetAllModelParts(childModel));
            }

            return allParts;
        }

        /// <summary>
        /// Enumerates through the given model and all of its child models.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private static IEnumerable<IModelConstruct3DData> EnumerateChildModels(IModelConstruct3DData model)
        {
            yield return model;

            foreach (IModelConstruct3DData childModel in model.ModelChildren)
            {
                foreach (IModelConstruct3DData c in EnumerateChildModels(childModel))
                {
                    yield return c;
                }
            }
        }

        #endregion
    }
}
