﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;

namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData
{
    public class BoxMeshPart3DData : ModelConstructPart3DData, IBoxMeshPart3DData
    {
        #region Fields

        private ObjectMaterial3D _backFaceMaterial;
        private ObjectMaterial3D _topFaceMaterial;
        private ObjectMaterial3D _bottomFaceMaterial;
        private ObjectMaterial3D _leftFaceMaterial;
        private ObjectMaterial3D _rightFaceMaterial;

        private ObjectSize3D _materialTileSize;

        private MeshBuilderInstructions _builder;

        #endregion

        #region Properties

        public MeshBuilderInstructions Builder
        {
            get { return _builder; }
            set { _builder = value; }
        }


        /// <summary>
        /// Gets/Sets the material to use for the back face
        /// </summary>
        public ObjectMaterial3D BackFaceMaterial
        {
            get { return _backFaceMaterial; }
            set
            {
                if (_backFaceMaterial != value)
                {
                    _backFaceMaterial = value;
                    OnPropertyChanged("BackFaceMaterial");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the material to use for the top face
        /// </summary>
        public ObjectMaterial3D TopFaceMaterial
        {
            get { return _topFaceMaterial; }
            set
            {
                if (_topFaceMaterial != value)
                {
                    _topFaceMaterial = value;
                    OnPropertyChanged("TopFaceMaterial");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the material to use for the bottom face
        /// </summary>
        public ObjectMaterial3D BottomFaceMaterial
        {
            get { return _bottomFaceMaterial; }
            set
            {
                if (_bottomFaceMaterial != value)
                {
                    _bottomFaceMaterial = value;
                    OnPropertyChanged("BottomFaceMaterial");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the material to use for the left face
        /// </summary>
        public ObjectMaterial3D LeftFaceMaterial
        {
            get { return _leftFaceMaterial; }
            set
            {
                if (_leftFaceMaterial != value)
                {
                    _leftFaceMaterial = value;
                    OnPropertyChanged("LeftFaceMaterial");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the material to use for the right face.
        /// </summary>
        public ObjectMaterial3D RightFaceMaterial
        {
            get { return _rightFaceMaterial; }
            set
            {
                if (_rightFaceMaterial != value)
                {
                    _rightFaceMaterial = value;
                    OnPropertyChanged("RightFaceMaterial");
                }
            }
        }

        public ObjectSize3D MaterialTileSize
        {
            get
            {
                if (_materialTileSize.X > 0 && _materialTileSize.Y > 0
                    && _materialTileSize.Z > 0)
                {
                    return _materialTileSize;
                }
                return base.Size;
            }
            set
            {
                _materialTileSize = value;
                OnPropertyChanged("MaterialTileSize");
            }
        }

        #endregion

        #region Constructor

        public BoxMeshPart3DData(ModelConstruct3DData parentData)
            : base(parentData)
        {
            ObjectMaterial3D mat = this.Material;
            _backFaceMaterial = mat;
            _topFaceMaterial = mat;
            _bottomFaceMaterial = mat;
            _leftFaceMaterial = mat;
            _rightFaceMaterial = mat;
        }

        #endregion

        #region Methods

        public override void SetMaterial(ObjectMaterial3D mat)
        {
            Boolean wasEditing = this.IsEditing;
            if (!wasEditing)
            {
                BeginEdit();
            }

            this.Material = mat;
            this.BackFaceMaterial = mat;
            this.TopFaceMaterial = mat;
            this.BottomFaceMaterial = mat;
            this.LeftFaceMaterial = mat;
            this.RightFaceMaterial = mat;


            if (!wasEditing)
            {
                EndEdit();
            }
        }

        public override void SetMaterial(ObjectColour newColour)
        {
            SetMaterial(newColour, false);
        }

        public void SetMaterial(ObjectColour newColour, Boolean isTiled)
        {
            Boolean wasEditing = this.IsEditing;
            if (!wasEditing)
            {
                BeginEdit();
            }

            ObjectMaterial3D mat = new ObjectMaterial3D();
            mat.FillColour = newColour;
            this.Material = mat;

            //update all materials to match the main
            this.BackFaceMaterial = mat;
            this.TopFaceMaterial = mat;
            this.BottomFaceMaterial = mat;
            this.LeftFaceMaterial = mat;
            this.RightFaceMaterial = mat;

            if (isTiled)
            {
                this.LineThickness = 1;
                this.LineColour = ObjectColour.Black;
            }


            if (!wasEditing)
            {
                EndEdit();
            }
        }

        public void SetMaterials(Dictionary<BoxFaceType, ObjectMaterial3D> materials)
        {
            Boolean wasEditing = this.IsEditing;
            if (!wasEditing)
            {
                BeginEdit();
            }

            this.Material = materials[BoxFaceType.Front];
            this.BackFaceMaterial = materials[BoxFaceType.Back];
            this.TopFaceMaterial = materials[BoxFaceType.Top];
            this.BottomFaceMaterial = materials[BoxFaceType.Bottom];
            this.LeftFaceMaterial = materials[BoxFaceType.Left];
            this.RightFaceMaterial = materials[BoxFaceType.Right];

            if (!wasEditing)
            {
                EndEdit();
            }
        }

        #endregion
    }
}
