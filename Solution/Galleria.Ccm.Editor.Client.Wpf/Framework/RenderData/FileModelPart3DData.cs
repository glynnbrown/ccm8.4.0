﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion

using System;

//TODO: Classes under this namespace will eventually move into the framework.
namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData
{
    /// <summary>
    /// Implementation of IFileModelPart3DData
    /// </summary>
    public sealed class FileModelPart3DData : ModelConstructPart3DData, IFileModelPart3DData
    {
        #region Fields
        private FileModelPartType _fileType = FileModelPartType.studio3ds;
        private Byte[] _fileData = null;
        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the type of file held by the
        /// file data stream
        /// </summary>
        public FileModelPartType FileType
        {
            get { return _fileType; }
            set
            {
                if (_fileType != value)
                {
                    _fileType = value;
                    OnPropertyChanged("FileType");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the actual file data stream.
        /// </summary>
        public Byte[] FileData
        {
            get { return _fileData; }
            set
            {
                if (_fileData != value)
                {
                    _fileData = value;
                    OnPropertyChanged("FileData");
                }
            }
        }

        #endregion

        #region Constructor

        public FileModelPart3DData(ModelConstruct3DData parentData)
            : base(parentData)
        {
        }

        #endregion
    }

}
