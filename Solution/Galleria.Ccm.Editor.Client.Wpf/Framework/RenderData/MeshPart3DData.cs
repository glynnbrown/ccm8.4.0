﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion



namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData
{
    public class MeshPart3DData : ModelConstructPart3DData, IMeshPart3DData
    {
        #region Fields

        private MeshBuilderInstructions _builder;

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the mesh builder to use.
        /// </summary>
        public MeshBuilderInstructions Builder
        {
            get { return _builder; }
            set 
            {
                if (value != _builder)
                {
                    _builder = value;
                    OnPropertyChanged("Builder");
                }
            }
        }


        #endregion

        #region Constructor

        public MeshPart3DData(ModelConstruct3DData parentData)
            : base(parentData)
        {
        }

        #endregion

    }
}
