﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion

using System;
using System.ComponentModel;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;
using Galleria.Ccm.Model;
using System.Windows;

//TODO: Classes under this namespace will eventually move into the framework.
namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData
{
    /// <summary>
    /// Base class implementation of IModelLabel3DData
    /// </summary>
    public class ModelLabel3DData : IModelLabel3DData
    {
        #region Fields

        private Boolean _isVisible = true;
        private ObjectSize3D _maxSize = new ObjectSize3D(10,10,10);
        private String _text;
        private ObjectPoint3D _offset = new ObjectPoint3D();
        private ObjectColour _textColour = ObjectColour.Black;
        private ObjectColour _backgroundColour = ObjectColour.White;
        private ObjectColour _borderColour = ObjectColour.Black;
        private Double _borderThickness = 0.5;
        private Double _fontSize = 8;
        private Double _minFontSize = 2;
        private Boolean _shrinkFontToFit = true;
        private Boolean _rotateFontToFit = true;
        private TextAlignment _alignment = TextAlignment.Left;
        private String _fontName = "Arial";
        private Double _margin = 0.5D;
        private LabelAttachFace _attachFace = LabelAttachFace.Front;
        private LabelTextWrapping _textWrapping = LabelTextWrapping.Word;

        private Boolean _textBoxFontBold = false;
        private float _backgroundTransparency =1.0f;
        private Boolean _textBoxFontItalic = false;
        private Boolean _textBoxFontUnderlined = false;
        private LabelHorizontalPlacementType _labelHorizontalPlacement = LabelHorizontalPlacementType.Left;
        private LabelVerticalPlacementType _labelVerticalPlacement = LabelVerticalPlacementType.Top;
        private LabelTextDirectionType _textDirection = LabelTextDirectionType.Horizontal;
        private Boolean _showOverImages = false;
        private Boolean _showLabelPerFacing = false;
        private Boolean _hasImage = false;



        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets whether this model is visible.
        /// </summary>
        public Boolean IsVisible
        {
            get { return _isVisible; }
            set
            {
                if (_isVisible != value)
                {
                    _isVisible = value;
                    OnPropertyChanged("IsVisible");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the maximum size of this label.
        /// </summary>
        public ObjectSize3D MaxSize
        {
            get { return _maxSize; }
            set 
            {
                if (value != _maxSize)
                {
                    _maxSize = value;
                    OnPropertyChanged("MaxSize");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the label text to display
        /// </summary>
        public String Text
        {
            get { return _text; }
            set
            {
                if (_text != value)
                {
                    _text = value;
                    OnPropertyChanged("Text");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the background colour of the label
        /// </summary>
        public ObjectColour BackgroundColour
        {
            get { return _backgroundColour; }
            set
            {
                if (_backgroundColour != value)
                {
                    _backgroundColour = value;
                    OnPropertyChanged("BackgroundColour");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the colour of the background
        /// </summary>
        public ObjectColour BorderColour
        {
            get { return _borderColour; }
            set
            {
                if (_borderColour != value)
                {
                    _borderColour = value;
                    OnPropertyChanged("BorderColour");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the thickness of the border.
        /// </summary>
        public Double BorderThickness
        {
            get { return _borderThickness; }
            set
            {
                if (_borderThickness != value)
                {
                    _borderThickness = value;
                    OnPropertyChanged("BorderThickness");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the colour of the text
        /// </summary>
        public ObjectColour TextColour
        {
            get { return _textColour; }
            set
            {
                if (_textColour != value)
                {
                    _textColour = value;
                    OnPropertyChanged("TextColour");
                }
            }
        }

        /// <summary>
        /// Gets/Sets how the label text should wrap
        /// </summary>
        public LabelTextWrapping TextWrapping
        {
            get { return _textWrapping; }
            set
            {
                if (value != _textWrapping)
                {
                    _textWrapping = value;
                    OnPropertyChanged("TextWrapping");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the size of the font.
        /// </summary>
        public Double FontSize
        {
            get { return _fontSize; }
            set
            {
                if (_fontSize != value)
                {
                    _fontSize = value;
                    OnPropertyChanged("FontSize");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the name of the font to use.
        /// </summary>
        public String FontName
        {
            get { return _fontName; }
            set
            {
                if (_fontName != value)
                {
                    _fontName = value;
                    OnPropertyChanged("FontName");
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether the label may be rotated to
        /// fit into the available space.
        /// </summary>
        public Double MinFontSize
        {
            get { return _minFontSize; }
            set
            {
                if (_minFontSize != value)
                {
                    Object oldValue = _minFontSize;
                    _minFontSize = value;
                    OnPropertyChanged("MinFontSize");
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether the label font may be shrunk to fit
        /// into the available space.
        /// </summary>
        public Boolean ShrinkFontToFit
        {
            get { return _shrinkFontToFit; }
            set
            {
                if (_shrinkFontToFit != value)
                {
                    _shrinkFontToFit = value;
                    OnPropertyChanged("ShrinkFontToFit");
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether the label may be rotated to
        /// fit into the available space.
        /// </summary>
        public Boolean RotateFontToFit
        {
            get { return _rotateFontToFit; }
            set
            {
                if (_rotateFontToFit != value)
                {
                    _rotateFontToFit = value;
                    OnPropertyChanged("RotateFontToFit");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the offset of the label
        /// </summary>
        public ObjectPoint3D Offset
        {
            get { return _offset; }
            set
            {
                if (_offset != value)
                {
                    _offset = value;
                    OnPropertyChanged("Offset");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the alignment of the label.
        /// </summary>
        public TextAlignment Alignment
        {
            get { return _alignment; }
            set
            {
                if (_alignment != value)
                {
                    _alignment = value;
                    OnPropertyChanged("Alignment");
                }
            }
        }

        /// <summary>
        /// Gets/Sets which face of the parent model the
        /// label should attach to.
        /// </summary>
        public LabelAttachFace AttachFace
        {
            get { return _attachFace; }
            set
            {
                if (_attachFace != value)
                {
                    _attachFace = value;
                    OnPropertyChanged("AttachFace");
                }
            }
        }

        /// <summary>
        /// Gets/Sets the margin of the label
        /// </summary>
        public Double Margin
        {
            get { return _margin; }
            set
            {
                if (_margin != value)
                {
                    _margin = value;
                    OnPropertyChanged("Margin");
                }
            }
        }

        public Boolean TextBoxFontBold
        {
            get { return _textBoxFontBold; }
            set
            {
                if (_textBoxFontBold != value)
                {
                    _textBoxFontBold = value;
                    OnPropertyChanged("TextBoxFontBold");
                }
            }
        }

        public float BackgroundTransparency
        {
            get { return _backgroundTransparency; }
            set
            {
                if (_backgroundTransparency != value)
                {
                    _backgroundTransparency = value;
                    OnPropertyChanged("BackgroundTransparency");
                }
            }
        }

        public Boolean TextBoxFontItalic
        {
            get { return _textBoxFontItalic; }
            set
            {
                if (_textBoxFontItalic != value)
                {
                    _textBoxFontItalic = value;
                    OnPropertyChanged("TextBoxFontItalic");
                }
            }
        }

        public Boolean TextBoxFontUnderlined
        {
            get { return _textBoxFontUnderlined; }
            set
            {
                if (_textBoxFontUnderlined != value)
                {
                    _textBoxFontUnderlined = value;
                    OnPropertyChanged("TextBoxFontUnderlined");
                }
            }
        }

        public LabelHorizontalPlacementType LabelHorizontalPlacement
        {
            get { return _labelHorizontalPlacement; }
            set
            {
                if (_labelHorizontalPlacement != value)
                {
                    _labelHorizontalPlacement = value;
                    OnPropertyChanged("LabelHorizontalPlacement");
                }
            }
        }

        public LabelVerticalPlacementType LabelVerticalPlacement
        {
            get { return _labelVerticalPlacement; }
            set
            {
                if (_labelVerticalPlacement != value)
                {
                    _labelVerticalPlacement = value;
                    OnPropertyChanged("LabelVerticalPlacement");
                }
            }
        }

        public LabelTextDirectionType TextDirection
        {
            get { return _textDirection; }
            set
            {
                if (_textDirection != value)
                {
                    _textDirection = value;
                    OnPropertyChanged("TextDirection");
                }
            }
        }

        public Boolean ShowOverImages
        {
            get { return _showOverImages; }
            set
            {
                if (_showOverImages != value)
                {
                    _showOverImages = value;
                    OnPropertyChanged("ShowOverImages");
                }
            }
        }

        public Boolean ShowLabelPerFacing
        {
            get { return _showLabelPerFacing; }
            set
            {
                if (_showLabelPerFacing != value)
                {
                    _showLabelPerFacing = value;
                    OnPropertyChanged("ShowLabelPerFacing");
                }
            }
        }

        public Boolean HasImage
        {
            get { return _hasImage; }
            set
            {
                if (_hasImage != value)
                {
                    _hasImage = value;
                    OnPropertyChanged("HasImage");
                }
            }
        }

        ///// <summary>
        ///// Gets a reference to the parent model data.
        ///// </summary>
        //public IModelConstruct3DData ParentModelData
        //{
        //    get { return _parentModelData; }
        //}

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ModelLabel3DData() { }

        #endregion

        #region IEditableObject Members

        private Boolean _isEditing;

        /// <summary>
        /// Returns true if the label is currently in editing mode.
        /// </summary>
        public Boolean IsEditing
        {
            get { return _isEditing; }
            private set
            {
                _isEditing = value;
                OnPropertyChanged("IsEditing");
            }

        }

        /// <summary>
        /// Puts this model into an editing state.
        /// </summary>
        public void BeginEdit()
        {
            if (!this.IsEditing)
            {
                this.IsEditing = true;
            }
        }

        /// <summary>
        /// Ends any active edit state on this model.
        /// </summary>
        public void EndEdit()
        {
            if (this.IsEditing)
            {
                this.IsEditing = false;
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        /// <summary>
        /// Event to notify when the value of a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Caled whenever a property changes.
        /// </summary>
        /// <param name="propertyName"></param>
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDisposable Members

        protected Boolean IsDisposed;

        public virtual void Dispose()
        {
            if (!IsDisposed)
            {
                DisposeBase();

                IsDisposed = true;
                GC.SuppressFinalize(this);
            }
        }

        protected virtual void DisposeBase()
        {

        }

        #endregion
    }
}
