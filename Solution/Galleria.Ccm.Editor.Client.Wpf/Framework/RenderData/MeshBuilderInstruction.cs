﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;

namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData
{
    public enum MeshBuilderInstructionType
    {
        AddBox,
        AddBoxFaces,
        AddNestableBox,
        AddJar,
        AddBottle,
        AddCan,
        RotateMesh,
        TileMesh
    }

    public class MeshBuilderInstruction
    {
        public MeshBuilderInstructionType InstructionType { get; private set; } 
        public Object[] Args { get; private set; }

        public MeshBuilderInstruction() { }

        public MeshBuilderInstruction(MeshBuilderInstructionType type, Object[] args)
        {
            InstructionType = type;
            Args = args;
        }

    }


    public sealed class MeshBuilderInstructions : MeshBuilderInstruction
    {
        #region Fields

        private List<MeshBuilderInstruction> _instructions = new List<MeshBuilderInstruction>();
        private Boolean _generateNormals;
        private Boolean _generateTextures;
        private ObjectPoint3D _appendPosition = new ObjectPoint3D();

        #endregion

        #region Properties

        public IEnumerable<MeshBuilderInstruction> Instructions
        {
            get { return _instructions; }
        }

        public Boolean GenerateNormals
        {
            get { return _generateNormals; }
        }

        public Boolean GenerateTextures
        {
            get { return _generateTextures; }
        }

        public ObjectPoint3D AppendPosition
        {
            get { return _appendPosition; }
        }

        #endregion

        #region Constructor

        public MeshBuilderInstructions(Boolean generateNormals, Boolean generateTextures)
        {
            _generateNormals = generateNormals;
            _generateTextures = generateTextures;
        }

        #endregion

        #region Methods

        public void Append(MeshBuilderInstructions instructions, ObjectPoint3D center)
        {
            instructions._appendPosition = center;
            _instructions.Add(instructions);
        }

        public void AddBox(Double boxWidth, Double boxHeight, Double boxDepth)
        {
            _instructions.Add(new MeshBuilderInstruction(MeshBuilderInstructionType.AddBox, new Object[3] { boxWidth, boxHeight, boxDepth }));
        }

        public void AddBoxFaces(ObjectPoint3D centerPoint, Double xlength, Double ylength, Double zlength, BoxFaceType faces)
        {
            _instructions.Add(new MeshBuilderInstruction(MeshBuilderInstructionType.AddBoxFaces,
                new Object[4] { centerPoint, ylength, zlength, faces }));
        }

        public void AddNestableBox(ObjectSize3D boxSize, ObjectSize3D nestSize, Boolean skew = true)
        {
            _instructions.Add(new MeshBuilderInstruction(MeshBuilderInstructionType.AddNestableBox, new Object[3] { boxSize, nestSize, skew }));
        }

        public void RotateMesh(ObjectRotation3D rotation)
        {
            _instructions.Add(new MeshBuilderInstruction(MeshBuilderInstructionType.RotateMesh, new Object[1] { rotation }));
        }

        public void TileMesh(Int32 high, Int32 wide, Int32 deep, Int32 hollowStart, ObjectPoint3D spacing)
        {
            _instructions.Add(new MeshBuilderInstruction(MeshBuilderInstructionType.TileMesh, new Object[5] { high, wide, deep, hollowStart, spacing }));
        }

        public void AddBottle(Double width, Double height, Double depth)
        {
            _instructions.Add(new MeshBuilderInstruction(MeshBuilderInstructionType.AddBottle, new Object[3] { width, height, depth }));
        }

        public void AddJar(Double width, Double height, Double depth)
        {
            _instructions.Add(new MeshBuilderInstruction(MeshBuilderInstructionType.AddJar, new Object[3] { width, height, depth }));
        }

        public void AddCan(Double width, Double height, Double depth)
        {
            _instructions.Add(new MeshBuilderInstruction(MeshBuilderInstructionType.AddCan, new Object[3] { width, height, depth }));
        }

        #endregion
    }

   
}
