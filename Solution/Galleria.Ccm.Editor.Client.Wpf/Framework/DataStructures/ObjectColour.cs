﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion

using System;

//TODO: Classes under this namespace will eventually move into the framework.
namespace Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures
{
    /// <summary>
    /// Structure holding values defining a colour.
    /// </summary>
    public struct ObjectColour
    {
        #region Properties

        public Byte Alpha { get; set; } // the colours alpha channel
        public Byte Red { get; set; } // the colours red channel
        public Byte Green { get; set; } // the colours green channel
        public Byte Blue { get; set; } // the colours blue channel

        #endregion

        #region Equality Overrides

        public override Boolean Equals(object obj)
        {
            return obj is ObjectColour && this == (ObjectColour)obj;
        }

        public override Int32 GetHashCode()
        {
            return base.GetHashCode();
        }

        public static Boolean operator ==(ObjectColour c1, ObjectColour c2)
        {
            if (c1.Alpha != c2.Alpha) { return false; }
            if (c1.Red != c2.Red) { return false; }
            if (c1.Green != c2.Green) { return false; }
            if (c1.Blue != c2.Blue) { return false; }
            return true;
        }

        public static Boolean operator !=(ObjectColour c1, ObjectColour c2)
        {
            return !(c1 == c2);
        }

        #endregion

        #region Static Helpers

        public static readonly ObjectColour White = NewColour(255, 255, 255, 255);
        public static readonly ObjectColour Black = NewColour(0, 0, 0, 255);


        public static ObjectColour NewColour(Single r, Single g, Single b)
        {
            ObjectColour output = new ObjectColour()
            {
                Red = Clamp(r * 255),
                Green = Clamp(g * 255),
                Blue = Clamp(b * 255),
                Alpha = (Byte)255
            };

            return output;
        }

        public static ObjectColour NewColour(Byte r, Byte g, Byte b, Byte a)
        {
            return
                new ObjectColour()
                {
                    Alpha = a,
                    Red = r,
                    Green = g,
                    Blue = b,
                };

            //Colour3D output = new Colour3D()
            //{
            //    Red = Clamp(R),
            //    Green = Clamp(G),
            //    Blue = Clamp(B),
            //    Alpha = Clamp(A)
            //};

            //return output;
        }

        public static ObjectColour NewColour(Int32 colourInt, Byte A)
        {
            var uint32Color = (((UInt32)Clamp(A)) << 24) |
                    (((UInt32)Clamp(((colourInt >> 0x10) & 0xff))) << 16) |
                    (((UInt32)Clamp(((colourInt >> 8) & 0xff))) << 8) |
                    (UInt32)Clamp((colourInt & 0xff));
            return IntToColor((Int32)uint32Color);
        }

        /// <summary>
        /// Convert colour int to Colour3D Color
        /// </summary>
        /// <param name="colorAsInt"></param>
        /// <returns></returns>
        public static ObjectColour IntToColor(Int32 colorAsInt)
        {
            if (colorAsInt == 0) 
            { 
                return ObjectColour.White; 
            }
            else
            {

                return ObjectColour.NewColour((Byte)((colorAsInt >> 16) & 0xff),
                    (Byte)((colorAsInt >> 8) & 0xff),
                    (Byte)((colorAsInt) & 0xff),
                    (Byte)((colorAsInt >> 24) & 0xff));
            }
        }

        /// <summary>
        /// Convert color to colour int
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static Int32 ColorToInt(ObjectColour colour)
        {
            var uint32Color = (((UInt32)colour.Alpha) << 24) |
                    (((UInt32)colour.Red) << 16) |
                    (((UInt32)colour.Green) << 8) |
                    (UInt32)colour.Blue;
            return (Int32)uint32Color;
        }

        /// <summary>
        /// Creates a random colour for the supplied string. This colour will
        /// always be the same for the same string.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static ObjectColour StringToColourInt(String text)
        {
            System.Text.Encoding unicode = System.Text.Encoding.Unicode;
            Byte[] charBytes = unicode.GetBytes(text);
            Int32 rCount = 1, gCount = 1, bCount = 1;
            Double rVal = charBytes[0];
            Double gVal = rVal, bVal = rVal;
            for (Int32 c = 1; c < charBytes.Length; c++)
            {
                if (((c + 1f) % 2f) == 0)
                {
                    rVal += charBytes[c];
                    rCount++;
                }
                if (((c + 1f) % 3f) == 0)
                {
                    gVal += charBytes[c];
                    gCount++;
                }
                if (((c + 1f) % 4f) == 0)
                {
                    bVal += charBytes[c];
                    bCount++;
                }
            }
            rVal /= rCount;
            gVal /= gCount;
            bVal /= bCount;

            return new ObjectColour()
            {
                Red = (Byte)(rVal * 255),
                Green = (Byte)(gVal * 255),
                Blue = (Byte)(bVal * 255),
                Alpha = (Byte)255
            };
        }

        /// <summary>
        /// Clamps the supplied value bettween 0 and 255
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static Byte Clamp(Single value)
        {
            Single output = Math.Max(value, 0);
            output = Math.Min(value, 255);
            return (Byte)(output);
        }

        #endregion
    }
}
