﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion

using System;

//TODO: Classes under this namespace will eventually move into the framework.
namespace Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures
{
    /// <summary>
    /// Represents a 3D material
    /// </summary>
    public struct ObjectMaterial3D
    {
        #region Fields
        private ObjectColour _fillColour;
        private Byte[] _imageData;
        private FillPatternType _fillPatternType;
        #endregion

        #region Properties

        /// <summary>
        /// The material colour
        /// </summary>
        public ObjectColour FillColour
        {
            get { return _fillColour; }
            set { _fillColour = value; }
        }

        /// <summary>
        /// The image material data.
        /// </summary>
        public Byte[] ImageData
        {
            get { return _imageData; }
            set { _imageData = value; }
        }

        /// <summary>
        /// The material fill pattern.
        /// </summary>
        public FillPatternType FillPattern
        {
            get { return _fillPatternType; }
            set { _fillPatternType = value; }
        }

        #endregion

        #region Constructors

        public ObjectMaterial3D(ObjectColour fill, FillPatternType pattern, Byte[] imageData)
        {
            _fillColour = fill;
            _fillPatternType = pattern;
            _imageData = imageData;
        }

        #endregion

        #region Equality Overrides

        public override Boolean Equals(object obj)
        {
            return obj is ObjectMaterial3D && this == (ObjectMaterial3D)obj;
        }

        public override Int32 GetHashCode()
        {
            return base.GetHashCode();
        }

        public static Boolean operator ==(ObjectMaterial3D p1, ObjectMaterial3D p2)
        {
            if (p1.FillColour != p2.FillColour) { return false; }
            if (p1.ImageData != p2.ImageData) { return false; }
            if (p1.FillPattern != p2.FillPattern) { return false; }
            return true;
        }

        public static Boolean operator !=(ObjectMaterial3D p1, ObjectMaterial3D p2)
        {
            return !(p1 == p2);
        }

        #endregion

        #region Static Helpers

        /// <summary>
        /// Helper method to create a simple diffuse material
        /// </summary>
        /// <param name="colour"></param>
        /// <returns></returns>
        public static ObjectMaterial3D CreateMaterial(ObjectColour colour)
        {
            ObjectMaterial3D mat = new ObjectMaterial3D();
            mat.FillColour = colour;
            return mat;
        }


        #endregion

        #region Nested Classes

        public enum FillPatternType
        {
            Solid,
            Border,
            Crosshatch,
            DiagonalUp,
            DiagonalDown,
            Horizontal,
            Vertical,
            Dotted
        }

        #endregion
    }

}
