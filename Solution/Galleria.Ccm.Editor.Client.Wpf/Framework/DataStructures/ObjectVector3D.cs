﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion

using System;
using System.Windows.Media.Media3D;
using System.Collections.Generic;

namespace Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures
{
    /// <summary>
    /// 3D vector implementation to allow use in silverlight.
    /// </summary>
    public struct ObjectVector3D
    {
        #region Fields

        private Single _x;
        private Single _y;
        private Single _z;

        #endregion

        #region Properties

        public Single X 
        { 
            get { return _x; } 
            set { _x = value; } 
        }

        public Single Y 
        { 
            get { return _y; } 
            set { _y = value; } 
        }
        
        public Single Z
        { 
            get { return _z; } 
            set { _z = value; } 
        }

        public Single Length
        {
            get { return Convert.ToSingle(Math.Sqrt(LengthSquared)); }
        }

        public Single LengthSquared
        {
            get { return _x * _x + _y * _y + _z * _z; }
        }

        #endregion

        #region Constructor

        public ObjectVector3D(Single x, Single y, Single z)
        {
            _x = x;
            _y = y;
            _z = z;
        }

        #endregion

        #region Methods

        public void Normalize()
        {
            Single xAbs = Math.Abs(_x);
            Single yAbs = Math.Abs(_y);
            Single zAbs = Math.Abs(_z);

            if (yAbs > xAbs)
            {
                xAbs = yAbs;
            }

            if (zAbs > xAbs)
            {
                xAbs = zAbs;
            }

            _x /= xAbs;
            _y /= xAbs;
            _z /= xAbs;

            Single length = this.Length;

            this.X *= (1F / length);
            this.Y *= (1F / length);
            this.Z *= (1F / length);
        }

        public static ObjectVector3D CrossProduct(ObjectVector3D p1, ObjectVector3D p2)
        {
            return
                new ObjectVector3D(
                    p1._y * p2._z - p1._z * p2._y,
                    p1._z * p2._x - p1._x * p2._z,
                    p1._x * p2._y - p1._y * p2._x);
        }

        public ObjectVector3D FindAnyPerpendicular()
        {
            Normalize();
            ObjectVector3D u = ObjectVector3D.CrossProduct(new ObjectVector3D(0, 1, 0), this);

            if (u.LengthSquared < 1e-3)
            {
                u = ObjectVector3D.CrossProduct(new ObjectVector3D(1, 0, 0), this);
            }

            return u;
        }

        public ObjectVector3D Transform(IEnumerable<Matrix3D> matricies)
        {
            Matrix3D final = Matrix3D.Identity;

            foreach (Matrix3D matrix in matricies)
            {
                final *= matrix;
            }

            return Transform(final);
        }

        public ObjectVector3D Transform(Matrix3D matrix)
        {
            return new ObjectVector3D(
                (X * Convert.ToSingle(matrix.M11)) 
                + (Y * Convert.ToSingle(matrix.M21)) 
                + (Z * Convert.ToSingle(matrix.M31)) 
                + Convert.ToSingle(matrix.OffsetX),

                (X * Convert.ToSingle(matrix.M12))
                + (Y * Convert.ToSingle(matrix.M22)) 
                + (Z * Convert.ToSingle(matrix.M32))
                + Convert.ToSingle(matrix.OffsetY),

                (X * Convert.ToSingle(matrix.M13)) 
                + (Y * Convert.ToSingle(matrix.M23))
                + (Z * Convert.ToSingle(matrix.M33))
                + Convert.ToSingle(matrix.OffsetZ)
                );
        }

        #endregion

        #region Overrides

        public override Boolean Equals(object obj)
        {
            return obj is ObjectVector3D && this == (ObjectVector3D)obj;
        }

        public override Int32 GetHashCode()
        {
            return base.GetHashCode();
        }

        public static Boolean operator ==(ObjectVector3D p1, ObjectVector3D p2)
        {
            if (p1.X != p2.X) { return false; }
            if (p1.Y != p2.Y) { return false; }
            if (p1.Z != p2.Z) { return false; }
            return true;
        }

        public static Boolean operator !=(ObjectVector3D p1, ObjectVector3D p2)
        {
            return !(p1 == p2);
        }




        public static ObjectVector3D operator +(ObjectVector3D v, ObjectVector3D p)
        {
            return new ObjectVector3D(v._x + p._x, v._y + p._y, v._z + p._z);
        }

        public static ObjectPoint3D operator +(ObjectVector3D v, ObjectPoint3D p)
        {
            return new ObjectPoint3D(v._x + p.X, v._y + p.Y, v._z + p.Z);
        }
        public static ObjectPoint3D operator +(ObjectPoint3D p, ObjectVector3D v)
        {
            return new ObjectPoint3D(v._x + p.X, v._y + p.Y, v._z + p.Z);
        }

        public static ObjectVector3D operator -(ObjectVector3D v)
        {
            return new ObjectVector3D(-v._x, -v._y, -v._z);
        }


        public static ObjectVector3D operator *(ObjectVector3D v, Single d)
        {
            return new ObjectVector3D(v._x * d, v._y * d, v._z * d);
        }
        public static ObjectVector3D operator *(Single d, ObjectVector3D v)
        {
            return new ObjectVector3D(v._x * d, v._y * d, v._z * d);
        }



        #endregion
    }
}
