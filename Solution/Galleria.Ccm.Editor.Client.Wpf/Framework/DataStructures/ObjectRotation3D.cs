﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion

using System;

//TODO: Classes under this namespace will eventually move into the framework.
namespace Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures
{
    /// <summary>
    /// Structure holding values defining a 3D rotation.
    /// </summary>
    public struct ObjectRotation3D
    {
        #region Fields

        private Single _angle;
        private Single _slope;
        private Single _roll;

        #endregion

        /// <summary>
        /// Rotation about the y axis in radians
        /// </summary>
        public Single Angle { get { return _angle; } set { _angle = value; } }

        /// <summary>
        /// Rotation about the x axis in radians
        /// </summary>
        public Single Slope { get { return _slope; } set { _slope = value; } }

        /// <summary>
        /// Rotation about the z axis in radians
        /// </summary>
        public Single Roll { get { return _roll; } set { _roll = value; } }


        public ObjectRotation3D(Single angle, Single slope, Single roll)
        {
            _angle = angle;
            _slope = slope;
            _roll = roll;
        }

        #region Overrides

        public override String ToString()
        {
            return String.Format("Angle = {0}, Slope = {1}, Roll = {2}", Angle, Slope, Roll);
        }

        public override Boolean Equals(object obj)
        {
            return obj is ObjectRotation3D && this == (ObjectRotation3D)obj;
        }

        public override Int32 GetHashCode()
        {
            return base.GetHashCode();
        }

        public static Boolean operator ==(ObjectRotation3D p1, ObjectRotation3D p2)
        {
            if (p1.Angle != p2.Angle) { return false; }
            if (p1.Slope != p2.Slope) { return false; }
            if (p1.Roll != p2.Roll) { return false; }
            return true;
        }

        public static Boolean operator !=(ObjectRotation3D p1, ObjectRotation3D p2)
        {
            return !(p1 == p2);
        }

        #endregion

    }
}
