﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Windows.Media.Media3D;

//TODO: Classes under this namespace will eventually move into the framework.

namespace Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures
{
    /// <summary>
    /// Structure holding values defining a point in 3D space.
    /// </summary>
    public struct ObjectPoint3D
    {
        #region Fields

        private Single _x;
        private Single _y;
        private Single _z;

        #endregion

        #region Properties

        public Single X 
        { 
            get { return _x; } 
            set { _x = value; } 
        }

        public Single Y 
        {
            get { return _y; } 
            set { _y = value; } 
        }

        public Single Z 
        { 
            get { return _z; } 
            set { _z = value; } 
        } 

        #endregion

        #region Constructor

        public ObjectPoint3D(Single x, Single y, Single z)
        {
            _x = x;
            _y = y;
            _z = z;
        }

        #endregion

        #region Methods

        public ObjectPoint3D Transform(IEnumerable<Matrix3D> matricies)
        {
            Matrix3D final = Matrix3D.Identity;

            foreach (Matrix3D matrix in matricies)
            {
                final *= matrix;
            }

            return Transform(final);
        }

        public ObjectPoint3D Transform(Matrix3D matrix)
        {
            return new ObjectPoint3D(
                (X * Convert.ToSingle(matrix.M11)) 
                + (Y * Convert.ToSingle(matrix.M21)) 
                + (Z * Convert.ToSingle(matrix.M31))
                + Convert.ToSingle(matrix.OffsetX),

                (X * Convert.ToSingle(matrix.M12)) 
                + (Y * Convert.ToSingle(matrix.M22)) 
                + (Z * Convert.ToSingle(matrix.M32))
                + Convert.ToSingle(matrix.OffsetY),

                (X * Convert.ToSingle(matrix.M13)) 
                + (Y * Convert.ToSingle(matrix.M23))
                + (Z * Convert.ToSingle(matrix.M33))
                + Convert.ToSingle(matrix.OffsetZ)
                );
        }

        /// <summary>
        /// Returns the absolute value of the point
        /// </summary>
        /// <returns></returns>
        public ObjectPoint3D Abs()
        {
            return new ObjectPoint3D(
                Math.Abs(this.X), Math.Abs(this.Y),  Math.Abs(this.Z));
        }

        public ObjectPoint3D Round()
        {
            return Round(0);
        }
        public ObjectPoint3D Round(Int32 dp)
        {
            return new ObjectPoint3D(
               Convert.ToSingle(Math.Round(this.X, dp)), 
               Convert.ToSingle(Math.Round(this.Y, dp)), 
               Convert.ToSingle(Math.Round(this.Z, dp)));
        }

        #endregion

        #region Overrides

        public override String ToString()
        {
            return String.Format("X = {0}, Y = {1}, Z = {2}", X, Y, Z);
        }

        public override Boolean Equals(object obj)
        {
            return obj is ObjectPoint3D && this == (ObjectPoint3D)obj;
        }

        public override Int32 GetHashCode()
        {
            return base.GetHashCode();
        }

        public static Boolean operator ==(ObjectPoint3D p1, ObjectPoint3D p2)
        {
            if (p1.X != p2.X) { return false; }
            if (p1.Y != p2.Y) { return false; }
            if (p1.Z != p2.Z) { return false; }
            return true;
        }

        public static Boolean operator !=(ObjectPoint3D p1, ObjectPoint3D p2)
        {
            return !(p1 == p2);
        }

        public static ObjectVector3D operator -(ObjectPoint3D p1, ObjectPoint3D p2)
        {
            return new ObjectVector3D(p1._x - p2._x, p1._y - p2._y, p1._z - p2._z);
        }

        public static ObjectPoint3D operator *(ObjectPoint3D v, Single d)
        {
            return new ObjectPoint3D(v._x * d, v._y * d, v._z * d);
        }
        public static ObjectPoint3D operator *(Single d, ObjectPoint3D v)
        {
            return new ObjectPoint3D(v._x * d, v._y * d, v._z * d);
        }

        public static ObjectPoint3D operator +(ObjectPoint3D v, ObjectPoint3D p)
        {
            return new ObjectPoint3D(v._x + p._x, v._y + p._y, v._z + p._z);
        }

        public static ObjectPoint3D operator -(ObjectPoint3D v)
        {
            return new ObjectPoint3D(-v._x, -v._y, -v._z);
        }

        #endregion
    }
}
