﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures
{
    /// <summary>
    /// Defines a rectangle in 3D space.
    /// </summary>
    public struct ObjectRect3D
    {
        #region Fields
        private Single _x;
        private Single _y;
        private Single _z;
        private Single _sizeX;
        private Single _sizeY;
        private Single _sizeZ;
        #endregion

        #region Properties

        public Boolean IsEmpty
        {
            get
            {
                return X == 0 && Y == 0 && Z == 0
                    && SizeX == 0 && SizeY == 0 && SizeZ == 0;
            }

        }

        public Single X
        {
            get { return _x; }
            set { _x = value; }
        }
        public Single Y
        {
            get { return _y; }
            set { _y = value; }
        }
        public Single Z
        {
            get { return _z; }
            set { _z = value; }
        }
        public Single SizeX
        {
            get { return _sizeX; }
            set { _sizeX = value; }
        }
        public Single SizeY
        {
            get { return _sizeY; }
            set { _sizeY = value; }
        }
        public Single SizeZ
        {
            get { return _sizeZ; }
            set { _sizeZ = value; }
        }

        #endregion

        #region Constructor

        public ObjectRect3D(Single x, Single y, Single z, Single width, Single height, Single depth)
        {
            _x = x;
            _y = y;
            _z = z;
            _sizeX = width;
            _sizeY = height;
            _sizeZ = depth;
        }

        #endregion
    }
}
