﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion

using System;
using System.Windows.Media.Media3D;
using System.Collections.Generic;

//TODO: Classes under this namespace will eventually move into the framework.
namespace Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures
{
    /// <summary>
    /// Structure holding values defining an object size in 3D.
    /// </summary>
    public struct ObjectSize3D
    {
        #region Fields

        private Single _x;
        private Single _y;
        private Single _z;

        #endregion

        #region Properties

        /// <summary>
        /// the x dimension size
        /// </summary>
        public Single X 
        {
            get { return _x; } 
            set { _x = value; } 
        }  

        /// <summary>
        /// the y dimension size
        /// </summary>
        public Single Y 
        { 
            get { return _y; } 
            set { _y = value; } 
        } 
 
        /// <summary>
        /// the z dimension size
        /// </summary>
        public Single Z 
        { 
            get { return _z; } 
            set { _z = value; } 
        } 

        #endregion

        #region Constructor

        public ObjectSize3D(Single x, Single y, Single z)
        {
            _x = x;
            _y = y;
            _z = z;
        }

        #endregion

        #region Methods

        public ObjectSize3D Transform(IEnumerable<Matrix3D> matricies)
        {
            Matrix3D final = Matrix3D.Identity;

            foreach (Matrix3D matrix in matricies)
            {
                final *= matrix;
            }

            return Transform(final);
        }

        public ObjectSize3D Transform(Matrix3D matrix)
        {
            return new ObjectSize3D(
                (X * Convert.ToSingle(matrix.M11)) 
                + (Y * Convert.ToSingle(matrix.M21)) 
                + (Z * Convert.ToSingle(matrix.M31)) 
                + Convert.ToSingle(matrix.OffsetX),

                (X * Convert.ToSingle(matrix.M12)) 
                + (Y * Convert.ToSingle(matrix.M22)) 
                + (Z * Convert.ToSingle(matrix.M32))
                + Convert.ToSingle(matrix.OffsetY),

                (X * Convert.ToSingle(matrix.M13)) 
                + (Y * Convert.ToSingle(matrix.M23)) 
                + (Z * Convert.ToSingle(matrix.M33))
                + Convert.ToSingle(matrix.OffsetZ)
                );
        }

        /// <summary>
        /// Returns the absolute value of the point
        /// </summary>
        /// <returns></returns>
        public ObjectSize3D Abs()
        {
            return new ObjectSize3D(
                Math.Abs(this.X), Math.Abs(this.Y), Math.Abs(this.Z));
        }

        #endregion

        #region Overrides

        public override String ToString()
        {
            return String.Format("X = {0}, Y = {1}, Z = {2}", X, Y, Z);
        }


        public override Boolean Equals(object obj)
        {
            return obj is ObjectSize3D && this == (ObjectSize3D)obj;
        }

        public override Int32 GetHashCode()
        {
            return base.GetHashCode();
        }

        public static Boolean operator ==(ObjectSize3D p1, ObjectSize3D p2)
        {
            if (p1.X != p2.X) { return false; }
            if (p1.Y != p2.Y) { return false; }
            if (p1.Z != p2.Z) { return false; }
            return true;
        }

        public static Boolean operator !=(ObjectSize3D p1, ObjectSize3D p2)
        {
            return !(p1 == p2);
        }

        #endregion

    }
}
