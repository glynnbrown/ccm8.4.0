﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData;
using HelixToolkit.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;
using System.Reflection;

namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderControls
{
    /// <summary>
    /// Builds 3D model meshes
    /// </summary>
    public class Model3DBuilder
    {
        #region Fields

        private Vector3DCollection _normals;
        private Point3DCollection _positions;
        private PointCollection _textureCoordinates;
        private Int32Collection _triangleIndices;

        #endregion

        #region Properties

        public Vector3DCollection Normals
        {
            get { return _normals; }
        }

        public Point3DCollection Positions
        {
            get { return _positions; }
        }

        public PointCollection TextureCoordinates
        {
            get { return _textureCoordinates; }
        }

        public Int32Collection TriangleIndices
        {
            get { return _triangleIndices; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public Model3DBuilder()
        {
            _positions = new Point3DCollection();
            _triangleIndices = new Int32Collection();
            _normals = new Vector3DCollection();
            _textureCoordinates = new PointCollection();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds a box with all faces
        /// </summary>
        /// <param name="center"></param>
        /// <param name="xlength"></param>
        /// <param name="ylength"></param>
        /// <param name="zlength"></param>
        public void AddBox(Point3D center, Double xlength, Double ylength, Double zlength)
        {
            AddBoxFaces(center, xlength, ylength, zlength, BoxFaceType.All);
        }

        /// <summary>
        /// Addsa box with the given faces.
        /// </summary>
        /// <param name="center"></param>
        /// <param name="xlength"></param>
        /// <param name="ylength"></param>
        /// <param name="zlength"></param>
        /// <param name="faces"></param>
        public void AddBoxFaces(Point3D center, Double xlength, Double ylength, Double zlength, BoxFaceType faces)
        {

            if ((faces & BoxFaceType.Front) == BoxFaceType.Front)
            {
                AddBoxFace(center, new Vector3D(0, 0, 1), new Vector3D(0, 1, 0), zlength, xlength, ylength);
            }

            if ((faces & BoxFaceType.Back) == BoxFaceType.Back)
            {
                AddBoxFace(center, new Vector3D(0, 0, -1), new Vector3D(0, 1, 0), zlength, xlength, ylength);
            }

            if ((faces & BoxFaceType.Left) == BoxFaceType.Left)
            {
                AddBoxFace(center, new Vector3D(-1, 0, 0), new Vector3D(0, 0, 1), xlength, ylength, zlength);
            }

            if ((faces & BoxFaceType.Right) == BoxFaceType.Right)
            {
                AddBoxFace(center, new Vector3D(1, 0, 0), new Vector3D(0, 0, 1), xlength, ylength, zlength);
            }

            if ((faces & BoxFaceType.Bottom) == BoxFaceType.Bottom)
            {
                AddBoxFace(center, new Vector3D(0, -1, 0), new Vector3D(0, 0, 1), ylength, xlength, zlength);
            }

            if ((faces & BoxFaceType.Top) == BoxFaceType.Top)
            {
                AddBoxFace(center, new Vector3D(0, 1, 0), new Vector3D(0, 0, 1), ylength, xlength, zlength);
            }

        }

        /// <summary>
        /// Adds a box face
        /// </summary>
        /// <param name="center"></param>
        /// <param name="normal"></param>
        /// <param name="up"></param>
        /// <param name="dist"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        private void AddBoxFace(Point3D center, Vector3D normal, Vector3D up, Double dist, Double width, Double height)
        {
            Vector3D right = Vector3D.CrossProduct(normal, up);
            Vector3D n = normal * dist / 2;
            up *= height / 2;
            right *= width / 2;

            Point3D p1 = center + n - up - right;
            Point3D p2 = center + n - up + right;
            Point3D p3 = center + n + up + right;
            Point3D p4 = center + n + up - right;

            Int32 i0 = this.Positions.Count;
            this.Positions.Add(p1);
            this.Positions.Add(p2);
            this.Positions.Add(p3);
            this.Positions.Add(p4);

            if (this.Normals != null)
            {
                this.Normals.Add(normal);
                this.Normals.Add(normal);
                this.Normals.Add(normal);
                this.Normals.Add(normal);
            }

            if (this.TextureCoordinates != null)
            {
                this.TextureCoordinates.Add(new Point(1, 1));
                this.TextureCoordinates.Add(new Point(0, 1));
                this.TextureCoordinates.Add(new Point(0, 0));
                this.TextureCoordinates.Add(new Point(1, 0));
            }

            this.TriangleIndices.Add(i0 + 2);
            this.TriangleIndices.Add(i0 + 1);
            this.TriangleIndices.Add(i0 + 0);
            this.TriangleIndices.Add(i0 + 0);
            this.TriangleIndices.Add(i0 + 3);
            this.TriangleIndices.Add(i0 + 2);
        }

        /// <summary>
        /// Adds an ellipsoid.
        /// </summary>
        /// <param name="center">
        /// The center of the ellipsoid.
        /// </param>
        /// <param name="radiusx">
        /// The x radius of the ellipsoid.
        /// </param>
        /// <param name="radiusy">
        /// The y radius of the ellipsoid.
        /// </param>
        /// <param name="radiusz">
        /// The z radius of the ellipsoid.
        /// </param>
        /// <param name="thetaDiv">
        /// The number of divisions around the ellipsoid.
        /// </param>
        /// <param name="phiDiv">
        /// The number of divisions from top to bottom of the ellipsoid.
        /// </param>
        public void AddEllipsoid(Point3D center, Double radiusx, Double radiusy, Double radiusz, Int32 thetaDiv = 20, Int32 phiDiv = 10)
        {
            MeshBuilder mb = new MeshBuilder();
            mb.AddEllipsoid(center, radiusx, radiusy, radiusz, thetaDiv, phiDiv);
            Append(mb);
        }

        /// <summary>
        /// Appends the specified mesh.
        /// </summary>
        /// <param name="mesh">
        /// The mesh.
        /// </param>
        private void Append(MeshBuilder mesh)
        {
            if (mesh == null)
            {
                throw new ArgumentNullException("mesh");
            }

            this.Append(mesh.Positions, mesh.TriangleIndices, mesh.Normals, mesh.TextureCoordinates);
        }

        /// <summary>
        /// Appends the specified points and triangles.
        /// </summary>
        /// <param name="positionsToAppend">
        /// The points to append.
        /// </param>
        /// <param name="triangleIndicesToAppend">
        /// The triangle indices to append.
        /// </param>
        /// <param name="normalsToAppend">
        /// The normal vectors to append.
        /// </param>
        /// <param name="textureCoordinatesToAppend">
        /// The texture coordinates to append.
        /// </param>
        public void Append(
            IList<Point3D> positionsToAppend,
            IList<Int32> triangleIndicesToAppend,
            IList<Vector3D> normalsToAppend = null,
            IList<Point> textureCoordinatesToAppend = null)
        {
            if (positionsToAppend == null)
            {
                throw new ArgumentNullException("positionsToAppend");
            }

            if (this.Normals != null && normalsToAppend == null)
            {
                throw new InvalidOperationException();
            }

            if (this.TextureCoordinates != null && textureCoordinatesToAppend == null)
            {
                throw new InvalidOperationException();
            }

            if (normalsToAppend != null && normalsToAppend.Count != positionsToAppend.Count)
            {
                throw new InvalidOperationException();
            }

            if (textureCoordinatesToAppend != null && textureCoordinatesToAppend.Count != positionsToAppend.Count)
            {
                throw new InvalidOperationException();
            }

            int index0 = this.Positions.Count;
            foreach (var p in positionsToAppend)
            {
                this.Positions.Add(p);
            }

            if (this.Normals != null && normalsToAppend != null)
            {
                foreach (var n in normalsToAppend)
                {
                    this.Normals.Add(n);
                }
            }

            if (this.TextureCoordinates != null && textureCoordinatesToAppend != null)
            {
                foreach (var t in textureCoordinatesToAppend)
                {
                    this.TextureCoordinates.Add(t);
                }
            }

            foreach (int i in triangleIndicesToAppend)
            {
                this.TriangleIndices.Add(index0 + i);
            }
        }

        /// <summary>
        /// Outs the current mesh
        /// </summary>
        /// <param name="freeze"></param>
        /// <returns></returns>
        public MeshGeometry3D ToMesh(Boolean freeze = false)
        {
            if (this.Normals != null && this.Positions.Count != this.Normals.Count)
            {
                throw new InvalidOperationException("Wrong number of normals");
            }

            if (this.TextureCoordinates != null && this.Positions.Count != this.TextureCoordinates.Count)
            {
                throw new InvalidOperationException("Wrong number of texture coords");
            }

            MeshGeometry3D mg = new MeshGeometry3D
            {
                Positions = this.Positions,
                TriangleIndices = this.TriangleIndices,
                Normals = this.Normals,
                TextureCoordinates = this.TextureCoordinates
            };

            if (freeze)
            {
                mg.Freeze();
            }

            return mg;
        }

        #endregion
    }


    
}
