﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media.Media3D;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;
using Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData;
using System.Windows.Media;
using System.Diagnostics;


namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderControls
{
    public sealed class ModelConstructMesh
    {
        #region Constants
        internal const Int16 FaceBitOffset = 9;
        internal const Int16 NestSizeAccuracy = 4;
        #endregion

        #region Fields

        private Boolean _generateNormals;
        private Boolean _generateTextures;

        private IList<Int32> _triangleIndices;
        private IList<ObjectPoint3D> _positions;
        private IList<ObjectVector3D> _normals;
        private IList<Point> _textureCoordinates;
        private ObjectPoint3D _min;
        private ObjectPoint3D _max;
        private ObjectSize3D _nestSize;
        #endregion

        #region Properties

        public IList<Int32> TriangleIndices
        {
            get { return _triangleIndices; }
            set { _triangleIndices = value; }
        }

        public IList<ObjectPoint3D> Positions
        {
            get { return _positions; }
            set
            {
                _positions = value;

                UpdateMinMax();
            }
        }

        public IList<ObjectVector3D> Normals
        {
            get { return _normals; }
            set
            {
                if (_generateNormals)
                {
                    _normals = value;
                }
            }
        }

        public IList<Point> TextureCoordinates
        {
            get { return _textureCoordinates; }
            set
            {
                if (_generateTextures)
                {
                    _textureCoordinates = value;
                }
            }
        }

        public ObjectPoint3D Min
        {
            get { return _min; }
            set { _min = value; }
        }

        public ObjectPoint3D Max
        {
            get { return _max; }
            set { _max = value; }
        }

        public ObjectPoint3D Size
        {
            get
            {
                return new ObjectPoint3D()
                {
                    X = Max.X - Min.X,
                    Y = Max.Y - Min.Y,
                    Z = Max.Z - Min.Z
                };
            }
        }

        /// <summary>
        /// If the mesh is tiled this is the nest amount in each direction.
        /// </summary>
        public ObjectSize3D NestSize
        {
            get { return _nestSize; }
            set { _nestSize = value; }
        }

        #endregion

        #region Constructor

        public ModelConstructMesh(Boolean generateNormals, Boolean generateTextureCoords)
        {
            _generateNormals = generateNormals;
            _generateTextures = generateTextureCoords;

            _positions = new List<ObjectPoint3D>();
            _triangleIndices = new List<Int32>();
            _min = new ObjectPoint3D();
            _max = new ObjectPoint3D();
            _nestSize = new ObjectSize3D();

            if (generateNormals)
            {
                _normals = new List<ObjectVector3D>();
            }

            if (generateTextureCoords)
            {
                _textureCoordinates = new List<Point>();
            }
        }

        #endregion

        #region Methods

        private void UpdateMinMax()
        {
            //update the min and max values
            ObjectPoint3D newMax;
            ObjectPoint3D newMin;
            GetMinMaxValues(this, out newMin, out newMax);
            this.Min = newMin;
            this.Max = newMax;
        }

        public void AddBox(Double boxWidth, Double boxHeight, Double boxDepth)
        {
            Single sizeX = Convert.ToSingle(boxWidth);
            Single sizeY = Convert.ToSingle(boxHeight);
            Single sizeZ = Convert.ToSingle(boxDepth);

            ObjectPoint3D p0 = new ObjectPoint3D(0, 0, 0); //bll
            ObjectPoint3D p1 = new ObjectPoint3D(sizeX, 0, 0); // blr
            ObjectPoint3D p2 = new ObjectPoint3D(sizeX, 0, sizeZ); //flr
            ObjectPoint3D p3 = new ObjectPoint3D(0, 0, sizeZ); //fll
            ObjectPoint3D p4 = new ObjectPoint3D(0, sizeY, 0); //bul
            ObjectPoint3D p5 = new ObjectPoint3D(sizeX, sizeY, 0);//bur
            ObjectPoint3D p6 = new ObjectPoint3D(sizeX, sizeY, sizeZ);//fur
            ObjectPoint3D p7 = new ObjectPoint3D(0, sizeY, sizeZ);//ful

            List<ObjectPoint3D> positions = new List<ObjectPoint3D>() 
            { 
                        p7, p3, p2, p6, //front (0,1,2,3)
                        p5, p1, p0, p4, //back (4,5,6,7)
                        p4, p0, p3, p7, //left (8,9,10,11)
                        p6, p2, p1, p5, //right (12,13,14,15)
                        p3, p0, p1, p2, //bottom (16,17,18,19)
                        p4, p7, p6, p5 //Top (20,21,22,23)
                };

            List<ObjectVector3D> normals = new List<ObjectVector3D>()
            {
                        {new ObjectVector3D(0,0,1)}, {new ObjectVector3D(0,0,1)}, {new ObjectVector3D(0,0,1)}, {new ObjectVector3D(0,0,1)},
                        {new ObjectVector3D(0,0,-1)}, {new ObjectVector3D(0,0,-1)}, {new ObjectVector3D(0,0,-1)}, {new ObjectVector3D(0,0,-1)}, 
                        {new ObjectVector3D(-1,0,0)}, {new ObjectVector3D(-1,0,0)}, {new ObjectVector3D(-1,0,0)}, {new ObjectVector3D(-1,0,0)}, 
                        {new ObjectVector3D(1,0,0)}, {new ObjectVector3D(1,0,0)}, {new ObjectVector3D(1,0,0)}, {new ObjectVector3D(1,0,0)}, 
                        {new ObjectVector3D(0,-1,0)}, {new ObjectVector3D(0,-1,0)}, {new ObjectVector3D(0,-1,0)}, {new ObjectVector3D(0,-1,0)},
                        {new ObjectVector3D(0,1,0)}, {new ObjectVector3D(0,1,0)}, {new ObjectVector3D(0,1,0)}, {new ObjectVector3D(0,1,0)}
            };

            List<Int32> triangleIndices = new List<Int32>(){ 
                        0, 1, 2, 0, 2, 3,
                        4, 5, 6, 4, 6, 7,
                        8, 9, 10, 8, 10, 11,
                        12, 13, 14, 12, 14, 15,
                        16, 17, 18, 16, 18, 19,
                        20, 21, 22, 20, 22, 23};

            Double heightMultiplier = 1;


            List<Point> textures = new List<Point>()
            {
                        #region Front
                        {new Point(){X = 0, Y = 0}}, 
                        {new Point(){X = 0, Y = boxHeight * heightMultiplier}},
                        {new Point(){X = boxWidth, Y = boxHeight* heightMultiplier}}, 
                        {new Point(){X = boxWidth, Y = 0}},
                        #endregion           
                        #region Back
                        {new Point(){X = boxWidth + boxDepth, Y = 0}}, 
                        {new Point(){X = boxWidth + boxDepth, Y =  boxHeight * heightMultiplier}},
                        {new Point(){X = (boxWidth * 2) + boxDepth, Y = boxHeight * heightMultiplier}},
                        {new Point(){X = (boxWidth * 2) + boxDepth, Y = 0}},
                        #endregion  
                        #region Left
                        {new Point(){X = (boxWidth * 2) + boxDepth, Y = 0}}, 
                        {new Point(){X = (boxWidth * 2) + boxDepth, Y = boxHeight * heightMultiplier}},
                        {new Point(){X = (boxWidth + boxDepth) * 2, Y = boxHeight * heightMultiplier}},
                        {new Point(){X = (boxWidth + boxDepth) * 2, Y = 0}},
                        #endregion 
                        #region Right
                        {new Point(){X = boxWidth, Y = 0}}, 
                        {new Point(){X = boxWidth, Y = boxHeight * heightMultiplier}},
                        {new Point(){X = boxWidth + boxDepth, Y = boxHeight * heightMultiplier}}, 
                        {new Point(){X = boxWidth + boxDepth, Y = 0}},
                        #endregion 
                        #region Bottom
                        {new Point(){X = boxWidth, Y = boxHeight}}, 
                        {new Point(){X = boxWidth, Y = boxHeight + boxDepth}},
                        {new Point(){X = boxWidth * 2, Y = boxHeight + boxDepth}},
                        {new Point(){X = boxWidth * 2, Y = boxHeight}},
                        #endregion
                        #region Top
                        {new Point(){X = 0, Y = boxHeight}}, 
                        {new Point(){X = 0, Y = boxHeight + boxDepth}},
                        {new Point(){X = boxWidth, Y = boxHeight + boxDepth}}, 
                        {new Point(){X = boxWidth, Y = boxHeight}}
                        #endregion
            };




            this.Positions = positions;
            this.Normals = normals;
            this.TriangleIndices = triangleIndices;
            this.TextureCoordinates = textures;


        }

        public void AddBoxFaces(ObjectPoint3D centerPoint, Double xlength, Double ylength, Double zlength, BoxFaceType faces)
        {
            Point3D center = centerPoint.ToPoint3D();

            if ((faces & BoxFaceType.Front) == BoxFaceType.Front)
            {
                AddBoxFace(center, new Vector3D(0, 0, 1), new Vector3D(0, 1, 0), zlength, xlength, ylength);
            }

            if ((faces & BoxFaceType.Back) == BoxFaceType.Back)
            {
                AddBoxFace(center, new Vector3D(0, 0, -1), new Vector3D(0, 1, 0), zlength, xlength, ylength);
            }

            if ((faces & BoxFaceType.Left) == BoxFaceType.Left)
            {
                AddBoxFace(center, new Vector3D(-1, 0, 0), new Vector3D(0, 0, 1), xlength, ylength, zlength);
            }

            if ((faces & BoxFaceType.Right) == BoxFaceType.Right)
            {
                AddBoxFace(center, new Vector3D(1, 0, 0), new Vector3D(0, 0, 1), xlength, ylength, zlength);
            }

            if ((faces & BoxFaceType.Bottom) == BoxFaceType.Bottom)
            {
                AddBoxFace(center, new Vector3D(0, -1, 0), new Vector3D(0, 0, 1), ylength, xlength, zlength);
            }

            if ((faces & BoxFaceType.Top) == BoxFaceType.Top)
            {
                AddBoxFace(center, new Vector3D(0, 1, 0), new Vector3D(0, 0, 1), ylength, xlength, zlength);
            }
        }
        
        public void AddNestableBox(ObjectSize3D boxSize, ObjectSize3D nestSize, Boolean skew = true)
        {
            if (nestSize.X > 0 || nestSize.Y > 0 || nestSize.Z > 0)
            {

                this.NestSize = nestSize;

                //Inner Points lower back
                ObjectPoint3D ip0 = new ObjectPoint3D(0, 0, 0); //inner bll
                ObjectPoint3D ip1 = new ObjectPoint3D(boxSize.X, 0, 0); //inner blr

                //Inner Points lower front
                ObjectPoint3D ip2 = new ObjectPoint3D(boxSize.X, 0, boxSize.Z); //inner flr
                ObjectPoint3D ip3 = new ObjectPoint3D(0, 0, boxSize.Z); //inner fll

                //Inner Points upper
                ObjectPoint3D ip4 = new ObjectPoint3D(0, boxSize.Y, 0); //inner bul
                ObjectPoint3D ip5 = new ObjectPoint3D(boxSize.X, boxSize.Y, 0); //inner bur
                ObjectPoint3D ip6 = new ObjectPoint3D(boxSize.X, boxSize.Y, boxSize.Z); //inner fur
                ObjectPoint3D ip7 = new ObjectPoint3D(0, boxSize.Y, boxSize.Z); //inner ful
                Single thick = 0;

                if (skew)
                {
                    ModifyBoxNestPoints(ref ip0, ref ip1, ref ip2, ref ip3, ref ip4, ref ip5, ref ip6, ref ip7, boxSize, nestSize, true, out thick);
                }

                ObjectPoint3D traySize = new ObjectPoint3D()
                {
                    X = Math.Max(boxSize.X, 0),
                    Y = Math.Max(boxSize.Y, 0),
                    Z = Math.Max(boxSize.Z, 0)
                };

                //Outer Points lower
                ObjectPoint3D op0 = new ObjectPoint3D(0, 0, 0); //Outer bll
                ObjectPoint3D op1 = new ObjectPoint3D(boxSize.X, 0, 0); //Outer blr
                ObjectPoint3D op2 = new ObjectPoint3D(boxSize.X, 0, boxSize.Z); //Outer flr
                ObjectPoint3D op3 = new ObjectPoint3D(0, 0, boxSize.Z); //Outer fll

                //Outer Points upper
                ObjectPoint3D op4 = new ObjectPoint3D(0, boxSize.Y, 0); //Outer bul
                ObjectPoint3D op5 = new ObjectPoint3D(boxSize.X, boxSize.Y, 0); //Outer bur
                ObjectPoint3D op6 = new ObjectPoint3D(boxSize.X, boxSize.Y, boxSize.Z); //Outer fur
                ObjectPoint3D op7 = new ObjectPoint3D(0, boxSize.Y, boxSize.Z); //Outer ful

                if (skew)
                {
                    ModifyBoxNestPoints(ref op0, ref op1, ref op2, ref op3, ref op4, ref op5, ref op6, ref op7, boxSize, nestSize, false, out thick);
                }

                List<ObjectPoint3D> positions = new List<ObjectPoint3D>() { 
                        op7, op3, op2, op6, //front (0,1,2,3)
                        op5, op1, op0, op4, //back (4,5,6,7)
                        op4, op0, op3, op7, //left (8,9,10,11)
                        op6, op2, op1, op5, //right (12,13,14,15)
                        op3, op0, op1, op2, //bottom (16,17,18,19)
                        op4, op7, op6, op5, //Top (20,21,22,23)

                        ip4, ip7, ip6, ip5, //Top (24,25,26,27)
                        ip7, ip3, ip2, ip6, //front (28,29,30,31)
                        ip5, ip1, ip0, ip4, //back (32,33,34,35)
                        ip4, ip0, ip3, ip7, //left (36,37,38,39)
                        ip6, ip2, ip1, ip5, //right (40,41,42,43)
                        ip3, ip0, ip1, ip2, //bottom (44,45,46,47)
                };

                List<ObjectVector3D> normals = null;
                if (this.Normals != null)
                {
                    normals = new List<ObjectVector3D>(){
                        //Outer
                        {new ObjectVector3D(0,0,1)}, {new ObjectVector3D(0,0,1)}, {new ObjectVector3D(0,0,1)}, {new ObjectVector3D(0,0,1)},
                        {new ObjectVector3D(0,0,-1)}, {new ObjectVector3D(0,0,-1)}, {new ObjectVector3D(0,0,-1)}, {new ObjectVector3D(0,0,-1)}, 
                        {new ObjectVector3D(-1,0,0)}, {new ObjectVector3D(-1,0,0)}, {new ObjectVector3D(-1,0,0)}, {new ObjectVector3D(-1,0,0)}, 
                        {new ObjectVector3D(1,0,0)}, {new ObjectVector3D(1,0,0)}, {new ObjectVector3D(1,0,0)}, {new ObjectVector3D(1,0,0)}, 
                        {new ObjectVector3D(0,-1,0)}, {new ObjectVector3D(0,-1,0)}, {new ObjectVector3D(0,-1,0)}, {new ObjectVector3D(0,-1,0)},
                        {new ObjectVector3D(0,1,0)}, {new ObjectVector3D(0,1,0)}, {new ObjectVector3D(0,1,0)}, {new ObjectVector3D(0,1,0)},
                        
                        //Inner
                        {new ObjectVector3D(0,-1,0)}, {new ObjectVector3D(0,-1,0)}, {new ObjectVector3D(0,-1,0)}, {new ObjectVector3D(0,-1,0)},
                        {new ObjectVector3D(0,0,-1)}, {new ObjectVector3D(0,0,-1)}, {new ObjectVector3D(0,0,-1)}, {new ObjectVector3D(0,0,-1)},
                        {new ObjectVector3D(0,0,1)}, {new ObjectVector3D(0,0,1)}, {new ObjectVector3D(0,0,1)}, {new ObjectVector3D(0,0,1)}, 
                        {new ObjectVector3D(1,0,0)}, {new ObjectVector3D(1,0,0)}, {new ObjectVector3D(1,0,0)}, {new ObjectVector3D(1,0,0)}, 
                        {new ObjectVector3D(-1,0,0)}, {new ObjectVector3D(-1,0,0)}, {new ObjectVector3D(-1,0,0)}, {new ObjectVector3D(-1,0,0)}, 
                        {new ObjectVector3D(0,1,0)}, {new ObjectVector3D(0,1,0)}, {new ObjectVector3D(0,1,0)}, {new ObjectVector3D(0,1,0)},
                    };
                }

                #region Create Indices and modify normals
                List<Int32> triangleIndices = new List<Int32>();
                List<Point> textureCoords = new List<Point>();

                //Coordinates to make a whole face the general solid colour
                Point solidColourNoBorder1 = new Point() { X = boxSize.X * 2 + boxSize.Z * 1.5, Y = boxSize.Y + boxSize.Z / 2 };
                Point solidColourNoBorder2 = new Point() { X = boxSize.X * 2 + boxSize.Z * 1.5, Y = boxSize.Y + boxSize.Z / 2 };
                Point solidColourNoBorder3 = new Point() { X = boxSize.X * 2 + boxSize.Z * 1.5, Y = boxSize.Y + boxSize.Z / 2 };
                Point solidColourNoBorder4 = new Point() { X = boxSize.X * 2 + boxSize.Z * 1.5, Y = boxSize.Y + boxSize.Z / 2 };

                //Coordinates to make a whole face the border colour
                Point border1 = new Point() { X = 0, Y = 0 };
                Point border2 = new Point() { X = 0, Y = 0 };
                Point border3 = new Point() { X = 0, Y = 0 };
                Point border4 = new Point() { X = 0, Y = 0 };

                textureCoords = new List<Point>(){
                        #region Front
                        {new Point(){X = 0, Y = 0}}, 
                        {new Point(){X = 0, Y = boxSize.Y}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X, Y = 0}},
                        #endregion           
                        #region Back
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}}, 
                        {new Point(){X = boxSize.X + boxSize.Z, Y =  boxSize.Y}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = 0}},
                        #endregion  
                        #region Left
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = 0}}, 
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X + boxSize.Z) * 2, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X + boxSize.Z) * 2, Y = 0}},
                        #endregion 
                        #region Right
                        {new Point(){X = boxSize.X, Y = 0}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y}},
                        {new Point(){X = boxSize.X + boxSize.Z, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}},
                        #endregion 
                        #region Bottom
                        {new Point(){X = boxSize.X, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X * 2, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X * 2, Y = boxSize.Y}},
                        #endregion
                        #region Top
                        {new Point(){X = 0, Y = boxSize.Y}}, 
                        {new Point(){X = 0, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y + boxSize.Z}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y}},
                        #endregion

                        #region Top
                        {new Point(){X = 0, Y = boxSize.Y}}, 
                        {new Point(){X = 0, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y + boxSize.Z}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y}},
                        #endregion
                        #region Front
                        {new Point(){X = 0, Y = 0}}, 
                        {new Point(){X = 0, Y = boxSize.Y}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X, Y = 0}},
                        #endregion           
                        #region Back
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}}, 
                        {new Point(){X = boxSize.X + boxSize.Z, Y =  boxSize.Y}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = 0}},
                        #endregion  
                        #region Left
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = 0}}, 
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X + boxSize.Z) * 2, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X + boxSize.Z) * 2, Y = 0}},
                        #endregion 
                        #region Righ
                        {new Point(){X = boxSize.X, Y = 0}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y}},
                        {new Point(){X = boxSize.X + boxSize.Z, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}},
                        #endregion 
                        #region Bottom
                        {new Point(){X = boxSize.X, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X * 2, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X * 2, Y = boxSize.Y}}
                        #endregion


                        };

                if (nestSize.Y > 0 && nestSize.X == 0 && nestSize.Z == 0)
                {
                    #region (nest High)
                    triangleIndices = new List<Int32>(){ 
                        //OUTER
                        0, 1, 2, 0, 2, 3, //front
                        4, 5, 6, 4, 6, 7, //back
                        8, 9, 10, 8, 10, 11, //left
                        12, 13, 14, 12, 14, 15,//right
                        16, 17, 18, 16, 18, 19,//bottom

                        20, 21, 25, 20, 25, 24, //top
                        27, 26, 22, 27, 22, 23, //top
                        20, 24, 27, 20, 27, 23, //top
                        25, 21, 22, 25, 22, 26, //top

                        //INNER
                        44, 46, 45, 44, 47, 46,//bottom
                        28, 30, 29, 28, 31, 30, //front
                        32, 34, 33, 32, 35, 34,//back
                        36, 38, 37, 36, 39, 38,//left
                        40, 42, 41, 40, 43, 42,//right
                        };
                    #endregion

                    textureCoords = new List<Point>(){
                        #region Front
                        {new Point(){X = 0, Y = 0}}, 
                        {new Point(){X = 0, Y = boxSize.Y}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X, Y = 0}},
                        #endregion           
                        #region Back
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}}, 
                        {new Point(){X = boxSize.X + boxSize.Z, Y =  boxSize.Y}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = 0}},
                        #endregion  
                        #region Left
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = 0}}, 
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X + boxSize.Z) * 2, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X + boxSize.Z) * 2, Y = 0}},
                        #endregion 
                        #region Right
                        {new Point(){X = boxSize.X, Y = 0}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y}},
                        {new Point(){X = boxSize.X + boxSize.Z, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}},
                        #endregion 
                        #region Bottom
                        {new Point(){X = boxSize.X, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X * 2, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X * 2, Y = boxSize.Y}},
                        #endregion
                        #region Top
                        {border1}, 
                        {border2},
                        {border3},
                        {border4},
                        #endregion

                        #region Top
                        {border1}, 
                        {border2},
                        {border3},
                        {border4},
                        #endregion
                        #region Front
                        {solidColourNoBorder1}, 
                        {solidColourNoBorder2},
                        {solidColourNoBorder3}, 
                        {solidColourNoBorder4},
                        #endregion           
                        #region Back
                        {solidColourNoBorder1}, 
                        {solidColourNoBorder2},
                        {solidColourNoBorder3}, 
                        {solidColourNoBorder4},
                        #endregion  
                        #region Left
                        {solidColourNoBorder1}, 
                        {solidColourNoBorder2},
                        {solidColourNoBorder3}, 
                        {solidColourNoBorder4},
                        #endregion 
                        #region Righ
                        {solidColourNoBorder1}, 
                        {solidColourNoBorder2},
                        {solidColourNoBorder3}, 
                        {solidColourNoBorder4},
                        #endregion 
                        #region Bottom
                        {solidColourNoBorder1}, 
                        {solidColourNoBorder2},
                        {solidColourNoBorder3}, 
                        {solidColourNoBorder4},
                        #endregion
                        };

                    if (normals != null)
                    {
                        normals[24] = new ObjectVector3D(0, 1, 0);
                        normals[25] = new ObjectVector3D(0, 1, 0);
                        normals[26] = new ObjectVector3D(0, 1, 0);
                        normals[27] = new ObjectVector3D(0, 1, 0);
                    }
                }
                else if (_nestSize.Y == 0 && _nestSize.X == 0 && _nestSize.Z > 0)
                {
                    #region (nest back)
                    triangleIndices = new List<Int32>(){ 
                        //OUTER
                        0, 1, 2, 0, 2, 3, //front                        
                        8, 9, 10, 8, 10, 11, //left
                        12, 13, 14, 12, 14, 15,//right
                        16, 17, 18, 16, 18, 19,//bottom
                        20, 21, 22, 20, 22, 23, //top
                        
                        //INNER
                        24, 26, 25, 24, 27, 26, //top
                        44, 46, 45, 44, 47, 46,//bottom
                        28, 30, 29, 28, 31, 30, //front                        
                        36, 38, 37, 36, 39, 38,//left
                        40, 42, 41, 40, 43, 42,//right

                        //container lip
                        4, 5, 33, 4, 33, 32,//back
                        35, 34, 6, 35, 6, 7,//back                        
                        4, 32, 35, 4, 35, 7,//back
                        33, 5, 6, 33, 6, 34,//back
                        };
                    #endregion

                    #region Texture Coords
                    textureCoords = new List<Point>(){
                        #region Front
                        {new Point(){X = 0, Y = 0}}, 
                        {new Point(){X = 0, Y = boxSize.Y}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X, Y = 0}},
                        #endregion           
                        #region Back
                        {border1}, 
                        {border2},
                        {border3},
                        {border4},
                        #endregion  
                        #region Left
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = 0}}, 
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X + boxSize.Z) * 2, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X + boxSize.Z) * 2, Y = 0}},
                        #endregion 
                        #region Right
                        {new Point(){X = boxSize.X, Y = 0}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y}},
                        {new Point(){X = boxSize.X + boxSize.Z, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}},
                        #endregion 
                        #region Bottom
                        {new Point(){X = boxSize.X, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X * 2, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X * 2, Y = boxSize.Y}},
                        #endregion
                        #region Top
                        {new Point(){X = 0, Y = boxSize.Y}}, 
                        {new Point(){X = 0, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y + boxSize.Z}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y}},
                        #endregion

                        #region Top
                        {solidColourNoBorder1}, 
                        {solidColourNoBorder2},
                        {solidColourNoBorder3}, 
                        {solidColourNoBorder4},
                        #endregion
                        #region Front
                        {solidColourNoBorder1}, 
                        {solidColourNoBorder2},
                        {solidColourNoBorder3}, 
                        {solidColourNoBorder4},
                        #endregion           
                        #region Back
                        {border1}, 
                        {border2},
                        {border3},
                        {border4},
                        #endregion  
                        #region Left
                        {solidColourNoBorder1}, 
                        {solidColourNoBorder2},
                        {solidColourNoBorder3}, 
                        {solidColourNoBorder4},
                        #endregion 
                        #region Righ
                        {solidColourNoBorder1}, 
                        {solidColourNoBorder2},
                        {solidColourNoBorder3}, 
                        {solidColourNoBorder4},
                        #endregion 
                        #region Bottom
                        {solidColourNoBorder1}, 
                        {solidColourNoBorder2},
                        {solidColourNoBorder3}, 
                        {solidColourNoBorder4},
                        #endregion
                        };
                    #endregion

                    if (normals != null)
                    {
                        normals[32] = new ObjectVector3D(0, 0, -1);
                        normals[33] = new ObjectVector3D(0, 0, -1);
                        normals[34] = new ObjectVector3D(0, 0, -1);
                        normals[35] = new ObjectVector3D(0, 0, -1);
                    }
                }
                else if (_nestSize.Y == 0 && _nestSize.X > 0 && _nestSize.Z == 0)
                {
                    #region (nest Right)
                    triangleIndices = new List<Int32>(){ 
                        //OUTER
                        0, 1, 2, 0, 2, 3, //front
                        4, 5, 6, 4, 6, 7, //back
                        8, 9, 10, 8, 10, 11, //left                        
                        16, 17, 18, 16, 18, 19,//bottom
                        20, 21, 22, 20, 22, 23, //top
                        
                        //INNER
                        24, 26, 25, 24, 27, 26, //top
                        44, 46, 45, 44, 47, 46,//bottom
                        28, 30, 29, 28, 31, 30, //front
                        32, 34, 33, 32, 35, 34,//back
                        36, 38, 37, 36, 39, 38,//left

                        //container lip
                        12, 13, 41, 12, 41, 40,//right
                        43, 42, 14, 43, 14, 15,//right                        
                        12, 40, 43, 12, 43, 15,//right
                        41, 13, 14, 41, 14, 42,//right
                        };
                    #endregion

                    #region Texture Coords
                    textureCoords = new List<Point>(){
                        #region Front
                        {new Point(){X = 0, Y = 0}}, 
                        {new Point(){X = 0, Y = boxSize.Y}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X, Y = 0}},
                        #endregion           
                        #region Back
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}}, 
                        {new Point(){X = boxSize.X + boxSize.Z, Y =  boxSize.Y}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = 0}},
                        #endregion  
                        #region Left
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = 0}}, 
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X + boxSize.Z) * 2, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X + boxSize.Z) * 2, Y = 0}},
                        #endregion 
                        #region Right
                        {border1}, 
                        {border2},
                        {border3},
                        {border4},
                        #endregion 
                        #region Bottom
                        {new Point(){X = boxSize.X, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X * 2, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X * 2, Y = boxSize.Y}},
                        #endregion
                        #region Top
                        {new Point(){X = 0, Y = boxSize.Y}}, 
                        {new Point(){X = 0, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y + boxSize.Z}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y}},
                        #endregion

                        #region Top
                        {solidColourNoBorder1}, 
                        {solidColourNoBorder2},
                        {solidColourNoBorder3}, 
                        {solidColourNoBorder4},
                        #endregion
                        #region Front
                        {solidColourNoBorder1}, 
                        {solidColourNoBorder2},
                        {solidColourNoBorder3}, 
                        {solidColourNoBorder4},
                        #endregion           
                        #region Back
                        {solidColourNoBorder1}, 
                        {solidColourNoBorder2},
                        {solidColourNoBorder3}, 
                        {solidColourNoBorder4},
                        #endregion  
                        #region Left
                        {solidColourNoBorder1}, 
                        {solidColourNoBorder2},
                        {solidColourNoBorder3}, 
                        {solidColourNoBorder4},
                        #endregion 
                        #region Right
                        {border1}, 
                        {border2},
                        {border3},
                        {border4},
                        #endregion 
                        #region Bottom
                        {solidColourNoBorder1}, 
                        {solidColourNoBorder2},
                        {solidColourNoBorder3}, 
                        {solidColourNoBorder4},
                        #endregion
                        };
                    #endregion

                    if (normals != null)
                    {
                        normals[40] = new ObjectVector3D(1, 0, 0);
                        normals[41] = new ObjectVector3D(1, 0, 0);
                        normals[42] = new ObjectVector3D(1, 0, 0);
                        normals[43] = new ObjectVector3D(1, 0, 0);
                    }
                }
                else if (_nestSize.Y > 0 && _nestSize.X == 0 && _nestSize.Z > 0)
                {
                    #region (nest High and deep)
                    triangleIndices = new List<Int32>(){ 
                        //OUTER
                        0, 1, 2, 0, 2, 3, //front
                        //4, 5, 6, 4, 6, 7, //back
                        //8, 9, 10, 8, 10, 11, //left
                        //12, 13, 14, 12, 14, 15,//right
                        16, 17, 18, 16, 18, 19,//bottom

                        //20, 21, 25, 20, 25, 24, //top
                        //27, 26, 22, 27, 22, 23, //top
                        //20, 24, 27, 20, 27, 23, //top
                        25, 21, 22, 25, 22, 26, //top

                        12, 13, 41, 12, 41, 40, //right
                        41, 13, 14, 41, 14, 42,//right

                        //(8,9,10,11)  (12,13,14,15)
                        //(36,37,38,39) (40,41,42,43)
                        10, 11, 39, 10, 39, 38, //left
                        37, 9, 10, 37, 10, 38,//left
                        
                        33, 5, 6, 33, 6, 34,//back

                        //INNER
                        44, 46, 45, 44, 47, 46,//bottom
                        28, 30, 29, 28, 31, 30, //front
                        //32, 34, 33, 32, 35, 34,//back
                        //36, 38, 37, 36, 39, 38,//left
                        //40, 42, 41, 40, 43, 42,//right
                        };
                    #endregion


                    textureCoords = new List<Point>(){
                        #region Front
                        {new Point(){X = 0, Y = 0}}, 
                        {new Point(){X = 0, Y = boxSize.Y}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X, Y = 0}},
                        #endregion           
                        #region Back
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}}, 
                        {new Point(){X = boxSize.X + boxSize.Z, Y =  boxSize.Y}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = 0}},
                        #endregion  
                        #region Left
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = 0}}, 
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X + boxSize.Z) * 2, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X + boxSize.Z) * 2, Y = 0}},
                        #endregion 
                        #region Right
                        {new Point(){X = boxSize.X, Y = 0}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y}},
                        {new Point(){X = boxSize.X + boxSize.Z, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}},
                        #endregion 
                        #region Bottom
                        {new Point(){X = boxSize.X, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X * 2, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X * 2, Y = boxSize.Y}},
                        #endregion
                        #region Top
                        {new Point(){X = 0, Y = boxSize.Y}}, 
                        {new Point(){X = 0, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y + boxSize.Z}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y}},
                        #endregion

                        #region Top                                                                                            
                        {new Point(){X = 0, Y = boxSize.Y}}, 
                        {new Point(){X = 0, Y = boxSize.Y + boxSize.Z - Math.Max(thick, _nestSize.Z)}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y + boxSize.Z - Math.Max(thick, _nestSize.Z)}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y}},
                        #endregion
                        #region Front
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = 0}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = boxSize.Y - Math.Max(thick, _nestSize.Y)}},
                        {new Point(){X = boxSize.X + boxSize.Z, Y =  boxSize.Y - Math.Max(thick, _nestSize.Y)}},
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}}, 
                        #endregion           
                        #region Back    
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}}, 
                        {new Point(){X = boxSize.X + boxSize.Z, Y =  boxSize.Y - Math.Max(thick, _nestSize.Y)}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = boxSize.Y - Math.Max(thick, _nestSize.Y)}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = 0}},
                        #endregion  
                        #region Left
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = 0}}, 
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = boxSize.Y - Math.Max(thick, _nestSize.Y)}},
                        {new Point(){X = (boxSize.X + boxSize.Z) * 2 - Math.Max(thick, _nestSize.Z), Y = boxSize.Y - Math.Max(thick, _nestSize.Y)}},
                        {new Point(){X = (boxSize.X + boxSize.Z) * 2 - Math.Max(thick, _nestSize.Z), Y = 0}},
                        #endregion 
                        #region Right
                        {new Point(){X = boxSize.X + Math.Max(thick, _nestSize.Z), Y = 0}}, 
                        {new Point(){X = boxSize.X + Math.Max(thick, _nestSize.Z), Y = boxSize.Y - Math.Max(thick, _nestSize.Y)}},
                        {new Point(){X = boxSize.X + boxSize.Z, Y = boxSize.Y- Math.Max(thick, _nestSize.Y)}}, 
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}},
                        #endregion 
                        #region Bottom     
                        {new Point(){X = 0, Y = boxSize.Y + boxSize.Z - Math.Max(thick, _nestSize.Z)}},
                        {new Point(){X = 0, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y + boxSize.Z - Math.Max(thick, _nestSize.Z)}}, 
                        
                        #endregion
                        };

                    if (normals != null)
                    {
                        normals[24] = new ObjectVector3D(0, 1, 0);
                        normals[25] = new ObjectVector3D(0, 1, 0);
                        normals[26] = new ObjectVector3D(0, 1, 0);
                        normals[27] = new ObjectVector3D(0, 1, 0);

                        normals[32] = new ObjectVector3D(0, 0, -1);
                        normals[33] = new ObjectVector3D(0, 0, -1);
                        normals[34] = new ObjectVector3D(0, 0, -1);
                        normals[35] = new ObjectVector3D(0, 0, -1);

                        normals[36] = new ObjectVector3D(-1, 0, 0);
                        normals[37] = new ObjectVector3D(-1, 0, 0);
                        normals[38] = new ObjectVector3D(-1, 0, 0);
                        normals[39] = new ObjectVector3D(-1, 0, 0);
                        normals[40] = new ObjectVector3D(1, 0, 0);
                        normals[41] = new ObjectVector3D(1, 0, 0);
                        normals[42] = new ObjectVector3D(1, 0, 0);
                        normals[43] = new ObjectVector3D(1, 0, 0);
                    }

                }
                else if (_nestSize.Y > 0 && _nestSize.X > 0 && _nestSize.Z == 0)
                {
                    #region (nest High and wide)
                    triangleIndices = new List<Int32>(){ 
                        //OUTER
                        //0, 1, 2, 0, 2, 3, //front
                        //4, 5, 6, 4, 6, 7, //back
                        8, 9, 10, 8, 10, 11, //left
                        //12, 13, 14, 12, 14, 15,//right
                        16, 17, 18, 16, 18, 19,//bottom

                         //Edge
                        20, 21, 25, 20, 25, 24, //top
                        33, 5, 6, 33, 6, 34,//back
                        6, 7, 35, 6, 35, 34,//back left
                        0, 1, 28, 1, 29, 28, //front left
                        1, 30, 29, 1, 2, 30, //front
                        13, 14, 41, 14, 42, 41,//right


                        //INNER
                        44, 46, 45, 44, 47, 46,//bottom
                        //28, 30, 29, 28, 31, 30, //front
                        //32, 34, 33, 32, 35, 34,//back
                        36, 38, 37, 36, 39, 38,//left
                        //40, 42, 41, 40, 43, 42,//right
                        };
                    #endregion

                    #region Texture Coords
                    textureCoords = new List<Point>(){
                        #region Front
                        {new Point(){X = 0, Y = 0}}, 
                        {new Point(){X = 0, Y = boxSize.Y}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X, Y = 0}},
                        #endregion           
                        #region Back
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}}, 
                        {new Point(){X = boxSize.X + boxSize.Z, Y =  boxSize.Y}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = 0}},
                        #endregion  
                        #region Left
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = 0}}, 
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X + boxSize.Z) * 2, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X + boxSize.Z) * 2, Y = 0}},
                        #endregion 
                        #region Right
                        {new Point(){X = boxSize.X, Y = 0}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y}},
                        {new Point(){X = boxSize.X + boxSize.Z, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}},
                        #endregion 
                        #region Bottom
                        {new Point(){X = boxSize.X, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X * 2, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X * 2, Y = boxSize.Y}},
                        #endregion
                        #region Top
                        {new Point(){X = 0, Y = boxSize.Y}}, 
                        {new Point(){X = 0, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y + boxSize.Z}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y}},
                        #endregion

                        #region Top
                        {new Point(){X = Math.Max(thick, _nestSize.X), Y = boxSize.Y}}, 
                        {new Point(){X = Math.Max(thick, _nestSize.X), Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X - Math.Max(thick, _nestSize.X), Y = boxSize.Y + boxSize.Z}}, 
                        {new Point(){X = boxSize.X - Math.Max(thick, _nestSize.X), Y = boxSize.Y}},
                        #endregion
                        #region Front
                        {new Point(){X = Math.Max(thick, _nestSize.X), Y = 0}}, 
                        {new Point(){X = Math.Max(thick, _nestSize.X), Y = boxSize.Y - Math.Max(thick, _nestSize.Y)}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y - Math.Max(thick, _nestSize.Y)}}, 
                        {new Point(){X = boxSize.X, Y = 0}},
                        #endregion           
                        #region Back
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}}, 
                        {new Point(){X = boxSize.X + boxSize.Z, Y =  boxSize.Y - Math.Max(thick, _nestSize.Y)}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z - Math.Max(thick, _nestSize.X), Y = boxSize.Y - Math.Max(thick, _nestSize.Y)}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z - Math.Max(thick, _nestSize.X), Y = 0}},
                        #endregion  
                        #region Left
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}},
                        {new Point(){X = boxSize.X + boxSize.Z, Y = boxSize.Y - Math.Max(thick, _nestSize.Y)}},                         
                        {new Point(){X = boxSize.X, Y = boxSize.Y - Math.Max(thick, _nestSize.Y)}},
                        {new Point(){X = boxSize.X, Y = 0}}, 
                        #endregion 
                        #region Right
                        {new Point(){X = boxSize.X, Y = 0}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y - Math.Max(thick, _nestSize.Y)}},
                        {new Point(){X = boxSize.X + boxSize.Z, Y = boxSize.Y - Math.Max(thick, _nestSize.Y)}}, 
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}},
                        #endregion 
                        #region Bottom
                        {new Point(){X = Math.Max(thick, _nestSize.X), Y = boxSize.Y + boxSize.Z}}, 
                        {new Point(){X = Math.Max(thick, _nestSize.X), Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y + boxSize.Z}},
                        #endregion
                        };
                    #endregion

                    if (normals != null)
                    {
                        normals[24] = new ObjectVector3D(0, 1, 0);
                        normals[25] = new ObjectVector3D(0, 1, 0);
                        normals[26] = new ObjectVector3D(0, 1, 0);
                        normals[27] = new ObjectVector3D(0, 1, 0);

                        normals[32] = new ObjectVector3D(0, 0, -1);
                        normals[33] = new ObjectVector3D(0, 0, -1);
                        normals[34] = new ObjectVector3D(0, 0, -1);
                        normals[35] = new ObjectVector3D(0, 0, -1);

                        normals[40] = new ObjectVector3D(1, 0, 0);
                        normals[41] = new ObjectVector3D(1, 0, 0);
                        normals[42] = new ObjectVector3D(1, 0, 0);
                        normals[43] = new ObjectVector3D(1, 0, 0);

                        normals[28] = new ObjectVector3D(0, 0, 1);
                        normals[29] = new ObjectVector3D(0, 0, 1);
                        normals[30] = new ObjectVector3D(0, 0, 1);
                        normals[31] = new ObjectVector3D(0, 0, 1);
                    }

                }
                else if (_nestSize.Y == 0 && _nestSize.X > 0 && _nestSize.Z > 0)
                {
                    #region (nest wide and deep)
                    triangleIndices = new List<Int32>(){ 
                        //OUTER
                        0, 1, 2, 0, 2, 3, //front
                        //4, 5, 6, 4, 6, 7, //back
                        8, 9, 10, 8, 10, 11, //left
                        //12, 13, 14, 12, 14, 15,//right
                        //16, 17, 18, 16, 18, 19,//bottom

                         //Edges
                        20, 21, 25, 20, 25, 24, //top
                        21, 22, 25, 22, 26, 25, //top
                        6, 7, 35, 6, 35, 34,//back left
                        16, 17, 45, 45, 44, 16,//bottom
                        16, 44, 47, 16, 47, 19,  //bottom
                        12, 13, 41, 41, 40, 12, //right

                        //INNER
                        //44, 46, 45, 44, 47, 46,//bottom
                        28, 30, 29, 28, 31, 30, //front
                        //32, 34, 33, 32, 35, 34,//back
                        36, 38, 37, 36, 39, 38,//left
                        //40, 42, 41, 40, 43, 42,//right
                        };
                    #endregion

                    #region Texture Coords
                    textureCoords = new List<Point>(){
                        #region Front
                        {new Point(){X = 0, Y = 0}}, 
                        {new Point(){X = 0, Y = boxSize.Y}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X, Y = 0}},
                        #endregion           
                        #region Back
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}}, 
                        {new Point(){X = boxSize.X + boxSize.Z, Y =  boxSize.Y}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = 0}},
                        #endregion  
                        #region Left
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = 0}}, 
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X + boxSize.Z) * 2, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X + boxSize.Z) * 2, Y = 0}},
                        #endregion 
                        #region Right
                        {new Point(){X = boxSize.X, Y = 0}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y}},
                        {new Point(){X = boxSize.X + boxSize.Z, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}},
                        #endregion 
                        #region Bottom
                        {new Point(){X = boxSize.X, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X * 2, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X * 2, Y = boxSize.Y}},
                        #endregion
                        #region Top
                        {new Point(){X = 0, Y = boxSize.Y}}, 
                        {new Point(){X = 0, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y + boxSize.Z}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y}},
                        #endregion

                        #region Top
                        {new Point(){X = Math.Max(thick, _nestSize.X), Y = boxSize.Y}}, 
                        {new Point(){X = Math.Max(thick, _nestSize.X), Y = boxSize.Y + boxSize.Z - Math.Max(thick, _nestSize.Z)}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y + boxSize.Z - Math.Max(thick, _nestSize.Z)}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y}},
                        #endregion
                        #region Front
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z- Math.Max(thick, _nestSize.X), Y = 0}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z - Math.Max(thick, _nestSize.X), Y = boxSize.Y}},
                        {new Point(){X = boxSize.X + boxSize.Z, Y =  boxSize.Y}},
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}},
                        #endregion           
                        #region Back
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = 0}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z- Math.Max(thick, _nestSize.X), Y =  boxSize.Y}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z- Math.Max(thick, _nestSize.X), Y = 0}}, 
                        #endregion  
                        #region Left
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}},
                        {new Point(){X = boxSize.X + boxSize.Z, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X + Math.Max(thick, _nestSize.Z), Y = boxSize.Y}},
                        {new Point(){X = boxSize.X + Math.Max(thick, _nestSize.Z), Y = 0}}, 
                        //{new Point(){X = (productboxSize.X + productboxSize.Z) * 2, Y = productboxSize.Y}},
                        //{new Point(){X = (productboxSize.X + productboxSize.Z) * 2, Y = 0}},
                        //{new Point(){X = (productboxSize.X * 2) + productboxSize.Z + Math.Max(thick, _nestSize.Z), Y = 0}}, 
                        //{new Point(){X = (productboxSize.X * 2) + productboxSize.Z + Math.Max(thick, _nestSize.Z), Y = productboxSize.Y}},
                        #endregion 
                        #region Right
                        {new Point(){X = boxSize.X + Math.Max(thick, _nestSize.Z), Y = 0}}, 
                        {new Point(){X = boxSize.X + Math.Max(thick, _nestSize.Z), Y = boxSize.Y}},
                        {new Point(){X = boxSize.X + boxSize.Z, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}},
                        #endregion 
                        #region Bottom

                         {new Point(){X = boxSize.X + Math.Max(thick, _nestSize.X), Y = boxSize.Y + Math.Max(thick, _nestSize.Z)}}, 
                        {new Point(){X = boxSize.X + Math.Max(thick, _nestSize.X), Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X * 2, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X * 2, Y = boxSize.Y + Math.Max(thick, _nestSize.Z)}},                       
                        #endregion
                        };
                    #endregion

                    if (normals != null)
                    {
                        normals[24] = new ObjectVector3D(0, 1, 0);
                        normals[25] = new ObjectVector3D(0, 1, 0);
                        normals[26] = new ObjectVector3D(0, 1, 0);
                        normals[34] = new ObjectVector3D(0, 0, -1);
                        normals[35] = new ObjectVector3D(0, 0, -1);
                        normals[44] = new ObjectVector3D(0, -1, 0);
                        normals[45] = new ObjectVector3D(0, -1, 0);
                        normals[47] = new ObjectVector3D(0, -1, 0);
                        normals[40] = new ObjectVector3D(1, 0, 0);
                        normals[41] = new ObjectVector3D(1, 0, 0);
                    }

                }
                else if (_nestSize.Y > 0 && _nestSize.X > 0 && _nestSize.Z > 0)
                {
                    #region (nest High and wide)
                    triangleIndices = new List<Int32>(){ 
                        //OUTER
                        0, 1, 2, 0, 2, 3, //front
                        //4, 5, 6, 4, 6, 7, //back
                        8, 9, 10, 8, 10, 11, //left
                        //12, 13, 14, 12, 14, 15,//right
                        16, 17, 18, 16, 18, 19,//bottom

                         //Edge
                        20, 21, 25, 20, 25, 24, //top
                        33, 5, 6, 33, 6, 34,//back
                        6, 7, 35, 6, 35, 34,//back left
                        13, 14, 41, 14, 42, 41,//right
                        25, 21, 22, 25, 22, 26, //top
                        12, 13, 41, 12, 41, 40, //right



                        //INNER
                        44, 46, 45, 44, 47, 46,//bottom
                        28, 30, 29, 28, 31, 30, //front
                        //32, 34, 33, 32, 35, 34,//back
                        36, 38, 37, 36, 39, 38,//left
                        //40, 42, 41, 40, 43, 42,//right
                        };
                    #endregion

                    #region Texture Coords
                    textureCoords = new List<Point>(){
                        #region Front
                        {new Point(){X = 0, Y = 0}}, 
                        {new Point(){X = 0, Y = boxSize.Y}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X, Y = 0}},
                        #endregion           
                        #region Back
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}}, 
                        {new Point(){X = boxSize.X + boxSize.Z, Y =  boxSize.Y}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = 0}},
                        #endregion  
                        #region Left
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = 0}}, 
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X + boxSize.Z) * 2, Y = boxSize.Y}},
                        {new Point(){X = (boxSize.X + boxSize.Z) * 2, Y = 0}},
                        #endregion 
                        #region Right
                        {new Point(){X = boxSize.X, Y = 0}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y}},
                        {new Point(){X = boxSize.X + boxSize.Z, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}},
                        #endregion 
                        #region Bottom
                        {new Point(){X = boxSize.X, Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X * 2, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X * 2, Y = boxSize.Y}},
                        #endregion
                        #region Top
                        {new Point(){X = 0, Y = boxSize.Y}}, 
                        {new Point(){X = 0, Y = boxSize.Y + boxSize.Z}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y + boxSize.Z}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y}},
                        #endregion

                        #region Top
                        {new Point(){X = Math.Max(thick, _nestSize.X), Y = boxSize.Y}}, 
                        {new Point(){X = Math.Max(thick, _nestSize.X), Y = boxSize.Y + boxSize.Z - Math.Max(thick, _nestSize.Z)}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y + boxSize.Z - Math.Max(thick, _nestSize.Z)}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y}},
                        #endregion
                        #region Front
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z - Math.Max(thick, _nestSize.X), Y = 0}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z - Math.Max(thick, _nestSize.X), Y = boxSize.Y - Math.Max(thick, _nestSize.Y)}},
                        {new Point(){X = boxSize.X + boxSize.Z, Y =  boxSize.Y - Math.Max(thick, _nestSize.Y)}},
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}}, 
                        #endregion           
                        #region Back
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}}, 
                        {new Point(){X = boxSize.X + boxSize.Z, Y =  boxSize.Y - Math.Max(thick, _nestSize.Y)}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z - Math.Max(thick, _nestSize.X), Y = boxSize.Y - Math.Max(thick, _nestSize.Y)}},
                        {new Point(){X = (boxSize.X * 2) + boxSize.Z - Math.Max(thick, _nestSize.X), Y = 0}},
                        #endregion  
                        #region Left
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}},
                        {new Point(){X = boxSize.X + boxSize.Z, Y = boxSize.Y - Math.Max(thick, _nestSize.Y)}},                                                 
                        {new Point(){X = boxSize.X + Math.Max(thick, _nestSize.Z), Y = boxSize.Y - Math.Max(thick, _nestSize.Y)}},
                        {new Point(){X = boxSize.X + Math.Max(thick, _nestSize.Z), Y = 0}}, 

                        #endregion 
                        #region Right
                        {new Point(){X = boxSize.X + Math.Max(thick, _nestSize.Z), Y = 0}}, 
                        {new Point(){X = boxSize.X + Math.Max(thick, _nestSize.Z), Y = boxSize.Y - Math.Max(thick, _nestSize.Y)}},
                        {new Point(){X = boxSize.X + boxSize.Z, Y = boxSize.Y - Math.Max(thick, _nestSize.Y)}}, 
                        {new Point(){X = boxSize.X + boxSize.Z, Y = 0}},
                        #endregion 
                        #region Bottom
                        
                        {new Point(){X = Math.Max(thick, _nestSize.X), Y = boxSize.Y + boxSize.Z - Math.Max(thick, _nestSize.Z)}},
                        {new Point(){X = Math.Max(thick, _nestSize.X), Y = boxSize.Y}}, 
                        {new Point(){X = boxSize.X, Y = boxSize.Y}},
                        {new Point(){X = boxSize.X, Y = boxSize.Y + boxSize.Z - Math.Max(thick, _nestSize.Z)}}, 
                        #endregion
                        };
                    #endregion

                    if (normals != null)
                    {
                        normals[24] = new ObjectVector3D(0, 1, 0);
                        normals[25] = new ObjectVector3D(0, 1, 0);
                        normals[26] = new ObjectVector3D(0, 1, 0);
                        normals[27] = new ObjectVector3D(0, 1, 0);

                        normals[32] = new ObjectVector3D(0, 0, -1);
                        normals[33] = new ObjectVector3D(0, 0, -1);
                        normals[34] = new ObjectVector3D(0, 0, -1);
                        normals[35] = new ObjectVector3D(0, 0, -1);

                        normals[40] = new ObjectVector3D(1, 0, 0);
                        normals[41] = new ObjectVector3D(1, 0, 0);
                        normals[42] = new ObjectVector3D(1, 0, 0);
                        normals[43] = new ObjectVector3D(1, 0, 0);

                        normals[28] = new ObjectVector3D(0, 0, 1);
                        normals[29] = new ObjectVector3D(0, 0, 1);
                        normals[30] = new ObjectVector3D(0, 0, 1);
                        normals[31] = new ObjectVector3D(0, 0, 1);
                    }

                }
                #endregion

                this.Positions = positions;
                this.Normals = normals;
                this.TriangleIndices = triangleIndices;
                this.TextureCoordinates = textureCoords;
            }
            else
            {
                AddBox(boxSize.X, boxSize.Y, boxSize.Z);
            }
        }

        public void RotateMesh(ObjectRotation3D rotation)
        {
            if (rotation.Angle == 0 && rotation.Slope == 0 && rotation.Roll == 0) { return; }

            ObjectPoint3D size = this.Size;
            ObjectPoint3D center = new ObjectPoint3D(size.X * 0.5F, size.Y * 0.5F, size.Z * 0.5F);

            Matrix3D rotationMatrix = GetRotationMatrix(rotation, center);
            Matrix3D sizeRotation = GetRotationMatrix(rotation, new ObjectPoint3D());

            ObjectPoint3D rotatedSize = size.Transform(sizeRotation).Abs();
            ObjectPoint3D rotatedCenter = new ObjectPoint3D(rotatedSize.X * 0.5F, rotatedSize.Y * 0.5F, rotatedSize.Z * 0.5F);

            ObjectPoint3D positionOffset = new ObjectPoint3D()
            {
                X = -center.X + rotatedCenter.X,
                Y = -center.Y + rotatedCenter.Y,
                Z = -center.Z + rotatedCenter.Z,
            };

            this.NestSize = this.NestSize.Transform(sizeRotation).Abs();

            RotateGeometry(this, new List<Matrix3D> { rotationMatrix });

            if (this.Normals != null)
            {
                this.Normals = RotateNormals(new List<Matrix3D> { sizeRotation }, this.Normals);
            }

            OffsetPoints(this, positionOffset);

        }

        public void TileMesh(Int32 high, Int32 wide, Int32 deep, Int32 hollowStart, ObjectPoint3D spacing)
        {
            ObjectSize3D nestSize = this.NestSize;

            List<ObjectPoint3D> singleTilePositions = new List<ObjectPoint3D>(this.Positions);
            List<ObjectVector3D> singleTileNormals = (this.Normals != null) ? new List<ObjectVector3D>(this.Normals) : null;
            List<Int32> singleTileTriangleIndices = new List<Int32>(this.TriangleIndices);
            List<Point> singleTileTextureCoordinates = (this.TextureCoordinates != null) ? new List<Point>(this.TextureCoordinates) : null;


            List<ObjectPoint3D> vertices = new List<ObjectPoint3D>();
            List<ObjectVector3D> normals = new List<ObjectVector3D>();
            List<Int32> triangleIndices = new List<Int32>();
            List<Point> textureCoordinates = new List<Point>();


            nestSize.X = Convert.ToSingle(Math.Round(nestSize.X, NestSizeAccuracy));
            nestSize.Y = Convert.ToSingle(Math.Round(nestSize.Y, NestSizeAccuracy));
            nestSize.Z = Convert.ToSingle(Math.Round(nestSize.Z, NestSizeAccuracy));

            //Set the apparent size of the object
            ObjectPoint3D placementSize = new ObjectPoint3D()
            {
                X = nestSize.X > 0 ? Math.Min(this.Size.X, nestSize.X) : this.Size.X,
                Y = nestSize.Y > 0 ? Math.Min(this.Size.Y, nestSize.Y) : this.Size.Y,
                Z = nestSize.Z > 0 ? Math.Min(this.Size.Z, nestSize.Z) : this.Size.Z
            };


            Int32 posCount = 0;
            Boolean draw = false;
            ObjectPoint3D minus = new ObjectPoint3D(1, 1, 1);

            //Ensure that at least one mesh will be drawn.
            if (high <= 0) high = 1;
            if (wide <= 0) wide = 1;
            if (deep <= 0) deep = 1;

            for (Int32 x = 0; x < wide; x++)
            {
                for (Int32 y = 0; y < high; y++)
                {
                    for (Int32 z = 0; z < deep; z++)
                    {
                        draw = hollowStart == 0;
                        draw = draw || (x < hollowStart) || (x >= wide - hollowStart);
                        draw = draw || (y < hollowStart) || (y >= high - hollowStart);
                        draw = draw || (z < hollowStart) || (z >= deep - hollowStart);
                        if (draw)
                        {
                            posCount = vertices.Count;

                            foreach (ObjectPoint3D p in singleTilePositions)
                            {
                                //take into account any nesting when tiling
                                if (nestSize.Y > 0 && nestSize.X == 0 && nestSize.Z > 0)
                                {
                                    vertices.Add(new ObjectPoint3D(
                                        p.X + (minus.X * placementSize.X * x),
                                        p.Y + (minus.Y * placementSize.Y * Math.Max(y, z)),
                                        p.Z + (minus.Z * placementSize.Z * Math.Max(y, z))));
                                }
                                else if (nestSize.Y > 0 && nestSize.X > 0 && nestSize.Z == 0)
                                {
                                    vertices.Add(new ObjectPoint3D(
                                        p.X + (minus.X * placementSize.X * Math.Max(x, y)),
                                        p.Y + (minus.Y * placementSize.Y * Math.Max(x, y)),
                                        p.Z + (minus.Z * placementSize.Z * z)));
                                }
                                else if (nestSize.Y == 0 && nestSize.X > 0 && nestSize.Z > 0)
                                {
                                    vertices.Add(new ObjectPoint3D(
                                        p.X + (minus.X * placementSize.X * Math.Max(x, z)),
                                        p.Y + (minus.Y * placementSize.Y * y),
                                        p.Z + (minus.Z * placementSize.Z * Math.Max(x, z))));
                                }
                                else if (nestSize.Y > 0 && nestSize.X > 0 && nestSize.Z > 0)
                                {
                                    vertices.Add(new ObjectPoint3D(
                                        p.X + (minus.X * placementSize.X * Math.Max(Math.Max(x, y), z)),
                                        p.Y + (minus.Y * placementSize.Y * Math.Max(Math.Max(x, y), z)),
                                        p.Z + (minus.Z * placementSize.Z * Math.Max(Math.Max(x, y), z))));
                                }
                                else
                                {
                                    vertices.Add(new ObjectPoint3D(
                                        p.X + (minus.X * placementSize.X * x) + (spacing.X * x),
                                        p.Y + (minus.Y * placementSize.Y * y) + (spacing.Y * y),
                                        p.Z + (minus.Z * placementSize.Z * z) + (spacing.Z * z)));
                                }

                            }
                            if (Normals != null)
                            {
                                foreach (ObjectVector3D n in singleTileNormals)
                                {
                                    normals.Add(n);
                                }
                            }
                            foreach (Int32 i in singleTileTriangleIndices)
                            {
                                triangleIndices.Add(i + posCount);
                            }
                            if (this.TextureCoordinates != null)
                            {
                                foreach (Point p in singleTileTextureCoordinates)
                                {
                                    textureCoordinates.Add(p);
                                }
                            }

                        }
                    }
                }
            }

            //update the mesh
            this.Positions = vertices;
            this.Normals = normals;
            this.TriangleIndices = triangleIndices;
            this.TextureCoordinates = textureCoordinates;
        }

        public ModelConstructMesh Clone()
        {
            ModelConstructMesh mesh = new ModelConstructMesh(_generateNormals, _generateTextures);

            mesh._triangleIndices = _triangleIndices;
            mesh._positions = _positions;
            mesh._normals = _normals;
            mesh._textureCoordinates = _textureCoordinates;
            mesh._min = _min;
            mesh._max = _max;
            mesh._nestSize = _nestSize;

            return mesh;
        }

        public void Append(ModelConstructMesh mesh, ObjectPoint3D offset)
        {
            ModelConstructMesh translatedMesh = mesh.Clone();
            translatedMesh.Positions = TransformPoints(new List<Matrix3D> { GetTranslateMatrix(offset) }, mesh.Positions);

            //append on the translated mesh.
            Append(translatedMesh);
        }

        /// <summary>
        /// Adds the given mesh to this one.
        /// </summary>
        /// <param name="mesh"></param>
        public void Append(ModelConstructMesh mesh)
        {
            Int32 index0 = this.Positions.Count;

            foreach (var p in mesh.Positions)
            {
                this.Positions.Add(p);
            }

            if (mesh.Normals != null && this.Normals != null)
            {
                foreach (var n in mesh.Normals)
                {
                    this.Normals.Add(n);
                }
            }

            if (mesh.TextureCoordinates != null && this.TextureCoordinates != null)
            {
                foreach (var t in mesh.TextureCoordinates)
                {
                    this.TextureCoordinates.Add(t);
                }
            }

            foreach (int i in mesh.TriangleIndices)
            {
                this.TriangleIndices.Add(index0 + i);
            }
        }

        /// <summary>
        /// Adds a bottle model based on the given dimensions.
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="depth"></param>
        public void AddBottle(Double width, Double height, Double depth)
        {
            Point3D center = new Point3D(width / 2.0, height / 2.0, depth / 2.0);

            Double neckHeight = (height * 0.2);
            Double neckDiameter = (width * 0.3);

            Double coneHeight = (height * 0.15);

            Double baseHeight = height - neckHeight - coneHeight;
            Double baseDiameter = Math.Min(width, depth);


            //draw the base cylinder
            Point3D baseP1 = new Point3D(center.X, 0, center.Z);
            Point3D baseP2 = new Point3D(center.X, baseHeight, center.Z);

            Vector3D baseN = baseP2 - baseP1;
            Double baseLength = baseN.Length;
            baseN.Normalize();
            AddCone(baseP1, baseN, baseDiameter / 2, baseDiameter / 2, baseLength, true, false, 20);


            Point3D joinCenter = new Point3D(center.X, baseHeight, center.Z);
            AddSphere(joinCenter, baseDiameter / 2.0);

            //draw the neck pipe
            Point3D neckP1 = new Point3D(center.X, height - neckHeight - 1, depth / 2.0);
            Point3D neckP2 = new Point3D(center.X, height, center.Z);
            AddPipe(neckP1, neckP2, 0, neckDiameter, 36);


            //scale based on the final box dimensions
            Scale(width / baseDiameter, 1, depth / baseDiameter);
            UpdateMinMax();

            OffsetPoints(this, -this.Min); 

            UpdateMinMax();
        }

        /// <summary>
        /// Adds a jar model based on the given dimensions
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="depth"></param>
        public void AddJar(Double width, Double height, Double depth)
        {
            Point3D center = new Point3D(width / 2.0, height / 2.0, depth / 2.0);

            Double lidHeight = Math.Min(2, height * 0.1);

            Double baseDiameter = Math.Max(width, depth);
            Double lidDiameter = baseDiameter - (baseDiameter * 0.1);

            //add the main jar
            Point3D p1 = new Point3D(center.X, 0, center.Z);
            Point3D p2 = new Point3D(center.X, height - lidHeight, center.Z);

            Vector3D baseN = p2 - p1;
            Double baseLength = baseN.Length;
            baseN.Normalize();
            AddCone(p1, baseN, baseDiameter / 2, lidDiameter / 2, baseLength, true, false, 20);

            //add the lid
            p1 = new Point3D(center.X, height - lidHeight, center.Z);
            p2 = new Point3D(center.X, height, center.Z);
            baseN = p2 - p1;
            baseLength = baseN.Length;
            baseN.Normalize();
            AddCone(p1, baseN, lidDiameter / 2, lidDiameter / 2, baseLength, false, true, 20);


            //scale based on the final box dimensions
            Scale(width / baseDiameter, 1, depth / baseDiameter);
            UpdateMinMax();

            OffsetPoints(this, -this.Min);

            UpdateMinMax();
        }

        /// <summary>
        /// Adds a can model based on the given dimensions
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="depth"></param>
        public void AddCan(Double width, Double height, Double depth)
        {
            Point3D center = new Point3D(width / 2.0, height / 2.0, depth / 2.0);
            Double diameter = Math.Max(width, depth);

            Point3D p1 = new Point3D(center.X, 0, center.Z);
            Point3D p2 = new Point3D(center.X, height, center.Z);

            Vector3D baseN = p2 - p1;
            Double baseLength = baseN.Length;
            baseN.Normalize();
            AddCone(p1, baseN, diameter / 2, diameter / 2, baseLength, true, true, 20);


            //scale based on the final box dimensions
            Scale(width / diameter, 1, depth / diameter);
            UpdateMinMax();

            OffsetPoints(this, -this.Min);

            UpdateMinMax();
        }

        /// <summary>
        /// Outs the current mesh
        /// </summary>
        /// <param name="freeze"></param>
        /// <returns></returns>
        public MeshGeometry3D ToMesh(Boolean freeze = false)
        {
            if (this.Normals != null && this.Positions.Count != this.Normals.Count)
            {
                Debug.WriteLine("Wrong number of normals");
            }

            if (this.TextureCoordinates != null && this.Positions.Count != this.TextureCoordinates.Count)
            {
                Debug.WriteLine("Wrong number of texture coords");
            }

            Point3DCollection positions = new Point3DCollection(this.Positions.Count());
            foreach (ObjectPoint3D p in this.Positions)
            {
                positions.Add(p.ToPoint3D());
            }
            if (freeze)
            {
                positions.Freeze();
            }

            Vector3DCollection normals = null;
            if (this.Normals != null && this.Normals.Count > 0)
            {
                normals = new Vector3DCollection(this.Normals.Count);
                foreach (ObjectVector3D p in this.Normals)
                {
                    normals.Add(p.ToVector3D());
                }

                if (freeze)
                {
                    normals.Freeze();
                }
            }

            PointCollection textureCoords = null;
            if (this.TextureCoordinates != null && this.TextureCoordinates.Count > 0)
            {
                textureCoords = new PointCollection(this.TextureCoordinates);
            }


            MeshGeometry3D mesh = new MeshGeometry3D()
            {
                Positions = positions,
                Normals = normals,
                TriangleIndices = new Int32Collection(this.TriangleIndices),
                TextureCoordinates = textureCoords
            };
            if (freeze)
            {
                mesh.Freeze();
            }

            return mesh;
        }

        #endregion

        #region Static Helpers

        public static void GetMinMaxValues(ModelConstructMesh mesh, out ObjectPoint3D min, out ObjectPoint3D max)
        {
            ObjectPoint3D newMax = new ObjectPoint3D(Single.MinValue, Single.MinValue, Single.MinValue);
            ObjectPoint3D newMin = new ObjectPoint3D(Single.MaxValue, Single.MaxValue, Single.MaxValue);

            foreach (ObjectPoint3D p in mesh.Positions)
            {
                newMax = new ObjectPoint3D(
                    Math.Max(newMax.X, p.X),
                    Math.Max(newMax.Y, p.Y),
                    Math.Max(newMax.Z, p.Z));

                newMin = new ObjectPoint3D(
                    Math.Min(newMin.X, p.X),
                    Math.Min(newMin.Y, p.Y),
                    Math.Min(newMin.Z, p.Z));
            }

            min = newMin;
            max = newMax;
        }

        private static void RotateGeometry(ModelConstructMesh mesh, IEnumerable<Matrix3D> transforms)
        {
            mesh.Positions = TransformPoints(transforms, mesh.Positions);
        }

        public static void OffsetPoints(ModelConstructMesh mesh, ObjectPoint3D offset)
        {
            List<ObjectPoint3D> output = new List<ObjectPoint3D>();
            foreach (ObjectPoint3D point in mesh.Positions)
            {
                ObjectPoint3D p = new ObjectPoint3D()
                {
                    X = point.X + offset.X,
                    Y = point.Y + offset.Y,
                    Z = point.Z + offset.Z
                };
                output.Add(p);
                mesh.Max = new ObjectPoint3D(
                    Math.Max(mesh.Max.X, p.X),
                    Math.Max(mesh.Max.Y, p.Y),
                    Math.Max(mesh.Max.Z, p.Z));
                mesh.Min = new ObjectPoint3D(
                    Math.Min(mesh.Min.X, p.X),
                    Math.Min(mesh.Min.Y, p.Y),
                    Math.Min(mesh.Min.Z, p.Z));
            }

            mesh.Positions = output;
        }

        public static Matrix3D GetRotationMatrix(ObjectRotation3D rotation, ObjectPoint3D center)
        {
            Matrix3D matrix = new Matrix3D();

            RotationQuat a = new RotationQuat(new Vector3D(0, -1, 0), rotation.Angle * (360f / (2 * Math.PI)));
            RotationQuat b = new RotationQuat(new Vector3D(-1, 0, 0), rotation.Slope * (360f / (2 * Math.PI)));
            RotationQuat c = new RotationQuat(new Vector3D(0, 0, -1), rotation.Roll * (360f / (2 * Math.PI)));

            //rotate as z, then x, then y
            matrix *= CreateRotationMatrix(ref c, ref center);
            matrix *= CreateRotationMatrix(ref b, ref center);
            matrix *= CreateRotationMatrix(ref a, ref center);

            return matrix;
        }

        private static List<ObjectVector3D> RotateNormals(IEnumerable<Matrix3D> transform, IEnumerable<ObjectVector3D> points)
        {
            List<ObjectVector3D> output = new List<ObjectVector3D>();
            foreach (ObjectVector3D point in points)
            {
                output.Add(point.Transform(transform));
            }

            return output;
        }

        private static List<ObjectPoint3D> TransformPoints(IEnumerable<Matrix3D> transform, IEnumerable<ObjectPoint3D> points)
        {
            List<ObjectPoint3D> output = new List<ObjectPoint3D>();
            foreach (ObjectPoint3D point in points)
            {
                output.Add(point.Transform(transform));
            }

            return output;
        }

        /// <summary>
        /// Takes the points from a box mesh and moves them to the correct position to be nestable.
        /// </summary>
        /// <param name="backLowerLeft"></param>
        /// <param name="backLowerRight"></param>
        /// <param name="frontLowerRight"></param>
        /// <param name="frontLowerLeft"></param>
        /// <param name="backUpperLeft"></param>
        /// <param name="backUpperRight"></param>
        /// <param name="frontUpperRight"></param>
        /// <param name="frontUpperLeft"></param>
        /// <param name="size"></param>
        /// <param name="nestSize"></param>
        /// <param name="isInnerPoints"></param>
        /// <param name="thick"></param>
        private static void ModifyBoxNestPoints(ref ObjectPoint3D backLowerLeft,
                                ref ObjectPoint3D backLowerRight,
                                ref ObjectPoint3D frontLowerRight,
                                ref ObjectPoint3D frontLowerLeft,
                                ref ObjectPoint3D backUpperLeft,
                                ref ObjectPoint3D backUpperRight,
                                ref ObjectPoint3D frontUpperRight,
                                ref ObjectPoint3D frontUpperLeft,
                                ObjectSize3D size,
                                ObjectSize3D nestSize,
                                Boolean isInnerPoints,
                                out Single thick)
        {
            List<Tuple<Single, Single>> nestValues = new List<Tuple<Single, Single>>()
            {
                new Tuple<Single,Single>(nestSize.X, size.X),
                new Tuple<Single,Single>(nestSize.Y, size.Y),
                new Tuple<Single,Single>(nestSize.Z, size.Z)
            };
            Single minNest = nestValues.Select(i => i.Item1).Where(n => n != 0).Min();
            Single minProductSize = nestValues.Where(i => i.Item1 != 0).Select(n => n.Item2).Min();
            Single maxCutin = minProductSize / 2;
            thick = (maxCutin * 0.05F * minNest) / minProductSize;

            //thick = Math.Min(thick, nestValues.Where(n => n != 0).Min());

            if (isInnerPoints)
            {
                #region Apply thickness
                backLowerLeft.X += Math.Max(thick, nestSize.X);
                backLowerLeft.Y += Math.Max(thick, nestSize.Y);
                backLowerLeft.Z += thick;

                backLowerRight.X -= thick;
                backLowerRight.Y += Math.Max(thick, nestSize.Y);
                backLowerRight.Z += thick;

                frontLowerRight.X -= thick;
                frontLowerRight.Y += Math.Max(thick, nestSize.Y);
                frontLowerRight.Z -= Math.Max(thick, nestSize.Z);

                frontLowerLeft.X += Math.Max(thick, nestSize.X);
                frontLowerLeft.Y += Math.Max(thick, nestSize.Y);
                frontLowerLeft.Z -= Math.Max(thick, nestSize.Z);

                backUpperLeft.X += Math.Max(thick, nestSize.X);
                backUpperLeft.Y -= thick;
                backUpperLeft.Z += thick;

                backUpperRight.X -= thick;
                backUpperRight.Y -= thick;
                backUpperRight.Z += thick;

                frontUpperRight.X -= thick;
                frontUpperRight.Y -= thick;
                frontUpperRight.Z -= Math.Max(thick, nestSize.Z);

                frontUpperLeft.X += Math.Max(thick, nestSize.X);
                frontUpperLeft.Y -= thick;
                frontUpperLeft.Z -= Math.Max(thick, nestSize.Z);
                #endregion
            }

            if (nestSize.Y > 0 && nestSize.X == 0 && nestSize.Z == 0)
            {
                #region Apply cuttin to bottom (nest High)
                Single cutinNestHigh = Math.Min((thick * size.Y) / nestSize.Y, (size.Y / 2) - thick);
                backLowerLeft.X += cutinNestHigh;
                backLowerLeft.Z += cutinNestHigh;

                backLowerRight.X -= cutinNestHigh;
                backLowerRight.Z += cutinNestHigh;

                frontLowerRight.X -= cutinNestHigh;
                frontLowerRight.Z -= cutinNestHigh;

                frontLowerLeft.X += cutinNestHigh;
                frontLowerLeft.Z -= cutinNestHigh;
                #endregion

                #region Flatten edges
                if (isInnerPoints)
                {
                    backUpperLeft.Y += thick;
                    backUpperRight.Y += thick;
                    frontUpperRight.Y += thick;
                    frontUpperLeft.Y += thick;
                }
                #endregion
            }
            else if (nestSize.Y == 0 && nestSize.X == 0 && nestSize.Z > 0)
            {
                #region Apply cuttin to front (nest back)
                Single cutinNestBack = Math.Min((thick * size.Z) / nestSize.Z, (size.Z / 2) - thick);
                frontLowerRight.X -= cutinNestBack;
                frontLowerRight.Y += cutinNestBack;

                frontLowerLeft.X += cutinNestBack;
                frontLowerLeft.Y += cutinNestBack;

                frontUpperRight.X -= cutinNestBack;
                frontUpperRight.Y -= cutinNestBack;

                frontUpperLeft.X += cutinNestBack;
                frontUpperLeft.Y -= cutinNestBack;
                #endregion

                #region Flatten edges
                if (isInnerPoints)
                {

                    backUpperLeft.Z -= thick;
                    backUpperRight.Z -= thick;
                    backLowerLeft.Z -= thick;
                    backLowerRight.Z -= thick;
                }
                #endregion
            }
            else if (nestSize.Y == 0 && nestSize.X > 0 && nestSize.Z == 0)
            {
                #region Apply cuttin to left (nest Right)
                Single cutinNestRight = Math.Min((thick * size.X) / nestSize.X, (size.X / 2) - thick);

                backLowerLeft.Z += cutinNestRight;
                backLowerLeft.Y += cutinNestRight;

                frontLowerLeft.Z -= cutinNestRight;
                frontLowerLeft.Y += cutinNestRight;

                backUpperLeft.Z += cutinNestRight;
                backUpperLeft.Y -= cutinNestRight;

                frontUpperLeft.Z -= cutinNestRight;
                frontUpperLeft.Y -= cutinNestRight;
                #endregion

                #region Flatten edges
                if (isInnerPoints)
                {
                    frontLowerRight.X += thick;
                    backUpperRight.X += thick;
                    frontUpperRight.X += thick;
                    backLowerRight.X += thick;
                }
                #endregion
            }
            else if (nestSize.Y > 0 && nestSize.X == 0 && nestSize.Z > 0)
            {
                #region Flatten edges
                if (isInnerPoints)
                {
                    frontUpperRight.Y += thick;
                    frontUpperLeft.Y += thick;
                    frontUpperRight.X += thick;
                    frontUpperLeft.X -= thick;

                    backLowerLeft.Z -= thick;
                    backLowerRight.Z -= thick;
                    backLowerLeft.X -= thick;
                    backLowerRight.X += thick;

                    frontLowerRight.X += thick;
                    frontLowerLeft.X -= thick;
                }
                #endregion
            }
            else if (nestSize.Y > 0 && nestSize.X > 0 && nestSize.Z == 0)
            {
                #region Flatten edges
                if (isInnerPoints)
                {
                    frontUpperLeft.Y += thick;
                    frontUpperLeft.Z += thick;
                    frontLowerLeft.Z += thick;
                    frontLowerRight.X += thick;
                    frontLowerRight.Z += thick;

                    backUpperLeft.Y += thick;
                    backUpperLeft.Z -= thick;
                    backLowerLeft.Z -= thick;
                    backLowerRight.X += thick;
                    backLowerRight.Z -= thick;

                }
                #endregion
            }
            else if (nestSize.Y == 0 && nestSize.X > 0 && nestSize.Z > 0)
            {
                #region Flatten edges
                if (isInnerPoints)
                {
                    backUpperLeft.Y += thick;
                    backUpperLeft.Z -= thick;
                    frontUpperLeft.Y += thick;
                    frontUpperRight.Y += thick;
                    frontUpperRight.X += thick;

                    backLowerLeft.Y -= thick;
                    backLowerLeft.Z -= thick;
                    frontLowerLeft.Y -= thick;
                    frontLowerRight.Y -= thick;
                    frontLowerRight.X += thick;
                }
                #endregion
            }
            else if (nestSize.Y > 0 && nestSize.X > 0 && nestSize.Z > 0)
            {
                #region Flatten edges
                if (isInnerPoints)
                {
                    backUpperLeft.Y += thick;
                    backUpperLeft.Z -= thick;

                    frontUpperLeft.Y += thick;

                    frontUpperRight.Y += thick;
                    frontUpperRight.X += thick;

                    //backLowerLeft.Y -= thick;
                    backLowerLeft.Z -= thick;

                    backLowerRight.X += thick;
                    backLowerRight.Z -= thick;

                }
                #endregion
            }


        }

        private static Matrix3D CreateRotationMatrix(ref RotationQuat quaternion, ref ObjectPoint3D center)
        {
            Matrix3D matrix = Matrix3D.Identity;
            Double wx, wy, wz, xx, yy, yz, xy, xz, zz, x2, y2, z2;



            x2 = quaternion.X + quaternion.X;

            y2 = quaternion.Y + quaternion.Y;

            z2 = quaternion.Z + quaternion.Z;

            xx = quaternion.X * x2;

            xy = quaternion.X * y2;

            xz = quaternion.X * z2;

            yy = quaternion.Y * y2;

            yz = quaternion.Y * z2;

            zz = quaternion.Z * z2;

            wx = quaternion.W * x2;

            wy = quaternion.W * y2;

            wz = quaternion.W * z2;



            matrix.M11 = 1.0 - (yy + zz);

            matrix.M12 = xy + wz;

            matrix.M13 = xz - wy;

            matrix.M21 = xy - wz;

            matrix.M22 = 1.0 - (xx + zz);

            matrix.M23 = yz + wx;

            matrix.M31 = xz + wy;

            matrix.M32 = yz - wx;

            matrix.M33 = 1.0 - (xx + yy);



            if (center.X != 0 || center.Y != 0 || center.Z != 0)
            {

                matrix.OffsetX = -center.X * matrix.M11 - center.Y * matrix.M21 - center.Z * matrix.M31 + center.X;

                matrix.OffsetY = -center.X * matrix.M12 - center.Y * matrix.M22 - center.Z * matrix.M32 + center.Y;

                matrix.OffsetZ = -center.X * matrix.M13 - center.Y * matrix.M23 - center.Z * matrix.M33 + center.Z;

            }



            return matrix;
        }

        private struct RotationQuat
        {
            private Double _x;
            private Double _y;
            private Double _z;
            private Double _w;

            public Double X { get { return _x; } }
            public Double Y { get { return _y; } }
            public Double Z { get { return _z; } }
            public Double W { get { return _w; } }

            public RotationQuat(Vector3D axis, Double angle)
            {
                angle %= 360.0; // Doing the modulo before converting to radians reduces total error

                Double angleInRadians = angle * (Math.PI / 180.0);

                Double length = axis.Length;
                Vector3D v = (axis / length) * Math.Sin(0.5 * angleInRadians);

                _x = v.X;
                _y = v.Y;
                _z = v.Z;
                _w = Math.Cos(0.5 * angleInRadians);
            }


        }

        /// <summary>
        /// Creates a translation matrix3D
        /// </summary>
        /// <param name="offset"></param>
        /// <returns></returns>
        public static Matrix3D GetTranslateMatrix(ObjectPoint3D offset)
        {
            Matrix3D matrix = new Matrix3D();

            matrix.M11 = matrix.M22 = matrix.M33 = matrix.M44 = 1.0;

            matrix.OffsetX = offset.X;

            matrix.OffsetY = offset.Y;

            matrix.OffsetZ = offset.Z;

            return matrix;
        }

        #endregion

        #region Helix Mesh Builder Methods

        //All code in this region taken and amended from Helix 3D Toolkit
        //http://helixtoolkit.codeplex.com, license: MIT


        /// <summary>
        /// Adds a sphere.
        /// </summary>
        /// <param name="center">
        /// The center of the sphere.
        /// </param>
        /// <param name="radius">
        /// The radius of the sphere.
        /// </param>
        /// <param name="thetaDiv">
        /// The number of divisions around the sphere.
        /// </param>
        /// <param name="phiDiv">
        /// The number of divisions from top to bottom of the sphere.
        /// </param>
        private void AddSphere(Point3D center, Double radius, Int32 thetaDiv = 20, Int32 phiDiv = 10)
        {
            AddEllipsoid(center, radius, radius, radius, thetaDiv, phiDiv);
        }

        /// <summary>
        /// Adds an ellipsoid.
        /// </summary>
        /// <param name="center">
        /// The center of the ellipsoid.
        /// </param>
        /// <param name="radiusx">
        /// The x radius of the ellipsoid.
        /// </param>
        /// <param name="radiusy">
        /// The y radius of the ellipsoid.
        /// </param>
        /// <param name="radiusz">
        /// The z radius of the ellipsoid.
        /// </param>
        /// <param name="thetaDiv">
        /// The number of divisions around the ellipsoid.
        /// </param>
        /// <param name="phiDiv">
        /// The number of divisions from top to bottom of the ellipsoid.
        /// </param>
        private void AddEllipsoid(Point3D center, Double radiusx, Double radiusy, Double radiusz, Int32 thetaDiv = 20, Int32 phiDiv = 10)
        {
            int index0 = this.Positions.Count;
            double dt = 2 * Math.PI / thetaDiv;
            double dp = Math.PI / phiDiv;

            for (int pi = 0; pi <= phiDiv; pi++)
            {
                double phi = pi * dp;

                for (int ti = 0; ti <= thetaDiv; ti++)
                {
                    // we want to start the mesh on the x axis
                    double theta = ti * dt;

                    // Spherical coordinates
                    // http://mathworld.wolfram.com/SphericalCoordinates.html
                    double x = Math.Cos(theta) * Math.Sin(phi);
                    double y = Math.Sin(theta) * Math.Sin(phi);
                    double z = Math.Cos(phi);

                    var p = new Point3D(center.X + (radiusx * x), center.Y + (radiusy * y), center.Z + (radiusz * z));
                    this.Positions.Add(p.ToObjectPoint3D());

                    if (this.Normals != null)
                    {
                        var n = new Vector3D(x, y, z);
                        this.Normals.Add(n.ToObjectVector3D());
                    }

                    if (this.TextureCoordinates != null)
                    {
                        var uv = new Point(theta / (2 * Math.PI), phi / Math.PI);
                        this.TextureCoordinates.Add(uv);
                    }
                }
            }

            AddRectangularMeshTriangleIndices(index0, phiDiv + 1, thetaDiv + 1, true);
        }

        /// <summary>
        /// Add triangle indices for a rectangular mesh.
        /// </summary>
        /// <param name="index0">
        /// The index offset.
        /// </param>
        /// <param name="rows">
        /// The number of rows.
        /// </param>
        /// <param name="columns">
        /// The number of columns.
        /// </param>
        /// <param name="isSpherical">
        /// set the flag to true to create a sphere mesh (triangles at top and bottom).
        /// </param>
        private void AddRectangularMeshTriangleIndices(Int32 index0, Int32 rows, Int32 columns, Boolean isSpherical = false)
        {
            for (int i = 0; i < rows - 1; i++)
            {
                for (int j = 0; j < columns - 1; j++)
                {
                    int ij = (i * columns) + j;
                    if (!isSpherical || i > 0)
                    {
                        this.TriangleIndices.Add(index0 + ij);
                        this.TriangleIndices.Add(index0 + ij + 1 + columns);
                        this.TriangleIndices.Add(index0 + ij + 1);
                    }

                    if (!isSpherical || i < rows - 2)
                    {
                        this.TriangleIndices.Add(index0 + ij + 1 + columns);
                        this.TriangleIndices.Add(index0 + ij);
                        this.TriangleIndices.Add(index0 + ij + columns);
                    }
                }
            }
        }

        /// <summary>
        /// Adds a cylinder to the mesh.
        /// </summary>
        /// <param name="p1">
        /// The first point.
        /// </param>
        /// <param name="p2">
        /// The second point.
        /// </param>
        /// <param name="diameter">
        /// The diameters.
        /// </param>
        /// <param name="thetaDiv">
        /// The number of divisions around the cylinder.
        /// </param>
        /// <remarks>
        /// See http://en.wikipedia.org/wiki/Cylinder_(geometry).
        /// </remarks>
        private void AddCylinder(Point3D p1, Point3D p2, Double diameter, Int32 thetaDiv)
        {
            Vector3D n = p2 - p1;
            Double l = n.Length;
            n.Normalize();
            AddCone(p1, n, diameter / 2, diameter / 2, l, false, false, thetaDiv);
        }

        /// <summary>
        /// Adds a (possibly truncated) cone.
        /// </summary>
        /// <param name="origin">
        /// The origin.
        /// </param>
        /// <param name="direction">
        /// The direction (normalization not required).
        /// </param>
        /// <param name="baseRadius">
        /// The base radius.
        /// </param>
        /// <param name="topRadius">
        /// The top radius.
        /// </param>
        /// <param name="height">
        /// The height.
        /// </param>
        /// <param name="baseCap">
        /// Include a base cap if set to <c>true</c> .
        /// </param>
        /// <param name="topCap">
        /// Include the top cap if set to <c>true</c> .
        /// </param>
        /// <param name="thetaDiv">
        /// The number of divisions around the cone.
        /// </param>
        /// <remarks>
        /// See http://en.wikipedia.org/wiki/Cone_(geometry).
        /// </remarks>
        private void AddCone(
            Point3D origin,
            Vector3D direction,
            Double baseRadius,
            Double topRadius,
            Double height,
            Boolean baseCap,
            Boolean topCap,
            Int32 thetaDiv)
        {
            var pc = new List<Point>();
            if (baseCap)
            {
                pc.Add(new Point(0, 0));
            }

            pc.Add(new Point(0, baseRadius));
            pc.Add(new Point(height, topRadius));
            if (topCap)
            {
                pc.Add(new Point(height, 0));
            }

            AddRevolvedGeometry(pc, origin, direction, thetaDiv);
        }

        /// <summary>
        /// Adds a surface of revolution.
        /// </summary>
        /// <param name="points">
        /// The points (x coordinates are distance from the origin along the axis of revolution, y coordinates are radius, )
        /// </param>
        /// <param name="origin">
        /// The origin of the revolution axis.
        /// </param>
        /// <param name="direction">
        /// The direction of the revolution axis.
        /// </param>
        /// <param name="thetaDiv">
        /// The number of divisions around the mesh.
        /// </param>
        /// <remarks>
        /// See http://en.wikipedia.org/wiki/Surface_of_revolution.
        /// </remarks>
        private void AddRevolvedGeometry(IList<Point> points, Point3D origin, Vector3D direction, Int32 thetaDiv)
        {
            direction.Normalize();

            // Find two unit vectors orthogonal to the specified direction
            Vector3D u = direction.FindAnyPerpendicular();
            Vector3D v = Vector3D.CrossProduct(direction, u);

            u.Normalize();
            v.Normalize();

            var circle = GetCircle(thetaDiv);

            int index0 = this.Positions.Count;
            int n = points.Count;

            int totalNodes = (points.Count - 1) * 2 * thetaDiv;
            int rowNodes = (points.Count - 1) * 2;

            for (int i = 0; i < thetaDiv; i++)
            {
                var w = (v * circle[i].X) + (u * circle[i].Y);

                for (int j = 0; j + 1 < n; j++)
                {
                    // Add segment
                    var q1 = origin + (direction * points[j].X) + (w * points[j].Y);
                    var q2 = origin + (direction * points[j + 1].X) + (w * points[j + 1].Y);

                    // TODO: should not add segment if q1==q2 (corner point)
                    // const double eps = 1e-6;
                    // if (Point3D.Subtract(q1, q2).LengthSquared < eps)
                    // continue;
                    double tx = points[j + 1].X - points[j].X;
                    double ty = points[j + 1].Y - points[j].Y;

                    var normal = (-direction * ty) + (w * tx);
                    normal.Normalize();

                    this.Positions.Add(q1.ToObjectPoint3D());
                    this.Positions.Add(q2.ToObjectPoint3D());

                    if (this.Normals != null)
                    {
                        this.Normals.Add(normal.ToObjectVector3D());
                        this.Normals.Add(normal.ToObjectVector3D());
                    }

                    if (this.TextureCoordinates != null)
                    {
                        this.TextureCoordinates.Add(new Point((double)i / (thetaDiv - 1), (double)j / (n - 1)));
                        this.TextureCoordinates.Add(new Point((double)i / (thetaDiv - 1), (double)(j + 1) / (n - 1)));
                    }

                    int i0 = index0 + (i * rowNodes) + (j * 2);
                    int i1 = i0 + 1;
                    int i2 = index0 + ((((i + 1) * rowNodes) + (j * 2)) % totalNodes);
                    int i3 = i2 + 1;

                    this.TriangleIndices.Add(i1);
                    this.TriangleIndices.Add(i0);
                    this.TriangleIndices.Add(i2);

                    this.TriangleIndices.Add(i1);
                    this.TriangleIndices.Add(i2);
                    this.TriangleIndices.Add(i3);
                }
            }
        }

        /// <summary>
        /// Gets a circle section (cached).
        /// </summary>
        /// <param name="thetaDiv">
        /// The number of division.
        /// </param>
        /// <returns>
        /// A circle.
        /// </returns>
        private static IList<Point> GetCircle(Int32 thetaDiv)
        {
            IList<Point> circle;
            //if (!CircleCache.Value.TryGetValue(thetaDiv, out circle))
            //{
            circle = new List<Point>();
            //CircleCache.Value.Add(thetaDiv, circle);
            for (int i = 0; i < thetaDiv; i++)
            {
                double theta = Math.PI * 2 * ((double)i / (thetaDiv - 1));
                circle.Add(new Point(Math.Cos(theta), -Math.Sin(theta)));
            }

            // PointCollection is not thread safe unless frozen
            //((PointCollection)circle).Freeze();
            //}

            return circle;
        }

        /// <summary>
        /// Adds a (possibly hollow) pipe.
        /// </summary>
        /// <param name="point1">
        /// The start point.
        /// </param>
        /// <param name="point2">
        /// The end point.
        /// </param>
        /// <param name="innerDiameter">
        /// The inner diameter.
        /// </param>
        /// <param name="diameter">
        /// The outer diameter.
        /// </param>
        /// <param name="thetaDiv">
        /// The number of divisions around the pipe.
        /// </param>
        private void AddPipe(Point3D point1, Point3D point2, Double innerDiameter, Double diameter, Int32 thetaDiv)
        {
            var dir = point2 - point1;

            Double height = dir.Length;
            dir.Normalize();

            var pc = new List<Point>
                {
                    new Point(0, innerDiameter / 2),
                    new Point(0, diameter / 2),
                    new Point(height, diameter / 2),
                    new Point(height, innerDiameter / 2)
                };

            if (innerDiameter > 0)
            {
                // Add the inner surface
                pc.Add(new Point(0, innerDiameter / 2));
            }

            AddRevolvedGeometry(pc, point1, dir, thetaDiv);
        }

        /// <summary>
        /// Scales the positions (and normal vectors).
        /// </summary>
        /// <param name="scaleX">
        /// The X scale factor.
        /// </param>
        /// <param name="scaleY">
        /// The Y scale factor.
        /// </param>
        /// <param name="scaleZ">
        /// The Z scale factor.
        /// </param>
        private void Scale(double scaleX, double scaleY, double scaleZ)
        {
            for (int i = 0; i < this.Positions.Count; i++)
            {
                this.Positions[i] = new Point3D(
                    this.Positions[i].X * scaleX, this.Positions[i].Y * scaleY, this.Positions[i].Z * scaleZ).ToObjectPoint3D();
            }

            if (this.Normals != null)
            {
                for (int i = 0; i < this.Normals.Count; i++)
                {
                    this.Normals[i] = new Vector3D(
                        this.Normals[i].X * scaleX, this.Normals[i].Y * scaleY, this.Normals[i].Z * scaleZ).ToObjectVector3D();
                    this.Normals[i].Normalize();
                }
            }
        }

        /// <summary>
        /// Adds the given box face
        /// </summary>
        /// <param name="center"></param>
        /// <param name="normal"></param>
        /// <param name="up"></param>
        /// <param name="dist"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        private void AddBoxFace(Point3D center, Vector3D normal, Vector3D up, Double dist, Double width, Double height)
        {
            Vector3D right = Vector3D.CrossProduct(normal, up);
            Vector3D n = normal * dist / 2;
            up *= height / 2;
            right *= width / 2;

            Point3D p1 = center + n - up - right;
            Point3D p2 = center + n - up + right;
            Point3D p3 = center + n + up + right;
            Point3D p4 = center + n + up - right;

            Int32 i0 = this.Positions.Count;
            this.Positions.Add(p1.ToObjectPoint3D());
            this.Positions.Add(p2.ToObjectPoint3D());
            this.Positions.Add(p3.ToObjectPoint3D());
            this.Positions.Add(p4.ToObjectPoint3D());

            if (this.Normals != null)
            {
                this.Normals.Add(normal.ToObjectVector3D());
                this.Normals.Add(normal.ToObjectVector3D());
                this.Normals.Add(normal.ToObjectVector3D());
                this.Normals.Add(normal.ToObjectVector3D());
            }

            if (this.TextureCoordinates != null)
            {
                this.TextureCoordinates.Add(new Point(1, 1));
                this.TextureCoordinates.Add(new Point(0, 1));
                this.TextureCoordinates.Add(new Point(0, 0));
                this.TextureCoordinates.Add(new Point(1, 0));
            }

            this.TriangleIndices.Add(i0 + 2);
            this.TriangleIndices.Add(i0 + 1);
            this.TriangleIndices.Add(i0 + 0);
            this.TriangleIndices.Add(i0 + 0);
            this.TriangleIndices.Add(i0 + 3);
            this.TriangleIndices.Add(i0 + 2);
        }

        #endregion

        #region Build Instructions

        public static ModelConstructMesh Build(MeshBuilderInstructions instructionGroup)
        {
            ModelConstructMesh mesh =
                new ModelConstructMesh(instructionGroup.GenerateNormals, instructionGroup.GenerateTextures);

            foreach (MeshBuilderInstruction i in instructionGroup.Instructions)
            {
                Execute(i, mesh);
            }

            return mesh;
        }

        private static void ExecuteGroup(MeshBuilderInstructions instructionGroup, ModelConstructMesh mesh)
        {
            ModelConstructMesh newMesh = new ModelConstructMesh(instructionGroup.GenerateNormals, instructionGroup.GenerateTextures);
            foreach (MeshBuilderInstruction i in instructionGroup.Instructions)
            {
                Execute(i, newMesh);
            }

            //append onto the existing mesh
            mesh.Append(newMesh, instructionGroup.AppendPosition);
        }

        private static void Execute(MeshBuilderInstruction instruction, ModelConstructMesh mesh)
        {
            if (instruction is MeshBuilderInstructions)
            {
                ExecuteGroup((MeshBuilderInstructions)instruction, mesh);
            }
            else
            {
                Object[] args = instruction.Args;

                switch (instruction.InstructionType)
                {
                    case MeshBuilderInstructionType.AddBox:
                        mesh.AddBox((Double)instruction.Args[0], (Double)instruction.Args[1], (Double)instruction.Args[2]);
                        break;

                    case MeshBuilderInstructionType.AddBoxFaces:
                        mesh.AddBoxFaces((ObjectPoint3D)args[0], (Double)args[1], (Double)args[2], (Double)args[3], (BoxFaceType)args[4]);
                        break;

                    case MeshBuilderInstructionType.AddNestableBox:
                        mesh.AddNestableBox((ObjectSize3D)instruction.Args[0], (ObjectSize3D)instruction.Args[1], (Boolean)instruction.Args[2]);
                        break;

                    case MeshBuilderInstructionType.AddBottle:
                        mesh.AddBottle((Double)instruction.Args[0], (Double)instruction.Args[1], (Double)instruction.Args[2]);
                        break;

                    case MeshBuilderInstructionType.AddJar:
                        mesh.AddJar((Double)instruction.Args[0], (Double)instruction.Args[1], (Double)instruction.Args[2]);
                        break;

                    case MeshBuilderInstructionType.AddCan:
                        mesh.AddCan((Double)instruction.Args[0], (Double)instruction.Args[1], (Double)instruction.Args[2]);
                        break;

                    case MeshBuilderInstructionType.RotateMesh:
                        mesh.RotateMesh((ObjectRotation3D)instruction.Args[0]);
                        break;

                    case MeshBuilderInstructionType.TileMesh:
                        mesh.TileMesh((Int32)instruction.Args[0], (Int32)instruction.Args[1],
                            (Int32)instruction.Args[2], (Int32)instruction.Args[3], (ObjectPoint3D)instruction.Args[4]);
                        break;

                    default: throw new NotImplementedException();
                }
            }
        }

        #endregion
    }
}
