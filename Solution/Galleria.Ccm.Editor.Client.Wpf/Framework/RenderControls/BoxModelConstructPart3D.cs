﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Copied and ammended from GFS.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;
using Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData;
using HelixToolkit.Wpf;

//TODO: Classes under this namespace will eventually move into the framework.
namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderControls
{
    /// <summary>
    /// Implementation of a model construct part that is a box.
    /// </summary>
    public class BoxModelConstructPart3D : ModelConstructPart3D
    {
        #region Fields
        private readonly IBoxModelPart3DData _partData;
        private GeometryModel3D _cutoutsModel;
        #endregion

        #region Properties

        public IBoxModelPart3DData PartData
        {
            get { return _partData; }
        }

        protected BoxFaceType BoxFaces
        {
            get { return PartData.BoxFaces; }
        }

        protected Double BoxFaceThickness
        {
            get { return PartData.BoxFaceThickness; }
        }

        protected ObjectMaterial3D BackFaceMaterial
        {
            get
            {
                ObjectMaterial3D mat = PartData.BackFaceMaterial;

                if (mat == null)
                {
                    mat = this.Material;
                }

                return mat;
            }
        }
        protected ObjectMaterial3D TopFaceMaterial
        {
            get
            {
               ObjectMaterial3D mat = PartData.TopFaceMaterial;

                if (mat == null)
                {
                    mat = this.Material;
                }

                return mat;
            }
        }
        protected ObjectMaterial3D BottomFaceMaterial
        {
            get
            {
                ObjectMaterial3D mat = PartData.BottomFaceMaterial;

                if (mat == null)
                {
                    mat = this.Material;
                }

                return mat;
            }
        }
        protected ObjectMaterial3D LeftFaceMaterial
        {
            get
            {
                ObjectMaterial3D mat = PartData.LeftFaceMaterial;

                if (mat == null)
                {
                    mat = this.Material;
                }

                return mat;
            }
        }
        protected ObjectMaterial3D RightFaceMaterial
        {
            get
            {
               ObjectMaterial3D mat = PartData.RightFaceMaterial;

                if (mat == null)
                {
                    mat = this.Material;
                }

                return mat;
            }
        }


        public Boolean RenderCutouts
        {
            get {return PartData.RenderCutouts;}
        }

        public IEnumerable<IModelConstructPart3DCutout> CutoutDefintions
        {
            get {return PartData.CutoutDefinitions;}
        }

        public Object Data
        {
            get {return PartData.Data;}
        }

        protected GeometryModel3D CutoutsModel
        {
            get { return _cutoutsModel; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="modelPartData">The part data.</param>
        public BoxModelConstructPart3D(IBoxModelPart3DData modelPartData)
            : base(modelPartData)
        {
            _partData = modelPartData;
            UpdateModel();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on the model part data.
        /// </summary>
        /// <param name="propertyName"></param>
        protected override void OnModelPartDataPropertyChanged(String propertyName)
        {
            base.OnModelPartDataPropertyChanged(propertyName);

            if (propertyName == "BoxFaces"
                || propertyName == "BoxFaceThickness")
            {
                OnGeometryChanged();
            }
            else if (propertyName == "BackFaceMaterial"
                || propertyName == "TopFaceMaterial"
                || propertyName == "BottomFaceMaterial"
                || propertyName == "LeftFaceMaterial"
                || propertyName == "RightFaceMaterial")
            {
                OnMaterialChanged();
            }
            else if (propertyName == "Data")
            {
                UpdateModel();
            }
            else if (propertyName == "RenderCutouts")
            {
                TessellateCutouts();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates the model geometry
        /// </summary>
        protected override void Tessellate()
        {
            Boolean materialChanged = false;

            if (this.Data is ITiled)
            {
                #region Tiled Box

                materialChanged = EnsureMainModelCount(1);

                //+ create the main visual
                Geometry3D geometry = new MeshGeometry3D();

                //only bother rendering if this is visible
                if (this.IsVisible
                    && this.Size.X > 0
                    && this.Size.Y > 0
                    && this.Size.Z > 0)
                {
                    Size3D size = new Size3D(this.Size.X, this.Size.Y, this.Size.Z);
                    ObjectPoint3D position = this.Position;
                    Point3D center = new Point3D(size.X / 2, size.Y / 2, size.Z / 2);

                    //Start with a simple box mesh
                    ModelConstructMesh mb = new ModelConstructMesh(true, true);
                    mb.AddBox(size.X, size.Y, size.Z);
                    MeshGeometry3D mesh = mb.ToMesh();

                    if (this.Data is ITiled)
                    {
                        ITiled tileData = (ITiled)this.Data;

                        mesh = ModelConstruct3DHelper.CreateTiledBoxMesh(size, mesh,
                                tileData.High, tileData.Wide, tileData.Deep,
                                tileData.HollowStart,
                            /*rotateTransform*/ModelConstruct3DHelper.CreateYawPitchRoll(this.Rotation),
                                tileData.FirstRowReduction,
                                new Point3D(tileData.SpacingX, tileData.SpacingY, tileData.SpacingZ), 
                                tileData.IsSingleUnitRotation);

                        TileModelMesh(mesh);
                    }
                    else
                    {
                        TileModelMesh(mesh);
                    }

                    geometry = mesh;
                }


                //apply the geometries
                GetMainModels().First().Geometry = geometry;

                #endregion
            }
            else
            {
                #region Normal Box

                List<BoxFaceType> faces = GetSeparateFaces(this.BoxFaces);

                Int32 modelCount =
                    (faces.Count != 6 || !AreAllFacesSameMaterial()) ? faces.Count : 1;

                materialChanged = EnsureMainModelCount(modelCount);

                ObjectSize3D size = this.Size;
                ObjectPoint3D position = this.Position;
                Point3D center = new Point3D(size.X / 2, size.Y / 2, size.Z / 2);


                if (this.BoxFaces == BoxFaceType.All && modelCount == 1)
                {
                    Geometry3D geometry = new MeshGeometry3D();

                    //only bother rendering if this is visible
                    if (this.IsVisible
                        && size.X > 0
                        && size.Y > 0
                        && size.Z > 0)
                    {

                        //Start with a simple box mesh
                        ModelConstructMesh mb = new ModelConstructMesh(true, true);
                        mb.AddBox(size.X, size.Y, size.Z);
                        MeshGeometry3D mesh = mb.ToMesh();

                        TileModelMesh(mesh);

                        geometry = mesh;
                    }

                    GetMainModels().First().Geometry = geometry;
                }
                else
                {
                    Double faceThickness = this.BoxFaceThickness;
                    Double sideYCenter = center.Y;
                    Double sideYLength = size.Y;
                    if (faces.Contains(BoxFaceType.Bottom))
                    {
                        sideYCenter += (faceThickness / 2);
                        sideYLength -= faceThickness;
                    }
                    if (faces.Contains(BoxFaceType.Top))
                    {
                        sideYCenter -= (faceThickness / 2);
                        sideYLength -= faceThickness;
                    }



                    Int32 modelIdx = 0;

                    foreach (BoxFaceType f in faces)
                    {
                        Geometry3D geometry = new MeshGeometry3D();

                        if (this.IsVisible
                        && size.X > 0
                        && size.Y > 0
                        && size.Z > 0)
                        {
                            Model3DBuilder mb = new Model3DBuilder();

                            if (this.BoxFaceThickness == 0)
                            {
                                mb.AddBoxFaces(center, size.X, size.Y, size.Z, f);
                            }
                            else
                            {
                                if (f == BoxFaceType.Front)
                                {
                                    Point3D frontFaceCenter = new Point3D(center.X, sideYCenter, center.Z + (size.Z / 2) - (faceThickness / 2));
                                    mb.AddBox(frontFaceCenter, size.X, sideYLength, faceThickness);
                                }
                                else if (f == BoxFaceType.Back)
                                {
                                    Point3D backFaceCenter = new Point3D(center.X, sideYCenter, center.Z - (size.Z / 2) + (faceThickness / 2));
                                    mb.AddBox(backFaceCenter, size.X, sideYLength, faceThickness);
                                }
                                else if (f == BoxFaceType.Left)
                                {
                                    Point3D leftFaceCenter = new Point3D(center.X - (size.X / 2) + (faceThickness / 2), sideYCenter, center.Z);
                                    mb.AddBox(leftFaceCenter, faceThickness, sideYLength, size.Z);
                                }
                                else if (f == BoxFaceType.Right)
                                {
                                    Point3D rightFaceCenter = new Point3D(center.X + (size.X / 2) - (faceThickness / 2), sideYCenter, center.Z);
                                    mb.AddBox(rightFaceCenter, faceThickness, sideYLength, size.Z);
                                }
                                else if (f == BoxFaceType.Bottom)
                                {
                                    Point3D bottomFaceCenter = new Point3D(center.X, center.Y - (size.Y / 2) + (faceThickness / 2), center.Z);
                                    mb.AddBox(bottomFaceCenter, size.X, faceThickness, size.Z);
                                }
                                else if (f == BoxFaceType.Top)
                                {
                                    Point3D topFaceCenter = new Point3D(center.X, center.Y + (size.Y / 2) - (faceThickness / 2), center.Z);
                                    mb.AddBox(topFaceCenter, size.X, faceThickness, size.Z);
                                }
                            }

                            MeshGeometry3D mesh = mb.ToMesh();

                            TileModelMesh(mesh);

                            geometry = mesh;
                        }

                        GetMainModels().ElementAt(modelIdx).Geometry = geometry;


                        modelIdx++;
                    }

                }
                #endregion
            }


            TessellateWireframe();
            TessellateCutouts();

            if (materialChanged)
            {
                OnMaterialChanged();
            }
        }

        /// <summary>
        /// Applies the model materials
        /// </summary>
        protected override void ApplyMaterials()
        {
            //Update the box model material
            Int32 expectedModelCount = 0;
            if (this.Data is ITiled)
            {
                expectedModelCount = 1;
            }
            else
            {
                List<BoxFaceType> faces = GetSeparateFaces(this.BoxFaces);

                expectedModelCount =
                    (faces.Count != 6 || !AreAllFacesSameMaterial()) ? faces.Count : 1;
            }

            if (GetMainModels().Count() == expectedModelCount)
            {
                Dictionary<BoxFaceType, ObjectMaterial3D> matDict = new Dictionary<BoxFaceType, ObjectMaterial3D>();
                matDict.Add(BoxFaceType.Front, this.Material);
                matDict.Add(BoxFaceType.Back, this.BackFaceMaterial);
                matDict.Add(BoxFaceType.Top, this.TopFaceMaterial);
                matDict.Add(BoxFaceType.Bottom, this.BottomFaceMaterial);
                matDict.Add(BoxFaceType.Left, this.LeftFaceMaterial);
                matDict.Add(BoxFaceType.Right, this.RightFaceMaterial);


                if (this.Data is ITiled)
                {
                    //Create tile size minus first row reduction 
                    //so that top lines are correct.
                    // - Commented out as this messed up front lines.. *facepalm*
                    ObjectSize3D tileSize =
                        new ObjectSize3D()
                        {
                            X = this.Size.X,
                            Y = this.Size.Y /*- ((ITiled)this.Data).FirstRowReduction*/,
                            Z = this.Size.Z
                        };


                    Brush matBrush = ModelConstruct3DHelper.CreateBoxMaterialBrush(matDict, tileSize,
                        this.PartData.LineThickness, this.PartData.LineColour);

                    MaterialGroup mGroup = new MaterialGroup();
                    mGroup.Children.Add(new DiffuseMaterial(Brushes.Black));
                    mGroup.Children.Add(new DiffuseMaterial(matBrush));
                    mGroup.Freeze();

                    GetMainModels().First().Material = mGroup;
                }
                else
                {
                    if (this.BoxFaces == BoxFaceType.All && expectedModelCount == 1)
                    {
                        ApplyMaterial(GetMainModels().First(), this.Material);
                    }
                    else
                    {
                        List<BoxFaceType> faces = GetSeparateFaces(this.BoxFaces);

                        Int32 modelIdx = 0;
                        foreach (BoxFaceType f in faces)
                        {
                            ApplyMaterial(GetMainModels().ElementAt(modelIdx), matDict[f]);
                            modelIdx++;
                        }
                    }

                }

            }
            else
            {
                //reload.
                Tessellate();
            }



            //update the material of the wireframe model.
            ApplyWireframeMaterial();
        }

        /// <summary>
        /// Returns true if all faces have the same material.
        /// </summary>
        /// <returns></returns>
        private Boolean AreAllFacesSameMaterial()
        {
            ObjectMaterial3D mat = this.Material;

            if (this.BottomFaceMaterial != mat) { return false; }
            if (this.TopFaceMaterial != mat) { return false; }
            if (this.BackFaceMaterial != mat) { return false; }
            if (this.LeftFaceMaterial != mat) { return false; }
            if (this.RightFaceMaterial != mat) { return false; }

            return true;
        }

        /// <summary>
        /// Returns a list of the separate faces to be created.
        /// </summary>
        /// <param name="boxFaces"></param>
        /// <returns></returns>
        private static List<BoxFaceType> GetSeparateFaces(BoxFaceType boxFaces)
        {
            List<BoxFaceType> faces = new List<BoxFaceType>();

            if ((boxFaces & BoxFaceType.Front) == BoxFaceType.Front) faces.Add(BoxFaceType.Front);
            if ((boxFaces & BoxFaceType.Back) == BoxFaceType.Back) faces.Add(BoxFaceType.Back);
            if ((boxFaces & BoxFaceType.Top) == BoxFaceType.Top) faces.Add(BoxFaceType.Top);
            if ((boxFaces & BoxFaceType.Bottom) == BoxFaceType.Bottom) faces.Add(BoxFaceType.Bottom);
            if ((boxFaces & BoxFaceType.Left) == BoxFaceType.Left) faces.Add(BoxFaceType.Left);
            if ((boxFaces & BoxFaceType.Right) == BoxFaceType.Right) faces.Add(BoxFaceType.Right);

            return faces;
        }

        protected override IEnumerable<GeometryModel3D> GetMainModels()
        {
            foreach (Model3D m in Content.Children)
            {
                if (m != base.WireframeGeometryModel
                    && m != _cutoutsModel)
                {
                    if (m is GeometryModel3D)
                    {
                        yield return (GeometryModel3D)m;
                    }
                }
            }
        }

        /// <summary>
        /// Creates the geometry for the cutouts.
        /// </summary>
        protected void TessellateCutouts()
        {
            if (this.IsVisible && this.RenderCutouts && this.CutoutDefintions.Count() > 0)
            {
                IEnumerable<IModelConstructPart3DCutout> cutoutDefinitions = this.CutoutDefintions;

                //create the model if missing
                if (_cutoutsModel == null)
                {
                    _cutoutsModel = new GeometryModel3D();
                    _cutoutsModel.Material = ModelConstruct3DHelper.CreateMaterial(Brushes.Black);
                    //_cutoutsModel.BackMaterial = _cutoutsModel.Material; //don't apply for performance
                    this.Content.Children.Add(_cutoutsModel);
                }

                //create the mesh
                Model3DBuilder mb = new Model3DBuilder();
                foreach (ModelConstructPart3DCutout cutout in cutoutDefinitions)
                {
                    if (cutout.ShapeType == CutoutShapeType.Circle)
                    {
                        mb.AddEllipsoid(cutout.Center.ToPoint3D(), cutout.Width / 2.0, cutout.Height / 2.0, cutout.Depth / 2.0);
                    }
                    else if (cutout.ShapeType == CutoutShapeType.H)
                    {
                        Point3D v1 = new Point3D(cutout.Center.X - (cutout.Width / 2.0) + ((cutout.Width / 3.0) / 2.0), cutout.Center.Y, cutout.Center.Z);
                        Point3D v2 = new Point3D(cutout.Center.X + (cutout.Width / 2.0) - ((cutout.Width / 3.0) / 2.0), cutout.Center.Y, cutout.Center.Z);

                        mb.AddBox(v1, cutout.Width / 3.0, cutout.Height, cutout.Depth);
                        mb.AddBox(cutout.Center.ToPoint3D(), cutout.Width, cutout.Height / 3.0, cutout.Depth);
                        mb.AddBox(v2, cutout.Width / 3.0, cutout.Height, cutout.Depth);
                    }
                    else
                    {
                        mb.AddBox(cutout.Center.ToPoint3D(), cutout.Width, cutout.Height, cutout.Depth);
                    }
                }
                _cutoutsModel.Geometry = mb.ToMesh();

            }
            else
            {
                if (_cutoutsModel != null)
                {
                    this.Content.Children.Remove(_cutoutsModel);
                    _cutoutsModel = null;
                }
            }
        }

        protected override void ClearAllModels()
        {
            base.ClearAllModels();
            _cutoutsModel = null;
        }

        #endregion
    }

}
