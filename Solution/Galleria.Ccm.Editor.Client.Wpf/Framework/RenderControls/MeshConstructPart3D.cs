﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Copied and ammended from GFS.
#endregion
#endregion

using System;
using System.Linq;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;
using Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData;

namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderControls
{
    public class MeshConstructPart3D : ModelConstructPart3D
    {
        #region Fields

        private readonly IMeshPart3DData _partData;
        private readonly GeometryModel3D _mainGeometry;

        #endregion

        #region Properties

        public IMeshPart3DData PartData
        {
            get { return _partData; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="modelPartData">The part data.</param>
        public MeshConstructPart3D(IMeshPart3DData modelPartData)
            : base(modelPartData)
        {
            base.RenderLineAsWireFrame = false;

            _partData = modelPartData;

            //Create the main geometry model.
            _mainGeometry = new GeometryModel3D();
            this.Content.Children.Add(_mainGeometry);


            UpdateModel();
        }

        #endregion

        #region Event Handlers

        protected override void OnModelPartDataPropertyChanged(String propertyName)
        {
            base.OnModelPartDataPropertyChanged(propertyName);

            if (propertyName == "Builder")
            {
                OnGeometryChanged();
            }
        }

        #endregion

        #region Methods

        protected override void Tessellate()
        {
            if (_mainGeometry == null) { return; }

            IMeshPart3DData meshData = this.PartData;

            if (this.IsVisible && meshData.Builder != null)
            {
                ModelConstructMesh meshBuilder = ModelConstructMesh.Build(meshData.Builder);
                MeshGeometry3D mesh = meshBuilder.ToMesh(true);

                _mainGeometry.Geometry = mesh;
            }
            else
            {
                _mainGeometry.Geometry = null;
            }

            TessellateWireframe();
        }

       
        #endregion

    }
}
