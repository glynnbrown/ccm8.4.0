﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;
using Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData;
using HelixToolkit.Wpf;
using System.Diagnostics;


namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderControls
{
    /// <summary>
    /// Helper class for ModelConstruct3D related operations.
    /// </summary>
    public static class ModelConstruct3DHelper
    {
        #region General

        /// <summary>
        /// Returns the ancestor data of the given type or null.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static T FindAncestorData<T>(IModelConstruct3DData data)
            where T : IModelConstruct3DData
        {
            if (data is T)
            {
                return (T)data;
            }

            IModelConstruct3DData curParentData = data.ParentModelData;
            while (curParentData != null)
            {
                if (curParentData is T)
                {
                    return (T)curParentData;
                }
                else
                {
                    curParentData = curParentData.ParentModelData;
                }
            }

            return default(T);
        }

        /// <summary>
        /// Returns all child data of the given type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<T> FindChildrenOfType<T>(IModelConstruct3DData data)
            where T : IModelConstruct3DData
        {
            List<T> returnList = new List<T>();

            if (data is T)
            {
                returnList.Add((T)data);
            }

            foreach (IModelConstruct3DData child in data.ModelChildren)
            {
                returnList.AddRange(FindChildrenOfType<T>(child));
            }

            return returnList;
        }

        #endregion

        #region Extensions

        /// <summary>
        /// Converts the given point to an object point
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static ObjectPoint3D ToObjectPoint3D(this Point3D p)
        {
            return new ObjectPoint3D(Convert.ToSingle(p.X), Convert.ToSingle(p.Y), Convert.ToSingle(p.Z));
        }

        /// <summary>
        /// Converts the given object point to a point 3d
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static Point3D ToPoint3D(this ObjectPoint3D p)
        {
            return new Point3D(p.X, p.Y, p.Z);
        }

        /// <summary>
        /// Converts the given object vector 3d to a vector 3d.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static Vector3D ToVector3D(this ObjectVector3D p)
        {
            return new Vector3D(p.X, p.Y, p.Z);
        }
        /// <summary>
        /// Converts the given object vector 3d to a vector 3d.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static ObjectVector3D ToObjectVector3D(this Vector3D p)
        {
            return new ObjectVector3D(Convert.ToSingle(p.X), Convert.ToSingle(p.Y),Convert.ToSingle(p.Z));
        }

        /// <summary>
        /// Converts the given objectcolour to a color.
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static Color ToColor(this ObjectColour c)
        {
            return
                new Color()
                {
                    A = c.Alpha,
                    R = c.Red,
                    G = c.Green,
                    B = c.Blue
                };
        }

        /// <summary>
        /// Converts the given object colour to a solid colour brush.
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static SolidColorBrush ToSolidBrush(this ObjectColour c)
        {
            SolidColorBrush brush = new SolidColorBrush(c.ToColor());
            brush.Freeze();
            return brush;
        }

        public static Point3D Transform(this Point3D p, IEnumerable<Matrix3D> matricies)
        {
            Matrix3D final = Matrix3D.Identity;

            foreach (Matrix3D matrix in matricies)
            {
                final *= matrix;
            }

            return p.Transform(final);
        }
        public static Point3D Transform(this Point3D p, Matrix3D matrix)
        {
            return new Point3D(
                (p.X * matrix.M11) + (p.Y * matrix.M21) + (p.Z * matrix.M31) + (matrix.OffsetX),
                (p.X * matrix.M12) + (p.Y * matrix.M22) + (p.Z * matrix.M32) + (matrix.OffsetY),
                (p.X * matrix.M13) + (p.Y * matrix.M23) + (p.Z * matrix.M33) + (matrix.OffsetZ)
                );
        }

        /// <summary>
        /// Returns the absolute value of the point
        /// </summary>
        /// <returns></returns>
        public static Point3D Abs(this Point3D p)
        {
            return new Point3D(Math.Abs(p.X), Math.Abs(p.Y), Math.Abs(p.Z));
        }

        public static Point3D Minus(this Point3D v)
        {
            return new Point3D(-v.X, -v.Y, -v.Z);
        }

        public static Vector3D Transform(this Vector3D v, IEnumerable<Matrix3D> matricies)
        {
            Matrix3D final = Matrix3D.Identity;

            foreach (Matrix3D matrix in matricies)
            {
                final *= matrix;
            }

            return v.Transform(final);
        }
        public static Vector3D Transform(this Vector3D v, Matrix3D matrix)
        {
            return new Vector3D(
                (v.X * matrix.M11) + (v.Y * matrix.M21) + (v.Z * matrix.M31) + (matrix.OffsetX),
                (v.X * matrix.M12) + (v.Y * matrix.M22) + (v.Z * matrix.M32) + (matrix.OffsetY),
                (v.X * matrix.M13) + (v.Y * matrix.M23) + (v.Z * matrix.M33) + (matrix.OffsetZ)
                );
        }

        public static Size3D Transform(this Size3D s, IEnumerable<Matrix3D> matricies)
        {
            Matrix3D final = Matrix3D.Identity;

            foreach (Matrix3D matrix in matricies)
            {
                final *= matrix;
            }

            return s.Transform(final);
        }
        public static Size3D Transform(this Size3D s, Matrix3D matrix)
        {
            return new Size3D(
                Math.Abs((s.X * matrix.M11) + (s.Y * matrix.M21) + (s.Z * matrix.M31) + (matrix.OffsetX)),
                Math.Abs((s.X * matrix.M12) + (s.Y * matrix.M22) + (s.Z * matrix.M32) + (matrix.OffsetY)),
                Math.Abs((s.X * matrix.M13) + (s.Y * matrix.M23) + (s.Z * matrix.M33) + (matrix.OffsetZ))
                );
        }

        /// <summary>
        /// Returns the absolute value of the point
        /// </summary>
        /// <returns></returns>
        public static Size3D Abs(this Size3D p)
        {
            return new Size3D(Math.Abs(p.X), Math.Abs(p.Y), Math.Abs(p.Z));
        }

        public static Vector3D FindAnyPerpendicular(this Vector3D p1)
        {
            p1.Normalize();
            Vector3D u = Vector3D.CrossProduct(new Vector3D(0, 1, 0), p1);

            if (u.LengthSquared < 1e-3)
            {
                u = Vector3D.CrossProduct(new Vector3D(1, 0, 0), p1);
            }

            return u;
        }

        #endregion

        #region Space Transforms

        /// <summary>
        /// Creates a new model transform based on the given rotation and position
        /// </summary>
        /// <param name="rotation"></param>
        /// <param name="pos"></param>
        /// <returns></returns>
        public static MatrixTransform3D CreateTransform(ObjectRotation3D rotation, ObjectPoint3D pos)
        {
            Matrix3D matrix = new Matrix3D();
            matrix.Append(CreateYawPitchRoll(rotation.Angle, rotation.Slope, rotation.Roll).Value);
            matrix.Append(new TranslateTransform3D(pos.X, pos.Y, pos.Z).Value);

            return new MatrixTransform3D(matrix);
        }

        ///// <summary>
        ///// Creates a new model transform based on the given rotation, rotation pivot point and position
        ///// </summary>
        ///// <param name="rotation"></param>
        ///// <param name="pos"></param>
        ///// <returns></returns>
        //public static MatrixTransform3D CreateTransform(ObjectRotation3D rotation, Point3D pivot, ObjectPoint3D pos)
        //{
        //    Matrix3D matrix = new Matrix3D();
        //    matrix.Append(CreateYawPitchRoll(rotation.Angle, rotation.Slope, rotation.Roll, pivot).Value);
        //    matrix.Append(new TranslateTransform3D(pos.X, pos.Y, pos.Z).Value);

        //    return new MatrixTransform3D(matrix);
        //}

        /// <summary>
        /// Should create a Matrix Tansform 3D object with the same Rotation matrix
        /// as if the XNA Quaternion.CreateFromYawPitchRoll method was used.
        /// </summary>
        /// <param name="Rotation"></param>
        /// <returns></returns>
        public static MatrixTransform3D CreateYawPitchRoll(ObjectRotation3D rotation)
        {
            return CreateYawPitchRoll(rotation.Angle, rotation.Slope, rotation.Roll);
        }
        public static MatrixTransform3D CreateYawPitchRoll(Double angle, Double slope, Double roll)
        {
            //NB- using minus vector values so that the transform goes clockwise.

            Matrix3D matrix = new Matrix3D();
            Quaternion a = new Quaternion(new Vector3D(0, -1, 0), angle * (360f / (2 * Math.PI)));
            Quaternion b = new Quaternion(new Vector3D(-1, 0, 0), slope * (360f / (2 * Math.PI)));
            Quaternion c = new Quaternion(new Vector3D(0, 0, -1), roll * (360f / (2 * Math.PI)));

            //rotate as z, then x, then y
            matrix.Rotate(c);
            matrix.Rotate(b);
            matrix.Rotate(a);

            return new MatrixTransform3D(matrix);

        }
        //public static MatrixTransform3D CreateYawPitchRoll(ObjectRotation3D rotation, Point3D centerPoint)
        //{
        //    return CreateYawPitchRoll(rotation.Angle, rotation.Slope, rotation.Roll, centerPoint);
        //}
        //private static MatrixTransform3D CreateYawPitchRoll(Double angle, Double slope, Double roll, Point3D centerPoint)
        //{
        //    Matrix3D matrix = new Matrix3D();
        //    Quaternion a = new Quaternion(new Vector3D(0, -1, 0), angle * (360f / (2 * Math.PI)));
        //    Quaternion b = new Quaternion(new Vector3D(-1, 0, 0), slope * (360f / (2 * Math.PI)));
        //    Quaternion c = new Quaternion(new Vector3D(0, 0, -1), roll * (360f / (2 * Math.PI)));

        //    //rotate as z, then x, then y
        //    matrix.RotateAt(c, centerPoint);
        //    matrix.RotateAt(b, centerPoint);
        //    matrix.RotateAt(a, centerPoint);

        //    return new MatrixTransform3D(matrix);

        //}

        /// <summary>
        /// Gets the relative transform for the given plan item.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static Matrix3D GetRelativeTransform(IModelConstruct3DData item)
        {
            Matrix3D relativeTransform = Matrix3D.Identity;


            IModelConstruct3DData processItem = item.ParentModelData;
            while (processItem != null)
            {
                MatrixTransform3D transform = CreateTransform(processItem.Rotation, processItem.Position);
                relativeTransform.Append(transform.Value);

                processItem = processItem.ParentModelData;
            }


            return relativeTransform;
        }

        /// <summary>
        /// Returns the local transform for the given fixture item.
        /// </summary>
        /// <param name="fixtureItem"></param>
        /// <returns></returns>
        public static Matrix3D GetLocalTransform(IModelConstruct3DData item)
        {
            MatrixTransform3D transform = CreateTransform(item.Rotation, item.Position);
            return transform.Value;
        }

        /// <summary>
        /// Returns the given local point relative to world space.
        /// </summary>
        /// <param name="localPoint"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public static Point3D ToWorld(Point3D localPoint, IModelConstruct3DData item)
        {
            Matrix3D relativeTransform = GetRelativeTransform(item);
            return new MatrixTransform3D(relativeTransform).Transform(localPoint);
        }
        public static ObjectPoint3D ToWorld(ObjectPoint3D localPoint, IModelConstruct3DData item)
        {
            Point3D lp = new Point3D(localPoint.X, localPoint.Y, localPoint.Z);
            Point3D wp = ToWorld(lp, item);

            return wp.ToObjectPoint3D();
        }

        /// <summary>
        /// Transforms from local to world coordinates.
        /// </summary>
        /// <param name="point">
        /// The point (local coordinates).
        /// </param>
        /// <returns>
        /// Transformed point (world coordinates).
        /// </returns>
        public static Point3D ToWorld(Point3D point, Visual3D relativeTo)
        {
            var mat = HelixToolkit.Wpf.Visual3DHelper.GetTransform(relativeTo);
            var t = new MatrixTransform3D(mat);
            return t.Transform(point);
        }

        /// <summary>
        /// Returns the given point local to the planitem.
        /// </summary>
        /// <param name="point"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public static Point3D ToLocal(Point3D worldSpacePoint, IModelConstruct3DData item)
        {
            Matrix3D relativeTransform = GetRelativeTransform(item);
            relativeTransform.Invert();
            return new MatrixTransform3D(relativeTransform).Transform(worldSpacePoint);
        }
        public static ObjectPoint3D ToLocal(ObjectPoint3D worldSpacePoint, IModelConstruct3DData item)
        {
            Point3D wp = new Point3D(worldSpacePoint.X, worldSpacePoint.Y, worldSpacePoint.Z);
            Point3D lp = ToLocal(wp, item);

            return lp.ToObjectPoint3D();
        }

        /// <summary>
        /// Returns the local values for the give item based
        /// on the given world rotation.
        /// </summary>
        /// <param name="worldRotation"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public static ObjectRotation3D ToLocal(ObjectRotation3D worldRotation, IModelConstruct3DData item)
        {
            ObjectRotation3D parentWorldRotation = new ObjectRotation3D();

            IModelConstruct3DData itemParent = item.ParentModelData;
            if (itemParent != null)
            {
                parentWorldRotation = GetWorldRotation(itemParent);
            }

            return new ObjectRotation3D()
            {
                Angle = worldRotation.Angle - parentWorldRotation.Angle,
                Slope = worldRotation.Slope - parentWorldRotation.Slope,
                Roll = worldRotation.Roll - parentWorldRotation.Roll
            };

        }

        /// <summary>
        /// Transforms from world to local coordinates.
        /// </summary>
        /// <param name="worldPoint">
        /// The point (world coordinates).
        /// </param>
        /// <returns>
        /// Transformed vector (local coordinates).
        /// </returns>
        public static Point3D ToLocal(Point3D worldPoint, Visual3D relativeTo)
        {
            var mat = HelixToolkit.Wpf.Visual3DHelper.GetTransform(relativeTo);
            mat.Invert();
            var t = new MatrixTransform3D(mat);
            return t.Transform(worldPoint);
        }

        /// <summary>
        /// Returns the position of the given item in world space.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static ObjectPoint3D GetWorldPosition(IModelConstruct3DData item)
        {
            ObjectPoint3D localPos = new ObjectPoint3D(item.Position.X, item.Position.Y, item.Position.Z);
            return ToWorld(localPos, item);
        }

        /// <summary>
        /// Gets the final world space rotation of the item.
        /// Should be used in conjunction with GetWorldPosition
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static ObjectRotation3D GetWorldRotation(IModelConstruct3DData item)
        {
            ObjectRotation3D rotation = new ObjectRotation3D();

            IModelConstruct3DData processItem = item.ParentModelData;
            while (processItem != null)
            {
                rotation.Angle += processItem.Rotation.Angle;
                rotation.Slope += processItem.Rotation.Slope;
                rotation.Roll += processItem.Rotation.Roll;

                processItem = processItem.ParentModelData;
            }


            return rotation;
        }

        /// <summary>
        /// Returns the axis-aligned 3D bounding box for the given item.
        /// </summary>
        /// <param name="sub"></param>
        /// <returns></returns>
        public static Rect3D GetWorldSpaceBounds(IModelConstruct3DData item)
        {
            Rect3D bounds = new Rect3D();

            if (!Double.IsInfinity(item.Size.X) && !Double.IsNaN(item.Size.X))
            {
                bounds.SizeX = item.Size.X;
                bounds.SizeY = item.Size.Y;
                bounds.SizeZ = item.Size.Z;
            }

            Matrix3D fullTransform = Matrix3D.Identity;

            //append the sub transform
            fullTransform.Append(GetLocalTransform(item));

            //append the relative transform.
            fullTransform.Append(GetRelativeTransform(item));

            return new MatrixTransform3D(fullTransform).TransformBounds(bounds);
        }



        #endregion

        #region Hit Testing

        public class ModelConstructHitResult
        {
            public Double Distance { get; set; }
            public Vector3D Normal { get; set; }
            public ModelConstruct3D Model { get; set; }
            public Point3D GlobalPosition { get; set; }
            public Point3D RayPointHit { get; set; }

            public ModelConstructHitResult()
            { }
        }

        /// <summary>
        /// Returns a list of hit model constructs
        /// </summary>
        /// <param name="vp"></param>
        /// <param name="hitPoint"></param>
        /// <returns></returns>
        public static IList<ModelConstructHitResult> FindModelConstructHits(Viewport3D vp, Point hitPoint)
        {
            List<ModelConstructHitResult> result = new List<ModelConstructHitResult>();

            var hits = Viewport3DHelper.FindHits(vp, hitPoint);
            foreach (var h in hits)
            {
                ModelConstruct3D visual = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<ModelConstruct3D>(h.Visual);
                if (visual != null)
                {
                    result.Add(
                        new ModelConstructHitResult()
                        {
                            Distance = h.Distance,
                            Model = visual,
                            Normal = h.Normal,
                            GlobalPosition = h.Position,
                            RayPointHit = h.RayHit.PointHit
                        });

                }
            }

            return result;
        }


        /// <summary>
        /// Returns the nearest hit model to the given hit point.
        /// </summary>
        /// <param name="vp"></param>
        /// <param name="camera"></param>
        /// <param name="hitPoint"></param>
        /// <param name="hitResult"></param>
        /// <returns></returns>
        [CLSCompliant(false)]
        public static ModelConstruct3D GetNearestModelConstruct(Viewport3D vp,
            ProjectionCamera camera, Point hitPoint, out Viewport3DHelper.HitResult hitResult)
        {
            Viewport3DHelper.HitResult result = null;
            Double currCameraDist = Double.PositiveInfinity;
            ModelConstruct3D hitVisual = null;

            var hits = Viewport3DHelper.FindHits(vp, hitPoint);
            foreach (var h in hits)
            {
                ModelConstruct3D visual = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<ModelConstruct3D>(h.Visual);
                if (visual != null)
                {
                    Double cameraDist = ModelConstruct3DHelper.GetCameraDistance(ModelConstruct3DHelper.FindBounds(visual), camera.Position);
                    if (cameraDist < currCameraDist)
                    {
                        hitVisual = visual;
                        currCameraDist = cameraDist;
                        result = h;
                    }
                    break;
                }

            }

            hitResult = result;
            return hitVisual;
        }

        /// <summary>
        /// Converts the given normal to a hit facing enum value.
        /// </summary>
        /// <param name="normal"></param>
        /// <returns></returns>
        public static HitFace NormalToFacing(Vector3D normal)
        {
            //ensure that we start with a vector that is fully normalised
            // there are issues where the hit result is being funky..
            Vector3D n = new Vector3D(normal.X, normal.Y, normal.Z);
            n.Normalize();

            if (n == new Vector3D(0, 0, 1))
            {
                return HitFace.Front;
            }

            if (n == new Vector3D(0, 0, -1))
            {
                return HitFace.Back;
            }

            if (n == new Vector3D(0, 1, 0))
            {
                return HitFace.Top;
            }

            if (n == new Vector3D(0, -1, 0))
            {
                return HitFace.Bottom;
            }

            if (n == new Vector3D(-1, 0, 0))
            {
                return HitFace.Left;
            }

            if (n == new Vector3D(1, 0, 0))
            {
                return HitFace.Right;
            }


            return HitFace.Front;
        }

        public enum HitFace
        {
            Front,
            Back,
            Top,
            Bottom,
            Left,
            Right,
        }

        /// <summary>
        /// Returns a list of intersection points for the given bounds by the given ray.
        /// </summary>
        /// <param name="bounds"></param>
        /// <param name="ray"></param>
        /// <returns></returns>
        [CLSCompliant(false)]
        public static List<Point3D> GetBoxPlaneIntersections(Rect3D bounds, Ray3D ray)
        {
            List<Point3D> intersectionPoints = new List<Point3D>();

            for (Int32 i = 0; i < 6; i++)
            {
                Point3D p = new Point3D();
                Vector3D normal = new Vector3D();

                switch (i)
                {
                    case 0: //front
                        normal = new Vector3D(0, 0, 1);
                        p = new Point3D(bounds.X + bounds.SizeX / 2, bounds.Y + bounds.SizeY / 2, bounds.Z + bounds.SizeZ);
                        break;

                    case 1: //back
                        normal = new Vector3D(0, 0, -1);
                        p = new Point3D(bounds.X + bounds.SizeX / 2, bounds.Y + bounds.SizeY / 2, bounds.Z);
                        break;

                    case 2: //left
                        normal = new Vector3D(-1, 0, 0);
                        p = new Point3D(bounds.X, bounds.Y + bounds.SizeY / 2, bounds.Z + bounds.SizeZ / 2);
                        break;

                    case 3: //right
                        normal = new Vector3D(1, 0, 0);
                        p = new Point3D(bounds.X + bounds.SizeX, bounds.Y + bounds.SizeY / 2, bounds.Z + bounds.SizeZ / 2);
                        break;

                    case 4: //top
                        normal = new Vector3D(0, 1, 0);
                        p = new Point3D(bounds.X + bounds.SizeX / 2, bounds.Y + bounds.SizeY, bounds.Z + bounds.SizeZ / 2);
                        break;

                    case 5: //bottom
                        normal = new Vector3D(0, -1, 0);
                        p = new Point3D(bounds.X + bounds.SizeX / 2, bounds.Y, bounds.Z + bounds.SizeZ / 2);
                        break;

                }


                Point3D? pi = ray.PlaneIntersection(p, normal);
                if (pi.HasValue)
                {
                    intersectionPoints.Add(
                        new Point3D(Math.Round(pi.Value.X), Math.Round(pi.Value.Y), Math.Round(pi.Value.Z)));
                }
            }


            return intersectionPoints;
        }

        /// <summary>
        /// Returns the modelvisual 3d at the given point on the viewport
        /// </summary>
        /// <param name="viewport"></param>
        /// <param name="location"></param>
        /// <returns></returns>
        public static DependencyObject GetHitTestResult(Viewport3D viewport, Point location)
        {
            HitTestResult result = VisualTreeHelper.HitTest(viewport, location);
            if (result != null)
            {
                return result.VisualHit;
            }

            return null;
        }

        /// <summary>
        /// Returns the actual face at the given point.
        /// </summary>
        /// <param name="modelData"></param>
        /// <param name="localPoint"></param>
        /// <returns></returns>
        public static ModelConstruct3DHelper.HitFace? GetFace(IModelConstruct3DData modelData, ObjectPoint3D localPoint)
        {
            Boolean isBetweenX = (localPoint.X >= 0 && localPoint.X <= modelData.Size.X);
            Boolean isBetweenY = (localPoint.Y >= 0 && localPoint.Y <= modelData.Size.Y);
            Boolean isBetweenZ = (localPoint.Z >= 0 && localPoint.Z <= modelData.Size.Z);

            //front
            if (localPoint.Z == modelData.Size.Z && isBetweenX && isBetweenY)
            {
                return ModelConstruct3DHelper.HitFace.Front;
            }


            //back
            else if (localPoint.Z == modelData.Position.Z && isBetweenX && isBetweenY)
            {
                return ModelConstruct3DHelper.HitFace.Back;
            }

            //top
            else if (localPoint.Y == modelData.Size.Y && isBetweenX && isBetweenZ)
            {
                return ModelConstruct3DHelper.HitFace.Top;
            }

            //bottom
            else if (localPoint.Y == 0 && isBetweenX && isBetweenZ)
            {
                return ModelConstruct3DHelper.HitFace.Top;
            }

            //right
            else if (localPoint.X == modelData.Size.X && isBetweenY && isBetweenZ)
            {
                return ModelConstruct3DHelper.HitFace.Right;
            }

            //left
            else if (localPoint.X == 0 && isBetweenY && isBetweenZ)
            {
                return ModelConstruct3DHelper.HitFace.Right;
            }

            return null;
        }

        #endregion

        #region Model Bounds

        /// <summary>
        /// Returns a rectangle that contains the model in screen space.
        /// </summary>
        /// <param name="model3D">the Model3D to get the bounding info from</param>
        /// <param name="includeChildModels">includes any other models attached to the supplied model</param>
        /// <returns>the bounding rectangle, or Rect.Empty if no model is found.</returns>
        public static Rect GetModel2DBounds(Viewport3D viewport, ModelConstruct3D model3D)
        {
            return GetModel2DBounds(viewport, model3D, Rect.Empty, false);
        }
        public static Rect GetModel2DBounds(Viewport3D viewport, ModelConstruct3D model3D, Rect bounds, Boolean thisModelOnly)
        {
            Rect output = bounds;
            if (model3D != null)
            {
                Rect3D bounds3D;
                if (thisModelOnly)
                {
                    bounds3D = GetWorldSpaceBounds(model3D.ModelData);
                }
                else
                {
                    bounds3D = FindBounds(model3D);
                }

                //covert all 8 points to 2d
                Point p1 = Viewport3DHelper.Point3DtoPoint2D(viewport, new Point3D(bounds3D.X, bounds3D.Y, bounds3D.Z));
                if (!Double.IsNaN(p1.X) && !Double.IsNaN(p1.Y))
                {
                    Point p2 = Viewport3DHelper.Point3DtoPoint2D(viewport, new Point3D(bounds3D.X + bounds3D.SizeX, bounds3D.Y, bounds3D.Z));
                    Point p3 = Viewport3DHelper.Point3DtoPoint2D(viewport, new Point3D(bounds3D.X + bounds3D.SizeX, bounds3D.Y, bounds3D.Z + bounds3D.SizeZ));
                    Point p4 = Viewport3DHelper.Point3DtoPoint2D(viewport, new Point3D(bounds3D.X, bounds3D.Y, bounds3D.Z + bounds3D.SizeZ));
                    Point p5 = Viewport3DHelper.Point3DtoPoint2D(viewport, new Point3D(bounds3D.X, bounds3D.Y + bounds3D.SizeY, bounds3D.Z));
                    Point p6 = Viewport3DHelper.Point3DtoPoint2D(viewport, new Point3D(bounds3D.X + bounds3D.SizeX, bounds3D.Y + bounds3D.SizeY, bounds3D.Z));
                    Point p7 = Viewport3DHelper.Point3DtoPoint2D(viewport, new Point3D(bounds3D.X + bounds3D.SizeX, bounds3D.Y + bounds3D.SizeY, bounds3D.Z + bounds3D.SizeZ));
                    Point p8 = Viewport3DHelper.Point3DtoPoint2D(viewport, new Point3D(bounds3D.X, bounds3D.Y + bounds3D.SizeY, bounds3D.Z + bounds3D.SizeZ));

                    List<Point> points = new List<Point> { p1, p2, p3, p4, p5, p6, p7, p8 };


                    Point bottomleft = new Point(points.Min(p => p.X), points.Min(p => p.Y));
                    Point topRight = new Point(points.Max(p => p.X), points.Max(p => p.Y));

                    output = new Rect(bottomleft, topRight);
                }
            }
            return output;
        }

        public static Rect3D FindBounds(ModelConstructPart3D part)
        {
            return part.GetBounds();
        }

        public static Rect3D FindBounds(Visual3D visual)
        {
            return FindBounds(visual, Transform3D.Identity);
        }

        /// <summary>
        /// Finds the bounding box for the specified visual.
        /// </summary>
        /// <param name="visual">
        /// The visual.
        /// </param>
        /// <param name="transform">
        /// The transform of visual.
        /// </param>
        /// <returns>
        /// </returns>
        private static Rect3D FindBounds(Visual3D visual, Transform3D transform)
        {
            var bounds = Rect3D.Empty;
            var childTransform = CombineTransform(visual.Transform, transform);
            var model = GetModel(visual);
            if (model != null)
            {
                // apply transform
                var transformedBounds = childTransform.TransformBounds(model.Bounds);
                if (!double.IsNaN(transformedBounds.X))
                {
                    bounds.Union(transformedBounds);
                }
            }

            foreach (var child in GetChildren(visual))
            {
                var b = FindBounds(child, childTransform);
                bounds.Union(b);
            }

            return bounds;
        }

        /// <summary>
        /// Combines two transforms.
        /// </summary>
        /// <param name="t1">
        /// The first transform.
        /// </param>
        /// <param name="t2">
        /// The second transform.
        /// </param>
        /// <returns>
        /// The combined transform group.
        /// </returns>
        public static Transform3D CombineTransform(Transform3D t1, Transform3D t2)
        {
            var g = new Transform3DGroup();
            g.Children.Add(t1);
            g.Children.Add(t2);
            return g;
        }

        /// <summary>
        /// Gets the model for the specified Visual3D.
        /// </summary>
        /// <param name="visual">
        /// The visual.
        /// </param>
        /// <returns>
        /// </returns>
        private static Model3D GetModel(Visual3D visual)
        {
            Model3D model;
            var mv = visual as ModelVisual3D;
            if (mv != null)
            {
                model = mv.Content;
            }
            else
            {
                model = Visual3DModelPropertyInfo.GetValue(visual, null) as Model3D;
            }

            return model;
        }

        /// <summary>
        /// The visual 3 d model property info.
        /// </summary>
        private static readonly PropertyInfo Visual3DModelPropertyInfo = typeof(Visual3D).GetProperty(
            "Visual3DModel", BindingFlags.Instance | BindingFlags.NonPublic);

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <param name="visual">
        /// The visual.
        /// </param>
        /// <returns>
        /// </returns>
        private static IEnumerable<Visual3D> GetChildren(Visual3D visual)
        {
            int n = VisualTreeHelper.GetChildrenCount(visual);
            for (int i = 0; i < n; i++)
            {
                var child = VisualTreeHelper.GetChild(visual, i) as Visual3D;
                if (child == null)
                {
                    continue;
                }

                yield return child;
            }
        }

        #endregion

        #region Material

        /// <summary>
        /// Creates a new brush based on the given facing materials
        /// </summary>
        /// <param name="materials"></param>
        /// <param name="tileSize"></param>
        /// <param name="borderThick"></param>
        /// <param name="lineColour"></param>
        /// <returns></returns>
        internal static Brush CreateBoxMaterialBrush(
           Dictionary<BoxFaceType, ObjectMaterial3D> materials, ObjectSize3D tileSize,
            Double borderThick, ObjectColour lineColour)
        {
            DrawingVisual dv = new DrawingVisual();
            Pen pen = null;

            Brush borderBrush = lineColour.ToSolidBrush();

            using (DrawingContext dc = dv.RenderOpen())
            {
                //+ Draw sides
                Single frontWidth = tileSize.X;
                Single frontHeight = tileSize.Y;
                Single topWidth = tileSize.X;
                Single topHeight = tileSize.Z;
                Single leftWidth = tileSize.Z;
                Single leftHeight = tileSize.Y;

                //front
                ObjectMaterial3D frontMaterial = materials[BoxFaceType.Front];
                if (frontMaterial.ImageData != null)
                {
                    dc.DrawImage(GetBitmapImage(frontMaterial.ImageData, frontWidth, frontHeight), 
                        new Rect(0, 0, frontWidth, frontHeight));
                }
                else
                {
                    Brush frontBrush = CreateRelativePatternBrush(frontMaterial.FillColour.ToSolidBrush(), 
                        ObjectColour.White.ToSolidBrush(), frontMaterial.FillPattern);

                    dc.DrawRectangle(frontBrush, pen, new Rect(0, 0, frontWidth, frontHeight));
                }

                //left
                ObjectMaterial3D leftMaterial = materials[BoxFaceType.Left];
                if (leftMaterial.ImageData != null)
                {
                    dc.DrawImage(GetBitmapImage(leftMaterial.ImageData, leftWidth, leftHeight), 
                        new Rect((tileSize.X * 2) + tileSize.Z, 0, leftWidth, leftHeight));
                }
                else
                {
                    Brush leftBrush =
                         CreateRelativePatternBrush(leftMaterial.FillColour.ToSolidBrush(),
                        ObjectColour.White.ToSolidBrush(), leftMaterial.FillPattern);

                    dc.DrawRectangle(leftBrush, pen, new Rect((tileSize.X * 2) + tileSize.Z, 0, leftWidth, leftHeight));
                }

                //back
                ObjectMaterial3D backMaterial = materials[BoxFaceType.Back];
                if (backMaterial.ImageData != null)
                {
                    dc.DrawImage(GetBitmapImage(backMaterial.ImageData, frontWidth, frontHeight), 
                        new Rect(tileSize.X + tileSize.Z, 0, frontWidth, frontHeight));
                }
                else
                {
                    Brush backBrush = CreateRelativePatternBrush(backMaterial.FillColour.ToSolidBrush(),
                        ObjectColour.White.ToSolidBrush(), backMaterial.FillPattern);

                    dc.DrawRectangle(backBrush, pen, new Rect(tileSize.X + tileSize.Z, 0, frontWidth, frontHeight));
                }

                //right
                ObjectMaterial3D rightMaterial = materials[BoxFaceType.Right];
                if (rightMaterial.ImageData != null)
                {
                    dc.DrawImage(GetBitmapImage(rightMaterial.ImageData, leftWidth, leftHeight), 
                        new Rect(tileSize.X, 0, leftWidth, leftHeight));
                }
                else
                {
                    Brush rightBrush = CreateRelativePatternBrush(rightMaterial.FillColour.ToSolidBrush(),
                        ObjectColour.White.ToSolidBrush(), rightMaterial.FillPattern);

                    dc.DrawRectangle(rightBrush, pen, new Rect(tileSize.X, 0, leftWidth, leftHeight));
                }

                //top
                
                ObjectMaterial3D topMaterial = materials[BoxFaceType.Top];
                if (topMaterial.ImageData != null)
                {
                    dc.DrawImage(GetBitmapImage(topMaterial.ImageData, topWidth, topHeight), 
                        new Rect(0, tileSize.Y, topWidth, topHeight));
                }
                else
                {
                    Brush topBrush = CreateRelativePatternBrush(topMaterial.FillColour.ToSolidBrush(),
                        ObjectColour.White.ToSolidBrush(), topMaterial.FillPattern);

                    dc.DrawRectangle(topBrush, pen, new Rect(0, tileSize.Y, topWidth, topHeight));
                }

                //bottom
                ObjectMaterial3D bottomMaterial = materials[BoxFaceType.Bottom];
                if (bottomMaterial.ImageData != null)
                {
                    dc.DrawImage(GetBitmapImage(bottomMaterial.ImageData, topWidth, topHeight), 
                        new Rect(tileSize.X, tileSize.Y, topWidth, topHeight));
                }
                else
                {
                    Brush bottomBrush = CreateRelativePatternBrush(bottomMaterial.FillColour.ToSolidBrush(),
                        ObjectColour.White.ToSolidBrush(), bottomMaterial.FillPattern);

                    dc.DrawRectangle(bottomBrush, pen, new Rect(tileSize.X, tileSize.Y, topWidth, topHeight));
                }

                //fill in the last part of the texture with the default colour, this helps prevent transparent lines
                //at some model edges.
                Brush defaultBrush = frontMaterial.FillColour.ToSolidBrush();
                dc.DrawRectangle(defaultBrush, pen, new Rect(tileSize.X * 2, tileSize.Y, tileSize.Z * 2, tileSize.Z));


                if (borderThick > 0)
                {

                    Double lineThick = borderThick, lineThickOver2, lineThickOver4;
                    lineThickOver2 = lineThick / 2;
                    lineThickOver4 = lineThick / 4;
                    Pen border = new Pen(borderBrush, lineThick);
                    Pen borderOver2 = new Pen(borderBrush, lineThickOver2);

                    dc.DrawLine(borderOver2, new Point(0, 0), new Point(tileSize.X * 2 + tileSize.Z * 2, 0));
                    dc.DrawLine(border, new Point(0, tileSize.Y), new Point(tileSize.X * 2 + tileSize.Z * 2, tileSize.Y));
                    dc.DrawLine(borderOver2, new Point(0, tileSize.Y + tileSize.Z), new Point(tileSize.X * 2 + tileSize.Z * 2, tileSize.Y + tileSize.Z));

                    dc.DrawLine(borderOver2, new Point(lineThickOver4, 0), new Point(lineThickOver4, tileSize.Y + tileSize.Z));
                    dc.DrawLine(border, new Point(tileSize.X, 0), new Point(tileSize.X, tileSize.Y + tileSize.Z));
                    dc.DrawLine(border, new Point(tileSize.X + tileSize.Z, 0), new Point(tileSize.X + tileSize.Z, tileSize.Y));
                    dc.DrawLine(borderOver2, new Point(tileSize.X * 2, tileSize.Y), new Point(tileSize.X * 2, tileSize.Y + tileSize.Z));

                    dc.DrawLine(border, new Point(tileSize.X * 2 + tileSize.Z, 0), new Point(tileSize.X * 2 + tileSize.Z, tileSize.Y + tileSize.Z));
                    dc.DrawLine(borderOver2, new Point(tileSize.X * 2 + tileSize.Z * 2 - lineThickOver4, 0), new Point(tileSize.X * 2 + tileSize.Z * 2 - lineThickOver4, tileSize.Y + tileSize.Z));
                }

            }

            DrawingBrush textureBrush = new DrawingBrush(dv.Drawing);
            RenderOptions.SetCachingHint(textureBrush, CachingHint.Cache);
            textureBrush.Freeze();


            Brush returnBrush = textureBrush;
            return returnBrush;
            
            //Boolean attemptImageRender = false;
            //if (attemptImageRender)
            //{

            //    Grid grid = new Grid();
            //    grid.Height = tileSize.Y + tileSize.Z;
            //    grid.Width = tileSize.X * 2 + tileSize.Z * 2;
            //    grid.Background = textureBrush;
            //    grid.Measure(new Size(1000, 1000));
            //    grid.Arrange(new Rect(grid.DesiredSize));

            //    // Scale dimensions from 96 dpi to get better quality
            //    Double scale = 500 / 96;

            //    //perform a straight render.
            //    try
            //    {
            //        RenderTargetBitmap rtb =
            //            new RenderTargetBitmap(
            //                (Int32)(scale * (grid.Width)),
            //                (Int32)(scale * (grid.Height)),
            //            scale * 96,
            //            scale * 96,
            //            PixelFormats.Default);
            //        rtb.Render(grid);
            //        rtb.Freeze();

            //        ImageBrush brush = new ImageBrush(rtb);
            //        brush.Freeze();
            //        returnBrush = brush;
            //    }
            //    catch (Exception)
            //    {
            //        //GC.Collect();
            //    }
            //}

            //return returnBrush;

        }

        internal static BitmapSource GetBitmapImage(Byte[] imageData, Double width, Double height)
        {
            BitmapSource img = null; 
            using (MemoryStream dataStream = new MemoryStream(imageData))
            {
                dataStream.Position = 0;

                //BitmapDecoder decoder = BitmapDecoder.Create(dataStream,
                //    BitmapCreateOptions.IgnoreColorProfile, BitmapCacheOption.OnLoad);
                //var photo = decoder.Frames[0];

                //var target =
                //    new TransformedBitmap(
                //        photo,
                //        new ScaleTransform(
                //            width / photo.Width * 96 / photo.DpiX,
                //            height / photo.Height * 96 / photo.DpiY,
                //            0, 0));

                img = BitmapFrame.Create(dataStream, BitmapCreateOptions.IgnoreColorProfile, BitmapCacheOption.OnLoad);
                img.Freeze();
            }

            return img;
        }
        internal static BitmapImage GetBitmapImage(Byte[] imageData)
        {
            BitmapImage img = new BitmapImage();
            using (MemoryStream dataStream = new MemoryStream(imageData))
            {
                dataStream.Position = 0;
                img.BeginInit();

                img.StreamSource = new MemoryStream(dataStream.ToArray());
                img.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
                img.CacheOption = BitmapCacheOption.Default;

                img.EndInit();
            }
            img.Freeze();

            return img;
        }

       


        internal static Material CreateMaterial(Brush brush)
        {
            return CreateMaterial(brush, 0);
        }
        internal static Material CreateMaterial(Brush brush, Double specularPower)
        {
            Material material = null;

            if (specularPower > 0)
            {
                MaterialGroup mg = new MaterialGroup();
                mg.Children.Add(new DiffuseMaterial(brush));

                if (specularPower > 0)
                {
                    mg.Children.Add(new SpecularMaterial(Brushes.White, specularPower));
                }

                material = mg;
            }
            else
            {
                material = new DiffuseMaterial(brush);
            }

            material.Freeze();
            return material;
        }


        public static Brush CreateRelativePatternBrush(Brush foreground, Brush background, ObjectMaterial3D.FillPatternType pattern)
        {
            Brush returnBrush;

            Double dLen = 10;
            Rect viewportRect = new Rect(0, 0, 0.2, 0.2);

            Pen pen = new Pen(foreground, 1);
            pen.StartLineCap = PenLineCap.Flat;
            pen.EndLineCap = PenLineCap.Flat;

            switch (pattern)
            {
                default:
                    Debug.Fail("Pattern not handled");
                    returnBrush = foreground;
                    break;

                case ObjectMaterial3D.FillPatternType.Solid:
                    returnBrush = foreground;
                    break;

                case ObjectMaterial3D.FillPatternType.Border:
                    {
                        pen.Thickness = 0.4;

                        DrawingVisual dv = new DrawingVisual();
                        using (DrawingContext dc = dv.RenderOpen())
                        {
                            dc.DrawRectangle(background, pen, new Rect(0, 0, dLen, dLen));
                        }

                        DrawingBrush db = new DrawingBrush(dv.Drawing);
                        db.TileMode = TileMode.None;
                        db.Freeze();
                        returnBrush = db;
                    }
                    break;

                case ObjectMaterial3D.FillPatternType.Crosshatch:
                    {
                        pen.Thickness = 0.2;

                        DrawingVisual dv = new DrawingVisual();
                        using (DrawingContext dc = dv.RenderOpen())
                        {
                            dc.DrawRectangle(background, null, new Rect(0, 0, dLen, dLen));
                            dc.DrawLine(pen, new Point(0, 0), new Point(dLen, dLen));
                            dc.DrawLine(pen, new Point(0, dLen), new Point(dLen, 0));

                        }

                        DrawingBrush db = new DrawingBrush(dv.Drawing);
                        db.TileMode = TileMode.Tile;
                        db.Viewport = viewportRect;
                        db.Viewbox = new Rect(0, 0, dLen, dLen);
                        db.ViewboxUnits = BrushMappingMode.Absolute;
                        db.Freeze();
                        returnBrush = db;
                    }
                    break;

                case ObjectMaterial3D.FillPatternType.DiagonalDown:
                    {
                        DrawingVisual dv = new DrawingVisual();
                        using (DrawingContext dc = dv.RenderOpen())
                        {
                            dc.DrawRectangle(background, null, new Rect(0, 0, dLen, dLen));
                            dc.DrawLine(pen, new Point(0, 0), new Point(dLen, dLen));

                            dc.DrawLine(pen, new Point(-1, dLen - 1), new Point(1, dLen + 1));
                            dc.DrawLine(pen, new Point(dLen - 1, -1), new Point(dLen + 1, 1));
                        }

                        DrawingBrush db = new DrawingBrush(dv.Drawing);
                        db.TileMode = TileMode.Tile;
                        db.Viewport = viewportRect;
                        db.Viewbox = new Rect(0, 0, dLen, dLen);
                        db.ViewboxUnits = BrushMappingMode.Absolute;
                        db.Freeze();
                        returnBrush = db;
                    }
                    break;

                case ObjectMaterial3D.FillPatternType.DiagonalUp:
                    {
                        DrawingVisual dv = new DrawingVisual();
                        using (DrawingContext dc = dv.RenderOpen())
                        {
                            dc.DrawRectangle(background, null, new Rect(0, 0, dLen, dLen));
                            dc.DrawLine(pen, new Point(0, dLen), new Point(dLen, 0));

                            dc.DrawLine(pen, new Point(dLen - 1, dLen + 1), new Point(dLen + 1, dLen - 1));
                            dc.DrawLine(pen, new Point(-1, 1), new Point(1, -1));
                        }

                        DrawingBrush db = new DrawingBrush(dv.Drawing);
                        db.TileMode = TileMode.Tile;
                        db.Viewport = viewportRect;
                        db.Viewbox = new Rect(0, 0, dLen, dLen);
                        db.ViewboxUnits = BrushMappingMode.Absolute;
                        db.Freeze();
                        returnBrush = db;
                    }
                    break;

                case ObjectMaterial3D.FillPatternType.Horizontal:
                    {
                        DrawingVisual dv = new DrawingVisual();
                        using (DrawingContext dc = dv.RenderOpen())
                        {
                            dc.DrawRectangle(background, null, new Rect(0, 0, dLen, dLen));
                            dc.DrawLine(pen, new Point(0, dLen / 2), new Point(dLen, dLen / 2));
                        }

                        DrawingBrush db = new DrawingBrush(dv.Drawing);
                        db.TileMode = TileMode.Tile;
                        db.Viewport = viewportRect;
                        db.Viewbox = new Rect(0, 0, dLen, dLen);
                        db.ViewboxUnits = BrushMappingMode.Absolute;
                        db.Freeze();
                        returnBrush = db;
                    }
                    break;

                case ObjectMaterial3D.FillPatternType.Vertical:
                    {
                        DrawingVisual dv = new DrawingVisual();
                        using (DrawingContext dc = dv.RenderOpen())
                        {
                            dc.DrawRectangle(background, null, new Rect(0, 0, dLen, dLen));
                            dc.DrawLine(pen, new Point(dLen / 2, 0), new Point(dLen / 2, dLen));
                        }

                        DrawingBrush db = new DrawingBrush(dv.Drawing);
                        db.TileMode = TileMode.Tile;
                        db.Viewport = viewportRect;
                        db.Viewbox = new Rect(0, 0, dLen, dLen);
                        db.ViewboxUnits = BrushMappingMode.Absolute;
                        db.Freeze();
                        returnBrush = db;
                    }
                    break;

                case ObjectMaterial3D.FillPatternType.Dotted:
                    {
                        DrawingVisual dv = new DrawingVisual();
                        using (DrawingContext dc = dv.RenderOpen())
                        {
                            dc.DrawRectangle(background, null, new Rect(0, 0, dLen, dLen));
                            dc.DrawEllipse(foreground, null, new Point(dLen / 2, dLen / 2), dLen / 4, dLen / 4);
                        }

                        DrawingBrush db = new DrawingBrush(dv.Drawing);
                        db.TileMode = TileMode.Tile;
                        db.Viewport = viewportRect;
                        db.Viewbox = new Rect(0, 0, dLen, dLen);
                        db.ViewboxUnits = BrushMappingMode.Absolute;
                        db.Freeze();
                        returnBrush = db;
                    }
                    break;
            }

            return returnBrush;
        }

        public static Brush CreateAbsolutePatternBrush(Brush foreground, Brush background, ObjectMaterial3D.FillPatternType pattern, Double dLen = 10)
        {
            Brush returnBrush;


            Pen pen = new Pen(foreground, 0.4);
            pen.StartLineCap = PenLineCap.Triangle;
            pen.EndLineCap = PenLineCap.Triangle;

            switch (pattern)
            {
                default:
                    Debug.Fail("Pattern not handled");
                    returnBrush = foreground;
                    break;

                case ObjectMaterial3D.FillPatternType.Solid:
                    returnBrush = foreground;
                    break;

                case ObjectMaterial3D.FillPatternType.Border:
                    {
                        pen.Thickness = 0.2;

                        DrawingVisual dv = new DrawingVisual();
                        using (DrawingContext dc = dv.RenderOpen())
                        {
                            dc.DrawRectangle(background, pen, new Rect(0, 0, dLen, dLen));
                        }

                        DrawingBrush db = new DrawingBrush(dv.Drawing);
                        db.TileMode = TileMode.None;
                        db.Freeze();
                        returnBrush = db;
                    }
                    break;

                case ObjectMaterial3D.FillPatternType.Crosshatch:
                    {
                        pen.Thickness = 0.2;

                        DrawingVisual dv = new DrawingVisual();
                        using (DrawingContext dc = dv.RenderOpen())
                        {
                            dc.DrawRectangle(background, null, new Rect(0, 0, dLen, dLen));
                            dc.DrawLine(pen, new Point(0, 0), new Point(dLen, dLen));
                            dc.DrawLine(pen, new Point(0, dLen), new Point(dLen, 0));

                        }

                        DrawingBrush db = new DrawingBrush(dv.Drawing);
                        db.TileMode = TileMode.Tile;
                        db.Viewport = new Rect(0, 0, dLen, dLen);
                        db.Viewbox = new Rect(0, 0, dLen, dLen);
                        db.ViewboxUnits = BrushMappingMode.Absolute;
                        db.ViewportUnits = BrushMappingMode.Absolute;
                        db.Freeze();
                        returnBrush = db;
                    }
                    break;

                case ObjectMaterial3D.FillPatternType.DiagonalDown:
                    {
                        DrawingVisual dv = new DrawingVisual();
                        using (DrawingContext dc = dv.RenderOpen())
                        {
                            dc.DrawRectangle(background, null, new Rect(0, 0, dLen, dLen));
                            dc.DrawLine(pen, new Point(0, 0), new Point(dLen, dLen));

                            dc.DrawLine(pen, new Point(-1, dLen - 1), new Point(1, dLen + 1));
                            dc.DrawLine(pen, new Point(dLen - 1, -1), new Point(dLen + 1, 1));
                        }

                        DrawingBrush db = new DrawingBrush(dv.Drawing);
                        db.TileMode = TileMode.Tile;
                        db.Viewport = new Rect(0, 0, dLen, dLen);
                        db.Viewbox = new Rect(0, 0, dLen, dLen);
                        db.ViewboxUnits = BrushMappingMode.Absolute;
                        db.ViewportUnits = BrushMappingMode.Absolute;
                        db.Freeze();
                        returnBrush = db;
                    }
                    break;

                case ObjectMaterial3D.FillPatternType.DiagonalUp:
                    {
                        DrawingVisual dv = new DrawingVisual();
                        using (DrawingContext dc = dv.RenderOpen())
                        {
                            dc.DrawRectangle(background, null, new Rect(0, 0, dLen, dLen));
                            dc.DrawLine(pen, new Point(0, dLen), new Point(dLen, 0));

                            dc.DrawLine(pen, new Point(dLen - 1, dLen + 1), new Point(dLen + 1, dLen - 1));
                            dc.DrawLine(pen, new Point(-1, 1), new Point(1, -1));
                        }

                        DrawingBrush db = new DrawingBrush(dv.Drawing);
                        db.TileMode = TileMode.Tile;
                        db.Viewport = new Rect(0, 0, dLen, dLen);
                        db.Viewbox = new Rect(0, 0, dLen, dLen);
                        db.ViewboxUnits = BrushMappingMode.Absolute;
                        db.ViewportUnits = BrushMappingMode.Absolute;
                        db.Freeze();
                        returnBrush = db;
                    }
                    break;

                case ObjectMaterial3D.FillPatternType.Horizontal:
                    {
                        DrawingVisual dv = new DrawingVisual();
                        using (DrawingContext dc = dv.RenderOpen())
                        {
                            dc.DrawRectangle(background, null, new Rect(0, 0, dLen, dLen));
                            dc.DrawLine(pen, new Point(0, dLen / 2), new Point(dLen, dLen / 2));
                        }

                        DrawingBrush db = new DrawingBrush(dv.Drawing);
                        db.TileMode = TileMode.Tile;
                        db.Viewport = new Rect(0, 0, dLen, dLen);
                        db.Viewbox = new Rect(0, 0, dLen, dLen);
                        db.ViewboxUnits = BrushMappingMode.Absolute;
                        db.ViewportUnits = BrushMappingMode.Absolute;
                        db.Freeze();
                        returnBrush = db;
                    }
                    break;

                case ObjectMaterial3D.FillPatternType.Vertical:
                    {
                        DrawingVisual dv = new DrawingVisual();
                        using (DrawingContext dc = dv.RenderOpen())
                        {
                            dc.DrawRectangle(background, null, new Rect(0, 0, dLen, dLen));
                            dc.DrawLine(pen, new Point(dLen / 2, 0), new Point(dLen / 2, dLen));
                        }

                        DrawingBrush db = new DrawingBrush(dv.Drawing);
                        db.TileMode = TileMode.Tile;
                        db.Viewport = new Rect(0, 0, dLen, dLen);
                        db.Viewbox = new Rect(0, 0, dLen, dLen);
                        db.ViewboxUnits = BrushMappingMode.Absolute;
                        db.ViewportUnits = BrushMappingMode.Absolute;
                        db.Freeze();
                        returnBrush = db;
                    }
                    break;

                case ObjectMaterial3D.FillPatternType.Dotted:
                    {
                        DrawingVisual dv = new DrawingVisual();
                        using (DrawingContext dc = dv.RenderOpen())
                        {
                            dc.DrawRectangle(background, null, new Rect(0, 0, dLen, dLen));
                            dc.DrawEllipse(foreground, null, new Point(dLen / 2, dLen / 2), dLen / 4, dLen / 4);
                        }

                        DrawingBrush db = new DrawingBrush(dv.Drawing);
                        db.TileMode = TileMode.Tile;
                        db.Viewport = new Rect(0, 0, dLen, dLen);
                        db.Viewbox = new Rect(0, 0, dLen, dLen);
                        db.ViewboxUnits = BrushMappingMode.Absolute;
                        db.ViewportUnits = BrushMappingMode.Absolute;
                        db.Freeze();
                        returnBrush = db;
                    }
                    break;
            }

            return returnBrush;
        }


        #endregion

        #region Mesh

        /// <summary>
        /// Creates a box mesh of the given product size. If the meshmaterial provided is a texture
        /// for the box then
        /// </summary>
        /// <param name="productSize">Dimensions of the product.</param>
        /// <param name="meshMaterial">Material to apply to the mesh. If this is a texture/image then it
        /// must be created with the format of front, right, back, left images at the top of the texture
        /// and the top and bottom images below for the texture co-ordinates to work correctly as well
        /// as each image being scaled to its relevant product side.</param>
        /// <returns></returns>
        private static MeshGeometry3D CreateBoxMesh(Size3D productSize)
        {
            Double minZ = 0;
            Double maxZ = productSize.Z;
            Point3D p0 = new Point3D(0, 0, minZ); //bll
            Point3D p1 = new Point3D(productSize.X, 0, minZ); // blr
            Point3D p2 = new Point3D(productSize.X, 0, maxZ); //flr
            Point3D p3 = new Point3D(0, 0, maxZ); //fll
            Point3D p4 = new Point3D(0, productSize.Y, minZ); //bul
            Point3D p5 = new Point3D(productSize.X, productSize.Y, minZ);//bur
            Point3D p6 = new Point3D(productSize.X, productSize.Y, maxZ);//fur
            Point3D p7 = new Point3D(0, productSize.Y, maxZ);//ful
            Point3DCollection positions = new Point3DCollection() { 
                        p7, p3, p2, p6, //front (0,1,2,3)
                        p5, p1, p0, p4, //back (4,5,6,7)
                        p4, p0, p3, p7, //left (8,9,10,11)
                        p6, p2, p1, p5, //right (12,13,14,15)
                        p3, p0, p1, p2, //bottom (16,17,18,19)
                        p4, p7, p6, p5 //Top (20,21,22,23)
                };
            Vector3DCollection normals = new Vector3DCollection(){
                        {new Vector3D(0,0,1)}, {new Vector3D(0,0,1)}, {new Vector3D(0,0,1)}, {new Vector3D(0,0,1)},
                        {new Vector3D(0,0,-1)}, {new Vector3D(0,0,-1)}, {new Vector3D(0,0,-1)}, {new Vector3D(0,0,-1)}, 
                        {new Vector3D(-1,0,0)}, {new Vector3D(-1,0,0)}, {new Vector3D(-1,0,0)}, {new Vector3D(-1,0,0)}, 
                        {new Vector3D(1,0,0)}, {new Vector3D(1,0,0)}, {new Vector3D(1,0,0)}, {new Vector3D(1,0,0)}, 
                        {new Vector3D(0,-1,0)}, {new Vector3D(0,-1,0)}, {new Vector3D(0,-1,0)}, {new Vector3D(0,-1,0)},
                        {new Vector3D(0,1,0)}, {new Vector3D(0,1,0)}, {new Vector3D(0,1,0)}, {new Vector3D(0,1,0)}};
            Int32Collection triangleIndices = new Int32Collection(){ 
                        0, 1, 2, 0, 2, 3,
                        4, 5, 6, 4, 6, 7,
                        8, 9, 10, 8, 10, 11,
                        12, 13, 14, 12, 14, 15,
                        16, 17, 18, 16, 18, 19,
                        20, 21, 22, 20, 22, 23};

            return new MeshGeometry3D()
            {
                Positions = positions,
                Normals = normals,
                TriangleIndices = triangleIndices,
                TextureCoordinates = CreateBoxMeshTextureCoords(productSize)
            };
        }


        internal static MeshGeometry3D CreateTiledBoxMesh(Size3D productSize,
                MeshGeometry3D boxMesh, Int32 high, Int32 wide, Int32 deep, Int32 hollowStart,
                Transform3D rotateTransform, Double firstRowReduction, 
                Point3D spacing,
                Boolean isSingleUnitRotation)
        {
            //Create copies of the original mesh componenets
            MeshGeometry3D firstRowMesh = CreateBoxMesh(new Size3D()
            {
                X = productSize.X,
                Y = productSize.Y - firstRowReduction,
                Z = productSize.Z
            });


            Point3DCollection singleTilePositions = new Point3DCollection(boxMesh.Positions);
            Vector3DCollection singleTileNormals = new Vector3DCollection(boxMesh.Normals);
            Int32Collection singleTileTriangleIndices = new Int32Collection(boxMesh.TriangleIndices);
            PointCollection singleTileTextureCoordinates = new PointCollection(boxMesh.TextureCoordinates);

            Int32 posCount = 0;

            Point3DCollection finalPositions = new Point3DCollection(singleTilePositions.Count * high * wide * deep);
            Vector3DCollection finalNormals = new Vector3DCollection(singleTileNormals.Count * high * wide * deep);
            Int32Collection finalTriangleIndices = new Int32Collection(singleTileTriangleIndices.Count * high * wide * deep);
            PointCollection finalTextureCoordinates = new PointCollection(singleTileTextureCoordinates.Count * high * wide * deep);


            Boolean draw = false;
            Point3D rotatedTiles = new Point3D(wide, high, deep);
            Point3D rotatedSpacing = new Point3D(spacing.X, spacing.Y, spacing.Z);
            Point3D minus = new Point3D(1, 1, 1);

            if (isSingleUnitRotation)
            {
                minus = rotateTransform.Inverse.Transform(minus);
                rotatedTiles = rotateTransform.Inverse.Transform(rotatedTiles);

                rotatedSpacing = rotateTransform.Inverse.Transform(rotatedSpacing);
            }

            high = (Int32)Math.Round(Math.Abs(rotatedTiles.Y), 0);
            wide = (Int32)Math.Round(Math.Abs(rotatedTiles.X), 0);
            deep = (Int32)Math.Round(Math.Abs(rotatedTiles.Z), 0);

            //Ensure that at least one mesh will be drawn.
            if (high <= 0) high = 1;
            if (wide <= 0) wide = 1;
            if (deep <= 0) deep = 1;

            for (Int32 x = 0; x < wide; x++)
            {
                for (Int32 y = 0; y < high; y++)
                {
                    for (Int32 z = 0; z < deep; z++)
                    {
                        draw = hollowStart == 0;
                        draw = draw || (x < hollowStart) || (x >= wide - hollowStart);
                        draw = draw || (y < hollowStart) || (y >= high - hollowStart);
                        draw = draw || (z < hollowStart) || (z >= deep - hollowStart);
                        if (draw)
                        {
                            posCount = finalPositions.Count;

                            if (y == 0 && firstRowReduction > 0)
                            {
                                foreach (Point3D p in firstRowMesh.Positions)
                                {

                                    //firstRowReduction
                                    finalPositions.Add(new Point3D(
                                        p.X + (minus.X * productSize.X * x),
                                        p.Y + (minus.Y * firstRowReduction),
                                        p.Z + (minus.Z * productSize.Z * z)));

                                }
                                foreach (Vector3D n in firstRowMesh.Normals)
                                {
                                    finalNormals.Add(n);
                                }
                                foreach (Int32 i in firstRowMesh.TriangleIndices)
                                {
                                    finalTriangleIndices.Add(i + posCount);
                                }
                                foreach (Point p in firstRowMesh.TextureCoordinates)
                                {
                                    finalTextureCoordinates.Add(p);
                                }
                            }
                            else
                            {
                                foreach (Point3D p in singleTilePositions)
                                {
                                    finalPositions.Add(new Point3D(
                                        p.X + (minus.X * productSize.X * x) + (rotatedSpacing.X * x),
                                        p.Y + (minus.Y * productSize.Y * y) + (rotatedSpacing.Y * y),
                                        p.Z + (minus.Z * productSize.Z * z) + (rotatedSpacing.Z * z)));

                                }
                                foreach (Vector3D n in singleTileNormals)
                                {
                                    finalNormals.Add(n);
                                }
                                foreach (Int32 i in singleTileTriangleIndices)
                                {
                                    finalTriangleIndices.Add(i + posCount);
                                }
                                foreach (Point p in singleTileTextureCoordinates)
                                {
                                    finalTextureCoordinates.Add(p);
                                }
                            }
                        }
                    }
                }
            }


            //copy over to the box mesh
            //nb -setting this against the mesh after is much faster than doing it before then adding points to the collection.
            boxMesh.Positions = finalPositions;
            boxMesh.TriangleIndices = finalTriangleIndices;
            boxMesh.Normals = finalNormals;
            boxMesh.TextureCoordinates = finalTextureCoordinates;

            return boxMesh;
        }

        /// <summary>
        /// Texture coordinates used when a mesh is using the CreateBoxMeshTexture
        /// </summary>
        /// <param name="productSize"></param>
        /// <returns></returns>
        private static PointCollection CreateBoxMeshTextureCoords(Size3D productSize)
        {
            return new PointCollection(){
                        #region Front       
                        {new Point(){X = 0, Y = 0}}, 
                        {new Point(){X = 0, Y = productSize.Y}},
                        {new Point(){X = productSize.X, Y = productSize.Y}}, 
                        {new Point(){X = productSize.X, Y = 0}},
                        #endregion           
                        #region Back
                        {new Point(){X = productSize.X + productSize.Z, Y = 0}}, 
                        {new Point(){X = productSize.X + productSize.Z, Y =  productSize.Y}},
                        {new Point(){X = (productSize.X * 2) + productSize.Z, Y = productSize.Y}},
                        {new Point(){X = (productSize.X * 2) + productSize.Z, Y = 0}},
                        #endregion  
                        #region Left   
                        {new Point(){X = (productSize.X * 2) + productSize.Z, Y = 0}}, 
                        {new Point(){X = (productSize.X * 2) + productSize.Z, Y = productSize.Y}},
                        {new Point(){X = (productSize.X + productSize.Z) * 2, Y = productSize.Y}},
                        {new Point(){X = (productSize.X + productSize.Z) * 2, Y = 0}},
                        #endregion 
                        #region Right
                        {new Point(){X = productSize.X, Y = 0}}, 
                        {new Point(){X = productSize.X, Y = productSize.Y}},
                        {new Point(){X = productSize.X + productSize.Z, Y = productSize.Y}}, 
                        {new Point(){X = productSize.X + productSize.Z, Y = 0}},
                        #endregion 
                        #region Bottom  
                        {new Point(){X = productSize.X, Y = productSize.Y}}, 
                        {new Point(){X = productSize.X, Y = productSize.Y + productSize.Z}},
                        {new Point(){X = productSize.X * 2, Y = productSize.Y + productSize.Z}},
                        {new Point(){X = productSize.X * 2, Y = productSize.Y}},
                        #endregion
                        #region Top
                        {new Point(){X = 0, Y = productSize.Y}}, 
                        {new Point(){X = 0, Y = productSize.Y + productSize.Z}},
                        {new Point(){X = productSize.X, Y = productSize.Y + productSize.Z}}, 
                        {new Point(){X = productSize.X, Y = productSize.Y}}};
                        #endregion
        }


        #endregion

        #region Viewport3D

        /// <summary>
        /// Finds the move vector for the given camera and 2d axis change.
        /// </summary>
        /// <param name="camera"></param>
        /// <param name="changeX"></param>
        /// <param name="changeY"></param>
        /// <returns></returns>
        public static Vector3D FindItemMoveVector(ProjectionCamera camera, Double changeX, Double changeY)
        {
            var axis1 = Vector3D.CrossProduct(camera.LookDirection, camera.UpDirection);
            var axis2 = Vector3D.CrossProduct(axis1, camera.LookDirection);
            axis1.Normalize();
            axis2.Normalize();

            //Double l = camera.LookDirection.Length;
            //Double f = l * 0.001;
            var move = (axis1 /** f*/ * changeX) + (axis2 /** f*/ * changeY);

            return move;
        }

      

        /// <summary>
        /// Gets the point 3D on the given viewport.
        /// </summary>
        /// <param name="hitLoc"></param>
        /// <param name="viewport"></param>
        /// <returns></returns>
        public static ObjectPoint3D GetObjectPoint3D(Point hitLoc, Viewport3D viewport)
        {
            return GetPoint3D(hitLoc, viewport).ToObjectPoint3D();
        }

        public static Point3D GetPoint3D(Point hitLoc, Viewport3D viewport)
        {
            Point3D position = new Point3D();

            Point3D? nrPos = Viewport3DHelper.FindNearestPoint(viewport, hitLoc);
            if (!nrPos.HasValue)
            {
                Ray3D posRay = Viewport3DHelper.Point2DtoRay3D(viewport, hitLoc);
                if (posRay != null)
                {
                    Vector3D rayDirection = posRay.Direction;
                    rayDirection.Normalize();

                    position = posRay.GetNearest(new Point3D());
                }
            }
            else { position = nrPos.Value; }

            //floor the y axis to 0.
            //position.Y = Math.Max(0, position.Y);

            //position.Y = Math.Round(position.Y, 0);
            //position.X = Math.Round(position.X, 0);
            //position.Z = Math.Round(position.Z, 0);

            return position;
        }

        /// <summary>
        /// Returns the distance of the given bounds to the camera.
        /// </summary>
        /// <param name="bounds"></param>
        /// <param name="cameraPos"></param>
        /// <returns></returns>
        public static Double GetCameraDistance(Rect3D bounds, Point3D cameraPos)
        {
            double d = double.MaxValue;
            d = Math.Min(d, cameraPos.DistanceTo(new Point3D(bounds.X, bounds.Y, bounds.Z)));
            d = Math.Min(d, cameraPos.DistanceTo(new Point3D(bounds.X + bounds.SizeX, bounds.Y, bounds.Z)));
            d = Math.Min(
                d, cameraPos.DistanceTo(new Point3D(bounds.X + bounds.SizeX, bounds.Y + bounds.SizeY, bounds.Z)));
            d = Math.Min(d, cameraPos.DistanceTo(new Point3D(bounds.X, bounds.Y + bounds.SizeY, bounds.Z)));
            d = Math.Min(d, cameraPos.DistanceTo(new Point3D(bounds.X, bounds.Y, bounds.Z + bounds.SizeZ)));
            d = Math.Min(
                d, cameraPos.DistanceTo(new Point3D(bounds.X + bounds.SizeX, bounds.Y, bounds.Z + bounds.SizeZ)));
            d = Math.Min(
                d,
                cameraPos.DistanceTo(
                    new Point3D(bounds.X + bounds.SizeX, bounds.Y + bounds.SizeY, bounds.Z + bounds.SizeZ)));
            d = Math.Min(
                d, cameraPos.DistanceTo(new Point3D(bounds.X, bounds.Y + bounds.SizeY, bounds.Z + bounds.SizeZ)));
            return d;
        }

        #endregion

    }
}
