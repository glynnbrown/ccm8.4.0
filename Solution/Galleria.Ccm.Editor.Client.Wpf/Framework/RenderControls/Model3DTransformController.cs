﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;
using Galleria.Framework.Model;
using HelixToolkit.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderControls
{
    public sealed class Model3DTransformController : ModelVisual3D
    {
        #region Fields

        private Color _xColour;
        private Color _yColour;
        private Color _zColour;

        private Model3DSizeControllerArrow _xySizeArrow;
        private Model3DSizeControllerArrow _xzSizeArrow;

        private Model3DSizeControllerArrow _yxSizeArrow;
        private Model3DSizeControllerArrow _yzSizeArrow;

        private Model3DSizeControllerArrow _zxSizeArrow;
        private Model3DSizeControllerArrow _zySizeArrow;

        private Model3DPositionControllerArrow _xArrow;
        private Model3DPositionControllerArrow _yArrow;
        private Model3DPositionControllerArrow _zArrow;

        private Model3DScaleControllerBall _xyScaleBall;
        private Model3DScaleControllerBall _xzScaleBall;
        private Model3DScaleControllerBall _yzScaleBall;

        #endregion

        #region Properties

        #region Center Property

        public static readonly DependencyProperty CenterProperty =
            DependencyProperty.Register("Center", typeof(Point3D), typeof(Model3DTransformController),
            new PropertyMetadata(new Point3D(), OnCenterPropertyChanged));

        private static void OnCenterPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
        }

        /// <summary>
        /// Gets/Sets the center position of the manipulator.
        /// This will also be used as the pivot point.
        /// </summary>
        public Point3D Center
        {
            get { return (Point3D)GetValue(CenterProperty); }
            set { SetValue(CenterProperty, value); }
        }

        #endregion

        #region Diameter Property

        public static readonly DependencyProperty DiameterProperty =
            DependencyProperty.Register("Diameter", typeof(Double), typeof(Model3DTransformController),
            new UIPropertyMetadata(1D, OnDiameterPropertyChanged));

        private static void OnDiameterPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
        }

        public Double Diameter
        {
            get { return (Double)GetValue(DiameterProperty); }
            set { SetValue(DiameterProperty, value); }
        }

        #endregion

        #region Length Property

        public static readonly DependencyProperty LengthProperty =
            DependencyProperty.Register("Length", typeof(Double), typeof(Model3DTransformController),
            new UIPropertyMetadata(20D, OnLengthPropertyChanged));

        private static void OnLengthPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Double Length
        {
            get { return (Double)GetValue(LengthProperty); }
            set { SetValue(LengthProperty, value); }
        }

        #endregion

        #region Visiblity Property

        public static readonly DependencyProperty VisibilityProperty =
            DependencyProperty.Register("Visibility", typeof(Visibility), typeof(Model3DTransformController),
            new UIPropertyMetadata(Visibility.Visible, OnVisibilityPropertyChanged));

        private static void OnVisibilityPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Model3DTransformController)obj).UpdateControllers();
        }

        public Visibility Visibility
        {
            get { return (Visibility)GetValue(VisibilityProperty); }
            set { SetValue(VisibilityProperty, value); }
        }

        #endregion

        #region CanTransformX Property

        public static readonly DependencyProperty CanTransformXProperty =
            DependencyProperty.Register("CanTransformX", typeof(Boolean), typeof(Model3DTransformController),
            new PropertyMetadata(true, OnCanTransformXPropertyChanged));

        private static void OnCanTransformXPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Model3DTransformController)obj).UpdateControllers();
        }

        public Boolean CanTransformX
        {
            get { return (Boolean)GetValue(CanTransformXProperty); }
            set { SetValue(CanTransformXProperty, value); }
        }

        #endregion

        #region CanTransformY Property

        public static readonly DependencyProperty CanTransformYProperty =
            DependencyProperty.Register("CanTransformY", typeof(Boolean), typeof(Model3DTransformController),
            new PropertyMetadata(true, OnCanTransformYPropertyChanged));

        private static void OnCanTransformYPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Model3DTransformController)obj).UpdateControllers();
        }

        public Boolean CanTransformY
        {
            get { return (Boolean)GetValue(CanTransformYProperty); }
            set { SetValue(CanTransformYProperty, value); }
        }

        #endregion

        #region CanTransformZ Property

        public static readonly DependencyProperty CanTransformZProperty =
            DependencyProperty.Register("CanTransformZ", typeof(Boolean), typeof(Model3DTransformController),
            new PropertyMetadata(true, OnCanTransformZPropertyChanged));

        private static void OnCanTransformZPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Model3DTransformController)obj).UpdateControllers();
        }

        public Boolean CanTransformZ
        {
            get { return (Boolean)GetValue(CanTransformZProperty); }
            set { SetValue(CanTransformZProperty, value); }
        }

        #endregion

        #endregion

        #region Events

        public event EventHandler PositionValueChangeBeginning;
        private void OnPositionValueChangeBeginning()
        {
            if (PositionValueChangeBeginning != null)
            {
                PositionValueChangeBeginning(this, EventArgs.Empty);
            }
        }

        public event EventHandler<EventArgs<Vector3D>> PositionValueChanged;
        private void OnPositionValueChanged(Vector3D changeVector)
        {
            if (PositionValueChanged != null)
            {
                PositionValueChanged(this, new EventArgs<Vector3D>(changeVector));
            }
        }

        public event EventHandler PositionValueChangeComplete;
        private void OnPositionValueChangeComplete()
        {
            if (PositionValueChangeComplete != null)
            {
                PositionValueChangeComplete(this, EventArgs.Empty);
            }
        }

        public event EventHandler SizeValueChangeBeginning;
        private void OnSizeValueChangeBeginning()
        {
            if (SizeValueChangeBeginning != null)
            {
                SizeValueChangeBeginning(this, EventArgs.Empty);
            }
        }

        public event EventHandler<EventArgs<Vector3D>> SizeValueChanged;
        private void OnSizeValueChanged(Vector3D changeVector)
        {
            if (SizeValueChanged != null)
            {
                SizeValueChanged(this, new EventArgs<Vector3D>(changeVector));
            }
        }

        public event EventHandler SizeValueChangeComplete;
        private void OnSizeValueChangeComplete()
        {
            if (SizeValueChangeComplete != null)
            {
                SizeValueChangeComplete(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Constructor

        public Model3DTransformController()
        {
            _xColour = Color.FromRgb(150, 75, 75);
            _yColour = Color.FromRgb(75, 150, 75);
            _zColour = Color.FromRgb(75, 75, 150);

            UpdateControllers();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Reloads the transform controllers
        /// </summary>
        private void UpdateControllers()
        {
            Boolean isThisVisible = this.Visibility == System.Windows.Visibility.Visible;

            #region Transform X
            if (isThisVisible && this.CanTransformX)
            {
                Color xColour = _xColour;

                //position x
                if (_xArrow == null)
                {
                    _xArrow = new Model3DPositionControllerArrow()
                    {
                        Colour = xColour,
                        Direction = new Vector3D(1, 0, 0),
                    };
                    BindingOperations.SetBinding(_xArrow, Model3DPositionControllerArrow.LengthProperty, new Binding(LengthProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_xArrow, Model3DPositionControllerArrow.DiameterProperty, new Binding(DiameterProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_xArrow, Model3DPositionControllerArrow.PositionProperty, new Binding(CenterProperty.Name) { Source = this });
                    //BindingOperations.SetBinding(_xArrow, Model3DPositionControllerArrow.VisibilityProperty, new Binding(VisibilityProperty.Name) { Source = this });
                    this.Children.Add(_xArrow);

                    _xArrow.ValueChangeBeginning += Arrow_ValueChangeBeginning;
                    _xArrow.ValueChanged += Arrow_ValueChanged;
                    _xArrow.ValueChangeComplete += Arrow_ValueChangeComplete;
                }

                //size x - y perp
                if (_xySizeArrow == null)
                {
                    _xySizeArrow = new Model3DSizeControllerArrow()
                    {
                        Colour = xColour,
                        Direction = new Vector3D(1, 0, 0),
                        Axis = new Vector3D(0, 1, 0)
                    };
                    BindingOperations.SetBinding(_xySizeArrow, Model3DSizeControllerArrow.LengthProperty, new Binding(LengthProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_xySizeArrow, Model3DSizeControllerArrow.DiameterProperty, new Binding(DiameterProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_xySizeArrow, Model3DSizeControllerArrow.PositionProperty, new Binding(CenterProperty.Name) { Source = this });
                    //BindingOperations.SetBinding(_xySizeArrow, Model3DSizeControllerArrow.VisibilityProperty, new Binding(VisibilityProperty.Name) { Source = this });
                    this.Children.Add(_xySizeArrow);

                    _xySizeArrow.ValueChangeBeginning += SizeArrow_ValueChangeBeginning;
                    _xySizeArrow.ValueChanged += SizeArrow_ValueChanged;
                    _xySizeArrow.ValueChangeComplete += SizeArrow_ValueChangeComplete;
                }

                //size x - z perp
                if (_xzSizeArrow == null)
                {
                    _xzSizeArrow = new Model3DSizeControllerArrow()
                    {
                        Colour = xColour,
                        Direction = new Vector3D(1, 0, 0),
                        Axis = new Vector3D(0, 0, -1)
                    };
                    BindingOperations.SetBinding(_xzSizeArrow, Model3DSizeControllerArrow.LengthProperty, new Binding(LengthProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_xzSizeArrow, Model3DSizeControllerArrow.DiameterProperty, new Binding(DiameterProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_xzSizeArrow, Model3DSizeControllerArrow.PositionProperty, new Binding(CenterProperty.Name) { Source = this });
                    //BindingOperations.SetBinding(_xzSizeArrow, Model3DSizeControllerArrow.VisibilityProperty, new Binding(VisibilityProperty.Name) { Source = this });
                    this.Children.Add(_xzSizeArrow);

                    _xzSizeArrow.ValueChangeBeginning += SizeArrow_ValueChangeBeginning;
                    _xzSizeArrow.ValueChanged += SizeArrow_ValueChanged;
                    _xzSizeArrow.ValueChangeComplete += SizeArrow_ValueChangeComplete;
                }

            }
            else
            {
                if (_xArrow != null)
                {
                    _xArrow.ValueChangeBeginning -= Arrow_ValueChangeBeginning;
                    _xArrow.ValueChanged -= Arrow_ValueChanged;
                    _xArrow.ValueChangeComplete -= Arrow_ValueChangeComplete;

                    this.Children.Remove(_xArrow);
                    _xArrow = null;
                }

                if (_xySizeArrow != null)
                {
                    _xySizeArrow.ValueChangeBeginning -= SizeArrow_ValueChangeBeginning;
                    _xySizeArrow.ValueChanged -= SizeArrow_ValueChanged;
                    _xySizeArrow.ValueChangeComplete -= SizeArrow_ValueChangeComplete;

                    this.Children.Remove(_xySizeArrow);
                    _xySizeArrow = null;
                }

                if (_xzSizeArrow != null)
                {
                    _xzSizeArrow.ValueChangeBeginning -= SizeArrow_ValueChangeBeginning;
                    _xzSizeArrow.ValueChanged -= SizeArrow_ValueChanged;
                    _xzSizeArrow.ValueChangeComplete -= SizeArrow_ValueChangeComplete;

                    this.Children.Remove(_xzSizeArrow);
                    _xzSizeArrow = null;
                }

            }

            #endregion

            #region Transform Y
            if (isThisVisible && this.CanTransformY)
            {
                Color yColour = _yColour;

                //position y
                if (_yArrow == null)
                {
                    _yArrow = new Model3DPositionControllerArrow()
                    {
                        Colour = yColour,
                        Direction = new Vector3D(0, 1, 0),
                    };
                    BindingOperations.SetBinding(_yArrow, Model3DPositionControllerArrow.LengthProperty, new Binding(LengthProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_yArrow, Model3DPositionControllerArrow.DiameterProperty, new Binding(DiameterProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_yArrow, Model3DPositionControllerArrow.PositionProperty, new Binding(CenterProperty.Name) { Source = this });
                    //BindingOperations.SetBinding(_yArrow, Model3DPositionControllerArrow.VisibilityProperty, new Binding(VisibilityProperty.Name) { Source = this });
                    this.Children.Add(_yArrow);

                    _yArrow.ValueChangeBeginning += Arrow_ValueChangeBeginning;
                    _yArrow.ValueChanged += Arrow_ValueChanged;
                    _yArrow.ValueChangeComplete += Arrow_ValueChangeComplete;
                }

                //size y - x perp
                if (_yxSizeArrow == null)
                {
                    _yxSizeArrow = new Model3DSizeControllerArrow()
                    {
                        Colour = yColour,
                        Direction = new Vector3D(0, 1, 0),
                        Axis = new Vector3D(1, 0, 0)
                    };
                    BindingOperations.SetBinding(_yxSizeArrow, Model3DSizeControllerArrow.LengthProperty, new Binding(LengthProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_yxSizeArrow, Model3DSizeControllerArrow.DiameterProperty, new Binding(DiameterProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_yxSizeArrow, Model3DSizeControllerArrow.PositionProperty, new Binding(CenterProperty.Name) { Source = this });
                    //BindingOperations.SetBinding(_yxSizeArrow, Model3DSizeControllerArrow.VisibilityProperty, new Binding(VisibilityProperty.Name) { Source = this });
                    this.Children.Add(_yxSizeArrow);

                    _yxSizeArrow.ValueChangeBeginning += SizeArrow_ValueChangeBeginning;
                    _yxSizeArrow.ValueChanged += SizeArrow_ValueChanged;
                    _yxSizeArrow.ValueChangeComplete += SizeArrow_ValueChangeComplete;
                }

                //size y - z perp
                if (_yzSizeArrow == null)
                {
                    _yzSizeArrow = new Model3DSizeControllerArrow()
                    {
                        Colour = yColour,
                        Direction = new Vector3D(0, 1, 0),
                        Axis = new Vector3D(0, 0, -1)
                    };
                    BindingOperations.SetBinding(_yzSizeArrow, Model3DSizeControllerArrow.LengthProperty, new Binding(LengthProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_yzSizeArrow, Model3DSizeControllerArrow.DiameterProperty, new Binding(DiameterProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_yzSizeArrow, Model3DSizeControllerArrow.PositionProperty, new Binding(CenterProperty.Name) { Source = this });
                    //BindingOperations.SetBinding(_yzSizeArrow, Model3DSizeControllerArrow.VisibilityProperty, new Binding(VisibilityProperty.Name) { Source = this });
                    this.Children.Add(_yzSizeArrow);

                    _yzSizeArrow.ValueChangeBeginning += SizeArrow_ValueChangeBeginning;
                    _yzSizeArrow.ValueChanged += SizeArrow_ValueChanged;
                    _yzSizeArrow.ValueChangeComplete += SizeArrow_ValueChangeComplete;
                }

            }
            else
            {
                if (_yArrow != null)
                {
                    _yArrow.ValueChangeBeginning -= Arrow_ValueChangeBeginning;
                    _yArrow.ValueChanged -= Arrow_ValueChanged;
                    _yArrow.ValueChangeComplete -= Arrow_ValueChangeComplete;

                    this.Children.Remove(_yArrow);
                    _yArrow = null;
                }

                if (_yxSizeArrow != null)
                {
                    _yxSizeArrow.ValueChangeBeginning -= SizeArrow_ValueChangeBeginning;
                    _yxSizeArrow.ValueChanged -= SizeArrow_ValueChanged;
                    _yxSizeArrow.ValueChangeComplete -= SizeArrow_ValueChangeComplete;

                    this.Children.Remove(_yxSizeArrow);
                    _yxSizeArrow = null;
                }

                if (_yzSizeArrow != null)
                {
                    _yzSizeArrow.ValueChangeBeginning -= SizeArrow_ValueChangeBeginning;
                    _yzSizeArrow.ValueChanged -= SizeArrow_ValueChanged;
                    _yzSizeArrow.ValueChangeComplete -= SizeArrow_ValueChangeComplete;

                    this.Children.Remove(_yzSizeArrow);
                    _yzSizeArrow = null;
                }

            }
            #endregion

            #region Transform Z
            if (isThisVisible && this.CanTransformZ)
            {
                Color zColour = _zColour;

                //position z
                if (_zArrow == null && this.CanTransformZ)
                {
                    _zArrow = new Model3DPositionControllerArrow()
                    {
                        Colour = zColour,
                        Direction = new Vector3D(0, 0, 1),
                    };
                    BindingOperations.SetBinding(_zArrow, Model3DPositionControllerArrow.LengthProperty, new Binding(LengthProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_zArrow, Model3DPositionControllerArrow.DiameterProperty, new Binding(DiameterProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_zArrow, Model3DPositionControllerArrow.PositionProperty, new Binding(CenterProperty.Name) { Source = this });
                    //BindingOperations.SetBinding(_zArrow, Model3DPositionControllerArrow.VisibilityProperty, new Binding(VisibilityProperty.Name) { Source = this });
                    this.Children.Add(_zArrow);

                    _zArrow.ValueChangeBeginning += Arrow_ValueChangeBeginning;
                    _zArrow.ValueChanged += Arrow_ValueChanged;
                    _zArrow.ValueChangeComplete += Arrow_ValueChangeComplete;
                }

                //size Z - x perp
                if (_zxSizeArrow == null)
                {
                    _zxSizeArrow = new Model3DSizeControllerArrow()
                    {
                        Colour = _zColour,
                        Direction = new Vector3D(0, 0, 1),
                        Axis = new Vector3D(1, 0, 0)
                    };
                    BindingOperations.SetBinding(_zxSizeArrow, Model3DSizeControllerArrow.LengthProperty, new Binding(LengthProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_zxSizeArrow, Model3DSizeControllerArrow.DiameterProperty, new Binding(DiameterProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_zxSizeArrow, Model3DSizeControllerArrow.PositionProperty, new Binding(CenterProperty.Name) { Source = this });
                    //BindingOperations.SetBinding(_zxSizeArrow, Model3DSizeControllerArrow.VisibilityProperty, new Binding(VisibilityProperty.Name) { Source = this });
                    this.Children.Add(_zxSizeArrow);

                    _zxSizeArrow.ValueChangeBeginning += SizeArrow_ValueChangeBeginning;
                    _zxSizeArrow.ValueChanged += SizeArrow_ValueChanged;
                    _zxSizeArrow.ValueChangeComplete += SizeArrow_ValueChangeComplete;
                }

                //size Z - Y perp
                if (_zySizeArrow == null)
                {
                    _zySizeArrow = new Model3DSizeControllerArrow()
                    {
                        Colour = _zColour,
                        Direction = new Vector3D(0, 0, 1),
                        Axis = new Vector3D(0, 1, 0)
                    };
                    BindingOperations.SetBinding(_zySizeArrow, Model3DSizeControllerArrow.LengthProperty, new Binding(LengthProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_zySizeArrow, Model3DSizeControllerArrow.DiameterProperty, new Binding(DiameterProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_zySizeArrow, Model3DSizeControllerArrow.PositionProperty, new Binding(CenterProperty.Name) { Source = this });
                    //BindingOperations.SetBinding(_zySizeArrow, Model3DSizeControllerArrow.VisibilityProperty, new Binding(VisibilityProperty.Name) { Source = this });
                    this.Children.Add(_zySizeArrow);

                    _zySizeArrow.ValueChangeBeginning += SizeArrow_ValueChangeBeginning;
                    _zySizeArrow.ValueChanged += SizeArrow_ValueChanged;
                    _zySizeArrow.ValueChangeComplete += SizeArrow_ValueChangeComplete;
                }

            }
            else
            {
                if (_zArrow != null)
                {
                    _zArrow.ValueChangeBeginning -= Arrow_ValueChangeBeginning;
                    _zArrow.ValueChanged -= Arrow_ValueChanged;
                    _zArrow.ValueChangeComplete -= Arrow_ValueChangeComplete;

                    this.Children.Remove(_zArrow);
                    _zArrow = null;
                }

                if (_zxSizeArrow != null)
                {
                    _zxSizeArrow.ValueChangeBeginning -= SizeArrow_ValueChangeBeginning;
                    _zxSizeArrow.ValueChanged -= SizeArrow_ValueChanged;
                    _zxSizeArrow.ValueChangeComplete -= SizeArrow_ValueChangeComplete;

                    this.Children.Remove(_zxSizeArrow);
                    _zxSizeArrow = null;
                }

                if (_zySizeArrow != null)
                {
                    _zySizeArrow.ValueChangeBeginning -= SizeArrow_ValueChangeBeginning;
                    _zySizeArrow.ValueChanged -= SizeArrow_ValueChanged;
                    _zySizeArrow.ValueChangeComplete -= SizeArrow_ValueChangeComplete;

                    this.Children.Remove(_zySizeArrow);
                    _zySizeArrow = null;
                }
            }
            #endregion

            Color scaleColour = Colors.DarkGray;

            #region Scale XY

            if (isThisVisible && this.CanTransformX && this.CanTransformY)
            {
                //scale xy
                if (_xyScaleBall == null)
                {
                    _xyScaleBall = new Model3DScaleControllerBall()
                    {
                        Colour = scaleColour,
                        Direction = new Vector3D(1, 1, 0)
                    };
                    BindingOperations.SetBinding(_xyScaleBall, Model3DSizeControllerArrow.LengthProperty, new Binding(LengthProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_xyScaleBall, Model3DSizeControllerArrow.DiameterProperty, new Binding(DiameterProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_xyScaleBall, Model3DSizeControllerArrow.PositionProperty, new Binding(CenterProperty.Name) { Source = this });
                    //BindingOperations.SetBinding(_xyScaleBall, Model3DSizeControllerArrow.VisibilityProperty, new Binding(VisibilityProperty.Name) { Source = this });
                    this.Children.Add(_xyScaleBall);

                    _xyScaleBall.ValueChangeBeginning += SizeArrow_ValueChangeBeginning;
                    _xyScaleBall.ValueChanged += SizeArrow_ValueChanged;
                    _xyScaleBall.ValueChangeComplete += SizeArrow_ValueChangeComplete;

                    _xyScaleBall.MouseEnter += ScaleBall_MouseEnter;
                    _xyScaleBall.MouseLeave += ScaleBall_MouseLeave;
                }

            }
            else
            {
                if (_xyScaleBall != null)
                {
                    _xyScaleBall.ValueChangeBeginning -= SizeArrow_ValueChangeBeginning;
                    _xyScaleBall.ValueChanged -= SizeArrow_ValueChanged;
                    _xyScaleBall.ValueChangeComplete -= SizeArrow_ValueChangeComplete;

                    _xyScaleBall.MouseEnter -= ScaleBall_MouseEnter;
                    _xyScaleBall.MouseLeave -= ScaleBall_MouseLeave;
                    this.Children.Remove(_xyScaleBall);
                    _xyScaleBall = null;
                }
            }

            #endregion

            #region Scale XZ
            if (isThisVisible && this.CanTransformX && this.CanTransformZ)
            {
                //scale xz
                if (_xzScaleBall == null)
                {
                    _xzScaleBall = new Model3DScaleControllerBall()
                    {
                        Colour = scaleColour,
                        Direction = new Vector3D(1, 0, 1)
                    };

                    BindingOperations.SetBinding(_xzScaleBall, Model3DSizeControllerArrow.LengthProperty, new Binding(LengthProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_xzScaleBall, Model3DSizeControllerArrow.DiameterProperty, new Binding(DiameterProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_xzScaleBall, Model3DSizeControllerArrow.PositionProperty, new Binding(CenterProperty.Name) { Source = this });
                    //BindingOperations.SetBinding(_xzScaleBall, Model3DSizeControllerArrow.VisibilityProperty, new Binding(VisibilityProperty.Name) { Source = this });
                    this.Children.Add(_xzScaleBall);

                    _xzScaleBall.ValueChangeBeginning += SizeArrow_ValueChangeBeginning;
                    _xzScaleBall.ValueChanged += SizeArrow_ValueChanged;
                    _xzScaleBall.ValueChangeComplete += SizeArrow_ValueChangeComplete;

                    _xzScaleBall.MouseEnter += ScaleBall_MouseEnter;
                    _xzScaleBall.MouseLeave += ScaleBall_MouseLeave;
                }
            }
            else
            {
                if (_xzScaleBall != null)
                {
                    _xzScaleBall.ValueChangeBeginning -= SizeArrow_ValueChangeBeginning;
                    _xzScaleBall.ValueChanged -= SizeArrow_ValueChanged;
                    _xzScaleBall.ValueChangeComplete -= SizeArrow_ValueChangeComplete;

                    _xzScaleBall.MouseEnter -= ScaleBall_MouseEnter;
                    _xzScaleBall.MouseLeave -= ScaleBall_MouseLeave;
                    this.Children.Remove(_xzScaleBall);
                    _xzScaleBall = null;
                }
            }
            #endregion

            #region Scale YZ
            if (isThisVisible && this.CanTransformY && this.CanTransformZ)
            {
                //yz
                if (_yzScaleBall == null)
                {
                    _yzScaleBall = new Model3DScaleControllerBall()
                    {
                        Colour = scaleColour,
                        Direction = new Vector3D(0, 1, 1)
                    };

                    BindingOperations.SetBinding(_yzScaleBall, Model3DSizeControllerArrow.LengthProperty, new Binding(LengthProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_yzScaleBall, Model3DSizeControllerArrow.DiameterProperty, new Binding(DiameterProperty.Name) { Source = this });
                    BindingOperations.SetBinding(_yzScaleBall, Model3DSizeControllerArrow.PositionProperty, new Binding(CenterProperty.Name) { Source = this });
                    //BindingOperations.SetBinding(_yzScaleBall, Model3DSizeControllerArrow.VisibilityProperty, new Binding(VisibilityProperty.Name) { Source = this });
                    this.Children.Add(_yzScaleBall);

                    _yzScaleBall.ValueChangeBeginning += SizeArrow_ValueChangeBeginning;
                    _yzScaleBall.ValueChanged += SizeArrow_ValueChanged;
                    _yzScaleBall.ValueChangeComplete += SizeArrow_ValueChangeComplete;

                    _yzScaleBall.MouseEnter += ScaleBall_MouseEnter;
                    _yzScaleBall.MouseLeave += ScaleBall_MouseLeave;
                }
            }
            else
            {
                if (_yzScaleBall != null)
                {
                    _yzScaleBall.ValueChangeBeginning -= SizeArrow_ValueChangeBeginning;
                    _yzScaleBall.ValueChanged -= SizeArrow_ValueChanged;
                    _yzScaleBall.ValueChangeComplete -= SizeArrow_ValueChangeComplete;

                    _yzScaleBall.MouseEnter -= ScaleBall_MouseEnter;
                    _yzScaleBall.MouseLeave -= ScaleBall_MouseLeave;
                    this.Children.Remove(_yzScaleBall);
                    _yzScaleBall = null;
                }
            }
            #endregion


            if (isThisVisible)
            {
                foreach (var child in this.Children)
                {
                    if (child is UIElement3D)
                    {
                        ((UIElement3D)child).Visibility = System.Windows.Visibility.Visible;
                    }
                }
            }
        }

        #endregion

        #region Event Handlers

        private void SizeArrow_ValueChangeBeginning(object sender, EventArgs e)
        {
            OnSizeValueChangeBeginning();
        }
        private void SizeArrow_ValueChanged(object sender, EventArgs<Vector3D> e)
        {
            //relay out
            OnSizeValueChanged(e.ReturnValue);
        }
        private void SizeArrow_ValueChangeComplete(object sender, EventArgs e)
        {
            OnSizeValueChangeComplete();
        }

        private void Arrow_ValueChangeBeginning(object sender, EventArgs e)
        {
            OnPositionValueChangeBeginning();
        }
        private void Arrow_ValueChanged(object sender, EventArgs<Vector3D> e)
        {
            //relay out
            OnPositionValueChanged(e.ReturnValue);
        }
        private void Arrow_ValueChangeComplete(object sender, EventArgs e)
        {
            OnPositionValueChangeComplete();
        }

        private void ScaleBall_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Model3DScaleControllerBall senderControl = (Model3DScaleControllerBall)sender;

            Color highlightColor = Colors.Yellow;

            //set the colour of the related size arrows to highlight.
            Vector3D direction = senderControl.Direction;

            if (direction.X != 0 && direction.Y != 0)
            {
                if (_xySizeArrow != null) _xySizeArrow.Colour = highlightColor;
                if (_yxSizeArrow != null) _yxSizeArrow.Colour = highlightColor;
            }
            if (direction.X != 0 && direction.Z != 0)
            {
                if (_xzSizeArrow != null) _xzSizeArrow.Colour = highlightColor;
                if (_zxSizeArrow != null) _zxSizeArrow.Colour = highlightColor;
            }
            if (direction.Y != 0 && direction.Z != 0)
            {
                if (_yzSizeArrow != null) _yzSizeArrow.Colour = highlightColor;
                if (_zySizeArrow != null) _zySizeArrow.Colour = highlightColor;
            }
        }
        private void ScaleBall_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            //reset all size arrow colours
            if (_xySizeArrow != null) _xySizeArrow.Colour = _xColour;
            if (_xzSizeArrow != null) _xzSizeArrow.Colour = _xColour;

            if (_yxSizeArrow != null) _yxSizeArrow.Colour = _yColour;
            if (_yzSizeArrow != null) _yzSizeArrow.Colour = _yColour;

            if (_zxSizeArrow != null) _zxSizeArrow.Colour = _zColour;
            if (_zySizeArrow != null) _zySizeArrow.Colour = _zColour;
        }

        #endregion

    }

    public class Model3DSizeControllerArrow : UIElement3D
    {
        #region Fields
        private GeometryModel3D _model;
        private Viewport3D _parentViewport;
        private ProjectionCamera _camera;
        private Vector3D _hitPlaneNormal;
        private Point3D _lastPoint;
        #endregion

        #region Properties

        #region Colour Property

        public static readonly DependencyProperty ColourProperty =
            DependencyProperty.Register("Colour", typeof(Color), typeof(Model3DSizeControllerArrow),
            new PropertyMetadata(Colors.Red, OnColourPropertyChanged));

        private static void OnColourPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Model3DSizeControllerArrow)obj).OnColourChanged();
        }

        /// <summary>
        /// Gets/Sets the colour of the arrow.
        /// </summary>
        public Color Colour
        {
            get { return (Color)GetValue(ColourProperty); }
            set { SetValue(ColourProperty, value); }
        }

        private void OnColourChanged()
        {
            Color color =
                (this.IsMouseOver) ? Colors.Yellow : this.Colour;

            SolidColorBrush brush = new SolidColorBrush(color);
            brush.Freeze();

            _model.Material = ModelConstruct3DHelper.CreateMaterial(brush);
            _model.BackMaterial = _model.Material;
        }

        #endregion

        #region Position Property

        public static readonly DependencyProperty PositionProperty =
            DependencyProperty.Register("Position", typeof(Point3D), typeof(Model3DSizeControllerArrow),
            new PropertyMetadata(new Point3D(), OnPositionPropertyChanged));

        private static void OnPositionPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Model3DSizeControllerArrow)obj).OnPositionChanged();
        }

        /// <summary>
        /// Gets/Sets the position of the arrow.
        /// This should be the center point of the model it is attached to.
        /// </summary>
        public Point3D Position
        {
            get { return (Point3D)GetValue(PositionProperty); }
            set { SetValue(PositionProperty, value); }
        }

        #endregion

        #region Offset Property

        public static readonly DependencyProperty OffsetProperty =
            DependencyProperty.Register("Offset", typeof(Vector3D), typeof(Model3DSizeControllerArrow),
            new PropertyMetadata(new Vector3D(0, 0, 0), OnOffsetPropertyChanged));

        private static void OnOffsetPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Model3DSizeControllerArrow)obj).OnPositionChanged();
        }

        /// <summary>
        /// Gets/Sets the offset of the arrow from the center point.
        /// </summary>
        public Vector3D Offset
        {
            get { return (Vector3D)GetValue(OffsetProperty); }
            set { SetValue(OffsetProperty, value); }
        }

        #endregion

        #region Direction Property

        public static readonly DependencyProperty DirectionProperty =
            DependencyProperty.Register("Direction", typeof(Vector3D), typeof(Model3DSizeControllerArrow),
            new UIPropertyMetadata(OnDirectionPropertyChanged));

        private static void OnDirectionPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Model3DSizeControllerArrow)obj).OnGeometryChanged();
        }

        /// <summary>
        /// Gets/Sets the direction the arrow will move the model in.
        /// </summary>
        public Vector3D Direction
        {
            get { return (Vector3D)GetValue(DirectionProperty); }
            set { SetValue(DirectionProperty, value); }
        }

        #endregion

        #region Diameter Property

        public static readonly DependencyProperty DiameterProperty =
            DependencyProperty.Register("Diameter", typeof(Double), typeof(Model3DSizeControllerArrow),
            new UIPropertyMetadata(1.5D, OnDiameterPropertyChanged));

        private static void OnDiameterPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Model3DSizeControllerArrow)obj).OnGeometryChanged();
        }

        public Double Diameter
        {
            get { return (Double)GetValue(DiameterProperty); }
            set { SetValue(DiameterProperty, value); }
        }

        #endregion

        #region Length Property

        public static readonly DependencyProperty LengthProperty =
            DependencyProperty.Register("Length", typeof(Double), typeof(Model3DSizeControllerArrow),
            new UIPropertyMetadata(10D, OnLengthPropertyChanged));

        private static void OnLengthPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Model3DSizeControllerArrow)obj).OnGeometryChanged();
        }

        /// <summary>
        /// 
        /// </summary>
        public Double Length
        {
            get { return (Double)GetValue(LengthProperty); }
            set { SetValue(LengthProperty, value); }
        }

        #endregion

        #region TargetSize Property

        public static readonly DependencyProperty TargetSizeProperty =
            DependencyProperty.Register("TargetSize", typeof(ObjectSize3D), typeof(Model3DSizeControllerArrow),
            new FrameworkPropertyMetadata(new ObjectSize3D(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnValuePropertyChanged));

        private static void OnValuePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {

        }

        /// <summary>
        /// Gets/Sets the value to be manipulated.
        /// </summary>
        public ObjectSize3D TargetSize
        {
            get { return (ObjectSize3D)GetValue(TargetSizeProperty); }
            set { SetValue(TargetSizeProperty, value); }
        }

        #endregion

        #region Axis Property

        public static readonly DependencyProperty AxisProperty =
            DependencyProperty.Register("Axis", typeof(Vector3D), typeof(Model3DSizeControllerArrow),
            new PropertyMetadata(new Vector3D(1, 0, 0), OnAxisPropertyChanged));

        private static void OnAxisPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Model3DSizeControllerArrow)obj).OnGeometryChanged();
        }

        public Vector3D Axis
        {
            get { return (Vector3D)GetValue(AxisProperty); }
            set { SetValue(AxisProperty, value); }
        }

        #endregion

        protected GeometryModel3D Model
        {
            get { return _model; }
        }

        #endregion

        #region Events

        public event EventHandler ValueChangeBeginning;
        private void OnValueChangeBeginning()
        {
            if (ValueChangeBeginning != null)
            {
                ValueChangeBeginning(this, EventArgs.Empty);
            }
        }

        public event EventHandler<EventArgs<Vector3D>> ValueChanged;
        private void OnValueChanged(Vector3D changeVector)
        {
            if (ValueChanged != null)
            {
                ValueChanged(this, new EventArgs<Vector3D>(changeVector));
            }
        }

        public event EventHandler ValueChangeComplete;
        private void OnValueChangeComplete()
        {
            if (ValueChangeComplete != null)
            {
                ValueChangeComplete(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Constructor

        public Model3DSizeControllerArrow()
        {
            //There is a wierd bug in the base ms code which causes IsVisible to be set to false.
            // This then stops all mouse events and captures.
            //To fix this the controller must start collapsed and then be made visible after it has
            // been added to the parent collection
            this.Visibility = System.Windows.Visibility.Collapsed;

            _model = new GeometryModel3D();
            this.Visual3DModel = _model;

            OnColourChanged();
        }



        #endregion

        #region Event Handlers

        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            OnColourChanged();
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            OnColourChanged();
        }

        /// <summary>
        /// The on mouse down.
        /// </summary>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);

            OnValueChangeBeginning();

            _parentViewport = Visual3DHelper.GetViewport3D(this);
            _camera = _parentViewport.Camera as ProjectionCamera;

            var projectionCamera = _camera;
            if (projectionCamera != null)
            {
                _hitPlaneNormal = projectionCamera.LookDirection;
            }

            this.CaptureMouse();

            var direction = this.ToWorld(this.Direction);

            var up = Vector3D.CrossProduct(_camera.LookDirection, direction);
            var hitPlaneOrigin = this.ToWorld(this.Position);
            _hitPlaneNormal = Vector3D.CrossProduct(up, direction);
            var p = e.GetPosition(_parentViewport);

            var np = this.GetNearestPoint(p, hitPlaneOrigin, _hitPlaneNormal);
            if (np == null)
            {
                return;
            }

            var lp = this.ToLocal(np.Value);

            _lastPoint = lp;

            this.CaptureMouse();

        }

        /// <summary>
        /// The on mouse move.
        /// </summary>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (this.IsMouseCaptured)
            {
                var hitPlaneOrigin = ToWorld(this.Position);
                var p = e.GetPosition(_parentViewport);
                var nearestPoint = GetNearestPoint(p, hitPlaneOrigin, _hitPlaneNormal);
                if (nearestPoint == null)
                {
                    return;
                }

                var delta = ToLocal(nearestPoint.Value) - _lastPoint;
                Double changeVal = Vector3D.DotProduct(delta, this.Direction);
                Vector3D changeVector = Vector3D.Multiply(Math.Round(changeVal, 2), this.Direction);

                //set
                //ObjectSize3D targetSize = this.TargetSize;
                //this.TargetSize =
                //    new ObjectSize3D()
                //    {
                //        X = targetSize.X += changeVector.X,
                //        Y = targetSize.Y += changeVector.Y,
                //        Z = targetSize.Z += changeVector.Z
                //    };

                OnValueChanged(changeVector);


                //take a reference to the point.
                nearestPoint = this.GetNearestPoint(p, hitPlaneOrigin, _hitPlaneNormal);
                if (nearestPoint != null)
                {
                    _lastPoint = ToLocal(nearestPoint.Value);
                }
            }
        }

        /// <summary>
        /// The on mouse up.
        /// </summary>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            this.ReleaseMouseCapture();

            base.OnMouseUp(e);

            this.ReleaseMouseCapture();
            OnValueChangeComplete();
        }

        protected virtual void OnGeometryChanged()
        {
            Double lineDiam = this.Diameter;
            Int32 thetaDiv = 10;
            Double lineLength = (this.Length / 2);

            var mb = new MeshBuilder(false, false);

            var p0 = new Point3D(0, 0, 0);
            var d = this.Direction;
            d.Normalize();

            //var p1 = p0 + (d * this.Length);
            //mb.AddArrow(p0, p1, this.Diameter);

            //mb.AddCylinder(p0 + (d * (lineLength/2)), p0 + (d * lineLength), lineDiam, thetaDiv);

            var axis = this.Axis;
            Point3D centerPoint = p0 + (d * lineLength);

            //if (d.X == 0)
            if (axis.X != 0)
            {
                Point3D p2 = centerPoint + (new Vector3D(1, 0, 0) * lineLength);
                mb.AddCylinder(centerPoint, p2, lineDiam, thetaDiv);
            }

            //if (d.Y == 0)
            if (axis.Y != 0)
            {
                Point3D p2 = centerPoint + (new Vector3D(0, 1, 0) * lineLength);
                mb.AddCylinder(centerPoint, p2, lineDiam, thetaDiv);
            }

            //if (d.Z == 0)
            if (axis.Z != 0)
            {
                Point3D p2 = centerPoint + (new Vector3D(0, 0, 1) * lineLength);
                mb.AddCylinder(centerPoint, p2, lineDiam, thetaDiv);
            }


            _model.Geometry = mb.ToMesh();
        }

        private void OnPositionChanged()
        {
            try
            {
                this.Transform = new TranslateTransform3D(
                    this.Position.X + this.Offset.X,
                    this.Position.Y + this.Offset.Y,
                    this.Position.Z + this.Offset.Z);
            }
            catch (Exception) { }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Transforms from world to local coordinates.
        /// </summary>
        /// <param name="worldPoint">
        /// The point (world coordinates).
        /// </param>
        /// <returns>
        /// Transformed vector (local coordinates).
        /// </returns>
        private Point3D ToLocal(Point3D worldPoint)
        {
            var mat = Visual3DHelper.GetTransform(this);
            mat.Invert();
            var t = new MatrixTransform3D(mat);
            return t.Transform(worldPoint);
        }

        /// <summary>
        /// Transforms from local to world coordinates.
        /// </summary>
        /// <param name="point">
        /// The point (local coordinates).
        /// </param>
        /// <returns>
        /// Transformed point (world coordinates).
        /// </returns>
        private Point3D ToWorld(Point3D point)
        {
            var mat = Visual3DHelper.GetTransform(this);
            var t = new MatrixTransform3D(mat);
            return t.Transform(point);
        }

        /// <summary>
        /// Transforms from local to world coordinates.
        /// </summary>
        /// <param name="vector">
        /// The vector (local coordinates).
        /// </param>
        /// <returns>
        /// Transformed vector (world coordinates).
        /// </returns>
        private Vector3D ToWorld(Vector3D vector)
        {
            var mat = Visual3DHelper.GetTransform(this);
            var t = new MatrixTransform3D(mat);
            return t.Transform(vector);
        }

        /// <summary>
        /// Gets the nearest point on the translation axis.
        /// </summary>
        /// <param name="position">
        /// The position (in screen coordinates).
        /// </param>
        /// <param name="hitPlaneOrigin">
        /// The hit plane origin (world coordinate system).
        /// </param>
        /// <param name="hitPlaneNormal">
        /// The hit plane normal (world coordinate system).
        /// </param>
        /// <returns>
        /// The nearest point (world coordinates) or null if no point could be found.
        /// </returns>
        private Point3D? GetNearestPoint(Point position, Point3D hitPlaneOrigin, Vector3D hitPlaneNormal)
        {
            var hpp = GetHitPlanePoint(position, hitPlaneOrigin, hitPlaneNormal);
            if (hpp == null)
            {
                return null;
            }

            var ray = new Ray3D(this.ToWorld(this.Position), this.ToWorld(this.Direction));
            return ray.GetNearest(hpp.Value);
        }

        /// <summary>
        /// Projects the point on the hit plane.
        /// </summary>
        /// <param name="p">
        /// The p.
        /// </param>
        /// <param name="hitPlaneOrigin">
        /// The hit Plane Origin.
        /// </param>
        /// <param name="hitPlaneNormal">
        /// The hit plane normal (world coordinate system).
        /// </param>
        /// <returns>
        /// The point in world coordinates.
        /// </returns>
        private Point3D? GetHitPlanePoint(Point p, Point3D hitPlaneOrigin, Vector3D hitPlaneNormal)
        {
            return Viewport3DHelper.UnProject(_parentViewport, p, hitPlaneOrigin, hitPlaneNormal);
        }

        #endregion
    }

    public sealed class Model3DScaleControllerBall : Model3DSizeControllerArrow
    {
        protected override void OnGeometryChanged()
        {
            MeshBuilder mb = new MeshBuilder(false, false);

            Vector3D direction = this.Direction;
            Double position = this.Length / 2;

            Point3D p0 = new Point3D(0, 0, 0);

            Point3D p1 =
                new Point3D()
                {
                    X = direction.X * position,
                    Y = direction.Y * position,
                    Z = direction.Z * position,
                };

            mb.AddSphere(p1, this.Diameter / 2);

            this.Model.Geometry = mb.ToMesh();
        }
    }

    public class Model3DPositionControllerArrow : UIElement3D
    {
        #region Fields
        private readonly GeometryModel3D _model;
        private Viewport3D _parentViewport;
        private ProjectionCamera _camera;
        private Vector3D _hitPlaneNormal;
        private Point3D _lastPoint;
        #endregion

        #region Properties

        #region Colour Property

        public static readonly DependencyProperty ColourProperty =
            DependencyProperty.Register("Colour", typeof(Color), typeof(Model3DPositionControllerArrow),
            new PropertyMetadata(Colors.Red, OnColourPropertyChanged));

        private static void OnColourPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Model3DPositionControllerArrow)obj).OnColourChanged();
        }

        /// <summary>
        /// Gets/Sets the colour of the arrow.
        /// </summary>
        public Color Colour
        {
            get { return (Color)GetValue(ColourProperty); }
            set { SetValue(ColourProperty, value); }
        }

        private void OnColourChanged()
        {
            Color color =
                (this.IsMouseOver) ? Colors.Yellow : this.Colour;

            SolidColorBrush brush = new SolidColorBrush(color);
            brush.Freeze();

            _model.Material = ModelConstruct3DHelper.CreateMaterial(brush);
            _model.BackMaterial = _model.Material;
        }

        #endregion

        #region Position Property

        public static readonly DependencyProperty PositionProperty =
            DependencyProperty.Register("Position", typeof(Point3D), typeof(Model3DPositionControllerArrow),
            new PropertyMetadata(new Point3D(), OnPositionPropertyChanged));

        private static void OnPositionPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Model3DPositionControllerArrow)obj).OnPositionChanged();
        }

        /// <summary>
        /// Gets/Sets the position of the arrow.
        /// This should be the center point of the model it is attached to.
        /// </summary>
        public Point3D Position
        {
            get { return (Point3D)GetValue(PositionProperty); }
            set { SetValue(PositionProperty, value); }
        }

        #endregion

        #region Offset Property

        public static readonly DependencyProperty OffsetProperty =
            DependencyProperty.Register("Offset", typeof(Point3D), typeof(Model3DPositionControllerArrow),
            new PropertyMetadata(new Point3D(0, 0, 0), OnOffsetPropertyChanged));

        private static void OnOffsetPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Model3DPositionControllerArrow)obj).OnPositionChanged();
        }

        /// <summary>
        /// Gets/Sets the offset of the arrow from the center point.
        /// </summary>
        public Point3D Offset
        {
            get { return (Point3D)GetValue(OffsetProperty); }
            set { SetValue(OffsetProperty, value); }
        }

        #endregion

        #region Direction Property

        public static readonly DependencyProperty DirectionProperty =
            DependencyProperty.Register("Direction", typeof(Vector3D), typeof(Model3DPositionControllerArrow),
            new UIPropertyMetadata(OnDirectionPropertyChanged));

        private static void OnDirectionPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Model3DPositionControllerArrow)obj).OnGeometryChanged();
        }

        /// <summary>
        /// Gets/Sets the direction the arrow will move the model in.
        /// </summary>
        public Vector3D Direction
        {
            get { return (Vector3D)GetValue(DirectionProperty); }
            set { SetValue(DirectionProperty, value); }
        }

        #endregion

        #region Diameter Property

        public static readonly DependencyProperty DiameterProperty =
            DependencyProperty.Register("Diameter", typeof(Double), typeof(Model3DPositionControllerArrow),
            new UIPropertyMetadata(1.5D, OnDiameterPropertyChanged));

        private static void OnDiameterPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Model3DPositionControllerArrow)obj).OnGeometryChanged();
        }

        public Double Diameter
        {
            get { return (Double)GetValue(DiameterProperty); }
            set { SetValue(DiameterProperty, value); }
        }

        #endregion

        #region Length Property

        public static readonly DependencyProperty LengthProperty =
            DependencyProperty.Register("Length", typeof(Double), typeof(Model3DPositionControllerArrow),
            new UIPropertyMetadata(10D, OnLengthPropertyChanged));

        private static void OnLengthPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Model3DPositionControllerArrow)obj).OnGeometryChanged();
        }

        /// <summary>
        /// 
        /// </summary>
        public Double Length
        {
            get { return (Double)GetValue(LengthProperty); }
            set { SetValue(LengthProperty, value); }
        }

        #endregion

        #region TargetPosition Property

        //public static readonly DependencyProperty TargetPositionProperty =
        //    DependencyProperty.Register("TargetPosition", typeof(ObjectPoint3D), typeof(Model3DPositionControllerArrow),
        //    new FrameworkPropertyMetadata(new ObjectPoint3D(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnTargetPositionPropertyChanged));

        //private static void OnTargetPositionPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{

        //}

        ///// <summary>
        ///// Gets/Sets the target position to be manipulated
        ///// </summary>
        //public ObjectPoint3D TargetPosition
        //{
        //    get { return (ObjectPoint3D)GetValue(TargetPositionProperty); }
        //    set { SetValue(TargetPositionProperty, value); }
        //}

        #endregion

        protected GeometryModel3D Model
        {
            get { return _model; }
        }

        #endregion

        #region Events

        public event EventHandler ValueChangeBeginning;
        private void OnValueChangeBeginning()
        {
            if (ValueChangeBeginning != null)
            {
                ValueChangeBeginning(this, EventArgs.Empty);
            }
        }

        public event EventHandler<EventArgs<Vector3D>> ValueChanged;
        private void OnValueChanged(Vector3D changeVector)
        {
            if (ValueChanged != null)
            {
                ValueChanged(this, new EventArgs<Vector3D>(changeVector));
            }
        }

        public event EventHandler ValueChangeComplete;
        private void OnValueChangeComplete()
        {
            if (ValueChangeComplete != null)
            {
                ValueChangeComplete(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Constructor

        public Model3DPositionControllerArrow()
        {
            //There is a wierd bug in the base ms code which causes IsVisible to be set to false.
            // This then stops all mouse events and captures.
            //To fix this the controller must start collapsed and then be made visible after it has
            // been added to the parent collection
            this.Visibility = System.Windows.Visibility.Collapsed;

            _model = new GeometryModel3D();
            this.Visual3DModel = _model;

            OnColourChanged();
        }

        #endregion

        #region Event Handlers

        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            OnColourChanged();
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            OnColourChanged();
        }

        /// <summary>
        /// The on mouse down.
        /// </summary>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);

            _parentViewport = Visual3DHelper.GetViewport3D(this);
            if (_parentViewport == null) { return; }

            _camera = _parentViewport.Camera as ProjectionCamera;
            if (_camera == null) { return; }

            OnValueChangeBeginning();

            _hitPlaneNormal = _camera.LookDirection;


            this.CaptureMouse();

            var direction = this.ToWorld(this.Direction);

            var up = Vector3D.CrossProduct(_camera.LookDirection, direction);
            var hitPlaneOrigin = this.ToWorld(this.Position);
            _hitPlaneNormal = Vector3D.CrossProduct(up, direction);
            var p = e.GetPosition(_parentViewport);

            var np = this.GetNearestPoint(p, hitPlaneOrigin, _hitPlaneNormal);
            if (np == null)
            {
                return;
            }

            var lp = this.ToLocal(np.Value);

            _lastPoint = lp;

            this.CaptureMouse();

        }

        /// <summary>
        /// The on mouse move.
        /// </summary>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (this.IsMouseCaptured)
            {
                var hitPlaneOrigin = ToWorld(this.Position);
                var p = e.GetPosition(_parentViewport);
                var nearestPoint = GetNearestPoint(p, hitPlaneOrigin, _hitPlaneNormal);
                if (nearestPoint == null)
                {
                    return;
                }

                //var delta = nearestPoint.Value - _lastPoint;
                var delta = ToLocal(nearestPoint.Value) - _lastPoint;
                //this.Value += Vector3D.DotProduct(delta, this.Direction);

                Double changeVal = Vector3D.DotProduct(delta, this.Direction);
                Vector3D changeVector = Vector3D.Multiply(Math.Round(changeVal, 2), this.Direction);

                //ObjectPoint3D targetPosition = this.TargetPosition;
                //this.TargetPosition =
                //    new ObjectPoint3D()
                //    {
                //        X = targetPosition.X + delta.X,
                //        Y = targetPosition.Y + delta.Y,
                //        Z = targetPosition.Z + delta.Z
                //    };

                OnValueChanged(changeVector);


                nearestPoint = this.GetNearestPoint(p, hitPlaneOrigin, _hitPlaneNormal);
                if (nearestPoint != null)
                {
                    _lastPoint = ToLocal(nearestPoint.Value);
                }
            }
        }

        /// <summary>
        /// The on mouse up.
        /// </summary>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            this.ReleaseMouseCapture();

            base.OnMouseUp(e);

            this.ReleaseMouseCapture();
            OnValueChangeComplete();
        }

        protected virtual void OnGeometryChanged()
        {
            var mb = new MeshBuilder(false, false);
            var p0 = new Point3D(0, 0, 0);
            var d = this.Direction;
            d.Normalize();
            var p1 = p0 + (d * this.Length);
            mb.AddArrow(p0, p1, this.Diameter);
            _model.Geometry = mb.ToMesh();
        }

        private void OnPositionChanged()
        {
            this.Transform = new TranslateTransform3D(
                this.Position.X + this.Offset.X,
                this.Position.Y + this.Offset.Y,
                this.Position.Z + this.Offset.Z);

        }

        #endregion

        #region Methods

        public void UpdateModel()
        {
            OnColourChanged();
            OnGeometryChanged();
            OnPositionChanged();
        }

        /// <summary>
        /// Transforms from world to local coordinates.
        /// </summary>
        /// <param name="worldPoint">
        /// The point (world coordinates).
        /// </param>
        /// <returns>
        /// Transformed vector (local coordinates).
        /// </returns>
        private Point3D ToLocal(Point3D worldPoint)
        {
            var mat = Visual3DHelper.GetTransform(this);
            mat.Invert();
            var t = new MatrixTransform3D(mat);
            return t.Transform(worldPoint);
        }

        /// <summary>
        /// Transforms from local to world coordinates.
        /// </summary>
        /// <param name="point">
        /// The point (local coordinates).
        /// </param>
        /// <returns>
        /// Transformed point (world coordinates).
        /// </returns>
        private Point3D ToWorld(Point3D point)
        {
            var mat = Visual3DHelper.GetTransform(this);
            var t = new MatrixTransform3D(mat);
            return t.Transform(point);
        }

        /// <summary>
        /// Transforms from local to world coordinates.
        /// </summary>
        /// <param name="vector">
        /// The vector (local coordinates).
        /// </param>
        /// <returns>
        /// Transformed vector (world coordinates).
        /// </returns>
        private Vector3D ToWorld(Vector3D vector)
        {
            var mat = Visual3DHelper.GetTransform(this);
            var t = new MatrixTransform3D(mat);
            return t.Transform(vector);
        }

        /// <summary>
        /// Gets the nearest point on the translation axis.
        /// </summary>
        /// <param name="position">
        /// The position (in screen coordinates).
        /// </param>
        /// <param name="hitPlaneOrigin">
        /// The hit plane origin (world coordinate system).
        /// </param>
        /// <param name="hitPlaneNormal">
        /// The hit plane normal (world coordinate system).
        /// </param>
        /// <returns>
        /// The nearest point (world coordinates) or null if no point could be found.
        /// </returns>
        private Point3D? GetNearestPoint(Point position, Point3D hitPlaneOrigin, Vector3D hitPlaneNormal)
        {
            var hpp = GetHitPlanePoint(position, hitPlaneOrigin, hitPlaneNormal);
            if (hpp == null)
            {
                return null;
            }

            var ray = new Ray3D(this.ToWorld(this.Position), this.ToWorld(this.Direction));
            return ray.GetNearest(hpp.Value);
        }

        /// <summary>
        /// Projects the point on the hit plane.
        /// </summary>
        /// <param name="p">
        /// The p.
        /// </param>
        /// <param name="hitPlaneOrigin">
        /// The hit Plane Origin.
        /// </param>
        /// <param name="hitPlaneNormal">
        /// The hit plane normal (world coordinate system).
        /// </param>
        /// <returns>
        /// The point in world coordinates.
        /// </returns>
        private Point3D? GetHitPlanePoint(Point p, Point3D hitPlaneOrigin, Vector3D hitPlaneNormal)
        {
            return Viewport3DHelper.UnProject(_parentViewport, p, hitPlaneOrigin, hitPlaneNormal);
        }

        #endregion

    }
}
