﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
//V8-24265 N.Haywood
//  Added more properties for labels
#endregion
#endregion

using System;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;
using Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData;
using Galleria.Ccm.Model;


namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderControls
{
    /// <summary>
    /// Maintains a 3d model which represents a label.
    /// </summary>
    public sealed class ModelLabel3D : IEditableObject, IDisposable
    {
        #region Fields

        const Double _imgResolution = 1000;

        private IModelLabel3DData _modelLabelData;
        private readonly GeometryModel3D _geometryModel;

        private Boolean _isGeometryChanged; //Flag to indicate if the geometry is out of date.

        #endregion

        #region Properties

        /// <summary>
        /// Returns the model3d content to be rendered.
        /// </summary>
        public Model3D Content
        {
            get { return _geometryModel; }
        }

        /// <summary>
        /// Returns the label data source.
        /// </summary>
        public IModelLabel3DData ModelLabelData
        {
            get { return _modelLabelData; }
        }

        #endregion

        #region Constructor

        public ModelLabel3D(IModelLabel3DData labelData)
        {
            _geometryModel = new GeometryModel3D();

            _modelLabelData = labelData;
            _modelLabelData.PropertyChanged += ModelLabelData_PropertyChanged;

            UpdateModel();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when a property changes on the model label data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ModelLabelData_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsEditing")
            {
                if (this.ModelLabelData.IsEditing && !this.IsEditing)
                {
                    BeginEdit();
                }
                else if (!this.ModelLabelData.IsEditing && this.IsEditing)
                {
                    EndEdit();
                }
            }
            else if (e.PropertyName == "Offset"
                || e.PropertyName == "BackgroundColour"
                || e.PropertyName == "BorderColour"
                || e.PropertyName == "BorderThickness"
                || e.PropertyName == "Text"
                || e.PropertyName == "TextColour"
                || e.PropertyName == "FontName"
                || e.PropertyName == "FontSize"
                || e.PropertyName == "MinFontSize"
                || e.PropertyName == "ShrinkFontToFit"
                || e.PropertyName == "RotateFontToFit"
                || e.PropertyName == "Alignment"
                || e.PropertyName == "AttachFace"
                || e.PropertyName == "TextWrapping"
                || e.PropertyName == "IsVisible"
                || e.PropertyName == "MaxSize"
                || e.PropertyName == "TextBoxFontBold"
                || e.PropertyName == "TextBoxFontItalic"
                || e.PropertyName == "TextBoxFontUnderlined"
                || e.PropertyName == "LabelHorizontalPlacement"
                || e.PropertyName == "LabelVerticalPlacement"
                || e.PropertyName == "TextBoxTextAlignment"
                || e.PropertyName == "TextDirection"
                || e.PropertyName == "TextWrapping"
                || e.PropertyName == "BackgroundTransparency"
                || e.PropertyName == "ShowOverImages"
                || e.PropertyName == "ShowLabelPerFacing"
                || e.PropertyName == "HasImage"
                )
            {
                OnGeometryChanged();
            }
        }

        /// <summary>
        /// Called whenever a geometry property changes.
        /// </summary>
        private void OnGeometryChanged()
        {
            if (!IsEditing)
            {
                _isGeometryChanged = true;

                this.Content.Dispatcher.BeginInvoke(
                    (Action)(() =>
                    {
                        if (_isGeometryChanged)
                        {
                            _isGeometryChanged = false;
                            Tessellate();
                        }
                    }), priority: System.Windows.Threading.DispatcherPriority.ContextIdle);
            }
            else
            {
                _isGeometryChanged = true;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Forces the model to update.
        /// </summary>
        public void UpdateModel()
        {
            OnGeometryChanged();
        }

        /// <summary>
        /// Creates the model geometry.
        /// </summary>
        private void Tessellate()
        {
            GeometryModel3D model = _geometryModel;

            IModelLabel3DData labelData = this.ModelLabelData;

            Geometry3D geometry = null;
            Material material = null;

            //only render if this is visible
            if (labelData.IsVisible && !String.IsNullOrEmpty(labelData.Text))
            {
                Point3D size = new Point3D(labelData.MaxSize.X, labelData.MaxSize.Y, labelData.MaxSize.Z);

                Double height;
                Double width;
                Vector3D up;
                Vector3D over;

                if (labelData.AttachFace == LabelAttachFace.Top)
                {
                    height = Math.Abs(size.Z);
                    width = Math.Abs(size.X);

                    up = new Vector3D(0, 0, -1);
                    over = new Vector3D(1, 0, 0);
                }
                else
                {
                    height = Math.Abs(size.Y);
                    width = Math.Abs(size.X);

                    up = new Vector3D(0, 1, 0);
                    over = new Vector3D(1, 0, 0);
                }


                if (height > 0 && width > 0 && !String.IsNullOrEmpty(labelData.Text))
                {
                    Thickness margin = new Thickness(labelData.Margin);
                    Point3D center = GetAnnotationPosition(size);

                    #region V1 - Drawing Brush
                    {
                        //NB- This is the old way of drawing the label but the measurement wasnt coming out
                        // quite right. Leaving in for now while new version is still being checked.

                        //margin = new Thickness(Math.Min(height * 0.05f, Math.Min(width * 0.05f, 2)));

                        ////create the label
                        //CreateTextLabel3D(
                        //   labelData,
                        //    /*isDoubleSided*/false,
                        //    height, width,
                        //    center, up, over,
                        //    margin,
                        //   out geometry, out material); 
                    }
                    #endregion

                    #region V2 - Drawing Brush with bitmap render
                    {
                        Double innerMaxHeight = Math.Max(height - margin.Top - margin.Bottom, 0);
                        Double innerMaxWidth = Math.Max(width - margin.Left - margin.Right, 0);

                        Vector3D textDirection = new Vector3D(1, 0, 0);

                        var xa = -0.5;
                        var ya = -0.5;
                        // Set horizontal alignment factor
                        if (this.ModelLabelData.LabelHorizontalPlacement == LabelHorizontalPlacementType.Left)
                        {
                            if ((labelData.TextDirection == LabelTextDirectionType.Vertical && !labelData.RotateFontToFit) || 
                                labelData.RotateFontToFit && innerMaxHeight > innerMaxWidth)
                            {
                                ya = -1;
                            }
                            else
                            {
                                xa = 0;
                            }
                        }
                        if (this.ModelLabelData.LabelHorizontalPlacement == LabelHorizontalPlacementType.Right)
                        {
                            if ((labelData.TextDirection == LabelTextDirectionType.Vertical && !labelData.RotateFontToFit) || 
                                labelData.RotateFontToFit && innerMaxHeight > innerMaxWidth)
                            {
                                ya = 0;
                            }
                            else
                            {
                                xa = -1;
                            }
                        }

                        // Set vertical alignment factor
                        if (this.ModelLabelData.LabelVerticalPlacement == LabelVerticalPlacementType.Bottom)
                        {
                            if ((labelData.TextDirection == LabelTextDirectionType.Vertical && !labelData.RotateFontToFit) || 
                                labelData.RotateFontToFit && innerMaxHeight > innerMaxWidth)
                            {
                                xa = 0;
                            }
                            else
                            {
                                ya = 0;
                            }
                        }
                        if (this.ModelLabelData.LabelVerticalPlacement == LabelVerticalPlacementType.Top)
                        {
                            if ((labelData.TextDirection == LabelTextDirectionType.Vertical && !labelData.RotateFontToFit) || 
                                labelData.RotateFontToFit && innerMaxHeight > innerMaxWidth)
                            {
                                xa = -1;
                            }
                            else
                            {
                                ya = -1;
                            }
                        }

                        if (labelData.RotateFontToFit)
                        {
                            if (innerMaxHeight > innerMaxWidth)
                            {
                                Double sizeX = innerMaxHeight;
                                innerMaxHeight = innerMaxWidth;
                                innerMaxWidth = sizeX;

                                up = new Vector3D(-1, 0, 0);

                                if (labelData.AttachFace == LabelAttachFace.Top)
                                {
                                    textDirection = new Vector3D(0, 0, -1);
                                }
                                else
                                {
                                    textDirection = new Vector3D(0, 1, 0);
                                }
                            }
                        }
                        else if (labelData.TextDirection == LabelTextDirectionType.Vertical)
                        {
                            Double sizeX = innerMaxHeight;
                            innerMaxHeight = innerMaxWidth;
                            innerMaxWidth = sizeX;

                            up = new Vector3D(-1, 0, 0);
                            textDirection = new Vector3D(0, 1, 0);
                        }

                        Double maxHeight = innerMaxHeight;
                        Double maxWidth = innerMaxWidth;

                        Double labelWidth;
                        Double labelHeight;
                        Material mat = CreateMaterial(maxWidth, maxHeight, out labelWidth, out labelHeight);



                        // Since the parameter coming in was the center of the label,
                        // we need to find the four corners
                        // p0 is the lower left corner
                        // p1 is the upper left
                        // p2 is the lower right
                        // p3 is the upper right
                        Point3D p0 = center + (xa * labelWidth) * textDirection + (ya * labelHeight) * up;
                        Point3D p1 = p0 + up * labelHeight;
                        Point3D p2 = p0 + textDirection * labelWidth;
                        Point3D p3 = p0 + up * labelHeight + textDirection * labelWidth;

                        // Now build the geometry for the sign.  It's just a
                        // rectangle made of two triangles, on each side.
                        var mg = new MeshGeometry3D { Positions = new Point3DCollection { p0, p1, p2, p3 } };

                        Boolean isDoubleSided = false;
                        if (isDoubleSided)
                        {
                            mg.Positions.Add(p0); // 4
                            mg.Positions.Add(p1); // 5
                            mg.Positions.Add(p2); // 6
                            mg.Positions.Add(p3); // 7
                        }

                        mg.TriangleIndices.Add(0);
                        mg.TriangleIndices.Add(3);
                        mg.TriangleIndices.Add(1);
                        mg.TriangleIndices.Add(0);
                        mg.TriangleIndices.Add(2);
                        mg.TriangleIndices.Add(3);

                        if (isDoubleSided)
                        {
                            mg.TriangleIndices.Add(4);
                            mg.TriangleIndices.Add(5);
                            mg.TriangleIndices.Add(7);
                            mg.TriangleIndices.Add(4);
                            mg.TriangleIndices.Add(7);
                            mg.TriangleIndices.Add(6);
                        }

                        Boolean isFlipped = false;
                        double u0 = isFlipped ? 1 : 0;
                        double u1 = isFlipped ? 0 : 1;

                        // These texture coordinates basically stretch the
                        // TextBox brush to cover the full side of the label.
                        mg.TextureCoordinates.Add(new Point(u0, 1));
                        mg.TextureCoordinates.Add(new Point(u0, 0));
                        mg.TextureCoordinates.Add(new Point(u1, 1));
                        mg.TextureCoordinates.Add(new Point(u1, 0));

                        if (isDoubleSided)
                        {
                            mg.TextureCoordinates.Add(new Point(u1, 1));
                            mg.TextureCoordinates.Add(new Point(u1, 0));
                            mg.TextureCoordinates.Add(new Point(u0, 1));
                            mg.TextureCoordinates.Add(new Point(u0, 0));
                        }


                        geometry = mg;
                        material = mat;
                    }
                    #endregion
                }

            }


            //apply the geometry and transform.
            model.Geometry = geometry;
            model.Material = material;
            //model.BackMaterial = material; 
        }

        /// <summary>
        /// Returns the center position of the given annotation.
        /// </summary>
        /// <param name="size"></param>
        /// <param name="annotation"></param>
        /// <returns></returns>
        private Point3D GetAnnotationPosition(Point3D size)
        {
            ObjectPoint3D offset = this.ModelLabelData.Offset;

            Double zOffset = size.Z + 0.1;
            Double xPos, yPos;
            switch (this.ModelLabelData.LabelHorizontalPlacement)
            {
                default:
                case LabelHorizontalPlacementType.Center:
                    xPos = size.X / 2F + offset.X;
                    break;
                case LabelHorizontalPlacementType.Left:
                    xPos = offset.X;
                    break;
                case LabelHorizontalPlacementType.Right:
                    xPos = size.X - offset.X;
                    break;
            }
            switch (this.ModelLabelData.LabelVerticalPlacement)
            {
                default:
                case LabelVerticalPlacementType.Center:
                    yPos = size.Y / 2F + offset.Y;
                    break;
                case LabelVerticalPlacementType.Bottom:
                    yPos = offset.Y;
                    break;
                case LabelVerticalPlacementType.Top:
                    yPos = size.Y - offset.Y;
                    break;
            }
            return new Point3D(xPos, yPos, offset.Z + zOffset);
        }

        private Material CreateMaterial(Double maxWidth, Double maxHeight, out Double actualWidth, out Double actualHeight)
        {
            IModelLabel3DData anno = this.ModelLabelData;

            maxWidth = Math.Max(1, maxWidth);
            maxHeight = Math.Max(1, maxHeight);

            String text = anno.Text;

            //ammend text as per required wrapping.
            switch (anno.TextWrapping)
            {
                default:
                case LabelTextWrapping.Word:
                    //default.
                    break;

                case LabelTextWrapping.Letter:
                    text = text.Replace(" ", "\u00a0"); //replace oridinary spaces with non line breaking ones.
                    String newText = String.Empty;
                    foreach (Char c in text.ToCharArray())
                    {
                        newText = String.Format("{0}{1}{2}", newText, c, "\u200B");
                    }
                    text = newText;
                    break;

                case LabelTextWrapping.None:
                    text = text.Replace(" ", "\u00a0"); //replace oridinary spaces with non line breaking ones.
                    break;
            }

            Typeface typeface;
            FontStyle fontStyle = FontStyles.Normal;
            FontWeight fontWeight = FontWeights.Normal;

            if (anno.TextBoxFontBold)
            {
                fontWeight = FontWeights.Bold;
            }
            if (anno.TextBoxFontItalic)
            {
                fontStyle = FontStyles.Italic;
            }

            typeface = new Typeface(new FontFamily(anno.FontName), fontStyle, fontWeight, FontStretches.Normal);


            FormattedText ft = new FormattedText(
                        text,
                        System.Globalization.CultureInfo.CurrentCulture,
                        FlowDirection.LeftToRight,
                        typeface,
                        anno.FontSize,
                        anno.TextColour.ToSolidBrush());

            if(anno.TextBoxFontUnderlined)
            {
                TextDecorationCollection decorations = new TextDecorationCollection();
                decorations.Add(TextDecorations.Underline);
                ft.SetTextDecorations(decorations);
            }

            ft.TextAlignment = anno.Alignment;
            ft.Trimming = TextTrimming.None;
            ft.MaxTextWidth = maxWidth;

            //shrink font if allowed
            if (anno.ShrinkFontToFit)
            {
                Double fontSize = anno.FontSize;
                while (fontSize > anno.MinFontSize && ft.Height > maxHeight)
                {
                    fontSize--;
                    ft.SetFontSize(fontSize);
                }
            }
            ft.MaxTextHeight = maxHeight;
            ft.Trimming = TextTrimming.CharacterEllipsis;
            ft.MaxTextWidth = ft.Width;

            actualWidth = ft.Width;
            actualHeight = ft.Height;

            if (actualWidth == 0 || actualHeight == 0)
            {
                return null;
            }
            else
            {
                DrawingVisual dv = new DrawingVisual();

                if (!anno.HasImage || anno.ShowOverImages)
                {
                    using (DrawingContext dc = dv.RenderOpen())
                    {
                        dc.PushOpacity(anno.BackgroundTransparency);
                        dc.DrawRectangle(
                            anno.BackgroundColour.ToSolidBrush(),
                            new Pen(anno.BorderColour.ToSolidBrush(), anno.BorderThickness),
                                new Rect(new Point(), new Point(actualWidth, actualHeight))
                                );

                        //place the text
                        dc.PushOpacity(1.0f);

                        Point textDrawPoint = new Point();
                        dc.DrawText(ft, textDrawPoint);
                    }
                }

                //+Create the material
                DrawingBrush db;
                db = new DrawingBrush(dv.Drawing);

                db.Freeze();

                Material mat = new DiffuseMaterial(db);

                #region old image render code?
                //Boolean attemptImageRender = true;
                //if (attemptImageRender)
                //{

                //    Grid grd = new Grid();
                //    grd.Height = actualHeight;
                //    grd.Width = actualWidth;
                //    grd.Background = db;
                //    grd.Measure(new Size(1000, 1000));
                //    grd.Arrange(new Rect(grd.DesiredSize));


                //    // Scale dimensions from 96 dpi to get better quality
                //    Double scale = 800 / 96;

                //    //perform a straight render.
                //    try
                //    {
                //        RenderTargetBitmap rtb =
                //            new RenderTargetBitmap(
                //                (Int32)(scale * (actualWidth)),
                //                (Int32)(scale * (actualHeight)),
                //            scale * 96,
                //            scale * 96,
                //            PixelFormats.Default);
                //        rtb.Render(grd);
                //        rtb.Freeze();

                //        // encode the image to stop the render target bitmap from leaking
                //        Byte[] renderBytes;
                //        using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
                //        {
                //            PngBitmapEncoder encoder = new PngBitmapEncoder();
                //            encoder.Frames.Add(BitmapFrame.Create(rtb));
                //            encoder.Save(memoryStream);

                //            // Export the stream to a byte array
                //            renderBytes = CreateImageBlob(memoryStream, true);
                //        }

                //        rtb.Clear();
                //        rtb = null;
                //        grd = null;

                //        BitmapImage img = new BitmapImage();
                //        img.BeginInit();
                //        img.StreamSource = new MemoryStream(renderBytes);
                //        img.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
                //        img.CacheOption = BitmapCacheOption.Default;
                //        img.EndInit();
                //        img.Freeze();

                //        ImageBrush brush = new ImageBrush(img);
                //        brush.Freeze();

                //        mat = new DiffuseMaterial(brush);
                //    }
                //    catch (Exception)
                //    {
                //        GC.Collect();
                //        GC.WaitForPendingFinalizers();
                //    }
                //}
                #endregion

                RenderOptions.SetCachingHint(mat, CachingHint.Cache);
                mat.Freeze();

                return mat;
            }
        }

        /// <summary>
        /// Creates a Byte array from a Stream
        /// </summary>
        /// <param name="dataStream">Stream to read</param>
        /// <param name="CloseStream">Closes the Stream once finished if True.</param>
        /// <returns>Array of Bytes</returns>
        private static Byte[] CreateImageBlob(Stream dataStream, Boolean CloseStream)
        {
            Byte[] blob = new Byte[dataStream.Length];
            //Ensure stream is at the start
            dataStream.Position = 0;

            //Stream read does not necessarily read all the bytes asked for so
            //a loop is required to ensure all bytes are read.
            for (Int32 pos = 0; pos < dataStream.Length; )
            {
                Int32 len = dataStream.Read(blob, pos, (Int32)dataStream.Length - pos);
                if (len == 0)
                {
                    break;
                }
                pos += len;

            }

            if (CloseStream) dataStream.Close();
            return blob;
        }

        #endregion

        #region Static Helpers


        ///// <summary>
        ///// Creates the text label 3d
        ///// </summary>
        ///// <param name="anno"></param>
        ///// <param name="borderThickness"></param>
        ///// <param name="doubleSided"></param>
        ///// <param name="maxHeight"></param>
        ///// <param name="maxWidth"></param>
        ///// <param name="center"></param>
        ///// <param name="over"></param>
        ///// <param name="up"></param>
        ///// <param name="margin"></param>
        ///// <param name="mesh"></param>
        ///// <param name="material"></param>
        //private static void CreateTextLabel3D(
        //    IModelLabel3DData anno,
        //       Boolean doubleSided,
        //       Double maxHeight, Double maxWidth,
        //       Point3D center, Vector3D up, Vector3D over,
        //        Thickness margin,
        //     out Geometry3D mesh, out Material material)
        //{
        //    Brush backgroundColour = anno.BackgroundColour.ToSolidBrush();
        //    Brush borderColour = anno.BorderColour.ToSolidBrush();
        //    Double borderThickness = anno.BorderThickness;

        //    String text = anno.Text;
        //    String fontName = anno.FontName;
        //    Double fontSize = anno.FontSize;
        //    Boolean rotateToFit = anno.RotateFontToFit;
        //    Boolean shrinkFontToFit = anno.ShrinkFontToFit;
        //    Double minFontSize = anno.MinFontSize;

        //    AnnotationAlignment alignment = anno.Alignment;

        //    Brush fontColour = anno.TextColour.ToSolidBrush();
        //    if (fontColour == null) fontColour = Brushes.Black;

        //    DrawingVisual dv = new DrawingVisual();
        //    Double innerMaxHeight = Math.Max(maxHeight - margin.Top - margin.Bottom, 0);
        //    Double innerMaxWidth = Math.Max(maxWidth - margin.Left - margin.Right, 0);
        //    Double height = 0;
        //    Double width = 0;
        //    Boolean backgroundOnly = fontSize <= 0;
        //    Boolean switchHeightWidth = false;

        //    Grid grid = new Grid();


        //    if (!backgroundOnly && !String.IsNullOrEmpty(text))
        //    {
        //        FormattedText ft = new FormattedText(
        //                text,
        //                System.Globalization.CultureInfo.CurrentCulture,
        //                FlowDirection.LeftToRight,
        //                new Typeface(fontName),
        //                fontSize,
        //                fontColour);

        //        if (innerMaxHeight > innerMaxWidth && rotateToFit)
        //        {
        //            Double temp = innerMaxHeight;
        //            innerMaxHeight = innerMaxWidth;
        //            innerMaxWidth = temp;
        //            switchHeightWidth = true;
        //        }

        //        height = ft.Height;
        //        width = ft.Width;

        //        StringBuilder sb = new StringBuilder(text);
        //        if (shrinkFontToFit)
        //        {
        //            fontSize = Math.Min(fontSize, (Int32)innerMaxHeight);
        //            fontSize = Math.Max(fontSize, minFontSize);
        //            while (ft.Height > innerMaxHeight || ft.Width > innerMaxWidth)
        //            {
        //                if (fontSize <= minFontSize)
        //                {
        //                    ft.SetFontSize(fontSize);

        //                    ft.MaxTextHeight = innerMaxHeight + 0.001f;
        //                    ft.MaxTextWidth = innerMaxWidth + 0.001f;

        //                    height = ft.Height;
        //                    width = ft.Width;
        //                    break;
        //                }
        //                else
        //                {
        //                    fontSize--;
        //                    ft.SetFontSize(fontSize);
        //                    height = ft.Height;
        //                    width = ft.Width;
        //                }
        //            }

        //        }
        //        else
        //        {
        //            ft.MaxTextHeight = innerMaxHeight + 0.001f;
        //            ft.MaxTextWidth = innerMaxWidth + 0.001f;

        //            height = ft.Height;
        //            width = ft.Width;


        //        }

        //        if (alignment == AnnotationAlignment.Stretch)
        //        {
        //            height = innerMaxHeight;
        //            width = innerMaxWidth;
        //        }

        //        //if we have failed to create any height or width then
        //        //draw the text proportionaly scaled
        //        if (height <= 0 || width <= 0)
        //        {
        //            ft.MaxTextHeight = Int16.MaxValue;
        //            ft.MaxTextWidth = innerMaxWidth;
        //            ft.MaxLineCount = 1;

        //            if (innerMaxHeight <= innerMaxWidth)
        //            {
        //                height = innerMaxHeight;
        //                width = ft.Width * (innerMaxHeight / ft.Height);

        //                if (width > innerMaxWidth)
        //                {
        //                    height = ft.Height * (innerMaxWidth / ft.Width);
        //                    width = innerMaxWidth;
        //                }
        //            }
        //            else
        //            {
        //                height = ft.Height * (innerMaxWidth / ft.Width);
        //                width = innerMaxWidth;

        //                if (height > innerMaxHeight)
        //                {
        //                    height = innerMaxHeight;
        //                    width = ft.Width * (innerMaxHeight / ft.Height);
        //                }
        //            }
        //        }



        //        using (DrawingContext dc = dv.RenderOpen())
        //        {
        //            if (switchHeightWidth)
        //            {
        //                RotateTransform rotate = new RotateTransform(-90);
        //                dc.PushTransform(rotate);
        //            }
        //            if (backgroundColour != null)
        //            {
        //                dc.DrawRectangle(backgroundColour,
        //                    new Pen(borderColour, Math.Min(borderThickness, 0.5f)),
        //                    new Rect(new Point(), new Point(ft.Width, ft.Height)));
        //            }

        //            dc.DrawText(ft, new Point());

        //        }

        //        grid.Height = ft.Height;
        //        grid.Width = ft.Width;
        //    }
        //    else
        //    {
        //        height = innerMaxHeight;
        //        width = innerMaxWidth;

        //        using (DrawingContext dc = dv.RenderOpen())
        //        {
        //            if (backgroundColour != null)
        //            {
        //                dc.DrawRectangle(backgroundColour,
        //                    null,
        //                    new Rect(new Point(0, 0), new Point(width, height)));
        //            }

        //        }

        //        grid.Height = height;
        //        grid.Width = width;
        //    }

        //    //+Create the material
        //    DrawingBrush db = new DrawingBrush(dv.Drawing);
        //    RenderOptions.SetCachingHint(db, CachingHint.Cache);
        //    db.Freeze();


        //    //if (switchHeightWidth)
        //    //{
        //    //    Double owidth = grid.Width;
        //    //    grid.Width = grid.Height;
        //    //    grid.Height = owidth;

        //    //}


        //    //grid.Background = db;
        //    //grid.Measure(new Size(1000, 1000));
        //    //grid.Arrange(new Rect(grid.DesiredSize));

        //    // Scale dimensions from 96 dpi to get better quality
        //    //Double scale = _imgResolution / 96;

        //    //perform a straight render.
        //    Material mat = new DiffuseMaterial(db);
        //    //try
        //    //{
        //    //    RenderTargetBitmap rtb =
        //    //        new RenderTargetBitmap(
        //    //            (Int32)(scale * (grid.ActualWidth)),
        //    //            (Int32)(scale * (grid.ActualHeight)),
        //    //        scale * 96,
        //    //        scale * 96,
        //    //        PixelFormats.Default);
        //    //    rtb.Render(grid);
        //    //    rtb.Freeze();

        //    //    ImageBrush brush = new ImageBrush(rtb);
        //    //    brush.Freeze();

        //    //    grid = null;
        //    //    rtb = null;

        //    //    mat = new DiffuseMaterial(brush);
        //    //}
        //    //catch (Exception)
        //    //{
        //    //    //GC.Collect();
        //    //}

        //    material = mat;


        //    switch (alignment)
        //    {
        //        case AnnotationAlignment.Centered:
        //            break;
        //        case AnnotationAlignment.CenterBottom:
        //            center.Y += height / 2;
        //            break;
        //        case AnnotationAlignment.LeftBottom:
        //            center.Y += height / 2;
        //            center.X += width / 2;
        //            break;
        //        case AnnotationAlignment.Stretch:
        //            break;
        //    }


        //    //+Create the mesh
        //    if (switchHeightWidth)
        //    {
        //        mesh = ModelConstruct3DHelper.CreateFaceGeometry(center, width, height, doubleSided, over, up);
        //    }
        //    else
        //    {
        //        mesh = ModelConstruct3DHelper.CreateFaceGeometry(center, height, width, doubleSided, over, up);
        //    }
        //}

        #endregion

        #region IEditableObject Members

        /// <summary>
        /// Flag to indicate if this is currently editing
        /// </summary>
        public Boolean IsEditing { get; private set; }

        public void BeginEdit()
        {
            IsEditing = true;
        }

        public void CancelEdit()
        {
            IsEditing = false;
        }

        public void EndEdit()
        {
            IsEditing = false;

            if (_isGeometryChanged)
            {
                OnGeometryChanged();
            }
        }


        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            if (!_isDisposed)
            {
                this.ModelLabelData.PropertyChanged -= ModelLabelData_PropertyChanged;

                _isDisposed = true;
                GC.SuppressFinalize(this);
            }
        }

        #endregion

    }
}
