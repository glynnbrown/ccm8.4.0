﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Copied and ammended from GFS.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;
using Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData;
using System.Diagnostics;
using HelixToolkit.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderControls
{
    /// <summary>
    /// Implementation of a model construct part based on raw mesh data.
    /// </summary>
    public class BoxMeshConstructPart3D : ModelConstructPart3D
    {
        #region Fields

        private readonly IBoxMeshPart3DData _partData;
        private readonly GeometryModel3D _mainGeometry;

        #endregion

        #region Properties

        public IBoxMeshPart3DData PartData
        {
            get { return _partData; }
        }


        protected ObjectMaterial3D BackFaceMaterial
        {
            get
            {
                ObjectMaterial3D mat = PartData.BackFaceMaterial;

                if (mat == null)
                {
                    mat = this.Material;
                }

                return mat;
            }
        }
        protected ObjectMaterial3D TopFaceMaterial
        {
            get
            {
                ObjectMaterial3D mat = PartData.TopFaceMaterial;

                if (mat == null)
                {
                    mat = this.Material;
                }

                return mat;
            }
        }
        protected ObjectMaterial3D BottomFaceMaterial
        {
            get
            {
                ObjectMaterial3D mat = PartData.BottomFaceMaterial;

                if (mat == null)
                {
                    mat = this.Material;
                }

                return mat;
            }
        }
        protected ObjectMaterial3D LeftFaceMaterial
        {
            get
            {
                ObjectMaterial3D mat = PartData.LeftFaceMaterial;

                if (mat == null)
                {
                    mat = this.Material;
                }

                return mat;
            }
        }
        protected ObjectMaterial3D RightFaceMaterial
        {
            get
            {
                ObjectMaterial3D mat = PartData.RightFaceMaterial;

                if (mat == null)
                {
                    mat = this.Material;
                }

                return mat;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="modelPartData">The part data.</param>
        public BoxMeshConstructPart3D(IBoxMeshPart3DData modelPartData)
            : base(modelPartData)
        {
            base.RenderLineAsWireFrame = false;

            _partData = modelPartData;

            //Create the main geometry model.
            _mainGeometry = new GeometryModel3D();
            this.Content.Children.Add(_mainGeometry);

            UpdateModel();
        }

        #endregion

        #region Event Handlers

        protected override void OnModelPartDataPropertyChanged(String propertyName)
        {
            base.OnModelPartDataPropertyChanged(propertyName);

            if (propertyName == "Builder")
            {
                OnGeometryChanged();
            }
            else if (propertyName == "BackFaceMaterial"
                || propertyName == "TopFaceMaterial"
                || propertyName == "BottomFaceMaterial"
                || propertyName == "LeftFaceMaterial"
                || propertyName == "RightFaceMaterial")
            {
                OnMaterialChanged();
            }
        }

        #endregion

        #region Methods

        protected override void Tessellate()
        {
            if (_mainGeometry == null) { return; }

            IBoxMeshPart3DData meshData = this.PartData;

            if (this.IsVisible && meshData.Builder != null)
            {
                ModelConstructMesh meshBuilder = ModelConstructMesh.Build(meshData.Builder);


                Point3DCollection positions = new Point3DCollection(meshBuilder.Positions.Count());
                foreach (ObjectPoint3D p in meshBuilder.Positions)
                {
                    positions.Add(p.ToPoint3D());
                }
                positions.Freeze();

                Vector3DCollection normals = null;
                if (meshBuilder.Normals != null)
                {
                    normals = new Vector3DCollection(meshBuilder.Normals.Count);
                    foreach (ObjectVector3D p in meshBuilder.Normals)
                    {
                        normals.Add(p.ToVector3D());
                    }
                    normals.Freeze();
                }


                MeshGeometry3D mesh = new MeshGeometry3D()
                {
                    Positions = positions,
                    Normals = normals,
                    TriangleIndices = new Int32Collection(meshBuilder.TriangleIndices),
                    TextureCoordinates = new PointCollection(meshBuilder.TextureCoordinates)
                };
                mesh.Freeze();

                _mainGeometry.Geometry = mesh;
            }
            else
            {
                _mainGeometry.Geometry = null;
            }


            TessellateWireframe();
        }

        /// <summary>
        /// Applies the model materials
        /// </summary>
        protected override void ApplyMaterials()
        {
            if (_mainGeometry == null) { return; }

            Dictionary<BoxFaceType, ObjectMaterial3D> matDict = new Dictionary<BoxFaceType, ObjectMaterial3D>();
            matDict.Add(BoxFaceType.Front, this.Material);
            matDict.Add(BoxFaceType.Back, this.BackFaceMaterial);
            matDict.Add(BoxFaceType.Top, this.TopFaceMaterial);
            matDict.Add(BoxFaceType.Bottom, this.BottomFaceMaterial);
            matDict.Add(BoxFaceType.Left, this.LeftFaceMaterial);
            matDict.Add(BoxFaceType.Right, this.RightFaceMaterial);

            ObjectSize3D tileSize = this.PartData.MaterialTileSize;
            if (tileSize.X == 0 && tileSize.Y == 0 && tileSize.Z == 0)
            {
                Debug.WriteLine("MaterialTileSize value not set");
                Rect3D geoBounds = _mainGeometry.Bounds;
                tileSize = new ObjectSize3D(Convert.ToSingle(geoBounds.SizeX), Convert.ToSingle(geoBounds.SizeY), Convert.ToSingle(geoBounds.SizeZ));
            }

            Brush matBrush = ModelConstruct3DHelper.CreateBoxMaterialBrush(matDict, tileSize,
                this.PartData.LineThickness, this.PartData.LineColour);

            Material m = new DiffuseMaterial(matBrush);
            m.Freeze();
            _mainGeometry.Material = m;

            //update the material of the wireframe model.
            ApplyWireframeMaterial();
        }

        #endregion
    }
}
