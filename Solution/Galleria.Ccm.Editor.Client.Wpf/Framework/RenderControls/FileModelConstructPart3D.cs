﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Copied and ammended from GFS.
#endregion
#endregion

using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;
using Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData;
using HelixToolkit.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderControls
{
    /// <summary>
    /// Implementation of a model construct part that is rendered from
    /// a file stream.
    /// </summary>
    public sealed class FileModelConstructPart3D : ModelConstructPart3D
    {
        #region Fields
        private readonly IFileModelPart3DData _partData;
        #endregion

        #region Properties

        public IFileModelPart3DData PartData
        {
            get { return _partData; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="modelPartData"></param>
        public FileModelConstructPart3D(IFileModelPart3DData modelPartData)
            : base(modelPartData)
        {
            _partData = modelPartData;
            UpdateModel();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on the model part data.
        /// </summary>
        /// <param name="propertyName"></param>
        protected override void OnModelPartDataPropertyChanged(String propertyName)
        {
            base.OnModelPartDataPropertyChanged(propertyName);

            if (propertyName == "FileData"
                || propertyName == "FileType")
            {
                OnGeometryChanged();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates the model geometry
        /// </summary>
        protected override void Tessellate()
        {
            //clear out any existing models.
            ClearAllModels();

            //create the default material
            ObjectMaterial3D mat = this.Material;
            Material defaultMaterial = null;

            if (mat.ImageData != null)
            {
                Brush matBrush = new ImageBrush(ModelConstruct3DHelper.GetBitmapImage(mat.ImageData));
                DiffuseMaterial diffuseMat = new DiffuseMaterial(matBrush);
                diffuseMat.Freeze();

                defaultMaterial = diffuseMat;
            }
            else
            {
                DiffuseMaterial diffuseMat = new DiffuseMaterial(mat.FillColour.ToSolidBrush());
                diffuseMat.Freeze();

                defaultMaterial = diffuseMat;
            }




            //load the model
            Model3DGroup model = null;
            try
            {
                using (MemoryStream stream = new MemoryStream(PartData.FileData))
                {
                    switch (PartData.FileType)
                    {
                        case FileModelPartType.studio3ds:
                            {
                                StudioReader reader = new StudioReader();
                                model = reader.Read(stream);
                            }
                            break;

                        case FileModelPartType.lwo:
                            {
                                LwoReader reader = new LwoReader();
                                reader.DefaultMaterial = defaultMaterial;
                                model = reader.Read(stream);
                            }
                            break;

                        case FileModelPartType.obj:
                            {
                                ObjReader reader = new ObjReader();
                                reader.DefaultMaterial = defaultMaterial;
                                model = reader.Read(stream);
                            }
                            break;

                        case FileModelPartType.stl:
                            {
                                StLReader reader = new StLReader();
                                reader.DefaultMaterial = defaultMaterial;
                                model = reader.Read(stream);
                            }
                            break;

                        case FileModelPartType.off:
                            {
                                OffReader reader = new OffReader();
                                reader.DefaultMaterial = defaultMaterial;
                                model = reader.Read(stream);
                            }
                            break;

                        default:
                            Debug.Fail("Not Supported");
                            break;
                    }
                }
            }
            catch (ArgumentException) { }


            if (model != null)
            {
                foreach (Model3D child in model.Children)
                {
                    Content.Children.Add(child);
                }
            }



            TessellateWireframe();
        }

        /// <summary>
        /// Applies the model material.
        /// </summary>
        protected override void ApplyMaterials()
        {
            //dont do anything to the main model.

            ApplyWireframeMaterial();
        }

        #endregion

    }
}
