﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Copied and ammended from the existing framework version
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;
using Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData;
using HelixToolkit.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderControls
{
    /// <summary>
    /// Abstract class used to create and manage a model construct part.
    /// </summary>
    public abstract class ModelConstructPart3D : IDisposable
    {
        #region Fields

        private IModelConstructPart3DData _modelPartData; //the source data.
        private readonly Model3DGroup _mainVisualGroup; //the main model group

        private GeometryModel3D _wireframeGeometryModel; //the geometry model used for the wireframe

        private Boolean _renderLineAsWireframe = true;

        #endregion

        #region Properties

        /// <summary>
        /// Flag to indicate if the transform needs updating.
        /// </summary>
        protected Boolean IsPositionChanged { get; private set; }

        /// <summary>
        /// Flag to indicate if the material needs updating.
        /// </summary>
        protected Boolean IsMaterialChanged { get; private set; }

        /// <summary>
        /// Flag to indicate if the geometry needs updating.
        /// </summary>
        protected Boolean IsGeometryChanged { get; private set; }

        /// <summary>
        /// The geometry model used for the wireframe
        /// </summary>
        protected GeometryModel3D WireframeGeometryModel
        {
            get { return _wireframeGeometryModel; }
        }


        protected Boolean RenderLineAsWireFrame
        {
            get { return _renderLineAsWireframe; }
            set { _renderLineAsWireframe = value; }
        }

        public Model3DGroup Content
        {
            get { return _mainVisualGroup; }
        }

        public IModelConstructPart3DData ModelPartData
        {
            get { return _modelPartData; }
        }

        public Boolean IsVisible
        {
            get
            {
                if (_modelPartData != null)
                {
                    return _modelPartData.IsVisible;
                }
                return false;
            }
        }

        public ObjectPoint3D Position
        {
            get
            {
                if (_modelPartData != null)
                {
                    return _modelPartData.Position;
                }
                return new ObjectPoint3D();
            }
        }

        public ObjectRotation3D Rotation
        {
            get
            {
                if (_modelPartData != null)
                {
                    return _modelPartData.Rotation;
                }
                return new ObjectRotation3D();
            }
        }

        public ObjectSize3D Size
        {
            get
            {
                if (_modelPartData != null)
                {
                    return _modelPartData.Size;
                }
                return new ObjectSize3D();
            }
        }

        public ObjectMaterial3D Material
        {
            get
            {
                if (_modelPartData != null)
                {
                    return _modelPartData.Material;
                }
                return ObjectMaterial3D.CreateMaterial(ObjectColour.White);
            }
        }

        #endregion

        #region Constructor

        public ModelConstructPart3D(IModelConstructPart3DData modelPartData)
        {
            _mainVisualGroup = new Model3DGroup();

            _modelPartData = modelPartData;
            _modelPartData.PropertyChanged += ModelPartData_PropertyChanged;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Event handler for PropertyChanged event on the model part data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ModelPartData_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnModelPartDataPropertyChanged(e.PropertyName);
        }

        /// <summary>
        /// Called whenever a property changes on the model part data.
        /// </summary>
        /// <param name="propertyName"></param>
        protected virtual void OnModelPartDataPropertyChanged(String propertyName)
        {
            if (propertyName == "IsEditing")
            {
                if (this.ModelPartData.IsEditing && !this.IsEditing)
                {
                    BeginEdit();
                }
                else if (!this.ModelPartData.IsEditing && this.IsEditing)
                {
                    EndEdit();
                }
            }
            else if (propertyName == "IsVisible"
                || propertyName == "Rotation"
                || propertyName == "Size"
                || propertyName == "IsWireframe")
            {
                OnGeometryChanged();
            }
            else if (propertyName == "Position")
            {
                OnPositionChanged();
            }
            else if (propertyName == "Material")
            {
                OnMaterialChanged();
            }
            else if (propertyName == "LineThickness"
                || propertyName == "LineColour")
            {
                TessellateWireframe();
                OnMaterialChanged();
            }

        }

        /// <summary>
        /// Called when the position value changes
        /// </summary>
        protected void OnPositionChanged()
        {
            if (!IsEditing)
            {
                IsPositionChanged = false;
                UpdateModelTransforms();
            }
            else
            {
                //flag the change
                IsPositionChanged = true;
            }
        }

        /// <summary>
        /// Called when the material property value changes
        /// </summary>
        protected void OnMaterialChanged()
        {
            if (!IsEditing)
            {
                IsMaterialChanged = false;
                ApplyMaterials();
            }
            else
            {
                //flag the change
                IsMaterialChanged = true;
            }
        }

        /// <summary>
        /// Called when a geometry property value changes.
        /// </summary>
        protected void OnGeometryChanged()
        {
            if (!IsEditing)
            {
                IsGeometryChanged = false;
                Tessellate();
            }
            else
            {
                IsGeometryChanged = true;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Forces an update of the model
        /// </summary>
        public void UpdateModel()
        {
            OnGeometryChanged();
            OnMaterialChanged();
            OnPositionChanged();
        }

        /// <summary>
        /// Abstract method to create the actual geometry
        /// of the model.
        /// </summary>
        protected abstract void Tessellate();

        /// <summary>
        /// Creates the wireframe model geometry
        /// </summary>
        protected void TessellateWireframe()
        {
            IModelConstructPart3DData partData = _modelPartData;

            Boolean renderWireframe = this.IsVisible && (partData.IsWireframe || (partData.LineThickness > 0 && _renderLineAsWireframe));

            if (renderWireframe)
            {
                //create the wireframe if required
                if (_wireframeGeometryModel == null)
                {
                    _wireframeGeometryModel = new GeometryModel3D();
                    _wireframeGeometryModel.Material = new DiffuseMaterial(Brushes.Black);
                    Content.Children.Add(_wireframeGeometryModel);
                }


                GeometryModel3D wireframeModel = _wireframeGeometryModel;
                Double wireThick = (partData.IsWireframe) ? 0.5 : partData.LineThickness;


                MeshBuilder mb = new MeshBuilder(false, false);
                foreach (GeometryModel3D model in GetMainModels())
                {
                    MeshGeometry3D mesh = model.Geometry as MeshGeometry3D;
                    if (mesh != null)
                    {
                        mb.AddEdges(mesh.Positions, MeshGeometryHelper.FindBorderEdges(mesh), wireThick, 36);
                    }

                }
                wireframeModel.Geometry = mb.ToMesh(/*freeze*/true);


                if (partData.IsWireframe)
                {
                    //remove orig models if this is wireframe only.
                    foreach (GeometryModel3D m in GetMainModels())
                    {
                        m.Geometry = null;
                    }
                }

            }
            else if (_wireframeGeometryModel != null)
            {
                _mainVisualGroup.Children.Remove(_wireframeGeometryModel);
                _wireframeGeometryModel = null;
            }
        }

        /// <summary>
        /// Applies the model materials.
        /// </summary>
        protected virtual void ApplyMaterials()
        {
            //Apply the main material to all of the main models.
            ObjectMaterial3D mat = this.Material;
            foreach (GeometryModel3D model in Content.Children)
            {
                if (model != _wireframeGeometryModel)
                {
                    ApplyMaterial(model, mat);
                }
            }


            ApplyWireframeMaterial();
        }

        /// <summary>
        /// Updates the position of the model.
        /// </summary>
        protected virtual void UpdateModelTransforms()
        {
            ObjectRotation3D rotation = this.Rotation;
            ObjectPoint3D pos = this.Position;

            Matrix3D matrix = new Matrix3D();
            matrix.Append(ModelConstruct3DHelper.CreateYawPitchRoll(rotation.Angle, rotation.Slope, rotation.Roll).Value);
            matrix.Append(new TranslateTransform3D(pos.X, pos.Y, pos.Z).Value);

            _mainVisualGroup.Transform = new MatrixTransform3D(matrix);
        }

        /// <summary>
        /// Gets the bounding rect3d for this part.
        /// </summary>
        /// <returns></returns>
        public Rect3D GetBounds()
        {
            var bounds = Rect3D.Empty;

            var model = this.Content;
            if (model != null)
            {
                var modelTransform = model.Transform;

                // apply transform
                var transformedBounds = modelTransform.TransformBounds(model.Bounds);
                if (!double.IsNaN(transformedBounds.X))
                {
                    bounds.Union(transformedBounds);
                }
            }

            return bounds;
        }

        /// <summary>
        /// Applies the wireframe model material
        /// </summary>
        protected void ApplyWireframeMaterial()
        {
            //set the wireframe material
            if (_wireframeGeometryModel != null)
            {
                Material wireMaterial;
                if (this.ModelPartData.IsWireframe)
                {
                    wireMaterial = new DiffuseMaterial(Brushes.Black);
                }
                else
                {
                    wireMaterial = new DiffuseMaterial(this.ModelPartData.LineColour.ToSolidBrush());
                }

                _wireframeGeometryModel.Material = wireMaterial;
                //_wireframeGeometryModel.BackMaterial = new DiffuseMaterial(Brushes.Black); //don't set for performance

            }
        }

        /// <summary>
        /// Returns the main geometry models.
        /// Excludes the wireframe and cutouts.
        /// </summary>
        /// <returns></returns>
        protected virtual IEnumerable<GeometryModel3D> GetMainModels()
        {
            foreach (Model3D m in Content.Children)
            {
                if (m != _wireframeGeometryModel)
                {
                    if (m is GeometryModel3D)
                    {
                        yield return (GeometryModel3D)m;
                    }
                }
            }
        }

        /// <summary>
        /// Clears all models
        /// </summary>
        protected virtual void ClearAllModels()
        {
            Content.Children.Clear();
            _wireframeGeometryModel = null;
        }

        /// <summary>
        /// Tiles the given mesh according to the parent 
        /// tile data.
        /// </summary>
        /// <param name="mesh"></param>
        protected void TileModelMesh(MeshGeometry3D mesh)
        {
            IModelConstruct3DData parentData = this.ModelPartData.ParentModelData;
            if (parentData != null && parentData.Data is ITiled)
            {
                Transform3DGroup modelTransform = new Transform3DGroup();
                modelTransform.Children.Add(ModelConstruct3DHelper.CreateYawPitchRoll(parentData.Rotation));

                ITiled tileData = (ITiled)parentData.Data;

                mesh = ModelConstruct3DHelper.CreateTiledBoxMesh(
                    new Size3D(parentData.Size.X, parentData.Size.Y, parentData.Size.Z),
                    mesh,
                    tileData.High, tileData.Wide, tileData.Deep,
                    tileData.HollowStart,
                    modelTransform,
                    0,
                    new Point3D(tileData.SpacingX, tileData.SpacingY, tileData.SpacingZ),
                    tileData.IsSingleUnitRotation);
            }
        }

        protected Boolean EnsureMainModelCount(Int32 count)
        {
            List<GeometryModel3D> models = GetMainModels().ToList();

            if (models.Count() != count)
            {
                while (models.Count() < count)
                {
                    GeometryModel3D newModel = new GeometryModel3D();

                    Content.Children.Add(newModel);
                    models.Add(newModel);
                }
                while (GetMainModels().Count() > count)
                {
                    GeometryModel3D oldModel = models.Last();

                    Content.Children.Remove(oldModel);
                    models.Remove(oldModel);
                }

                return true;
            }

            return false;
        }

        #endregion

        #region IEditableObject Members

        /// <summary>
        /// Flag to indicate if this is currently editing
        /// </summary>
        public Boolean IsEditing { get; private set; }

        public void BeginEdit()
        {
            IsEditing = true;
        }

        public void EndEdit()
        {
            IsEditing = false;

            if (IsGeometryChanged)
            {
                OnGeometryChanged();
            }

            if (IsMaterialChanged)
            {
                OnMaterialChanged();
            }

            if (IsPositionChanged)
            {
                OnPositionChanged();
            }
        }

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            if (!_isDisposed)
            {
                this.ModelPartData.PropertyChanged -= ModelPartData_PropertyChanged;


                _isDisposed = true;
                GC.SuppressFinalize(this);
            }
        }


        #endregion

        #region Static Helpers

        /// <summary>
        /// Applies the given material to the model.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mat"></param>
        protected static void ApplyMaterial(GeometryModel3D model, ObjectMaterial3D mat)
        {
            if (model != null && mat != null)
            {
                Material modelMaterial = null;

                if (mat.ImageData != null)
                {
                    Brush matBrush = new ImageBrush(ModelConstruct3DHelper.GetBitmapImage(mat.ImageData));
                    DiffuseMaterial diffuseMat = new DiffuseMaterial(matBrush);
                    diffuseMat.Freeze();

                    modelMaterial = diffuseMat;
                }
                else
                {
                    Brush matBrush = 
                        ModelConstruct3DHelper.CreateRelativePatternBrush(mat.FillColour.ToSolidBrush(),
                        Brushes.White, mat.FillPattern);
                    DiffuseMaterial diffuseMat = new DiffuseMaterial(matBrush);
                    diffuseMat.Freeze();

                    modelMaterial = diffuseMat;
                }

                //apply to the model
                model.Material = modelMaterial;
            }
        }

        #endregion

    }

}
