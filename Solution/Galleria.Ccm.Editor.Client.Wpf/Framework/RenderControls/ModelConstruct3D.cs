﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Copied and ammended from GFS.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;
using Galleria.Ccm.Editor.Client.Wpf.Framework.RenderData;
using System.Diagnostics;

namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderControls
{
    /// <summary>
    /// ModelVisual3D implementation controlled by ModelConstruct3DData.
    /// </summary>
    public class ModelConstruct3D : ModelVisual3D, IEditableObject, INotifyPropertyChanged, IDisposable
    {
        #region Fields

        private Model3DGroup _model3DContentGroup;
        private IModelConstruct3DData _modelData;

        private readonly ObservableCollection<ModelConstructPart3D> _modelParts = new ObservableCollection<ModelConstructPart3D>();
        private readonly ObservableCollection<ModelLabel3D> _modelLabels = new ObservableCollection<ModelLabel3D>();

        #endregion

        #region Properties

        /// <summary>
        /// Gets the actual model data from which this was created.
        /// </summary>
        public IModelConstruct3DData ModelData
        {
            get { return _modelData; }
        }

        #region IsVisible Property

        public static readonly DependencyProperty IsVisibleProperty =
            DependencyProperty.Register("IsVisible", typeof(Boolean), typeof(ModelConstruct3D),
            new UIPropertyMetadata(true, OnIsVisiblePropertyChanged));

        /// <summary>
        /// Gets/Sets whether this part should be rendered.
        /// </summary>
        protected Boolean IsVisible
        {
            get { return (Boolean)GetValue(IsVisibleProperty); }
            set { SetValue(IsVisibleProperty, value); }
        }

        private static void OnIsVisiblePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((ModelConstruct3D)obj).OnIsVisibleChanged(); ;
        }

        private void OnIsVisibleChanged()
        {
            //ripple the value down to all models and parts
            Boolean isVisible = this.IsVisible;

            foreach (ModelConstructPart3D modelPart in this.ModelParts)
            {
                modelPart.ModelPartData.IsVisible = isVisible;
            }

            foreach (ModelConstruct3D modelChild in this.ModelChildren)
            {
                modelChild.ModelData.IsVisible = isVisible;
            }

            foreach (ModelLabel3D anno in this.Annotations)
            {
                anno.ModelLabelData.IsVisible = isVisible;
            }
        }

        #endregion

        #region IsWireframe Property

        public static readonly DependencyProperty IsWireframeProperty =
            DependencyProperty.Register("IsWireframe", typeof(Boolean), typeof(ModelConstruct3D),
            new UIPropertyMetadata(true, OnIsWireframePropertyChanged));

        /// <summary>
        /// Gets/Sets whether this model and all of its children
        /// should be rendered as wireframe only.
        /// </summary>
        protected Boolean IsWireframe
        {
            get { return (Boolean)GetValue(IsWireframeProperty); }
            set { SetValue(IsWireframeProperty, value); }
        }

        private static void OnIsWireframePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((ModelConstruct3D)obj).OnIsWireframeChanged();
        }

        private void OnIsWireframeChanged()
        {
            //ripple the value down to all models and parts
            Boolean isWire = this.IsWireframe;

            foreach (ModelConstructPart3D modelPart in this.ModelParts)
            {
                modelPart.ModelPartData.IsWireframe = isWire;
            }

            foreach (ModelConstruct3D modelChild in this.ModelChildren)
            {
                modelChild.ModelData.IsWireframe = isWire;
            }

        }

        #endregion

        #region Position Property

        public static readonly DependencyProperty PositionProperty =
            DependencyProperty.Register("Position", typeof(ObjectPoint3D), typeof(ModelConstruct3D),
            new UIPropertyMetadata(new ObjectPoint3D(), OnPositionPropertyChanged));

        /// <summary>
        /// Gets/Sets the position data for this model.
        /// </summary>
        public ObjectPoint3D Position
        {
            get { return (ObjectPoint3D)GetValue(PositionProperty); }
            protected set { SetValue(PositionProperty, value); }
        }

        private static void OnPositionPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((ModelConstruct3D)obj).UpdateModelPosition();
        }

        #endregion

        #region Rotation Property

        public static readonly DependencyProperty RotationProperty =
            DependencyProperty.Register("Rotation", typeof(ObjectRotation3D), typeof(ModelConstruct3D),
            new UIPropertyMetadata(new ObjectRotation3D(), OnRotationPropertyChanged));

        private static void OnRotationPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((ModelConstruct3D)obj).UpdateModelPosition();
        }

        /// <summary>
        /// Gets/Sets the rotation vector
        /// </summary>
        public ObjectRotation3D Rotation
        {
            get { return (ObjectRotation3D)GetValue(RotationProperty); }
            protected set { SetValue(RotationProperty, value); }
        }

        #endregion

        //#region IsCenterPivot Property

        //public static readonly DependencyProperty IsCenterPivotProperty =
        //    DependencyProperty.Register("IsCenterPivot", typeof(Boolean), typeof(ModelConstruct3D),
        //    new UIPropertyMetadata(false, OnIsCenterPivotPropertyChanged));

        //private static void OnIsCenterPivotPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    ((ModelConstruct3D)obj).UpdateModelPosition();
        //}

        ///// <summary>
        ///// Gets/Sets whether rotation should use a center pivot point.
        ///// </summary>
        //public Boolean IsCenterPivot
        //{
        //    get { return (Boolean)GetValue(IsCenterPivotProperty); }
        //    set { SetValue(IsCenterPivotProperty, value); }
        //}

        //#endregion

        #region Size Property

        public static readonly DependencyProperty SizeProperty =
            DependencyProperty.Register("Size", typeof(ObjectSize3D), typeof(ModelConstruct3D),
            new UIPropertyMetadata(new ObjectSize3D(), OnSizePropertyChanged));

        private static void OnSizePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((ModelConstruct3D)obj).UpdateModelPosition();
        }

        /// <summary>
        /// Gets/Sets the size information for this model.
        /// </summary>
        public ObjectSize3D Size
        {
            get { return (ObjectSize3D)GetValue(SizeProperty); }
            private set { SetValue(SizeProperty, value); }
        }

        #endregion

        #region DataProperty

        public static readonly DependencyProperty DataProperty =
            DependencyProperty.Register("Data", typeof(Object), typeof(ModelConstruct3D),
            new UIPropertyMetadata(null, OnDataPropertyChanged));

        private static void OnDataPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            //((ModelConstructPart3D)obj).OnGeometryChanged();
        }

        protected Object Data
        {
            get { return GetValue(DataProperty); }
            set { SetValue(DataProperty, value); }
        }

        #endregion

        #region WorldPosition Property

        public static readonly DependencyProperty WorldPositionProperty =
        DependencyProperty.Register("WorldPosition", typeof(Point3D), typeof(ModelConstruct3D),
         new PropertyMetadata(new Point3D()));

        /// <summary>
        /// Gets the position of this model relative to world space
        /// </summary>
        public Point3D WorldPosition
        {
            get { return (Point3D)GetValue(WorldPositionProperty); }
            private set { SetValue(WorldPositionProperty, value); }
        }

        #endregion

        #region WorldCenter Property

        public static readonly DependencyProperty WorldCenterProperty =
            DependencyProperty.Register("WorldCenter", typeof(Point3D), typeof(ModelConstruct3D),
            new PropertyMetadata(new Point3D()));

        /// <summary>
        /// Gets the center point of this model relative to world space
        /// </summary>
        public Point3D WorldCenter
        {
            get { return (Point3D)GetValue(WorldCenterProperty); }
            private set { SetValue(WorldCenterProperty, value); }
        }

        #endregion

        /// <summary>
        /// Returns the child models held by this.
        /// </summary>
        public ReadOnlyCollection<ModelConstruct3D> ModelChildren
        {
            get { return this.Children.OfType<ModelConstruct3D>().ToList().AsReadOnly(); }
        }

        /// <summary>
        /// Returns the direct model parts held by this.
        /// </summary>
        public ReadOnlyCollection<ModelConstructPart3D> ModelParts
        {
            get
            {
                return _modelParts.ToList().AsReadOnly();
            }
        }

        /// <summary>
        /// Returns the child annotation models held by this.
        /// </summary>
        public ReadOnlyCollection<ModelLabel3D> Annotations
        {
            get
            {
                return _modelLabels.ToList().AsReadOnly();
            }
        }

        /// <summary>
        /// Returns true if the model transform has changed.
        /// </summary>
        protected Boolean IsModelTransformChanged { get; private set; }

        /// <summary>
        /// Returns the collection of direct children 
        /// for this model.
        /// </summary>
        private Model3DCollection Model3DContentChildren
        {
            get
            {
                if (_model3DContentGroup == null)
                {
                    _model3DContentGroup = new Model3DGroup();
                    this.Content = _model3DContentGroup;
                }
                return _model3DContentGroup.Children;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="modelData"></param>
        public ModelConstruct3D(IModelConstruct3DData modelData)
        {
            _modelData = modelData;
            OnModelDataChanged(null, _modelData);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when the model data changes.
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        private void OnModelDataChanged(IModelConstruct3DData oldValue, IModelConstruct3DData newValue)
        {
            if (oldValue != null)
            {
                ((INotifyCollectionChanged)oldValue.ModelChildren).CollectionChanged -= ModelData_ModelChildrenCollectionChanged;
                ((INotifyCollectionChanged)oldValue.ModelParts).CollectionChanged -= ModelData_ModelPartsCollectionChanged;
                ((INotifyCollectionChanged)oldValue.ModelLabels).CollectionChanged -= ModelData_ModelLabelsCollectionChanged;

                //NB- Don't clear bindings for speed.
            }

            if (newValue != null)
            {
                BindingOperations.SetBinding(this, IsVisibleProperty, new Binding("IsVisible") { Source = newValue });
                BindingOperations.SetBinding(this, IsWireframeProperty, new Binding("IsWireframe") { Source = newValue });
                BindingOperations.SetBinding(this, PositionProperty, new Binding("Position") { Source = newValue });
                //BindingOperations.SetBinding(this, IsCenterPivotProperty, new Binding("IsCenterPivot") { Source = newValue });
                BindingOperations.SetBinding(this, RotationProperty, new Binding("Rotation") { Source = newValue });
                BindingOperations.SetBinding(this, SizeProperty, new Binding("Size") { Source = newValue });
                BindingOperations.SetBinding(this, DataProperty, new Binding("Data") { Source = newValue });

                ((INotifyCollectionChanged)newValue.ModelChildren).CollectionChanged += ModelData_ModelChildrenCollectionChanged;
                ((INotifyCollectionChanged)newValue.ModelParts).CollectionChanged += ModelData_ModelPartsCollectionChanged;
                ((INotifyCollectionChanged)newValue.ModelLabels).CollectionChanged += ModelData_ModelLabelsCollectionChanged;
            }

            ClearAllChildren();

            if (newValue != null)
            {
                foreach (IModelConstruct3DData modelData in this.ModelData.ModelChildren)
                {
                    AddChild(new ModelConstruct3D(modelData));
                }

                foreach (IModelConstructPart3DData partData in this.ModelData.ModelParts)
                {
                    AddPart(CreatePart(partData));
                }

                foreach (IModelLabel3DData labelData in this.ModelData.ModelLabels)
                {
                    AddLabel(new ModelLabel3D(labelData));
                }
            }

            UpdateModelPosition();
        }

        /// <summary>
        /// Called when the model data child collection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ModelData_ModelChildrenCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            List<ModelConstruct3D> modelChildren = this.ModelChildren.ToList();

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (IModelConstruct3DData modelData in e.NewItems)
                    {
                        ModelConstruct3D modelChild = modelChildren.FirstOrDefault(m => m.ModelData == modelData);
                        if (modelChild == null)
                        {
                            AddChild(new ModelConstruct3D(modelData));
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (IModelConstruct3DData modelData in e.OldItems)
                    {
                        ModelConstruct3D modelChild = modelChildren.FirstOrDefault(m => m.ModelData == modelData);
                        if (modelChild != null)
                        {
                            RemoveChild(modelChild);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    foreach (ModelConstruct3D modelChild in modelChildren)
                    {
                        RemoveChild(modelChild);
                    }

                    //readd
                    foreach (IModelConstruct3DData modelData in this.ModelData.ModelChildren)
                    {
                        AddChild(new ModelConstruct3D(modelData));
                    }

                    break;
            }

            UpdateModelPosition();
        }

        /// <summary>
        /// Called when the model data part collection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ModelData_ModelPartsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            List<ModelConstructPart3D> modelParts = this.ModelParts.ToList();

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (IModelConstructPart3DData partData in e.NewItems)
                    {
                        ModelConstructPart3D modelChild = modelParts.FirstOrDefault(m => m.ModelPartData == partData);
                        if (modelChild == null)
                        {
                            AddPart(CreatePart(partData));
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (IModelConstructPart3DData partData in e.OldItems)
                    {
                        ModelConstructPart3D modelChild = modelParts.FirstOrDefault(m => m.ModelPartData == partData);
                        if (modelChild != null)
                        {
                            RemovePart(modelChild);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    foreach (ModelConstructPart3D partData in modelParts)
                    {
                        RemovePart(partData);
                    }

                    //readd
                    foreach (IModelConstructPart3DData partData in this.ModelData.ModelParts)
                    {
                        AddPart(CreatePart(partData));
                    }

                    break;
            }

            UpdateModelPosition();
        }

        /// <summary>
        /// Called when the model data label collection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ModelData_ModelLabelsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            List<ModelLabel3D> modelLabels = this.Annotations.ToList();

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (IModelLabel3DData labelData in e.NewItems)
                    {
                        ModelLabel3D modelChild = modelLabels.FirstOrDefault(m => m.ModelLabelData == labelData);
                        if (modelChild == null)
                        {
                            AddLabel(new ModelLabel3D(labelData));
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (IModelLabel3DData labelData in e.OldItems)
                    {
                        ModelLabel3D modelChild = modelLabels.FirstOrDefault(m => m.ModelLabelData == labelData);
                        if (modelChild != null)
                        {
                            RemoveLabel(modelChild);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    foreach (ModelLabel3D labelData in modelLabels)
                    {
                        RemoveLabel(labelData);
                    }

                    //readd
                    foreach (IModelLabel3DData labelData in this.ModelData.ModelLabels)
                    {
                        AddLabel(new ModelLabel3D(labelData));
                    }

                    break;
            }

            UpdateModelPosition();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the position of this model.
        /// </summary>
        public void UpdateModelPosition()
        {
            if (!this.IsEditing)
            {
                Rect3D bounds = GetBounds();

                ObjectPoint3D pos = this.Position;
                ObjectSize3D size = this.Size;
                ObjectRotation3D rotation = this.Rotation;

                //set the transform
                this.Transform = ModelConstruct3DHelper.CreateTransform(rotation, pos);

                //check if we are attached to a viewport.
                Viewport3D vp = HelixToolkit.Wpf.Visual3DHelper.GetViewport3D(this);
                if (vp == null) return;

                ProjectionCamera cam = vp.Camera as ProjectionCamera;
                if (cam == null) return;

                //update the world center point value.
                Visual3D relativeVisual = VisualTreeHelper.GetParent(this) as Visual3D;
                if (relativeVisual == null) relativeVisual = this;

                this.WorldPosition = ModelConstruct3DHelper.ToWorld(pos.ToPoint3D(), relativeVisual);
                this.WorldCenter = ModelConstruct3DHelper.ToWorld(
                    new Point3D(pos.X + (bounds.SizeX / 2), pos.Y + (bounds.SizeY / 2), pos.Z + (bounds.SizeZ / 2)), relativeVisual);

 
                IsModelTransformChanged = false;
            }
            else
            {
                IsModelTransformChanged = true;
            }

        }

        /// <summary>
        /// Gets the bounding Rect3D for this model.
        /// </summary>
        /// <returns></returns>
        public Rect3D GetBounds()
        {
            Rect3D bounds = Rect3D.Empty;

            foreach (ModelConstructPart3D part in this.ModelParts)
            {
                bounds.Union(ModelConstruct3DHelper.FindBounds(part));
            }

            foreach (ModelConstruct3D model in this.ModelChildren)
            {
                bounds.Union(ModelConstruct3DHelper.FindBounds(model));
            }

            return bounds;
        }

        /// <summary>
        /// Returns the child model with the given data
        /// </summary>
        /// <param name="modelData"></param>
        /// <returns></returns>
        public ModelConstruct3D FindModel(IModelConstruct3DData modelData)
        {
            //return this if this is the one..
            if (this.ModelData == modelData) return this;

            //try to find in child models
            foreach (ModelConstruct3D child in this.ModelChildren)
            {
                ModelConstruct3D model = child.FindModel(modelData);
                if (model != null)
                {
                    return model;
                }
            }

            //model not found.
            return null;
        }

        #region Children Methods

        /// <summary>
        /// Returns all child models underneath this one.
        /// </summary>
        /// <returns></returns>
        public List<ModelConstruct3D> GetAllChildModels()
        {
            return GetAllChildModels(this);
        }

        /// <summary>
        /// Returns all child parts underneath this one.
        /// </summary>
        /// <returns></returns>
        public List<ModelConstructPart3D> GetAllParts()
        {
            return GetAllModelParts(this);
        }

        /// <summary>
        /// Adds a child model
        /// </summary>
        /// <param name="child"></param>
        private void AddChild(ModelConstruct3D child)
        {
            this.Children.Add(child);
        }

        /// <summary>
        /// Removes the requested child model
        /// </summary>
        /// <param name="child"></param>
        private Boolean RemoveChild(ModelConstruct3D child)
        {
            Boolean childFound = false;

            if (this.Children.Contains(child))
            {
                this.Children.Remove(child);

                if (child is IDisposable)
                {
                    ((IDisposable)child).Dispose();
                }

                childFound = true;
            }
            else
            {
                //pass the request down
                foreach (var modelChild in this.Children)
                {
                    if (modelChild is ModelConstruct3D)
                    {
                        childFound = ((ModelConstruct3D)modelChild).RemoveChild(child);
                    }

                    if (childFound) break;
                }
            }

            return childFound;
        }

        /// <summary>
        /// Adds a child part
        /// </summary>
        /// <param name="part"></param>
        private void AddPart(ModelConstructPart3D part)
        {
            if (part != null)
            {
                //copy rippled settings
                part.ModelPartData.IsVisible = this.IsVisible;
                part.ModelPartData.IsWireframe = this.IsWireframe;

                //nb - parts are added to the front of the collection and labels to 
                // the end. This is to resolve issues with transparency.

                _modelParts.Add(part);
                this.Model3DContentChildren.Insert(0, part.Content);
            }
        }

        /// <summary>
        /// Removes the requested part
        /// </summary>
        /// <param name="part"></param>
        private void RemovePart(ModelConstructPart3D part)
        {
            _modelParts.Remove(part);

            this.Model3DContentChildren.Remove(part.Content);

            if (part is IDisposable)
            {
                ((IDisposable)part).Dispose();
            }
        }

        /// <summary>
        /// Adds the given label.
        /// </summary>
        /// <param name="label"></param>
        private void AddLabel(ModelLabel3D label)
        {
            //nb - parts are added to the front of the collection and labels to 
            // the end. This is to resolve issues with transparency.

            _modelLabels.Add(label);
            this.Model3DContentChildren.Add(label.Content);

            //this.Children.Add(label);
        }

        /// <summary>
        /// Removes the requested label.
        /// </summary>
        /// <param name="label"></param>
        private void RemoveLabel(ModelLabel3D label)
        {
            _modelLabels.Remove(label);
            this.Model3DContentChildren.Remove(label.Content);

            //this.Children.Remove(label);

            if (label is IDisposable)
            {
                ((IDisposable)label).Dispose();
            }
        }

        /// <summary>
        /// Clears out all child models and parts
        /// ensuring that each one is correctly disposed.
        /// </summary>
        private void ClearAllChildren()
        {
            foreach (ModelConstructPart3D childPart in this.ModelParts)
            {
                RemovePart(childPart);
            }

            foreach (ModelLabel3D label in this.Annotations)
            {
                RemoveLabel(label);
            }

            foreach (ModelConstruct3D childModel in this.ModelChildren)
            {
                RemoveChild(childModel);
            }


            //call dispose on all children
            List<IDisposable> disposables = this.Children.OfType<IDisposable>().ToList();

            if (this.Children.Count > 0)
            {
                this.Children.Clear();
            }

            foreach (var child in disposables)
            {
                child.Dispose();
            }
        }

        #endregion

        #endregion

        #region IEditableObject Members

        /// <summary>
        /// Flag to indicate if this is currently editing
        /// </summary>
        public Boolean IsEditing { get; private set; }

        /// <summary>
        /// Starts a new edit on this model
        /// </summary>
        public void BeginEdit()
        {
            IsEditing = true;
        }

        /// <summary>
        /// Cancels the current edit
        /// </summary>
        public void CancelEdit()
        {
            IsEditing = false;
        }

        /// <summary>
        /// Commits the active edit.
        /// </summary>
        public void EndEdit()
        {
            IsEditing = false;

            if (IsModelTransformChanged)
            {
                UpdateModelPosition();
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(e.Property.Name));
            }
        }

        #endregion

        #region IDisposable Members

        private Boolean IsDisposed;

        public void Dispose()
        {
            if (!IsDisposed)
            {
                OnModelDataChanged(this.ModelData, null);

                IsDisposed = true;
                GC.SuppressFinalize(this);
            }
        }

        #endregion

        #region Static Helpers

        private static readonly Dictionary<Type, Type> InterfaceToPartTypeDict = new Dictionary<Type, Type>()
            {
                {typeof(IBoxModelPart3DData), typeof(BoxModelConstructPart3D)},
                {typeof(IFileModelPart3DData), typeof(FileModelConstructPart3D)},
                {typeof(IBoxMeshPart3DData), typeof(BoxMeshConstructPart3D)},
                 {typeof(IMeshPart3DData), typeof(MeshConstructPart3D)},
            };


        public static void RegisterPartType(Type dataInterfaceType, Type modelconstructPart3DType)
        {
            if(!dataInterfaceType.IsInterface || !dataInterfaceType.GetInterfaces().Contains(typeof(IModelConstructPart3DData)))
            {
                throw new ArgumentOutOfRangeException("Interface should implement IModelConstructPart3DData");
            }
            if(modelconstructPart3DType.IsInterface || !modelconstructPart3DType.IsSubclassOf(typeof(ModelConstructPart3D)))
            {
                throw new ArgumentOutOfRangeException("Interface should inherit from ModelConstructPart3D");
            }

            //register
            InterfaceToPartTypeDict[dataInterfaceType] = modelconstructPart3DType;
        }
        

        /// <summary>
        /// Creates the part control from the given data.
        /// </summary>
        /// <param name="partData"></param>
        /// <returns></returns>
        private static ModelConstructPart3D CreatePart(IModelConstructPart3DData partData)
        {
            ModelConstructPart3D part = null;

            Type[] interfaces = partData.GetType().GetInterfaces();
            foreach (var entry in InterfaceToPartTypeDict)
            {
                if (interfaces.Contains(entry.Key))
                {
                    Type constructType = entry.Value;

                    try
                    {
                        part = Activator.CreateInstance(constructType, partData) as ModelConstructPart3D;
                    }
                    catch (Exception)
                    {
                        Debug.Fail("Failed to created part");
                    }
                    break;
                }
            }

            Debug.Assert(part != null, "Failed to created part");


            return part;
        }

        /// <summary>
        /// Returns all parts for the given model including those belonging to child models
        /// </summary>
        /// <returns></returns>
        private static List<ModelConstructPart3D> GetAllModelParts(ModelConstruct3D model)
        {
            List<ModelConstructPart3D> allParts = new List<ModelConstructPart3D>();
            allParts.AddRange(model.ModelParts);

            foreach (ModelConstruct3D childModel in model.ModelChildren)
            {
                allParts.AddRange(GetAllModelParts(childModel));
            }

            return allParts;
        }

        /// <summary>
        /// Returns all parts for the given model including those belonging to child models
        /// </summary>
        /// <returns></returns>
        private static List<ModelConstruct3D> GetAllChildModels(ModelConstruct3D model)
        {
            List<ModelConstruct3D> allChildren = new List<ModelConstruct3D>();
            allChildren.Add(model);

            foreach (ModelConstruct3D childModel in model.ModelChildren)
            {
                allChildren.AddRange(GetAllChildModels(childModel));
            }

            return allChildren;
        }

        #endregion
    }
    
}
