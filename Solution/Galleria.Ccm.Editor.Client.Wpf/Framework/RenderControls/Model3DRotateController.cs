﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Galleria.Ccm.Editor.Client.Wpf.Framework.DataStructures;
using Galleria.Framework.Model;
using HelixToolkit.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.Framework.RenderControls
{
    public sealed class Model3DRotateController : ModelVisual3D
    {
        #region Fields

        private RotationWheel3D _xAxisWheel;
        private RotationWheel3D _yAxisWheel;
        private RotationWheel3D _zAxisWheel;

        #endregion

        #region Properties

        #region Center Property

        public static readonly DependencyProperty CenterProperty =
            DependencyProperty.Register("Center", typeof(Point3D), typeof(Model3DRotateController),
            new PropertyMetadata(new Point3D(), OnCenterPropertyChanged));

        private static void OnCenterPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
        }

        /// <summary>
        /// Gets/Sets the center position of the manipulator.
        /// This will also be used as the pivot point.
        /// </summary>
        public Point3D Center
        {
            get { return (Point3D)GetValue(CenterProperty); }
            set { SetValue(CenterProperty, value); }
        }

        #endregion

        #region Angle Property

        public static readonly DependencyProperty AngleProperty =
            DependencyProperty.Register("Angle", typeof(Double), typeof(Model3DRotateController),
            new FrameworkPropertyMetadata(0D, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnAnglePropertyChanged));

        private static void OnAnglePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            Model3DRotateController senderControl = (Model3DRotateController)obj;

            //update the target rotation property
            //ObjectRotation3D currRotation = senderControl.TargetRotation;
            //senderControl.TargetRotation =
            //    new ObjectRotation3D()
            //    {
            //        Angle = currRotation.Angle + (Double)e.NewValue,
            //        Slope = currRotation.Slope,
            //        Roll = currRotation.Roll
            //    };

            if (senderControl.AngleChanged != null)
            {
                senderControl.AngleChanged(senderControl, EventArgs.Empty);
            }
        }

        public Double Angle
        {
            get { return (Double)GetValue(AngleProperty); }
            set { SetValue(AngleProperty, value); }
        }

        public event EventHandler AngleChanged;

        #endregion

        #region Slope Property

        public static readonly DependencyProperty SlopeProperty =
            DependencyProperty.Register("Slope", typeof(Double), typeof(Model3DRotateController),
            new FrameworkPropertyMetadata(0D, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnSlopePropertyChanged));

        private static void OnSlopePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            Model3DRotateController senderControl = (Model3DRotateController)obj;

            //update the target rotation property
            //ObjectRotation3D currRotation = senderControl.TargetRotation;
            //senderControl.TargetRotation =
            //    new ObjectRotation3D()
            //    {
            //        Angle = currRotation.Angle,
            //        Slope = currRotation.Slope + (Double)e.NewValue,
            //        Roll = currRotation.Roll
            //    };

            if (senderControl.SlopeChanged != null)
            {
                senderControl.SlopeChanged(senderControl, EventArgs.Empty);
            }
        }

        public Double Slope
        {
            get { return (Double)GetValue(SlopeProperty); }
            set { SetValue(SlopeProperty, value); }
        }

        public event EventHandler SlopeChanged;

        #endregion

        #region Roll Property

        public static readonly DependencyProperty RollProperty =
            DependencyProperty.Register("Roll", typeof(Double), typeof(Model3DRotateController),
            new FrameworkPropertyMetadata(0D, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnRollPropertyChanged));

        private static void OnRollPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            Model3DRotateController senderControl = (Model3DRotateController)obj;

            //update the target rotation property
            //ObjectRotation3D currRotation = senderControl.TargetRotation;
            //senderControl.TargetRotation =
            //    new ObjectRotation3D()
            //    {
            //        Angle = currRotation.Angle,
            //        Slope = currRotation.Slope,
            //        Roll = currRotation.Roll + (Double)e.NewValue
            //    };

            if (senderControl.RollChanged != null)
            {
                senderControl.RollChanged(senderControl, EventArgs.Empty);
            }
        }

        public Double Roll
        {
            get { return (Double)GetValue(RollProperty); }
            set { SetValue(RollProperty, value); }
        }

        public event EventHandler RollChanged;

        #endregion

        #region Diameter Property

        public static readonly DependencyProperty DiameterProperty =
            DependencyProperty.Register("Diameter", typeof(Double), typeof(Model3DRotateController),
            new UIPropertyMetadata(27D, OnDiameterPropertyChanged));

        private static void OnDiameterPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
        }

        public Double Diameter
        {
            get { return (Double)GetValue(DiameterProperty); }
            set { SetValue(DiameterProperty, value); }
        }

        #endregion

        #region InnerDiameter Property

        public static readonly DependencyProperty InnerDiameterProperty =
            DependencyProperty.Register("InnerDiameter", typeof(Double), typeof(Model3DRotateController),
            new UIPropertyMetadata(25D, OnInnerDiameterPropertyChanged));

        private static void OnInnerDiameterPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
        }

        /// <summary>
        /// Gets/Sets the inner diameter of the rotation circle.
        /// </summary>
        public Double InnerDiameter
        {
            get { return (Double)GetValue(InnerDiameterProperty); }
            set { SetValue(InnerDiameterProperty, value); }
        }

        #endregion

        #region Length Property

        public static readonly DependencyProperty LengthProperty =
            DependencyProperty.Register("Length", typeof(Double), typeof(Model3DRotateController),
            new UIPropertyMetadata(1D, OnLengthPropertyChanged));

        private static void OnLengthPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Double Length
        {
            get { return (Double)GetValue(LengthProperty); }
            set { SetValue(LengthProperty, value); }
        }

        #endregion

        #region Visiblity Property

        public static readonly DependencyProperty VisibilityProperty =
            DependencyProperty.Register("Visibility", typeof(Visibility), typeof(Model3DRotateController),
            new UIPropertyMetadata(Visibility.Visible, OnVisibilityPropertyChanged));

        private static void OnVisibilityPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Model3DRotateController)obj).UpdateWheels();
        }

        public Visibility Visibility
        {
            get { return (Visibility)GetValue(VisibilityProperty); }
            set { SetValue(VisibilityProperty, value); }
        }

        #endregion

        #region TargetRotation Property

        //public static readonly DependencyProperty TargetRotationProperty =
        //    DependencyProperty.Register("TargetRotation", typeof(ObjectRotation3D), typeof(Model3DRotateController),
        //    new FrameworkPropertyMetadata(new ObjectRotation3D(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnTargetRotationPropertyChanged));

        //private static void OnTargetRotationPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{ }

        //public ObjectRotation3D TargetRotation
        //{
        //    get { return (ObjectRotation3D)GetValue(TargetRotationProperty); }
        //    set { SetValue(TargetRotationProperty, value); }
        //}

        #endregion

        #region CanRotateX Property

        public static readonly DependencyProperty CanRotateXProperty =
            DependencyProperty.Register("CanRotateX", typeof(Boolean), typeof(Model3DRotateController),
            new UIPropertyMetadata(true, OnCanRotatePropertyChanged));

        private static void OnCanRotatePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((Model3DRotateController)obj).UpdateWheels();
        }

        public Boolean CanRotateX
        {
            get { return (Boolean)GetValue(CanRotateXProperty); }
            set { SetValue(CanRotateXProperty, value); }
        }

        #endregion

        #region CanRotateY Property

        public static readonly DependencyProperty CanRotateYProperty =
            DependencyProperty.Register("CanRotateY", typeof(Boolean), typeof(Model3DRotateController),
            new UIPropertyMetadata(true, OnCanRotatePropertyChanged));

        public Boolean CanRotateY
        {
            get { return (Boolean)GetValue(CanRotateYProperty); }
            set { SetValue(CanRotateYProperty, value); }
        }

        #endregion

        #region CanRotateZ Property

        public static readonly DependencyProperty CanRotateZProperty =
            DependencyProperty.Register("CanRotateZ", typeof(Boolean), typeof(Model3DRotateController),
            new UIPropertyMetadata(true, OnCanRotatePropertyChanged));

        public Boolean CanRotateZ
        {
            get { return (Boolean)GetValue(CanRotateZProperty); }
            set { SetValue(CanRotateZProperty, value); }
        }

        #endregion

        #endregion

        #region Events

        public event EventHandler ValueChangeBeginning;
        private void OnValueChangeBeginning()
        {
            if (ValueChangeBeginning != null)
            {
                ValueChangeBeginning(this, EventArgs.Empty);
            }
        }

        public event EventHandler<EventArgs<Vector3D>> ValueChanged;
        private void OnValueChanged(Vector3D changeVector)
        {
            if (ValueChanged != null)
            {
                ValueChanged(this, new EventArgs<Vector3D>(changeVector));
            }
        }

        public event EventHandler ValueChangeComplete;
        private void OnValueChangeComplete()
        {
            if (ValueChangeComplete != null)
            {
                ValueChangeComplete(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Create a new rotation controller
        /// </summary>
        public Model3DRotateController()
        {
            UpdateWheels();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when a wheel manipulation starts
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Wheel_ValueChangeBeginning(object sender, EventArgs e)
        {
            OnValueChangeBeginning();
        }

        /// <summary>
        /// Called whenever a wheel value changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Wheel_ValueChanged(object sender, EventArgs<Vector3D> e)
        {
            //relay out
            OnValueChanged(e.ReturnValue);
        }

        /// <summary>
        /// Called when a wheel manipulation ends.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Wheel_ValueChangeComplete(object sender, EventArgs e)
        {
            OnValueChangeComplete();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the child wheel controls
        /// </summary>
        private void UpdateWheels()
        {
            Boolean isThisVisible = this.Visibility == System.Windows.Visibility.Visible;

            #region X Axis

            if (isThisVisible && this.CanRotateX && _xAxisWheel == null)
            {
                //X axis wheel
                _xAxisWheel = new RotationWheel3D();
                _xAxisWheel.Colour = Color.FromRgb(150, 75, 75);
                _xAxisWheel.Axis = new Vector3D(1, 0, 0);
                BindingOperations.SetBinding(_xAxisWheel, RotationWheel3D.LengthProperty, new Binding(LengthProperty.Name) { Source = this });
                BindingOperations.SetBinding(_xAxisWheel, RotationWheel3D.DiameterProperty, new Binding(DiameterProperty.Name) { Source = this });
                BindingOperations.SetBinding(_xAxisWheel, RotationWheel3D.InnerDiameterProperty, new Binding(InnerDiameterProperty.Name) { Source = this });
                BindingOperations.SetBinding(_xAxisWheel, RotationWheel3D.PositionProperty, new Binding(CenterProperty.Name) { Source = this });
                BindingOperations.SetBinding(_xAxisWheel, RotationWheel3D.ValueProperty, new Binding(SlopeProperty.Name) { Source = this, Mode = BindingMode.TwoWay, UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged });
                this.Children.Add(_xAxisWheel);

                _xAxisWheel.ValueChangeBeginning += Wheel_ValueChangeBeginning;
                _xAxisWheel.ValueChanged += Wheel_ValueChanged;
                _xAxisWheel.ValueChangeComplete += Wheel_ValueChangeComplete;
            }
            else if (_xAxisWheel != null)
            {
                _xAxisWheel.ValueChangeBeginning -= Wheel_ValueChangeBeginning;
                _xAxisWheel.ValueChanged -= Wheel_ValueChanged;
                _xAxisWheel.ValueChangeComplete -= Wheel_ValueChangeComplete;

                this.Children.Remove(_xAxisWheel);
                _xAxisWheel = null;
            }

            #endregion

            #region Y Axis

            if (isThisVisible && this.CanRotateY && _yAxisWheel == null)
            {
                //y axis wheel
                _yAxisWheel = new RotationWheel3D();
                _yAxisWheel.Colour = Color.FromRgb(75, 150, 75);
                _yAxisWheel.Axis = new Vector3D(0, 1, 0);
                BindingOperations.SetBinding(_yAxisWheel, RotationWheel3D.LengthProperty, new Binding(LengthProperty.Name) { Source = this });
                BindingOperations.SetBinding(_yAxisWheel, RotationWheel3D.DiameterProperty, new Binding(DiameterProperty.Name) { Source = this });
                BindingOperations.SetBinding(_yAxisWheel, RotationWheel3D.InnerDiameterProperty, new Binding(InnerDiameterProperty.Name) { Source = this });
                BindingOperations.SetBinding(_yAxisWheel, RotationWheel3D.PositionProperty, new Binding(CenterProperty.Name) { Source = this });
                BindingOperations.SetBinding(_yAxisWheel, RotationWheel3D.ValueProperty, new Binding(AngleProperty.Name) { Source = this, Mode = BindingMode.TwoWay, UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged });
                this.Children.Add(_yAxisWheel);

                _yAxisWheel.ValueChangeBeginning += Wheel_ValueChangeBeginning;
                _yAxisWheel.ValueChanged += Wheel_ValueChanged;
                _yAxisWheel.ValueChangeComplete += Wheel_ValueChangeComplete;

            }
            else if (_yAxisWheel != null)
            {
                _yAxisWheel.ValueChangeBeginning -= Wheel_ValueChangeBeginning;
                _yAxisWheel.ValueChanged -= Wheel_ValueChanged;
                _yAxisWheel.ValueChangeComplete -= Wheel_ValueChangeComplete;

                this.Children.Remove(_yAxisWheel);
                _yAxisWheel = null;
            }

            #endregion

            #region Z Axis

            if (isThisVisible && this.CanRotateZ && _zAxisWheel == null)
            {
                //z axis wheel
                _zAxisWheel = new RotationWheel3D();
                _zAxisWheel.Colour = Color.FromRgb(75, 75, 150);
                _zAxisWheel.Axis = new Vector3D(0, 0, 1);
                BindingOperations.SetBinding(_zAxisWheel, RotationWheel3D.LengthProperty, new Binding(LengthProperty.Name) { Source = this });
                BindingOperations.SetBinding(_zAxisWheel, RotationWheel3D.DiameterProperty, new Binding(DiameterProperty.Name) { Source = this });
                BindingOperations.SetBinding(_zAxisWheel, RotationWheel3D.InnerDiameterProperty, new Binding(InnerDiameterProperty.Name) { Source = this });
                BindingOperations.SetBinding(_zAxisWheel, RotationWheel3D.PositionProperty, new Binding(CenterProperty.Name) { Source = this });
                BindingOperations.SetBinding(_zAxisWheel, RotationWheel3D.ValueProperty, new Binding(RollProperty.Name) { Source = this, Mode = BindingMode.TwoWay, UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged });
                this.Children.Add(_zAxisWheel);

                _zAxisWheel.ValueChangeBeginning += Wheel_ValueChangeBeginning;
                _zAxisWheel.ValueChanged += Wheel_ValueChanged;
                _zAxisWheel.ValueChangeComplete += Wheel_ValueChangeComplete;

            }
            else if (_zAxisWheel != null)
            {
                _zAxisWheel.ValueChangeBeginning -= Wheel_ValueChangeBeginning;
                _zAxisWheel.ValueChanged -= Wheel_ValueChanged;
                _zAxisWheel.ValueChangeComplete -= Wheel_ValueChangeComplete;

                this.Children.Remove(_zAxisWheel);
                _zAxisWheel = null;
            }

            #endregion


            if (isThisVisible)
            {
                foreach (var child in this.Children)
                {
                    if (child is UIElement3D)
                    {
                        ((UIElement3D)child).Visibility = System.Windows.Visibility.Visible;
                    }
                }
            }
        }

        #endregion
    }

    /// <summary>
    /// A single rotation wheel used as part of the RotationBallVisual3D
    /// </summary>
    public sealed class RotationWheel3D : UIElement3D
    {
        #region Fields

        private GeometryModel3D _model;
        private Viewport3D _parentViewport;
        private ProjectionCamera _camera;
        private Vector3D _hitPlaneNormal;
        private Point3D _lastPoint;

        #endregion

        #region Properties

        #region Colour Property

        public static readonly DependencyProperty ColourProperty =
            DependencyProperty.Register("Colour", typeof(Color), typeof(RotationWheel3D),
            new PropertyMetadata(Colors.Red, OnColourPropertyChanged));

        private static void OnColourPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((RotationWheel3D)obj).OnColourChanged();
        }

        /// <summary>
        /// Gets/Sets the colour of the arrow.
        /// </summary>
        public Color Colour
        {
            get { return (Color)GetValue(ColourProperty); }
            set { SetValue(ColourProperty, value); }
        }

        private void OnColourChanged()
        {
            Color color =
                (this.IsMouseOver) ? Colors.Yellow : this.Colour;

            SolidColorBrush brush = new SolidColorBrush(color);
            brush.Freeze();

            _model.Material = ModelConstruct3DHelper.CreateMaterial(brush);
            _model.BackMaterial = _model.Material;
        }

        #endregion

        #region Position Property

        public static readonly DependencyProperty PositionProperty =
            DependencyProperty.Register("Position", typeof(Point3D), typeof(RotationWheel3D),
            new PropertyMetadata(new Point3D(), OnPositionPropertyChanged));

        private static void OnPositionPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((RotationWheel3D)obj).OnPositionChanged();
        }

        /// <summary>
        /// Gets/Sets the position of the manipulator.
        /// This should be the center point of the model it is attached to.
        /// This will also be used as the pivot point.
        /// </summary>
        public Point3D Position
        {
            get { return (Point3D)GetValue(PositionProperty); }
            set { SetValue(PositionProperty, value); }
        }

        #endregion

        #region Offset Property

        public static readonly DependencyProperty OffsetProperty =
            DependencyProperty.Register("Offset", typeof(Vector3D), typeof(RotationWheel3D),
            new PropertyMetadata(new Vector3D(0, 0, 0), OnOffsetPropertyChanged));

        private static void OnOffsetPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((RotationWheel3D)obj).OnPositionChanged();
        }

        /// <summary>
        /// Gets/Sets the offset of the arrow from the center point.
        /// </summary>
        public Vector3D Offset
        {
            get { return (Vector3D)GetValue(OffsetProperty); }
            set { SetValue(OffsetProperty, value); }
        }

        #endregion

        #region Axis Property

        public static readonly DependencyProperty AxisProperty =
            DependencyProperty.Register("Axis", typeof(Vector3D), typeof(RotationWheel3D),
            new UIPropertyMetadata(new Vector3D(0, 0, 0), OnAxisPropertyChanged));

        private static void OnAxisPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((RotationWheel3D)obj).OnGeometryChanged();
        }

        /// <summary>
        /// Gets/Sets the axis the rotation is to be performed on.
        /// </summary>
        public Vector3D Axis
        {
            get { return (Vector3D)GetValue(AxisProperty); }
            set { SetValue(AxisProperty, value); }
        }

        #endregion

        #region Diameter Property

        public static readonly DependencyProperty DiameterProperty =
            DependencyProperty.Register("Diameter", typeof(Double), typeof(RotationWheel3D),
            new UIPropertyMetadata(27D, OnDiameterPropertyChanged));

        private static void OnDiameterPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((RotationWheel3D)obj).OnGeometryChanged();
        }

        public Double Diameter
        {
            get { return (Double)GetValue(DiameterProperty); }
            set { SetValue(DiameterProperty, value); }
        }

        #endregion

        #region InnerDiameter Property

        public static readonly DependencyProperty InnerDiameterProperty =
            DependencyProperty.Register("InnerDiameter", typeof(Double), typeof(RotationWheel3D),
            new UIPropertyMetadata(25D, OnInnerDiameterPropertyChanged));

        private static void OnInnerDiameterPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((RotationWheel3D)obj).OnGeometryChanged();
        }

        /// <summary>
        /// Gets/Sets the inner diameter of the rotation circle.
        /// </summary>
        public Double InnerDiameter
        {
            get { return (Double)GetValue(InnerDiameterProperty); }
            set { SetValue(InnerDiameterProperty, value); }
        }

        #endregion

        #region Length Property

        public static readonly DependencyProperty LengthProperty =
            DependencyProperty.Register("Length", typeof(Double), typeof(RotationWheel3D),
            new UIPropertyMetadata(1D, OnLengthPropertyChanged));

        private static void OnLengthPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((RotationWheel3D)obj).OnGeometryChanged();
        }

        /// <summary>
        /// 
        /// </summary>
        public Double Length
        {
            get { return (Double)GetValue(LengthProperty); }
            set { SetValue(LengthProperty, value); }
        }

        #endregion

        #region Value

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(Double), typeof(RotationWheel3D),
            new FrameworkPropertyMetadata(0D, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnValuePropertyChanged));

        private static void OnValuePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {

        }

        public Double Value
        {
            get { return (Double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        #endregion

        #endregion

        #region Events

        public event EventHandler ValueChangeBeginning;
        private void OnValueChangeBeginning()
        {
            if (ValueChangeBeginning != null)
            {
                ValueChangeBeginning(this, EventArgs.Empty);
            }
        }

        public event EventHandler<EventArgs<Vector3D>> ValueChanged;
        private void OnValueChanged(Vector3D changeVector)
        {
            if (ValueChanged != null)
            {
                ValueChanged(this, new EventArgs<Vector3D>(changeVector));
            }
        }

        public event EventHandler ValueChangeComplete;
        private void OnValueChangeComplete()
        {
            if (ValueChangeComplete != null)
            {
                ValueChangeComplete(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Constructor

        public RotationWheel3D()
        {
            //There is a wierd bug in the base ms code which causes IsVisible to be set to false.
            // This then stops all mouse events and captures.
            //To fix this the controller must start collapsed and then be made visible after it has
            // been added to the parent collection
            this.Visibility = System.Windows.Visibility.Collapsed;

            _model = new GeometryModel3D();
            this.Visual3DModel = _model;

            OnGeometryChanged();
        }

        #endregion

        #region Event Handlers

        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            OnColourChanged();
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            OnColourChanged();
        }

        /// <summary>
        /// The on mouse down.
        /// </summary>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);

            OnValueChangeBeginning();

            _parentViewport = Visual3DHelper.GetViewport3D(this);
            _camera = _parentViewport.Camera as ProjectionCamera;

            var projectionCamera = _camera;
            if (projectionCamera != null)
            {
                _hitPlaneNormal = projectionCamera.LookDirection;
            }

            this.CaptureMouse();

            var hitPlaneOrigin = this.ToWorld(this.Position);
            var hitPlaneNormal = this.ToWorld(this.Axis);
            var p = e.GetPosition(_parentViewport);

            var hitPoint = this.GetHitPlanePoint(p, hitPlaneOrigin, hitPlaneNormal);
            if (hitPoint != null)
            {
                _lastPoint = this.ToLocal(hitPoint.Value);
            }

            this.CaptureMouse();
        }

        /// <summary>
        /// The on mouse move.
        /// </summary>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            Point3D pivot = this.Position;

            if (this.IsMouseCaptured)
            {
                var hitPlaneOrigin = this.ToWorld(this.Position);
                var hitPlaneNormal = this.ToWorld(this.Axis);

                var position = e.GetPosition(_parentViewport);
                var hitPoint = this.GetHitPlanePoint(position, hitPlaneOrigin, hitPlaneNormal);
                if (hitPoint == null)
                {
                    return;
                }

                var currentPoint = this.ToLocal(hitPoint.Value);

                var v = _lastPoint - this.Position;
                var u = currentPoint - this.Position;

                if (Double.IsNaN(v.X))
                {
                    v = new Vector3D();
                }

                v.Normalize();
                u.Normalize();

                Vector3D currentAxis = Vector3D.CrossProduct(u, v);
                Double sign = -Vector3D.DotProduct(this.Axis, currentAxis);
                Double theta = Math.Sign(sign) * Math.Asin(currentAxis.Length) / Math.PI * 180;

                Double newValue = -(theta / (360f / (2 * Math.PI)));
                this.Value = newValue;



                Double changeVal = newValue;
                Vector3D changeVector = Vector3D.Multiply(Math.Round(changeVal, 2), this.Axis);
                OnValueChanged(changeVector);



                hitPoint = this.GetHitPlanePoint(position, hitPlaneOrigin, hitPlaneNormal);
                if (hitPoint != null)
                {
                    _lastPoint = this.ToLocal(hitPoint.Value);
                }
            }
        }

        /// <summary>
        /// The on mouse up.
        /// </summary>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            this.ReleaseMouseCapture();

            base.OnMouseUp(e);

            this.ReleaseMouseCapture();

            OnValueChangeComplete();
        }

        /// <summary>
        /// Updates the geometry of this model
        /// </summary>
        private void OnGeometryChanged()
        {
            var mb = new MeshBuilder(false, false);
            var p0 = new Point3D(0, 0, 0);

            var d = this.Axis;
            d.Normalize();

            var p1 = p0 - (d * this.Length * 0.5);
            var p2 = p0 + (d * this.Length * 0.5);
            mb.AddPipe(p1, p2, this.InnerDiameter, this.Diameter, 60);

            _model.Geometry = mb.ToMesh();

            if (_model.Material == null)
            {
                OnColourChanged();
            }
        }

        /// <summary>
        /// Updates the transform of this model.
        /// </summary>
        private void OnPositionChanged()
        {
            try
            {
                this.Transform = new TranslateTransform3D(
                    this.Position.X + this.Offset.X,
                    this.Position.Y + this.Offset.Y,
                    this.Position.Z + this.Offset.Z);
            }
            catch (Exception) { }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Transforms from world to local coordinates.
        /// </summary>
        /// <param name="worldPoint">
        /// The point (world coordinates).
        /// </param>
        /// <returns>
        /// Transformed vector (local coordinates).
        /// </returns>
        private Point3D ToLocal(Point3D worldPoint)
        {
            var mat = Visual3DHelper.GetTransform(this);
            mat.Invert();
            var t = new MatrixTransform3D(mat);
            return t.Transform(worldPoint);
        }

        /// <summary>
        /// Transforms from local to world coordinates.
        /// </summary>
        /// <param name="point">
        /// The point (local coordinates).
        /// </param>
        /// <returns>
        /// Transformed point (world coordinates).
        /// </returns>
        private Point3D ToWorld(Point3D point)
        {
            var mat = Visual3DHelper.GetTransform(this);
            var t = new MatrixTransform3D(mat);
            return t.Transform(point);
        }

        /// <summary>
        /// Transforms from local to world coordinates.
        /// </summary>
        /// <param name="vector">
        /// The vector (local coordinates).
        /// </param>
        /// <returns>
        /// Transformed vector (world coordinates).
        /// </returns>
        private Vector3D ToWorld(Vector3D vector)
        {
            var mat = Visual3DHelper.GetTransform(this);
            var t = new MatrixTransform3D(mat);
            return t.Transform(vector);
        }

        /// <summary>
        /// Projects the point on the hit plane.
        /// </summary>
        /// <param name="p">
        /// The p.
        /// </param>
        /// <param name="hitPlaneOrigin">
        /// The hit Plane Origin.
        /// </param>
        /// <param name="hitPlaneNormal">
        /// The hit plane normal (world coordinate system).
        /// </param>
        /// <returns>
        /// The point in world coordinates.
        /// </returns>
        private Point3D? GetHitPlanePoint(Point p, Point3D hitPlaneOrigin, Vector3D hitPlaneNormal)
        {
            return Viewport3DHelper.UnProject(_parentViewport, p, hitPlaneOrigin, hitPlaneNormal);
        }

        #endregion
    }
}
