﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// L.Hodson 
//  Created.
// CCM-25395 : A.Probyn 
//  Added additional properties and code for Product Library
// V8-25797 : A.Silva 
//  Added support for ICustomColumnLayoutView.
// V8-26155 : A.Silva 
//  Modified Column Property to always return a collection of brand new data grid columns.
// V8-26171 : A.Silva 
//  Added CustomColumnManager and removed ICustomColumnLayoutView.
// V8-26329 : A.Silva 
//  Added _columnLayoutManager_CurrentColumnLayoutEdited eventhandler.
// V8-26218 : A.Silva 
//  The Columns Property now returns a collection with all the columns as readonly.
// V8-26370 : A.Silva 
//  Corrected disposing of _columnLayoutManager.
// V8-26472 : A.Probyn 
//  Updated reference to ColumnLayoutManager Constructor
// V8-27087 : L.Ineson
//  Made sure that this uses the viewstate method to get the entity display units.
// V8-27170 : A.Silva ~ Moved the Column Layout Manager to the View.
// V8-27648 : A.Probyn ~ Extended to support ProductLibraryProducts
// V8-28001 : A.Kuszyk
//  Added product clearing events and handlers.
#endregion

#region Version History: (CCM 8.1.0)
// V8-30130 : A.Probyn
//  Extended file data load process to handle any unhandled exceptions correctly.
#endregion

#region Version History: (CCM 8.20)
// V8-30557 : J.Pickup
//  ProductCount helper property has been introduced.
#endregion

#region Version History: (CCM 8.3.0)
//  V8-32154 : A.Probyn 
//      ~ Updated so that custom column layout isn't loaded or shown for product libraries from files.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow.DataManagement;
using Galleria.Ccm.Editor.Client.Wpf.ProductLibraryMaintenance;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Imports.Processes;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Imports;
using Galleria.Framework.Processes;
using Galleria.Framework.ViewModel;
using BackgroundWorker = Csla.Threading.BackgroundWorker;
using File = System.IO.File;
using ProcessHelper = Galleria.Ccm.Imports.Processes.ProcessHelper;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Editor.Client.Wpf.ProductLibrary
{
    /// <summary>
    ///     Viewmodel for the ProductLibraryPanel control.
    /// </summary>
    public sealed class ProductLibraryPanelViewModel : ViewModelAttachedControlObject<ProductLibraryPanel>
    {
        #region Constants

        /// <summary>
        ///     Name used to access the preferred <see cref="CustomColumnLayout" /> for the user in this window.
        /// </summary>
        internal const String ScreenKey = "ProductLibraryPanel";

        internal const CustomColumnLayoutType ColumnLayoutType = CustomColumnLayoutType.Product;

        #endregion

        #region Fields

        private readonly ProductLibraryViewModel _currentProductLibrary;
        private PlanogramViewObjectView _activePlanView;
        private ProductUniverse _currentProductUniverse;

        #region ImportProcess related

        private readonly Stopwatch _timer = new Stopwatch();
        private ModalBusy _busyDialog;
        private Boolean _hasFileMissingException;
        private Boolean _hasIoException;
        private Boolean _hasOutOfMemoryException;
        private Boolean _productLoadInProgress;
        private Int32 _productLoadProgress;

        #endregion

        #endregion

        #region Binding Property Paths

        //Properties

        public static readonly PropertyPath CurrentProductLibraryProperty =
            WpfHelper.GetPropertyPath<ProductLibraryPanelViewModel>(p => p.CurrentProductLibrary);

        public static readonly PropertyPath CurrentProductUniverseProperty =
            WpfHelper.GetPropertyPath<ProductLibraryPanelViewModel>(p => p.CurrentProductUniverse);

        public static readonly PropertyPath ProductLoadInProgressProperty =
            WpfHelper.GetPropertyPath<ProductLibraryPanelViewModel>(p => p.ProductLoadInProgress);

        public static readonly PropertyPath ProductLoadProgressProperty =
            WpfHelper.GetPropertyPath<ProductLibraryPanelViewModel>(p => p.ProductLoadProgress);

        public static readonly PropertyPath ProductsProperty =
            WpfHelper.GetPropertyPath<ProductLibraryPanelViewModel>(p => p.Products);

        public static readonly PropertyPath ProductsCountProperty =
           WpfHelper.GetPropertyPath<ProductLibraryPanelViewModel>(p => p.ProductsCount);

        public static readonly PropertyPath SelectedProductLibraryStatusProperty =
            WpfHelper.GetPropertyPath<ProductLibraryPanelViewModel>(p => p.SelectedProductLibraryStatus);

        #endregion

        #region Properties

        /// <summary>
        ///     Gets/sets the current loaded product library
        /// </summary>
        public ProductLibraryViewModel CurrentProductLibrary
        {
            get { return _currentProductLibrary; }
        }

        /// <summary>
        ///     Gets/sets the current loaded product universe
        /// </summary>
        public ProductUniverse CurrentProductUniverse
        {
            get { return _currentProductUniverse; }
            set
            {
                _currentProductUniverse = value;
                OnCurrentProductUniverseChanged();
                OnPropertyChanged(CurrentProductUniverseProperty);
            }
        }

        /// <summary>
        /// Returns the active plan view
        /// </summary>
        public PlanogramViewObjectView ActivePlanView
        {
            get { return _activePlanView; }
        }

        /// <summary>
        ///     Indiciates where product load in progress
        /// </summary>
        public Boolean ProductLoadInProgress
        {
            get { return _productLoadInProgress; }
            set
            {
                _productLoadInProgress = value;
                OnPropertyChanged(ProductLoadInProgressProperty);
                OnPropertyChanged(SelectedProductLibraryStatusProperty);
            }
        }

        /// <summary>
        ///     Current progress (0-100) of the product data load. (Not update)
        /// </summary>
        public Int32 ProductLoadProgress
        {
            get { return _productLoadProgress; }
            set
            {
                _productLoadProgress = value;
                OnPropertyChanged(ProductLoadProgressProperty);
                OnPropertyChanged(SelectedProductLibraryStatusProperty);
            }
        }

        /// <summary>
        ///     Return the selected product libraries products
        /// </summary>
        public BulkObservableCollection<ProductLibraryProduct> Products
        {
            get { return _currentProductLibrary.Products; }
        }

        /// <summary>
        ///     Return the selected product libraries products
        /// </summary>
        public String ProductsCount
        {
            get { return _currentProductLibrary.Products.Count().ToString(); }
        }

        /// <summary>
        ///     Returns product library status
        /// </summary>
        public String SelectedProductLibraryStatus
        {
            get
            {
                if (CurrentProductLibrary == null && CurrentProductUniverse == null)
                    return Message.Ribbon_ProductLibraryContext_NoSelectedProductLibrary;
                //If in load OR loaded and products exist
                if (ProductLoadInProgress && Products.Count <= 0)
                {
                    return Message.Ribbon_ProductLibraryContext_FileLoading;
                }
                if (ProductLoadInProgress && Products.Count > 0)
                {
                    return String.Format(Message.Ribbon_ProductLibraryContext_SelectedProductLibraryLoadProgress,
                        ProductLoadProgress);
                }
                //If not loading and no products exist
                if (!ProductLoadInProgress && Products.Count <= 0)
                {
                    return Message.Ribbon_ProductLibraryContext_NoProductsCouldBeLoaded;
                }
                return Message.Ribbon_ProductLibraryContext_ProductsLoaded;
            }
        }

        #endregion

        #region Constructor

        public ProductLibraryPanelViewModel()
            : this(App.ViewState.ProductLibraryView, App.ViewState.ActivePlanogramView)
        { }

        /// <summary>
        /// Constructor
        /// </summary>
        public ProductLibraryPanelViewModel(ProductLibraryViewModel productLibraryViewModel, PlanogramViewObjectView activePlanView)
        {
            //Attach to current product library event
            _currentProductLibrary = productLibraryViewModel;
            _currentProductLibrary.ProductsRequireClearing += OnProductsRequireClearing;
            _currentProductLibrary.ModelChanged += CurrentProductLibrary_ModelChanged;

            _activePlanView = activePlanView;
        }
        
        #endregion

        #region Events
        /// <summary>
        /// Fired when Products need to be cleared by the UI.
        /// </summary>
        public event EventHandler ProductsRequireClearing;

        /// <summary>
        /// Fired when Product Library changes and the grid needs a column update.
        /// </summary>
        public event EventHandler ProductLibraryGridRefreshRequired; 
        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when the current product library signals that products need clearing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnProductsRequireClearing(Object sender, EventArgs e)
        {
            if (ProductsRequireClearing != null) ProductsRequireClearing(this, EventArgs.Empty);
        }

        /// <summary>
        /// Called when the current product library file needs the grid columns updating
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnProductLibraryGridRefreshRequired()
        {
            if (ProductLibraryGridRefreshRequired != null) ProductLibraryGridRefreshRequired(this, EventArgs.Empty);
        }

        /// <summary>
        ///     Handler for the current product library changing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CurrentProductLibrary_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<Model.ProductLibrary> e)
        {
            //Trigger current product library changed method
            OnCurrentProductLibraryChanged();

            OnProductLibraryGridRefreshRequired();
        }
        

        #endregion

        #region Methods

        /// <summary>
        ///     Root method to trigger import from specified file
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="worksheet"></param>
        /// <param name="isFirstRowHeaders"></param>
        internal void BeginDataLoad()
        {
            //Clear existing products
            Products.Clear();

            if (CurrentProductLibrary.Model.FileType == ImportDefinitionFileType.Excel)
            {
                String filePath = this.CurrentProductLibrary.DataSourceName;
                if (String.IsNullOrEmpty(filePath))
                {
                    filePath = ProductLibraryHelper.ResolveFilePathFromProductLibraryId(
                       CurrentProductLibrary.Model.Id.ToString());
                }


                //Trigger import from excel
                ImportFromExcelFile(filePath,
                    CurrentProductLibrary.Model.ExcelWorkbookSheet,
                    CurrentProductLibrary.Model.IsFirstRowHeaders);
            }
            else if (CurrentProductLibrary.Model.FileType == ImportDefinitionFileType.Text)
            {
                String filePath = this.CurrentProductLibrary.DataSourceName;
                if (String.IsNullOrEmpty(filePath))
                {
                    filePath = ProductLibraryHelper.ResolveFilePathFromProductLibraryId(
                       CurrentProductLibrary.Model.Id.ToString());
                }

                //Trigger import from text
                ImportFromTextFile(
                    filePath,
                    CurrentProductLibrary.Model.TextDataFormat,
                    CurrentProductLibrary.Model.TextDelimiterType,
                    CurrentProductLibrary.Model.IsFirstRowHeaders);
            }

            UpdateMainPageStatusText();
        }

        /// <summary>
        ///     Root method to trigger load from specified product universe
        /// </summary>
        internal void BeginProductUniverseLoad()
        {
            _timer.Restart();

            //start the import worker
            var importWorker = new BackgroundWorker();
            importWorker.DoWork += FetchProductUniverseProductsWorker_DoWork;
            importWorker.RunWorkerCompleted += FetchProductUniverseProducts_RunWorkerCompleted;
            importWorker.WorkerReportsProgress = false;
            importWorker.WorkerSupportsCancellation = false;

            importWorker.RunWorkerAsync(new object[] { CurrentProductUniverse.Id });

            //show busy
            if (AttachedControl != null)
            {
                _busyDialog = new ModalBusy();
                _busyDialog.Header = Message.ProductLibraryPanel_LoadingProductUniverseProducts;
                _busyDialog.Description = Message.ProductLibraryPanel_LoadingProductUniverseProducts_Description;
                _busyDialog.IsDeterminate = false;
                _busyDialog.Owner = AttachedControl.FindVisualAncestor<ExtendedRibbonWindow>();
                App.ShowWindow(_busyDialog, /*isModal*/true);
            }

            UpdateMainPageStatusText();
        }

        /// <summary>
        ///     Method to return an import mapping list from the selected product librarys mappings
        /// </summary>
        /// <returns></returns>
        private IImportMappingList LoadMappingList()
        {
            //Retrieve mapped columns
            IEnumerable<ProductLibraryColumnMapping> mappedColumnsList
                = CurrentProductLibrary.Model.ColumnMappings.Where(
                    m => m.IsMapped // customer source column was mapped
                         || (m.CanDefault && m.Default != null)); // no source but our destination should be defaulted

            IImportMappingList productMappingList = ProductLibraryImportMappingList.NewProductLibraryImportMappingList();

            //Populate column mappings
            foreach (var mapping in mappedColumnsList)
            {
                ImportMapping importColumnMapping = null;
                importColumnMapping =
                    productMappingList.Where(
                        p => p.PropertyName.ToLowerInvariant() == mapping.Destination.ToLowerInvariant())
                        .FirstOrDefault();

                if (importColumnMapping != null) //fresh definition or could not find mapping
                {
                    importColumnMapping.PropertyDefault = mapping.Default;
                    importColumnMapping.ColumnReference = mapping.Source; //source = customer data column
                    //temporary workaround the fact that the galleria framework has FixedWidthColumnStart noted as nullable
                    if (mapping.TextFixedWidthColumnStart != 0)
                    {
                        importColumnMapping.FixedWidthColumnStart = mapping.TextFixedWidthColumnStart;
                    }
                    else
                    {
                        importColumnMapping.FixedWidthColumnStart = 0;
                    }

                    //temporary workaround the fact that the galleria framework has FixedWidthColumnStart noted as nullable
                    if (mapping.TextFixedWidthColumnEnd != 0)
                    {
                        importColumnMapping.FixedWidthColumnEnd = mapping.TextFixedWidthColumnEnd;
                    }
                    else
                    {
                        importColumnMapping.FixedWidthColumnEnd = 0;
                    }
                }
            }
            return productMappingList;
        }

        /// <summary>
        ///     Method to handle the effects of the currently loaded product library changing
        /// </summary>
        private void OnCurrentProductLibraryChanged()
        {
            if (CurrentProductLibrary != null && CurrentProductLibrary.Model != null)
            {
                //Trigger load of data
                BeginDataLoad();
            }
            else
            {
                //Ensure mainpage view model is cleared too
                if (App.MainPageViewModel.SelectedProductLibrary != null)
                {
                    App.MainPageViewModel.SelectedProductLibrary = null;
                }

                //Clear existing products
                Products.Clear();
            }
        }

        /// <summary>
        ///     Method to handle the effects of the currently loaded product universe changing
        /// </summary>
        private void OnCurrentProductUniverseChanged()
        {
            if (CurrentProductUniverse != null)
            {
                //Clear existing products
                Products.Clear();

                //Begin load
                BeginProductUniverseLoad();
            }
            else
            {
                //Ensure mainpage view model is cleared too
                if (App.MainPageViewModel.SelectedProductUniverseInfo != null)
                {
                    App.MainPageViewModel.SelectedProductUniverseInfo = null;
                }

                //Clear existing products
                Products.Clear();
            }
        }

        /// <summary>
        ///     Method to handle the update of status text - MUST BE CALLED FROM MAIN THREAD
        /// </summary>
        private void UpdateMainPageStatusText()
        {
            if (App.MainPageViewModel != null)
            {
                App.MainPageViewModel.StatusBarText = SelectedProductLibraryStatus;
            }
        }

        #region Product Universe Products Fetch

        /// <summary>
        ///     Work for fetching the product universe products
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FetchProductUniverseProductsWorker_DoWork(Object sender, DoWorkEventArgs e)
        {
            //Get generic args
            var args = (Object[])e.Argument;
            var productUniverseId = (Int32)args[0];

            ProductLoadInProgress = true;

            //Load full fat products
            ProductList productList = ProductList.FetchByProductUniverseId(CurrentProductUniverse.Id);

            //Set result
            e.Result = productList;
        }

        /// <summary>
        ///     Worker completed for fetching the product universe products
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FetchProductUniverseProducts_RunWorkerCompleted(Object sender, RunWorkerCompletedEventArgs e)
        {
            var importWorker = (BackgroundWorker)sender;
            importWorker.DoWork -= FetchProductUniverseProductsWorker_DoWork;
            importWorker.RunWorkerCompleted -= FetchProductUniverseProducts_RunWorkerCompleted;

            Boolean stillInProgress = false;

            if (e.Error == null)
            {
                ProductLoadInProgress = true;

                //Update status
                UpdateMainPageStatusText();

                //Trigger async process to populate all the required product objects
                //Create new worker
                var productDataWorker = new System.ComponentModel.BackgroundWorker();
                productDataWorker.WorkerReportsProgress = true;
                productDataWorker.WorkerSupportsCancellation = true;
                productDataWorker.DoWork += PopulateProductUniverseProductsWorker_DoWork;
                productDataWorker.ProgressChanged += PopulateProductsWorker_ProgressChanged;
                productDataWorker.RunWorkerCompleted += PopulateProductsWorker_RunWorkerCompleted;

                //Run async worker & begin population of products
                productDataWorker.RunWorkerAsync(new[] { e.Result });
            }

            //Hide busy
            if (_busyDialog != null)
            {
                _busyDialog.Close();
                _busyDialog = null;
            }

            ProductLoadInProgress = stillInProgress;

            _timer.Stop();
            Debug.WriteLine("Fetch of product universe data: {0} secs", _timer.Elapsed.TotalSeconds);
        }

        /// <summary>
        ///     Data load worker do work handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PopulateProductUniverseProductsWorker_DoWork(Object sender, DoWorkEventArgs e)
        {
            var worker = (System.ComponentModel.BackgroundWorker)sender;
            var resultdata = new List<Product>();
            var args = (Object[])e.Argument;
            var productList = (ProductList)args[0];

            if (productList != null)
            {
                //Keep track of progress
                Int32 productCount = 0;

                //Get number of valida products
                Int32 totalValidProductcount = productList.Count;

                if (totalValidProductcount > 0)
                {
                    //foreach validated row
                    foreach (var product in productList)
                    {
                        productCount++;

                        //Use the ReportProgress event instead of the OnProductDataChanged event directly
                        //so that we avoid threading ownership issues.
                        worker.ReportProgress((Int32)((Single)productCount / totalValidProductcount * 100),
                            new Object[] { product });
                    }
                }
            }
        }

        #endregion

        #region Import

        /// <summary>
        ///     Root method to trigger import from specified excel file
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="worksheet"></param>
        /// <param name="isFirstRowHeaders"></param>
        internal void ImportFromExcelFile(String filePath, String worksheet, Boolean isFirstRowHeaders)
        {
            _timer.Restart();

            //start the import worker
            var importWorker = new BackgroundWorker();
            importWorker.DoWork += ImportWorker_DoWork;
            importWorker.RunWorkerCompleted += ImportWorker_RunWorkerCompleted;
            importWorker.WorkerReportsProgress = false;
            importWorker.WorkerSupportsCancellation = false;

            importWorker.RunWorkerAsync(new object[] { ImportDefinitionFileType.Excel, filePath, worksheet, isFirstRowHeaders });

            //show busy
            if (AttachedControl != null)
            {
                _busyDialog = new ModalBusy();
                _busyDialog.Header = Message.ImportDefinition_Validation_BusyImportingHeader;
                _busyDialog.Description = Message.ImportDefinition_Validation_BusyImportingDescription;
                _busyDialog.IsDeterminate = false;
                _busyDialog.Owner = AttachedControl.FindVisualAncestor<ExtendedRibbonWindow>();
                App.ShowWindow(_busyDialog, /*isModal*/true);
            }
        }

        /// <summary>
        ///     Root method to trigger impo from specified text file
        /// </summary>
        /// <param name="filePath"></param>
        internal void ImportFromTextFile(String filePath, ImportDefinitionTextDataFormatType? textFormatType,
            ImportDefinitionTextDelimiterType? textDelimiterType, Boolean isFirstRowHeaders)
        {
            _timer.Restart();

            //start the import worker
            var importWorker = new BackgroundWorker();
            importWorker.DoWork += ImportWorker_DoWork;
            importWorker.RunWorkerCompleted += ImportWorker_RunWorkerCompleted;
            importWorker.WorkerReportsProgress = false;
            importWorker.WorkerSupportsCancellation = false;

            importWorker.RunWorkerAsync(new object[] { ImportDefinitionFileType.Text, filePath, textFormatType, textDelimiterType, isFirstRowHeaders });

            //show busy
            if (AttachedControl != null)
            {
                _busyDialog = new ModalBusy();
                _busyDialog.Header = Message.ImportDefinition_Validation_BusyImportingHeader;
                _busyDialog.Description = Message.ImportDefinition_Validation_BusyImportingDescription;
                _busyDialog.IsDeterminate = false;
                _busyDialog.Owner = AttachedControl.FindVisualAncestor<ExtendedRibbonWindow>();
                App.ShowWindow(_busyDialog, /*isModal*/true);
            }
        }

        private void ImportWorker_DoWork(Object sender, DoWorkEventArgs e)
        {
            //Get generic args
            var args = (Object[])e.Argument;
            var fileType = (ImportDefinitionFileType)args[0];
            var filePath = (String)args[1];

            _hasOutOfMemoryException = false;
            _hasIoException = false;
            _hasFileMissingException = false;

            ProductLoadInProgress = true;

            //create this as a new object so that we don't get threading issues
            ImportFileData fileData = ImportFileData.NewImportFileData();
            fileData.SourceFilePath = filePath;

            if (!String.IsNullOrEmpty(filePath))
            {
                //Check file still exists
                if (!File.Exists(filePath))
                {
                    //File doesn't exist, flag for exception to be displayed
                    _hasFileMissingException = true;
                }
                else
                {
                    if (fileType.Equals(ImportDefinitionFileType.Excel))
                    {
                        //Get excel args
                        var worksheet = (String)args[2];
                        var isFirstRowHeaders = (Boolean)args[3];

                        #region Excel Load

                        //Resolve worksheet
                        Int32 worksheetIndex = FileHelper.ResolveWorksheetIndex(filePath, worksheet);

                        //Load data
                        DataTable tableData = FileHelper.LoadSpecificWorksheet(filePath, worksheetIndex,
                             isFirstRowHeaders,
                             ref _hasOutOfMemoryException, ref _hasIoException);

                        if (tableData != null)
                        {
                            //setup columns
                            Int32 colNumber = 1;
                            foreach (DataColumn col in tableData.Columns)
                            {
                                ImportFileDataColumn importColumn =
                                    ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                                importColumn.Header = col.ColumnName;
                                importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                                fileData.Columns.Add(importColumn);

                                colNumber++;
                            }

                            //setup rows
                            Int32 rowNumber = 1;
                            foreach (DataRow row in tableData.Rows)
                            {
                                ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);
                                Int32 rowItemArrayCount = row.ItemArray.Count();

                                //cycle through adding cells
                                for (Int32 cellIndex = 0; cellIndex < rowItemArrayCount; cellIndex++)
                                {
                                    ImportFileDataCell importCell =
                                        ImportFileDataCell.NewImportFileDataCell(cellIndex + 1, row.ItemArray[cellIndex]);
                                    importRow.Cells.Add(importCell);
                                }

                                // through adding rows
                                fileData.Rows.Add(importRow);

                                rowNumber++;
                            }
                        }

                        #endregion
                    }
                    else if (fileType.Equals(ImportDefinitionFileType.Text))
                    {
                        //Get  text args
                        var textFormatType = (ImportDefinitionTextDataFormatType?)args[2];
                        var textDelimiterType = (ImportDefinitionTextDelimiterType?)args[3];
                        var isFirstRowHeaders = (Boolean)args[4];

                        #region Text Load

                        var rawData = new ObservableCollection<string>();
                        fileData.MappingList = LoadMappingList();

                        Boolean isFixedWidth = textFormatType.HasValue &&
                                               textFormatType.Value == ImportDefinitionTextDataFormatType.Fixed;

                        //Start reading the file line by line adding the data rows, using the current character encoding
                        if (File.Exists(filePath))
                        {
                            //attempt to detect file encoding
                            Encoding currentEncoding = FileHelper.DetectFileEncoding(filePath);

                            var reader = new StreamReader(filePath, currentEncoding);
                            while (!reader.EndOfStream)
                            {
                                if (!reader.EndOfStream)
                                {
                                    rawData.Add(reader.ReadLine());
                                }
                                else
                                {
                                    break;
                                }
                            }
                            reader.Close();

                            //get the delimiter if required 
                            Char[] delimiter = null;
                            if (textDelimiterType != null)
                            {
                                delimiter = (!isFixedWidth && textDelimiterType.HasValue)
                                    ? FileHelper.GetDelimiterString(textDelimiterType.Value).ToCharArray()
                                    : null;
                            }

                            //cycle through the rows
                            for (Int32 i = 0; i < rawData.Count; i++)
                            {
                                String row = rawData[i];

                                if (String.IsNullOrEmpty(row))
                                {
                                    //skip this item if the row is null or empty.
                                    continue;
                                }

                                //split the string into the defined columns
                                var splitLine = new List<String>();
                                if (isFixedWidth)
                                {
                                    Int32 lastPoint = 0;
                                    var fixedWidthLines = new List<ImportFixedLineDef>();
                                    foreach (var mapping in fileData.MappingList.Where(p => p.IsColumnReferenceSet))
                                    {
                                        if (mapping.FixedWidthColumnEnd.HasValue)
                                        {
                                            fixedWidthLines.Add(new ImportFixedLineDef(mapping.FixedWidthColumnEnd.Value));
                                        }
                                    }

                                    foreach (var lineDef in fixedWidthLines.OrderBy(l => l.LineValue))
                                    {
                                        //The start and end of the substring cannot be greater than the string length
                                        Int32 startIndex = Math.Min(row.Length, lastPoint);
                                        Int32 length = Math.Min(Math.Max(row.Length - lastPoint, 0),
                                            lineDef.LineValue - lastPoint);

                                        String colString = row.Substring(startIndex, length);
                                        splitLine.Add(colString);
                                        lastPoint = lineDef.LineValue;
                                    }
                                }
                                else
                                {
                                    if (delimiter != null)
                                    {
                                        splitLine.AddRange(row.Split(delimiter));
                                    }
                                }

                                //create a new import data row
                                ImportFileDataRow dataRow = ImportFileDataRow.NewImportFileDataRow(i + 1);

                                Int32 colNumber = 1;
                                foreach (var colString in splitLine)
                                {
                                    String cellValue = colString.TrimEnd(' ');

                                    //check if we have a column, if not then add
                                    if (fileData.Columns.Count < colNumber)
                                    {
                                        ImportFileDataColumn column =
                                            ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                                        column.Header = String.Format(Message.ImportDefinition_ImportColumnName,
                                            colNumber);
                                        column.CellAlignment = ImportFileDataHorizontalAlignment.Left;

                                        fileData.Columns.Add(column);
                                    }

                                    //add the cell data
                                    dataRow.Cells.Add(ImportFileDataCell.NewImportFileDataCell(colNumber, cellValue));

                                    colNumber++;
                                }

                                fileData.Rows.Add(dataRow);
                            }

                            //If first row column headers
                            if (isFirstRowHeaders)
                            {
                                //Get first row containing headers
                                ImportFileDataRow headerRow = fileData.Rows[0];

                                //Remove from the main dataset
                                fileData.Rows.RemoveAt(0);

                                //Manually update column headers
                                foreach (var colDef in fileData.Columns)
                                {
                                    if (headerRow[colDef.ColumnNumber] != null)
                                    {
                                        colDef.Header = headerRow[colDef.ColumnNumber].Value.ToString();
                                    }
                                }
                            }
                        }

                        #endregion
                    }
                }
            }
            e.Result = fileData;
        }

        private void ImportWorker_RunWorkerCompleted(Object sender, RunWorkerCompletedEventArgs e)
        {
            var importWorker = (BackgroundWorker)sender;
            importWorker.DoWork -= ImportWorker_DoWork;
            importWorker.RunWorkerCompleted -= ImportWorker_RunWorkerCompleted;

            Boolean stillInProgress = false;
            if (_hasFileMissingException)
            {
                //Hide busy
                if (_busyDialog != null)
                {
                    _busyDialog.Close();
                    _busyDialog = null;
                }

                CommonHelper.GetWindowService()
                    .ShowOkMessage(Ccm.Common.Wpf.Services.MessageWindowType.Error,
                    Message.ImportDefinition_ImportTitle_FileMissing,
                    Message.ImportDefinition_FileMissing);
            }
            else if (_hasOutOfMemoryException)
            {
                //Hide busy
                if (_busyDialog != null)
                {
                    _busyDialog.Close();
                    _busyDialog = null;
                }


                CommonHelper.GetWindowService()
                   .ShowOkMessage(Ccm.Common.Wpf.Services.MessageWindowType.Error,
                   Message.ImportDefinition_ImportTitle_FileTooLarge,
                   Message.ImportDefinition_FileTooLarge);
            }
            else if (_hasIoException)
            {
                //Hide busy 
                if (_busyDialog != null)
                {
                    _busyDialog.Close();
                    _busyDialog = null;
                }

                CommonHelper.GetWindowService()
                   .ShowOkMessage(Ccm.Common.Wpf.Services.MessageWindowType.Error,
                   Message.ImportDefinition_ImportTitle,
                   Message.ImportDefinition_FileInUse);
            }
            else if (e.Error != null)
            { 
                //Hide busy 
                if (_busyDialog != null)
                {
                    _busyDialog.Close();
                    _busyDialog = null;
                }

                //If any other exception is detected which is not handled
                CommonHelper.GetWindowService()
                   .ShowOkMessage(Ccm.Common.Wpf.Services.MessageWindowType.Error,
                   Message.ImportDefinition_ImportTitle_ImportFailed,
                   Message.ImportDefinition_ImportTitle_ImportFailedDescription);
            }
            else
            {
                //The file data was imported so we can continue
                stillInProgress = true;

                ImportFileData fileData = (ImportFileData)e.Result;

                fileData.MappingList = LoadMappingList();
                if (String.IsNullOrEmpty(fileData.SourceFilePath))
                {
                    fileData.SourceFilePath =
                        ProductLibraryHelper.ResolveFilePathFromProductLibraryId(CurrentProductLibrary.Model.Id);
                    if (String.IsNullOrEmpty(fileData.SourceFilePath))
                    {
                        fileData.SourceFilePath = App.ViewState.ProductLibraryView.DataSourceName;
                    }
                }

                ProductLoadInProgress = stillInProgress;

                //Begin Validation
                ValidateFile(fileData, CurrentProductLibrary.Model.StartAtRow,
                    CurrentProductLibrary.Model.IsFirstRowHeaders);
            }

            if (_busyDialog != null)
            {
                _busyDialog.Close();
                _busyDialog = null;
            }

            ProductLoadInProgress = stillInProgress;

            _timer.Stop();
            Debug.WriteLine("Import from Excel: {0} secs", _timer.Elapsed.TotalSeconds);
        }

        #endregion

        #region Validate

        /// <summary>
        ///     Root method to trigger validation of loaded file data
        /// </summary>
        /// <param name="fileData"></param>
        /// <param name="startRow"></param>
        /// <param name="isHeadersInFirstRow"></param>
        internal void ValidateFile(ImportFileData fileData, Int32 startRow, Boolean isHeadersInFirstRow)
        {
            _timer.Restart();

            UpdateMainPageStatusText();

            fileData.RaiseChangeEvents = false;

            var validateProcessFactory = new ProcessFactory<ValidateProductLibraryProcess>();
            validateProcessFactory.ProcessCompleted += ValidateFileProcessCompleted;
            validateProcessFactory.Execute(new ValidateProductLibraryProcess(1, fileData, startRow, isHeadersInFirstRow));

            //Ensure busy dialog is closed
            if (_busyDialog != null)
            {
                _busyDialog.Close();
                _busyDialog = null;
            }

            //show busy
            if (AttachedControl != null)
            {
                _busyDialog = new ModalBusy();
                _busyDialog.Header = Message.ProductLibraryPanel_Validating;
                _busyDialog.Description = Message.ProductLibraryPanel_Validating_Desc;
                _busyDialog.IsDeterminate = false;
                _busyDialog.Owner = AttachedControl.FindVisualAncestor<ExtendedRibbonWindow>();
                App.ShowWindow(_busyDialog, /*isModal*/true);
            }
        }

        private void ValidateFileProcessCompleted<T>(object sender, ProcessCompletedEventArgs<T> e)
            where T : Imports.Processes.ValidateProcessBase<T>
        {
            // unsubscribe events
            var processFactory = (ProcessFactory<T>)sender;
            processFactory.ProcessCompleted -= ValidateFileProcessCompleted;

            Boolean stillInProgress = false;
            if (e.Error != null)
            {
                if (AttachedControl != null)
                {
                    var errorWindow = new ModalMessage();
                    errorWindow.Description = Message.DataManagement_Validation_FailMessage;
                    errorWindow.Header = Message.DataManagement_Validation_FailHeader;
                    errorWindow.Button1Content = Message.Generic_OK;
                    errorWindow.ButtonCount = 1;
                    errorWindow.MessageIcon = ImageResources.Error_32;
                    App.ShowWindow(errorWindow, true);
                }
            }
            else
            {
                var validationViewModel = new ImportDataValidationViewModel();

                //Load results into validation view model
                validationViewModel.LoadProcessResults(e);

                stillInProgress = true;

                //Any issues?
                Int32 issueCount = validationViewModel.WorksheetErrors.SelectMany(f => f.Errors).Count();
                Boolean canContinue = true;
                if (issueCount != 0 && e.Process.ValidatedData.GetValidRows().Any())
                {
                    //Show model validation window
                    var win = new ProductLibraryValidationWindow(validationViewModel);
                    App.ShowWindow(win, true);

                    canContinue = win.DialogResult.HasValue ? win.DialogResult.Value : false;
                }

                validationViewModel.ValidatedFileData.RaiseChangeEvents = true;

                //Ensure all errors have been resolved
                if (canContinue &&
                    validationViewModel.WorksheetErrors.SelectMany(p => p.Errors)
                        .Count(p => p.ErrorType == ValidationErrorType.Error) <= 0)
                {
                    //Start population of product objects async with validated data
                    PopulateProducts(validationViewModel.ValidatedFileData);
                }
                else
                {
                    //Clear selected product library
                    CurrentProductLibrary.Model = null;
                    //Trigger current product library changed method
                    OnCurrentProductLibraryChanged();
                    stillInProgress = false;
                }

                //Clear view model to reduce memory
                validationViewModel.Dispose();
                validationViewModel = null;
            }

            ProductLoadInProgress = stillInProgress;

            //Hide busy
            if (_busyDialog != null)
            {
                _busyDialog.Close();
                _busyDialog = null;
            }

            _timer.Stop();
            Debug.WriteLine("Validation: {0} secs", _timer.Elapsed.TotalSeconds);
        }

        #endregion

        #region PopulateProducts

        /// <summary>
        ///     Root method to populate products from validation file data
        /// </summary>
        /// <param name="fileData"></param>
        internal void PopulateProducts(ImportFileData validatedFileData)
        {
            UpdateMainPageStatusText();

            //Trigger async process to populate all the required product objects
            //Create new worker
            var productDataWorker = new System.ComponentModel.BackgroundWorker();
            productDataWorker.WorkerReportsProgress = true;
            productDataWorker.WorkerSupportsCancellation = true;
            productDataWorker.DoWork += PopulateProductsWorker_DoWork;
            productDataWorker.ProgressChanged += PopulateProductsWorker_ProgressChanged;
            productDataWorker.RunWorkerCompleted += PopulateProductsWorker_RunWorkerCompleted;

            //Run async worker
            productDataWorker.RunWorkerAsync(new Object[] { validatedFileData });
        }

        /// <summary>
        ///     Data load worker do work handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PopulateProductsWorker_DoWork(Object sender, DoWorkEventArgs e)
        {
            var worker = (System.ComponentModel.BackgroundWorker)sender;
            var resultdata = new List<Product>();
            var args = (Object[])e.Argument;
            var validatedFileData = (ImportFileData)args[0];

            if (validatedFileData != null)
            {
                //Keep track of progress
                Int32 productCount = 0;

                //get a list of all mappings that need to be processed
                IEnumerable<ImportMapping> resolvedMappings =
                    validatedFileData.MappingList.Where(m => m.IsColumnReferenceSet);

                //Get number of valida products
                Int32 totalValidProductcount = validatedFileData.GetValidRows().Count();

                if (totalValidProductcount > 0)
                {
                    //foreach validated row
                    foreach (var row in validatedFileData.GetValidRows())
                    {
                        productCount++;
                        
                        //Create product
                        Product product = Product.NewProduct();
                        
                        //Create product library product
                        ProductLibraryProduct libProduct = ProductLibraryProduct.NewProductLibraryProduct(product);

                        //Enumerate through resolved mappings
                        foreach (var mapping in resolvedMappings)
                        {
                            Int32 columnNumber = validatedFileData.GetMappedColNumber(mapping);

                            //GFS-24338 : Handle case when column not present in file but exists in template.
                            Object cellValue = null;
                            if (columnNumber > 0)
                            {
                                cellValue = row[columnNumber].CurrentValue;
                            }

                            // [GFS-13815] Corrected null value checks
                            cellValue = ProcessHelper.CheckNullCellValues(mapping, cellValue);

                            //use the mapping list method to set the value
                            ProductLibraryImportMappingList.SetValueByMappingId(mapping.PropertyIdentifier, cellValue, libProduct);
                        }

                        //Use the ReportProgress event instead of the OnProductDataChanged event directly
                        //so that we avoid threading ownership issues.
                        worker.ReportProgress((Int32)((Single)productCount / totalValidProductcount * 100),
                            new Object[] { libProduct });
                    }
                }
            }
        }

        /// <summary>
        ///     PopulateProductsWorker progress changed event is used privatly and interfaced with as
        ///     a OnProductDataAdded event so that we don't have to expose the worker itself.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PopulateProductsWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            ProductLoadProgress = e.ProgressPercentage;

            var args = (Object[])e.UserState;
            if (args[0] is ProductLibraryProduct)
            {
                //Create product library product
                ProductLibraryProduct productToAdd = (ProductLibraryProduct)args[0];

                //add the product 
                if (!Products.Contains(productToAdd))
                {
                    Products.Add(productToAdd);
                }
            }
            else if (args[0] is Product)
            {
                //Create product library product
                Product productToAdd = (Product)args[0];
                ProductLibraryProduct libProduct = ProductLibraryProduct.NewProductLibraryProduct(productToAdd);

                //add the product 
                if (!Products.Contains(libProduct))
                {
                    Products.Add(libProduct);
                }
            }

            OnPropertyChanged(ProductsCountProperty);
            UpdateMainPageStatusText();
        }

        /// <summary>
        ///     Completed event for the product data compilation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PopulateProductsWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!e.Cancelled)
            {
                //Products wil have asynchronously been loaded

                //Clear loading cursor in toolbar
                ProductLoadInProgress = false;

                UpdateMainPageStatusText();
            }
        }

        #endregion

        #endregion

        #region IDisposable override

        protected override void Dispose(Boolean disposing)
        {
            if (IsDisposed) return;

            _currentProductLibrary.ProductsRequireClearing -= OnProductsRequireClearing;
            _currentProductLibrary.ModelChanged -= CurrentProductLibrary_ModelChanged;

            IsDisposed = true;
        }

        #endregion


    }
}