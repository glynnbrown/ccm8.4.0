﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// L.Hodson ~ Created.
// CCM-25395 : A.Probyn 
//  Added additional properties and code for Product Library
// V8-25863 : A.Probyn
//      Set default placement to be top
// V8-25797 : A.Silva ~ removed the column set generation code for the data grid. Now it is the view model the one in charge of that.
// V8-26155 : A.Silva ~ When Dispose is called it no longer disposes the viewmodel as it is shared across every instance.
// V8-26171 : A.Silva ~ Removed references to the old Custom Column Layout Editor.
// V8-27170 : A.Silva ~ Moved in the Column Layout Manager from the View Model.
// V8-27648 : A.Probyn ~ Updated to support ProductLibraryProduct
// V8-28001 : A.Kuszyk
//  Added Events and Handlers for repository changing events that signal that products need clearing.
// V8-28040 : A.Probyn
//  ~ Added code to call ApplyFiltersIntoGrid after column set is applied to the grid.
#endregion
#region Version History: (CCM 8.2.0)
//V8-30958 : L.Ineson
//  Updated after column manager changes.
#endregion
#region Version History: (CCM 8.3.0)
//  V8-32154 : A.Probyn 
//      ~ Updated so that custom column layout isn't loaded or shown for product libraries from files.
//      ~ Fixed it so product libraries from files create columns using the associated property info.
#endregion
#endregion

using System;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.PlanRepository;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using System.Windows.Controls;
using Galleria.Ccm.Imports.Mappings;
using System.Globalization;
using Galleria.Framework.Imports;
using Galleria.Framework.ViewModel;
using System.Collections.Generic;
using System.Windows.Input;

namespace Galleria.Ccm.Editor.Client.Wpf.ProductLibrary
{
    /// <summary>
    ///     Interaction logic for ProductLibraryPanel.xaml
    /// </summary>
    public sealed partial class ProductLibraryPanel : IDisposable
    {
        #region Fields

        /// <summary>
        ///     The <see cref="ColumnLayoutManager"/> in charge of the Product Grid.
        /// </summary>
        private ColumnLayoutManager _columnLayoutManager;

        /// <summary>
        ///     The current set of columns to be displayed on the Product Grid.
        /// </summary>
        private readonly DataGridColumnCollection _columnSet = new DataGridColumnCollection();

        #endregion

        #region Properties


        #region ViewModel Property

        /// <summary>
        ///     The <see cref="DependencyProperty"/> for the <see cref="ViewModel"/> property.
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof (ProductLibraryPanelViewModel), typeof (ProductLibraryPanel),
                new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        ///     Gets or sets the current view model for this view.
        /// </summary>
        public ProductLibraryPanelViewModel ViewModel
        {
            get { return (ProductLibraryPanelViewModel) GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        /// <summary>
        ///     Invoked whenever this view's view model changes.
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = (ProductLibraryPanel) obj;

            if (e.OldValue != null)
            {
                ProductLibraryPanelViewModel oldModel = (ProductLibraryPanelViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.ProductsRequireClearing -= senderControl.OnProductsRequireClearing;
                oldModel.ProductLibraryGridRefreshRequired -= senderControl.ViewModel_ProductLibraryGridRefreshRequired;
            }

            if (e.NewValue != null)
            {
                ProductLibraryPanelViewModel newModel = (ProductLibraryPanelViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.ProductsRequireClearing += senderControl.OnProductsRequireClearing;
                newModel.ProductLibraryGridRefreshRequired += senderControl.ViewModel_ProductLibraryGridRefreshRequired;
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        ///     Constructor
        /// </summary>
        public ProductLibraryPanel(ProductLibraryPanelViewModel viewModel)
        {
            this.Title = Message.ProductLibrary_PanelTitle;

            InitializeComponent();

            this.ViewModel = viewModel;

            Loaded += ProductLibraryPanel_FirstTimeLoaded;
        }

        #endregion

        #region Methods

        /// <summary>
        /// A delegate to clear products from the view model.
        /// </summary>
        private delegate void ClearProducts();
        
        /// <summary>
        /// Called when the view model signals that the UI needs to clear products.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnProductsRequireClearing(Object sender, EventArgs e)
        {
            ClearProducts clearProducts = () => 
                {
                    App.MainPageViewModel.SelectedProductLibrary = null;
                    ViewModel.CurrentProductUniverse = null;
                };
            this.Dispatcher.Invoke(clearProducts, null);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        ///     First time loaded event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductLibraryPanel_FirstTimeLoaded(object sender, RoutedEventArgs e)
        {
            Loaded -= ProductLibraryPanel_FirstTimeLoaded;

            //Initialize the custom column manager.
            var uomCollection = (App.ViewState.Settings.Model != null) ?
                DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.Settings.Model) : DisplayUnitOfMeasureCollection.Empty;

            _columnLayoutManager = new ColumnLayoutManager(
                new ProductColumnLayoutFactory(typeof(ProductLibraryProduct)),
                uomCollection,
                ProductLibraryPanelViewModel.ScreenKey);

            _columnLayoutManager.ColumnSetChanging += ColumnLayoutManager_ColumnSetChanging;
            
            //If default on load is a product libraryy
            if (this.ViewModel.CurrentProductLibrary.Model != null)
            {
                //Load grid with columns depending on what is loaded on default
                ViewModel_ProductLibraryGridRefreshRequired(this, new EventArgs());
            }

            _columnLayoutManager.AttachDataGrid(this.xProductLibraryProductUniversesGrid);
        }

        private void ColumnLayoutManager_ColumnSetChanging(object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            //strip out metadata columns as they are not valid for the universe.
            foreach (DataGridColumn column in columnSet.ToList())
            {
                if (ExtendedDataGrid.GetBindingProperty(column).StartsWith("Meta"))
                    columnSet.Remove(column);
            }
        }


        private void ViewModel_ProductLibraryGridRefreshRequired(object sender, EventArgs e)
        {
            //Load the mapped template into the grid
            this.xProductLibraryProductLibraryGrid.Columns.Clear();

            if (this.ViewModel.CurrentProductLibrary != null && this.ViewModel.CurrentProductLibrary.Model != null)
            {
                ProductLibraryImportMappingList mappingList = ProductLibraryImportMappingList.NewProductLibraryImportMappingList();
                IEnumerable<ObjectFieldInfo> fields = _columnLayoutManager.LayoutFactory.GetModelObjectFields();

                foreach (ProductLibraryColumnMapping column in this.ViewModel.CurrentProductLibrary.Model.ColumnMappings.Where(p => p.IsMapped))
                {
                    //Apply any required unit of measure to headers
                    String destinationText = String.Format(CultureInfo.CurrentCulture, column.Destination);

                    //Find property path
                    ImportMapping mapping = mappingList.FirstOrDefault(p => p.PropertyName.ToUpperInvariant() == destinationText.ToUpperInvariant());

                    String header = (mapping != null) ? mapping.PropertyName : destinationText;

                    //Get column group name
                    String columnGroupName = ProductLibraryImportMappingList.GetDestinationColumnGroupName(column.Destination);

                    ObjectFieldInfo fieldInfo = fields.FirstOrDefault(p => p.PropertyName.Equals(mappingList.GetBindingPath(mapping.PropertyIdentifier)));
                    if (mapping != null && fieldInfo != null)
                    {
                        //Create grid column
                        DataGridColumn gridColumn = CommonHelper.CreateReadOnlyColumn(fieldInfo, App.ViewState.DisplayUnits, columnGroup: columnGroupName, isVisible: true);
                        gridColumn.Header = header;

                        //Add to grid
                        this.xProductLibraryProductLibraryGrid.Columns.Add(gridColumn);
                    }
                }
            }
        }


        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            if (!_isDisposed)
            {
                ViewModel.ProductsRequireClearing -= OnProductsRequireClearing;
                ViewModel = null;

                if (_columnLayoutManager != null)
                {
                    _columnLayoutManager.ColumnSetChanging -= ColumnLayoutManager_ColumnSetChanging;
                    _columnLayoutManager.Dispose();
                    _columnLayoutManager = null;
                }

                GC.SuppressFinalize(this);

                _isDisposed = true;
            }
        }

        #endregion

    }
}