﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25395 : A.Probyn 
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow.DataManagement;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.ProductLibrary
{
    /// <summary>
    /// Interaction logic for ProductLibraryValidationWindow.xaml
    /// </summary>
    public partial class ProductLibraryValidationWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ProductLibraryValidationViewModel), typeof(ProductLibraryValidationWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ProductLibraryValidationWindow senderControl = (ProductLibraryValidationWindow)obj;

            if (e.OldValue != null)
            {
                ProductLibraryValidationViewModel oldModel = (ProductLibraryValidationViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                ProductLibraryValidationViewModel newModel = (ProductLibraryValidationViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        /// <summary>
        /// Returns the viewmodel for this control
        /// </summary>
        public ProductLibraryValidationViewModel ViewModel
        {
            get { return (ProductLibraryValidationViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public ProductLibraryValidationWindow(ImportDataValidationViewModel importDataViewModel)
        {
            this.ViewModel = new ProductLibraryValidationViewModel(importDataViewModel);

            InitializeComponent();

            this.contentArea.Children.Add(new ImportDataValidation(importDataViewModel));
        }

        #endregion

        /// <summary>
        /// Disposes of the viewmodel on close
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    if (this.ViewModel != null)
                    {
                        ProductLibraryValidationViewModel oldModel = this.ViewModel;
                        this.ViewModel = null;
                        if (oldModel != null)
                        {
                            oldModel.Dispose();
                        }
                    }

                }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }
    }
}
