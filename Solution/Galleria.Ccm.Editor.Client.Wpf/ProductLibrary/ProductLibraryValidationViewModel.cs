﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25395 : A.Probyn 
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow.DataManagement;
using System.Windows;
using Galleria.Framework.Helpers;
using System.Diagnostics;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using System.IO;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Framework.Imports;
using Galleria.Framework.Processes;
using Galleria.Ccm.Imports.Processes;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Editor.Client.Wpf.ProductLibrary
{
    /// <summary>
    /// Viewmodel for the controlling the product library validation
    /// </summary>
    public sealed class ProductLibraryValidationViewModel : ViewModelAttachedControlObject<ProductLibraryValidationWindow>
    {
        #region Fields

        private ImportDataValidationViewModel _importDataValidationViewModel;
        private ModalBusy _busyDialog;

        #endregion

        #region Property Paths

        public static readonly PropertyPath ImportDataValidationViewModelProperty = WpfHelper.GetPropertyPath<ProductLibraryValidationViewModel>(p => p.ImportDataValidationViewModel);
        public static readonly PropertyPath LoadCommandProperty = WpfHelper.GetPropertyPath<ProductLibraryValidationViewModel>(p => p.LoadCommand);
        public static readonly PropertyPath ExportErrorsCommandProperty = WpfHelper.GetPropertyPath<ProductLibraryValidationViewModel>(p => p.ExportErrorsCommand);
        #endregion

        #region Properties

        /// <summary>
        /// Gets/sets the current import data validation view model
        /// </summary>
        public ImportDataValidationViewModel ImportDataValidationViewModel
        {
            get { return _importDataValidationViewModel; }
            set
            {
                _importDataValidationViewModel = value;
                OnPropertyChanged(ImportDataValidationViewModelProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public ProductLibraryValidationViewModel(ImportDataValidationViewModel importViewModel)
        {
            this.ImportDataValidationViewModel = importViewModel;
        }

        #endregion

        #region Commands

        #region FinishCommand

        private RelayCommand _loadCommand;

        public RelayCommand LoadCommand
        {
            get
            {
                if (_loadCommand == null)
                {
                    _loadCommand = new RelayCommand(
                        p => LoadCommand_Executed(),
                        p => LoadCommand_CanExecute())
                    {
                        FriendlyName = Message.ProductLibraryValidation_Load,
                        FriendlyDescription = Message.ProductLibraryValidation_Load_Desc,
                        DisabledReason = Message.ProductLibraryValidation_Load_ErrorsDisabledReason
                    };
                    base.ViewModelCommands.Add(_loadCommand);
                }
                return _loadCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean LoadCommand_CanExecute()
        {
            if (this.ImportDataValidationViewModel != null && this.ImportDataValidationViewModel.ValidatedFileData != null)
            {
                if (!this.ImportDataValidationViewModel.ValidatedFileData.GetValidRows().Any())
                {
                    this.LoadCommand.DisabledReason = Message.ProductLibraryValidation_Load_NoProductsDisabledReason;
                }
                return this.ImportDataValidationViewModel.IsDataValid();
            }
            return false;
        }

        private void LoadCommand_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.DialogResult = true;

                //Close window
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region ExportErrorsCommand


        private RelayCommand _exportErrorsCommand;

        /// <summary>
        /// Exports the errors to excel
        /// </summary>
        public RelayCommand ExportErrorsCommand
        {
            get
            {
                if (_exportErrorsCommand == null)
                {
                    _exportErrorsCommand = new RelayCommand(
                        p => ExportErrors_Executed(),
                        p => ExportErrors_CanExecute())
                    {
                        FriendlyName = Message.ProductLibraryValidation_ExportErrors,
                        FriendlyDescription = Message.ProductLibraryValidation_ExportErrors_Description,
                        DisabledReason = Message.ProductLibraryValidation_ExportErrors_DisabledReason,
                    };
                    base.ViewModelCommands.Add(_exportErrorsCommand);
                }
                return _exportErrorsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ExportErrors_CanExecute()
        {
            return this.ImportDataValidationViewModel.WorksheetErrors.Count > 0;
        }

        private void ExportErrors_Executed()
        {
            if (this.AttachedControl != null)
            {
                //get the filename to save as
                Microsoft.Win32.SaveFileDialog saveDialog = new Microsoft.Win32.SaveFileDialog();
                saveDialog.CreatePrompt = false;
                saveDialog.Filter = Message.FileHelper_ExportFilter;
                saveDialog.FilterIndex = 2;

                String importFileName = Path.GetFileNameWithoutExtension(this.ImportDataValidationViewModel.ValidatedFileData.SourceFilePath);
                saveDialog.FileName = String.Format(Message.ProductLibraryValidation_ExportErrorsFileName, importFileName);

                saveDialog.OverwritePrompt = true;

                if (saveDialog.ShowDialog() == true)
                {
                    Boolean canContinue = true;


                    //If the selected file type is XLS
                    if (saveDialog.FilterIndex == 1)
                    {
                        //Show warning dialog informing user that if > 65k rows, they will not be outputted
                        //as it exceeds this file type row limit.
                        ModalMessage modelMessageWindow = new ModalMessage();
                        modelMessageWindow.Description = Message.FileHelper_RowNumberExceedsMaxLimitWarning;
                        modelMessageWindow.ButtonCount = 2;
                        modelMessageWindow.Button1Content = Message.Generic_OK;
                        modelMessageWindow.Button2Content = Message.Generic_Cancel;
                        modelMessageWindow.DefaultButton = ModalMessageButton.Button1;
                        modelMessageWindow.CancelButton = ModalMessageButton.Button2;
                        modelMessageWindow.Icon = ImageResources.Warning_32;
                        modelMessageWindow.ShowDialog();

                        //set the result
                        canContinue = (modelMessageWindow.Result == ModalMessageResult.Button1);
                    }


                    if (canContinue)
                    {
                        String fileName = saveDialog.FileName;

                        //Try to delete existing
                        if (System.IO.File.Exists(fileName))
                        {
                            canContinue = FileHelper.DeleteExistingFile(fileName);
                        }

                        if (canContinue)
                        {
                            ExportErrors(this.ImportDataValidationViewModel.ValidatedFileData, this.ImportDataValidationViewModel.WorksheetErrors, saveDialog.FileName);
                        }

                    }
                }
            }
        }

        #region Export Errors

        private void ExportErrors(ImportFileData importData, IEnumerable<ValidationErrorGroup> errors, String fileName)
        {
            ProcessFactory<ExportErrorsProcess> exportProcessFactory = new ProcessFactory<ExportErrorsProcess>();
            exportProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ExportErrorsProcess>>(OnExportErrorsProcessCompleted);
            exportProcessFactory.Execute(new ExportErrorsProcess(importData, errors, fileName));

            //show busy
            if (this.AttachedControl != null)
            {
                _busyDialog = new ModalBusy();
                _busyDialog.Header = Message.ProductLibraryValidation_BusyExportingErrorsHeader;
                _busyDialog.Description = Message.ProductLibraryValidation_BusyExportingErrorsDescription;
                _busyDialog.IsDeterminate = false;
                _busyDialog.Owner = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<ExtendedRibbonWindow>(this.AttachedControl);
                App.ShowWindow(_busyDialog, /*isModal*/true);
            }
        }

        /// <summary>
        /// Handles the complete of the export errors process
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnExportErrorsProcessCompleted(object sender, ProcessCompletedEventArgs<ExportErrorsProcess> e)
        {
            //unsubscribe events
            ProcessFactory<ExportErrorsProcess> processFactory = (ProcessFactory<ExportErrorsProcess>)sender;
            processFactory.ProcessCompleted -= OnExportErrorsProcessCompleted;

            //hide busy
            if (_busyDialog != null)
            {
                _busyDialog.Close();
                _busyDialog = null;
            }


            //show a dialog window displaying the result
            String description = null;
            String header = null;

            if (e.Error == null)
            {
                header = Message.ProductLibraryValidation_ExportErrorsSuccess;
            }
            else
            {
                header = Message.ProductLibraryValidation_ExportErrorsFailed;
                description = e.Error.Message;
            }


            if (this.AttachedControl != null)
            {
                ModalMessage errorWindow = new ModalMessage();
                errorWindow.Description = description;
                errorWindow.Header = header;
                errorWindow.ButtonCount = 1;
                errorWindow.Button1Content = Message.Generic_OK;
                errorWindow.DefaultButton = ModalMessageButton.Button1;
                App.ShowWindow(errorWindow, this.AttachedControl, /*isModal*/true);
            }
        }

        #endregion


        #endregion

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                //Property view model is disposed higher up
                this.ImportDataValidationViewModel.Dispose();

                base.IsDisposed = true;
            }
        }

        #endregion
    }
}


