﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion
#region Version History: (CCM 8.1)
// V8-27207 : I.George
//  Added Supplier Discount and Capacity properties to GetPropertyDefinitions method
#endregion
#region Version History: (CCM 8.2)
// V8-30668 : L.Luong
//  Should now show merchandisable depth or height depending on the component.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Windows;
using System.Windows.Data;
using Csla.Core;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf.PropertyEditor;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;


namespace Galleria.Ccm.Editor.Client.Wpf.ComponentEditor
{
    public sealed class ComponentEditorSelection : BulkObservableCollection<EditorSubComponentView>, IDisposable
    {
        #region Fields
        private Boolean _isLoggingUndoableActions;
        private EditorComponentView _componentView;
        private EditorSubComponentMultiView _subcomponentView;
        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets whether changes should be logged as undoable actions.
        /// Note - this requires that a plan was specified on create.
        /// </summary>
        public Boolean IsLoggingUndoableActions
        {
            get { return _componentView != null && _isLoggingUndoableActions; }
            set { _isLoggingUndoableActions = value; }
        }

        /// <summary>
        /// Returns the component parent for this selection.
        /// </summary>
        public EditorComponentView ParentComponent
        {
            get { return _componentView; }
        }

        /// <summary>
       /// Returns the merged view of the selected subcompoents
       /// </summary>
        public EditorSubComponentMultiView SubComponentView
        {
            get
            {
                if (_subcomponentView == null)
                {
                    if (Items.Count > 0)
                    {
                        _subcomponentView = new EditorSubComponentMultiView(Items);
                        _subcomponentView.IsLoggingUndoableActions = IsLoggingUndoableActions;
                    }
                }
                return _subcomponentView;
            }
        }

        /// <summary>
        /// Returns the unit of measure used by properties
        /// </summary>
        public DisplayUnitOfMeasureCollection UnitsOfMeasure
        {
            get
            {
                if (_componentView != null)
                {
                    return _componentView.UnitsOfMeasure;
                }
                else return DisplayUnitOfMeasureCollection.Empty;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public ComponentEditorSelection(EditorComponentView component)
        {
            _componentView = component;
            IsLoggingUndoableActions = true;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out actions whenever a bulk collection change is performed
        /// </summary>
        /// <param name="e"></param>
        protected override void OnBulkCollectionChanged(BulkCollectionChangedEventArgs e)
        {

            ClearViews();

            base.OnBulkCollectionChanged(e);

            foreach (PropertyInfo propertyInfo in this.GetType().GetProperties())
            {
                OnPropertyChanged(propertyInfo.Name);
            }

        }

        #endregion

        #region Methods

        private void ClearViews()
        {
            if (_subcomponentView != null)
            {
                _subcomponentView.Dispose();
                _subcomponentView = null;
            }
        }

        /// <summary>
        /// Returns the property definitions to use for the current selection.
        /// </summary>
        public List<PropertyItemDescription> GetPropertyDefinitions()
        {
            List<PropertyItemDescription> definitions = new List<PropertyItemDescription>();

            var uomValues = _componentView.UnitsOfMeasure;

            var uomConverter = Application.Current.FindResource(ResourceKeys.ConverterUnitDisplay) as IValueConverter;
            var lengthUom = _componentView.UnitsOfMeasure.Length;
            var currencyUom = _componentView.UnitsOfMeasure.Currency;

            if (this.Items.Count == 0)
            {


                //Show component properties
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.NameProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.ComponentTypeProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.HeightProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.WidthProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.DepthProperty, uomValues));

                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.IsMoveableProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.IsDisplayOnlyProperty, uomValues));
                //definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.IsMerchandisedTopDownProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.CanAttachShelfEdgeLabelProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.RetailerReferenceCodeProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.BarCodeProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.ManufacturerProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.ManufacturerPartNumberProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.ManufacturerPartNameProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.SupplierNameProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.SupplierPartNumberProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.SupplierCostPriceProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.SupplierDiscountProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.MinPurchaseQtyProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.WeightLimitProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.WeightProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.CapacityProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.VolumeProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramComponent.DiameterProperty, uomValues));
                

                //prefix all paths and set the category name
                foreach (var d in definitions)
                {
                    d.PropertyName = String.Format("ParentComponent.{0}", d.PropertyName);
                    d.CategoryName = Message.Generic_Component;
                }
            }
            else
            {
                //Show subcomponent properties
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.NameProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.HeightProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.WidthProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.DepthProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.XProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.YProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.ZProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.AngleProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.SlopeProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.RollProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.MerchandisingTypeProperty, uomValues));
                if (SubComponentView != null)
                {
                    if (ParentComponent.ComponentType == Model.FixtureComponentType.ClipStrip
                    || ParentComponent.ComponentType == Model.FixtureComponentType.Rod
                    || ParentComponent.ComponentType == Model.FixtureComponentType.Bar
                    || ParentComponent.ComponentType == Model.FixtureComponentType.Peg
                    || SubComponentView.MerchandisingType == FixtureSubComponentMerchandisingType.Hang
                    || SubComponentView.MerchandisingType == FixtureSubComponentMerchandisingType.HangFromBottom)
                    {
                        definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.MerchandisableDepthProperty, uomValues));
                    }
                    else
                    {
                        definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.MerchandisableHeightProperty, uomValues));
                    }
                }
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.FillColourFrontProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.FillColourBackProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.FillColourTopProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.FillColourBottomProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.FillColourLeftProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.FillColourRightProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.LineColourProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.LineThicknessProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.RiserHeightProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.RiserThicknessProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.RiserColourProperty, uomValues));
                definitions.Add(CommonHelper.GetPropertyDescription(PlanogramSubComponent.IsRiserPlacedOnFrontProperty, uomValues));

                //prefix all paths and set the category name
                foreach (var d in definitions)
                {
                    d.PropertyName = String.Format("SubComponentView.{0}", d.PropertyName);
                    d.CategoryName = Message.Generic_SubComponent;
                }
            }

            return definitions;
        }

        #endregion

        #region INotifyPropertyChanged Members

        private void OnPropertyChanged(String propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                ClearViews();

                _isDisposed = true;
                GC.SuppressFinalize(this);
            }
        }

        #endregion

        #region Static Helpers

        private static PropertyPath GetPropertyPath(System.Linq.Expressions.Expression<Func<ComponentEditorSelection, object>> expression)
        {
            return WpfHelper.GetPropertyPath<ComponentEditorSelection>(expression);
        }

        #endregion
    }
}
