﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Ineson 
//  Created.
// V8-25734 : L.Ineson
//  Moved out rendering controls to framework.
// V8-26530 : L.Ineson
//  Updated rotation method to rotate the items with a center pivot.
#endregion
#endregion

using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Common.Rendering;
using Galleria.Framework.Controls.Wpf.HelperClasses;
using Galleria.Framework.Controls.Wpf.Rendering;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Rendering;
using HelixToolkit.Wpf;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace Galleria.Ccm.Editor.Client.Wpf.ComponentEditor
{
    public static class ComponentEditorHelper
    {
        #region Item

        public static EditorSubComponentView GetHitSub(DependencyObject model)
        {
            try
            {
                DependencyObject dep = model;

                while (dep != null)
                {
                    ModelConstruct3D depModel = dep as ModelConstruct3D;
                    if (depModel != null
                        && depModel.ModelData is IPlanPart3DData)
                    {
                        EditorSubComponentView subView = ((IPlanPart3DData)depModel.ModelData).PlanPart as EditorSubComponentView;
                        if (subView != null)
                        {
                            return subView;
                        }
                    }

                    //try next parent.
                    dep = VisualTreeHelper.GetParent(dep);

                }
                return null;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Resizes the given plan items by the provided change vector.
        /// </summary>
        public static Boolean ResizeItems(IEnumerable<EditorSubComponentView> items, Double changeX, Double changeY, Double changeZ)
        {
            Boolean valueChanged = false;

            foreach (EditorSubComponentView sub in items)
            {
                //Height
                Single newHeight = Convert.ToSingle(sub.Height + changeY);
                if (newHeight != sub.Height)
                {
                    if (newHeight > 0.01f)
                    {
                        sub.Height = newHeight;
                        valueChanged = true;
                    }
                    else
                    {
                        sub.Height = 0.01f;
                    }
                }


                //Width
                Single newWidth = Convert.ToSingle(sub.Width + changeX);
                if (newWidth != sub.Width)
                {
                    if (newWidth > 0.01f)
                    {
                        sub.Width = newWidth;
                        valueChanged = true;
                    }
                    else
                    {
                        sub.Width = 0.01f;
                    }
                }

                //Depth
                Single newDepth = Convert.ToSingle(sub.Depth + changeZ);
                if (newDepth != sub.Depth)
                {
                    if (newDepth > 0.01f)
                    {
                        sub.Depth = newDepth;
                        valueChanged = true;
                    }
                    else
                    {
                        sub.Depth = 0.01f;
                    }
                }
            }

            return valueChanged;
        }

        /// <summary>
        /// Repositions the given plan items according to the provided change vector.
        /// Items will be moved at the highest possible level based on the provided selection.
        /// </summary>
        /// <param name="items"></param>
        /// <param name="changeX"></param>
        /// <param name="changeY"></param>
        /// <param name="changeZ"></param>
        public static void RepositionItems(IEnumerable<EditorSubComponentView> items, Double changeX, Double changeY, Double changeZ)
        {
            foreach (EditorSubComponentView item in items)
            {
                item.X = Convert.ToSingle(item.X + changeX);
                item.Y =  Convert.ToSingle(item.Y + changeY);
                item.Z = Convert.ToSingle(item.Z + changeZ);

                //if the item has collision detection, 
                //first check that setting the new position
                // would not make it collide with others
                //if (item.HasCollisionDetection)
                //{
                //    Rect3D newBounds = item.GetWorldSpaceBounds();

                //    foreach (var sub in item.ParentComponentView.SubComponents)
                //    {
                //        if (sub != item && sub.HasCollisionDetection)
                //        {
                //            Rect3D subBounds = sub.GetWorldSpaceBounds();
                //            if (newBounds.IntersectsWith(subBounds))
                //            {
                //                //undo
                //                item.X = Convert.ToSingle(item.X - changeX);
                //                item.Y = Convert.ToSingle(item.Y - changeY);
                //                item.Z = Convert.ToSingle(item.Z - changeZ);
                //                break;
                //            }
                //        }
                //    }
                //}

            }
        }

        /// <summary>
        /// Rotates the given plan items about their center according to the provided change vector.
        /// Items will be moved at the highest possible level based on the provided selection.
        /// </summary>
        /// <param name="items"></param>
        /// <param name="slopeChange">the slope rotation change in radians</param>
        /// <param name="angleChange">the angle rotation change in radians </param>
        /// <param name="rollChange"> the roll rotation change in radians</param>
        public static void RotateItemsAboutCenter(IEnumerable<EditorSubComponentView> items, Double slopeChange, Double angleChange, Double rollChange)
        {
            foreach (EditorSubComponentView item in items)
            {
                PointValue origin = new PointValue();
                PointValue pivot = new PointValue(item.Width / 2F, item.Height / 2F, item.Depth / 2F);

                //get the current rotation offset
                Matrix3D rotationMatrix = ModelConstruct3DHelper.GetRotationMatrix(new RotationValue(item.Angle, item.Slope, item.Roll), pivot);
                PointValue currentRotationOffset = origin.Transform(rotationMatrix);

                //work out the raw position without any rotation.
                PointValue rawPos = new PointValue(item.X - currentRotationOffset.X, item.Y - currentRotationOffset.Y, item.Z - currentRotationOffset.Z);

                //now apply the rotation to the item.
                Double newDegrees;

                //Angle
                if (angleChange != 0)
                {
                    newDegrees = CommonHelper.ToDegrees(item.Angle + angleChange);
                    item.Angle = Convert.ToSingle(CommonHelper.ToRadians(newDegrees));
                }

                //Slope
                if (slopeChange != 0)
                {
                    newDegrees = CommonHelper.ToDegrees(item.Slope + slopeChange);
                    item.Slope = Convert.ToSingle(CommonHelper.ToRadians(newDegrees));
                }

                //Roll
                if (rollChange != 0)
                {
                    newDegrees = CommonHelper.ToDegrees(item.Roll + rollChange);
                    item.Roll = Convert.ToSingle(CommonHelper.ToRadians(newDegrees));
                }


                //get the offset of the new rotation
                rotationMatrix = ModelConstruct3DHelper.GetRotationMatrix(new RotationValue(item.Angle, item.Slope, item.Roll), pivot);
                PointValue newRotationOffset = origin.Transform(rotationMatrix);

                //correct the raw position
                item.X = rawPos.X + newRotationOffset.X;
                item.Y = rawPos.Y + newRotationOffset.Y;
                item.Z = rawPos.Z + newRotationOffset.Z;

            }
        }

        #endregion

        #region Snapping / Collision Detection

        /// <summary>
        /// Returns a hit test result for the subcomponent nearest to the 2D hit point 
        /// based on an intersection ray.
        /// </summary>
        /// <param name="vp">The viewport.</param>
        /// <param name="hitPoint">The 2d hit</param>
        /// <param name="Plan3DData">The model data for the planogram we are hit testing against.</param>
        /// <returns></returns>
        public static ComponentEditorHitTestResult GetNearestSubComponent(Viewport3D vp, Point hitPoint)
        {
            ComponentEditorHitTestResult planHitResult = new ComponentEditorHitTestResult();
            planHitResult.ViewerHitPoint = ModelConstruct3DHelper.GetModelPoint3D(hitPoint, vp);

            ProjectionCamera camera = vp.Camera as ProjectionCamera;
            if (camera == null) throw new ArgumentException("Viewer must use a projection camera");

            //get the subcomponent nearest to the camera at the given hit point.
            Viewport3DHelper.HitResult hitResult;
            ModelConstruct3D nearestModel = ModelConstruct3DHelper.GetNearestModelConstruct(vp, camera, hitPoint, out hitResult);
            if (nearestModel != null)
            {
                FixtureSubComponent3DData subData = nearestModel.ModelData as FixtureSubComponent3DData;
                if (subData != null)
                {
                    planHitResult.NearestSubComponentData = subData;
                    planHitResult.NearestSubComponent = subData.SubComponent as EditorSubComponentView;

                    if (hitResult != null)
                    {
                        planHitResult.SubComponentLocalHitPoint = hitResult.RayHit.PointHit.ToPointValue();
                        planHitResult.SubComponentWorldHitPoint = ModelConstruct3DHelper.ToWorld(planHitResult.SubComponentLocalHitPoint, nearestModel.ModelData);
                        planHitResult.SubComponentFaceHit = ModelConstruct3DHelper.NormalToFacing(hitResult.Normal);
                    }
                }

            }

            return planHitResult;
        }

        #endregion

    }

    /// <summary>
    /// Denotes the different types of documents in the component editor.
    /// </summary>
    public enum ComponentEditorDocumentType
    {
        Front, 
        Perspective,
        Left,
        Right,
        Top,
        Bottom,
        Back,
        SubComponentList
    }

    public sealed class ComponentEditorHitTestResult
    {
        public PointValue ViewerHitPoint { get; set; }

        public FixtureSubComponent3DData NearestSubComponentData { get; set; }
        public EditorSubComponentView NearestSubComponent { get; set; }
        public PointValue SubComponentLocalHitPoint { get; set; }
        public PointValue SubComponentWorldHitPoint { get; set; }
        public ModelConstruct3DHelper.HitFace SubComponentFaceHit { get; set; }
    }

    public sealed class ComponentEditorItemDragDropArgs
    {
        public List<ComponentEditorDragDropItem> DragItems { get; private set; }

        public Boolean IsCreateCopy { get; set; }

        public ComponentEditorItemDragDropArgs()
        {
            this.DragItems = new List<ComponentEditorDragDropItem>();
        }
    }

    public sealed class ComponentEditorDragDropItem
    {
        public EditorSubComponentView Item { get; set; }// the item.
        public ModelConstruct3D Model3DVis { get; set; }// the actual models that were moved.
        public PointValue DragOffset { get; set; } //the dragging position offset from the mouse point.
    }
}
