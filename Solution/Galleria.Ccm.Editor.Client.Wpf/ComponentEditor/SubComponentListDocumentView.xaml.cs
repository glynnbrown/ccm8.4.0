﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26495 L.Ineson 
//  Created.
#endregion
#endregion

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Planograms.Model;
using System.Collections.Generic;
using System.Windows.Data;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Converters;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;


namespace Galleria.Ccm.Editor.Client.Wpf.ComponentEditor
{
    /// <summary>
    /// Interaction logic for SubComponentListDocumentView.xaml
    /// </summary>
    public partial class SubComponentListDocumentView : UserControl, IComponentEditorDocumentView
    {
        #region Fields

        private readonly DataGridColumnCollection _columnSet = new DataGridColumnCollection();

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(SubComponentListDocument), typeof(SubComponentListDocumentView),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Called whenever the viewmodel context changes
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            SubComponentListDocumentView senderControl = (SubComponentListDocumentView)obj;

            if (e.OldValue != null)
            {
                SubComponentListDocument oldModel = (SubComponentListDocument)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                SubComponentListDocument newModel = (SubComponentListDocument)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
            }
        }

        /// <summary>
        /// Gets the viewmodel controller for this view.
        /// </summary>
        public SubComponentListDocument ViewModel
        {
            get { return (SubComponentListDocument)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }


        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public SubComponentListDocumentView(SubComponentListDocument document)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            this.ViewModel = document;

            this.Loaded += SubComponentListDocumentView_Loaded;

        }

        /// <summary>
        ///Carries out intial load actions.
        /// </summary>
        private void SubComponentListDocumentView_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= SubComponentListDocumentView_Loaded;

            UpdateColumnSet();

            //Apply the column set to the grid.
            this.xGrid.ColumnSet = _columnSet;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on the viewmodel.
        /// </summary>
        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
        }

        /// <summary>
        /// Called whenever a grid row is double clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xListGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            this.ViewModel.ParentController.ShowItemPropertiesCommand.Execute();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Recreates the column set.
        /// </summary>
        private void UpdateColumnSet()
        {
            //clear down the grid.
            if (_columnSet.Count > 0) _columnSet.Clear();
            if (this.xGrid.GroupByDescriptions.Count > 0) this.xGrid.GroupByDescriptions.Clear();
            if (this.xGrid.SortByDescriptions.Count > 0) this.xGrid.SortByDescriptions.Clear();

            if (this.ViewModel != null)
            {
                var uomValues = this.ViewModel.Component.UnitsOfMeasure;

                String groupName = Message.ComponentProperties_ViewComponentGeneral;

                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.NameProperty, uomValues, groupName));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.HeightProperty, uomValues, groupName));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.WidthProperty, uomValues, groupName));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.DepthProperty, uomValues, groupName));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.XProperty, uomValues, groupName));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.YProperty, uomValues, groupName));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.ZProperty, uomValues, groupName));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.AngleProperty, uomValues, groupName));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.SlopeProperty, uomValues, groupName));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.RollProperty, uomValues, groupName));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.FaceThicknessFrontProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.FaceThicknessBackProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.FaceThicknessTopProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.FaceThicknessBottomProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.FaceThicknessLeftProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.FaceThicknessRightProperty, uomValues, groupName, isVisible: false));

                groupName = Message.ComponentProperties_ViewComponentMerchandising;
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.MerchandisableHeightProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.CombineTypeProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.MerchandisingStrategyXProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.MerchandisingStrategyYProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.MerchandisingStrategyZProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.MerchandisingTypeProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.IsProductOverlapAllowedProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.LeftOverhangProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.RightOverhangProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.TopOverhangProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.BottomOverhangProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.FrontOverhangProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.BackOverhangProperty, uomValues, groupName, isVisible: false));


                groupName = Message.ComponentProperties_ViewComponentNotches;
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.NotchStartXProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.NotchSpacingXProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.NotchStartYProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.NotchSpacingYProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.NotchHeightProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.NotchWidthProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.IsNotchPlacedOnFrontProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.NotchStyleTypeProperty, uomValues, groupName, isVisible: false));

                groupName = Message.ComponentProperties_ViewComponentRiser;
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.RiserHeightProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.RiserThicknessProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.IsRiserPlacedOnFrontProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.IsRiserPlacedOnBackProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.IsRiserPlacedOnLeftProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.IsRiserPlacedOnRightProperty, uomValues, groupName, isVisible: false));


                groupName = Message.ComponentProperties_ViewComponentPegboard;
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.MerchConstraintRow1StartXProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.MerchConstraintRow1SpacingXProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.MerchConstraintRow1StartYProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.MerchConstraintRow1SpacingYProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.MerchConstraintRow1HeightProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.MerchConstraintRow1WidthProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.MerchConstraintRow2StartXProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.MerchConstraintRow2SpacingXProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.MerchConstraintRow2StartYProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.MerchConstraintRow2SpacingYProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.MerchConstraintRow2HeightProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.MerchConstraintRow2WidthProperty, uomValues, groupName, isVisible: false));

                groupName = Message.ComponentProperties_ViewComponentDividers;
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.DividerObstructionHeightProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.DividerObstructionWidthProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.DividerObstructionDepthProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.DividerObstructionStartXProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.DividerObstructionSpacingXProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.DividerObstructionStartYProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.DividerObstructionSpacingYProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.DividerObstructionStartZProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.DividerObstructionSpacingZProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.IsDividerObstructionAtStartProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.IsDividerObstructionAtEndProperty, uomValues, groupName, isVisible: false));
                _columnSet.Add(CommonHelper.CreateEditableColumn(FixtureSubComponent.IsDividerObstructionByFacingProperty, uomValues, groupName, isVisible: false));


            }
        }

        #endregion

        #region IComponentEditorDocumentView Members

        public IComponentEditorDocument GetDocument()
        {
            return this.ViewModel as IComponentEditorDocument;
        }

        public void Dispose()
        {
            this.ViewModel = null;
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
