﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26495 : L.Ineson
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using System.Collections.ObjectModel;
using System.Windows;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Editor.Client.Wpf.ComponentEditor
{
    /// <summary>
    /// Viewmodel controller for subcomponent list document view
    /// </summary>
    public sealed class SubComponentListDocument : ComponentEditorDocument<SubComponentListDocumentView>
    {
        #region Binding Property Paths

        public static PropertyPath ItemRowsProperty = WpfHelper.GetPropertyPath<SubComponentListDocument>(p => p.ItemRows);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the document title
        /// </summary>
        public override string Title
        {
            get { return Message.ComponentEditor_SubcomponentListTitle; }
        }

        /// <summary>
        /// Returns the document type.
        /// </summary>
        public override ComponentEditorDocumentType DocumentType
        {
            get { return ComponentEditorDocumentType.SubComponentList; }
        }


        public ObservableCollection<EditorSubComponentView> ItemRows
        {
            get { return this.Component.SubComponents; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public SubComponentListDocument(ComponentEditorViewModel parentController)
            : base(parentController)
        {
            this.IsZoomingSupported = false;
        }

        #endregion
    }
}
