﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
//V8-25397 : A.Probyn
//  Forced ReloadDocuments() after window has rendered. 3D wasn't rendering correctly on load before window is rendered.
// V8-26495 : L.Ineson
//  Brought up to date.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Model;
using Xceed.Wpf.AvalonDock;
using Xceed.Wpf.AvalonDock.Controls;
using Xceed.Wpf.AvalonDock.Layout;
using System.ComponentModel;

namespace Galleria.Ccm.Editor.Client.Wpf.ComponentEditor
{
    /// <summary>
    /// Interaction logic for ComponentEditorOrganiser.xaml
    /// </summary>
    public sealed partial class ComponentEditorOrganiser : ExtendedRibbonWindow, ILayoutUpdateStrategy
    {
        #region Fields
        private Boolean _supressShowHidePanels;
        private Boolean _suppressActiveDocChange;
        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ComponentEditorViewModel), typeof(ComponentEditorOrganiser),
            new UIPropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ComponentEditorOrganiser senderControl = (ComponentEditorOrganiser)obj;

            if (e.OldValue != null)
            {
                ComponentEditorViewModel oldModel = (ComponentEditorViewModel)e.OldValue;
                oldModel.AttachedControl = null;

                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
                oldModel.Documents.BulkCollectionChanged -= senderControl.ViewModel_DocumentsBulkCollectionChanged;
            }

            if (e.NewValue != null)
            {
                ComponentEditorViewModel newModel = (ComponentEditorViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
                newModel.Documents.BulkCollectionChanged += senderControl.ViewModel_DocumentsBulkCollectionChanged;
            }

            senderControl.ReloadDocuments();
            senderControl.ShowHidePanels();
        }


        public ComponentEditorViewModel ViewModel
        {
            get { return (ComponentEditorViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region DocumentControls Property

        public static readonly DependencyProperty DocumentControlsProperty =
            DependencyProperty.Register("DocumentControls", typeof(ObservableCollection<IComponentEditorDocumentView>), typeof(ComponentEditorOrganiser),
            new PropertyMetadata(null));

        /// <summary>
        /// Returns the editable collection of document controls.
        /// </summary>
        public ObservableCollection<IComponentEditorDocumentView> DocumentControls
        {
            get { return (ObservableCollection<IComponentEditorDocumentView>)GetValue(DocumentControlsProperty); }
        }

        #endregion

        #region Panels Property

        public static readonly DependencyProperty PanelsProperty =
            DependencyProperty.Register("Panels", typeof(ObservableCollection<UIElement>), typeof(ComponentEditorOrganiser),
            new PropertyMetadata(null));

        /// <summary>
        /// Returns the collection of panels to be displayed by the docking manager.
        /// </summary>
        public ObservableCollection<UIElement> Panels
        {
            get { return (ObservableCollection<UIElement>)GetValue(PanelsProperty); }
        }

        #endregion

        #region ActiveContent Property

        public static readonly DependencyProperty ActiveContentProperty =
            DependencyProperty.Register("ActiveContent", typeof(Object), typeof(ComponentEditorOrganiser),
           new PropertyMetadata(null, OnActiveContentPropertyChanged));

        private static void OnActiveContentPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((ComponentEditorOrganiser)obj).OnActiveContentChanged(e.NewValue as IComponentEditorDocumentView);
        }

        /// <summary>
        /// Gets/Sets the currently active content.
        /// </summary>
        public Object ActiveContent
        {
            get { return GetValue(ActiveContentProperty); }
            set { SetValue(ActiveContentProperty, value); }
        }

        /// <summary>
        /// Called whenever the value of the active content property changes.
        /// </summary>
        /// <param name="newValue"></param>
        private void OnActiveContentChanged(IComponentEditorDocumentView newValue)
        {
            if (!_suppressActiveDocChange
                && newValue != null
                && this.ViewModel != null)
            {
                _suppressActiveDocChange = true;

                IComponentEditorDocumentView activeDoc = newValue;
                if (activeDoc != null)
                {
                    this.ViewModel.SelectedDocument = activeDoc.GetDocument();

                    //fix to stop drag drop issues
                    ((UIElement)activeDoc).AllowDrop = true;
                }

                _suppressActiveDocChange = false;
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="componentView"></param>
        public ComponentEditorOrganiser(ComponentEditorViewModel viewModel)
        {
            SetValue(DocumentControlsProperty, new ObservableCollection<IComponentEditorDocumentView>());
            SetValue(PanelsProperty, new ObservableCollection<UIElement>());

            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //set the help file key for this window.
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.ComponentEditor);

            this.ViewModel = viewModel;

            this.Loaded += ComponentEditorOrganiser_Loaded;
        }

        /// <summary>
        /// Carries out initial load actions.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComponentEditorOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ComponentEditorOrganiser_Loaded;

            //V8-25397 : Ensure documents are loaded after window is rendered
            if (this.DocumentControls.Count == 0)
            {
                ReloadDocuments();
            }
            SetupInitialLayout();

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Fix to make sure that clicking a controller in a floating window makes it active.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewMouseUp(System.Windows.Input.MouseButtonEventArgs e)
        {
            base.OnPreviewMouseUp(e);

            if (this.ViewModel != null)
            {
                LayoutDocumentControl parentControl = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<LayoutDocumentControl>(this);
                if (parentControl != null)
                {
                    if (this.IsMouseOver && e.ChangedButton == System.Windows.Input.MouseButton.Left)
                    {
                        LayoutDocument doc = parentControl.Model as LayoutDocument;
                        if (doc != null)
                        {
                            if (doc.IsFloating && !doc.IsActive)
                            {
                                doc.IsActive = true;
                            }
                        }

                    }
                }
            }
        }

        /// <summary>
        /// Responds to property changes on the viewmodel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ComponentEditorViewModel.SelectedDocumentProperty.Path)
            {
                if (!_suppressActiveDocChange)
                {
                    _suppressActiveDocChange = true;

                    IComponentEditorDocumentView docControl = this.DocumentControls.FirstOrDefault(d => d.GetDocument() ==
                        this.ViewModel.SelectedDocument);
                    if (docControl != null)
                    {

                        this.ActiveContent = docControl;

                        //Fix to force document selection.
                        LayoutDocument anchorable =
                        this.xDockingManager.Layout.Descendents().OfType<LayoutDocument>()
                        .FirstOrDefault(d => d.Content == docControl);

                        foreach (LayoutDocumentPaneControl tabControl in
                                this.xDockingManager.FindVisualChildren<LayoutDocumentPaneControl>().ToList())
                        {
                            if (tabControl.Items.Contains(anchorable))
                            {
                                tabControl.SelectedIndex = tabControl.Items.IndexOf(anchorable);
                                anchorable.IsSelected = true;
                                break;
                            }
                        }

                    }
                    _suppressActiveDocChange = false;
                }

            }
            else if (e.PropertyName == ComponentEditorViewModel.IsPropertiesPanelVisibleProperty.Path)
            {
                ShowHidePanels();
            }
        }

        /// <summary>
        /// Responds to changes to the viewmodel documents collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_DocumentsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (IComponentEditorDocument doc in e.ChangedItems)
                        {
                            this.DocumentControls.Add(CreateDocumentView(doc));
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        //find the document and remove it.
                        foreach (IComponentEditorDocument doc in e.ChangedItems)
                        {
                            var docView = this.DocumentControls.FirstOrDefault(p => p.GetDocument() == doc);
                            if (docView != null)
                            {
                                this.DocumentControls.Remove(docView);

                                //dispose
                                this.ViewModel.CloseDocument(docView.GetDocument());
                                docView.Dispose();
                            }
                        }
                    }
                    break;


                case NotifyCollectionChangedAction.Reset:
                    ReloadDocuments();
                    break;

            }
        }

        /// <summary>
        /// Called when a document is closed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xDockingManager_DocumentClosed(object sender, DocumentClosedEventArgs e)
        {
            LayoutDocument closedDoc = e.Document;
            if (closedDoc != null)
            {
                //remove the document from the viewmodel.
                var doc = closedDoc.Content as IComponentEditorDocumentView;
                if (doc != null)
                {
                    this.ViewModel.CloseDocument(doc.GetDocument());
                    doc.Dispose();
                }

            }
        }

        /// <summary>
        /// Called a panel is hiding.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Panel_Hiding(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _supressShowHidePanels = true;

            LayoutAnchorable senderControl = (LayoutAnchorable)sender;

            if (senderControl.Content is ComponentEditorPropertiesPanel)
            {
                this.ViewModel.IsPropertiesPanelVisible = false;
            }

            _supressShowHidePanels = false;
        }

        /// <summary>
        /// Called when a panel is closing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Panel_Closed(object sender, EventArgs e)
        {
            LayoutAnchorable senderControl = (LayoutAnchorable)sender;
            senderControl.Hiding -= Panel_Hiding;
            senderControl.Closed -= Panel_Closed;

            IDisposable disposableContent = senderControl.Content as IDisposable;
            if (disposableContent != null)
            {
                disposableContent.Dispose();
            }

            if (senderControl.Content is ComponentEditorPropertiesPanel)
            {
                this.ViewModel.IsPropertiesPanelVisible = false;
            }
        }

        /// <summary>
        /// Called on a key down preview.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewKeyDown(System.Windows.Input.KeyEventArgs e)
        {
            //[GEM:26568] - Do not process if source is a textbox.
            if (e.OriginalSource is System.Windows.Controls.TextBox) return;
            if (e.OriginalSource is Fluent.TextBox) return;

            if (this.ViewModel.ProcessKeyDown(e.Key, Keyboard.Modifiers))
            {
                e.Handled = true;
            }

            base.OnPreviewKeyDown(e);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Reloads all documents
        /// </summary>
        private void ReloadDocuments()
        {
            if (this.DocumentControls.Count > 0)
            {
                var docs = this.DocumentControls.ToList();

                this.DocumentControls.Clear();

                foreach (var d in docs)
                {
                    if (this.ViewModel != null)
                    {
                        this.ViewModel.CloseDocument(d.GetDocument());
                    }
                    d.Dispose();
                }
            }

            if (this.ViewModel != null)
            {
                foreach (var doc in this.ViewModel.Documents)
                {
                    this.DocumentControls.Add(CreateDocumentView(doc));
                }
            }
        }

        /// <summary>
        /// Shows/Hides the panels
        /// </summary>
        private void ShowHidePanels()
        {
            if (_supressShowHidePanels) { return; }


            if (this.ViewModel != null)
            {
                List<LayoutAnchorable> anchorables = this.xDockingManager.Layout.Descendents().OfType<LayoutAnchorable>().ToList();

                //Properties Panel
                ShowHidePanel<ComponentEditorPropertiesPanel>(
                    this.ViewModel.IsPropertiesPanelVisible,
                    new Action(
                    () =>
                    {
                        ComponentEditorPropertiesPanel propertiesPanel = new ComponentEditorPropertiesPanel();
                        propertiesPanel.ViewModel = this.ViewModel.SelectedSubcomponents;
                        propertiesPanel.GroupName = "Properties";
                        propertiesPanel.PlacementStrategy = AnchorableShowStrategy.Right;
                        this.Panels.Add(propertiesPanel);
                    }),
                    anchorables);


            }
            else
            {
                this.Panels.Clear();
            }
        }

        /// <summary>
        /// Tiles all  plan documents horizontally.
        /// </summary>
        public void TileWindows(Orientation tileOrientation)
        {
            if (this.xDockingManager != null)
            {
                //get a list of all layout content
                List<LayoutDocument> contentList = this.xDockingManager.Layout.Descendents().OfType<LayoutDocument>().ToList();

                //create a temp pane and move all children there
                LayoutDocumentPane tempPane = new LayoutDocumentPane();
                LayoutDocumentPaneGroup tempGroup = new LayoutDocumentPaneGroup(tempPane);
                this.xDockingManager.Layout.RootPanel.Children.Add(tempGroup);

                foreach (LayoutDocument documentTab in contentList)
                {
                    tempPane.Children.Add(documentTab);
                }

                //remove all other panes
                foreach (ILayoutPanelElement ele in this.xDockingManager.Layout.RootPanel.Children.ToList())
                {
                    if (ele != tempGroup)
                    {
                        this.xDockingManager.Layout.RootPanel.Children.Remove(ele);
                    }
                }

                this.xDockingManager.Layout.RootPanel.Orientation = Orientation.Horizontal;


                //create the new  group with a pane per doc
                LayoutDocumentPaneGroup newGroup = new LayoutDocumentPaneGroup();
                newGroup.Orientation = tileOrientation;
                this.xDockingManager.Layout.RootPanel.Children.Insert(0, newGroup);

                foreach (LayoutDocument documentTab in contentList)
                {
                    newGroup.Children.Add(new LayoutDocumentPane(documentTab));
                }


                //remove the temporary pane again
                this.xDockingManager.Layout.RootPanel.Children.Remove(tempGroup);


                //queue a zoom to fit all
                Dispatcher.BeginInvoke(
                    (Action)(() =>
                    {
                        if (this.ViewModel != null)
                        {
                            this.ViewModel.ZoomToFitCommand.Execute();
                        }

                    }), priority: System.Windows.Threading.DispatcherPriority.Render);
            }
        }

        /// <summary>
        /// Docks all plan documents into a single tab group.
        /// </summary>
        public void DockAllViews()
        {
            if (this.xDockingManager != null)
            {
                var selectedDoc = this.ViewModel.SelectedDocument;

                //get a list of all layout content
                List<LayoutDocument> contentList = this.xDockingManager.Layout.Descendents().OfType<LayoutDocument>().ToList();

                //create a temp pane and move all children there
                LayoutDocumentPane tempPane = new LayoutDocumentPane();
                LayoutDocumentPaneGroup tempGroup = new LayoutDocumentPaneGroup(tempPane);
                this.xDockingManager.Layout.RootPanel.Children.Add(tempGroup);

                foreach (LayoutDocument documentTab in contentList)
                {
                    tempPane.Children.Add(documentTab);
                }

                //remove all other panes
                foreach (ILayoutPanelElement ele in this.xDockingManager.Layout.RootPanel.Children.ToList())
                {
                    if (ele != tempGroup)
                    {
                        this.xDockingManager.Layout.RootPanel.Children.Remove(ele);
                    }
                }

                this.xDockingManager.Layout.RootPanel.Orientation = Orientation.Horizontal;

                //ensure the correct document is selected.
                this.ViewModel.SelectedDocument = selectedDoc;
                this.ViewModel.ZoomToFitCommand.Execute();
            }
        }

        /// <summary>
        /// Arranges all plan document windows.
        /// </summary>
        public void ArrangeAllWindows()
        {
            if (this.xDockingManager != null)
            {
                //get a list of all layout content
                List<LayoutDocument> contentList = this.xDockingManager.Layout.Descendents().OfType<LayoutDocument>().ToList();

                //create a temp pane and move all children there
                LayoutDocumentPane tempPane = new LayoutDocumentPane();
                LayoutDocumentPaneGroup tempGroup = new LayoutDocumentPaneGroup(tempPane);
                this.xDockingManager.Layout.RootPanel.Children.Add(tempGroup);

                foreach (LayoutDocument documentTab in contentList)
                {
                    tempPane.Children.Add(documentTab);
                }

                //remove all other panes
                foreach (ILayoutPanelElement ele in this.xDockingManager.Layout.RootPanel.Children.ToList())
                {
                    if (ele != tempGroup)
                    {
                        this.xDockingManager.Layout.RootPanel.Children.Remove(ele);
                    }
                }

                this.xDockingManager.Layout.RootPanel.Orientation = Orientation.Horizontal;

                //group documents based on where they should live
                List<IComponentEditorDocumentView> leftDocs = new List<IComponentEditorDocumentView>();
                List<IComponentEditorDocumentView> rightDocs = new List<IComponentEditorDocumentView>();
                List<IComponentEditorDocumentView> bottomDocs = new List<IComponentEditorDocumentView>();
                List<IComponentEditorDocumentView> topDocs = new List<IComponentEditorDocumentView>();
                List<IComponentEditorDocumentView> centerDocs = new List<IComponentEditorDocumentView>();

                foreach (var docView in this.DocumentControls)
                {
                    var planDoc = docView.GetDocument();
                    if (planDoc != null)
                    {
                        switch (planDoc.DocumentType)
                        {
                            case ComponentEditorDocumentType.Front: centerDocs.Add(docView); break;
                            case ComponentEditorDocumentType.Back: centerDocs.Add(docView); break;
                            case ComponentEditorDocumentType.Top: topDocs.Add(docView); break;
                            case ComponentEditorDocumentType.Bottom: bottomDocs.Add(docView); break;
                            case ComponentEditorDocumentType.Left: leftDocs.Add(docView); break;
                            case ComponentEditorDocumentType.Right: rightDocs.Add(docView); break;
                            case ComponentEditorDocumentType.Perspective: centerDocs.Add(docView); break;
                            case ComponentEditorDocumentType.SubComponentList: bottomDocs.Add(docView); break;
                            default:
                                Debug.Fail("Type not handled");
                                centerDocs.Add(docView);
                                break;
                        }
                    }
                }

                //create each panel

                //create right
                if (rightDocs.Count > 0)
                {
                    LayoutDocumentPaneGroup rightGroup = new LayoutDocumentPaneGroup();
                    rightGroup.Orientation = Orientation.Horizontal;
                    this.xDockingManager.Layout.RootPanel.Children.Insert(0, rightGroup);

                    foreach (var docView in rightDocs)
                    {
                        LayoutDocument documentTab = contentList.FirstOrDefault(c => c.Content == docView);
                        if (documentTab != null)
                        {
                            LayoutDocumentPane pane = new LayoutDocumentPane();
                            rightGroup.Children.Add(pane);
                            pane.Children.Add(documentTab);
                        }
                    }
                }


                //create center vert
                LayoutDocumentPaneGroup centerVertGroup = new LayoutDocumentPaneGroup();
                centerVertGroup.Orientation = Orientation.Vertical;
                this.xDockingManager.Layout.RootPanel.Children.Insert(0, centerVertGroup);

                //create top
                if (topDocs.Count > 0)
                {
                    LayoutDocumentPaneGroup topGroup = new LayoutDocumentPaneGroup();
                    topGroup.Orientation = Orientation.Vertical;
                    centerVertGroup.Children.Add(topGroup);

                    foreach (var docView in topDocs)
                    {
                        LayoutDocument documentTab = contentList.FirstOrDefault(c => c.Content == docView);
                        if (documentTab != null)
                        {
                            LayoutDocumentPane pane = new LayoutDocumentPane();
                            topGroup.Children.Add(pane);
                            pane.Children.Add(documentTab);
                        }
                    }
                }

                //create center
                if (centerDocs.Count > 0)
                {
                    LayoutDocumentPaneGroup centerGroup = new LayoutDocumentPaneGroup();
                    centerGroup.Orientation = Orientation.Horizontal;
                    centerVertGroup.Children.Add(centerGroup);

                    foreach (var docView in centerDocs)
                    {
                        LayoutDocument documentTab = contentList.FirstOrDefault(c => c.Content == docView);
                        if (documentTab != null)
                        {
                            LayoutDocumentPane pane = new LayoutDocumentPane();
                            centerGroup.Children.Add(pane);
                            pane.Children.Add(documentTab);
                        }
                    }
                }

                //create bottom
                if (bottomDocs.Count > 0)
                {
                    LayoutDocumentPaneGroup bottomGroup = new LayoutDocumentPaneGroup();
                    bottomGroup.Orientation = Orientation.Vertical;
                    centerVertGroup.Children.Add(bottomGroup);

                    foreach (var docView in bottomDocs)
                    {
                        LayoutDocument documentTab = contentList.FirstOrDefault(c => c.Content == docView);
                        if (documentTab != null)
                        {
                            LayoutDocumentPane pane = new LayoutDocumentPane();
                            bottomGroup.Children.Add(pane);
                            pane.Children.Add(documentTab);
                        }
                    }
                }

                //create left
                if (leftDocs.Count > 0)
                {
                    LayoutDocumentPaneGroup leftGroup = new LayoutDocumentPaneGroup();
                    leftGroup.Orientation = Orientation.Horizontal;
                    this.xDockingManager.Layout.RootPanel.Children.Insert(0, leftGroup);

                    foreach (var docView in leftDocs)
                    {
                        LayoutDocument documentTab = contentList.FirstOrDefault(c => c.Content == docView);
                        if (documentTab != null)
                        {
                            LayoutDocumentPane pane = new LayoutDocumentPane();
                            leftGroup.Children.Add(pane);
                            pane.Children.Add(documentTab);
                        }
                    }
                }


                //remove the temporary pane again
                this.xDockingManager.Layout.RootPanel.Children.Remove(tempGroup);

                //queue a zoom to fit all
                Dispatcher.BeginInvoke(
                    (Action)(() =>
                    {
                        if (this.ViewModel != null)
                        {
                            this.ViewModel.ZoomToFitCommand.Execute();
                        }

                    }), priority: System.Windows.Threading.DispatcherPriority.Render);

            }
        }

        /// <summary>
        /// Sets up the initial document layout
        /// </summary>
        public void SetupInitialLayout()
        {
            this.ViewModel.IsPropertiesPanelVisible = false;

            var frontDocument = this.DocumentControls.FirstOrDefault(d => d.GetDocument().DocumentType == ComponentEditorDocumentType.Front);
            var perspectiveDocument = this.DocumentControls.FirstOrDefault(d => d.GetDocument().DocumentType == ComponentEditorDocumentType.Perspective);
            var topDocument = this.DocumentControls.FirstOrDefault(d => d.GetDocument().DocumentType == ComponentEditorDocumentType.Top);

            //get a list of all layout content
            List<LayoutDocument> contentList = this.xDockingManager.Layout.Descendents().OfType<LayoutDocument>().ToList();

            //create a temp pane and move all children there
            LayoutDocumentPane tempPane = new LayoutDocumentPane();
            LayoutDocumentPaneGroup tempGroup = new LayoutDocumentPaneGroup(tempPane);
            this.xDockingManager.Layout.RootPanel.Children.Add(tempGroup);

            foreach (LayoutDocument documentTab in contentList)
            {
                tempPane.Children.Add(documentTab);
            }

            //remove all other panes
            foreach (ILayoutPanelElement ele in this.xDockingManager.Layout.RootPanel.Children.ToList())
            {
                if (ele != tempGroup)
                {
                    this.xDockingManager.Layout.RootPanel.Children.Remove(ele);
                }
            }


            this.xDockingManager.Layout.RootPanel.Orientation = Orientation.Horizontal;


            LayoutDocumentPaneGroup rightGroup = new LayoutDocumentPaneGroup();
            rightGroup.Orientation = Orientation.Vertical;
            this.xDockingManager.Layout.RootPanel.Children.Insert(0, rightGroup);

            LayoutDocument docTab =
                contentList.FirstOrDefault(c => c.Content == frontDocument);
            if (docTab != null)
            {
                LayoutDocumentPane pane = new LayoutDocumentPane();
                rightGroup.Children.Add(pane);
                pane.Children.Add(docTab);
            }

            docTab =
                contentList.FirstOrDefault(c => c.Content == perspectiveDocument);
            if (docTab != null)
            {
                LayoutDocumentPane pane = new LayoutDocumentPane();
                rightGroup.Children.Add(pane);
                pane.Children.Add(docTab);
            }

            LayoutDocumentPaneGroup leftGroup = new LayoutDocumentPaneGroup();
            leftGroup.Orientation = Orientation.Horizontal;
            this.xDockingManager.Layout.RootPanel.Children.Insert(0, leftGroup);


            docTab = contentList.FirstOrDefault(c => c.Content == topDocument);
            if (docTab != null)
            {
                LayoutDocumentPane pane = new LayoutDocumentPane();
                leftGroup.Children.Add(pane);
                pane.Children.Add(docTab);
            }

            //remove the temporary pane again
            this.xDockingManager.Layout.RootPanel.Children.Remove(tempGroup);


            //turn the properties panel back on
            this.ViewModel.IsPropertiesPanelVisible = true;

            //queue a zoom to fit all
            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    if (this.ViewModel != null)
                    {
                        this.ViewModel.ZoomToFitCommand.Execute();
                    }

                }), priority: System.Windows.Threading.DispatcherPriority.Render);


        }

        /// <summary>
        /// Creates the view control for the given document.
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        private static IComponentEditorDocumentView CreateDocumentView(IComponentEditorDocument doc)
        {
            switch (doc.DocumentType)
            {
                case ComponentEditorDocumentType.Front:
                case ComponentEditorDocumentType.Back:
                case ComponentEditorDocumentType.Bottom:
                case ComponentEditorDocumentType.Left:
                case ComponentEditorDocumentType.Perspective:
                case ComponentEditorDocumentType.Right:
                case ComponentEditorDocumentType.Top:
                    return new ComponentEditorVisualDocumentView(doc as ComponentEditorVisualDocument);

                case ComponentEditorDocumentType.SubComponentList:
                    return new SubComponentListDocumentView(doc as SubComponentListDocument);

                default:
                    return null;
            }
        }

        #endregion

        #region ILayoutUpdateStrategy Members

        /// <summary>
        /// Called before a document has been inserted
        /// </summary>
        /// <param name="layout">the layout to which it will be added.</param>
        /// <param name="anchorableToShow">the document itself.</param>
        /// <param name="destinationContainer">the specific destination panel or null if the document is new.</param>
        /// <returns>True if the document has been added by this method</returns>
        Boolean ILayoutUpdateStrategy.BeforeInsertDocument(LayoutRoot layout, LayoutDocument anchorableToShow, ILayoutContainer destinationContainer)
        {
            //Turn document floating off as it is causing
            // preview mouse move to trigger and lock the screen up.
            anchorableToShow.CanFloat = true;


            return false;
        }

        /// <summary>
        /// Called after the insertion of a document
        /// </summary>
        /// <param name="layout"></param>
        /// <param name="anchorableShown"></param>
        void ILayoutUpdateStrategy.AfterInsertDocument(LayoutRoot layout, LayoutDocument anchorableShown)
        {

        }

        /// <summary>
        /// Called before an anchorable is to be inserted.
        /// </summary>
        /// <param name="layout">the layout to which it will be added.</param>
        /// <param name="anchorableToShow">the anchorable itself.</param>
        /// <param name="destinationContainer">the specific destination panel or null if the anchorable is new.</param>
        /// <returns>True if the anchorable has been added by this method</returns>
        Boolean ILayoutUpdateStrategy.BeforeInsertAnchorable(LayoutRoot layout, LayoutAnchorable anchorableToShow, ILayoutContainer destinationContainer)
        {
            //Turn document floating off as it is causing
            // preview mouse move to trigger and lock the screen up.
            anchorableToShow.CanFloat = true;

            //If the pane has a specific destination then continue.
            if (destinationContainer != null
                /*&& destinationContainer.FindParent<LayoutFloatingWindow>() != null*/)
            {
                return false;
            }

            DockingPanelControl panel = anchorableToShow.Content as DockingPanelControl;
            if (panel != null)
            {
                LayoutAnchorablePane destPane =
                    layout.Descendents().OfType<LayoutAnchorablePane>().FirstOrDefault(d => d.Name == panel.GroupName);
                if (destPane == null)
                {
                    destPane = new LayoutAnchorablePane(anchorableToShow);
                    destPane.Name = panel.GroupName;
                    destPane.DockWidth = new GridLength(panel.DockedWidth, GridUnitType.Pixel);


                    //Add the pane acording to the placement strategy
                    AnchorableShowStrategy strategy = panel.PlacementStrategy;

                    Boolean most = (strategy & AnchorableShowStrategy.Most) == AnchorableShowStrategy.Most;
                    Boolean left = (strategy & AnchorableShowStrategy.Left) == AnchorableShowStrategy.Left;
                    Boolean right = (strategy & AnchorableShowStrategy.Right) == AnchorableShowStrategy.Right;
                    Boolean top = (strategy & AnchorableShowStrategy.Top) == AnchorableShowStrategy.Top;
                    Boolean bottom = (strategy & AnchorableShowStrategy.Bottom) == AnchorableShowStrategy.Bottom;


                    if (layout.RootPanel == null)
                        layout.RootPanel = new LayoutPanel() { Orientation = (left || right ? Orientation.Horizontal : Orientation.Vertical) };

                    if (left || right)
                    {
                        if (layout.RootPanel.Orientation == Orientation.Vertical &&
                            layout.RootPanel.ChildrenCount > 1)
                        {
                            layout.RootPanel = new LayoutPanel(layout.RootPanel);
                        }

                        layout.RootPanel.Orientation = Orientation.Horizontal;

                        if (left)
                            layout.RootPanel.Children.Insert(0, destPane);
                        else
                            layout.RootPanel.Children.Add(destPane);
                    }
                    else
                    {
                        if (layout.RootPanel.Orientation == Orientation.Horizontal &&
                            layout.RootPanel.ChildrenCount > 1)
                        {
                            layout.RootPanel = new LayoutPanel(layout.RootPanel);
                        }

                        layout.RootPanel.Orientation = Orientation.Vertical;

                        if (top)
                            layout.RootPanel.Children.Insert(0, destPane);
                        else
                            layout.RootPanel.Children.Add(destPane);
                    }

                    return true;
                }
                else
                {
                    destPane.Children.Add(anchorableToShow);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Called after the insertion of an anchorable.
        /// </summary>
        /// <param name="layout"></param>
        /// <param name="anchorableShown"></param>
        void ILayoutUpdateStrategy.AfterInsertAnchorable(LayoutRoot layout, LayoutAnchorable anchorableShown)
        {
            anchorableShown.Hiding += Panel_Hiding;
            anchorableShown.Closed += Panel_Closed;
        }

        #endregion

        #region window close

        /// <summary>
        /// On the application being closed by the cross button, check if changes may require saving
        /// </summary>
        protected override void OnCrossCloseRequested(CancelEventArgs e)
        {
            if (!e.Cancel)
            {
                if (!this.ViewModel.ContinueWithItemChange())
                    e.Cancel = true;
            }

            base.OnCrossCloseRequested(e);
        }


        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    this.ViewModel.IsPropertiesPanelVisible = false;

                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion

        #region Static Helpers

        /// <summary>
        /// Gets the status bar text for the given hover item.
        /// </summary>
        /// <param name="hoveredItem"></param>
        /// <returns></returns>
        public static String GetStatusBarText(EditorSubComponentView hoveredItem)
        {
            String newStatusText = null;

            if (hoveredItem != null)
            {
                newStatusText = hoveredItem.Name;
            }


            return newStatusText;
        }

        /// <summary>
        /// Helper to show/hide a specific panel.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="isVisible"></param>
        /// <param name="createPanelAction"></param>
        /// <param name="anchorables"></param>
        private static void ShowHidePanel<T>(Boolean isVisible, Action createPanelAction, IEnumerable<LayoutAnchorable> anchorables)
        {
            LayoutAnchorable panel = anchorables.FirstOrDefault(l => l.Content is T);

            if (isVisible)
            {
                if (panel != null)
                {
                    if (panel.IsHidden)
                    {
                        panel.Show();
                    }
                }
                else
                {
                    createPanelAction.Invoke();
                }
            }
            else
            {
                if (panel != null)
                {
                    if (!panel.IsHidden)
                    {
                        panel.Hide();
                    }
                }

            }

        }

        #endregion

    }
}
