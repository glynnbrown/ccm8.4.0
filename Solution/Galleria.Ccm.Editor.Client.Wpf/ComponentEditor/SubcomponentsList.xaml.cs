﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Galleria.Ccm.Editor.Client.Wpf.Common;

namespace Galleria.Ccm.Editor.Client.Wpf.ComponentEditor
{
    /// <summary>
    /// Interaction logic for SubComponentsList.xaml
    /// </summary>
    public sealed partial class SubComponentsList : DockingPanelControl
    {
        #region Properties

        #region ViewModel property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ComponentEditorViewModel), typeof(SubComponentsList),
            new PropertyMetadata(null));

        public ComponentEditorViewModel ViewModel
        {
            get { return (ComponentEditorViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion


        #endregion

        #region Constructor

        public SubComponentsList()
        {
            InitializeComponent();
        }

        #endregion
    }
}
