﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26495 : L.Ineson
//  Created.
#endregion
#endregion

using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.ComponentEditor
{
    public abstract class ComponentEditorDocument<T> : ViewModelAttachedControlObject<T>, IComponentEditorDocument
        where T : System.Windows.FrameworkElement
    {
        #region Fields

        private ComponentEditorViewModel _parentController;
        private Boolean _isActiveDocument;

        private Boolean _isZoomingSupported;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath IsActiveDocumentProperty = WpfHelper.GetPropertyPath<IComponentEditorDocument>(p => p.IsActiveDocument);
        public static readonly PropertyPath TitleProperty = WpfHelper.GetPropertyPath<IComponentEditorDocument>(p => p.Title);
        public static readonly PropertyPath ComponentProperty = WpfHelper.GetPropertyPath<IComponentEditorDocument>(p => p.Component);
        public static readonly PropertyPath SelectedSubComponentsProperty = WpfHelper.GetPropertyPath<IComponentEditorDocument>(p => p.SelectedSubComponents);

        public static readonly PropertyPath IsZoomingSupportedProperty = WpfHelper.GetPropertyPath<IComponentEditorDocument>(p => p.IsZoomingSupported);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the parent controller of this document.
        /// </summary>
        public ComponentEditorViewModel ParentController
        {
            get { return _parentController; }
        }

        /// <summary>
        /// Gets/Sets whether this is the active document for the application.
        /// </summary>
        public Boolean IsActiveDocument
        {
            get { return _isActiveDocument; }
            set
            {
                _isActiveDocument = value;
                OnPropertyChanged(IsActiveDocumentProperty);

            }
        }

        /// <summary>
        /// Returns the document title.
        /// </summary>
        public abstract String Title { get; }

        /// <summary>
        /// Returns the attached document view.
        /// </summary>
        public IComponentEditorDocumentView AttachedDocumentView
        {
            get { return this.AttachedControl as IComponentEditorDocumentView; }
        }

        /// <summary>
        /// Returns true if zooming is supported
        /// </summary>
        public Boolean IsZoomingSupported
        {
            get { return _isZoomingSupported; }
            protected set
            {
                _isZoomingSupported = value;
                OnPropertyChanged(IsZoomingSupportedProperty);
            }
        }


        #region Controller Inherited Properties

        /// <summary>
        /// Gets the source component model.
        /// </summary>
        public EditorComponentView Component
        {
            get { return _parentController.Component; }
        }

        /// <summary>
        /// Returns the view of selected plan items.
        /// </summary>
        public ComponentEditorSelection SelectedSubComponents
        {
            get { return _parentController.SelectedSubcomponents; }
        }

        /// <summary>
        /// Gets/Sets whether intensive updates should be supressed.
        /// </summary>
        public Boolean SuppressPerformanceIntensiveUpdates
        {
            get { return _parentController.SuppressPerformanceIntensiveUpdates; }
            set { _parentController.SuppressPerformanceIntensiveUpdates = value; }
        }

        /// <summary>
        /// Gets/Sets whether grid lines should be shown
        /// </summary>
        public Boolean ShowGridlines
        {
            get { return _parentController.ShowGridlines; }
        }

        /// <summary>
        /// Gets the major distance of fixed gridlines
        /// </summary>
        public Double GridlineMajorDist
        {
            get { return _parentController.GridlineMajorDist; }
        }

        /// <summary>
        /// Gets the minor distance of fixed gridlines.
        /// </summary>
        public Double GridlineMinorDist
        {
            get { return _parentController.GridlineMinorDist; }
        }

        public Double GridlineAreaHeight
        {
            get { return _parentController.GridlineAreaHeight; }
        }

        public Double GridlineAreaWidth
        {
            get { return _parentController.GridlineAreaWidth; }
        }

        public Double GridlineAreaDepth
        {
            get { return _parentController.GridlineAreaDepth; }
        }

        #endregion


        public abstract ComponentEditorDocumentType DocumentType { get; }


        #endregion

        #region Events

        #region Zooming

        /// <summary>
        /// Requests that a zoom be performed
        /// </summary>
        public event EventHandler<ZoomEventArgs> Zooming;

        private void OnZooming(ZoomType type, Object args = null)
        {
            if (Zooming != null)
            {
                Zooming(this, new ZoomEventArgs(type, args));
            }
        }

        #endregion

        #region Gridlines Changed

        public event EventHandler GridlinesChanged;

        private void OnGridlinesChanged()
        {
            if (GridlinesChanged != null)
            {
                GridlinesChanged(this, EventArgs.Empty);
            }
        }

        #endregion

        #endregion

        #region Constructor

        public ComponentEditorDocument(ComponentEditorViewModel parentController)
        {
            _parentController = parentController;
            _parentController.PropertyChanged += ParentController_PropertyChanged;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on the parent controller.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ParentController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ComponentEditorViewModel.ShowGridlinesProperty.Path
                || e.PropertyName == ComponentEditorViewModel.GridlineMajorDistProperty.Path
                || e.PropertyName == ComponentEditorViewModel.GridlineMinorDistProperty.Path
                || e.PropertyName == ComponentEditorViewModel.GridlineAreaHeightProperty.Path
                || e.PropertyName == ComponentEditorViewModel.GridlineAreaWidthProperty.Path
                || e.PropertyName == ComponentEditorViewModel.GridlineAreaDepthProperty.Path)
            {
                OnGridlinesChanged();
            }


            //pass through.
            OnParentControllerPropertyChanged(e.PropertyName);
        }

        protected virtual void OnParentControllerPropertyChanged(String propertyName)
        {
            //override if required.
        }

        #endregion

        #region Methods

        /// <summary>
        /// Processes the keyboard shortcut against any document 
        /// specific actions.
        /// </summary>
        /// <param name="modifiers"></param>
        /// <param name="key"></param>
        public virtual Boolean ProcessKeyboardShortcut(ModifierKeys modifiers, Key key)
        {
            //override if required.
            return false;
        }

        #region Zooming

        /// <summary>
        /// Zooms to fit the entire plan bounds
        /// </summary>
        public void ZoomToFit()
        {
            OnZooming(ZoomType.ZoomToFit);
        }

        /// <summary>
        /// Zooms to fit the plan by height
        /// </summary>
        public void ZoomToFitHeight()
        {
            OnZooming(ZoomType.ZoomToFitHeight);
        }

        /// <summary>
        /// Zoom to fit the plan width
        /// </summary>
        public void ZoomToFitWidth()
        {
            OnZooming(ZoomType.ZoomToFitWidth);
        }

        /// <summary>
        /// Zooms in on the plan
        /// </summary>
        public void ZoomIn()
        {
            OnZooming(ZoomType.ZoomIn);
        }

        /// <summary>
        /// Zooms the plan out
        /// </summary>
        public void ZoomOut()
        {
            OnZooming(ZoomType.ZoomOut);
        }

        /// <summary>
        /// Zooms to fit the currently selected plan items.
        /// </summary>
        public void ZoomSelection()
        {
            OnZooming(ZoomType.ZoomItems, this.SelectedSubComponents.ToList());
        }

        #endregion

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                DisposeBase();

                base.IsDisposed = true;
            }
        }

        protected void DisposeBase()
        {
            _parentController.PropertyChanged -= ParentController_PropertyChanged;
        }

        #endregion
    }
}
