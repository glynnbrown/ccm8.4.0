﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
// V8-25734 : L.Ineson
//  Moved out rendering controls to framework.
// V8-27242 : I.George
//  Added Zoom commands to the context menu
#endregion
#region Version History: (CCM 8.3.0)
// V8-31498 : L.Ineson
//  Added call to lighting helper.
#endregion
#endregion

using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Common.Rendering;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Controls.Wpf.HelperClasses;
using Galleria.Framework.Controls.Wpf.Rendering;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Controls.Wpf.Helpers;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Rendering;
using Galleria.Framework.Rendering.Interfaces;
using Galleria.Framework.ViewModel;
using HelixToolkit.Wpf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace Galleria.Ccm.Editor.Client.Wpf.ComponentEditor
{
    /// <summary>
    /// Interaction logic for ComponentEditorVisualDocumentView.xaml
    /// </summary>
    public sealed partial class ComponentEditorVisualDocumentView : UserControl, IComponentEditorDocumentView
    {
        #region Fields

        const Int32 _roundingPrecision = 1;

        private ModelConstruct3D _modelVisual;
        //private Boolean _isGridlineRefreshRequired;

        private Transform3DController _transformController;
        private Rotation3DController _rotationController;
        private Boolean _lockControllers;

        private Boolean _isDraggingAdorner;
        private DragSelectionAdorner _dragSelectionAdorner;
        private DragSelectionAdorner _orthPartCreator;
        private Model3DCreator _perspectivePartCreator;

        private ModelConstruct3D[] _previewModels = null; //models used to preview drags

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ComponentEditorVisualDocument), typeof(ComponentEditorVisualDocumentView),
            new UIPropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ComponentEditorVisualDocumentView senderControl = (ComponentEditorVisualDocumentView)obj;

            FixtureComponent3DData newModelData = null;

            if (e.OldValue != null)
            {
                ComponentEditorVisualDocument oldModel = (ComponentEditorVisualDocument)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
                oldModel.SelectedSubComponents.BulkCollectionChanged -= senderControl.ViewModel_SelectedSubComponentsCollectionChanged;

                oldModel.Zooming -= senderControl.ViewModel_Zooming;
                oldModel.GridlinesChanged -= senderControl.ViewModel_GridlinesChanged;
            }

            if (e.NewValue != null)
            {
                ComponentEditorVisualDocument newModel = (ComponentEditorVisualDocument)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
                newModel.SelectedSubComponents.BulkCollectionChanged += senderControl.ViewModel_SelectedSubComponentsCollectionChanged;

                newModel.Zooming += senderControl.ViewModel_Zooming;
                newModel.GridlinesChanged += senderControl.ViewModel_GridlinesChanged;

                newModelData = newModel.ComponentModelData;
            }

            FixtureComponent3DData oldModelData = (senderControl._modelVisual != null) ? senderControl._modelVisual.ModelData as FixtureComponent3DData : null;
            senderControl.ViewModel_Model3DDataChanged(oldModelData, newModelData);
        }



        public ComponentEditorVisualDocument ViewModel
        {
            get { return (ComponentEditorVisualDocument)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="viewModel"></param>
        public ComponentEditorVisualDocumentView(ComponentEditorVisualDocument viewModel)
        {
            InitializeComponent();

            this.ViewModel = viewModel;

            SetupForViewType();

            PlanCameraHelper.SetPanningMode(this.xViewer, false);
        }

        #endregion

        #region Event Handlers

        #region Local Overrides

        /// <summary>
        /// Called whenever a key up occurs on this control
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewKeyUp(KeyEventArgs e)
        {
            base.OnPreviewKeyUp(e);

            if (e.Key == Key.LeftCtrl || e.Key == Key.RightCtrl
                || e.Key == Key.LeftShift || e.Key == Key.RightShift)
            {
                this.ViewModel.Component.EndUndoableAction();
            }
        }

        /// <summary>
        /// Called when a drag drop action is moved over this.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnDragOver(DragEventArgs e)
        {
            base.OnDragOver(e);

            if (DragDropBehaviour.IsUnpackedType<ComponentEditorItemDragDropArgs>(e.Data))
            {
                OnSubComponentDragOver(DragDropBehaviour.UnpackData<ComponentEditorItemDragDropArgs>(e.Data), e);
            }
        }

        /// <summary>
        /// Called whenever a drag behaviour drops here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void OnDrop(DragEventArgs e)
        {
            base.OnDrop(e);

            if (DragDropBehaviour.IsUnpackedType<ComponentEditorItemDragDropArgs>(e.Data))
            {
                OnSubComponentDropped(DragDropBehaviour.UnpackData<ComponentEditorItemDragDropArgs>(e.Data), e);
            }

            //tidy up any leftover dragging models
            ClearPreviewModels();
        }

        #endregion

        #region ViewModel Event Handlers

        /// <summary>
        /// Called when the planogram model changes
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        private void ViewModel_Model3DDataChanged(IModelConstruct3DData oldModel, IModelConstruct3DData newModel)
        {
            if (_modelVisual != null)
            {
                _modelVisual = null;
            }

            //clear the viewer children.
            if (this.xViewer != null)
            {
                foreach (Visual3D visual in this.xViewer.Children.ToList())
                {
                    if (visual is ModelConstruct3D)
                    {
                        this.xViewer.Children.Remove(visual);
                    }
                }
            }


            if (newModel != null)
            {
                _modelVisual = new ModelConstruct3D(newModel);

                if (this.xViewer != null)
                {
                    this.xViewer.Children.Add(_modelVisual);
                    UpdateGridlines();
                }
            }

        }

        /// <summary>
        /// Called when a property changes on the viewmodel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ComponentEditorVisualDocument.IsActiveDocumentProperty.Path)
            {
                if (this.ViewModel.IsActiveDocument)
                {
                    //force keyboard focus on the viewer
                    if (Keyboard.FocusedElement != this.xViewer)
                    {
                        Keyboard.Focus(this.xViewer);
                    }
                }
            }
            else if (e.PropertyName == ComponentEditorVisualDocument.Model3DDataProperty.Path)
            {
                ViewModel_Model3DDataChanged((Plan3DData)_modelVisual.ModelData, this.ViewModel.ComponentModelData);
            }
            else if (e.PropertyName == ComponentEditorVisualDocument.IsInZoomToAreaModeProperty.Path)
            {
                PlanCameraHelper.SetZoomToAreaMode(this.xViewer, this.ViewModel.IsInZoomToAreaMode);
            }
            else if (e.PropertyName == ComponentEditorVisualDocument.IsInPanningModeProperty.Path)
            {
                PlanCameraHelper.SetPanningMode(this.xViewer, this.ViewModel.IsInPanningMode);
            }
            else if (e.PropertyName == ComponentEditorVisualDocument.MouseToolTypeProperty.Path)
            {
                CancelPerspectivePartCreator();

                if (this.ViewModel.MouseToolType == MouseToolType.CreatePart)
                {
                    this.Cursor = Cursors.Cross;
                }
                else
                {
                    this.Cursor = null;
                }

                SetModelController();
            }
        }

        /// <summary>
        /// Called when the contents of the viewmodel SelectedPlanItems collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_SelectedSubComponentsCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            SetModelController();
        }

        /// <summary>
        /// Called when the viewmodel requests a zoom
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_Zooming(object sender, ZoomEventArgs e)
        {
            switch (e.Mode)
            {
                case ZoomType.ZoomToFit:
                    ZoomToFit();
                    break;

                case ZoomType.ZoomIn:
                    PlanCameraHelper.ZoomIn(this.xViewer);
                    break;

                case ZoomType.ZoomOut:
                    PlanCameraHelper.ZoomOut(this.xViewer);
                    break;

                case ZoomType.ZoomItems:
                    ZoomItems((IEnumerable<EditorSubComponentView>)e.Parameter);
                    break;

                default:
                    Debug.Fail("Not implemented");
                    ZoomToFit();
                    break;
            }
        }

        /// <summary>
        /// Called whenever the viewmodel fires the gridlines changed event.
        /// </summary>
        private void ViewModel_GridlinesChanged(object sender, EventArgs e)
        {
            UpdateGridlines();
            ZoomToFit();
        }

        #endregion

        #region Viewer Event Handlers

        /// <summary>
        /// Called when the xviewer is initially loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xViewer_Loaded(object sender, RoutedEventArgs e)
        {
            this.xViewer.Loaded -= xViewer_Loaded;

            if (this.xViewer.CameraController == null)
            {
                //force assignment.
                this.xViewer.ApplyTemplate();
            }

            if (this.xViewer.CameraController != null)
            {
                //stupid fix to stop this mucking up the docpanel.
                // as otherwise camera controller keeps stealing keyboard focus
                this.xViewer.CameraController.Focusable = false;

                if (!this.xViewer.IsMeasureValid)
                {
                    this.xViewer.SizeChanged += xViewer_InitialSizeSet;
                }
                else
                {
                    ZoomToFit();
                }
            }
        }

        /// <summary>
        /// Called whene viewer size is initialized
        /// </summary>
        private void xViewer_InitialSizeSet(object sender, SizeChangedEventArgs e)
        {
            if (this.xViewer.IsMeasureValid)
            {
                this.xViewer.SizeChanged -= xViewer_InitialSizeSet;
                ZoomToFit();
            }
        }

        /// <summary>
        /// Called when any mouse button is pressed on the viewer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xViewer_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //force keyboard focus on the viewer
            Boolean isZoomingByRect = (this.ViewModel != null && this.ViewModel.IsInZoomToAreaMode);

            if (!isZoomingByRect && Keyboard.FocusedElement != this.xViewer)
            {
                Keyboard.Focus(this.xViewer);
            }
        }

        /// <summary>
        /// Responds to a mouse left click on the viewport
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xViewer_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //return straight out if we have no viewmodel.
            if (this.ViewModel == null) { return; }
            if (this.ViewModel.IsInZoomToAreaMode) { return; } //or if we are in zoom to area mode
            if (this.ViewModel.IsInPanningMode) { return; } //or if we are in panning mode.


            if (this.ViewModel.MouseToolType == MouseToolType.CreatePart)
            {
                //we in create part mode.
                if (this.ViewModel.ViewType != CameraViewType.Perspective)
                {
                    BeginOrthPartCreator(e);
                }
                else
                {
                    BeginPerspectivePartCreator(e);
                }

                e.Handled = true;
                return;
            }

            //update the current selection
            this.ViewModel.ProcessUserClickSelection(GetHitPlanItem(e), Keyboard.Modifiers, /*mouseUp*/false);

            //start a drag selection if no modifier is active.
            if (Keyboard.Modifiers == ModifierKeys.Shift
                && this.ViewModel.MouseToolType == MouseToolType.Pointer)
            {
                _dragSelectionAdorner = new DragSelectionAdorner(this.xViewerCanvas, e.GetPosition(this.xViewerCanvas));
            }
        }

        /// <summary>
        /// Responds to a mouse left up on the viewport
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xViewer_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Boolean handled = false;

            handled = this.ViewModel.IsInZoomToAreaMode;

            if (!handled)
            {

                //remove selected item
                this.ViewModel.ProcessUserClickSelection(GetHitPlanItem(e), Keyboard.Modifiers, /*mouseUp*/true);

                handled = PerspectivePartCreator_ProcesssMouseLeftButtonUp(e);
            }

            if (!handled)
            {
                //handled = MouseToolType_ProcessMouseUp(e);
            }
        }

        /// <summary>
        /// Called when the viewer is double clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xViewer_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.ViewModel != null)
            {
                if (this.ViewModel.SelectedSubComponents.Count > 0)
                {
                    this.ViewModel.ParentController.ShowItemPropertiesCommand.Execute();
                }
                e.Handled = true;
            }
        }

        /// <summary>
        /// Called when a preview mouse right button down on the viewer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xViewer_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            //nb cannot do this for perspective view as it messes with rotation!
            if (this.ViewModel != null && this.ViewModel.ViewType != CameraViewType.Perspective)
            {
                this.ViewModel.ProcessUserClickSelection(GetHitPlanItem(e, false), Keyboard.Modifiers, /*isMouseUp*/false);

                if (Keyboard.Modifiers == ModifierKeys.None)
                {
                    ShowContextMenu();
                }
            }
            ClearPreviewModels();
        }

        /// <summary>
        /// Called whenever the viewer notifies of a preview mouse move event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xViewer_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            //return straight out if the camera controller is processing.
            if (this.ViewModel != null
                && (this.ViewModel.IsInZoomToAreaMode || this.ViewModel.IsInPanningMode)) { return; }


            if (e.LeftButton == MouseButtonState.Pressed && !DragDropBehaviour.IsDragging)
            {
                if (Keyboard.Modifiers == ModifierKeys.None || Keyboard.Modifiers == ModifierKeys.Control)
                {
                    if (this.ViewModel != null && this.ViewModel.MouseToolType == MouseToolType.Pointer
                        && !_isDraggingAdorner)
                    {
                        BeginSubComponentDrag(e);
                    }
                }
            }
        }

        /// <summary>
        /// Called as the mouse moves over the viewer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xViewer_MouseMove(object sender, MouseEventArgs e)
        {
            Boolean handled = false;

            handled = this.ViewModel.IsInZoomToAreaMode || this.ViewModel.IsInPanningMode;

            if (!handled && MouseToolTypeHelper.IsCreateType(this.ViewModel.MouseToolType))
            {
                if (this.ViewModel.ViewType == CameraViewType.Perspective)
                {
                    handled = PerspectivePartCreator_ProcessMouseMove(e);
                }
                else
                {
                    handled = OrthPartCreator_ProcessMouseMove(e);
                }
            }

            if (!handled)
            {
                #region Update status bar

                if (this.ViewModel != null)
                {
                    PointValue hitPoint3D = ModelConstruct3DHelper.GetModelPoint3D(e.GetPosition(this.xViewer), this.xViewer.Viewport);

                    switch (this.ViewModel.ViewType)
                    {
                        case CameraViewType.Front:
                        case CameraViewType.Back:
                            this.ViewModel.ParentController.StatusBarText =
                                String.Format("({0}: {1}, {2}: {3})",
                                AxisTypeHelper.FriendlyNames[AxisType.X], Math.Round(hitPoint3D.X, 2),
                                AxisTypeHelper.FriendlyNames[AxisType.Y], Math.Round(hitPoint3D.Y, 2));
                            break;

                        case CameraViewType.Right:
                        case CameraViewType.Left:
                            this.ViewModel.ParentController.StatusBarText =
                                String.Format("({0}: {1}, {2}: {3})",
                                AxisTypeHelper.FriendlyNames[AxisType.Z], Math.Round(hitPoint3D.Z, 2),
                                AxisTypeHelper.FriendlyNames[AxisType.Y], Math.Round(hitPoint3D.Y, 2));
                            break;

                        case CameraViewType.Top:
                        case CameraViewType.Bottom:
                            this.ViewModel.ParentController.StatusBarText =
                                String.Format("({0}: {1}, {2}: {3})",
                                AxisTypeHelper.FriendlyNames[AxisType.X], Math.Round(hitPoint3D.X, 2),
                                AxisTypeHelper.FriendlyNames[AxisType.Z], Math.Round(hitPoint3D.Z, 2));
                            break;

                        default:
                            this.ViewModel.ParentController.StatusBarText =
                                String.Format("({0}: {1}, {2}: {3}, {4}: {5})",
                                AxisTypeHelper.FriendlyNames[AxisType.X], Math.Round(hitPoint3D.X, 2),
                                AxisTypeHelper.FriendlyNames[AxisType.Y], Math.Round(hitPoint3D.Y, 2),
                                AxisTypeHelper.FriendlyNames[AxisType.Z], Math.Round(hitPoint3D.Z, 2));
                            break;
                                
                    }

                }
                #endregion


                //handled = MouseToolType_ProcessMouseMove(e);
            }

            if (!handled)
            {
                //process any ongoing drag selection
                handled = DragSelectionAdorner_ProcessMouseMove(e);
            }

        }

        /// <summary>
        /// Called when the mouse leaves the viewer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xViewer_MouseLeave(object sender, MouseEventArgs e)
        {
            //reset parent controller status text back to null.
            //if (this.ViewModel != null)
            //{
            //    this.ViewModel.UpdateStatusBarText(null);
            //    App.MainPageViewModel.StatusBarMouseText = null;
            //}


            if (!this.IsMouseOver)
            {
                ClearPreviewModels();
            }
        }

        /// <summary>
        /// Called when the view camera changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xViewer_CameraChanged(object sender, RoutedEventArgs e)
        {
            //copy over the camera to the manipulators view.
            CameraHelper.Copy(this.xViewer.Camera, this.xManipulatorViewer.Camera as ProjectionCamera);
        }

        /// <summary>
        /// Called when the viewer zooms by rectangle
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xViewer_ZoomedByRectangle(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel != null
                && this.ViewModel.ViewType != CameraViewType.Perspective)
            {
                ProjectionCamera camera = this.xViewer.Camera as ProjectionCamera;


                //flip the camera look direction.
                Vector3D newLookDirection =
                    new Vector3D(camera.LookDirection.X, camera.LookDirection.Y, camera.LookDirection.Z);

                switch (this.ViewModel.ViewType)
                {
                    case CameraViewType.Front:
                    case CameraViewType.Back:
                        newLookDirection.Z = -newLookDirection.Z;
                        break;

                    case CameraViewType.Left:
                    case CameraViewType.Right:
                        newLookDirection.X = -newLookDirection.X;
                        break;

                    case CameraViewType.Top:
                    case CameraViewType.Bottom:
                        newLookDirection.Y = -newLookDirection.Y;
                        break;
                }


                camera.LookDirection = newLookDirection;


                if (this.ViewModel.IsInZoomToAreaMode)
                {
                    this.ViewModel.IsInZoomToAreaMode = false;
                }
            }
        }

        #endregion

        #endregion

        #region Camera

        /// <summary>
        /// Performs any setup actions required for the selected viewtype.
        /// </summary>
        private void SetupForViewType()
        {
            if (this.xViewer == null || this.xManipulatorViewer == null)
            {
                ApplyTemplate();
            }

            RenderOptions.SetBitmapScalingMode(this.xViewer, BitmapScalingMode.NearestNeighbor);

            CameraViewType viewType = this.ViewModel.ViewType;

            //Add lighting.
            PlanLightingHelper.SetupDefaultLighting(this.xViewer, viewType);

            //reset the manipulator camera
            Viewport3D manipulatorViewport = this.xManipulatorViewer;
            if (viewType == CameraViewType.Perspective)
            {
                manipulatorViewport.Camera = new PerspectiveCamera();
            }
            else
            {
                RenderOptions.SetEdgeMode(manipulatorViewport, EdgeMode.Aliased);
                manipulatorViewport.Camera = new OrthographicCamera();
            }

            //set the main camera
            PlanCameraHelper.SetupForCameraType(this.xViewer, viewType);
        }

        /// <summary>
        /// Zooms the plan to fit.
        /// </summary>
        public void ZoomToFit()
        {
            Rect3D bounds3D = new Rect3D();
            
            bounds3D.SizeX =
                Math.Max(_modelVisual.Position.X + _modelVisual.Size.Width, this.ViewModel.GridlineAreaWidth);


            bounds3D.SizeY =
               Math.Max(_modelVisual.Position.Y + _modelVisual.Size.Height, this.ViewModel.GridlineAreaHeight);

            bounds3D.SizeZ =
               Math.Max(_modelVisual.Position.Z + _modelVisual.Size.Depth, this.ViewModel.GridlineAreaDepth);


            PlanCameraHelper.ZoomToFit(this.xViewer, bounds3D);
        }

        /// <summary>
        /// Zooms to fit the currently selected plan items.
        /// </summary>
        private void ZoomItems(IEnumerable<EditorSubComponentView> items)
        {
            if (items.Any())
            {
                if (this.ViewModel != null)
                {
                    //get all of the selected models
                    List<ModelVisual3D> selectedModels = new List<ModelVisual3D>();
                    foreach (var item in items)
                    {
                        ModelVisual3D visual = RenderControlsHelper.FindItemModel(_modelVisual, item) as ModelVisual3D;
                        if (visual != null)
                        {
                            selectedModels.Add(visual);
                        }
                    }

                    //Perform the zoom
                    PlanCameraHelper.ZoomItems(this.xViewer, selectedModels);
                }
            }
            else
            {
                ZoomToFit();
            }
        }

        #endregion

        #region Model Controllers / Mouse Tools

        #region Transform / Rotate Controllers

        /// <summary>
        /// Updates the model controller state.
        /// </summary>
        private void SetModelController()
        {
            Viewport3D viewer = this.xManipulatorViewer;

            if (this.ViewModel != null && viewer != null)
            {
                Boolean clearTransformController = !_lockControllers;
                Boolean clearRotationController = !_lockControllers;

                //tidy up any unrequired controllers
                if (clearTransformController && _transformController != null)
                {
                    _transformController.Visibility = System.Windows.Visibility.Collapsed;
                    BindingOperations.ClearBinding(_transformController, Transform3DController.CenterProperty);

                }
                if (clearRotationController && _rotationController != null)
                {
                    _rotationController.Visibility = System.Windows.Visibility.Collapsed;
                    BindingOperations.ClearBinding(_rotationController, Transform3DController.CenterProperty);
                }



                var selection = this.ViewModel.SelectedSubComponents;
                if (selection.Count > 0 && this.ViewModel.MouseToolType != MouseToolType.Pointer)
                {
                    EditorSubComponentView item = selection.Last();

                    switch (this.ViewModel.MouseToolType)
                    {
                        case MouseToolType.Transform:
                            {
                                clearTransformController = false;

                                //create the controller if necessary
                                if (_transformController == null)
                                {
                                    _transformController = new Transform3DController();
                                    _transformController.Visibility = Visibility.Collapsed;
                                    viewer.Children.Add(_transformController);

                                    SetEventHandlers(_transformController, /*isSubscribe*/true);
                                }

                                //attach the controller to the item.
                                SetTransformController(_transformController, item, _modelVisual, viewer);
                            }
                            break;

                        case MouseToolType.Rotate:
                            {
                                clearRotationController = false;

                                if (_rotationController == null)
                                {
                                    _rotationController = new Rotation3DController();
                                    _rotationController.Visibility = System.Windows.Visibility.Collapsed;
                                    viewer.Children.Add(_rotationController);

                                    SetEventHandlers(_rotationController, /*isSubscribe*/true);
                                }

                                //attach the controller to the item.
                                SetRotationController(_rotationController, item, _modelVisual, viewer);
                            }
                            break;

                    }

                }

            }

        }

        /// <summary>
        /// Updates the given transform controller as per the given item.
        /// </summary>
        /// <param name="transformController"></param>
        /// <param name="item"></param>
        /// <param name="parentFixtureModel"></param>
        /// <param name="viewer"></param>
        public static void SetTransformController(Transform3DController transformController, EditorSubComponentView item,
            ModelConstruct3D parentFixtureModel, Viewport3D viewer)
        {
            ModelConstruct3D model = null;

            IModelConstruct3DData modelData = RenderControlsHelper.FindItemModelData(parentFixtureModel.ModelData as ModelConstruct3DData, item);
            if (modelData != null)
            {
                model = parentFixtureModel.FindModel(modelData);
            }

            if (model != null)
            {
                model.UpdateModelPosition();

                BindingOperations.SetBinding(transformController, Transform3DController.CenterProperty,
                   new Binding(ModelConstruct3D.WorldCenterProperty.Name) { Source = model });

                if (viewer.Camera is OrthographicCamera)
                {
                    Vector3D moveVector = ModelConstruct3DHelper.FindItemMoveVector(viewer.Camera as ProjectionCamera, 2, 2);
                    transformController.CanTransformX = (moveVector.X != 0);
                    transformController.CanTransformY = (moveVector.Y != 0);
                    transformController.CanTransformZ = (moveVector.Z != 0);

                    Rect3D modelBounds = model.GetBounds();
                    if (!modelBounds.IsEmpty)
                    {
                        Double length = transformController.Length;
                        Double minBoundsLength = new Double[] { modelBounds.SizeX, modelBounds.SizeY, modelBounds.SizeY }.Min();
                        length = Math.Max(length, minBoundsLength * 0.1);

                        transformController.Length = length;
                    }

                }


                transformController.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Updates the given rotation controller as per the given item.
        /// </summary>
        /// <param name="transformController"></param>
        /// <param name="item"></param>
        /// <param name="parentFixtureModel"></param>
        /// <param name="viewer"></param>
        public static void SetRotationController(Rotation3DController rotateController, EditorSubComponentView item,
            ModelConstruct3D parentFixtureModel, Viewport3D viewer)
        {
            ModelConstruct3D model = null;

            IModelConstruct3DData modelData = RenderControlsHelper.FindItemModelData(parentFixtureModel.ModelData as ModelConstruct3DData, item);
            if (modelData != null)
            {
                model = parentFixtureModel.FindModel(modelData);
            }

            if (model != null)
            {
                model.UpdateModelPosition();

                BindingOperations.SetBinding(rotateController, Rotation3DController.CenterProperty,
                       new Binding(ModelConstruct3D.WorldCenterProperty.Name) { Source = model });

                rotateController.Length = 1D;
                rotateController.InnerDiameter = 25D;
                rotateController.Diameter = 26D;


                if (viewer.Camera is OrthographicCamera)
                {
                    Vector3D moveVector = ModelConstruct3DHelper.FindItemMoveVector(viewer.Camera as ProjectionCamera, 2, 2);
                    rotateController.CanRotateX = (moveVector.X == 0);
                    rotateController.CanRotateY = (moveVector.Y == 0);
                    rotateController.CanRotateZ = (moveVector.Z == 0);
                }
            }

            rotateController.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Hooks/Unhooks the event handlers for the given transform controller
        /// </summary>
        /// <param name="ctl"></param>
        /// <param name="isSubscribe"></param>
        private void SetEventHandlers(Transform3DController ctl, Boolean isSubscribe)
        {
            if (isSubscribe)
            {
                ctl.PositionValueChangeBeginning += PositionController_ValueChangeBeginning;
                ctl.PositionValueChanged += PositionController_ValueChanged;
                ctl.PositionValueChangeComplete += Model3DController_ValueChangeComplete;

                ctl.SizeValueChangeBeginning += SizeController_ValueChangeBeginning;
                ctl.SizeValueChanged += SizeController_ValueChanged;
                ctl.SizeValueChangeComplete += Model3DController_ValueChangeComplete;
            }
            else
            {
                ctl.PositionValueChangeBeginning -= PositionController_ValueChangeBeginning;
                ctl.PositionValueChanged -= PositionController_ValueChanged;
                ctl.PositionValueChangeComplete -= Model3DController_ValueChangeComplete;

                ctl.SizeValueChangeBeginning -= SizeController_ValueChangeBeginning;
                ctl.SizeValueChanged -= SizeController_ValueChanged;
                ctl.SizeValueChangeComplete -= Model3DController_ValueChangeComplete;
            }

        }

        /// <summary>
        /// Hooks/Unhooks the event handlers for the given rotation controller
        /// </summary>
        /// <param name="ctl"></param>
        /// <param name="isSubscribe"></param>
        private void SetEventHandlers(Rotation3DController ctl, Boolean isSubscribe)
        {
            if (isSubscribe)
            {
                ctl.ValueChangeBeginning += RotateController_ValueChangeBeginning;
                ctl.ValueChanged += RotateController_ValueChanged;
                ctl.ValueChangeComplete += Model3DController_ValueChangeComplete;
            }
            else
            {
                ctl.ValueChangeBeginning -= RotateController_ValueChangeBeginning;
                ctl.ValueChanged -= RotateController_ValueChanged;
                ctl.ValueChangeComplete -= Model3DController_ValueChangeComplete;
            }

        }

        /// <summary>
        /// Called when a size controller begins its manipulation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SizeController_ValueChangeBeginning(object sender, EventArgs e)
        {
            this.ViewModel.Component.BeginUndoableAction(Message.ModelUndoAction_ResizeItem);

            //suppress updates to improve performance
            this.ViewModel.SuppressPerformanceIntensiveUpdates = true;
        }

        /// <summary>
        /// Called whenever a size controller value changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SizeController_ValueChanged(object sender, Controller3DValueChangedArgs e)
        {
            Vector3D vector = e.ChangeVector;

            e.Cancelled =
                !ComponentEditorHelper.ResizeItems(this.ViewModel.SelectedSubComponents, 
                Math.Round(vector.X, _roundingPrecision), 
                Math.Round(vector.Y, _roundingPrecision), 
                Math.Round(vector.Z, _roundingPrecision));
        }

        /// <summary>
        /// Called when a position controller begins its manipulation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PositionController_ValueChangeBeginning(object sender, EventArgs e)
        {
            _lockControllers = true;
            this.ViewModel.Component.BeginUndoableAction(Message.ModelUndoAction_MoveItem);

            //if ctrl is pressed then create a new copy of the items.
            if (Keyboard.Modifiers == ModifierKeys.Control)
            {
                List<EditorSubComponentView> newItems = new List<EditorSubComponentView>();
                foreach (EditorSubComponentView item in this.ViewModel.SelectedSubComponents.ToList())
                {
                    newItems.Add(this.ViewModel.Component.AddSubComponentCopy(item));
                }
                this.ViewModel.SelectedSubComponents.Clear();
                this.ViewModel.SelectedSubComponents.AddRange(newItems);
            }


            //suppress updates to improve performance
            this.ViewModel.SuppressPerformanceIntensiveUpdates = true;
        }

        /// <summary>
        /// Called whenever a position controller value changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PositionController_ValueChanged(object sender, Controller3DValueChangedArgs e)
        {
            Vector3D vector = e.ChangeVector;

            ComponentEditorHelper.RepositionItems(this.ViewModel.SelectedSubComponents,
                Math.Round(vector.X, _roundingPrecision),
                Math.Round(vector.Y, _roundingPrecision),
                Math.Round(vector.Z, _roundingPrecision));
        }

        /// <summary>
        /// Called when a rotate controller begins its manipulation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RotateController_ValueChangeBeginning(object sender, EventArgs e)
        {
            this.ViewModel.Component.BeginUndoableAction(Message.ModelUndoAction_RotateItem);

            //suppress updates to improve performance
            this.ViewModel.SuppressPerformanceIntensiveUpdates = true;
        }

        /// <summary>
        /// Called whenever a rotate controller value changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RotateController_ValueChanged(object sender, EventArgs<Vector3D> e)
        {
            ComponentEditorHelper.RotateItemsAboutCenter(
                this.ViewModel.SelectedSubComponents,
               e.ReturnValue.X, 
                e.ReturnValue.Y, 
                e.ReturnValue.Z);
        }

        /// <summary>
        /// Called when a mouse tool controller completes its manipulation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model3DController_ValueChangeComplete(object sender, EventArgs e)
        {
            this.ViewModel.Component.EndUndoableAction();

            //unsupress updates
            this.ViewModel.SuppressPerformanceIntensiveUpdates = false;

            _lockControllers = false;
        }

        #endregion

        #region Orth Part Creator

        /// <summary>
        /// Starts a new orth part creator if we are not already
        /// processing one.
        /// </summary>
        private void BeginOrthPartCreator(MouseEventArgs e)
        {
            if (_orthPartCreator == null)
            {
                _orthPartCreator = new DragSelectionAdorner(this.xViewerCanvas, e.GetPosition(this.xViewerCanvas));
                _orthPartCreator.PenColour = Brushes.DarkRed;
                _orthPartCreator.PenDashStyle = null;
            }
        }

        /// <summary>
        /// Carries out any orth part creator processing required when the mouse moves.
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        private Boolean OrthPartCreator_ProcessMouseMove(MouseEventArgs e)
        {
            Boolean handled = false;

            if (_orthPartCreator != null)
            {
                if (_isDraggingAdorner == false && e.LeftButton == MouseButtonState.Pressed)
                {
                    Point curMousePos = e.GetPosition(this.xViewerCanvas);
                    Point startPos = _orthPartCreator.StartPoint.Value;
                    Double difTolerance = 4;

                    //only start the drag if the mouse has moved enough.
                    if ((curMousePos.X - startPos.X) > difTolerance
                        || (startPos.X - curMousePos.X) > difTolerance
                        || (curMousePos.Y - startPos.Y) > difTolerance
                        || (startPos.Y - curMousePos.Y) > difTolerance)
                    {
                        AdornerLayer aLayer = AdornerLayer.GetAdornerLayer(this.xViewerCanvas);
                        if (aLayer != null)
                        {
                            _orthPartCreator.DragSizeChanged += OrthPartCreator_DragSizeChanged;
                            _orthPartCreator.DragSelectionCompleted += OrthPartCreator_PartDefinitionCompleted;
                            aLayer.Add(_orthPartCreator);
                        }

                        _isDraggingAdorner = true;
                        handled = true;
                    }
                }
            }


            return handled;
        }

        /// <summary>
        /// Cancels the ongoing orth part creator tool.
        /// </summary>
        private void CancelOrthPartCreator()
        {
            if (_orthPartCreator != null)
            {
                _orthPartCreator.DragSizeChanged -= OrthPartCreator_DragSizeChanged;
                _orthPartCreator.DragSelectionCompleted -= OrthPartCreator_PartDefinitionCompleted;

                _isDraggingAdorner = false;
                _orthPartCreator.ReleaseMouseCapture();
                _orthPartCreator = null;
            }
        }

        /// <summary>
        /// Called when the drag size changes on the orth part creator.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OrthPartCreator_DragSizeChanged(object sender, RoutedEventArgs e)
        {
            DragSelectionAdorner senderControl = (DragSelectionAdorner)sender;

            Rect rct = senderControl.GetSelectionRect();
            if (!rct.IsEmpty)
            {
                String displayTxt = null;

                var partBounds = GetPartDefinitionSize(rct, this.ViewModel.MouseToolType);
                if (!partBounds.IsEmpty())
                {
                    displayTxt = String.Format("{0} x {1} x {2}", partBounds.Height, partBounds.Width, partBounds.Depth);
                }

                senderControl.DisplayText = displayTxt;
            }
        }

        /// <summary>
        /// Called when the model 3d creator completes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OrthPartCreator_PartDefinitionCompleted(object sender, RoutedEventArgs e)
        {
            DragSelectionAdorner senderControl = (DragSelectionAdorner)sender;
            CancelOrthPartCreator();

            //reset the mouse tooltype back
            MouseToolType toolType = this.ViewModel.MouseToolType;
            if (!MouseToolTypeHelper.IsCreateType(toolType)) { return; }

            Rect rct = senderControl.GetSelectionRect();
            if (!rct.IsEmpty)
            {
                var partBounds = GetPartDefinitionSize(rct, this.ViewModel.MouseToolType);
                if (!partBounds.IsEmpty())
                {
                    this.ViewModel.CreateNewFixturePart(
                        partBounds.X, 
                        partBounds.Y, 
                        partBounds.Z,
                        partBounds.Height,
                        partBounds.Width,
                        partBounds.Depth);


                    //create the part
                    //Size3D newModelSize = new Size3D(partBounds.Width, partBounds.Height, partBounds.Depth);
                    //Point3D newModelPos = new Point3D(partBounds.X, partBounds.Y, partBounds.Z);

                    //// hit test each corner and the center of the rect getting the hit model which 
                    //// is closest to the camera
                    //Viewport3DHelper.HitResult hitResult = null;
                    //Double currCameraDist = Double.PositiveInfinity;
                    //ModelConstruct3D hitVisual = null;

                    //foreach (Point p in
                    //    new Point[]{ rct.BottomLeft, rct.BottomRight, rct.TopLeft, rct.TopRight,
                    //    new Point(rct.BottomLeft.X + rct.Width/2, rct.BottomLeft.Y + rct.Height/2)})
                    //{
                    //    var hits = Viewport3DHelper.FindHits(this.xViewer.Viewport, p);
                    //    foreach (var h in hits)
                    //    {
                    //        ModelConstruct3D visual = h.Visual.FindVisualAncestor<ModelConstruct3D>();
                    //        if (visual != null)
                    //        {
                    //            Double cameraDist = ModelConstruct3DHelper.GetCameraDistance(
                    //                ModelConstruct3DHelper.FindBounds(visual), this.xViewer.Camera.Position);

                    //            if (cameraDist < currCameraDist)
                    //            {
                    //                hitVisual = visual;
                    //                currCameraDist = cameraDist;
                    //                hitResult = h;
                    //            }
                    //            break;
                    //        }
                    //    }
                    //}
                    //if (hitVisual != null)
                    //{
                    //    //we hit something so correct the model position to make it more accurate.
                    //    Point3D hitModelPos = Model3DHelper.ToPoint3D(hitVisual.Position);
                    //    newModelPos = hitModelPos + hitResult.Normal;
                    //}



                    //this.ViewModel.CreateNewFixturePart(
                    //    newModelPos.X, newModelPos.Y, newModelPos.Z,
                    //    partBounds.Height, 
                    //    partBounds.Width, 
                    //    partBounds.Depth);
                }
            }
        }

        /// <summary>
        /// Returns the bounding size rectable for the given orth dimensions.
        /// </summary>
        /// <param name="rct"></param>
        /// <param name="toolType"></param>
        /// <returns></returns>
        private RectValue GetPartDefinitionSize(Rect rct, MouseToolType toolType)
        {
            RectValue returnRect = new RectValue();
            if (!rct.IsEmpty)
            {
                Viewport3D viewport = this.xViewer.Viewport;
                PointValue btmLeft = ModelConstruct3DHelper.GetModelPoint3D(rct.BottomLeft, viewport);
                PointValue topRight = ModelConstruct3DHelper.GetModelPoint3D(rct.TopRight, viewport);

                Single partWidth = 0;
                Single partHeight = 0;
                Single partDepth = 0;

                PointValue pos = new PointValue();


                //set default initial values based on tool type
                if (toolType == MouseToolType.CreatePart)
                {
                    partHeight = 5;
                    partWidth = 5;
                    partDepth = 5;
                }
                else
                {
#if DEBUG
                    throw new NotSupportedException();
#endif
                }



                switch (this.ViewModel.ViewType)
                {
                    case CameraViewType.Front:
                        {
                            partWidth = topRight.X - btmLeft.X;
                            partHeight = topRight.Y - btmLeft.Y;
                            pos = new PointValue(btmLeft.X, btmLeft.Y, 0);
                        }
                        break;

                    case CameraViewType.Back:
                        {
                            partWidth = btmLeft.X - topRight.X;
                            partHeight = topRight.Y - btmLeft.Y;
                            pos = new PointValue(topRight.X, btmLeft.Y, 0);
                        }
                        break;

                    case CameraViewType.Top:
                        {
                            partWidth = topRight.X - btmLeft.X;
                            partDepth = btmLeft.Z - topRight.Z;
                            pos = new PointValue(btmLeft.X, 0, topRight.Z);
                        }
                        break;

                    case CameraViewType.Bottom:
                        {
                            partWidth = topRight.X - btmLeft.X;
                            partDepth = topRight.Z - btmLeft.Z;

                            pos = new PointValue(btmLeft.X, 0, btmLeft.Z);
                        }
                        break;

                    case CameraViewType.Left:
                        {
                            partDepth = topRight.Z - btmLeft.Z;
                            partHeight = topRight.Y - btmLeft.Y;
                            pos = new PointValue(0, btmLeft.Y, btmLeft.Z);
                        }
                        break;

                    case CameraViewType.Right:
                        {
                            partDepth = btmLeft.Z - topRight.Z;
                            partHeight = topRight.Y - btmLeft.Y;
                            pos = new PointValue(0, btmLeft.Y, topRight.Z);
                        }
                        break;

                    case CameraViewType.Perspective:
                        Debug.Fail("Should never hit this");
                        break;

                }

                returnRect = new RectValue(
                    Convert.ToSingle(Math.Round(pos.X, _roundingPrecision)),
                    Convert.ToSingle(Math.Round(pos.Y, _roundingPrecision)),
                    Convert.ToSingle(Math.Round(pos.Z, _roundingPrecision)),
                    Convert.ToSingle(Math.Round(partWidth, _roundingPrecision)),
                    Convert.ToSingle(Math.Round(partHeight, _roundingPrecision)),
                    Convert.ToSingle(Math.Round(partDepth, _roundingPrecision)));
            }

            return returnRect;
        }

        #endregion

        #region Perspective Part Creator

        /// <summary>
        /// Starts a new perspective part creator if we are not
        /// already processing one.
        /// </summary>
        private void BeginPerspectivePartCreator(MouseEventArgs e)
        {
            if (_perspectivePartCreator == null)
            {
                _perspectivePartCreator = new Model3DCreator();
                _perspectivePartCreator.PartDefinitionCompleted += PerspectivePartCreator_PartDefinitionCompleted;
                this.xManipulatorViewer.Children.Add(_perspectivePartCreator);
                _perspectivePartCreator.StartMouseCapture(e, this.xViewer.Viewport);
            }
        }

        /// <summary>
        /// Carries out any perspective part creator processing required when the mouse moves.
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        private Boolean PerspectivePartCreator_ProcessMouseMove(MouseEventArgs e)
        {
            Boolean handled = false;

            if (_perspectivePartCreator != null)
            {
                _perspectivePartCreator.ProcessMouseMove(e);
                //UpdateStatusBar(_perspectivePartCreator.GetStatusText());
                handled = true;
            }

            return handled;
        }

        /// <summary>
        /// Processes mouse left button up for the perspective part creator as required.
        /// </summary>
        private Boolean PerspectivePartCreator_ProcesssMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            Boolean handled = false;

            if (_perspectivePartCreator != null)
            {
                _perspectivePartCreator.ProcessMouseUp(e);
                handled = true;
            }

            return handled;
        }

        /// <summary>
        /// Called when the model 3d creator completes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PerspectivePartCreator_PartDefinitionCompleted(object sender, EventArgs e)
        {
            Model3DCreator senderControl = (Model3DCreator)sender;

            this.ViewModel.CreateNewFixturePart(
                Convert.ToSingle(Math.Round(senderControl.Position.X, _roundingPrecision)),
                Convert.ToSingle(Math.Round(senderControl.Position.Y, _roundingPrecision)),
                Convert.ToSingle(Math.Round(senderControl.Position.Z, _roundingPrecision)),
                Convert.ToSingle(Math.Round(senderControl.Size.Y, _roundingPrecision)),
                Convert.ToSingle(Math.Round(senderControl.Size.X, _roundingPrecision)),
                Convert.ToSingle(Math.Round(senderControl.Size.Z, _roundingPrecision))
                );

            CancelPerspectivePartCreator();
        }

        /// <summary>
        /// Cancels part creation mode
        /// </summary>
        private void CancelPerspectivePartCreator()
        {
            if (_perspectivePartCreator != null)
            {
                Model3DCreator partCreator = _perspectivePartCreator;

                //cancel part creation.
                partCreator.ReleaseMouseCapture();

                partCreator.PartDefinitionCompleted -= PerspectivePartCreator_PartDefinitionCompleted;

                this.xManipulatorViewer.Children.Remove(partCreator);

                _perspectivePartCreator = null;

                if (this.ViewModel != null)
                {
                    this.ViewModel.MouseToolType = MouseToolType.Pointer;
                }
            }
        }

        #endregion


        #endregion

        #region Rubberband Selection

        /// <summary>
        /// Processes mouse move actions for the drag selection as required
        /// </summary>
        /// <param name="e"></param>
        private Boolean DragSelectionAdorner_ProcessMouseMove(MouseEventArgs e)
        {
            Boolean handled = false;

            if (_dragSelectionAdorner != null && _isDraggingAdorner == false
                    && e.LeftButton == MouseButtonState.Pressed)
            {
                AdornerLayer aLayer = AdornerLayer.GetAdornerLayer(this.xViewerCanvas);
                if (aLayer != null)
                {
                    _dragSelectionAdorner.DragSelectionCompleted += DragSelectionAdorner_SelectionCompleted;
                    aLayer.Add(_dragSelectionAdorner);
                }

                _isDraggingAdorner = true;

                handled = true;
            }

            return handled;
        }

        /// <summary>
        /// Called when a drag selector completes its selection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DragSelectionAdorner_SelectionCompleted(object sender, RoutedEventArgs e)
        {
            DragSelectionAdorner senderControl = (DragSelectionAdorner)sender;
            senderControl.DragSelectionCompleted -= DragSelectionAdorner_SelectionCompleted;

            _isDraggingAdorner = false;
            senderControl.ReleaseMouseCapture();
            _dragSelectionAdorner = null;

            Viewport3D viewport = this.xViewer.Viewport;


            Rect selectionRect = senderControl.GetSelectionRect();
            if (!selectionRect.IsEmpty)
            {
                List<EditorSubComponentView> selectionItems = new List<EditorSubComponentView>();

                foreach (ModelConstruct3D model in _modelVisual.GetAllChildModels())
                {
                    //TODO: We are selecting using 2d bounds but this might have issues where angles are involved..
                    Rect bounds = ModelConstruct3DHelper.GetModel2DBounds(viewport, model, Rect.Empty, /*thisOnly*/true);
                    if (selectionRect.Contains(bounds))
                    {
                        EditorSubComponentView item = null;

                        //get the selectable plan item.
                        IPlanPart3DData planPart3DData = model.ModelData as IPlanPart3DData;
                        if (planPart3DData != null)
                        {
                            item = planPart3DData.PlanPart as EditorSubComponentView;
                        }

                        if (item != null && !selectionItems.Contains(item))
                        {
                            selectionItems.Add(item);
                        }

                    };
                }


                this.ViewModel.SelectedSubComponents.AddRange(selectionItems);

            }
        }

        #endregion

        #region SubComponent Drag/Drop

        /// <summary>
        /// Starts a new drag action for the given fixture item.
        /// </summary>
        /// <param name="dragItem"></param>
        private void BeginSubComponentDrag(MouseEventArgs e)
        {
            PointValue viewportHitCenter = ModelConstruct3DHelper.GetModelPoint3D(e.GetPosition(this.xViewer), this.xViewer.Viewport);

            ComponentEditorItemDragDropArgs itemArgs = new ComponentEditorItemDragDropArgs();
            itemArgs.IsCreateCopy = (Keyboard.Modifiers == ModifierKeys.Control);

            foreach (EditorSubComponentView item in this.ViewModel.SelectedSubComponents)
            {
                ModelConstruct3D modelVis = RenderControlsHelper.FindItemModel(_modelVisual, item);
                ModelConstruct3D parentModel = VisualTreeHelper.GetParent(modelVis) as ModelConstruct3D;
                PointValue worldPos = ModelConstruct3DHelper.ToWorld(modelVis.ModelData.Position, parentModel.ModelData);

                PointValue dragOffset =
                    new PointValue()
                    {
                        X = worldPos.X - viewportHitCenter.X,
                        Y = worldPos.Y - viewportHitCenter.Y,
                        Z = worldPos.Z - viewportHitCenter.Z
                    };

                itemArgs.DragItems.Add(
                    new ComponentEditorDragDropItem()
                    {
                        Item = item,
                        Model3DVis = modelVis,
                        DragOffset = dragOffset
                    });
            }

            DragDropBehaviour dragArgs = new DragDropBehaviour(itemArgs);
            dragArgs.DragComplete += SubComponentDrag_Complete;
            dragArgs.DragArea = this;
            dragArgs.BeginDrag();
        }
        private void SubComponentDrag_Complete(object sender, EventArgs e)
        {
            DragDropBehaviour dragArgs = sender as DragDropBehaviour;
            dragArgs.DragComplete -= SubComponentDrag_Complete;

            if (_previewModels != null)
            {
                foreach (var model in _previewModels)
                {
                    this.xManipulatorViewer.Children.Remove(model);
                    model.Dispose();
                }
                _previewModels = null;
            }
        }

        /// <summary>
        /// Called whenever a fixture item is dragged over this control
        /// </summary>
        /// <param name="fixtureItemArgs"></param>
        private void OnSubComponentDragOver(ComponentEditorItemDragDropArgs fixtureItemArgs, DragEventArgs e)
        {
            if (_previewModels == null)
            {
                ModelConstruct3D[] models = new ModelConstruct3D[fixtureItemArgs.DragItems.Count];

                Int32 idx = 0;
                foreach (var dragEntry in fixtureItemArgs.DragItems)
                {
                    EditorSubComponentView item = dragEntry.Item;
                    ModelConstruct3D itemDragModel = new ModelConstruct3D(new FixtureSubComponent3DData(dragEntry.Item, new FixtureRenderSettings()));

                    if (itemDragModel != null)
                    {
                        itemDragModel.ModelData.IsWireframe = true;
                        this.xManipulatorViewer.Children.Add(itemDragModel);

                        models[idx] = itemDragModel;
                        idx++;
                    }
                    else
                    {
                        //stop if we failed to create the model.
                        return;
                    }
                }

                _previewModels = models;
            }

            //Update dragging model positions.
            Point hitPoint = e.GetPosition(this.xViewer);
            PointValue hitPoint3D = ModelConstruct3DHelper.GetModelPoint3D(hitPoint, this.xViewer.Viewport);

            ComponentEditorHitTestResult subHitResult = ComponentEditorHelper.GetNearestSubComponent(this.xViewer.Viewport, hitPoint);


            for (Int32 i = 0; i < fixtureItemArgs.DragItems.Count; i++)
            {
                var dragEntry = fixtureItemArgs.DragItems[i];
                PointValue dragOffset = dragEntry.DragOffset;
                ModelConstruct3D itemDragModel = _previewModels[i];

                PointValue newPosition =
                    new PointValue()
                    {
                        X = hitPoint3D.X + dragOffset.X,
                        Y = hitPoint3D.Y + dragOffset.Y,
                        Z = hitPoint3D.Z + dragOffset.Z,
                    };

                itemDragModel.ModelData.Position = newPosition;
            }

        }

        /// <summary>
        /// Called whenever a fixture item is dropped.
        /// </summary>
        /// <param name="fixtureItemArgs"></param>
        private void OnSubComponentDropped(ComponentEditorItemDragDropArgs fixtureItemArgs, DragEventArgs e)
        {
            if (_previewModels == null) { return; }

            EditorComponentView component = this.ViewModel.Component;
            FixtureComponent3DData fixtureModelData = this.ViewModel.ComponentModelData;
            Viewport3D vp = this.xViewer.Viewport;
            ProjectionCamera camera = this.xViewer.Camera;
            Point hitPoint = e.GetPosition(this.xViewer);
            PointValue hitPoint3D = ModelConstruct3DHelper.GetModelPoint3D(hitPoint, vp);

            component.BeginUndoableAction();


            List<EditorSubComponentView> newSelection = new List<EditorSubComponentView>();

            //Process dragged fixtures
            PointValue viewportHitCenter = ModelConstruct3DHelper.GetModelPoint3D(e.GetPosition(this.xViewer), vp);
            Vector3D changeVector = ModelConstruct3DHelper.FindItemMoveVector(camera, 1, 1);

            for (Int32 i = 0; i < fixtureItemArgs.DragItems.Count; i++)
            {
                var dragEntry = fixtureItemArgs.DragItems[i];
                var item = dragEntry.Item;


                ModelConstruct3D modelVis = dragEntry.Model3DVis;
                ModelConstruct3D parentModel = VisualTreeHelper.GetParent(modelVis) as ModelConstruct3D;
                ModelConstruct3D itemDragModel = _previewModels[i];

                Point3D newWorldPos = itemDragModel.ModelData.Position.ToPoint3D();
                Point newWorldhitPoint = Viewport3DHelper.Point3DtoPoint2D(this.xViewer.Viewport, newWorldPos);

                //create the copy.
                if (fixtureItemArgs.IsCreateCopy)
                {
                    item = component.AddSubComponentCopy(item);
                    if (item == null) { continue; }
                }


                //update the item position.
                IModelConstruct3DData modelData = RenderControlsHelper.FindItemModelData(fixtureModelData, item);
                Point3D newLocalPos = ModelConstruct3DHelper.ToLocal(newWorldPos, modelData);
                if (changeVector.X != 0) item.X = Convert.ToSingle(Math.Round(newLocalPos.X, _roundingPrecision));
                if (changeVector.Y != 0) item.Y = Convert.ToSingle(Math.Round(newLocalPos.Y, _roundingPrecision));
                if (changeVector.Z != 0) item.Z = Convert.ToSingle(Math.Round(newLocalPos.Z, _roundingPrecision));


                newSelection.Add(item);



            }


            component.EndUndoableAction();

            //apply the new selection
            this.ViewModel.SelectedSubComponents.Clear();
            this.ViewModel.SelectedSubComponents.AddRange(newSelection);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the size and placement of the gridlines based on the model.
        /// </summary>
        private void UpdateGridlines()
        {
            GridLinesVisual3D gridlines = this.xGridlines;
            if (gridlines != null && this.ViewModel != null)
            {
                gridlines.Visible = this.ViewModel.ShowGridlines;
                this.XAxisArrow.Visible = false;
                this.YAxisArrow.Visible = false;
                this.ZAxisArrow.Visible = false;

                //Determine the line bounds.
                Rect3D bounds = new Rect3D();
                bounds.SizeX = this.ViewModel.GridlineAreaWidth;
                bounds.SizeY = this.ViewModel.GridlineAreaHeight;
                bounds.SizeZ = this.ViewModel.GridlineAreaDepth;

                Double planLinesMargin = 0;
                Double axisArrowExtra = 0;

                //update the gridlines.
                if (!bounds.IsEmpty)
                {
                    gridlines.MajorDistance = Math.Max(this.ViewModel.GridlineMajorDist, 0.5);
                    gridlines.MinorDistance = Math.Max(this.ViewModel.GridlineMinorDist, 0.5);


                    Double centerX = bounds.X + (bounds.SizeX / 2);
                    Double centerY = bounds.Y + (bounds.SizeY / 2);
                    Double centerZ = bounds.Z + (bounds.SizeZ / 2);

                    this.XAxisArrow.Point2 = new Point3D(bounds.SizeX + planLinesMargin + axisArrowExtra, 0, 0);
                    this.YAxisArrow.Point2 = new Point3D(0, bounds.SizeY + planLinesMargin + axisArrowExtra, 0);
                    this.ZAxisArrow.Point2 = new Point3D(0, 0, bounds.SizeZ + planLinesMargin + axisArrowExtra);

                    switch (this.ViewModel.ViewType)
                    {
                        case CameraViewType.Perspective:
                            gridlines.Normal = new Vector3D(0, 1, 0);
                            gridlines.LengthDirection = new Vector3D(1, 0, 0);
                            gridlines.Center = new Point3D(centerX, bounds.Y, centerZ);
                            gridlines.Length = bounds.SizeX + planLinesMargin;
                            gridlines.Width = bounds.SizeZ + planLinesMargin;

                            this.XAxisArrow.Visible = true;
                            this.ZAxisArrow.Visible = true;
                            break;

                        case CameraViewType.Front:
                            gridlines.Normal = new Vector3D(0, 0, 1);
                            gridlines.LengthDirection = new Vector3D(0, 1, 0);
                            gridlines.Center = new Point3D(centerX, centerY, bounds.Z);
                            gridlines.Length = bounds.SizeY + planLinesMargin;
                            gridlines.Width = bounds.SizeX + planLinesMargin;

                            this.XAxisArrow.Visible = true;
                            this.YAxisArrow.Visible = true;
                            break;

                        case CameraViewType.Back:
                            gridlines.Normal = new Vector3D(0, 0, 1);
                            gridlines.LengthDirection = new Vector3D(0, 1, 0);
                            gridlines.Center = new Point3D(centerX, centerY, bounds.Z + bounds.SizeZ);
                            gridlines.Length = bounds.SizeY + planLinesMargin;
                            gridlines.Width = bounds.SizeX + planLinesMargin;

                            this.XAxisArrow.Visible = true;
                            this.YAxisArrow.Visible = true;
                            break;

                        case CameraViewType.Left:
                            gridlines.Normal = new Vector3D(1, 0, 0);
                            gridlines.LengthDirection = new Vector3D(0, 1, 0);
                            gridlines.Center = new Point3D(centerX, centerY, centerZ);
                            gridlines.Length = bounds.SizeY + planLinesMargin;
                            gridlines.Width = bounds.SizeZ + planLinesMargin;

                            this.YAxisArrow.Visible = true;
                            this.ZAxisArrow.Visible = true;
                            break;

                        case CameraViewType.Right:
                            gridlines.Normal = new Vector3D(1, 0, 0);
                            gridlines.LengthDirection = new Vector3D(0, 1, 0);
                            gridlines.Center = new Point3D(bounds.X, centerY, centerZ);
                            gridlines.Length = bounds.SizeY + planLinesMargin;
                            gridlines.Width = bounds.SizeZ + planLinesMargin;

                            this.YAxisArrow.Visible = true;
                            this.ZAxisArrow.Visible = true;
                            break;

                        case CameraViewType.Top:
                            gridlines.Normal = new Vector3D(0, 1, 0);
                            gridlines.LengthDirection = new Vector3D(1, 0, 0);
                            gridlines.Center = new Point3D(centerX, bounds.Y, centerZ);
                            gridlines.Length = bounds.SizeX + planLinesMargin;
                            gridlines.Width = bounds.SizeZ + planLinesMargin;

                            this.XAxisArrow.Visible = true;
                            this.ZAxisArrow.Visible = true;
                            break;

                        case CameraViewType.Bottom:
                            gridlines.Normal = new Vector3D(0, 1, 0);
                            gridlines.LengthDirection = new Vector3D(1, 0, 0);
                            gridlines.Center = new Point3D(centerX, bounds.Y + bounds.SizeY, centerZ);
                            gridlines.Length = bounds.SizeX + planLinesMargin;
                            gridlines.Width = bounds.SizeZ + planLinesMargin;

                            this.XAxisArrow.Visible = true;
                            this.ZAxisArrow.Visible = true;
                            break;
                    }
                }


            }
        }

        /// <summary>
        /// Creates and shows the context menu.
        /// </summary>
        private void ShowContextMenu()
        {
            //show the context menu
            Fluent.ContextMenu contextMenu = new Fluent.ContextMenu();

            RelayCommand showItemPropertiesCmd = this.ViewModel.ParentController.ShowItemPropertiesCommand;
            

            Fluent.MenuItem propertiesMi
                = new Fluent.MenuItem()
                {
                    Command = showItemPropertiesCmd,
                    Header = showItemPropertiesCmd.FriendlyName,
                    Icon = showItemPropertiesCmd.SmallIcon,
                };
            contextMenu.Items.Add(propertiesMi);


            contextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.MousePoint;
            contextMenu.IsOpen = true;

            //Zooming
            contextMenu.Items.Add(new Separator());

            var zoomHeader = new Fluent.MenuItem() { Header = Message.Ribbon_GroupHeader_Zoom };
            contextMenu.Items.Add(zoomHeader);

            RelayCommand ZoomInCmd = this.ViewModel.ParentController.ZoomInCommand;
            zoomHeader.Items.Add(new Fluent.MenuItem() { Command = ZoomInCmd, Header = ZoomInCmd.FriendlyName, Icon = ZoomInCmd.SmallIcon, });

            RelayCommand ZoomOutCmd = this.ViewModel.ParentController.ZoomOutCommand;
            zoomHeader.Items.Add(new Fluent.MenuItem() { Command = ZoomOutCmd, Header = ZoomOutCmd.FriendlyName, Icon = ZoomOutCmd.SmallIcon });

            RelayCommand ToggleZoomToAreaModeCmd = this.ViewModel.ParentController.ToggleZoomToAreaModeCommand;
            zoomHeader.Items.Add(new Fluent.MenuItem() { Command = ToggleZoomToAreaModeCmd, Header = ToggleZoomToAreaModeCmd.FriendlyName, Icon = ToggleZoomToAreaModeCmd.SmallIcon});

            RelayCommand ZoomToFitCmd = this.ViewModel.ParentController.ZoomToFitCommand;
            zoomHeader.Items.Add(new Fluent.MenuItem() { Command = ZoomToFitCmd, Header = ZoomToFitCmd.FriendlyName, Icon = ZoomToFitCmd.SmallIcon });
        }

        /// <summary>
        /// Returns the hit plan item for the given mouse args
        /// </summary>
        /// <param name="mouseArgs"></param>
        /// <returns></returns>
        private EditorSubComponentView GetHitPlanItem(MouseEventArgs mouseArgs, Boolean isSelectableOnly = true)
        {
            EditorSubComponentView hitPlanItem = null;

            Viewport3D viewport = this.xViewer.Viewport;
            if (viewport != null)
            {
                Point loc = mouseArgs.GetPosition(viewport);
                DependencyObject hitModel = ModelConstruct3DHelper.GetHitTestResult(viewport, loc);
                if (hitModel != null)
                {
                    hitPlanItem = ComponentEditorHelper.GetHitSub(hitModel);
                }
            }

            return hitPlanItem;
        }

        /// <summary>
        /// Clears all preview models from the visual.
        /// </summary>
        private void ClearPreviewModels()
        {
            if (_previewModels != null)
            {
                foreach (var model in _previewModels)
                {
                    this.xManipulatorViewer.Children.Remove(model);
                    model.Dispose();
                }
                _previewModels = null;
            }
        }

        #endregion

        #region IComponentEditorDocumentView Members

        public IComponentEditorDocument GetDocument()
        {
            return this.ViewModel as IComponentEditorDocument;
        }

        public void Dispose()
        {
            this.ViewModel = null;
            GC.SuppressFinalize(this);
        }

        #endregion

    }
}
