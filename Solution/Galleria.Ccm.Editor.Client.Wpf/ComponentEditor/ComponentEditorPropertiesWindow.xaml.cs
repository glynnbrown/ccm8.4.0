﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Common;

namespace Galleria.Ccm.Editor.Client.Wpf.ComponentEditor
{
    /// <summary>
    /// Interaction logic for ComponentEditorPropertiesWindow.xaml
    /// </summary>
    public sealed partial class ComponentEditorPropertiesWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ComponentEditorPropertiesViewModel), typeof(ComponentEditorPropertiesWindow),
            new UIPropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ComponentEditorPropertiesWindow senderControl = (ComponentEditorPropertiesWindow)obj;

            if (e.OldValue != null)
            {
                ComponentEditorPropertiesViewModel oldModel = (ComponentEditorPropertiesViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                ComponentEditorPropertiesViewModel newModel = (ComponentEditorPropertiesViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        public ComponentEditorPropertiesViewModel ViewModel
        {
            get { return (ComponentEditorPropertiesViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="component"></param>
        /// <param name="selection"></param>
        public ComponentEditorPropertiesWindow(EditorComponentView component, ComponentEditorSelection selection)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            AddEventHandlers();

            InitializeComponent();

            this.ViewModel = new ComponentEditorPropertiesViewModel(component, selection);


            this.Loaded += ComponentEditorPropertiesWindow_Loaded;
        }

        /// <summary>
        /// Carries out initial load actions.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComponentEditorPropertiesWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ComponentEditorPropertiesWindow_Loaded;

            Dispatcher.BeginInvoke(
                (Action)(() => Mouse.OverrideCursor = null), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion

        #region Event Handlers

        private void AddEventHandlers()
        {
            this.AddHandler(ImageSelector.EditRequestedEvent, (RoutedEventHandler)OnImageEditRequested);
            this.AddHandler(ImageSelector.ClearRequestedEvent, (RoutedEventHandler)OnImageClearRequested);
        }

        /// <summary>
        /// Called when the user requests to edit an image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnImageEditRequested(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel != null)
            {
                ImageSelector senderControl = (ImageSelector)e.OriginalSource;
                this.ViewModel.SetImage(senderControl.ImageName);
            }
        }

        /// <summary>
        /// Called when the user requests to clear an image.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnImageClearRequested(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel != null)
            {
                ImageSelector senderControl = (ImageSelector)e.OriginalSource;
                this.ViewModel.ClearImage(senderControl.ImageName);
            }
        }

        #endregion

        #region Window Close

        public Boolean IsClosing { get; private set; }

        /// <summary>
        /// Called when the window close cross has been pressed
        /// </summary>
        /// <param name="e"></param>
        protected override void OnCrossCloseRequested(System.ComponentModel.CancelEventArgs e)
        {
            IsClosing = true;
            this.ViewModel.CancelCommand.Execute();

            base.OnCrossCloseRequested(e);
        }

        /// <summary>
        /// Disposes of the viewmodel when the window has closed.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion
    }
}
