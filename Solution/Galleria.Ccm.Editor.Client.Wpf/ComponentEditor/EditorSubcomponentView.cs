﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Ineson ~ Created.
#endregion
#region Version History: CCM820
// V8-30668 : L.Luong
//  Added Merchandisable depth property.
#endregion
#region Version History: (CCM 8.3)
// V8-32524 : L.Ineson
// Updated to use more of the base model following changes to support annotations.
//CCM-18454 : L.Ineson
//  Corrected image by id fetch.
#endregion
#endregion

using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Common.Rendering;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf.Rendering;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Media.Media3D;

namespace Galleria.Ccm.Editor.Client.Wpf.ComponentEditor
{
    /// <summary>
    /// Provides a simple view of a FixtureSubComponent.
    /// </summary>
    public sealed class EditorSubComponentView : ExtendedViewModelObject, IFixtureSubComponentRenderable, IDataErrorInfo
    {
        #region Fields

        private FixtureSubComponent _subComponent;
        //private PlanogramSubComponent _planSubComponent;

        #endregion

        #region Binding PropertyPaths


        public static readonly PropertyPath NameProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.Name);
        public static readonly PropertyPath HeightProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.Height);
        public static readonly PropertyPath WidthProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.Width);
        public static readonly PropertyPath DepthProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.Depth);
        public static readonly PropertyPath XProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.X);
        public static readonly PropertyPath YProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.Y);
        public static readonly PropertyPath ZProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.Z);
        public static readonly PropertyPath SlopeProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.Slope);
        public static readonly PropertyPath AngleProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.Angle);
        public static readonly PropertyPath RollProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.Roll);
        public static readonly PropertyPath ShapeTypeProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.ShapeType);
        public static readonly PropertyPath MerchandisableHeightProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.MerchandisableHeight);
        public static readonly PropertyPath MerchandisableDepthProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.MerchandisableDepth);
        public static readonly PropertyPath FillPatternTypeFrontProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.FillPatternTypeFront);
        public static readonly PropertyPath FillPatternTypeBackProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.FillPatternTypeBack);
        public static readonly PropertyPath FillPatternTypeTopProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.FillPatternTypeTop);
        public static readonly PropertyPath FillPatternTypeBottomProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.FillPatternTypeBottom);
        public static readonly PropertyPath FillPatternTypeLeftProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.FillPatternTypeLeft);
        public static readonly PropertyPath FillPatternTypeRightProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.FillPatternTypeRight);
        public static readonly PropertyPath FillColourFrontProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.FillColourFront);
        public static readonly PropertyPath FillColourBackProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.FillColourBack);
        public static readonly PropertyPath FillColourTopProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.FillColourTop);
        public static readonly PropertyPath FillColourBottomProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.FillColourBottom);
        public static readonly PropertyPath FillColourLeftProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.FillColourLeft);
        public static readonly PropertyPath FillColourRightProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.FillColourRight);
        public static readonly PropertyPath LineColourProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.LineColour);
        public static readonly PropertyPath TransparencyPercentFrontProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.TransparencyPercentFront);
        public static readonly PropertyPath TransparencyPercentBackProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.TransparencyPercentBack);
        public static readonly PropertyPath TransparencyPercentTopProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.TransparencyPercentTop);
        public static readonly PropertyPath TransparencyPercentBottomProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.TransparencyPercentBottom);
        public static readonly PropertyPath TransparencyPercentLeftProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.TransparencyPercentLeft);
        public static readonly PropertyPath TransparencyPercentRightProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.TransparencyPercentRight);
        public static readonly PropertyPath FaceThicknessFrontProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.FaceThicknessFront);
        public static readonly PropertyPath FaceThicknessBackProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.FaceThicknessBack);
        public static readonly PropertyPath FaceThicknessTopProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.FaceThicknessTop);
        public static readonly PropertyPath FaceThicknessBottomProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.FaceThicknessBottom);
        public static readonly PropertyPath FaceThicknessLeftProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.FaceThicknessLeft);
        public static readonly PropertyPath FaceThicknessRightProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.FaceThicknessRight);
        public static readonly PropertyPath RiserHeightProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.RiserHeight);
        public static readonly PropertyPath RiserThicknessProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.RiserThickness);
        public static readonly PropertyPath IsRiserPlacedOnFrontProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.IsRiserPlacedOnFront);
        public static readonly PropertyPath IsRiserPlacedOnBackProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.IsRiserPlacedOnBack);
        public static readonly PropertyPath IsRiserPlacedOnLeftProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.IsRiserPlacedOnLeft);
        public static readonly PropertyPath IsRiserPlacedOnRightProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.IsRiserPlacedOnRight);
        public static readonly PropertyPath RiserColourProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.RiserColour);
        public static readonly PropertyPath RiserTransparencyPercentProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.RiserTransparencyPercent);
        public static readonly PropertyPath NotchStartXProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.NotchStartX);
        public static readonly PropertyPath NotchSpacingXProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.NotchSpacingX);
        public static readonly PropertyPath NotchStartYProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.NotchStartY);
        public static readonly PropertyPath NotchSpacingYProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.NotchSpacingY);
        public static readonly PropertyPath NotchHeightProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.NotchHeight);
        public static readonly PropertyPath NotchWidthProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.NotchWidth);
        public static readonly PropertyPath IsNotchPlacedOnFrontProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.IsNotchPlacedOnFront);
        public static readonly PropertyPath NotchStyleTypeProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.NotchStyleType);
        public static readonly PropertyPath DividerObstructionHeightProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.DividerObstructionHeight);
        public static readonly PropertyPath DividerObstructionWidthProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.DividerObstructionWidth);
        public static readonly PropertyPath DividerObstructionDepthProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.DividerObstructionDepth);
        public static readonly PropertyPath DividerObstructionStartXProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.DividerObstructionStartX);
        public static readonly PropertyPath DividerObstructionSpacingXProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.DividerObstructionSpacingX);
        public static readonly PropertyPath DividerObstructionStartYProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.DividerObstructionStartY);
        public static readonly PropertyPath DividerObstructionSpacingYProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.DividerObstructionSpacingY);
        public static readonly PropertyPath DividerObstructionStartZProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.DividerObstructionStartZ);
        public static readonly PropertyPath DividerObstructionSpacingZProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.DividerObstructionSpacingZ);
        public static readonly PropertyPath IsDividerObstructionAtStartProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.IsDividerObstructionAtStart);
        public static readonly PropertyPath IsDividerObstructionAtEndProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.IsDividerObstructionAtEnd);
        public static readonly PropertyPath IsDividerObstructionByFacingProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.IsDividerObstructionByFacing);
        public static readonly PropertyPath DividerObstructionFillColourProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.DividerObstructionFillColour);
        public static readonly PropertyPath DividerObstructionFillPatternProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.DividerObstructionFillPattern);
        public static readonly PropertyPath MerchConstraintRow1StartXProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.MerchConstraintRow1StartX);
        public static readonly PropertyPath MerchConstraintRow1SpacingXProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.MerchConstraintRow1SpacingX);
        public static readonly PropertyPath MerchConstraintRow1StartYProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.MerchConstraintRow1StartY);
        public static readonly PropertyPath MerchConstraintRow1SpacingYProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.MerchConstraintRow1SpacingY);
        public static readonly PropertyPath MerchConstraintRow1HeightProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.MerchConstraintRow1Height);
        public static readonly PropertyPath MerchConstraintRow1WidthProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.MerchConstraintRow1Width);
        public static readonly PropertyPath MerchConstraintRow2StartXProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.MerchConstraintRow2StartX);
        public static readonly PropertyPath MerchConstraintRow2SpacingXProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.MerchConstraintRow2SpacingX);
        public static readonly PropertyPath MerchConstraintRow2StartYProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.MerchConstraintRow2StartY);
        public static readonly PropertyPath MerchConstraintRow2SpacingYProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.MerchConstraintRow2SpacingY);
        public static readonly PropertyPath MerchConstraintRow2HeightProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.MerchConstraintRow2Height);
        public static readonly PropertyPath MerchConstraintRow2WidthProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.MerchConstraintRow2Width);
        public static readonly PropertyPath LineThicknessProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.LineThickness);
        public static readonly PropertyPath MerchandisingTypeProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.MerchandisingType);
        public static readonly PropertyPath CombineTypeProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.CombineType);
        public static readonly PropertyPath IsProductOverlapAllowedProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.IsProductOverlapAllowed);
        public static readonly PropertyPath IsProductSqueezeAllowedProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.IsProductSqueezeAllowed);
        public static readonly PropertyPath MerchandisingStrategyXProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.MerchandisingStrategyX);
        public static readonly PropertyPath MerchandisingStrategyYProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.MerchandisingStrategyY);
        public static readonly PropertyPath MerchandisingStrategyZProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.MerchandisingStrategyZ);
        public static readonly PropertyPath LeftOverhangProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.LeftOverhang);
        public static readonly PropertyPath RightOverhangProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.RightOverhang);
        public static readonly PropertyPath FrontOverhangProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.FrontOverhang);
        public static readonly PropertyPath BackOverhangProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.BackOverhang);
        public static readonly PropertyPath TopOverhangProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.TopOverhang);
        public static readonly PropertyPath BottomOverhangProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.BottomOverhang);


        public static readonly PropertyPath FrontImageProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.FrontImage);
        public static readonly PropertyPath BottomImageProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.BottomImage);
        public static readonly PropertyPath BackImageProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.BackImage);
        public static readonly PropertyPath TopImageProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.TopImage);
        public static readonly PropertyPath LeftImageProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.LeftImage);
        public static readonly PropertyPath RightImageProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.RightImage);

        public static readonly PropertyPath FillColourProperty = WpfHelper.GetPropertyPath<EditorSubComponentView>(p => p.FillColour);

        #endregion

        #region Properties

        public EditorComponentView ParentComponentView
        {
            get { return base.Parent as EditorComponentView; }
        }

        //public PlanogramSubComponent SourcePlanogramSubComponent
        //{
        //    get { return _planSubComponent; }
        //    set { _planSubComponent = value; }
        //}

        public FixtureSubComponent Model
        {
            get { return _subComponent; }
        }

        #region Model properties

        public String Name
        {
            get { return _subComponent.Name; }
            set { _subComponent.Name = value; }
        }

        public Single X
        {
            get { return _subComponent.X; }
            set { _subComponent.X = value; }
        }
        public Single Y
        {
            get { return _subComponent.Y; }
            set { _subComponent.Y = value; }
        }
        public Single Z
        {
            get { return _subComponent.Z; }
            set { _subComponent.Z = value; }
        }

        public Single Slope
        {
            get { return _subComponent.Slope; }
            set { _subComponent.Slope = value; }
        }
        public Single Angle
        {
            get { return _subComponent.Angle; }
            set { _subComponent.Angle = value; }
        }
        public Single Roll
        {
            get { return _subComponent.Roll; }
            set { _subComponent.Roll = value; }
        }

        public Single Height
        {
            get { return _subComponent.Height; }
            set { _subComponent.Height = value; }
        }
        public Single Width
        {
            get { return _subComponent.Width; }
            set { _subComponent.Width = value; }
        }
        public Single Depth
        {
            get { return _subComponent.Depth; }
            set { _subComponent.Depth = value; }
        }

        public FixtureSubComponentShapeType ShapeType
        {
            get { return _subComponent.ShapeType; }
            set { _subComponent.ShapeType = value; }
        }
        public Boolean HasCollisionDetection
        {
            get { return _subComponent.HasCollisionDetection; }
            set { _subComponent.HasCollisionDetection = value; }
        }

        public Int32 FillColourFront
        {
            get { return _subComponent.FillColourFront; }
            set { _subComponent.FillColourFront = value; }
        }
        public Int32 FillColourBack
        {
            get
            {
                if (_subComponent.FillColourBack != 0)
                {
                    return _subComponent.FillColourBack;
                }
                else { return _subComponent.FillColourFront; }
            }
            set { _subComponent.FillColourBack = value; }
        }
        public Int32 FillColourTop
        {
            get
            {
                if (_subComponent.FillColourTop != 0)
                {
                    return _subComponent.FillColourTop;
                }
                else { return _subComponent.FillColourFront; }
            }
            set { _subComponent.FillColourTop = value; }
        }
        public Int32 FillColourBottom
        {
            get
            {
                if (_subComponent.FillColourBottom != 0)
                {
                    return _subComponent.FillColourBottom;
                }
                else { return _subComponent.FillColourFront; }
            }
            set { _subComponent.FillColourBottom = value; }
        }
        public Int32 FillColourLeft
        {
            get
            {
                if (_subComponent.FillColourLeft != 0)
                {
                    return _subComponent.FillColourLeft;
                }
                else { return _subComponent.FillColourFront; }
            }
            set { _subComponent.FillColourLeft = value; }
        }
        public Int32 FillColourRight
        {
            get
            {
                if (_subComponent.FillColourRight != 0)
                {
                    return _subComponent.FillColourRight;
                }
                else { return _subComponent.FillColourFront; }
            }
            set { _subComponent.FillColourRight = value; }
        }

        public Int32 LineColour
        {
            get { return _subComponent.LineColour; }
            set { _subComponent.LineColour = value; }
        }
        public Single LineThickness
        {
            get { return _subComponent.LineThickness; }
            set { _subComponent.LineThickness = value; }
        }

        public Int32 TransparencyPercentFront
        {
            get { return _subComponent.TransparencyPercentFront; }
            set { _subComponent.TransparencyPercentFront = value; }
        }
        public Int32 TransparencyPercentBack
        {
            get { return _subComponent.TransparencyPercentBack; }
            set { _subComponent.TransparencyPercentBack = value; }
        }
        public Int32 TransparencyPercentTop
        {
            get { return _subComponent.TransparencyPercentTop; }
            set { _subComponent.TransparencyPercentTop = value; }
        }
        public Int32 TransparencyPercentBottom
        {
            get { return _subComponent.TransparencyPercentBottom; }
            set { _subComponent.TransparencyPercentBottom = value; }
        }
        public Int32 TransparencyPercentLeft
        {
            get { return _subComponent.TransparencyPercentLeft; }
            set { _subComponent.TransparencyPercentLeft = value; }
        }
        public Int32 TransparencyPercentRight
        {
            get { return _subComponent.TransparencyPercentRight; }
            set { _subComponent.TransparencyPercentRight = value; }
        }

        public FixtureSubComponentFillPatternType FillPatternTypeFront
        {
            get { return _subComponent.FillPatternTypeFront; }
            set { _subComponent.FillPatternTypeFront = value; }
        }
        public FixtureSubComponentFillPatternType FillPatternTypeBack
        {
            get { return _subComponent.FillPatternTypeBack; }
            set { _subComponent.FillPatternTypeBack = value; }
        }
        public FixtureSubComponentFillPatternType FillPatternTypeTop
        {
            get { return _subComponent.FillPatternTypeTop; }
            set { _subComponent.FillPatternTypeTop = value; }
        }
        public FixtureSubComponentFillPatternType FillPatternTypeBottom
        {
            get { return _subComponent.FillPatternTypeBottom; }
            set { _subComponent.FillPatternTypeBottom = value; }
        }
        public FixtureSubComponentFillPatternType FillPatternTypeLeft
        {
            get { return _subComponent.FillPatternTypeLeft; }
            set { _subComponent.FillPatternTypeLeft = value; }
        }
        public FixtureSubComponentFillPatternType FillPatternTypeRight
        {
            get { return _subComponent.FillPatternTypeRight; }
            set { _subComponent.FillPatternTypeRight = value; }
        }

        public Single FaceThicknessFront
        {
            get { return _subComponent.FaceThicknessFront; }
            set { _subComponent.FaceThicknessFront = value; }
        }
        public Single FaceThicknessBack
        {
            get { return _subComponent.FaceThicknessBack; }
            set { _subComponent.FaceThicknessBack = value; }
        }
        public Single FaceThicknessTop
        {
            get { return _subComponent.FaceThicknessTop; }
            set { _subComponent.FaceThicknessTop = value; }
        }
        public Single FaceThicknessBottom
        {
            get { return _subComponent.FaceThicknessBottom; }
            set { _subComponent.FaceThicknessBottom = value; }
        }
        public Single FaceThicknessLeft
        {
            get { return _subComponent.FaceThicknessLeft; }
            set { _subComponent.FaceThicknessLeft = value; }
        }
        public Single FaceThicknessRight
        {
            get { return _subComponent.FaceThicknessRight; }
            set { _subComponent.FaceThicknessRight = value; }
        }

        public Single RiserHeight
        {
            get { return _subComponent.RiserHeight; }
            set { _subComponent.RiserHeight = value; }
        }
        public Single RiserThickness
        {
            get { return _subComponent.RiserThickness; }
            set { _subComponent.RiserThickness = value; }
        }
        public Int32 RiserColour
        {
            get { return _subComponent.RiserColour; }
            set { _subComponent.RiserColour = value; }
        }
        public Int32 RiserTransparencyPercent
        {
            get { return _subComponent.RiserTransparencyPercent; }
            set { _subComponent.RiserTransparencyPercent = value; }
        }
        public Boolean IsRiserPlacedOnFront
        {
            get { return _subComponent.IsRiserPlacedOnFront; }
            set { _subComponent.IsRiserPlacedOnFront = value; }
        }
        public Boolean IsRiserPlacedOnBack
        {
            get { return _subComponent.IsRiserPlacedOnBack; }
            set { _subComponent.IsRiserPlacedOnBack = value; }
        }
        public Boolean IsRiserPlacedOnLeft
        {
            get { return _subComponent.IsRiserPlacedOnLeft; }
            set { _subComponent.IsRiserPlacedOnLeft = value; }
        }
        public Boolean IsRiserPlacedOnRight
        {
            get { return _subComponent.IsRiserPlacedOnRight; }
            set { _subComponent.IsRiserPlacedOnRight = value; }
        }

        public Single NotchStartX
        {
            get { return _subComponent.NotchStartX; }
            set { _subComponent.NotchStartX = value; }
        }
        public Single NotchSpacingX
        {
            get { return _subComponent.NotchSpacingX; }
            set { _subComponent.NotchSpacingX = value; }
        }
        public Single NotchStartY
        {
            get { return _subComponent.NotchStartY; }
            set { _subComponent.NotchStartY = value; }
        }
        public Single NotchSpacingY
        {
            get { return _subComponent.NotchSpacingY; }
            set { _subComponent.NotchSpacingY = value; }
        }
        public Single NotchHeight
        {
            get { return _subComponent.NotchHeight; }
            set { _subComponent.NotchHeight = value; }
        }
        public Single NotchWidth
        {
            get { return _subComponent.NotchWidth; }
            set { _subComponent.NotchWidth = value; }
        }
        public Boolean IsNotchPlacedOnFront
        {
            get { return _subComponent.IsNotchPlacedOnFront; }
            set { _subComponent.IsNotchPlacedOnFront = value; }
        }
        public Boolean IsNotchPlacedOnBack
        {
            get { return _subComponent.IsNotchPlacedOnBack; }
            set { _subComponent.IsNotchPlacedOnBack = value; }
        }
        public Boolean IsNotchPlacedOnLeft
        {
            get { return _subComponent.IsNotchPlacedOnLeft; }
            set { _subComponent.IsNotchPlacedOnLeft = value; }
        }
        public Boolean IsNotchPlacedOnRight
        {
            get { return _subComponent.IsNotchPlacedOnRight; }
            set { _subComponent.IsNotchPlacedOnRight = value; }
        }
        public FixtureSubComponentNotchStyleType NotchStyleType
        {
            get { return _subComponent.NotchStyleType; }
            set { _subComponent.NotchStyleType = value; }
        }

        public Single MerchConstraintRow1StartX
        {
            get { return _subComponent.MerchConstraintRow1StartX; }
            set { _subComponent.MerchConstraintRow1StartX = value; }
        }
        public Single MerchConstraintRow1SpacingX
        {
            get { return _subComponent.MerchConstraintRow1SpacingX; }
            set { _subComponent.MerchConstraintRow1SpacingX = value; }
        }
        public Single MerchConstraintRow1StartY
        {
            get { return _subComponent.MerchConstraintRow1StartY; }
            set { _subComponent.MerchConstraintRow1StartY = value; }
        }
        public Single MerchConstraintRow1SpacingY
        {
            get { return _subComponent.MerchConstraintRow1SpacingY; }
            set { _subComponent.MerchConstraintRow1SpacingY = value; }
        }
        public Single MerchConstraintRow1Height
        {
            get { return _subComponent.MerchConstraintRow1Height; }
            set { _subComponent.MerchConstraintRow1Height = value; }
        }
        public Single MerchConstraintRow1Width
        {
            get { return _subComponent.MerchConstraintRow1Width; }
            set { _subComponent.MerchConstraintRow1Width = value; }
        }
        public Single MerchConstraintRow2StartX
        {
            get { return _subComponent.MerchConstraintRow2StartX; }
            set { _subComponent.MerchConstraintRow2StartX = value; }
        }
        public Single MerchConstraintRow2SpacingX
        {
            get { return _subComponent.MerchConstraintRow2SpacingX; }
            set { _subComponent.MerchConstraintRow2SpacingX = value; }
        }
        public Single MerchConstraintRow2StartY
        {
            get { return _subComponent.MerchConstraintRow2StartY; }
            set { _subComponent.MerchConstraintRow2StartY = value; }
        }
        public Single MerchConstraintRow2SpacingY
        {
            get { return _subComponent.MerchConstraintRow2SpacingY; }
            set { _subComponent.MerchConstraintRow2SpacingY = value; }
        }
        public Single MerchConstraintRow2Height
        {
            get { return _subComponent.MerchConstraintRow2Height; }
            set { _subComponent.MerchConstraintRow2Height = value; }
        }
        public Single MerchConstraintRow2Width
        {
            get { return _subComponent.MerchConstraintRow2Width; }
            set { _subComponent.MerchConstraintRow2Width = value; }
        }

        public Single DividerObstructionHeight
        {
            get { return _subComponent.DividerObstructionHeight; }
            set { _subComponent.DividerObstructionHeight = value; }
        }
        public Single DividerObstructionWidth
        {
            get { return _subComponent.DividerObstructionWidth; }
            set { _subComponent.DividerObstructionWidth = value; }
        }
        public Single DividerObstructionDepth
        {
            get { return _subComponent.DividerObstructionDepth; }
            set { _subComponent.DividerObstructionDepth = value; }
        }
        public Single DividerObstructionStartX
        {
            get { return _subComponent.DividerObstructionStartX; }
            set { _subComponent.DividerObstructionStartX = value; }
        }
        public Single DividerObstructionSpacingX
        {
            get { return _subComponent.DividerObstructionSpacingX; }
            set { _subComponent.DividerObstructionSpacingX = value; }
        }
        public Single DividerObstructionStartY
        {
            get { return _subComponent.DividerObstructionStartY; }
            set { _subComponent.DividerObstructionStartY = value; }
        }
        public Single DividerObstructionSpacingY
        {
            get { return _subComponent.DividerObstructionSpacingY; }
            set { _subComponent.DividerObstructionSpacingY = value; }
        }
        public Single DividerObstructionStartZ
        {
            get { return _subComponent.DividerObstructionStartZ; }
            set { _subComponent.DividerObstructionStartZ = value; }
        }
        public Single DividerObstructionSpacingZ
        {
            get { return _subComponent.DividerObstructionSpacingZ; }
            set { _subComponent.DividerObstructionSpacingZ = value; }
        }
        public Boolean IsDividerObstructionAtStart
        {
            get { return _subComponent.IsDividerObstructionAtStart; }
            set { _subComponent.IsDividerObstructionAtStart = value; }
        }
        public Boolean IsDividerObstructionAtEnd
        {
            get { return _subComponent.IsDividerObstructionAtEnd; }
            set { _subComponent.IsDividerObstructionAtEnd = value; }
        }
        public Boolean IsDividerObstructionByFacing
        {
            get { return _subComponent.IsDividerObstructionByFacing; }
            set { _subComponent.IsDividerObstructionByFacing = value; }
        }
        public Int32 DividerObstructionFillColour
        {
            get { return _subComponent.DividerObstructionFillColour; }
            set { _subComponent.DividerObstructionFillColour = value; }
        }
        public FixtureSubComponentFillPatternType DividerObstructionFillPattern
        {
            get { return _subComponent.DividerObstructionFillPattern; }
            set { _subComponent.DividerObstructionFillPattern = value; }
        }

        public Single MerchandisableHeight
        {
            get { return _subComponent.MerchandisableHeight; }
            set { _subComponent.MerchandisableHeight = value; }
        }
        public Single MerchandisableDepth
        {
            get { return _subComponent.MerchandisableDepth; }
            set { _subComponent.MerchandisableDepth = value; }
        }
        public FixtureSubComponentCombineType CombineType
        {
            get { return _subComponent.CombineType; }
            set { _subComponent.CombineType = value; }
        }
        public FixtureSubComponentXMerchStrategyType MerchandisingStrategyX
        {
            get { return _subComponent.MerchandisingStrategyX; }
            set { _subComponent.MerchandisingStrategyX = value; }
        }
        public FixtureSubComponentYMerchStrategyType MerchandisingStrategyY
        {
            get { return _subComponent.MerchandisingStrategyY; }
            set { _subComponent.MerchandisingStrategyY = value; }
        }
        public FixtureSubComponentZMerchStrategyType MerchandisingStrategyZ
        {
            get { return _subComponent.MerchandisingStrategyZ; }
            set { _subComponent.MerchandisingStrategyZ = value; }
        }
        public FixtureSubComponentMerchandisingType MerchandisingType
        {
            get { return _subComponent.MerchandisingType; }
            set { _subComponent.MerchandisingType = value; }
        }
        public Boolean IsProductOverlapAllowed
        {
            get { return _subComponent.IsProductOverlapAllowed; }
            set { _subComponent.IsProductOverlapAllowed = value; }
        }
        public Boolean IsProductSqueezeAllowed
        {
            get { return _subComponent.IsProductSqueezeAllowed; }
            set { _subComponent.IsProductSqueezeAllowed = value; }
        }

        public Single LeftOverhang
        {
            get { return _subComponent.LeftOverhang; }
            set { _subComponent.LeftOverhang = value; }
        }
        public Single RightOverhang
        {
            get { return _subComponent.RightOverhang; }
            set { _subComponent.RightOverhang = value; }
        }
        public Single FrontOverhang
        {
            get { return _subComponent.FrontOverhang; }
            set { _subComponent.FrontOverhang = value; }
        }
        public Single BackOverhang
        {
            get { return _subComponent.BackOverhang; }
            set { _subComponent.BackOverhang = value; }
        }
        public Single TopOverhang
        {
            get { return _subComponent.TopOverhang; }
            set { _subComponent.TopOverhang = value; }
        }
        public Single BottomOverhang
        {
            get { return _subComponent.BottomOverhang; }
            set { _subComponent.BottomOverhang = value; }
        }

        public Boolean IsHang
        {
            get
            {
                return ParentComponentView.ComponentType == FixtureComponentType.ClipStrip
                    || ParentComponentView.ComponentType == FixtureComponentType.Rod
                    || MerchandisingType == FixtureSubComponentMerchandisingType.Hang
                    || MerchandisingType == FixtureSubComponentMerchandisingType.HangFromBottom;
            }
        }

        #endregion


        /// <summary>
        /// Helper to get/set a value against all fill colours
        /// </summary>
        public Int32 FillColour
        {
            get { return Model.FillColourFront; }
            set
            {
                Model.FillColourFront = value;
                Model.FillColourBack = value;
                Model.FillColourTop = value;
                Model.FillColourBottom = value;
                Model.FillColourLeft = value;
                Model.FillColourRight = value;
            }
        }

        #region Images

        public FixtureImage FrontImage
        {
            get
            {
                Int32? id = this.Model.ImageIdFront;
                if (id != null && ParentComponentView != null)
                {
                    return ParentComponentView.Images.FirstOrDefault(p => p.Id == id);
                }
                return null;
            }
            set
            {
                OnPropertyChanging(FrontImageProperty);

                this.Model.ImageIdFront = (value != null) ? (Int32?)value.Id : null;
                OnPropertyChanged(FrontImageProperty);
                OnPropertyChanged("FrontImageData");
            }
        }
        public FixtureImage BackImage
        {
            get
            {
                Int32? id = this.Model.ImageIdBack;
                if (id != null && ParentComponentView != null)
                {
                    return ParentComponentView.Images.FirstOrDefault(p => p.Id == id);
                }
                return null;
            }
            set
            {
                OnPropertyChanging(BackImageProperty);

                this.Model.ImageIdBack = (value != null) ? (Int32?)value.Id : null;
                OnPropertyChanged(BackImageProperty);
                OnPropertyChanged("BackImageData");
            }
        }
        public FixtureImage TopImage
        {
            get
            {
                Int32? id = this.Model.ImageIdTop;
                if (id != null && ParentComponentView != null)
                {
                    return ParentComponentView.Images.FirstOrDefault(p => p.Id == id);
                }
                return null;
            }
            set
            {
                OnPropertyChanging(TopImageProperty);

                this.Model.ImageIdTop = (value != null) ? (Int32?)value.Id : null;
                OnPropertyChanged(TopImageProperty);
                OnPropertyChanged("TopImageData");
            }
        }
        public FixtureImage BottomImage
        {
            get
            {
                Int32? id = this.Model.ImageIdBottom;
                if (id != null && ParentComponentView != null)
                {
                    return ParentComponentView.Images.FirstOrDefault(p => p.Id == id);
                }
                return null;
            }
            set 
            {
                OnPropertyChanging(BottomImageProperty);

                this.Model.ImageIdBottom = (value != null) ? (Int32?)value.Id : null;
                OnPropertyChanged(BottomImageProperty);
                OnPropertyChanged("BottomImageData");
            }
        }
        public FixtureImage LeftImage
        {
            get
            {
                Int32? id = this.Model.ImageIdLeft;
                if (id != null && ParentComponentView != null)
                {
                    return ParentComponentView.Images.FirstOrDefault(p => p.Id == id);
                }
                return null;
            }
            set {OnPropertyChanging(LeftImageProperty);

            this.Model.ImageIdLeft = (value != null) ? (Int32?)value.Id : null;
                OnPropertyChanged(LeftImageProperty);
                OnPropertyChanged("LeftImageData");
            }
        }
        public FixtureImage RightImage
        {
            get
            {
                Int32? id = this.Model.ImageIdRight;
                if (id != null && ParentComponentView != null)
                {
                    return ParentComponentView.Images.FirstOrDefault(p => p.Id == id);
                }
                return null;
            }
            set {OnPropertyChanging(RightImageProperty);

            this.Model.ImageIdRight = (value != null) ? (Int32?)value.Id : null;
                OnPropertyChanged(RightImageProperty);
                OnPropertyChanged("RightImageData");
            }
        }

        #endregion

        public Boolean IsMerchandisable
        {
            get
            {
                return this.MerchandisingType != FixtureSubComponentMerchandisingType.None;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="parentComponentView"></param>
        public EditorSubComponentView(FixtureSubComponent subComponent)
        {
            _subComponent = subComponent;
            subComponent.PropertyChanging += Model_PropertyChanging;
            subComponent.PropertyChanged += Model_PropertyChanged;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property is changing on the base model objects.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            if (typeof(EditorSubComponentView).GetProperty(e.PropertyName) != null)
            {
                OnPropertyChanging(e.PropertyName);
            }
        }

        /// <summary>
        /// Called whenever a property has changed on the base model objects
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (typeof(EditorSubComponentView).GetProperty(e.PropertyName) != null)
            {
                OnPropertyChanged(e.PropertyName);
            }

            switch (e.PropertyName)
            {
                case "ImageIdFront":
                    OnPropertyChanged("FrontImage");
                    OnPropertyChanged("FrontImageData");
                    break;

                case "ImageIdBack":
                    OnPropertyChanged("BackImage");
                    OnPropertyChanged("BackImageData");
                    break;

                case "ImageIdTop":
                    OnPropertyChanged("TopImage");
                    OnPropertyChanged("TopImageData");
                    break;

                case "ImageIdBottom":
                    OnPropertyChanged("BottomImage");
                    OnPropertyChanged("BottomImageData");
                    break;

                case "ImageIdLeft":
                    OnPropertyChanged("LeftImage");
                    OnPropertyChanged("LeftImageData");
                    break;

                case "ImageIdRight":
                    OnPropertyChanged("RightImage");
                    OnPropertyChanged("RightImageData");
                    break;

                case "IsDividerObstructionByFacing":
                    OnPropertyChanged("DividerObstructionSpacingX");
                    OnPropertyChanged("DividerObstructionSpacingY");
                    OnPropertyChanged("DividerObstructionSpacingZ");
                    break;

                case "FillColourFront":
                case "FillColourBack":
                case "FillColourTop":
                case "FillColourBottom":
                case "FillColourLeft":
                case "FillColourRight":
                    OnPropertyChanged(FillColourProperty);
                    break;
            }
        }

        #endregion

        #region Methods

        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Sets all image properties to the value given.
        /// </summary>
        public void SetAllImageProperties(FixtureImage img)
        {
            this.FrontImage = img;
            this.BackImage = img;
            this.TopImage = img;
            this.BottomImage = img;
            this.LeftImage = img;
            this.RightImage = img;
        }

        /// <summary>
        /// Returns the axis-aligned 3D bounding box for the given item.
        /// </summary>
        /// <param name="sub"></param>
        /// <returns></returns>
        public Rect3D GetWorldSpaceBounds()
        {
            Rect3D bounds = new Rect3D();
            bounds.SizeX = this.Width;
            bounds.SizeY = this.Height;
            bounds.SizeZ = this.Depth;

            Matrix3D fullTransform = Matrix3D.Identity;

            //append the sub transform
            fullTransform.Append(
                ModelConstruct3DHelper.CreateTransform(
                new RotationValue(this.Angle, this.Slope, this.Roll),
                new PointValue(this.X, this.Y, this.Z)).Value);

            //nb - no relative transform to be applied.

            return new MatrixTransform3D(fullTransform).TransformBounds(bounds);
        }

        #endregion

        #region ISubComponentRenderable Members

        Byte[] IFixtureSubComponentRenderable.FrontImageData
        {
            get
            {
                FixtureImage img = this.FrontImage;
                if (img != null)
                {
                    return img.ImageData;
                }
                return null;
            }
        }

        Byte[] IFixtureSubComponentRenderable.BackImageData
        {
            get
            {
                FixtureImage img = this.BackImage;
                if (img != null)
                {
                    return img.ImageData;
                }
                return null;
            }
        }

        Byte[] IFixtureSubComponentRenderable.TopImageData
        {
            get
            {
                FixtureImage img = this.TopImage;
                if (img != null)
                {
                    return img.ImageData;
                }
                return null;
            }
        }

        Byte[] IFixtureSubComponentRenderable.BottomImageData
        {
            get
            {
                FixtureImage img = this.BottomImage;
                if (img != null)
                {
                    return img.ImageData;
                }
                return null;
            }
        }

        Byte[] IFixtureSubComponentRenderable.LeftImageData
        {
            get
            {
                FixtureImage img = this.LeftImage;
                if (img != null)
                {
                    return img.ImageData;
                }
                return null;
            }
        }

        Byte[] IFixtureSubComponentRenderable.RightImageData
        {
            get
            {
                FixtureImage img = this.RightImage;
                if (img != null)
                {
                    return img.ImageData;
                }
                return null;
            }
        }

        Boolean IFixtureSubComponentRenderable.IsChest
        {
            get
            {
                if (MerchandisingType == FixtureSubComponentMerchandisingType.Stack)
                {
                    if (FaceThicknessTop == 0
                        && FaceThicknessBottom > 0)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        Int32 IFixtureSubComponentRenderable.FillColourFront
        {
            get { return _subComponent.FillColourFront; }
        }
        Int32 IFixtureSubComponentRenderable.FillColourBack
        {
            get { return _subComponent.FillColourBack; }
        }
        Int32 IFixtureSubComponentRenderable.FillColourTop
        {
            get { return _subComponent.FillColourTop; }
        }
        Int32 IFixtureSubComponentRenderable.FillColourBottom
        {
            get { return _subComponent.FillColourBottom; }
        }
        Int32 IFixtureSubComponentRenderable.FillColourLeft
        {
            get { return _subComponent.FillColourLeft; }
        }
        Int32 IFixtureSubComponentRenderable.FillColourRight
        {
            get { return _subComponent.FillColourRight; }
        }
        Int32 IFixtureSubComponentRenderable.LineColour
        {
            get { return _subComponent.LineColour; }
        }
        Int32 IFixtureSubComponentRenderable.RiserColour
        {
            get { return _subComponent.RiserColour; }
        }



        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _subComponent.PropertyChanging -= Model_PropertyChanging;
                    _subComponent.PropertyChanged -= Model_PropertyChanged;

                    RemoveAllChildEventHooks();
                }
                base.IsDisposed = true;
            }

        }

        #endregion

        #region IDataErrorInfo Members

        public String Error
        {
            get { throw new NotImplementedException(); }
        }

        public String this[String columnName]
        {
            get
            {
                //TODO: Implement
                return null;
            }
        }

        #endregion

    }

}
