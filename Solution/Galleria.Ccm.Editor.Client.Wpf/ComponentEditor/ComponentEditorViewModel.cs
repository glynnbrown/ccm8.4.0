﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
// V8-25734 : L.Ineson
//  Moved out rendering controls to framework.
// V8-27204 : I. George
//  Changed the Friendly description names containing plan/planogram to component
// V8-27242 : I.George
//  Added SmallIcon Images to ZoomToFit, ZoomIn, ZoomOut and ZoomArea commands
// V8-27973 : L.Ineson
//  Keyboard shortcuts are no longer processed when the source was a textbox.
#endregion
#endregion

using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Common.Rendering;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Controls.Wpf.Helpers;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf.Rendering;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Rendering;
using Galleria.Framework.Rendering.Interfaces;

namespace Galleria.Ccm.Editor.Client.Wpf.ComponentEditor
{
    /// <summary>
    /// Viewmodel controller for the ConponentEditorOrganiser
    /// </summary>
    public sealed class ComponentEditorViewModel : ViewModelAttachedControlObject<ComponentEditorOrganiser>, IFixtureRenderSettings
    {
        #region Fields

        private Boolean? _dialogResult;

        private EditorComponentView _componentView;
        private FixtureComponent3DData _componentModelData;

        private readonly BulkObservableCollection<IComponentEditorDocument> _documents = new BulkObservableCollection<IComponentEditorDocument>();
        private ReadOnlyBulkObservableCollection<IComponentEditorDocument> _documentsRO;
        private IComponentEditorDocument _selectedDocument;

        private ComponentEditorSelection _selectedSubComponents;

        private String _statusBarText;
       
        private Boolean _showGridlines = false;
        private Double _gridlineMinorDist = 5D;
        private Double _gridlineMajorDist = 10D;

        private Boolean _isPropertiesPanelVisible = false;
        private Boolean _isInPanningMode;
        private Boolean _isInZoomToAreaMode;
        private MouseToolType _mouseToolType;

        private readonly List<EditorSubComponentView> _clipboardList = new List<EditorSubComponentView>();

        private Boolean _suppressPerformanceIntensiveUpdates;

        private Boolean _showFixtureImages = true;
        private Boolean _showFixtureFillPatterns = true;
        private Boolean _showNotches = true;
        private Boolean _showPegHoles = true;
        private Boolean _showDividerLines = true;
        private Boolean _showShelfRisers = true;
        private Boolean _showChestWalls = true;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath ComponentProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.Component);
        public static readonly PropertyPath ComponentModelDataProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.ComponentModelData);
        public static readonly PropertyPath DocumentsProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.Documents);
        public static readonly PropertyPath SelectedDocumentProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.SelectedDocument);
        public static readonly PropertyPath SelectedSubComponentsProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.SelectedSubcomponents);
        public static readonly PropertyPath ShowGridlinesProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.ShowGridlines);
        public static readonly PropertyPath GridlineMinorDistProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.GridlineMinorDist);
        public static readonly PropertyPath GridlineMajorDistProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.GridlineMajorDist);
        public static readonly PropertyPath GridlineAreaHeightProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.GridlineAreaHeight);
        public static readonly PropertyPath GridlineAreaWidthProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.GridlineAreaWidth);
        public static readonly PropertyPath GridlineAreaDepthProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.GridlineAreaDepth);
        public static readonly PropertyPath IsInPanningModeProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.IsInPanningMode);
        public static readonly PropertyPath IsInZoomToAreaModeProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.IsInZoomToAreaMode);
        public static readonly PropertyPath MouseToolTypeProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.MouseToolType);
        public static readonly PropertyPath IsPropertiesPanelVisibleProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.IsPropertiesPanelVisible);
        public static readonly PropertyPath ShowFixtureImagesProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.ShowFixtureImages);
        public static readonly PropertyPath ShowFixtureFillPatternsProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.ShowFixtureFillPatterns);
        public static readonly PropertyPath ShowNotchesProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.ShowNotches);
        public static readonly PropertyPath ShowPegHolesProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.ShowPegHoles);
        public static readonly PropertyPath ShowDividerLinesProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.ShowDividerLines);
        public static readonly PropertyPath ShowShelfRisersProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.ShowShelfRisers);
        public static readonly PropertyPath ShowChestWallsProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.ShowChestWalls);
        public static readonly PropertyPath SuppressPerformanceIntensiveUpdatesProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.SuppressPerformanceIntensiveUpdates);
        public static readonly PropertyPath StatusBarTextProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.StatusBarText);

        //Commands

        public static readonly PropertyPath ApplyCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.ApplyCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.CancelCommand);

        public static readonly PropertyPath NewDocumentCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.NewDocumentCommand);
        public static readonly PropertyPath CopyCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.CopyCommand);
        public static readonly PropertyPath CutCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.CutCommand);
        public static readonly PropertyPath PasteCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.PasteCommand);
        public static readonly PropertyPath DeleteSelectedCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.DeleteSelectedCommand);
        public static readonly PropertyPath ZoomToFitCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.ZoomToFitCommand);
        public static readonly PropertyPath ZoomInCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.ZoomInCommand);
        public static readonly PropertyPath ZoomOutCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.ZoomOutCommand);
        public static readonly PropertyPath ZoomSelectionCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.ZoomSelectionCommand);
        public static readonly PropertyPath ToggleZoomToAreaModeCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.ToggleZoomToAreaModeCommand);
        public static readonly PropertyPath TogglePanningModeCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.TogglePanningModeCommand);
        public static readonly PropertyPath RedoCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.RedoCommand);
        public static readonly PropertyPath UndoCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.UndoCommand);
        public static readonly PropertyPath TileViewsHorizontalCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.TileViewsHorizontalCommand);
        public static readonly PropertyPath TileViewsVerticalCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.TileViewsVerticalCommand);
        public static readonly PropertyPath DockAllViewsCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.DockAllViewsCommand);
        public static readonly PropertyPath ArrangeAllViewsCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.ArrangeAllViewsCommand);
        public static readonly PropertyPath ShowPlanItemPropertiesCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.ShowItemPropertiesCommand);
        public static readonly PropertyPath ToggleIsPropertiesPanelVisibleCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorViewModel>(p => p.ToggleIsPropertiesPanelVisibleCommand);
       

        #endregion

        #region Properties

        /// <summary>
        /// Returns the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;

                if (this.AttachedControl != null)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }

        /// <summary>
        /// Returns the component we are editing.
        /// </summary>
        public EditorComponentView Component
        {
            get { return _componentView; }
        }

        /// <summary>
        /// Returns the 3d data used to render the component visual.
        /// </summary>
        public FixtureComponent3DData ComponentModelData
        {
            get { return _componentModelData; }
        }

        /// <summary>
        /// Returns the collection of plan documents for the source plan
        /// </summary>
        public ReadOnlyBulkObservableCollection<IComponentEditorDocument> Documents
        {
            get
            {
                if (_documentsRO == null)
                {
                    _documentsRO = new ReadOnlyBulkObservableCollection<IComponentEditorDocument>(_documents);
                }
                return _documentsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the selected plan document.
        /// </summary>
        public IComponentEditorDocument SelectedDocument
        {
            get { return _selectedDocument; }
            set
            {
                _selectedDocument = value;
                OnPropertyChanged(SelectedDocumentProperty);

                UpdateActiveDocumentFlags();

                //auto turn off panning mode
                this.IsInPanningMode = false;
                this.IsInZoomToAreaMode = false;
            }
        }

        /// <summary>
        /// Returns the view of the selected subcomponents
        /// </summary>
        public ComponentEditorSelection SelectedSubcomponents
        {
            get { return _selectedSubComponents; }
        }


        #region View Settings

        /// <summary>
        /// Gets/Sets whether the document is in panning mode.
        /// Requires IsZoomingSupported = true.
        /// </summary>
        public Boolean IsInPanningMode
        {
            get { return _isInPanningMode; }
            set
            {
                if (_isInPanningMode != value)
                {
                    _isInPanningMode = value;
                    OnPropertyChanged(IsInPanningModeProperty);

                    //Turn off Zoom to area mode
                    if (this.IsInZoomToAreaMode)
                    {
                        this.IsInZoomToAreaMode = false;
                    }
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether the canvas is in zoom to area mode.
        /// </summary>
        public Boolean IsInZoomToAreaMode
        {
            get { return _isInZoomToAreaMode; }
            set
            {
                if (_isInZoomToAreaMode != value)
                {
                    _isInZoomToAreaMode = value;
                    OnPropertyChanged(IsInZoomToAreaModeProperty);

                    //Turn off panning mode
                    if (this.IsInPanningMode)
                    {
                        this.IsInPanningMode = false;
                    }
                }
            }
        }

        /// <summary>
        /// Gets/Sets the current mouse tool type in use.
        /// </summary>
        public MouseToolType MouseToolType
        {
            get { return _mouseToolType; }
            set
            {
                _mouseToolType = value;
                OnPropertyChanged(MouseToolTypeProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether the properties panel is visible
        /// </summary>
        public Boolean IsPropertiesPanelVisible
        {
            get { return _isPropertiesPanelVisible; }
            set
            {
                _isPropertiesPanelVisible = value;
                OnPropertyChanged(IsPropertiesPanelVisibleProperty);
            }
        }


        public Boolean SuppressPerformanceIntensiveUpdates
        {
            get { return _suppressPerformanceIntensiveUpdates; }
            set
            {
                _suppressPerformanceIntensiveUpdates = value;
                OnPropertyChanged(SuppressPerformanceIntensiveUpdatesProperty);
            }
        }


        /// <summary>
        /// Gets/Sets whether fixture images should be shown.
        /// </summary>
        public Boolean ShowFixtureImages
        {
            get { return _showFixtureImages; }
            set
            {
                if (value != _showFixtureImages)
                {
                    _showFixtureImages = value;
                    OnPropertyChanged(ShowFixtureImagesProperty);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether fixture fill patterns should be drawn.
        /// </summary>
        public Boolean ShowFixtureFillPatterns
        {
            get { return _showFixtureFillPatterns; }
            set
            {
                if (value != _showFixtureFillPatterns)
                {
                    _showFixtureFillPatterns = value;
                    OnPropertyChanged(ShowFixtureFillPatternsProperty);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether notches should be drawn.
        /// </summary>
        public Boolean ShowNotches
        {
            get { return _showNotches; }
            set
            {
                if (value != _showNotches)
                {
                    _showNotches = value;
                    OnPropertyChanged(ShowNotchesProperty);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether pegholes should be drawn.
        /// </summary>
        public Boolean ShowPegHoles
        {
            get { return _showPegHoles; }
            set
            {
                if (value != _showPegHoles)
                {
                    _showPegHoles = value;
                    OnPropertyChanged(ShowPegHolesProperty);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether slot lines should be drawn.
        /// </summary>
        public Boolean ShowDividerLines
        {
            get { return _showDividerLines; }
            set
            {
                if (_showDividerLines != value)
                {
                    _showDividerLines = value;
                    OnPropertyChanged(ShowDividerLinesProperty);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether risers should be drawn
        /// </summary>
        public Boolean ShowShelfRisers
        {
            get { return _showShelfRisers; }
            set
            {
                if (_showShelfRisers != value)
                {
                    _showShelfRisers = value;
                    OnPropertyChanged(ShowShelfRisersProperty);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether chest walls
        /// </summary>
        public Boolean ShowChestWalls
        {
            get { return _showChestWalls; }
            set
            {
                if (_showChestWalls != value)
                {
                    _showChestWalls = value;
                    OnPropertyChanged(ShowChestWallsProperty);
                }
            }
        }

        #endregion

        #region Gridline Properties

        /// <summary>
        /// Gets/Sets whether grid lines should be shown
        /// </summary>
        public Boolean ShowGridlines
        {
            get { return _showGridlines; }
            set
            {
                _showGridlines = value;
                OnPropertyChanged(ShowGridlinesProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the major distance of the grid lines
        /// </summary>
        public Double GridlineMajorDist
        {
            get { return _gridlineMajorDist; }
            set
            {
                _gridlineMajorDist = value;
                OnPropertyChanged(GridlineMajorDistProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the minor distance of the gridlines
        /// </summary>
        public Double GridlineMinorDist
        {
            get { return _gridlineMinorDist; }
            set
            {
                _gridlineMinorDist = value;
                OnPropertyChanged(GridlineMinorDistProperty);

                this.GridlineMajorDist = value * 2;
            }
        }

        /// <summary>
        /// Gets the height of the gridlined area
        /// </summary>
        public Double GridlineAreaHeight
        {
            get { return this.Component.Height; }
        }

        /// <summary>
        /// Gets the width of the gridlined area
        /// </summary>
        public Double GridlineAreaWidth
        {
            get { return this.Component.Width; }
        }

        /// <summary>
        /// Gets the depth of the gridlined area
        /// </summary>
        public Double GridlineAreaDepth
        {
            get { return this.Component.Depth; }
        }

        #endregion

        /// <summary>
        /// Gets/Sets the text to be displayed in the status bar.
        /// </summary>
        public String StatusBarText
        {
            get { return _statusBarText; }
            set
            {
                _statusBarText = value;
                OnPropertyChanged(StatusBarTextProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="componentView"></param>
        public ComponentEditorViewModel(EditorComponentView componentView)
        {
            _componentView = componentView;
            _componentView.PropertyChanged += ComponentView_PropertyChanged;
            _componentModelData = new FixtureComponent3DData(componentView, this);

            _selectedSubComponents = new ComponentEditorSelection(componentView);
            _selectedSubComponents.BulkCollectionChanged += SelectedSubComponents_BulkCollectionChanged;

            this.ShowGridlines = true;


            //add a view
            AddNewDocument(ComponentEditorDocumentType.Perspective);
            AddNewDocument(ComponentEditorDocumentType.Front);
            AddNewDocument(ComponentEditorDocumentType.Top);
        }

       
        #endregion

        #region Commands

        #region NewDocument

        private RelayCommand _newDocumentCommand;

        /// <summary>
        /// Creates a new document of the current item
        /// </summary>
        public RelayCommand NewDocumentCommand
        {
            get
            {
                if (_newDocumentCommand == null)
                {
                    _newDocumentCommand = new RelayCommand(
                        p => NewDocument_Executed(p))
                    {
                        //no values as command are param specific 
                    };
                    base.ViewModelCommands.Add(_newDocumentCommand);
                }
                return _newDocumentCommand;
            }
        }

        private void NewDocument_Executed(Object args)
        {
            //determine which type to create
            ComponentEditorDocumentType docType = ComponentEditorDocumentType.Perspective;
            if (args is ComponentEditorDocumentType)
            {
                docType = (ComponentEditorDocumentType)args;
            }

            AddNewDocument(docType);
        }

        #endregion

        #region Copy

        private RelayCommand _copyCommand;

        /// <summary>
        /// Copies the currently selected items
        /// </summary>
        public RelayCommand CopyCommand
        {
            get
            {
                if (_copyCommand == null)
                {
                    _copyCommand = new RelayCommand(
                       p => Copy_Executed(),
                       p => Copy_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_Copy,
                        FriendlyDescription = Message.Ribbon_Copy_Desc,
                        SmallIcon = ImageResources.Copy_16,
                        InputGestureModifiers = MainPageCommands.Copy.InputGestureModifiers,
                        InputGestureKey = MainPageCommands.Copy.InputGestureKey
                    };
                    ViewModelCommands.Add(_copyCommand);
                }
                return _copyCommand;
            }
        }

        private Boolean Copy_CanExecute()
        {
            if (this.SelectedSubcomponents.Count == 0)
            {
                this.CopyCommand.DisabledReason = Message.DisabledReason_NoSelectedItems;
                return false;
            }

            return true;
        }

        private void Copy_Executed()
        {
            _clipboardList.Clear();
            _clipboardList.AddRange(this.SelectedSubcomponents);
        }

        #endregion

        #region Cut


        private RelayCommand _cutCommand;

        /// <summary>
        /// Cuts the selected items from the plan and puts them into the clipboard.
        /// </summary>
        public RelayCommand CutCommand
        {
            get
            {
                if (_cutCommand == null)
                {
                    _cutCommand = new RelayCommand(
                        p => Cut_Executed(),
                        p => Cut_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_Cut,
                        FriendlyDescription = Message.ComponentEditor_Cut_Desc,
                        SmallIcon = ImageResources.Cut_16,
                        InputGestureModifiers = MainPageCommands.Cut.InputGestureModifiers,
                        InputGestureKey = MainPageCommands.Cut.InputGestureKey
                    };
                    ViewModelCommands.Add(_cutCommand);
                }
                return _cutCommand;
            }
        }

        private Boolean Cut_CanExecute()
        {
            if (this.SelectedSubcomponents.Count == 0)
            {
                this.CutCommand.DisabledReason = Message.DisabledReason_NoSelectedItems;
                return false;
            }

            return true;
        }

        private void Cut_Executed()
        {
            //copy to the clipboard
            this.CopyCommand.Execute();

            //delete the item
            this.DeleteSelectedCommand.Execute();
        }

        #endregion

        #region Paste

        private RelayCommand _pasteCommand;

        /// <summary>
        /// Pastes the clipboard items.
        /// </summary>
        public RelayCommand PasteCommand
        {
            get
            {
                if (_pasteCommand == null)
                {
                    _pasteCommand = new RelayCommand(
                        p => Paste_Executed(),
                        p => Paste_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_Paste,
                        FriendlyDescription = Message.ComponentEditor_Paste_Desc,
                        SmallIcon = ImageResources.Paste_16,
                        InputGestureModifiers = MainPageCommands.Paste.InputGestureModifiers,
                        InputGestureKey = MainPageCommands.Paste.InputGestureKey
                    };
                    ViewModelCommands.Add(_pasteCommand);
                }
                return _pasteCommand;
            }
        }

        private Boolean Paste_CanExecute()
        {
            if (_clipboardList.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void Paste_Executed()
        {
            EditorComponentView component = this.Component;
            component.BeginUndoableAction();

            List<EditorSubComponentView> newSelectedItems = new List<EditorSubComponentView>();

            foreach (EditorSubComponentView item in _clipboardList)
            {
                EditorSubComponentView copy = component.AddSubComponentCopy(item);
                if (copy == null || item.Model.IsDeleted) continue; // Do not move item if the source is cut
              
                    item.X += item.Width;
                    newSelectedItems.Add(copy);
              
            }

            component.EndUndoableAction();


            if (this.SelectedSubcomponents.Count > 0) { this.SelectedSubcomponents.Clear(); }
            this.SelectedSubcomponents.AddRange(newSelectedItems);
        }

        #endregion

        #region DeleteSelected

        private RelayCommand _deletedSelectedCommand;

        /// <summary>
        /// Deletes the currently selected items on the active plan
        /// </summary>
        public RelayCommand DeleteSelectedCommand
        {
            get
            {
                if (_deletedSelectedCommand == null)
                {
                    _deletedSelectedCommand = new RelayCommand(
                        p => DeleteSelected_Executed(),
                        p => DeleteSelected_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_DeleteSelected,
                        FriendlyDescription = Message.ComponentEditor_DeleteSelected_Desc,
                        SmallIcon = ImageResources.Delete_16,
                        InputGestureModifiers = MainPageCommands.RemoveSelected.InputGestureModifiers,
                        InputGestureKey = MainPageCommands.RemoveSelected.InputGestureKey
                    };
                    ViewModelCommands.Add(_deletedSelectedCommand);
                }
                return _deletedSelectedCommand;
            }
        }

        private Boolean DeleteSelected_CanExecute()
        {
            if (this.SelectedSubcomponents.Count == 0)
            {
                this.DeleteSelectedCommand.DisabledReason = Message.DisabledReason_NoSelectedItems;
                return false;
            }

            return true;
        }

        private void DeleteSelected_Executed()
        {
            List<EditorSubComponentView> removeItems = this.SelectedSubcomponents.ToList();
            this.SelectedSubcomponents.Clear();

            EditorComponentView fx = this.Component;
            fx.BeginUndoableAction();

            foreach (var item in removeItems)
            {
                fx.RemoveSubComponent(item);
            }

            fx.EndUndoableAction();
        }

        #endregion

        #region Redo

        private RelayCommand _redoCommand;

        /// <summary>
        /// Performs a redo on the active planogram
        /// </summary>
        public RelayCommand RedoCommand
        {
            get
            {
                if (_redoCommand == null)
                {
                    _redoCommand = new RelayCommand(
                        p => Redo_Executed(p),
                        p => Redo_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_Redo,
                        FriendlyDescription = Message.Ribbon_Redo_Desc,
                        SmallIcon = ImageResources.Redo_16,
                        InputGestureModifiers = MainPageCommands.Redo.InputGestureModifiers,
                        InputGestureKey = MainPageCommands.Redo.InputGestureKey
                    };
                    ViewModelCommands.Add(_redoCommand);
                }
                return _redoCommand;
            }
        }

        private Boolean Redo_CanExecute()
        {
            if (this.Component == null)
            {
                return false;
            }

            if (this.Component.RedoActions.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void Redo_Executed(Object args)
        {
            this.Component.Redo(args as ModelUndoAction);
        }

        #endregion

        #region Undo

        private RelayCommand _undoCommand;

        /// <summary>
        /// Performs and undo of the last action on the active planogram.
        /// </summary>
        public RelayCommand UndoCommand
        {
            get
            {
                if (_undoCommand == null)
                {
                    _undoCommand = new RelayCommand(
                        p => Undo_Executed(p),
                        p => Undo_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_Undo,
                        SmallIcon = ImageResources.Undo_16,
                        FriendlyDescription = Message.Ribbon_Undo_Desc,
                        InputGestureModifiers = MainPageCommands.Undo.InputGestureModifiers,
                        InputGestureKey = MainPageCommands.Undo.InputGestureKey
                    };
                    ViewModelCommands.Add(_undoCommand);
                }
                return _undoCommand;
            }
        }

        private Boolean Undo_CanExecute()
        {
            if (this.Component == null)
            {
                return false;
            }

            if (this.Component.UndoActions.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void Undo_Executed(Object args)
        {
            this.Component.Undo(args as ModelUndoAction);
        }

        #endregion

        #region ZoomToFit

        private RelayCommand _zoomToFitCommand;

        /// <summary>
        /// Zooms the view on the active document to fit the available space
        /// </summary>
        public RelayCommand ZoomToFitCommand
        {
            get
            {
                if (_zoomToFitCommand == null)
                {
                    _zoomToFitCommand = new RelayCommand(
                        p => ZoomToFit_Executed(),
                        p => ZoomToFit_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ZoomFitToScreen,
                        FriendlyDescription = Message.ComponentEditor_ZoomFitToScreen_Desc,
                        Icon = ImageResources.ZoomToFit_32,
                        SmallIcon = ImageResources.ZoomToFit_32,
                        InputGestureModifiers = MainPageCommands.ZoomToFit.InputGestureModifiers,
                        InputGestureKey = MainPageCommands.ZoomToFit.InputGestureKey
                    };
                    base.ViewModelCommands.Add(_zoomToFitCommand);
                }
                return _zoomToFitCommand;
            }
        }

        private Boolean ZoomToFit_CanExecute()
        {


            return true;
        }

        private void ZoomToFit_Executed()
        {
            foreach (var doc in this.Documents)
            {
                doc.ZoomToFit();
            }
        }

        #endregion

        #region ZoomIn

         private RelayCommand _zoomInCommand;

        /// <summary>
        /// Zooms in the view on the active document by 1 point
        /// </summary>
        public RelayCommand ZoomInCommand
        {
            get
            {
                if (_zoomInCommand == null)
                {
                    _zoomInCommand = new RelayCommand(
                        p => ZoomIn_Executed(),
                        p => ZoomIn_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ZoomIn,
                        FriendlyDescription = Message.ComponentEditor_ZoomIn_Desc,
                        SmallIcon = ImageResources.ZoomIn_16,
                        Icon = ImageResources.ZoomIn_32,
                        InputGestureModifiers = MainPageCommands.ZoomIn.InputGestureModifiers,
                        InputGestureKey = MainPageCommands.ZoomIn.InputGestureKey
                    };
                    base.ViewModelCommands.Add(_zoomInCommand);
                }
                return _zoomInCommand;
            }
        }

        private Boolean ZoomIn_CanExecute()
        {
            //if zoom to fit is not supported
            if (!this.ZoomToFitCommand.CanExecute())
            {
                this.ZoomInCommand.DisabledReason = this.ZoomToFitCommand.DisabledReason;
                return false;
            }

            return true;
        }

        private void ZoomIn_Executed()
        {
            foreach (var doc in this.Documents)
            {
                doc.ZoomIn();
            }
        }

        #endregion

        #region ZoomOut

        private RelayCommand _zoomOutCommand;

        /// <summary>
        /// Zooms in the view on the active document by 1 point
        /// </summary>
        public RelayCommand ZoomOutCommand
        {
            get
            {
                if (_zoomOutCommand == null)
                {
                    _zoomOutCommand = new RelayCommand(
                        p => ZoomOut_Executed(),
                        p => ZoomOut_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ZoomOut,
                        FriendlyDescription = Message.CmponentEditor_ZoomOut_Desc,
                        SmallIcon = ImageResources.ZoomOut_16,
                        Icon = ImageResources.ZoomOut_32,
                        InputGestureModifiers = MainPageCommands.ZoomOut.InputGestureModifiers,
                        InputGestureKey = MainPageCommands.ZoomOut.InputGestureKey
                    };
                    base.ViewModelCommands.Add(_zoomOutCommand);
                }
                return _zoomOutCommand;
            }
        }

        private Boolean ZoomOut_CanExecute()
        {
            //if zoom to fit is not supported
            if (!this.ZoomToFitCommand.CanExecute())
            {
                this.ZoomOutCommand.DisabledReason = this.ZoomToFitCommand.DisabledReason;
                return false;
            }

            return true;
        }

        private void ZoomOut_Executed()
        {
            foreach (var doc in this.Documents)
            {
                doc.ZoomOut();
            }
        }

        #endregion

        #region ZoomSelection

        private RelayCommand _zoomSelectionCommand;

        /// <summary>
        /// Zooms in the view on the active document by 1 point
        /// </summary>
        public RelayCommand ZoomSelectionCommand
        {
            get
            {
                if (_zoomSelectionCommand == null)
                {
                    _zoomSelectionCommand = new RelayCommand(
                        p => ZoomSelection_Executed(),
                        p => ZoomSelection_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ZoomSelectedPlanItems,
                        FriendlyDescription = Message.ComponentEditor_ZoomSelectedPlanItems_Desc,
                        Icon = ImageResources.ZoomSelectedPlanItems_32,
                        InputGestureModifiers = MainPageCommands.ZoomSelectedItems.InputGestureModifiers,
                        InputGestureKey = MainPageCommands.ZoomSelectedItems.InputGestureKey
                    };
                    base.ViewModelCommands.Add(_zoomSelectionCommand);
                }
                return _zoomSelectionCommand;
            }
        }

        private Boolean ZoomSelection_CanExecute()
        {
            if (this.SelectedSubcomponents.Count == 0)
            {
                return false;
            }

            return true;

        }

        private void ZoomSelection_Executed()
        {
            foreach (var doc in this.Documents)
            {
                doc.ZoomSelection();
            }
        }

        #endregion

        #region ToggleZoomToAreaMode

        private RelayCommand _toggleZoomToAreaModeCommand;

        /// <summary>
        /// Starts a zoom to area drag
        /// </summary>
        public RelayCommand ToggleZoomToAreaModeCommand
        {
            get
            {
                if (_toggleZoomToAreaModeCommand == null)
                {
                    _toggleZoomToAreaModeCommand = new RelayCommand(
                        p => ToggleZoomToAreaMode_Executed(),
                        p => ToggleZoomToAreaMode_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ZoomToArea,
                        FriendlyDescription = Message.Ribbon_ZoomToArea_Desc,
                        Icon = ImageResources.ZoomToArea_32,
                        SmallIcon = ImageResources.ZoomToArea_32,
                        InputGestureModifiers = MainPageCommands.ToggleZoomToAreaMode.InputGestureModifiers,
                        InputGestureKey = MainPageCommands.ToggleZoomToAreaMode.InputGestureKey
                    };
                    base.ViewModelCommands.Add(_toggleZoomToAreaModeCommand);
                }
                return _toggleZoomToAreaModeCommand;
            }
        }

        private Boolean ToggleZoomToAreaMode_CanExecute()
        {
            return true;
        }

        private void ToggleZoomToAreaMode_Executed()
        {
            this.IsInZoomToAreaMode = !this.IsInZoomToAreaMode;
        }

        #endregion

        #region TogglePanningMode

        private RelayCommand _togglePanningModeCommand;

        /// <summary>
        /// Turns panning mode on or off
        /// </summary>
        public RelayCommand TogglePanningModeCommand
        {
            get
            {
                if (_togglePanningModeCommand == null)
                {
                    _togglePanningModeCommand = new RelayCommand(
                        p => TogglePanningMode_Executed(),
                        p => TogglePanningMode_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_TogglePanningMode,
                        FriendlyDescription = Message.ComponentEditor_TogglePanningMode_Desc,
                        SmallIcon = ImageResources.TogglePanningMode_16,
                        InputGestureModifiers = MainPageCommands.TogglePanningMode.InputGestureModifiers,
                        InputGestureKey = MainPageCommands.TogglePanningMode.InputGestureKey
                    };
                    base.ViewModelCommands.Add(_togglePanningModeCommand);
                }
                return _togglePanningModeCommand;
            }
        }

        private Boolean TogglePanningMode_CanExecute()
        {
            return true;
        }

        private void TogglePanningMode_Executed()
        {
            this.IsInPanningMode = !this.IsInPanningMode;
        }

        #endregion

        #region ToggleIsPropertiesPanelVisible

        private RelayCommand _toggleIsPropertiesPanelVisibleCommand;

        public RelayCommand ToggleIsPropertiesPanelVisibleCommand
        {
            get
            {
                if (_toggleIsPropertiesPanelVisibleCommand == null)
                {
                    _toggleIsPropertiesPanelVisibleCommand = new RelayCommand(
                        p => ToggleIsPropertiesPanelVisible_Executed())
                    {
                        FriendlyName = Message.Ribbon_Properties,
                        FriendlyDescription = Message.Ribbon_PropertiesPanel,
                        Icon = ImageResources.PlanItemProperties_32,
                        SmallIcon = ImageResources.PlanItemProperties_16,
                        InputGestureModifiers = MainPageCommands.ShowHidePropertiesPanel.InputGestureModifiers,
                        InputGestureKey = MainPageCommands.ShowHidePropertiesPanel.InputGestureKey
                    };
                    base.ViewModelCommands.Add(_toggleIsPropertiesPanelVisibleCommand);
                }
                return _toggleIsPropertiesPanelVisibleCommand;
            }
        }

        private void ToggleIsPropertiesPanelVisible_Executed()
        {
            this.IsPropertiesPanelVisible = !this.IsPropertiesPanelVisible;
        }

        #endregion

        #region ShowPlanItemProperties


        private RelayCommand _showItemPropertiesCommand;

        /// <summary>
        /// Shows the properties window for the currently 
        /// selected items.
        /// </summary>
        public RelayCommand ShowItemPropertiesCommand
        {
            get
            {
                if (_showItemPropertiesCommand == null)
                {
                    _showItemPropertiesCommand = new RelayCommand(
                        p => ShowItemProperties_Executed(),
                        p => ShowItemProperties_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_Properties,
                        FriendlyDescription = Message.Ribbon_Properties_Desc,
                        Icon = ImageResources.PlanItemProperties_32,
                        SmallIcon = ImageResources.PlanItemProperties_16,
                        InputGestureModifiers = MainPageCommands.ShowSelectedItemProperties.InputGestureModifiers,
                        InputGestureKey = MainPageCommands.ShowSelectedItemProperties.InputGestureKey
                    };
                    this.ViewModelCommands.Add(_showItemPropertiesCommand);
                }
                return _showItemPropertiesCommand;
            }
        }

        private Boolean ShowItemProperties_CanExecute()
        {
            if (this.SelectedSubcomponents.Count == 0)
            {
                this.ShowItemPropertiesCommand.DisabledReason = Message.DisabledReason_NoSelectedItems;
                return false;
            }

            this.ShowItemPropertiesCommand.DisabledReason = null;
            return true;
        }

        private void ShowItemProperties_Executed()
        {
            ComponentEditorPropertiesWindow win = new ComponentEditorPropertiesWindow(this.Component, this.SelectedSubcomponents);
            App.ShowWindow(win, this.AttachedControl, true);
        }

        #endregion

        #region TileViewsHorizontal

        private RelayCommand _tileViewsHorizontalCommand;

        /// <summary>
        /// Tiles all view panels horizontally within the active planogram
        /// </summary>
        public RelayCommand TileViewsHorizontalCommand
        {
            get
            {
                if (_tileViewsHorizontalCommand == null)
                {
                    _tileViewsHorizontalCommand = new RelayCommand(
                        p => TileViewsHorizontal_Executed(),
                        p => TileViewsHorizontal_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ViewsTileHoriz,
                        FriendlyDescription = Message.ComponentEditor_ViewsTileHoriz_Desc,
                        SmallIcon = ImageResources.View_ViewsTileHoriz_16,
                        InputGestureModifiers = MainPageCommands.TileViewsHorizontal.InputGestureModifiers,
                        InputGestureKey = MainPageCommands.TileViewsHorizontal.InputGestureKey
                    };
                    base.ViewModelCommands.Add(_tileViewsHorizontalCommand);
                }
                return _tileViewsHorizontalCommand;
            }
        }

        private Boolean TileViewsHorizontal_CanExecute()
        {
            return true;
        }

        private void TileViewsHorizontal_Executed()
        {
            TileWindows(System.Windows.Controls.Orientation.Horizontal);
        }

        #endregion

        #region TileViewsVertical

        private RelayCommand _tileViewsVerticalCommand;

        /// <summary>
        /// Tiles all view panels vertically within the active planogram window
        /// </summary>
        public RelayCommand TileViewsVerticalCommand
        {
            get
            {
                if (_tileViewsVerticalCommand == null)
                {
                    _tileViewsVerticalCommand = new RelayCommand(
                        p => TileViewsVertical_Executed(),
                        p => TileViewsVertical_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ViewsTileVert,
                        FriendlyDescription = Message.ComponentEditor_ViewsTileVert_Desc,
                        SmallIcon = ImageResources.View_ViewsTileVert_16,
                        InputGestureModifiers = MainPageCommands.TileViewsVertical.InputGestureModifiers,
                        InputGestureKey = MainPageCommands.TileViewsVertical.InputGestureKey
                    };
                    base.ViewModelCommands.Add(_tileViewsVerticalCommand);
                }
                return _tileViewsVerticalCommand;
            }
        }

        private Boolean TileViewsVertical_CanExecute()
        {
            return true;
        }

        private void TileViewsVertical_Executed()
        {
            TileWindows(System.Windows.Controls.Orientation.Vertical);
        }

        #endregion

        #region DockAllViews

        private RelayCommand _dockAllViewsCommand;

        /// <summary>
        /// Docks all views for the active planogram into a single tab group 
        /// with the active view selected.
        /// </summary>
        public RelayCommand DockAllViewsCommand
        {
            get
            {
                if (_dockAllViewsCommand == null)
                {
                    _dockAllViewsCommand = new RelayCommand(
                        p => DockAllViews_Executed(),
                        p => DockAllViews_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ViewsDockAll,
                        FriendlyDescription = Message.ComponentEditor_ViewsDockAll_Desc,
                        SmallIcon = ImageResources.View_ViewsDockAll_16,
                        InputGestureModifiers = MainPageCommands.DockAllViews.InputGestureModifiers,
                        InputGestureKey = MainPageCommands.DockAllViews.InputGestureKey
                    };
                    base.ViewModelCommands.Add(_dockAllViewsCommand);
                }
                return _dockAllViewsCommand;
            }
        }

        private Boolean DockAllViews_CanExecute()
        {
            return true;
        }

        private void DockAllViews_Executed()
        {
            DockAllViews();
        }

        #endregion

        #region ArrangeAllViews

        private RelayCommand _arrangeAllViewsCommand;

        /// <summary>
        /// Arranges all panels within the active controller
        /// </summary>
        public RelayCommand ArrangeAllViewsCommand
        {
            get
            {
                if (_arrangeAllViewsCommand == null)
                {
                    _arrangeAllViewsCommand = new RelayCommand(
                        p => ArrangeAllViews_Executed(),
                        p => ArrangeAllViews_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ViewsArrangeAll,
                        FriendlyDescription = Message.ComponentEditor_ViewsArrangeAll_Desc,
                        Icon = ImageResources.View_ViewsArrangeAll_32,
                        SmallIcon = ImageResources.View_ViewsArrangeAll_16,
                        InputGestureModifiers = MainPageCommands.ArrangeAllViews.InputGestureModifiers,
                        InputGestureKey = MainPageCommands.ArrangeAllViews.InputGestureKey
                    };
                    base.ViewModelCommands.Add(_arrangeAllViewsCommand);
                }
                return _arrangeAllViewsCommand;
            }
        }

        private Boolean ArrangeAllViews_CanExecute()
        {
            return true;
        }

        private void ArrangeAllViews_Executed()
        {
            ArrangeAllWindows();
        }

        #endregion

        #region Apply

        private RelayCommand _applyCommand;

        /// <summary>
        /// Applies changes made back to the planogram or fixture library
        /// (depending on the component loaded)
        /// </summary>
        public RelayCommand ApplyCommand
        {
            get
            {
                if (_applyCommand == null)
                {
                    _applyCommand = new RelayCommand(
                        p => Apply_Executed(),
                        p => Apply_CanExecute())
                    {
                        FriendlyName = Message.Generic_Apply,
                        FriendlyDescription = Message.ComponentEditor_Apply_Desc,
                        Icon = ImageResources.Apply_32,
                        SmallIcon = ImageResources.Apply_32,
                    };
                    base.ViewModelCommands.Add(_applyCommand);
                }
                return _applyCommand;
            }
        }

        private Boolean Apply_CanExecute()
        {
            //must have at least one subcomponent
            if (this.Component.SubComponents.Count == 0)
            {
                this.ApplyCommand.DisabledReason = Message.ComponentEditor_Apply_DisabledNoSubs;
                return false;
            }

            return true;
        }

        private void Apply_Executed()
        {
            this.DialogResult = true;
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel,
                        FriendlyDescription = Message.ComponentEditor_Cancel_Desc,
                        Icon = ImageResources.Delete_32,
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Processes the given key combination.
        /// </summary>
        /// <param name="key">The key pressed</param>
        /// <param name="modifier">Any active modifier</param>
        /// <returns>True if a command was executed.</returns>
        public Boolean ProcessKeyDown(Key key, ModifierKeys modifier)
        {
            Boolean isProcessed = false;

            foreach (IRelayCommand cmd in this.ViewModelCommands.ToList())
            {
                if (cmd.InputGestureKey == key
                    && cmd.InputGestureModifiers == modifier)
                {
                    if (cmd.CanExecute(null))
                    {
                        cmd.Execute(null);
                        isProcessed = true;
                    }
                }
            }

            if (!isProcessed)
            {
                if (this.SelectedDocument != null)
                {
                    this.SelectedDocument.ProcessKeyboardShortcut(modifier, key);
                }
            }

            return isProcessed;
        }

        /// <summary>
        /// Called whenever 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComponentView_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == EditorComponentView.HeightProperty.Path)
            {
                OnPropertyChanged(GridlineAreaHeightProperty);
            }
            else if (e.PropertyName == EditorComponentView.WidthProperty.Path)
            {
                OnPropertyChanged(GridlineAreaWidthProperty);
            }
            else if (e.PropertyName == EditorComponentView.DepthProperty.Path)
            {
                OnPropertyChanged(GridlineAreaDepthProperty);
            }
        }


        /// <summary>
        /// Called when the contents of the selected plan items collection chnages
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>.
        private void SelectedSubComponents_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (EditorSubComponentView item in e.ChangedItems)
                    {
                        FixtureSubComponent3DData model = RenderControlsHelper.FindItemModelData(this.ComponentModelData, item) as FixtureSubComponent3DData;
                        if (model != null)
                        {
                            model.IsSelected = true;
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (EditorSubComponentView item in e.ChangedItems)
                    {
                        FixtureSubComponent3DData model = RenderControlsHelper.FindItemModelData(this.ComponentModelData, item) as FixtureSubComponent3DData;
                        if (model != null)
                        {
                            model.IsSelected = false;
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        //update all selections

                        foreach (IModelConstruct3DData model in this.ComponentModelData.GetAllChildModels())
                        {
                            if (model is FixtureSubComponent3DData)
                            {
                                FixtureSubComponent3DData subComponent = (FixtureSubComponent3DData)model;
                                subComponent.IsSelected = this.SelectedSubcomponents.Contains(subComponent.PlanPart as EditorSubComponentView);
                            }
                        }
                    }
                    break;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds a new document to this controller
        /// </summary>
        /// <param name="docType"></param>
        public IComponentEditorDocument AddNewDocument(ComponentEditorDocumentType docType)
        {
            base.ShowWaitCursor(true);

            EditorComponentView component = this.Component;

            //reset the mouse type back to pointer
            this.MouseToolType = MouseToolType.Pointer;


            IComponentEditorDocument document = this.Documents.FirstOrDefault(d => d.DocumentType == docType);

            //create
            if (document == null)
            {
                switch (docType)
                {
                    case ComponentEditorDocumentType.Front:
                        document = new ComponentEditorVisualDocument(CameraViewType.Front, this);
                        break;

                    case ComponentEditorDocumentType.Perspective:
                        document = new ComponentEditorVisualDocument(CameraViewType.Perspective, this);
                        break;

                    case ComponentEditorDocumentType.Back:
                        document = new ComponentEditorVisualDocument(CameraViewType.Back, this);
                        break;

                    case ComponentEditorDocumentType.Top:
                        document = new ComponentEditorVisualDocument(CameraViewType.Top, this);
                        break;

                    case ComponentEditorDocumentType.Bottom:
                        document = new ComponentEditorVisualDocument(CameraViewType.Bottom, this);
                        break;

                    case ComponentEditorDocumentType.Left:
                        document = new ComponentEditorVisualDocument(CameraViewType.Left, this);
                        break;

                    case ComponentEditorDocumentType.Right:
                        document = new ComponentEditorVisualDocument(CameraViewType.Right, this);
                        break;

                    case ComponentEditorDocumentType.SubComponentList:
                        document = new SubComponentListDocument(this);
                        break;

                    default:
                        Debug.Fail("Document type not handled");
                        break;
                }


                //add and select
                if (document != null)
                {
                    _documents.Add(document);
                }
            }

            //set the selected doc
            this.SelectedDocument = document;


            base.ShowWaitCursor(false);

            return document;
        }

        /// <summary>
        /// Called when a document docking tab is closed.
        /// </summary>
        /// <param name="controller"></param>
        public void CloseDocument(IComponentEditorDocument doc)
        {
            _documents.Remove(doc);

            if (this.SelectedDocument == doc)
            {
                this.SelectedDocument = this.Documents.FirstOrDefault();
            }

            doc.Dispose();
        }

        /// <summary>
        /// Closes all open documents.
        /// </summary>
        public void CloseAllDocuments()
        {
            foreach (var doc in this.Documents.ToList())
            {
                CloseDocument(doc);
            }
        }

        /// <summary>
        /// Tiles all  plan documents in the given orientation
        /// </summary>
        public void TileWindows(Orientation orientation)
        {
            //Pass through
            if (this.AttachedControl != null)
            {
                this.AttachedControl.TileWindows(orientation);
            }
        }

        /// <summary>
        /// Docks all plan documents into a single tab group.
        /// </summary>
        public void DockAllViews()
        {
            //Pass through
            if (this.AttachedControl != null)
            {
                this.AttachedControl.DockAllViews();
            }
        }

        /// <summary>
        /// Arranges all plan documents
        /// </summary>
        public void ArrangeAllWindows()
        {
            //Pass through
            if (this.AttachedControl != null)
            {
                this.AttachedControl.ArrangeAllWindows();
            }
        }

        /// <summary>
        /// Updates the IsActiveDocument flag for all documents held by this controller
        /// </summary>
        private void UpdateActiveDocumentFlags()
        {
            var selectedDoc = this.SelectedDocument;

            foreach (var doc in this.Documents)
            {
                doc.IsActiveDocument = (doc == selectedDoc);
            }
        }

        /// <summary>
        /// Shows a warning requesting user ok to continue if current item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            //return true if the component has not changed.
            if (!this.Component.HasChanged()) return true;


            //otherwise prompt the user to apply changes or cancel the close action
            var promptResult =
            CommonHelper.GetWindowService().ShowMessage(
                Ccm.Common.Wpf.Services.MessageWindowType.Question,
                Message.ComponentEditor_ItemChangedPromptHeader,
                Message.ComponentEditor_ItemChangedPromptDesc,
                Message.Generic_Apply,
                Message.ComponentEditor_ItemChangedPromptDiscard,
                Message.Generic_Cancel);

            if (promptResult == Framework.Controls.Wpf.ModalMessageResult.Button1)
            {
                ApplyCommand.Execute();
                return true; //continue with action
            }
            else if(promptResult == Framework.Controls.Wpf.ModalMessageResult.Button2)
            {
                CancelCommand.Execute();
                return true;//continue with action
            }
            else return false;//action cancelled
        }

        #endregion

        #region IFixtureRenderSettings Members

        Boolean IFixtureRenderSettings.ShowWireframeOnly { get { return false; } set { } }
        Double IFixtureRenderSettings.SelectionLineThickness { get { return 0.2; } }

        ModelColour IFixtureRenderSettings.SelectionColour
        {
            get { return _selectionColour; }
        }
        private static ModelColour _selectionColour =
           ModelColour.NewColour(System.Windows.Media.Colors.Orange.R,
           System.Windows.Media.Colors.Orange.G, System.Windows.Media.
           Colors.Orange.B, System.Windows.Media.Colors.Orange.A);

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _componentView.PropertyChanged -= ComponentView_PropertyChanged;
                    _selectedSubComponents.BulkCollectionChanged -= SelectedSubComponents_BulkCollectionChanged;

                    //dispose all documents
                    List<IComponentEditorDocument> docList = _documents.ToList();
                    _documents.Clear();
                    docList.ForEach(d => d.Dispose());
                }

                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
