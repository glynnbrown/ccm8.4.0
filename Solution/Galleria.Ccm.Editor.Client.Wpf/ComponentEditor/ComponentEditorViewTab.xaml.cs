﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27242 : I.George ~ Created.
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Fluent;

namespace Galleria.Ccm.Editor.Client.Wpf.ComponentEditor
{
    /// <summary>
    /// Interaction logic for ComponentEditorViewTab.xaml
    /// </summary>
    public sealed partial class ComponentEditorViewTab : RibbonTabItem
    {
        #region ViewModel
        public static readonly DependencyProperty ViewModelProperty =
           DependencyProperty.Register("ViewModel", typeof(ComponentEditorViewModel), typeof(ComponentEditorViewTab),
           new PropertyMetadata(null));

        public ComponentEditorViewModel ViewModel
        {
            get { return (ComponentEditorViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }
        #endregion
        #region Constructor

        public ComponentEditorViewTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
