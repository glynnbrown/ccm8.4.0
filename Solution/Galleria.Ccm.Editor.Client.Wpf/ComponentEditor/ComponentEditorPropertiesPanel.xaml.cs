﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion
#region Version History: CCM820
// V8-30668 : L.Luong
//  Added ViewModel_ChildChanged.
#endregion
#endregion

using System;
using System.Windows;
using Csla.Core;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf.PropertyEditor;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Editor.Client.Wpf.ComponentEditor
{
    /// <summary>
    /// Interaction logic for ComponentEditorPropertiesPanel.xaml
    /// </summary>
    public sealed partial class ComponentEditorPropertiesPanel : DockingPanelControl, IDisposable
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ComponentEditorSelection), typeof(ComponentEditorPropertiesPanel),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ComponentEditorPropertiesPanel senderControl = (ComponentEditorPropertiesPanel)obj;

            if (e.OldValue != null)
            {
                ComponentEditorSelection oldModel = (ComponentEditorSelection)e.OldValue;
                oldModel.BulkCollectionChanged -= senderControl.ViewModel_BulkCollectionChanged;
                oldModel.ParentComponent.ChildChanged -= senderControl.ViewModel_ChildChanged;
            }

            if (e.NewValue != null)
            {
                ComponentEditorSelection newModel = (ComponentEditorSelection)e.NewValue;
                newModel.BulkCollectionChanged += senderControl.ViewModel_BulkCollectionChanged;
                newModel.ParentComponent.ChildChanged += senderControl.ViewModel_ChildChanged;
            }

            senderControl.UpdatePropertyDefinitions();
            senderControl.xPropertyGrid.SourceObject = e.NewValue;
        }


        /// <summary>
        /// Gets/Sets the context.
        /// </summary>
        public ComponentEditorSelection ViewModel
        {
            get { return (ComponentEditorSelection)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ComponentEditorPropertiesPanel()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// called if the merchandising type changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_ChildChanged(object sender, ChildChangedEventArgs e)
        {
            if (e.PropertyChangedArgs != null)
            {
                if (e.PropertyChangedArgs.PropertyName == EditorSubComponentMultiView.MerchandisingTypeProperty.Path)
                {
                    UpdatePropertyDefinitions();
                }
            }
        }

        /// <summary>
        /// Called whenever the selection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            UpdatePropertyDefinitions();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the property definitions on the grid.
        /// </summary>
        private void UpdatePropertyDefinitions()
        {
            ComponentEditorSelection propContext = this.ViewModel;
            PropertyEditor propGrid = this.xPropertyGrid;

            if (propGrid.PropertyDescriptions.Count > 0)
            {
                propGrid.PropertyDescriptions.Clear();
            }

            //add in property definitions
            if (propContext != null)
            {
                propGrid.PropertyDescriptions.AddRange(propContext.GetPropertyDefinitions());
            }

        }

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            if (!_isDisposed)
            {
                this.ViewModel = null;

                GC.SuppressFinalize(this);
                _isDisposed = true;
            }
        }

        #endregion
    }
}
