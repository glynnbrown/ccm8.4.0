﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
// V8-25734 : L.Ineson
//  Moved out rendering controls to framework.
#endregion
#region Version History: (CCM 8.01)
// V8-27231 : L.Ineson
// Made the component type setter private.
#endregion
#region Version History: (CCM 8.3)
// V8-32524 : L.Ineson
// Updated to use more of the base model following changes to support annotations.
#endregion
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using Csla.Core;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Common.Rendering;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;


namespace Galleria.Ccm.Editor.Client.Wpf.ComponentEditor
{
    /// <summary>
    /// Provides a simple view of a fixture component as a root item
    /// </summary>
    public sealed class EditorComponentView : ExtendedViewModelObject, IFixtureComponentRenderable
    {
        #region Fields

        private FixturePackage _fixturePackage;

        private readonly ObservableCollection<EditorSubComponentView> _subComponents = new ObservableCollection<EditorSubComponentView>();
        private FixtureComponent _component;
        private PlanogramComponent _planComponent;
        private readonly DisplayUnitOfMeasureCollection _unitsOfMeasure;

        //undo redo
        const Int32 _undoMax = 10;
        private readonly ObservableCollection<ModelUndoAction> _undoActions = new ObservableCollection<ModelUndoAction>();
        private readonly ObservableCollection<ModelUndoAction> _redoActions = new ObservableCollection<ModelUndoAction>();
        private Boolean _suppressChangeRecording;
        private Boolean _isRecordingUndoableAction;
        private ModelUndoAction _processingUndoAction;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath IsRecordingUndoableActionProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.IsRecordingUndoableAction);
        public static readonly PropertyPath SubComponentsProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.SubComponents);

        public static readonly PropertyPath NameProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.Name);
        public static readonly PropertyPath HeightProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.Height);
        public static readonly PropertyPath WidthProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.Width);
        public static readonly PropertyPath DepthProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.Depth);
        public static readonly PropertyPath IsMoveableProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.IsMoveable);
        public static readonly PropertyPath IsDisplayOnlyProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.IsDisplayOnly);
        public static readonly PropertyPath CanAttachShelfEdgeLabelProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.CanAttachShelfEdgeLabel);
        public static readonly PropertyPath RetailerReferenceCodeProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.RetailerReferenceCode);
        public static readonly PropertyPath BarCodeProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.BarCode);
        public static readonly PropertyPath ManufacturerProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.Manufacturer);
        public static readonly PropertyPath ManufacturerPartNameProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.ManufacturerPartName);
        public static readonly PropertyPath ManufacturerPartNumberProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.ManufacturerPartNumber);
        public static readonly PropertyPath SupplierCostPriceProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.SupplierCostPrice);
        public static readonly PropertyPath SupplierDiscountProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.SupplierDiscount);
        public static readonly PropertyPath SupplierLeadTimeProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.SupplierLeadTime);
        public static readonly PropertyPath MinPurchaseQtyProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.MinPurchaseQty);
        public static readonly PropertyPath WeightLimitProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.WeightLimit);
        public static readonly PropertyPath VolumeProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.Volume);
        public static readonly PropertyPath DiameterProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.Diameter);
        public static readonly PropertyPath CapacityProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.Capacity);
        public static readonly PropertyPath ComponentTypeProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.ComponentType);

        public static readonly PropertyPath FillColourProperty = WpfHelper.GetPropertyPath<EditorComponentView>(p => p.FillColour);

        #endregion

        #region Properties

        public FixtureComponent Model
        {
            get { return _component; }
        }

        /// <summary>
        /// Returns the original planogram component.
        /// </summary>
        public PlanogramComponent SourcePlanogramComponent
        {
            get { return _planComponent; }
        }

        /// <summary>
        /// Returns the collection of child SubComponent views.
        /// </summary>
        public ObservableCollection<EditorSubComponentView> SubComponents
        {
            get { return _subComponents; }
        }

        /// <summary>
        /// Returns the collection of linked images.
        /// </summary>
        public ObservableCollection<FixtureImage> Images
        {
            get { return _fixturePackage.Images; }
        }

        /// <summary>
        /// Returns the collection of undo actions available for this planogram.
        /// </summary>
        public ReadOnlyObservableCollection<ModelUndoAction> UndoActions
        {
            get { return new ReadOnlyObservableCollection<ModelUndoAction>(_undoActions); }
        }

        /// <summary>
        /// Returns the collection of redo actions available for this planogram
        /// </summary>
        public ReadOnlyObservableCollection<ModelUndoAction> RedoActions
        {
            get { return new ReadOnlyObservableCollection<ModelUndoAction>(_redoActions); }
        }

        /// <summary>
        /// Returns true if this view is currently in editing mode
        /// During this time any performance intensive updates should be supressed.
        /// </summary>
        public Boolean IsRecordingUndoableAction
        {
            get { return _isRecordingUndoableAction; }
            private set
            {
                _isRecordingUndoableAction = value;
                OnPropertyChanged(IsRecordingUndoableActionProperty);
            }
        }


        #region Model Properties

        public String Name
        {
            get { return _component.Name; }
            set
            {
                _component.Name = value;
            }
        }

        /// <summary>
        /// Gets/Sets the type this component represents.
        /// </summary>
        public FixtureComponentType ComponentType
        {
            get { return _component.ComponentType; }
            private set { _component.ComponentType = value; }
        }

        public Single Height
        {
            get { return _component.Height; }
            set { _component.Height = value; }
        }

        public Single Width
        {
            get { return _component.Width; }
            set { _component.Width = value; }
        }

        public Single Depth
        {
            get { return _component.Depth; }
            set { _component.Depth = value; }
        }

        public Boolean IsMoveable
        {
            get { return _component.IsMoveable; }
            set { _component.IsMoveable = value; }
        }

        public Boolean IsDisplayOnly
        {
            get { return _component.IsDisplayOnly; }
            set { _component.IsDisplayOnly = value; }
        }

        public Boolean CanAttachShelfEdgeLabel
        {
            get { return _component.CanAttachShelfEdgeLabel; }
            set { _component.CanAttachShelfEdgeLabel = value; }
        }

        public String RetailerReferenceCode
        {
            get { return _component.RetailerReferenceCode; }
            set { _component.RetailerReferenceCode = value; }
        }

        public String BarCode
        {
            get { return _component.BarCode; }
            set { _component.BarCode = value; }
        }

        public String Manufacturer
        {
            get { return _component.Manufacturer; }
            set { _component.Manufacturer = value; }
        }

        public String ManufacturerPartName
        {
            get { return _component.ManufacturerPartName; }
            set { _component.ManufacturerPartName = value; }
        }

        public String ManufacturerPartNumber
        {
            get { return _component.ManufacturerPartNumber; }
            set { _component.ManufacturerPartNumber = value; }
        }

        public String SupplierName
        {
            get { return _component.SupplierName; }
            set { _component.SupplierName = value; }
        }

        public String SupplierPartNumber
        {
            get { return _component.SupplierPartNumber; }
            set { _component.SupplierPartNumber = value; }
        }

        public Single? SupplierCostPrice
        {
            get { return _component.SupplierCostPrice; }
            set { _component.SupplierCostPrice = value; }
        }

        public Single? SupplierDiscount
        {
            get { return _component.SupplierDiscount; }
            set { _component.SupplierDiscount = value; }
        }

        public Single? SupplierLeadTime
        {
            get { return _component.SupplierLeadTime; }
            set { _component.SupplierLeadTime = value; }
        }

        public Int32? MinPurchaseQty
        {
            get { return _component.MinPurchaseQty; }
            set { _component.MinPurchaseQty = value; }
        }

        public Single? WeightLimit
        {
            get { return _component.WeightLimit; }
            set { _component.WeightLimit = value; }
        }

        public Single? Weight
        {
            get { return _component.Weight; }
            set { _component.Weight = value; }
        }

        public Single? Volume
        {
            get { return _component.Volume; }
            set { _component.Volume = value; }
        }

        public Single? Diameter
        {
            get { return _component.Diameter; }
            set { _component.Diameter = value; }
        }

        public Int16? Capacity
        {
            get { return _component.Capacity; }
            set { _component.Capacity = value; }
        }

        #endregion


        public DisplayUnitOfMeasureCollection UnitsOfMeasure
        {
            get { return _unitsOfMeasure; }
        }

        /// <summary>
        /// Gets/Sets a general colour for all subcomponents of this
        /// </summary>
        public Int32 FillColour
        {
            get
            {
                Int32 returnVal = 0;
                if (this.SubComponents.Count > 0)
                {
                    returnVal = this.SubComponents.First().FillColour;
                }
                return returnVal;
            }
            set
            {
                foreach (var subComponent in this.SubComponents)
                {
                    subComponent.FillColour = value;
                }
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public EditorComponentView(PlanogramComponent planComponent)
        {
            //subscribe child collections
            AddChildEventHooks(_subComponents);
            _subComponents.CollectionChanged += SubComponents_CollectionChanged;

            _planComponent = planComponent;

            //set uom
            _unitsOfMeasure = DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(planComponent.Parent);

            _fixturePackage = FixturePackage.NewFixturePackage(planComponent);

            _component = _fixturePackage.Components.First();
            _component.PropertyChanging += Model_PropertyChanging;
            _component.PropertyChanged += Model_PropertyChanged;
            _component.ChildChanging += Model_ChildChanging;
            _component.ChildChanged += Model_ChildChanged;
            _component.SubComponents.BulkCollectionChanged += PlanogramComponent_SubComponentsBulkCollectionChanged;

            //load child views.
            PlanogramComponent_SubComponentsBulkCollectionChanged(_component, 
                new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property is changing on the base model objects.
        /// </summary>
        private void Model_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            String propertyName = e.PropertyName;

            //If this is a property change, then check if it is one we dont care about.
            Boolean processUndo = true;
            if (String.IsNullOrEmpty(propertyName)
                || propertyName == "CustomAttributes"
                || propertyName.EndsWith("Async")
                || propertyName.StartsWith("Meta"))
            {
                processUndo = false;
            }


            //record this action.
            if (processUndo)
            {
                RecordUndoableAction(sender, new PropertyChangingEventArgs(propertyName));
            }


            //raise out the vm property change
            if (typeof(EditorComponentView).GetProperty(propertyName) != null)
            {
                OnPropertyChanging(propertyName);
            }
        }

        /// <summary>
        /// Called whenever a property changes on the base model objects.
        /// </summary>
        private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            String propertyName = e.PropertyName;

            //If this is a property change, then check if it is one we dont care about.
            Boolean processUndo = true;
            if (String.IsNullOrEmpty(propertyName)
                || propertyName == "CustomAttributes"
                || propertyName.EndsWith("Async")
                || propertyName.StartsWith("Meta"))
            {
                processUndo = false;
            }


            //record this action and complete it immediately if we are not recording.
            if (processUndo)
            {
                RecordUndoableAction(sender, new PropertyChangedEventArgs(propertyName),
                    /*complete*/!IsRecordingUndoableAction);
            }


            if (typeof(EditorComponentView).GetProperty(e.PropertyName) != null)
            {
                OnPropertyChanged(e.PropertyName);
            }
        }

        /// <summary>
        /// Called whenever a child of the model is about to change.
        /// </summary>
        private void Model_ChildChanging(object sender, ChildChangingEventArgs e)
        {
            //If this is a property change, then check if it is one we dont care about.
            Boolean processUndo = true;
            if (e.PropertyChangingArgs != null
                && (String.IsNullOrEmpty(e.PropertyChangingArgs.PropertyName)
                || e.PropertyChangingArgs.PropertyName == "CustomAttributes"
                || e.PropertyChangingArgs.PropertyName.EndsWith("Async")
                || e.PropertyChangingArgs.PropertyName.StartsWith("Meta")))
            {
                processUndo = false;
            }

            //Process the event for undo 
            if (processUndo)
            {
                RecordUndoableAction(e);
            }
        }

        /// <summary>
        /// Called whenever a child of the component model changes.
        /// </summary>
        private void Model_ChildChanged(object sender, ChildChangedEventArgs e)
        {
            // Clear the redo actions, if we're on the UI thread.
            Dispatcher dispatcher = Dispatcher.FromThread(Thread.CurrentThread);
            if (App.Current != null && dispatcher  == App.Current.Dispatcher)
            {
                try
                {
                    _redoActions.Clear();
                    CommandManager.InvalidateRequerySuggested();
                }
                catch (NotSupportedException)
                {
                    // If, for some reason (likely we're on the wrong thread), we couldn't
                    // clear the redo actions, just carry on regardless.
                }
            }

            //We dont want to process the undo if the property is
            // an async loaded one
            Boolean processUndo = true;
            if (e.PropertyChangedArgs != null
                && (String.IsNullOrEmpty(e.PropertyChangedArgs.PropertyName)
                || e.PropertyChangedArgs.PropertyName == "CustomAttributes"
                || e.PropertyChangedArgs.PropertyName.EndsWith("Async")
                || e.PropertyChangedArgs.PropertyName.StartsWith("Meta")))
            {
                processUndo = false;
            }

            //Process the event for undo.
            if (processUndo)
            {
                RecordUndoableAction(e);
            }
        }

        /// <summary>
        /// Called when the PlanogramComponent.SubComponents collection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanogramComponent_SubComponentsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (FixtureSubComponent subComponent in e.ChangedItems)
                    {
                        EditorSubComponentView view = CreateSubComponentView(subComponent);
                        if (view != null) _subComponents.Add(view);
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (FixtureSubComponent subComponent in e.ChangedItems)
                    {
                        EditorSubComponentView view =
                            _subComponents.FirstOrDefault(a => a.Model == subComponent);
                        if (view != null) _subComponents.Remove(view);
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    _subComponents.Clear();
                    foreach (FixtureSubComponent subComponent in this.Model.SubComponents)
                    {
                        EditorSubComponentView view = CreateSubComponentView(subComponent);
                        if (view != null) _subComponents.Add(view);
                    }
                    break;
            }
        }

        /// <summary>
        /// Called whenever a child object notifies that it has changed
        /// </summary>
        /// <param name="e"></param>
        protected override void OnChildChanged(ChildChangedEventArgs e)
        {
            base.OnChildChanged(e);

            if (e.PropertyChangedArgs != null
                && e.PropertyChangedArgs.PropertyName == EditorSubComponentView.FillColourProperty.Path)
            {
                OnPropertyChanged(FillColourProperty);
            }
        }

        /// <summary>
        /// Called whenever the subcomponents collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SubComponents_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (EditorSubComponentView o in e.NewItems)
                    {
                        AddChildEventHooks(o);
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (EditorSubComponentView o in e.OldItems)
                    {
                        RemoveChildEventHooks(o);
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        RemoveAllChildEventHooks(typeof(EditorSubComponentView));

                        foreach (EditorSubComponentView child in (IEnumerable)sender)
                        {
                            AddChildEventHooks(child);
                        }
                    }
                    break;
            }

            OnPropertyChanged(FillColourProperty);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns true if the fixture package has been edited.
        /// </summary>
        /// <returns></returns>
        public Boolean HasChanged()
        {
            return _fixturePackage.IsDirty && !_fixturePackage.IsInitialized;
        }

        /// <summary>
        /// Adds a new subcomponent.
        /// </summary>
        public EditorSubComponentView AddSubComponent(Single x, Single y, Single z, Single height, Single width, Single depth)
        {
            String subName = String.Format(CultureInfo.CurrentCulture,
                Message.NewPlanogramSubComponentName, this.SubComponents.Count + 1);

            FixtureSubComponent subComponent =
                FixtureSubComponent.NewFixtureSubComponent(subName, height, width, depth, CommonHelper.ColorToInt(Colors.LightGray));

            subComponent.X = x;
            subComponent.Y = y;
            subComponent.Z = z;

            this.Model.SubComponents.Add(subComponent);

            return this.SubComponents.Last(s => s.Model == subComponent);
        }

        /// <summary>
        /// Adds a new copy of the given subcomponent.
        /// </summary>
        public EditorSubComponentView AddSubComponentCopy(EditorSubComponentView src)
        {
            var subComponent = src.Model.Copy();
            this.Model.SubComponents.Add(subComponent);

            return this.SubComponents.Last(s => s.Model == subComponent);
        }

        /// <summary>
        /// Removes the given subcomponent.
        /// </summary>
        /// <param name="subComponentView"></param>
        public void RemoveSubComponent(EditorSubComponentView subComponentView)
        {
            this.Model.SubComponents.Remove(subComponentView.Model);
        }

        /// <summary>
        /// Adds the given image
        /// </summary>
        /// <param name="imgData"></param>
        /// <param name="fileName"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public FixtureImage AddImage(Byte[] imgData, String fileName, String description)
        {
            FixtureImage img = FixtureImage.NewFixtureImage();
            img.FileName = fileName;
            img.Description = description;
            img.ImageData = imgData;
            this.Images.Add(img);

            return img;
        }

        /// <summary>
        /// Returns the view of the given subcomponent model.
        /// </summary>
        /// <param name="sub"></param>
        /// <returns></returns>
        private EditorSubComponentView CreateSubComponentView(FixtureSubComponent sub)
        {
            return new EditorSubComponentView(sub);
        }

        /// <summary>
        /// Creates a planogram component from this view or updates the exiting linked model.
        /// </summary>
        /// <param name="parentPlanogram"></param>
        /// <returns></returns>
        public PlanogramComponent ToPlanogramComponent()
        {
            PlanogramComponent planComponent =
            this.Model.AddOrUpdatePlanogramComponent(_planComponent.Parent);

            List<PlanogramSubComponent> finalSubs = new List<PlanogramSubComponent>();

            //add or update subcomponents
            Single minX = this.SubComponents.Min(c => c.X);
            Single minY = this.SubComponents.Min(c => c.Y);
            Single minZ = this.SubComponents.Min(c => c.Z);

            foreach (PlanogramSubComponent planSub in planComponent.SubComponents)
            {
                //realign the position bounds
                planSub.X = planSub.X - minX;
                planSub.Y = planSub.Y - minY;
                planSub.Z = planSub.Z - minZ;
            }


            //correct the component type.
            planComponent.ComponentType = planComponent.GetPlanogramComponentType();

            return planComponent;
        }

        #endregion

        #region UndoableAction Recording

        /// <summary>
        /// Begins a new edit on this planogram.
        /// This will start recording a new undo action.
        /// </summary>
        public void BeginUndoableAction()
        {
            BeginUndoableAction(null);
        }
        public void BeginUndoableAction(String undoDescription)
        {
            //if we are already recording a group, just ignore this call.
            if (IsRecordingUndoableAction) return;

            //if we are recording a single action, complete it.
            if (_processingUndoAction != null) CompleteUndoableAction();

            //Set the flag and create the new action
            this.IsRecordingUndoableAction = true;
            _processingUndoAction = new ModelUndoAction();
            _processingUndoAction.DescriptionText = undoDescription;
        }

        /// <summary>
        /// Cancels any outstanding edit.
        /// </summary>
        public void CancelUndoableAction(Boolean undoLoggedChanges)
        {
            //if nothing changed, return.
            if (_processingUndoAction == null) return;

            if (undoLoggedChanges
                && _processingUndoAction.ActionItems.Count > 0)
            {
                //commit the action then undo it.
                CompleteUndoableAction();
                Undo();
                _redoActions.Remove(_redoActions.FirstOrDefault());
            }
            else
            {
                //just discard the action
                _processingUndoAction = null;
                IsRecordingUndoableAction = false;
            }
        }

        /// <summary>
        /// Completes the current edit.
        /// A new undo action will be created for actions recorded
        /// </summary>
        /// <returns>true if an action was created</returns>
        public void EndUndoableAction()
        {
            //turn off recording
            this.IsRecordingUndoableAction = false;

            //if nothing changed, return false.
            if (_processingUndoAction == null) return;

            CompleteUndoableAction();
        }

        /// <summary>
        /// Adds action item if the plan is currently editing
        /// </summary>
        /// <param name="e"></param>
        private void RecordUndoableAction(Object sender, PropertyChangingEventArgs e)
        {
            if (_suppressChangeRecording) return;

            if (String.IsNullOrEmpty(e.PropertyName)) return;
            
            //If we have no active action then create one.
            if (_processingUndoAction == null)
            {
                _processingUndoAction = new ModelUndoAction();
            }

            //add the action item.
            try
            {
                System.Reflection.PropertyInfo pInfo = sender.GetType().GetProperty(e.PropertyName);
                if (pInfo != null && pInfo.CanRead && pInfo.GetSetMethod() != null)
                {
                    Object value = pInfo.GetValue(sender, null);

                    ModelUndoPropertyActionItem item = new ModelUndoPropertyActionItem();
                    item.TargetObject = sender;
                    item.PropertyName = e.PropertyName;
                    item.OldValue = value;
                    item.NewValue = value;
                    item.IsBusy = true;
                    _processingUndoAction.ActionItems.Add(item);
                }
            }
            catch (Exception ex)
            {
                LocalHelper.RecordException(ex);
            }
        }

        /// <summary>
        /// Adds action item if the plan is currently editing
        /// </summary>
        /// <param name="e"></param>
        private void RecordUndoableAction(Object sender, PropertyChangedEventArgs e, Boolean completeAction)
        {
            //just return out if we have no original state saved
            // or if the property is one we cannot record.
            if (_processingUndoAction == null) return;
            if (String.IsNullOrEmpty(e.PropertyName)) return;
            if (_suppressChangeRecording) return;


            //find the initial state in the current action.
            ModelUndoPropertyActionItem item =
                _processingUndoAction.ActionItems.OfType<ModelUndoPropertyActionItem>()
                .LastOrDefault(a => a.TargetObject == sender && a.PropertyName == e.PropertyName);
            if (item == null) return;

            //log the change.
            System.Reflection.PropertyInfo pInfo = sender.GetType().GetProperty(e.PropertyName);
            if (pInfo != null && pInfo.GetSetMethod() != null) item.NewValue = pInfo.GetValue(sender, null);

            //flag the property change action as ready.
            item.IsBusy = false;

            //complete the action immediately if required.
            if (completeAction) CompleteUndoableAction();

        }

        /// <summary>
        /// Adds action item if the plan is currently editing
        /// </summary>
        /// <param name="e"></param>
        private void RecordUndoableAction(ChildChangingEventArgs e)
        {
            if (_suppressChangeRecording) return;

            //PropertyChanging:
            if (e.PropertyChangingArgs != null)
            {
                if (String.IsNullOrEmpty(e.PropertyChangingArgs.PropertyName)) return;
                if (e.PropertyChangingArgs.PropertyName == "IsUpdating") return;

                //If we have no active action then create one.
                if (_processingUndoAction == null)
                {
                    _processingUndoAction = new ModelUndoAction();
                }

                RecordUndoableAction(e.ChildObject, e.PropertyChangingArgs);
            }

            //Collection changing - not required.
        }

        /// <summary>
        /// Adds action item if the plan is currently editing
        /// </summary>
        /// <param name="e"></param>
        private void RecordUndoableAction(ChildChangedEventArgs e)
        {
            if (_suppressChangeRecording) return;

            //** Property Change:
            if (e.PropertyChangedArgs != null)
            {
                //just return out if we have no original state saved
                if (_processingUndoAction == null) return;

                RecordUndoableAction(e.ChildObject, e.PropertyChangedArgs, !IsRecordingUndoableAction);
            }

            //** Collection Change:
            else if (e.CollectionChangedArgs != null)
            {
                //If we have no active action then create one.
                if (_processingUndoAction == null)
                {
                    _processingUndoAction = new ModelUndoAction();
                }

                //create the collection changed action.
                switch (e.CollectionChangedArgs.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        {
                            ModelUndoCollectionActionItem item = new ModelUndoCollectionActionItem();
                            item.ActionType = e.CollectionChangedArgs.Action;
                            item.ModelList = e.ChildObject as IList;
                            item.ModelObject = e.CollectionChangedArgs.NewItems[0] as IModelObject;
                            _processingUndoAction.ActionItems.Add(item);
                        }
                        break;

                    case NotifyCollectionChangedAction.Remove:
                        {
                            ModelUndoCollectionActionItem item = new ModelUndoCollectionActionItem();
                            item.ActionType = e.CollectionChangedArgs.Action;
                            item.ModelList = e.ChildObject as IList;
                            item.ModelObject = e.CollectionChangedArgs.OldItems[0] as IModelObject;
                            _processingUndoAction.ActionItems.Add(item);
                        }
                        break;
                }

                if(!IsRecordingUndoableAction) CompleteUndoableAction();
            }
        }

        /// <summary>
        /// Completes the current;y processing undoable action.
        /// </summary>
        private void CompleteUndoableAction()
        {
            //return out if we are null or still processing.
            if (_processingUndoAction == null) return;
            if (_processingUndoAction.ActionItems.Any(u => u.IsBusy)) return;

            //Copy the action we are undoing
            ModelUndoAction undo = _processingUndoAction;

            //reset the main action
            _processingUndoAction = null;
            IsRecordingUndoableAction = false;

            if (undo.ActionItems.Count > 0)
            {
                //add the undo action
                _undoActions.Insert(0, undo);

                if (_undoActions.Count > _undoMax)
                {
                    _undoActions.Remove(_undoActions.Last());
                }

                //Request a requery so that the undo button updates.
                CommandManager.InvalidateRequerySuggested();
            }
        }

        /// <summary>
        /// Undos all actions upto the one given.
        /// If no action is provided only the last action is removed.
        /// </summary>
        /// <param name="action"></param>
        public void Undo()
        {
            Undo(null);
        }
        public void Undo(ModelUndoAction action)
        {
            ModelUndoAction ac = (action != null) ? action : _undoActions.FirstOrDefault();

            if (ac != null && _undoActions.Contains(ac))
            {
                //flag up that we are processing changes
                // so that things don't get reprocessed.
                _suppressChangeRecording = true;


                foreach (ModelUndoAction undoAct in _undoActions.ToList())
                {
                    if (undoAct.Undo())
                    {
                        _undoActions.Remove(undoAct);

                        //add as a redo item.
                        _redoActions.Insert(0, undoAct);
                        if (_redoActions.Count > _undoMax)
                        {
                            _redoActions.Remove(_redoActions.Last());
                        }
                    }
                    else
                    {
                        Debug.Fail("Failed to undo last action");
                        break;
                    }

                    if (undoAct == ac) break;
                }


                //cancel off process flag.
                _suppressChangeRecording = false;
            }
        }

        /// <summary>
        /// Reapplied all actions upto the one given
        /// If no action is provided only the last action is redone.
        /// </summary>
        /// <param name="action"></param>
        public void Redo()
        {
            Redo(null);
        }
        public void Redo(ModelUndoAction action)
        {
            ModelUndoAction ac = (action != null) ? action : _redoActions.FirstOrDefault();

            if (ac != null && _redoActions.Contains(ac))
            {
                //flag up that we are processing changes
                // so that things don't get reprocessed.
                _suppressChangeRecording = true;

                foreach (ModelUndoAction redoAction in _redoActions.ToList())
                {

                    if (redoAction.Redo())
                    {
                        _redoActions.Remove(redoAction);

                        //add as a undo item again.
                        _undoActions.Insert(0, redoAction);
                        if (_undoActions.Count > _undoMax)
                        {
                            _undoActions.Remove(_undoActions.Last());
                        }
                    }
                    else
                    {
                        Debug.Fail("Failed to redo last action");
                        _redoActions.Remove(redoAction);
                        break;
                    }

                    if (redoAction == ac) break;
                }

                //reallow change recording.
                _suppressChangeRecording = false;
            }
        }

        /// <summary>
        /// Clears out all existing undo and redo actions.
        /// </summary>
        public void ClearAllUndoRedoActions()
        {
            _undoActions.Clear();
            _redoActions.Clear();
        }

        #endregion

        #region IFixtureComponentRenderable Members

        event NotifyCollectionChangedEventHandler IFixtureComponentRenderable.SubComponentsCollectionChanged
        {
            add { _subComponents.CollectionChanged += value; }
            remove { _subComponents.CollectionChanged -= value; }
        }

        IEnumerable<IFixtureSubComponentRenderable> IFixtureComponentRenderable.SubComponents
        {
            get { return this.SubComponents; }
        }

        Single IFixtureComponentRenderable.X { get { return 0; } }
        Single IFixtureComponentRenderable.Y { get { return 0; } }
        Single IFixtureComponentRenderable.Z { get { return 0; } }
        Single IFixtureComponentRenderable.Angle { get { return 0; } }
        Single IFixtureComponentRenderable.Slope { get { return 0; } }
        Single IFixtureComponentRenderable.Roll { get { return 0; } }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _component.PropertyChanging -= Model_PropertyChanging;
                    _component.PropertyChanged -= Model_PropertyChanged;
                    _component.ChildChanging -= Model_ChildChanging;
                    _component.ChildChanged -= Model_ChildChanged;
                    _subComponents.CollectionChanged -= SubComponents_CollectionChanged;
                    _component.SubComponents.BulkCollectionChanged -= PlanogramComponent_SubComponentsBulkCollectionChanged;

                    RemoveAllChildEventHooks();
                }

                base.IsDisposed = true;
            }

        }

        #endregion

    }


}
