﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26495 : L.Ineson
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Framework.Collections;
using System.Windows.Input;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Editor.Client.Wpf.ComponentEditor
{
    public interface IComponentEditorDocument : IDisposable
    {
        /// <summary>
        /// Returns the parent controller of this document
        /// </summary>
        ComponentEditorViewModel ParentController { get; }

        /// <summary>
        /// Returns the type of this document.
        /// </summary>
        ComponentEditorDocumentType DocumentType { get; }

        /// <summary>
        /// Gets/Sets whether this is the active document
        /// for the entire app.
        /// Used for styling purposes.
        /// </summary>
        Boolean IsActiveDocument { get; set; }

        /// <summary>
        /// Gets the document title.
        /// </summary>
        String Title { get; }

        /// <summary>
        /// Gets the document control view.
        /// </summary>
        IComponentEditorDocumentView AttachedDocumentView { get; }

        /// <summary>
        /// The component model source
        /// </summary>
        EditorComponentView Component { get; }

        /// <summary>
        /// Returns the view of selected items.
        /// </summary>
        ComponentEditorSelection SelectedSubComponents { get; }

        #region Zooming / Navigation

        /// <summary>
        /// Returns true if this doc supports zooming
        /// </summary>
        Boolean IsZoomingSupported { get; }

        void ZoomToFit();

        void ZoomIn();

        void ZoomOut();

        void ZoomSelection();

        #endregion


        /// <summary>
        /// Carries out any document specific keyboard commands.
        /// </summary>
        /// <param name="modifiers"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        Boolean ProcessKeyboardShortcut(ModifierKeys modifiers, Key key);
    }


    public interface IComponentEditorDocumentView
    {
        IComponentEditorDocument GetDocument();
        void Dispose();
    }
}
