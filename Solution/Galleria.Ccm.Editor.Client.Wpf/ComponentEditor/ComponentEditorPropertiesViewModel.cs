﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History : CCM800
// V8-26495 : L.Ineson 
//  Created.
#endregion

#region Version History : CCM801
// V8-28760 : A.Kuszyk
//  Amended ShowImageSelector to use session image location.
// V8-28103 : M.Shelley
//  Modified ".." to localised message label
#endregion

#region Version History : CCM811
// V8-29406 : N.Haywood
//  Added file type validation for OpenFileDialog
#endregion

#region Version History : CCM820
// V8-30668 : L.Luong
//  Added IsHang property.
#endregion
#endregion

using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using Galleria.Ccm.Model;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Microsoft.Win32;
using Galleria.Framework.Controls.Wpf;
using System.Collections.Generic;
using System.ComponentModel;

namespace Galleria.Ccm.Editor.Client.Wpf.ComponentEditor
{
    /// <summary>
    /// Viewmodel controller for ComponentEditorPropertiesWindow
    /// </summary>
    public sealed class ComponentEditorPropertiesViewModel : ViewModelAttachedControlObject<ComponentEditorPropertiesWindow>
    {
        #region Fields

        private readonly EditorComponentView _component;
        private readonly ComponentEditorSelection _editorContext;
        private Boolean _isHang;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath ComponentProperty = WpfHelper.GetPropertyPath<ComponentEditorPropertiesViewModel>(p => p.Component);
        public static readonly PropertyPath SubComponentContextProperty = WpfHelper.GetPropertyPath<ComponentEditorPropertiesViewModel>(p => p.SubComponentContext);
        public static readonly PropertyPath EditorContextProperty = WpfHelper.GetPropertyPath<ComponentEditorPropertiesViewModel>(p => p.EditorContext);
        public static readonly PropertyPath IsHangProperty = WpfHelper.GetPropertyPath<ComponentEditorPropertiesViewModel>(p => p.IsHang);

        //Commands
        public static readonly PropertyPath OKCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorPropertiesViewModel>(p => p.OKCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorPropertiesViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath SetAllSubComponentImagesCommandProperty = WpfHelper.GetPropertyPath<ComponentEditorPropertiesViewModel>(p => p.SetAllSubComponentImagesCommand);

        #endregion

        #region Properties

        public EditorComponentView Component
        {
            get { return _component; }
        }

        public ComponentEditorSelection EditorContext
        {
            get { return _editorContext; }
        }

        public EditorSubComponentMultiView SubComponentContext
        {
            get { return _editorContext.SubComponentView; }
        }

        public Boolean IsHang
        {
            get { return _isHang; }
            set
            {
                _isHang = value;

                OnPropertyChanged(IsHangProperty);
            }
        }
        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public ComponentEditorPropertiesViewModel(EditorComponentView component, ComponentEditorSelection selection)
        {
            _component = component;
            _editorContext = selection;

            IsHang = Component.ComponentType == FixtureComponentType.ClipStrip
                || Component.ComponentType == FixtureComponentType.Rod
                || Component.ComponentType == FixtureComponentType.Bar
                || Component.ComponentType == FixtureComponentType.Peg
                || SubComponentContext.MerchandisingType == FixtureSubComponentMerchandisingType.Hang
                || SubComponentContext.MerchandisingType == FixtureSubComponentMerchandisingType.HangFromBottom;

            SubComponentContext.PropertyChanged += EditorComponentHasChanged;
        }

        #endregion

        #region Event Handlers

        public void EditorComponentHasChanged(Object args, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == EditorSubComponentMultiView.MerchandisingTypeProperty.Path)
            {
                IsHang = Component.ComponentType == Model.FixtureComponentType.ClipStrip
                || Component.ComponentType == Model.FixtureComponentType.Rod
                || Component.ComponentType == Model.FixtureComponentType.Bar
                || Component.ComponentType == Model.FixtureComponentType.Peg
                || SubComponentContext.MerchandisingType == FixtureSubComponentMerchandisingType.Hang
                || SubComponentContext.MerchandisingType == FixtureSubComponentMerchandisingType.HangFromBottom;
            }
        }

        #endregion

        #region Commands

        #region SetAllSubComponentImagesCommand

        private RelayCommand _setAllSubComponentImagesCommand;

        /// <summary>
        /// Allows the user to select and set all images
        /// for the current subcomponent
        /// </summary>
        public RelayCommand SetAllSubComponentImagesCommand
        {
            get
            {
                if (_setAllSubComponentImagesCommand == null)
                {
                    _setAllSubComponentImagesCommand = new RelayCommand(
                        p => SetAllSubComponentImages_Executed())
                        {
                            FriendlyName = Message.Generic_Ellipsis
                        };
                    base.ViewModelCommands.Add(_setAllSubComponentImagesCommand);
                }
                return _setAllSubComponentImagesCommand;
            }
        }

        private void SetAllSubComponentImages_Executed()
        {
            if (SubComponentContext.Items.Any())
            {
                FixtureImage img = ShowImageSelector();
                if (img != null)
                {
                    foreach (var sub in SubComponentContext.Items)
                    {
                        sub.SetAllImageProperties(img);
                    }
                }
            }

        }

        #endregion

        #region OKCommand

        private RelayCommand _okCommand;

        /// <summary>
        /// Commits changes and closes the window.
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OK_Executed(),
                        p => OK_CanExecute())
                    {
                        FriendlyName = Message.Generic_OK,
                        InputGestureKey = Key.Enter
                    };
                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }

        private Boolean OK_CanExecute()
        {
            return true;
        }

        private void OK_Executed()
        {
            if (this.AttachedControl != null)
            {
                //make sure that all data is correct and close
                // the attached window.
                this.AttachedControl.UpdateBindingSources();
                this.AttachedControl.Close();
            }

            //end the planogram undo record.
            _component.EndUndoableAction();

        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels any changes made and closes
        /// the window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel,
                        InputGestureKey = Key.Escape
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            _component.CancelUndoableAction(true);

            if (this.AttachedControl != null)
            {
                if (!this.AttachedControl.IsClosing)
                {
                    this.AttachedControl.Close();
                }
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Shows the image selector dialog.
        /// </summary>
        /// <returns></returns>
        private FixtureImage ShowImageSelector()
        {
            if (this.AttachedControl == null) return null;

            FixtureImage img = null;

            //Show the open file dialog.
            String defaultImageDirectory = App.ViewState.SessionImageLocation;

            OpenFileDialog dlg = new OpenFileDialog();
            if (Directory.Exists(defaultImageDirectory))
            {
                dlg.InitialDirectory = defaultImageDirectory;
            }
            dlg.Filter = Message.PlanItemProperties_SelectProductImageFilter;
            dlg.Multiselect = false;
            dlg.CheckFileExists = true;


            if (dlg.ShowDialog(this.AttachedControl) == true)
            {
                List<String> extensions = new List<String>(new String[] { ".png", ".bmp", ".jpg", ".jpeg" });

                if (!extensions.Any(s => String.Equals(Path.GetExtension(dlg.FileName), s, StringComparison.OrdinalIgnoreCase)))
                {
                    ModalMessage msg = new ModalMessage
                        {
                            Title = Message.General_WrongFileType_Title,
                            Header = Message.General_WrongFileType_Title,
                            MessageIcon = ImageResources.Warning_32,
                            Description = Message.General_WrongFileType_Description,
                            ButtonCount = 1,
                            Button1Content = Message.Generic_OK,
                            DefaultButton = ModalMessageButton.Button1
                        };
                    App.ShowWindow(msg, true);
                }
                else
                {
                    String fileName = dlg.FileName;
                    img = _component.AddImage(System.IO.File.ReadAllBytes(fileName), fileName, Path.GetFileNameWithoutExtension(fileName));
                    App.ViewState.SessionImageLocation = Path.GetDirectoryName(fileName);
                }
            }

            return img;
        }

        /// <summary>
        /// Launches the window to select a product image.
        /// </summary>
        /// <param name="propertyName"></param>
        public void SetImage(String propertyName)
        {
            if (_component.Images != null)
            {
                EditorSubComponentView sub = this.EditorContext.FirstOrDefault();
                if (sub != null)
                {
                    PropertyInfo pInfo = typeof(EditorSubComponentView).GetProperty(propertyName);
                    if (pInfo != null && pInfo.CanWrite)
                    {
                        FixtureImage img = ShowImageSelector();
                        if (img != null)
                        {
                            pInfo.SetValue(sub, img, null);
                        }

                    }
                }

            }
        }

        /// <summary>
        /// Clears the value of the given image property
        /// </summary>
        /// <param name="propertyName"></param>
        public void ClearImage(String propertyName)
        {
            EditorSubComponentView sub = this.EditorContext.FirstOrDefault();
            if (sub != null)
            {
                PropertyInfo pInfo = typeof(EditorSubComponentView).GetProperty(propertyName);
                if (pInfo != null && pInfo.CanWrite)
                {
                    pInfo.SetValue(sub, null, null);
                }
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                SubComponentContext.PropertyChanged -= EditorComponentHasChanged;
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
