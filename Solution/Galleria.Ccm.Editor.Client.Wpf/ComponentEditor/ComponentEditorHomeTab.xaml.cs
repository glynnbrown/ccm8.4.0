﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion

#endregion


using System.Windows;
using Fluent;

namespace Galleria.Ccm.Editor.Client.Wpf.ComponentEditor
{
    /// <summary>
    /// Interaction logic for ComponentEditorHomeTab.xaml
    /// </summary>
    public sealed partial class ComponentEditorHomeTab : RibbonTabItem
    {
        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ComponentEditorViewModel), typeof(ComponentEditorHomeTab),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets the viewmodel controller for this window.
        /// </summary>
        public ComponentEditorViewModel ViewModel
        {
            get { return (ComponentEditorViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor

        public ComponentEditorHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
