﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26495 : L.Ineson
//  Created
#endregion
#region Version History: CCM803
// V8-29594 : L.Ineson
//  Removed riser and subcomponent transparency properties.
#endregion
#region Version History: CCM820
// V8-30668 : L.Luong
//  Added Merchandisable depth property.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using Galleria.Ccm.Common.Wpf.Converters;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Model;
using Galleria.Framework.Attributes;
using Galleria.Framework.Controls.Wpf.PropertyEditor;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.ComponentEditor
{
    public sealed class EditorSubComponentMultiView : ViewModelObject, IDataErrorInfo
    {
        #region Fields

        private IEnumerable<EditorSubComponentView> _items;

        private EditorComponentView _componentView;
        private Boolean _isLoggingUndoableActions = true;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath NameProperty = GetPropertyPath(p => p.Name);
        public static readonly PropertyPath HeightProperty = GetPropertyPath(p => p.Height);
        public static readonly PropertyPath WidthProperty = GetPropertyPath(p => p.Width);
        public static readonly PropertyPath DepthProperty = GetPropertyPath(p => p.Depth);
        public static readonly PropertyPath XProperty = GetPropertyPath(p => p.X);
        public static readonly PropertyPath YProperty = GetPropertyPath(p => p.Y);
        public static readonly PropertyPath ZProperty = GetPropertyPath(p => p.Z);
        public static readonly PropertyPath AngleProperty = GetPropertyPath(p => p.Angle);
        public static readonly PropertyPath SlopeProperty = GetPropertyPath(p => p.Slope);
        public static readonly PropertyPath RollProperty = GetPropertyPath(p => p.Roll);
        public static readonly PropertyPath FillColourFrontProperty = GetPropertyPath(p => p.FillColourFront);
        public static readonly PropertyPath FillColourBackProperty = GetPropertyPath(p => p.FillColourBack);
        public static readonly PropertyPath FillColourTopProperty = GetPropertyPath(p => p.FillColourTop);
        public static readonly PropertyPath FillColourBottomProperty = GetPropertyPath(p => p.FillColourBottom);
        public static readonly PropertyPath FillColourLeftProperty = GetPropertyPath(p => p.FillColourLeft);
        public static readonly PropertyPath FillColourRightProperty = GetPropertyPath(p => p.FillColourRight);
        public static readonly PropertyPath LineColourProperty = GetPropertyPath(p => p.LineColour);
        public static readonly PropertyPath LineThicknessProperty = GetPropertyPath(p => p.LineThickness);
        public static readonly PropertyPath FillPatternTypeFrontProperty = GetPropertyPath(p => p.FillPatternTypeFront);
        public static readonly PropertyPath FillPatternTypeBackProperty = GetPropertyPath(p => p.FillPatternTypeBack);
        public static readonly PropertyPath FillPatternTypeTopProperty = GetPropertyPath(p => p.FillPatternTypeTop);
        public static readonly PropertyPath FillPatternTypeBottomProperty = GetPropertyPath(p => p.FillPatternTypeBottom);
        public static readonly PropertyPath FillPatternTypeLeftProperty = GetPropertyPath(p => p.FillPatternTypeLeft);
        public static readonly PropertyPath FillPatternTypeRightProperty = GetPropertyPath(p => p.FillPatternTypeRight);
        public static readonly PropertyPath RiserHeightProperty = GetPropertyPath(p => p.RiserHeight);
        public static readonly PropertyPath RiserThicknessProperty = GetPropertyPath(p => p.RiserThickness);
        public static readonly PropertyPath RiserColourProperty = GetPropertyPath(p => p.RiserColour);
        public static readonly PropertyPath IsRiserPlacedOnFrontProperty = GetPropertyPath(p => p.IsRiserPlacedOnFront);
        public static readonly PropertyPath IsRiserPlacedOnBackProperty = GetPropertyPath(p => p.IsRiserPlacedOnBack);
        public static readonly PropertyPath IsRiserPlacedOnLeftProperty = GetPropertyPath(p => p.IsRiserPlacedOnLeft);
        public static readonly PropertyPath IsRiserPlacedOnRightProperty = GetPropertyPath(p => p.IsRiserPlacedOnRight);
        public static readonly PropertyPath NotchStartXProperty = GetPropertyPath(p => p.NotchStartX);
        public static readonly PropertyPath NotchSpacingXProperty = GetPropertyPath(p => p.NotchSpacingX);
        public static readonly PropertyPath NotchStartYProperty = GetPropertyPath(p => p.NotchStartY);
        public static readonly PropertyPath NotchSpacingYProperty = GetPropertyPath(p => p.NotchSpacingY);
        public static readonly PropertyPath NotchWidthProperty = GetPropertyPath(p => p.NotchWidth);
        public static readonly PropertyPath IsNotchPlacedOnFrontProperty = GetPropertyPath(p => p.IsNotchPlacedOnFront);
        public static readonly PropertyPath NotchStyleTypeProperty = GetPropertyPath(p => p.NotchStyleType);
        public static readonly PropertyPath NotchHeightProperty = GetPropertyPath(p => p.NotchHeight);
        public static readonly PropertyPath MerchandisableHeightProperty = GetPropertyPath(p => p.MerchandisableHeight);
        public static readonly PropertyPath MerchandisableDepthProperty = GetPropertyPath(p => p.MerchandisableDepth);
        public static readonly PropertyPath DividerObstructionHeightProperty = GetPropertyPath(p => p.DividerObstructionHeight);
        public static readonly PropertyPath DividerObstructionWidthProperty = GetPropertyPath(p => p.DividerObstructionWidth);
        public static readonly PropertyPath DividerObstructionDepthProperty = GetPropertyPath(p => p.DividerObstructionDepth);
        public static readonly PropertyPath DividerObstructionStartXProperty = GetPropertyPath(p => p.DividerObstructionStartX);
        public static readonly PropertyPath DividerObstructionSpacingXProperty = GetPropertyPath(p => p.DividerObstructionSpacingX);
        public static readonly PropertyPath DividerObstructionStartYProperty = GetPropertyPath(p => p.DividerObstructionStartY);
        public static readonly PropertyPath DividerObstructionSpacingYProperty = GetPropertyPath(p => p.DividerObstructionSpacingY);
        public static readonly PropertyPath DividerObstructionStartZProperty = GetPropertyPath(p => p.DividerObstructionStartZ);
        public static readonly PropertyPath DividerObstructionSpacingZProperty = GetPropertyPath(p => p.DividerObstructionSpacingZ);
        public static readonly PropertyPath IsDividerObstructionAtStartProperty = GetPropertyPath(p => p.IsDividerObstructionAtStart);
        public static readonly PropertyPath IsDividerObstructionAtEndProperty = GetPropertyPath(p => p.IsDividerObstructionAtEnd);
        public static readonly PropertyPath IsDividerObstructionByFacingProperty = GetPropertyPath(p => p.IsDividerObstructionByFacing);
        public static readonly PropertyPath DividerObstructionFillColourProperty = GetPropertyPath(p => p.DividerObstructionFillColour);
        public static readonly PropertyPath DividerObstructionFillPatternProperty = GetPropertyPath(p => p.DividerObstructionFillPattern);
        public static readonly PropertyPath MerchConstraintRow1StartXProperty = GetPropertyPath(p => p.MerchConstraintRow1StartX);
        public static readonly PropertyPath MerchConstraintRow1SpacingXProperty = GetPropertyPath(p => p.MerchConstraintRow1SpacingX);
        public static readonly PropertyPath MerchConstraintRow1StartYProperty = GetPropertyPath(p => p.MerchConstraintRow1StartY);
        public static readonly PropertyPath MerchConstraintRow1SpacingYProperty = GetPropertyPath(p => p.MerchConstraintRow1SpacingY);
        public static readonly PropertyPath MerchConstraintRow1HeightProperty = GetPropertyPath(p => p.MerchConstraintRow1Height);
        public static readonly PropertyPath MerchConstraintRow1WidthProperty = GetPropertyPath(p => p.MerchConstraintRow1Width);
        public static readonly PropertyPath MerchConstraintRow2StartXProperty = GetPropertyPath(p => p.MerchConstraintRow2StartX);
        public static readonly PropertyPath MerchConstraintRow2SpacingXProperty = GetPropertyPath(p => p.MerchConstraintRow2SpacingX);
        public static readonly PropertyPath MerchConstraintRow2StartYProperty = GetPropertyPath(p => p.MerchConstraintRow2StartY);
        public static readonly PropertyPath MerchConstraintRow2SpacingYProperty = GetPropertyPath(p => p.MerchConstraintRow2SpacingY);
        public static readonly PropertyPath MerchConstraintRow2HeightProperty = GetPropertyPath(p => p.MerchConstraintRow2Height);
        public static readonly PropertyPath MerchConstraintRow2WidthProperty = GetPropertyPath(p => p.MerchConstraintRow2Width);
        public static readonly PropertyPath FaceThicknessFrontProperty = GetPropertyPath(p => p.FaceThicknessFront);
        public static readonly PropertyPath FaceThicknessBackProperty = GetPropertyPath(p => p.FaceThicknessBack);
        public static readonly PropertyPath FaceThicknessTopProperty = GetPropertyPath(p => p.FaceThicknessTop);
        public static readonly PropertyPath FaceThicknessBottomProperty = GetPropertyPath(p => p.FaceThicknessBottom);
        public static readonly PropertyPath FaceThicknessLeftProperty = GetPropertyPath(p => p.FaceThicknessLeft);
        public static readonly PropertyPath FaceThicknessRightProperty = GetPropertyPath(p => p.FaceThicknessRight);
        public static readonly PropertyPath MerchandisingTypeProperty = GetPropertyPath(p => p.MerchandisingType);
        public static readonly PropertyPath LeftOverhangProperty = GetPropertyPath(p => p.LeftOverhang);
        public static readonly PropertyPath RightOverhangProperty = GetPropertyPath(p => p.RightOverhang);
        public static readonly PropertyPath FrontOverhangProperty = GetPropertyPath(p => p.FrontOverhang);
        public static readonly PropertyPath BackOverhangProperty = GetPropertyPath(p => p.BackOverhang);
        public static readonly PropertyPath TopOverhangProperty = GetPropertyPath(p => p.TopOverhang);
        public static readonly PropertyPath BottomOverhangProperty = GetPropertyPath(p => p.BottomOverhang);
        public static readonly PropertyPath CombineTypeProperty = GetPropertyPath(p => p.CombineType);
        public static readonly PropertyPath MerchandisingStrategyXProperty = GetPropertyPath(p => p.MerchandisingStrategyX);
        public static readonly PropertyPath MerchandisingStrategyYProperty = GetPropertyPath(p => p.MerchandisingStrategyY);
        public static readonly PropertyPath MerchandisingStrategyZProperty = GetPropertyPath(p => p.MerchandisingStrategyZ);
        public static readonly PropertyPath IsProductOverlapAllowedProperty = GetPropertyPath(p => p.IsProductOverlapAllowed);
        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets whether changes should be logged as undoable actions.
        /// Note - this requires that a plan was specified on create.
        /// </summary>
        public Boolean IsLoggingUndoableActions
        {
            get { return _componentView != null && _isLoggingUndoableActions; }
            set { _isLoggingUndoableActions = value; }
        }

        public IEnumerable<EditorSubComponentView> Items
        {
            get
            {
                return _items;
            }
        }

        public Int32 Count
        {
            get { return Items.Count(); }
        }



        [ModelObjectDisplayName(typeof(FixtureSubComponent), "NameProperty")]
        public String Name
        {
            get
            {
                if (Items.Count() == 1)
                {
                    return Items.First().Name;
                }
                else
                {
                    return "<Multiple>";
                }
            }
            set
            {
                if (Items.Count() == 1)
                {
                    Items.First().Name = value;
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "HeightProperty")]
        public Single? Height
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.HeightProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.HeightProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "WidthProperty")]
        public Single? Width
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.WidthProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.WidthProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "DepthProperty")]
        public Single? Depth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.DepthProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.DepthProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "XProperty")]
        public Single? X
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.XProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.XProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "YProperty")]
        public Single? Y
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.YProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.YProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "ZProperty")]
        public Single? Z
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.ZProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.ZProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "AngleProperty")]
        public Single? Angle
        {
            get
            {
                Single? value = GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.AngleProperty.Path);

                //convert to degrees
                if (value.HasValue)
                {
                    value = Convert.ToSingle(CommonHelper.ToDegrees(value.Value));
                }

                return value;
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.AngleProperty.Path,
                        Convert.ToSingle(CommonHelper.ToRadians(value.Value)));
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "SlopeProperty")]
        public Single? Slope
        {
            get
            {
                Single? value = GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.SlopeProperty.Path);

                //convert to degrees
                if (value.HasValue)
                {
                    value = Convert.ToSingle(CommonHelper.ToDegrees(value.Value));
                }

                return value;
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.SlopeProperty.Path,
                        Convert.ToSingle(CommonHelper.ToRadians(value.Value)));
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "RollProperty")]
        public Single? Roll
        {
            get
            {
                Single? value = GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.RollProperty.Path);

                //convert to degrees
                if (value.HasValue)
                {
                    value = Convert.ToSingle(CommonHelper.ToDegrees(value.Value));
                }

                return value;
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.RollProperty.Path,
                        Convert.ToSingle(CommonHelper.ToRadians(value.Value)));
                }
            }
        }

        #region Colour Properties


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "FillColourFrontProperty")]
        public Color FillColourFront
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, FixtureSubComponent.FillColourFrontProperty.Name);

                if (val.HasValue)
                {
                    return CommonHelper.IntToColor(val.Value);
                }
                else
                {
                    return Colors.White;
                }
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, FixtureSubComponent.FillColourFrontProperty.Name, CommonHelper.ColorToInt(value));
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "FillColourBackProperty")]
        public Color FillColourBack
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, FixtureSubComponent.FillColourBackProperty.Name);

                if (val.HasValue)
                {
                    return CommonHelper.IntToColor(val.Value);
                }
                else
                {
                    return Colors.White;
                }
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, FixtureSubComponent.FillColourBackProperty.Name, CommonHelper.ColorToInt(value));
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "FillColourTopProperty")]
        public Color FillColourTop
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, FixtureSubComponent.FillColourTopProperty.Name);

                if (val.HasValue)
                {
                    return CommonHelper.IntToColor(val.Value);
                }
                else
                {
                    return Colors.White;
                }
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, FixtureSubComponent.FillColourTopProperty.Name, CommonHelper.ColorToInt(value));
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "FillColourBottomProperty")]
        public Color FillColourBottom
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, FixtureSubComponent.FillColourBottomProperty.Name);

                if (val.HasValue)
                {
                    return CommonHelper.IntToColor(val.Value);
                }
                else
                {
                    return Colors.White;
                }
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, FixtureSubComponent.FillColourBottomProperty.Name, CommonHelper.ColorToInt(value));
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "FillColourLeftProperty")]
        public Color FillColourLeft
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, FixtureSubComponent.FillColourLeftProperty.Name);

                if (val.HasValue)
                {
                    return CommonHelper.IntToColor(val.Value);
                }
                else
                {
                    return Colors.White;
                }
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, FixtureSubComponent.FillColourLeftProperty.Name, CommonHelper.ColorToInt(value));
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "FillColourRightProperty")]
        public Color FillColourRight
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, FixtureSubComponent.FillColourRightProperty.Name);

                if (val.HasValue)
                {
                    return CommonHelper.IntToColor(val.Value);
                }
                else
                {
                    return Colors.White;
                }
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, FixtureSubComponent.FillColourRightProperty.Name, CommonHelper.ColorToInt(value));
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "FillPatternTypeFrontProperty")]
        public FixtureSubComponentFillPatternType? FillPatternTypeFront
        {
            get
            {
                return GetCommonPropertyValue<FixtureSubComponentFillPatternType?>(Items, EditorSubComponentView.FillPatternTypeFrontProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.FillPatternTypeFrontProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "FillPatternTypeBackProperty")]
        public FixtureSubComponentFillPatternType? FillPatternTypeBack
        {
            get
            {
                return GetCommonPropertyValue<FixtureSubComponentFillPatternType?>(Items, EditorSubComponentView.FillPatternTypeBackProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.FillPatternTypeBackProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "FillPatternTypeTopProperty")]
        public FixtureSubComponentFillPatternType? FillPatternTypeTop
        {
            get
            {
                return GetCommonPropertyValue<FixtureSubComponentFillPatternType?>(Items, EditorSubComponentView.FillPatternTypeTopProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.FillPatternTypeTopProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "FillPatternTypeBottomProperty")]
        public FixtureSubComponentFillPatternType? FillPatternTypeBottom
        {
            get
            {
                return GetCommonPropertyValue<FixtureSubComponentFillPatternType?>(Items, EditorSubComponentView.FillPatternTypeBottomProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.FillPatternTypeBottomProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "FillPatternTypeLeftProperty")]
        public FixtureSubComponentFillPatternType? FillPatternTypeLeft
        {
            get
            {
                return GetCommonPropertyValue<FixtureSubComponentFillPatternType?>(Items, EditorSubComponentView.FillPatternTypeLeftProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.FillPatternTypeLeftProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "FillPatternTypeRightProperty")]
        public FixtureSubComponentFillPatternType? FillPatternTypeRight
        {
            get
            {
                return GetCommonPropertyValue<FixtureSubComponentFillPatternType?>(Items, EditorSubComponentView.FillPatternTypeRightProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.FillPatternTypeRightProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "LineColourProperty")]
        public Color LineColour
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, FixtureSubComponent.LineColourProperty.Name);

                if (val.HasValue)
                {
                    return CommonHelper.IntToColor(val.Value);
                }
                else
                {
                    return Colors.White;
                }
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, FixtureSubComponent.LineColourProperty.Name, CommonHelper.ColorToInt(value));
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "LineThicknessProperty")]
        public Single? LineThickness
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.LineThicknessProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.LineThicknessProperty.Path, value.Value);
                }
            }
        }

       
        #endregion

        #region Riser Properties

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "RiserHeightProperty")]
        public Single? RiserHeight
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.RiserHeightProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.RiserHeightProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "RiserThicknessProperty")]
        public Single? RiserThickness
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.RiserThicknessProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.RiserThicknessProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "RiserColourProperty")]
        public Color RiserColour
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, FixtureSubComponent.RiserColourProperty.Name);

                if (val.HasValue)
                {
                    return CommonHelper.IntToColor(val.Value);
                }
                else
                {
                    return Colors.White;
                }
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, FixtureSubComponent.RiserColourProperty.Name, CommonHelper.ColorToInt(value));
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "IsRiserPlacedOnFrontProperty")]
        public Boolean? IsRiserPlacedOnFront
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, EditorSubComponentView.IsRiserPlacedOnFrontProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.IsRiserPlacedOnFrontProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "IsRiserPlacedOnBackProperty")]
        public Boolean? IsRiserPlacedOnBack
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, EditorSubComponentView.IsRiserPlacedOnBackProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.IsRiserPlacedOnBackProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "IsRiserPlacedOnLeftProperty")]
        public Boolean? IsRiserPlacedOnLeft
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, EditorSubComponentView.IsRiserPlacedOnLeftProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.IsRiserPlacedOnLeftProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "IsRiserPlacedOnRightProperty")]
        public Boolean? IsRiserPlacedOnRight
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, EditorSubComponentView.IsRiserPlacedOnRightProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.IsRiserPlacedOnRightProperty.Path, value.Value);
                }
            }
        }

        #endregion

        #region Notch Properties

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "NotchStartXProperty")]
        public Single? NotchStartX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.NotchStartXProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.NotchStartXProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "NotchSpacingXProperty")]
        public Single? NotchSpacingX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.NotchSpacingXProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.NotchSpacingXProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "NotchStartYProperty")]
        public Single? NotchStartY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.NotchStartYProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.NotchStartYProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "NotchSpacingYProperty")]
        public Single? NotchSpacingY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.NotchSpacingYProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.NotchSpacingYProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "NotchHeightProperty")]
        public Single? NotchHeight
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.NotchHeightProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.NotchHeightProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "NotchWidthProperty")]
        public Single? NotchWidth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.NotchWidthProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.NotchWidthProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "IsNotchPlacedOnFrontProperty")]
        public Boolean? IsNotchPlacedOnFront
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, EditorSubComponentView.IsNotchPlacedOnFrontProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.IsNotchPlacedOnFrontProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "NotchStyleTypeProperty")]
        [PropertyEditorItemsSource(typeof(PropertyEditorItemsSource<FixtureSubComponentNotchStyleType>))]
        public FixtureSubComponentNotchStyleType? NotchStyleType
        {
            get
            {
                return GetCommonPropertyValue<FixtureSubComponentNotchStyleType?>(Items, EditorSubComponentView.NotchStyleTypeProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.NotchStyleTypeProperty.Path, value.Value);
                }
            }
        }

        #endregion

        #region MerchConstraint Properties

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "MerchConstraintRow1StartXProperty")]
        public Single? MerchConstraintRow1StartX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.MerchConstraintRow1StartXProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.MerchConstraintRow1StartXProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "MerchConstraintRow1SpacingXProperty")]
        public Single? MerchConstraintRow1SpacingX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.MerchConstraintRow1SpacingXProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.MerchConstraintRow1SpacingXProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "MerchConstraintRow1StartYProperty")]
        public Single? MerchConstraintRow1StartY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.MerchConstraintRow1StartYProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.MerchConstraintRow1StartYProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "MerchConstraintRow1SpacingYProperty")]
        public Single? MerchConstraintRow1SpacingY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.MerchConstraintRow1SpacingYProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.MerchConstraintRow1SpacingYProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "MerchConstraintRow1HeightProperty")]
        public Single? MerchConstraintRow1Height
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.MerchConstraintRow1HeightProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.MerchConstraintRow1HeightProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "MerchConstraintRow1WidthProperty")]
        public Single? MerchConstraintRow1Width
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.MerchConstraintRow1WidthProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.MerchConstraintRow1WidthProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "MerchConstraintRow2StartXProperty")]
        public Single? MerchConstraintRow2StartX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.MerchConstraintRow2StartXProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.MerchConstraintRow2StartXProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "MerchConstraintRow2SpacingXProperty")]
        public Single? MerchConstraintRow2SpacingX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.MerchConstraintRow2SpacingXProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.MerchConstraintRow2SpacingXProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "MerchConstraintRow2StartYProperty")]
        public Single? MerchConstraintRow2StartY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.MerchConstraintRow2StartYProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.MerchConstraintRow2StartYProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "MerchConstraintRow2SpacingYProperty")]
        public Single? MerchConstraintRow2SpacingY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.MerchConstraintRow2SpacingYProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.MerchConstraintRow2SpacingYProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "MerchConstraintRow2HeightProperty")]
        public Single? MerchConstraintRow2Height
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.MerchConstraintRow2HeightProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.MerchConstraintRow2HeightProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "MerchConstraintRow2WidthProperty")]
        public Single? MerchConstraintRow2Width
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.MerchConstraintRow2WidthProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.MerchConstraintRow2WidthProperty.Path, value.Value);
                }
            }
        }

        #endregion

        #region Face Thickness Properties

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "FaceThicknessFrontProperty")]
        public Single? FaceThicknessFront
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, FixtureSubComponent.FaceThicknessFrontProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, FixtureSubComponent.FaceThicknessFrontProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "FaceThicknessBackProperty")]
        public Single? FaceThicknessBack
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, FixtureSubComponent.FaceThicknessBackProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, FixtureSubComponent.FaceThicknessBackProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "FaceThicknessTopProperty")]
        public Single? FaceThicknessTop
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, FixtureSubComponent.FaceThicknessTopProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, FixtureSubComponent.FaceThicknessTopProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "FaceThicknessBottomProperty")]
        public Single? FaceThicknessBottom
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, FixtureSubComponent.FaceThicknessBottomProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, FixtureSubComponent.FaceThicknessBottomProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "FaceThicknessLeftProperty")]
        public Single? FaceThicknessLeft
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, FixtureSubComponent.FaceThicknessLeftProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, FixtureSubComponent.FaceThicknessLeftProperty.Name, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "FaceThicknessRightProperty")]
        public Single? FaceThicknessRight
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, FixtureSubComponent.FaceThicknessRightProperty.Name);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, FixtureSubComponent.FaceThicknessRightProperty.Name, value.Value);
                }
            }
        }

        #endregion

        #region DividerObstruction Properties

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "DividerObstructionHeightProperty")]
        public Single? DividerObstructionHeight
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.DividerObstructionHeightProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.DividerObstructionHeightProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "DividerObstructionWidthProperty")]
        public Single? DividerObstructionWidth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.DividerObstructionWidthProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.DividerObstructionWidthProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "DividerObstructionDepthProperty")]
        public Single? DividerObstructionDepth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.DividerObstructionDepthProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.DividerObstructionDepthProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "DividerObstructionStartXProperty")]
        public Single? DividerObstructionStartX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.DividerObstructionStartXProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.DividerObstructionStartXProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "DividerObstructionSpacingXProperty")]
        public Single? DividerObstructionSpacingX
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.DividerObstructionSpacingXProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.DividerObstructionSpacingXProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "DividerObstructionStartYProperty")]
        public Single? DividerObstructionStartY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.DividerObstructionStartYProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.DividerObstructionStartYProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "DividerObstructionSpacingYProperty")]
        public Single? DividerObstructionSpacingY
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.DividerObstructionSpacingYProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.DividerObstructionSpacingYProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "DividerObstructionStartZProperty")]
        public Single? DividerObstructionStartZ
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.DividerObstructionStartZProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.DividerObstructionStartZProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "DividerObstructionSpacingZProperty")]
        public Single? DividerObstructionSpacingZ
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.DividerObstructionSpacingZProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.DividerObstructionSpacingZProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "IsDividerObstructionAtStartProperty")]
        public Boolean? IsDividerObstructionAtStart
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, EditorSubComponentView.IsDividerObstructionAtStartProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.IsDividerObstructionAtStartProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "IsDividerObstructionAtEndProperty")]
        public Boolean? IsDividerObstructionAtEnd
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, EditorSubComponentView.IsDividerObstructionAtEndProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.IsDividerObstructionAtEndProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "IsDividerObstructionByFacingProperty")]
        public Boolean? IsDividerObstructionByFacing
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, EditorSubComponentView.IsDividerObstructionByFacingProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.IsDividerObstructionByFacingProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "DividerObstructionFillColourProperty")]
        public Color DividerObstructionFillColour
        {
            get
            {
                Int32? val = GetCommonPropertyValue<Int32?>(Items, FixtureSubComponent.DividerObstructionFillColourProperty.Name);

                if (val.HasValue)
                {
                    return CommonHelper.IntToColor(val.Value);
                }
                else
                {
                    return Colors.White;
                }
            }
            set
            {
                if (value != null)
                {
                    SetPropertyValue(Items, FixtureSubComponent.DividerObstructionFillColourProperty.Name, CommonHelper.ColorToInt(value));
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "DividerObstructionFillPatternProperty")]
        public FixtureSubComponentFillPatternType? DividerObstructionFillPattern
        {
            get
            {
                return GetCommonPropertyValue<FixtureSubComponentFillPatternType?>(Items, EditorSubComponentView.DividerObstructionFillPatternProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.DividerObstructionFillPatternProperty.Path, value.Value);
                }
            }
        }

        #endregion

        #region Merchandising Properties

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "MerchandisableHeightProperty")]
        public Single? MerchandisableHeight
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.MerchandisableHeightProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.MerchandisableHeightProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "MerchandisableDepthProperty")]
        public Single? MerchandisableDepth
        {
            get
            {
                return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.MerchandisableDepthProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.MerchandisableDepthProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "CombineTypeProperty")]
        [PropertyEditorItemsSource(typeof(PropertyEditorItemsSource<FixtureSubComponentCombineType>))]
        public FixtureSubComponentCombineType? CombineType
        {
            get
            {
                return GetCommonPropertyValue<FixtureSubComponentCombineType?>(Items, EditorSubComponentView.CombineTypeProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.CombineTypeProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "MerchandisingStrategyXProperty")]
        [PropertyEditorItemsSource(typeof(PropertyEditorItemsSource<FixtureSubComponentXMerchStrategyType>))]
        public FixtureSubComponentXMerchStrategyType? MerchandisingStrategyX
        {
            get
            {
                return GetCommonPropertyValue<FixtureSubComponentXMerchStrategyType?>(Items, EditorSubComponentView.MerchandisingStrategyXProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.MerchandisingStrategyXProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "MerchandisingStrategyYProperty")]
        [PropertyEditorItemsSource(typeof(PropertyEditorItemsSource<FixtureSubComponentYMerchStrategyType>))]
        public FixtureSubComponentYMerchStrategyType? MerchandisingStrategyY
        {
            get
            {
                return GetCommonPropertyValue<FixtureSubComponentYMerchStrategyType?>(Items, EditorSubComponentView.MerchandisingStrategyYProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.MerchandisingStrategyYProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "MerchandisingStrategyZProperty")]
        [PropertyEditorItemsSource(typeof(PropertyEditorItemsSource<FixtureSubComponentZMerchStrategyType>))]
        public FixtureSubComponentZMerchStrategyType? MerchandisingStrategyZ
        {
            get
            {
                return GetCommonPropertyValue<FixtureSubComponentZMerchStrategyType?>(Items, EditorSubComponentView.MerchandisingStrategyZProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.MerchandisingStrategyZProperty.Path, value.Value);
                }
            }
        }


        [ModelObjectDisplayName(typeof(FixtureSubComponent), "MerchandisingTypeProperty")]
        [PropertyEditorItemsSource(typeof(PropertyEditorItemsSource<FixtureSubComponentMerchandisingType>))]
        public FixtureSubComponentMerchandisingType? MerchandisingType
        {
            get { return GetCommonPropertyValue<FixtureSubComponentMerchandisingType?>(Items, EditorSubComponentView.MerchandisingTypeProperty.Path); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.MerchandisingTypeProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "IsProductOverlapAllowedProperty")]
        public Boolean? IsProductOverlapAllowed
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, EditorSubComponentView.IsProductOverlapAllowedProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.IsProductOverlapAllowedProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "IsProductSqueezeAllowedProperty")]
        public Boolean? IsProductSqueezeAllowed
        {
            get
            {
                return GetCommonPropertyValue<Boolean?>(Items, EditorSubComponentView.IsProductSqueezeAllowedProperty.Path);
            }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.IsProductSqueezeAllowedProperty.Path, value.Value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "LeftOverhangProperty")]
        public Single? LeftOverhang
        {
            get { return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.LeftOverhangProperty.Path); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.LeftOverhangProperty.Path, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "RightOverhangProperty")]
        public Single? RightOverhang
        {
            get { return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.RightOverhangProperty.Path); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.RightOverhangProperty.Path, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "TopOverhangProperty")]
        public Single? TopOverhang
        {
            get { return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.TopOverhangProperty.Path); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.TopOverhangProperty.Path, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "BottomOverhangProperty")]
        public Single? BottomOverhang
        {
            get { return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.BottomOverhangProperty.Path); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.BottomOverhangProperty.Path, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "FrontOverhangProperty")]
        public Single? FrontOverhang
        {
            get { return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.FrontOverhangProperty.Path); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.FrontOverhangProperty.Path, value);
                }
            }
        }

        [ModelObjectDisplayName(typeof(FixtureSubComponent), "BackOverhangProperty")]
        public Single? BackOverhang
        {
            get { return GetCommonPropertyValue<Single?>(Items, EditorSubComponentView.BackOverhangProperty.Path); }
            set
            {
                if (value.HasValue)
                {
                    SetPropertyValue(Items, EditorSubComponentView.BackOverhangProperty.Path, value);
                }
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="items"></param>
        public EditorSubComponentMultiView(IEnumerable<EditorSubComponentView> items)
        {
            _items = items;
            _componentView = items.First().ParentComponentView;

            foreach (var i in items)
            {
                i.PropertyChanged += Item_PropertyChanged;
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on any linked plan item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(e.PropertyName);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the common value of the given property for all the items
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        private static T GetCommonPropertyValue<T>(IEnumerable<Object> items, String propertyName)
        {
            T value = default(T);

            if (items.Any())
            {
                Type classType = items.First().GetType();
                System.Reflection.PropertyInfo pInfo = classType.GetProperty(propertyName);
                if (pInfo != null)
                {
                    Boolean isFirst = true;
                    foreach (Object item in items)
                    {
                        T itemValue = (T)pInfo.GetValue(item, null);
                        if (isFirst)
                        {
                            value = itemValue;
                        }
                        else if (!Object.Equals(itemValue, value))
                        {
                            return default(T);
                        }

                        isFirst = false;
                    }
                }
            }

            return value;

        }

        /// <summary>
        /// Sets the given value against
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        private void SetPropertyValue<T>(IEnumerable<Object> items, String propertyName, T value)
        {
            if (items.Any())
            {

                //Start logging the change
                Boolean isProcessingUndoable = false;
                if (IsLoggingUndoableActions)
                {
                    if (!_componentView.IsRecordingUndoableAction)
                    {
                        isProcessingUndoable = true;
                        _componentView.BeginUndoableAction(propertyName);
                    }
                }

                //make the property change
                Type classType = items.First().GetType();
                System.Reflection.PropertyInfo pInfo = classType.GetProperty(propertyName);

                if (pInfo != null)
                {
                    foreach (Object item in items)
                    {
                        pInfo.SetValue(item, value, null);
                    }
                }

                //Commit the log.
                if (isProcessingUndoable)
                {
                    _componentView.EndUndoableAction();
                }
            }
        }

        #endregion

        #region Static Helpers

        private static PropertyPath GetPropertyPath(System.Linq.Expressions.Expression<Func<EditorSubComponentMultiView, Object>> expression)
        {
            return WpfHelper.GetPropertyPath<EditorSubComponentMultiView>(expression);
        }

        #endregion

        #region IDataErrorInfo Members

        public String Error
        {
            get { throw new NotImplementedException(); }
        }

        public String this[String columnName]
        {
            get
            {
                return _items.First()[columnName];
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                foreach (var i in _items)
                {
                    i.PropertyChanged -= Item_PropertyChanged;
                }
                base.IsDisposed = true;
            }
        }

        #endregion

    }
}
