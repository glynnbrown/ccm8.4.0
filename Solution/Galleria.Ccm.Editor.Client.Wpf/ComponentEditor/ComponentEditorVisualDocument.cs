﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
// V8-25734 : L.Ineson
//  Moved out rendering controls to framework.
#endregion
#endregion

using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Common.Rendering;
using Galleria.Framework.Controls.Wpf.Rendering;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Controls.Wpf.Helpers;
using Galleria.Framework.Rendering;
using Galleria.Framework.Rendering.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Media3D;

namespace Galleria.Ccm.Editor.Client.Wpf.ComponentEditor
{
    /// <summary>
    /// ViewModel controller for ComponentEditorVisualDocumentView
    /// </summary>
    public sealed class ComponentEditorVisualDocument : ComponentEditorDocument<ComponentEditorVisualDocumentView>
    {
        #region Fields

        private readonly CameraViewType _viewType;
        private Boolean _ignoreNextMouseUpSelection;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath Model3DDataProperty = WpfHelper.GetPropertyPath<ComponentEditorVisualDocument>(p => p.ComponentModelData);
        public static readonly PropertyPath ViewTypeProperty = WpfHelper.GetPropertyPath<ComponentEditorVisualDocument>(p => p.ViewType);
        public static readonly PropertyPath MouseToolTypeProperty = WpfHelper.GetPropertyPath<ComponentEditorVisualDocument>(p => p.MouseToolType);
        public static readonly PropertyPath IsInPanningModeProperty = WpfHelper.GetPropertyPath<ComponentEditorVisualDocument>(p => p.IsInPanningMode);
        public static readonly PropertyPath IsInZoomToAreaModeProperty = WpfHelper.GetPropertyPath<ComponentEditorVisualDocument>(p => p.IsInZoomToAreaMode);

        #endregion

        #region Properties

        public override ComponentEditorDocumentType DocumentType
        {
            get
            {
                switch (_viewType)
                {
                    default:
                    case CameraViewType.Perspective: return ComponentEditorDocumentType.Perspective;
                    case CameraViewType.Front: return ComponentEditorDocumentType.Front;
                    case CameraViewType.Back: return ComponentEditorDocumentType.Back;
                    case CameraViewType.Top: return ComponentEditorDocumentType.Top;
                    case CameraViewType.Bottom: return ComponentEditorDocumentType.Bottom;
                    case CameraViewType.Left: return ComponentEditorDocumentType.Left;
                    case CameraViewType.Right: return ComponentEditorDocumentType.Right;
                }
            }
        }

        /// <summary>
        /// Returns the document title
        /// </summary>
        public override String Title
        {
            get { return CameraViewTypeHelper.FriendlyNames[this.ViewType]; }
        }


        public CameraViewType ViewType
        {
            get { return _viewType; }
        }


        public FixtureComponent3DData ComponentModelData
        {
            get { return ParentController.ComponentModelData; }
        }

        /// <summary>
        /// Gets/Sets the current mouse tool type in use.
        /// </summary>
        public MouseToolType MouseToolType
        {
            get { return ParentController.MouseToolType; }
            set { ParentController.MouseToolType = value; }
        }


        public Boolean IsInPanningMode
        {
            get { return ParentController.IsInPanningMode; }
        }

        public Boolean IsInZoomToAreaMode
        {
            get { return ParentController.IsInZoomToAreaMode; }
            set { ParentController.IsInZoomToAreaMode = value; }
        }

        #endregion

        #region Contructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ComponentEditorVisualDocument(CameraViewType viewType, ComponentEditorViewModel parentController)
            :base(parentController)
        {
            this.IsZoomingSupported = true;

            _viewType = viewType;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on the parent controller
        /// </summary>
        /// <param name="propertyName"></param>
        protected override void OnParentControllerPropertyChanged(String propertyName)
        {
            base.OnParentControllerPropertyChanged(propertyName);

            if (propertyName == ComponentEditorViewModel.MouseToolTypeProperty.Path
                || propertyName == ComponentEditorViewModel.IsInPanningModeProperty.Path
                || propertyName == ComponentEditorViewModel.IsInZoomToAreaModeProperty.Path)
            {
                //pass through
                OnPropertyChanged(propertyName);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Carries out document specific actions based on
        /// the given keyboard shortcut keys.
        /// </summary>
        /// <param name="modifiers"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public override Boolean ProcessKeyboardShortcut(ModifierKeys modifiers, Key key)
        {
            Boolean isProcessed = true;

            //const Single moveAmount = 1;
            const Single resizeAmount = 5F;

            #region [Enter] - Show plan item properties
            if (key == Key.Enter)
            {
                if (this.SelectedSubComponents.Count > 0)
                {
                    this.ParentController.ShowItemPropertiesCommand.Execute();
                }
            }
            #endregion

            #region [Esc] - Reset mouse pointer / Clear selection
            else if (key == Key.Escape)
            {
                if (this.MouseToolType != MouseToolType.Pointer)
                {
                    this.MouseToolType = MouseToolType.Pointer;
                }
                else if (this.SelectedSubComponents.Count > 0)
                {
                    this.SelectedSubComponents.Clear();
                }
            }
            #endregion

            #region  [Ctrl] + [arrow] - Move selected items
            else if (modifiers == ModifierKeys.Control &&
                (key == Key.Up || key == Key.Down || key == Key.Left || key == Key.Right))
            {
                //create new undo if req
                if (!this.Component.IsRecordingUndoableAction)
                {
                    this.Component.BeginUndoableAction();
                }

                const Single changeAmount = 1F;

                Double changeX = 0;
                Double changeY = 0;

                switch (key)
                {
                    case Key.Up: changeY = changeAmount; break;
                    case Key.Down: changeY = -changeAmount; break;
                    case Key.Left: changeX = -changeAmount; break;
                    case Key.Right: changeX = changeAmount; break;
                }

                if (changeX != 0 || changeY != 0)
                {
                    Vector3D moveVector = new Vector3D(1, 1, 0);
                    if (this.AttachedControl != null)
                    {
                        moveVector =
                            ModelConstruct3DHelper.FindItemMoveVector(
                            this.AttachedControl.xViewer.Camera, changeX, changeY);
                    }
                    ComponentEditorHelper.RepositionItems(this.SelectedSubComponents, moveVector.X, moveVector.Y, moveVector.Z);
                }
            }
            #endregion

            #region [Shift + arrow] - Resize selected fixtures items
            else if (modifiers == ModifierKeys.Shift
            && (key == Key.Up || key == Key.Down || key == Key.Left || key == Key.Right))
            {
                //create new undo if req
                if (!this.Component.IsRecordingUndoableAction)
                {
                    this.Component.BeginUndoableAction();
                }

                Double changeX = 0;
                Double changeY = 0;

                switch (key)
                {
                    case Key.Up: changeY = resizeAmount; break;
                    case Key.Down: changeY = -resizeAmount; break;
                    case Key.Left: changeX = -resizeAmount; break;
                    case Key.Right: changeX = resizeAmount; break;
                }

                if (changeX != 0 || changeY != 0)
                {
                    Vector3D moveVector = new Vector3D(1, 1, 0);
                    if (this.AttachedControl != null)
                    {
                        moveVector =
                            ModelConstruct3DHelper.FindItemMoveVector(
                            this.AttachedControl.xViewer.Camera, changeX, changeY);
                    }
                    ComponentEditorHelper.ResizeItems(this.SelectedSubComponents, moveVector.X, moveVector.Y, moveVector.Z);
                }
            }
            #endregion

            #region [Ctrl] + [Shift] + [arrow] - Rotate selected items.
            else if (modifiers == (ModifierKeys.Control | ModifierKeys.Shift)
                && (key == Key.Up || key == Key.Down || key == Key.Left || key == Key.Right))
            {
                //create new undo if req
                if (!this.Component.IsRecordingUndoableAction)
                {
                    this.Component.BeginUndoableAction();
                }

                Single changeAmount = Convert.ToSingle(2 / (360f / (2 * Math.PI)));

                Double rotateX = 0;
                Double rotateY = 0;

                switch (key)
                {
                    case Key.Up: rotateX = -changeAmount; break;
                    case Key.Down: rotateX = changeAmount; break;
                    case Key.Left: rotateY = -changeAmount; break;
                    case Key.Right: rotateY = changeAmount; break;
                }

                if (rotateX != 0 || rotateY != 0)
                {
                    Vector3D moveVector = new Vector3D(1, 1, 0);
                    if (this.AttachedControl != null)
                    {
                        moveVector =
                            ModelConstruct3DHelper.FindItemMoveVector(
                            this.AttachedControl.xViewer.Camera, rotateX, rotateY);
                    }
                    ComponentEditorHelper.RotateItemsAboutCenter(this.SelectedSubComponents, moveVector.X, moveVector.Y, moveVector.Z);
                }
            }
            #endregion

            #region [Ctrl] + [A] - Select all items
            else if (modifiers == ModifierKeys.Control && key == Key.A)
            {
                this.SelectedSubComponents.Clear();

                //add all subcomponents
                this.SelectedSubComponents.AddRange(this.Component.SubComponents);
            }
            #endregion

            else
            {
                isProcessed = false;
            }


            return isProcessed;
        }

        /// <summary>
        /// Updates the selected items collection based on an item clicked by the user.
        /// </summary>
        /// <param name="hitItem"></param>
        /// <param name="modifiers"></param>
        /// <param name="isMouseUp"></param>
        public void ProcessUserClickSelection(EditorSubComponentView hitItem, ModifierKeys modifiers, Boolean isMouseUp)
        {
            ComponentEditorSelection selectedItems = this.SelectedSubComponents;

            if (isMouseUp && _ignoreNextMouseUpSelection)
            {
                //this is a mouseup and we are ignoring 
                // so just reset the flag and return
                _ignoreNextMouseUpSelection = false;
            }
            else if (hitItem == null)
            {
                _ignoreNextMouseUpSelection = false;

                //no item was hit so just clear the current selection
                if (selectedItems.Count > 0) selectedItems.Clear();
            }
            else
            {
                _ignoreNextMouseUpSelection = false;

                Boolean oldIsSelected = selectedItems.Contains(hitItem);


                //+ Mouse Down -
                if (!isMouseUp)
                {
                    if (modifiers == ModifierKeys.None)
                    {
                        if (!oldIsSelected)
                        {
                            //mousedown, no mod, not selected - select single.
                            if (selectedItems.Count > 0) selectedItems.Clear();

                            selectedItems.Add(hitItem);

                            _ignoreNextMouseUpSelection = true;
                        }
                        else
                        {
                            //mouse down, no mod, already selected - do nothing.
                        }
                    }
                    else if (modifiers == ModifierKeys.Control)
                    {
                        if (!oldIsSelected)
                        {
                            //mousedown, ctrl, not selected - add to existing selection

                            selectedItems.Add(hitItem);
                            _ignoreNextMouseUpSelection = true;
                        }
                        else
                        {
                            //mousedown, ctrl, already selected - do nothing
                        }

                    }

                    //TODO:
                    //mousedown, shft, not selected - select all between by seq no
                    //mousedown, shft, already selected - do nothing.

                }

                //+ Mouse Up
                else
                {
                    if (modifiers == ModifierKeys.None)
                    {
                        if (!oldIsSelected)
                        {
                            //mouseup, no mod, not selected - do nothing
                            //should have been ignored during mouse down..
                        }
                        else
                        {
                            //mouseup, no mod, already selected - ensure single select
                            if (selectedItems.Count > 1)
                            {
                                selectedItems.RemoveRange(selectedItems.Where(s => s != hitItem).ToList());
                            }
                        }
                    }
                    else if (modifiers == ModifierKeys.Control)
                    {
                        if (!oldIsSelected)
                        {
                            //mouseup, ctrl, not selected - do nothing
                            //should have been ignored during mouse down..
                        }
                        else
                        {
                            //mouseup, ctrl, already selected - remove from selection
                            selectedItems.Remove(hitItem);
                        }
                    }

                    //TODO:
                    //mouseup, shft, not selected - ignore
                    //mouseup, shft, already selected - remove all between by seq no except selection item.

                }


            }

        }

        /// <summary>
        /// Creates a new fixture part from the given definition
        /// </summary>
        /// <param name="position"></param>
        /// <param name="size"></param>
        /// <param name="toolType"></param>
        public void CreateNewFixturePart(Single posX, Single posY, Single posZ, Single height, Single width, Single depth)
        {
            MouseToolType toolType = this.MouseToolType;

            //reset the tooltype back to a pointer.
            this.MouseToolType = MouseToolType.Pointer;

            //create the part
            EditorSubComponentView newPart;
            if (toolType == MouseToolType.CreatePart)
            {
                EditorComponentView component = this.Component;
                FixtureComponent3DData cModelData = this.ComponentModelData;

                EditorSubComponentView subcomponent = component.AddSubComponent(posX, posY,posZ, height, width, depth);
                newPart = subcomponent;

                //check if this new part collides with another.
                IModelConstruct3DData subData = RenderControlsHelper.FindItemModelData(cModelData, subcomponent);
                Debug.Assert(subData != null, "Failed to find subdata");
                if (subData != null)
                {

                    Rect3D newSubBounds = ModelConstruct3DHelper.GetWorldSpaceBounds(subData);

                    Dictionary<FixtureSubComponent3DData, Rect3D> subBoundsDict = new Dictionary<FixtureSubComponent3DData, Rect3D>();
                    foreach (var modelChild in cModelData.ModelChildren)
                    {
                        FixtureSubComponent3DData childSub = modelChild as FixtureSubComponent3DData;
                        if (childSub != null && childSub.SubComponent != newPart)
                        {
                            subBoundsDict.Add(childSub, ModelConstruct3DHelper.GetWorldSpaceBounds(childSub));
                        }
                    }

                    //check if the new part hits anything and if so move it.
                    foreach (var entry in subBoundsDict)
                    {
                        if (newSubBounds.IntersectsWith(entry.Value))
                        {
                            Rect3D collisionBounds = entry.Value;

                            switch (this.ViewType)
                            {
                                case CameraViewType.Front:
                                default:
                                    newPart.Z = Convert.ToSingle(collisionBounds.Z + newSubBounds.SizeZ);
                                    break;

                                case CameraViewType.Back:
                                    newPart.Z = Convert.ToSingle(collisionBounds.Z - collisionBounds.SizeZ);
                                    break;

                                case CameraViewType.Left:
                                    newPart.X = Convert.ToSingle(collisionBounds.X - newSubBounds.SizeX);
                                    break;

                                case CameraViewType.Right:
                                    newPart.X = Convert.ToSingle(collisionBounds.X + collisionBounds.SizeX);
                                    break;

                                case CameraViewType.Top:
                                    newPart.Y = Convert.ToSingle(collisionBounds.Y + collisionBounds.SizeY);
                                    break;

                                case CameraViewType.Bottom:
                                    newPart.Y = Convert.ToSingle(collisionBounds.Y - newSubBounds.SizeY);
                                    break;
                            }

                            newSubBounds.X = newPart.X;
                            newSubBounds.Y = newPart.Y;
                            newSubBounds.Z = newPart.Z;
                        }
                    }



                    //select it
                    if (this.SelectedSubComponents.Count > 0) this.SelectedSubComponents.Clear();
                    this.SelectedSubComponents.Add(newPart);

                    //zoom to fit all other docs
                    foreach (var doc in this.ParentController.Documents)
                    {
                        if (doc != this)
                        {
                            doc.ZoomToFit();
                        }
                    }

                }
            }

        }

       #endregion

        #region IDisposable

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                DisposeBase();

                base.IsDisposed = true;
            }
        }

        #endregion

    }
}
