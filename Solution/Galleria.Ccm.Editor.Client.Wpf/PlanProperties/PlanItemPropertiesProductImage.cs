﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26148 : L.Ineson
//  Created.
#endregion

#region Version History: CCM801

// V8-28676 : A.Silva
//      Added StoredToolTip dependency property.
//      Clear_CanExecute now will not be enabled if there is a stored tooltip (indicating the image is not in the planogram).

#endregion

#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanProperties
{
    /// <summary>
    /// Control used to display and manage a product image.
    /// </summary>
    public sealed class PlanItemPropertiesProductImage : Control
    {
        #region Properties

        #region Header Property

        public static readonly DependencyProperty HeaderProperty =
            DependencyProperty.Register("Header", typeof(String), typeof(PlanItemPropertiesProductImage),
            new PropertyMetadata(String.Empty));

        /// <summary>
        /// Gets/Sets the image header text.
        /// </summary>
        public String Header
        {
            get { return (String)GetValue(HeaderProperty); }
            set { SetValue(HeaderProperty, value); }
        }

        #endregion

        #region ImageName

        public static readonly DependencyProperty ImageNameProperty =
            DependencyProperty.Register("ImageName", typeof(String), typeof(PlanItemPropertiesProductImage),
            new PropertyMetadata(String.Empty));

        /// <summary>
        /// Gets/Sets the name of the image this represents.
        /// </summary>
        public String ImageName
        {
            get { return (String)GetValue(ImageNameProperty); }
            set { SetValue(ImageNameProperty, value); }
        }

        #endregion

        #region SourceProperty

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(ImageSource), typeof(PlanItemPropertiesProductImage),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the image source
        /// </summary>
        public ImageSource Source
        {
            get { return (ImageSource)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }

        #endregion

        #region IsLoading Property

        public static readonly DependencyProperty IsLoadingProperty =
            DependencyProperty.Register("IsLoading", typeof(Boolean), typeof(PlanItemPropertiesProductImage),
            new PropertyMetadata(true));

        /// <summary>
        /// Gets/Sets whether the image loading busy should be displayed.
        /// </summary>
        public Boolean IsLoading
        {
            get { return (Boolean)GetValue(IsLoadingProperty); }
            set { SetValue(IsLoadingProperty, value); }
        }

        #endregion

        #region StoredToolTip Property

        public static readonly DependencyProperty StoredToolTipProperty = DependencyProperty.Register("StoredToolTip",
            typeof(String), typeof(PlanItemPropertiesProductImage), 
            new PropertyMetadata(null));

        /// <summary>
        ///     Gets or sets the tooltip to show whether an image is stored 
        ///     or retrieved from a folder or the repository.
        /// </summary>
        public String StoredToolTip
        {
            get { return (String)GetValue(StoredToolTipProperty); }
            set { SetValue(StoredToolTipProperty, value); }
        }

        #endregion

        #endregion

        #region Events

        #region EditRequested

        public static readonly RoutedEvent EditRequestedEvent =
            EventManager.RegisterRoutedEvent("EditRequested", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PlanItemPropertiesProductImage));

        public event RoutedEventHandler EditRequested
        {
            add { AddHandler(EditRequestedEvent, value); }
            remove { RemoveHandler(EditRequestedEvent, value); }
        }

        private void OnEditRequested()
        {
            //raise out the selected event
            RaiseEvent(new RoutedEventArgs(PlanItemPropertiesProductImage.EditRequestedEvent));
        }

        #endregion

        #region ClearRequested

        public static readonly RoutedEvent ClearRequestedEvent =
            EventManager.RegisterRoutedEvent("ClearRequested", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PlanItemPropertiesProductImage));

        public event RoutedEventHandler ClearRequested
        {
            add { AddHandler(ClearRequestedEvent, value); }
            remove { RemoveHandler(ClearRequestedEvent, value); }
        }

        private void OnClearRequested()
        {
            //raise out the selected event
            RaiseEvent(new RoutedEventArgs(PlanItemPropertiesProductImage.ClearRequestedEvent));
        }

        #endregion

        #endregion

        #region Constructor

        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline",
            Justification = "Dependency properties are initialized in-line.")]
        static PlanItemPropertiesProductImage()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(PlanItemPropertiesProductImage), new FrameworkPropertyMetadata(typeof(PlanItemPropertiesProductImage)));
        }

        public PlanItemPropertiesProductImage() { }

        #endregion

        #region Commands

        #region EditCommand

        private RelayCommand _editCommand;

        /// <summary>
        /// Triggers the event to allow the image to be set.
        /// </summary>
        public RelayCommand EditCommand
        {
            get
            {
                if (_editCommand == null)
                {
                    _editCommand = new RelayCommand(
                        p => Edit_Executed(),
                        p => Edit_CanExecute())
                    {
                        FriendlyName = Message.PlanItemProperties_EditProductImage,
                        SmallIcon = ImageResources.PlanItemProperties_EditProductImage_16
                    };
                }
                return _editCommand;
            }
        }

        private Boolean Edit_CanExecute()
        {
            if (this.IsLoading) { return false; }
            return true;
        }

        private void Edit_Executed()
        {
            OnEditRequested();
        }

        #endregion

        #region ClearCommand

        private RelayCommand _clearCommand;

        public RelayCommand ClearCommand
        {
            get
            {
                if (_clearCommand == null)
                {
                    _clearCommand = new RelayCommand(
                        p => Clear_Executed(),
                        p => Clear_CanExecute())
                    {
                        FriendlyName = Message.PlanItemProperties_ClearProductImage,
                        SmallIcon = ImageResources.PlanItemProperties_ClearProductImage_16
                    };
                }
                return _clearCommand;
            }
        }

        private Boolean Clear_CanExecute()
        {
            if (this.IsLoading) { return false; }
            if (this.Source == null) { return false; }
            if (!String.IsNullOrEmpty(this.StoredToolTip)) {return false;}
            return true;
        }

        private void Clear_Executed()
        {
            OnClearRequested();
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out actions on mouse double click.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnMouseDoubleClick(e);

            OnEditRequested();
        }

        #endregion
    }
}
