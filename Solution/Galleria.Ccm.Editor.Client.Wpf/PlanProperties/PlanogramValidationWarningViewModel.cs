﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-27589 : A.Silva
//      Created.
// V8-27980 : A.Silva
//      Added ValidationWarningResolvedCount property.
// V8-29054 : M.Pettit
//      Added validation warning visbility based on user settings.
#endregion
#region Version History: (CCM v8.1)
// V8-27828 : L.Ineson
//  Fixed issues with selection mappings.
// V8-30242 : L.Ineson
//  Corrected the way that the warnings collection is loaded.
//  Brought rest of the code up to standard to stop memory leaks.
#endregion
#region Version History: (CCM v8.3)
// V8-31519 : L.Ineson
//  Stopped plan item selection being cleared when validation warning rows are changed.
// V8-32088 : M.Pettit
//  Added ToggleDetailedDescriptionsCommand
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Engine.Tasks.ProductAttrbiuteValidation.V1;
using Galleria.Ccm.Processes.ProductAttributeValidation;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;


namespace Galleria.Ccm.Editor.Client.Wpf.PlanProperties
{
    /// <summary>
    /// Viewmodel controller for the validation warnings panel control.
    /// </summary>
    public sealed class PlanogramValidationWarningViewModel : ViewModelAttachedControlObject<PlanogramValidationWarningPanel>
    {
        #region Fields

        private readonly PlanogramView _plan;
        private readonly PlanItemSelection _planItemSelection;
        private Boolean _isSynchingSelection;

        private readonly BulkObservableCollection<ValidationWarningRow> _validationWarningRows = new BulkObservableCollection<ValidationWarningRow>();
        private ReadOnlyBulkObservableCollection<ValidationWarningRow> _validationWarningRowsRo;

        private readonly ObservableCollection<ValidationWarningRow> _validationWarningSelection = new ObservableCollection<ValidationWarningRow>();

        private Int32 _validationWarningResolvedCount;
        private Boolean _showFullDescriptions = false;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath ClearCorrectedValidationWarningsCommandProperty = WpfHelper.GetPropertyPath<PlanogramValidationWarningViewModel>(o => o.ClearResolvedValidationWarningsCommand);
        public static readonly PropertyPath ValidationWarningRowsProperty = WpfHelper.GetPropertyPath<PlanogramValidationWarningViewModel>(o => o.ValidationWarningRows);
        public static readonly PropertyPath ValidationWarningSelectionProperty = WpfHelper.GetPropertyPath<PlanogramValidationWarningViewModel>(o => o.ValidationWarningSelection);
        public static readonly PropertyPath ValidationWarningResolvedCountProperty = WpfHelper.GetPropertyPath<PlanogramValidationWarningViewModel>(o => o.ValidationWarningResolvedCount);
        public static readonly PropertyPath ShowFullDescriptionsProperty = WpfHelper.GetPropertyPath<PlanogramValidationWarningViewModel>(o => o.ShowFullDescriptions);
        //Commands
        public static readonly PropertyPath ClearResolvedValidationWarningsCommandProperty = WpfHelper.GetPropertyPath<PlanogramValidationWarningViewModel>(o => o.ClearResolvedValidationWarningsCommand);
        public static readonly PropertyPath ToggleDetailedDescriptionsCommandProperty = WpfHelper.GetPropertyPath<PlanogramValidationWarningViewModel>(o => o.ToggleDetailedDescriptionsCommand);
        #endregion

        #region Properties

        /// <summary>
        /// Returns the validation warnings to be displayed.
        /// </summary>
        public ReadOnlyBulkObservableCollection<ValidationWarningRow> ValidationWarningRows
        {
            get
            {
                if (_validationWarningRowsRo == null)
                {
                    _validationWarningRowsRo = new ReadOnlyBulkObservableCollection<ValidationWarningRow>(_validationWarningRows);
                }
                return _validationWarningRowsRo;
            }
        }

        /// <summary>
        /// Returns the selected warnings.
        /// </summary>
        public ObservableCollection<ValidationWarningRow> ValidationWarningSelection
        {
            get { return _validationWarningSelection; }
        }

        /// <summary>
        /// Returns a count of the number of warnings that have been resolved.
        /// </summary>
        public Int32 ValidationWarningResolvedCount
        {
            get
            {
                return this.ValidationWarningRows.Count(o => o.IsCorrected);
            }
        }

        /// <summary>
        /// Determines whethe the full or short descriptions should be displayed
        /// </summary>
        public Boolean ShowFullDescriptions
        {
            get
            {
                return _showFullDescriptions;
            }
            set
            {
                _showFullDescriptions = value;
                OnPropertyChanged(ShowFullDescriptionsProperty);

            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="plan">the plan view context</param>
        /// <param name="planItemSelection">the planogram selected items controller.</param>
        public PlanogramValidationWarningViewModel(PlanogramView plan, PlanItemSelection planItemSelection)
        {
            _plan = plan;
            _planItemSelection = planItemSelection;

            UpdateWarningRows();

            //subscribe events
            _plan.ValidationWarningsChanged += PlanOnValidationWarningsChanged;
            ValidationWarningSelection.CollectionChanged += ValidationWarningSelectionOnCollectionChanged;
            _planItemSelection.BulkCollectionChanged += PlanItemSelectionOnBulkCollectionChanged;
        }

        #endregion

        #region Commands

        #region CheckValidationWarnings

        private RelayCommand _clearResolvedValidationWarningsCommand;

        /// <summary>
        /// Clears all resolved warnings from the display.
        /// </summary>
        public RelayCommand ClearResolvedValidationWarningsCommand
        {
            get
            {
                if (_clearResolvedValidationWarningsCommand == null)
                {
                    _clearResolvedValidationWarningsCommand = new RelayCommand(
                        o => ClearResolvedValidationWarnings_Executed())
                        {
                            FriendlyName = Message.EditorValidationWarningPanel_ClearResolvedValidationWarnings,
                            SmallIcon = ImageResources.EditorValidation_ItemsRefresh
                        };

                    base.ViewModelCommands.Add(_clearResolvedValidationWarningsCommand);
                }

                return _clearResolvedValidationWarningsCommand;
            }
        }

        private void ClearResolvedValidationWarnings_Executed()
        {
            if (App.ViewState.EntityId != 0)
                ValidatePlanogramProductsAgainstMasterData();

            _plan.CheckValidationWarnings(/*resetList*/true);

            UpdateWarningRows();
        }

        private void ValidatePlanogramProductsAgainstMasterData()
        {
            Model.Entity entity = Model.Entity.FetchById(App.ViewState.EntityId);
            var validationEngine = new AttributeValidationEngine();
            validationEngine.ValidateProductAttributes(_plan.Model, entity);
        }

        #endregion


        #region Toggle Detailed Descriptions

        private RelayCommand _toggleDetailedDescriptionsCommand;

        /// <summary>
        /// Switches all child warning rows from detailed to summary descriptions
        /// </summary>
        public RelayCommand ToggleDetailedDescriptionsCommand
        {
            get
            {
                if (_toggleDetailedDescriptionsCommand == null)
                {
                    _toggleDetailedDescriptionsCommand = new RelayCommand(
                        o => ToggleDetailedDescriptions_Executed())
                    {
                        FriendlyName = Message.EditorValidationWarningPanel_ToggleDetailedDescriptions,
                        SmallIcon = ImageResources.EditorValidation_ToggleDetailedDescription
                    };

                    base.ViewModelCommands.Add(_toggleDetailedDescriptionsCommand);
                }

                return _toggleDetailedDescriptionsCommand;
            }
        }

        private void ToggleDetailedDescriptions_Executed()
        {
            this.ShowFullDescriptions = !this.ShowFullDescriptions;

            UpdateWarningRows();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Updates the warning rows collection.
        /// </summary>
        private void UpdateWarningRows()
        {
            _isSynchingSelection = true;

            if (_validationWarningRows.Count > 0) _validationWarningRows.Clear();

            if (_plan != null)
            {
                //Only show warnings specified by the user
                _validationWarningRows.AddRange(_plan.ValidationWarningRows.Where(w => w.ShowWarningToUser));

            }

            foreach (ValidationWarningRow row in _validationWarningRows)
            {
                row.Description = ShowFullDescriptions ? row.FullDescription : row.ShortDescription;
            }

            //force an update on the count
            OnPropertyChanged(ValidationWarningResolvedCountProperty);

            _isSynchingSelection = false;

            //reset the selected rows back.
            PlanItemSelectionOnBulkCollectionChanged(_planItemSelection,
                new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));
        }

        /// <summary>
        ///     Maps a list of warning rows to their corresponding plan items.
        /// </summary>
        /// <param name="rows">The list of <see cref="ValidationWarningRow"/> items to be mapped.</param>
        /// <returns>A new list of plan items that correspond to those in the source warning rows.</returns>
        public IEnumerable<IPlanItem> MapFromValidationWarningRows(IEnumerable<ValidationWarningRow> rows)
        {
            List<IPlanItem> planItems = new List<IPlanItem>();
            IList<ValidationWarningRow> warningRows = rows as IList<ValidationWarningRow> ?? rows.ToList();

            // Select any fixture component that is referred to in warnings.
            IEnumerable<ValidationWarningRow> componentWarnings = warningRows.Where(row => row.AssemblyComponent != null || row.FixtureComponent != null);

            planItems.AddRange(_plan.EnumerateAllComponents()
                .Where(view =>
                    componentWarnings.Any(row =>
                        (row.FixtureComponent != null && Object.Equals(row.FixtureComponent.Id, view.FixtureComponentModel.Id))
                        || (row.AssemblyComponent != null && Object.Equals(row.AssemblyComponent.Id, view.AssemblyComponentModel.Id)))));

            // Select any fixture assemblies that are referred to in warnings.
            IEnumerable<ValidationWarningRow> assemblyWarnings = warningRows.Where(row => row.FixtureAssembly != null);
            planItems.AddRange(_plan.EnumerateAllAssemblies()
                .Where(view => assemblyWarnings.Any(row => row.FixtureAssembly != null && Object.Equals(row.FixtureAssembly.Id, view.FixtureAssemblyModel.Id))));

            // Select any positions that are referred to in warnings.
            IEnumerable<ValidationWarningRow> positionWarnings = warningRows.Where(row => row.Position != null);
            planItems.AddRange(_plan.EnumerateAllPositions()
                .Where(view => positionWarnings.Any(row => row.Position != null && Object.Equals(row.Position.Id, view.Model.Id))));

            // Select any positions that are refered to by product attribute validation warnings.
            IEnumerable<ValidationWarningRow> productWarnings = warningRows.Where(row => row.ProductAttributeComparisonResult != null);
            planItems.AddRange(_plan.EnumerateAllPositions()
                .Where(view => productWarnings.Any(row => row.ProductAttributeComparisonResult != null && Object.Equals(row.ProductAttributeComparisonResult.ProductGtin, view.Product.Gtin))));

            return planItems;
        }

        /// <summary>
        ///     Maps a list of plan items to their corresponding warning rows.
        /// </summary>
        /// <param name="items">The list of <see cref="IPlanItem"/> items to be mapped.</param>
        /// <returns>A new list of warning rows that correspond to those in the source plan items.</returns>
        private IEnumerable<ValidationWarningRow> MapToValidationWarningRows(IEnumerable<IPlanItem> items)
        {
            List<ValidationWarningRow> warningRows = new List<ValidationWarningRow>();
            foreach (var planItem in items)
            {
                List<ValidationWarningRow> newWarningRows = new List<ValidationWarningRow>();
                switch (planItem.PlanItemType)
                {
                    case PlanItemType.Component:
                        newWarningRows = ValidationWarningRows.Where(
                            row =>
                                (row.FixtureComponent != null && Object.Equals(row.FixtureComponent.Id, planItem.Component.FixtureComponentModel.Id))
                                 || (row.AssemblyComponent != null && Object.Equals(row.AssemblyComponent.Id, planItem.Component.AssemblyComponentModel.Id)))
                                .ToList();
                        break;
                    case PlanItemType.Assembly:
                        newWarningRows = ValidationWarningRows.Where(
                            row => row.FixtureAssembly != null && Object.Equals(row.FixtureAssembly.Id, planItem.Assembly.FixtureAssemblyModel.Id)).ToList();
                        break;
                    case PlanItemType.Position:
                        newWarningRows = ValidationWarningRows.Where(row => row.Position != null && Object.Equals(row.Position.Id, planItem.Position.Model.Id)).ToList();
                        break;
                }
                if (newWarningRows.Any())
                {
                    warningRows.AddRange(newWarningRows);
                }
            }

            return warningRows;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the planogram warnings collection changes.
        /// </summary>
        private void PlanOnValidationWarningsChanged(object sender, EventArgs eventArgs)
        {
            UpdateWarningRows();
        }

        /// <summary>
        ///     Invoked whenever the plan item selection changes.
        /// </summary>
        private void PlanItemSelectionOnBulkCollectionChanged(Object sender, BulkCollectionChangedEventArgs e)
        {
            if (_isSynchingSelection) return;

            _isSynchingSelection = true;


            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var validationWarningRow in MapToValidationWarningRows(e.ChangedItems.Cast<IPlanItem>()))
                    {
                        ValidationWarningSelection.Add(validationWarningRow);
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (var validationWarningRow in MapToValidationWarningRows(e.ChangedItems.Cast<IPlanItem>()))
                    {
                        ValidationWarningSelection.Remove(validationWarningRow);
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    if (ValidationWarningSelection.Count > 0) ValidationWarningSelection.Clear();
                    foreach (var validationWarningRow in MapToValidationWarningRows(_planItemSelection))
                    {
                        ValidationWarningSelection.Add(validationWarningRow);
                    }
                    break;
            }


            _isSynchingSelection = false;
        }

        /// <summary>
        ///     Invoked whenever the warning rows selection changes.
        /// </summary>
        private void ValidationWarningSelectionOnCollectionChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            if (_isSynchingSelection) return;

            _isSynchingSelection = true;

            List<ValidationWarningRow> selectedValidationWarningRows;

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        selectedValidationWarningRows = ValidationWarningSelection.ToList();

                        //find any items not already in the collection
                        var newItems = e.NewItems.Cast<ValidationWarningRow>().Where(
                            x => !selectedValidationWarningRows.Any(
                                y => x.Position == y.Position &&
                                     x.WarningType == y.WarningType));
                        //add them
                        selectedValidationWarningRows.AddRange(newItems);

                        _planItemSelection.Clear();
                        _planItemSelection.AddRange(MapFromValidationWarningRows(selectedValidationWarningRows));
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        selectedValidationWarningRows = ValidationWarningSelection.ToList();
                        _planItemSelection.RemoveRange(MapFromValidationWarningRows(e.OldItems.Cast<ValidationWarningRow>())
                            .Except(MapFromValidationWarningRows(selectedValidationWarningRows)));
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        _planItemSelection.Clear();
                        _planItemSelection.AddRange(MapFromValidationWarningRows(ValidationWarningRows));
                    }
                    break;
            }

            _isSynchingSelection = false;
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (IsDisposed) return;

            if (disposing)
            {
                _plan.ValidationWarningsChanged -= PlanOnValidationWarningsChanged;
                ValidationWarningSelection.CollectionChanged -= ValidationWarningSelectionOnCollectionChanged;
                _planItemSelection.BulkCollectionChanged -= PlanItemSelectionOnBulkCollectionChanged;

                _validationWarningRows.Clear();
            }

            IsDisposed = true;
        }

        #endregion
    }
}