﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25420 : L.Hodson
//` Simplified to use only the basic viewport otherwise this falls over when too many bays are added.
// V8-27517 : A.Probyn
//  Added defensive code to UpdateSize so that invalid width cannot be caluclated causing exception.
// V8-26762 : L.Ineson
//  Positions are no longer displayed.
#endregion

#region Version History: (CCM 8.1.1)
// V8-30311 : N.Haywood
//  Removed NearPlaneDistance
#endregion

#region Version History: (CCM 8.3.0)
// V8-29527 : M.Shelley
//  Added support for multiple bay editing
// V8-32058 : A.Probyn
//  Updated so the visual responds to the backboard being visible or 
//  not, and zooming to fit height when it does.
// V8-32097 : M.Shelley
//  Allow the use of the Shift key to select a range of fixtures
//  Prevent the user from de-selecting all bays
#endregion

#endregion

using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Framework.Controls.Wpf.Rendering;
using Galleria.Framework.Planograms.Controls.Wpf.Helpers;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Rendering.Interfaces;
using HelixToolkit.Wpf;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanProperties
{
    /// <summary>
    /// Interaction logic for PlanogramPropertiesBayVisual.xaml
    /// </summary>
    public sealed partial class PlanogramPropertiesBayVisual : UserControl, IDisposable
    {
        #region Fields

        private ModelConstruct3D _modelVisual;

        #endregion

        #region Properties

        #region Fixture Property

        public static readonly DependencyProperty FixtureProperty =
            DependencyProperty.Register("Fixture", typeof(PlanogramFixtureView), typeof(PlanogramPropertiesBayVisual),
            new PropertyMetadata(null, OnFixturePropertyChanged));

        private static void OnFixturePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((PlanogramPropertiesBayVisual)obj).Render();
        }

        /// <summary>
        /// Gets/Sets the fixture to be displayed.
        /// </summary>
        public PlanogramFixtureView Fixture
        {
            get { return (PlanogramFixtureView)GetValue(FixtureProperty); }
            private set { SetValue(FixtureProperty, value); }
        }

        #endregion

        #region IsSelectedProperty

        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(Boolean), typeof(PlanogramPropertiesBayVisual),
            new PropertyMetadata(false, OnIsSelectedPropertyChanged));

        private static void OnIsSelectedPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PlanogramPropertiesBayVisual senderControl = (PlanogramPropertiesBayVisual)obj;

            if ((Boolean)e.NewValue)
            {
                senderControl.xBorder.Background = App.Current.Resources[Galleria.Framework.Controls.Wpf.ResourceKeys.Main_BrushSelected] as Brush;
                senderControl.OnSelected();
            }
            else
            {
                senderControl.xBorder.Background = Brushes.Transparent;
                senderControl.OnDeselected();
            }
        }

        /// <summary>
        /// Gets/Sets whether this is selected.
        /// </summary>
        public Boolean IsSelected
        {
            get { return (Boolean)GetValue(IsSelectedProperty); }
            set
            {
                SetValue(IsSelectedProperty, value);
            }
        }

        #endregion

        #endregion

        #region Events

        #region Selected

        public static readonly RoutedEvent SelectedEvent =
            EventManager.RegisterRoutedEvent("Selected", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PlanogramPropertiesBayVisual));

        public event RoutedEventHandler Selected
        {
            add { AddHandler(SelectedEvent, value); }
            remove { RemoveHandler(SelectedEvent, value); }
        }

        private void OnSelected()
        {
            //raise out the selected event
            RaiseEvent(new RoutedEventArgs(PlanogramPropertiesBayVisual.SelectedEvent));
        }

        #endregion

        #region De-selected

        public static readonly RoutedEvent DeselectedEvent =
            EventManager.RegisterRoutedEvent("Deselected", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PlanogramPropertiesBayVisual));

        public event RoutedEventHandler Deselected
        {
            add { AddHandler(DeselectedEvent, value); }
            remove { RemoveHandler(DeselectedEvent, value); }
        }

        private void OnDeselected()
        {
            //raise out the selected event
            RaiseEvent(new RoutedEventArgs(PlanogramPropertiesBayVisual.DeselectedEvent));
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fixture"></param>
        /// <param name="availableHeight"></param>
        public PlanogramPropertiesBayVisual(PlanogramFixtureView fixture, Double availableHeight)
        {
            this.SizeChanged += new SizeChangedEventHandler(PlanogramPropertiesBayVisual_SizeChanged);

            InitializeComponent();

            SetupViewer();

            this.Fixture = fixture;
            this.MaxHeight = availableHeight;

            UpdateSize();

            this.Loaded += new RoutedEventHandler(PlanogramPropertiesBayVisual_Loaded);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when the user clicks on this control
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            if (Keyboard.Modifiers == ModifierKeys.Control)
            {
                this.IsSelected = !this.IsSelected;
            }
            else
            {
                // Check if this is already set and if so, force a change event
                if (this.IsSelected)
                {
                    OnSelected();
                }
                this.IsSelected = true;
            }
        }

        /// <summary>
        /// Called on load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanogramPropertiesBayVisual_Loaded(object sender, RoutedEventArgs e)
        {
            ZoomToFitHeight(this.xViewer, _modelVisual, false);
        }

        /// <summary>
        /// Called whenever the size of this visual changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanogramPropertiesBayVisual_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ZoomToFitHeight(this.xViewer, _modelVisual, false);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Carries out initial setup actions for the viewport.
        /// </summary>
        private void SetupViewer()
        {
            if (this.xViewer == null)
            {
                ApplyTemplate();
            }

            Viewport3D mainViewport = this.xViewer;

            //Reduce the rendering edge mode for performance.
            RenderOptions.SetEdgeMode(mainViewport, EdgeMode.Aliased);


            Vector3D cameraUp = new Vector3D(0, 1, 0);
            Vector3D cameraLook = new Vector3D(0, 0, -244);

            mainViewport.Camera =
                new OrthographicCamera()
                {
                    UpDirection = cameraUp,
                    LookDirection = cameraLook,
                };

        }

        /// <summary>
        /// Renders the visual
        /// </summary>
        private void Render()
        {
            if (_modelVisual != null)
            {
                _modelVisual = null;
            }

            //clear the viewer children.
            if (this.xViewer != null)
            {
                foreach (Visual3D visual in this.xViewer.Children.ToList())
                {
                    if (visual is ModelConstruct3D)
                    {
                        this.xViewer.Children.Remove(visual);

                        //completely dispose of the model and its data.
                        ModelConstruct3D visualModel = (ModelConstruct3D)visual;
                        visualModel.Dispose();
                        visualModel.ModelData.Dispose();
                    }
                }
            }

            if (this.Fixture != null)
            {
                PlanogramFixtureView fixtureView = this.Fixture;
                PlanogramFixtureDesignView newModel = fixtureView.Planogram.FlattenedView.FlattenedFixtures.First(f => f.Fixture == fixtureView);

                if (newModel != null && this.xViewer != null)
                {
                    PlanRenderSettings renderSettings = new PlanRenderSettings();
                    renderSettings.ShowPositions = false; //V8-26762

                    ModelConstruct3D model = PlanDocumentHelper.CreateModel(newModel, renderSettings);
                    IModelConstruct3DData modelData = model.ModelData;

                    _modelVisual = model;
                    this.xViewer.Children.Add(_modelVisual);

                    CameraHelper.ZoomExtents((ProjectionCamera)this.xViewer.Camera, this.xViewer);
                }
            }
        }

        /// <summary>
        /// Updates the size of this control to be relative to the fixture dimesions.
        /// </summary>
        public void UpdateSize()
        {
            Double maxFixtureHeight = this.Fixture.Planogram.Fixtures.Max(f => f.Height);
            Double fixtureHeightRatio = this.MaxHeight / maxFixtureHeight;

            //If height is negative, set to 0 to stop 2 negative values (height & width) producing positive in multiplication.
            if (this.Fixture.Height >= 0)
            {
                this.Height = (this.Fixture.Height * fixtureHeightRatio) >= 0 ? (this.Fixture.Height * fixtureHeightRatio) : 0;

                Double hwRatio = this.Fixture.Width / this.Fixture.Height;

                //If height is negative, set to 0 to stop 2 negative values producing positive.
                if (this.Fixture.Height >= 0)
                {
                    this.Width = (this.Height * hwRatio) >= 0 ? (this.Height * hwRatio) : 0;
                }
            }
            else 
            {
                this.Height = 0; 
                this.Width = 0;
            }

            ZoomToFitHeight(this.xViewer, _modelVisual, false);
        }

        /// <summary>
        /// Zooms to fit the model by height.
        /// </summary>
        private static void ZoomToFitHeight(Viewport3D viewport, ModelConstruct3D modelVisual, Boolean doInflate = true)
        {
            if (viewport != null && viewport.Camera != null)
            {
                #region Perspective
                if (viewport.Camera is PerspectiveCamera)
                {
                    CameraHelper.ZoomExtents((ProjectionCamera)viewport.Camera, viewport);
                }
                #endregion

                #region Orthographic
                else if (viewport.Camera is OrthographicCamera)
                {
                    //zoom to fit all first
                    CameraHelper.ZoomExtents((ProjectionCamera)viewport.Camera, viewport);


                    //In order to perform the zoom by rectangle we must be able to invert the camera
                    // Sometimes however the model is so big that when fully zoomed to extent this is not possible.
                    // If this is the case then try zooming in a bit.
                    //Matrix3D matrixCamera = Viewport3DHelper.GetCameraTransform(viewport);
                    //if (!matrixCamera.HasInverse)
                    //{
                    //    Int32 tryCount = 0;
                    //    while (!matrixCamera.HasInverse && tryCount < 15)
                    //    {
                    //        cameraController.Zoom(-0.1);
                    //        matrixCamera = Viewport3DHelper.GetCameraTransform(viewport);
                    //        tryCount++;
                    //    }

                    //}

                    //get the bounds of the full model
                    Rect bounds = Rect.Empty;
                    if (modelVisual != null)
                    {
                        bounds.Union(ModelConstruct3DHelper.GetModel2DBounds(viewport, modelVisual));
                    }

                    if (bounds.IsEmpty || Double.IsNaN(bounds.Height))
                    {
                        return;
                    }



                    Double viewportXYRatio = viewport.ActualWidth / viewport.ActualHeight;

                    //if the height of the model is smaller than the width
                    // then adjust the width to the viewport ratio as we should always fit to height.
                    if (bounds.Height < bounds.Width)
                    {
                        bounds.Width = bounds.Height * viewportXYRatio;
                    }
                    //TODO -> What if height is smaller than width but when fitted width would be smaller then actualviewportwidth
                    // model would be to left...


                    //zoom
                    PlanCameraHelper.ZoomToFitBounds(bounds, viewport, viewport.Camera as ProjectionCamera, doInflate);
                }
                #endregion

            }
        }


        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            if (!_isDisposed)
            {
                this.Fixture = null;
                _isDisposed = true;

                GC.SuppressFinalize(this);
            }
        }

        #endregion
    }
}
