﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26148 : L.Ineson
//  Created.
#endregion

#endregion

using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanProperties
{
    /// <summary>
    /// Interaction logic for AnnotationPropertiesWindow.xaml
    /// </summary>
    public sealed partial class AnnotationPropertiesWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AnnotationPropertiesViewModel), typeof(AnnotationPropertiesWindow),
            new UIPropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AnnotationPropertiesWindow senderControl = (AnnotationPropertiesWindow)obj;

            if (e.OldValue != null)
            {
                AnnotationPropertiesViewModel oldModel = (AnnotationPropertiesViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                AnnotationPropertiesViewModel newModel = (AnnotationPropertiesViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }

        }


        /// <summary>
        /// Gets the viewmodel controller for this window.
        /// </summary>
        public AnnotationPropertiesViewModel ViewModel
        {
            get { return (AnnotationPropertiesViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion


        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="viewModel"></param>
        public AnnotationPropertiesWindow(AnnotationPropertiesViewModel viewModel)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            this.ViewModel = viewModel;

            this.Title = (viewModel.EditorContext.Items.Count() > 1) ?
                Message.AnnotationProperties_TitleMultiSelect : Message.AnnotationProperties_Title;

            this.Loaded += PlanItemPropertiesWindow_Loaded;
        }


        /// <summary>
        /// Carries out intial load actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanItemPropertiesWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= PlanItemPropertiesWindow_Loaded;


            Dispatcher.BeginInvoke(
                (Action)(() => Mouse.OverrideCursor = null), priority: DispatcherPriority.Background);
        }

        #endregion
 
        #region window close

        public Boolean IsClosing { get; private set; }

        /// <summary>
        /// Called when the window close cross has been pressed
        /// </summary>
        /// <param name="e"></param>
        protected override void OnCrossCloseRequested(System.ComponentModel.CancelEventArgs e)
        {
            IsClosing = true;
            this.ViewModel.CancelCommand.Execute();

            base.OnCrossCloseRequested(e);
        }

        /// <summary>
        /// Disposes of the viewmodel when the window has closed.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion
    }
}
