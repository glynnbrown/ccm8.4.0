﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26148 : L.Ineson
//  Created.
// V8-27888 : L.Luong
//  Added ReadOnly check for Inventory values
#endregion

#region Version History : CCM 801
// V8-28760 : A.Kuszyk
//  Amended SetProductImage to use session image location.
#endregion
#region Version History : CCM 802
// V8-29112 : I.George
// Removed code that checks if metricSpecialtype is RegularSalesUnit before setting ReadOnly property to false
#endregion
#region Version History : CCM 810
// V8-29533 : D.Pleasance
//  Amended OK_CanExecute so ProductView can be checked to see if its valid also. This is to allow check for duplicate Gtins
#endregion
#region Version History : CCM 811
//V8-30443 : A.Probyn
//  Set for all products selected
// V8-29406 : N.Haywood
//  Added file type validation for OpenFileDialog
// V8-30443 : A.Probyn
//  Updated ClearProductImage to clear multiple images at once
// V8-28688 : A.Probyn
//  Added new parameter to PositionPropertiesViewModel to work out if a product view is actually selected 
//  when a position view is loaded in order for the position properties to be visible too.
#endregion

#endregion

using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Rendering;
using Galleria.Framework.ViewModel;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanProperties
{
    /// <summary>
    /// ViewModel controller for PositionPropertiesWindow
    /// </summary>
    public sealed class PositionPropertiesViewModel : ViewModelAttachedControlObject<PositionPropertiesWindow>
    {
        #region Fields

        private Boolean? _dialogResult;

        private readonly PlanogramView _plan;
        private readonly PlanItemSelection _editorContext;

        private ModelConstruct3DData _positionPreviewData;
        private PlanRenderSettings _positionPreviewSettings;
        private Boolean _showProductImages;
        private Boolean _showPositionProperties;
        private Boolean _isReadOnly;
        private Boolean _isProductSelected;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath EditorContextProperty = WpfHelper.GetPropertyPath<PositionPropertiesViewModel>(p => p.EditorContext);
        public static readonly PropertyPath PositionContextProperty = WpfHelper.GetPropertyPath<PositionPropertiesViewModel>(p => p.PositionContext);
        public static readonly PropertyPath ProductContextProperty = WpfHelper.GetPropertyPath<PositionPropertiesViewModel>(p => p.ProductContext);
        public static readonly PropertyPath PositionPreviewDataProperty = WpfHelper.GetPropertyPath<PositionPropertiesViewModel>(p => p.PositionPreviewData);
        public static readonly PropertyPath ShowProductImagesProperty = WpfHelper.GetPropertyPath<PositionPropertiesViewModel>(p => p.ShowProductImages);
        public static readonly PropertyPath ShowPositionPropertiesProperty = WpfHelper.GetPropertyPath<PositionPropertiesViewModel>(p => p.ShowPositionProperties);
        public static readonly PropertyPath IsReadOnlyProperty = WpfHelper.GetPropertyPath<PositionPropertiesViewModel>(p => p.IsReadOnly);
        public static readonly PropertyPath IsProductSelectedProperty = WpfHelper.GetPropertyPath<PositionPropertiesViewModel>(p => p.IsProductSelected);
        //public static readonly PropertyPath ShowPositionPreviewImagesProperty = WpfHelper.GetPropertyPath<PositionPropertiesViewModel>(p => p.ShowPositionPreviewImages);
        //public static readonly PropertyPath ShowPositionPreviewShapesProperty = WpfHelper.GetPropertyPath<PositionPropertiesViewModel>(p => p.ShowPositionPreviewShapes);

        //Commands
        public static readonly PropertyPath OKCommandProperty = WpfHelper.GetPropertyPath<PositionPropertiesViewModel>(p => p.OKCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<PositionPropertiesViewModel>(p => p.CancelCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;

                if (this.AttachedControl != null
                    && !this.AttachedControl.IsClosing)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }


        /// <summary>
        /// Returns the editor context
        /// </summary>
        public PlanItemSelection EditorContext
        {
            get { return _editorContext; }
        }

        public PlanogramProductMultiView ProductContext
        {
            get{return _editorContext.ProductView;}
        }

        public PlanogramPositionMultiView PositionContext
        {
            get { return _editorContext.PositionView; }
        }

        /// <summary>
        /// Returns true if product images should be shown.
        /// </summary>
        public Boolean ShowProductImages
        {
            get { return _showProductImages; }
            private set
            {
                _showProductImages = value;
                OnPropertyChanged(ShowProductImagesProperty);
            }
        }

        /// <summary>
        /// Returns true if position properties should be shown
        /// </summary>
        public Boolean ShowPositionProperties
        {
            get { return _showPositionProperties; }
            private set
            {
                _showPositionProperties = value;
                OnPropertyChanged(ShowPositionPropertiesProperty);
            }
        }

        /// <summary>
        /// Gets the model construct data used to display a position preview.
        /// </summary>
        public ModelConstruct3DData PositionPreviewData
        {
            get { return _positionPreviewData; }
            private set
            {
                if (_positionPreviewData != null)
                {
                    _positionPreviewData.Dispose();
                }

                _positionPreviewData = value;
                OnPropertyChanged(PositionPreviewDataProperty);
            }
        }

        public Boolean IsReadOnly
        {
            get { return _isReadOnly; }
            set
            {
                _isReadOnly = value;

                OnPropertyChanged(IsReadOnlyProperty);
            }
        }

        public Boolean IsProductSelected
        {
            get { return _isProductSelected; }
        }

        ///// <summary>
        ///// Gets/Sets whether product images 
        ///// should be displayed in the position preview
        ///// </summary>
        //public Boolean ShowPositionPreviewImages
        //{
        //    get
        //    {
        //        if (_positionPreviewSettings != null)
        //        {
        //            return _positionPreviewSettings.ShowProductImages;
        //        }
        //        return false;
        //    }
        //    set
        //    {
        //        if (_positionPreviewSettings != null)
        //        {
        //            _positionPreviewSettings.ShowProductImages = value;
        //        }
        //        OnPropertyChanged(ShowPositionPreviewImagesProperty);
        //    }
        //}

        ///// <summary>
        ///// Gets/Sets whether product shapes 
        ///// should be displayed in the position preview
        ///// </summary>
        //public Boolean ShowPositionPreviewShapes
        //{
        //    get
        //    {
        //        if (_positionPreviewSettings != null)
        //        {
        //            return _positionPreviewSettings.ShowProductShapes;
        //        }
        //        return false;
        //    }
        //    set
        //    {
        //        if (_positionPreviewSettings != null)
        //        {
        //            _positionPreviewSettings.ShowProductShapes = value;
        //        }
        //        OnPropertyChanged(ShowPositionPreviewShapesProperty);
        //    }
        //}

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="plan">the parent planogram</param>
        /// <param name="selection">the </param>
        public PositionPropertiesViewModel(PlanogramView plan, PlanItemSelection selection, Boolean isProductSelected)
        {
            _plan = plan;
            _editorContext = selection;
            _isProductSelected = isProductSelected;

            if (_plan != null) 
            {
                _isReadOnly = true;
            }

            _editorContext.BulkCollectionChanged += EditorContext_BulkCollectionChanged;

            OnSelectionChanged();

            //Start a multi edit
            selection.IsLoggingUndoableActions = false;
            _plan.BeginUndoableAction(Message.PlanItemProperties_UndoableActionName);

        }

        #endregion

        #region Commands

        #region OK Command

        private RelayCommand _okCommand;

        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OK_Executed(),
                        p => OK_CanExecute())
                    {
                        FriendlyName = Message.Generic_OK,
                        InputGestureKey = Key.Enter
                    };
                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }

        private Boolean OK_CanExecute()
        {
            if ((_plan != null && !_plan.Model.IsValid) || (_editorContext.ProductView != null && !_editorContext.ProductView.IsValid))
            {
                return false;
            }           
            return true;
        }

        private void OK_Executed()
        {
            LocalHelper.UpdateFocusedBindingSource(this.AttachedControl);

            //complete the plan edit
            _plan.EndUndoableAction();
            _editorContext.IsLoggingUndoableActions = true;

            this.DialogResult = true;
        }

        #endregion

        #region Cancel Command

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels any changes made.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel,
                        InputGestureKey = Key.Escape
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            //cancel any changes
            _plan.CancelUndoableAction(/*undoLoggedChanges*/true);
            _editorContext.IsLoggingUndoableActions = true;

            this.DialogResult = false;
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenver the editor context items change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditorContext_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            OnSelectionChanged();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Called whenever the selection changes
        /// </summary>
        private void OnSelectionChanged()
        {
            PlanItemSelection selection = this.EditorContext;

            //Create a new position preview if required.
            ModelConstruct3DData posPreview = null;
            if (selection.Count == 1 && selection.HasPositions)
            {
                if (_positionPreviewSettings == null)
                {
                    _positionPreviewSettings = new PlanRenderSettings();
                    //LabelSetting label = LabelSetting.NewLabelSetting();
                    //label.Text = "<Product.Name>";
                    //settings.ProductLabel = new LabelSettingView(label);
                    _positionPreviewSettings.ShowPositionUnits = true;

                    //show images if enabled on the current view document:
                    _positionPreviewSettings.ShowProductImages = 
                        App.MainPageViewModel.ActivePlanController.SelectedPlanDocument.ShowProductImages;
                }

                posPreview = new PlanPosition3DData(selection[0].Position, _positionPreviewSettings);
            }
            this.PositionPreviewData = posPreview;


            //show the product images if only 1 position or product is selected.
            this.ShowProductImages =
                (ProductContext != null 
                && selection.Count == 1
                && (selection.HasPositions || ProductContext.Items.Count() == 1));

            //show and default view to position info if positions are selected.
            this.ShowPositionProperties =
                (selection.Count > 0 && selection.HasPositions);
        }

        /// <summary>
        /// Launches the window to select a product image.
        /// </summary>
        /// <param name="propertyName"></param>
        public void SetProductImage(String propertyName)
        {
            if (_plan.Images != null)
            {
                if (ProductContext.Items.Any())
                {
                    PropertyInfo pInfo = typeof(PlanogramProductView).GetProperty(propertyName);
                    if (pInfo != null && pInfo.CanWrite)
                    {
                        PlanogramImage selectedImage = pInfo.GetValue(ProductContext.Items.First(), null) as PlanogramImage;

                        PlanogramImage img = null;
                        if (this.AttachedControl != null)
                        {
                            //Show the open file dialog.
                            OpenFileDialog dlg = new OpenFileDialog();
                            if (Directory.Exists(App.ViewState.SessionImageLocation))
                            {
                                dlg.InitialDirectory = App.ViewState.SessionImageLocation;
                            }
                            dlg.Filter = Message.PlanItemProperties_SelectProductImageFilter;
                            dlg.Multiselect = false;
                            dlg.CheckFileExists = true;

                            if (dlg.ShowDialog(this.AttachedControl) == true)
                            {
                                List<String> extensions = new List<String>(new String[]{".png", ".bmp", ".jpg", ".jpeg"});

                                if (!extensions.Any(s => String.Equals(Path.GetExtension(dlg.FileName), s, StringComparison.OrdinalIgnoreCase)))
                                {
                                    ModalMessage msg = new ModalMessage
                                        {
                                            Title = Message.General_WrongFileType_Title,
                                            Header = Message.General_WrongFileType_Title,
                                            MessageIcon = ImageResources.Warning_32,
                                            Description = Message.General_WrongFileType_Description,
                                            ButtonCount = 1,
                                            Button1Content = Message.Generic_OK,
                                            DefaultButton = ModalMessageButton.Button1
                                        };
                                    App.ShowWindow(msg, true);
                                }
                                else
                                {
                                    String fileName = dlg.FileName;
                                    img = _plan.AddPlanogramImage(File.ReadAllBytes(fileName), fileName, System.IO.Path.GetFileNameWithoutExtension(fileName));
                                    App.ViewState.SessionImageLocation = Path.GetDirectoryName(fileName);
                                }
                            }
                        }

                        if (img != null)
                        {
                            //V8-30443 : Set for all products selected
                            foreach(PlanogramProductView product in ProductContext.Items)
                            {
                                pInfo.SetValue(product, img, null);
                            }
                        }

                    }
                }

            }
        }

        /// <summary>
        /// Clears the value of the given image property
        /// </summary>
        /// <param name="propertyName"></param>
        public void ClearProductImage(String propertyName)
        {
            if (ProductContext.Items.Any())
            {
                foreach (PlanogramProductView product in ProductContext.Items)
                {
                    PropertyInfo pInfo = typeof(PlanogramProductView).GetProperty(propertyName);
                    if (pInfo != null && pInfo.CanWrite)
                    {
                        pInfo.SetValue(product, null, null);
                    }                    
                }
            }
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                _editorContext.BulkCollectionChanged -= EditorContext_BulkCollectionChanged;

                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
