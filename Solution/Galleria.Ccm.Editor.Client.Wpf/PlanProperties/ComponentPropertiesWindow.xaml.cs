﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26148 : L.Ineson
//  Created.
#endregion

#endregion

using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanProperties
{
    /// <summary>
    /// Interaction logic for ComponentPropertiesWindow.xaml
    /// </summary>
    public sealed partial class ComponentPropertiesWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ComponentPropertiesViewModel), typeof(ComponentPropertiesWindow),
            new UIPropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ComponentPropertiesWindow senderControl = (ComponentPropertiesWindow)obj;

            if (e.OldValue != null)
            {
                ComponentPropertiesViewModel oldModel = (ComponentPropertiesViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                ComponentPropertiesViewModel newModel = (ComponentPropertiesViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }


        /// <summary>
        /// Gets the viewmodel controller for this window.
        /// </summary>
        public ComponentPropertiesViewModel ViewModel
        {
            get { return (ComponentPropertiesViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="viewModel"></param>
        public ComponentPropertiesWindow(ComponentPropertiesViewModel viewModel)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //set the help file key for this window.
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.ComponentProperties);

            this.ViewModel = viewModel;

            if (this.ViewModel.ComponentContext == null)
            {
                this.Title = (viewModel.EditorContext.Count > 1) ?
                    Message.AssemblyProperties_TitleMultiSelect : Message.AssemblyProperties_Title;
            }
            else
            {
                this.Title = (viewModel.EditorContext.Count > 1) ?
                    Message.ComponentProperties_TitleMultiSelect : Message.ComponentProperties_Title;
            }

            this.Loaded += ComponentPropertiesWindow_Loaded;
        }


        /// <summary>
        /// Carries out intial load actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComponentPropertiesWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ComponentPropertiesWindow_Loaded;


            if (this.ViewModel.ComponentContext != null)
            {
                this.ViewComponentGeneral.IsChecked = true;
            }
            else
            {
                this.ViewAssemblyDetails.IsChecked = true;
            }


            Dispatcher.BeginInvoke(
                (Action)(() => Mouse.OverrideCursor = null), priority: DispatcherPriority.Background);
        }

        #endregion
 
        #region window close

        public Boolean IsClosing { get; private set; }

        /// <summary>
        /// Called when the window close cross has been pressed
        /// </summary>
        /// <param name="e"></param>
        protected override void OnCrossCloseRequested(System.ComponentModel.CancelEventArgs e)
        {
            IsClosing = true;
            this.ViewModel.CancelCommand.Execute();

            base.OnCrossCloseRequested(e);
        }

        /// <summary>
        /// Disposes of the viewmodel when the window has closed.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion
    }


}
