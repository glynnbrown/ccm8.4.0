﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26148 : L.Ineson
//  Created.
// V8-25496 : L.Ineson
//  Corrected preview zooming.
#endregion
#region Version History: (CCM 8.1.1)
// V8-28688 : A.Probyn
//  Updated PlanItemPropertiesWindow_Loaded to select the product tab if required
#endregion
#region Version History : CCM 820
// V8-30791 : D.Pleasance
//  Added Handler for ColorPickerBox.SelectedColorChangedEvent, to track user colors
#endregion
#region Version History : CCM 830
// V8-25498 : L.Ineson
//  Added call to lighting helper for the position preview viewport.
#endregion
#endregion

using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Controls.Wpf.Rendering;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Controls.Wpf.Helpers;
using Galleria.Framework.Rendering;
using HelixToolkit.Wpf;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Windows.Threading;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanProperties
{
    /// <summary>
    /// Interaction logic for PositionPropertiesWindow.xaml
    /// </summary>
    public sealed partial class PositionPropertiesWindow : ExtendedRibbonWindow
    {
        #region Fields

        private ModelConstruct3D _positionPreviewModel;

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PositionPropertiesViewModel), typeof(PositionPropertiesWindow),
            new UIPropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PositionPropertiesWindow senderControl = (PositionPropertiesWindow)obj;

            if (e.OldValue != null)
            {
                PositionPropertiesViewModel oldModel = (PositionPropertiesViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                PositionPropertiesViewModel newModel = (PositionPropertiesViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }

            senderControl.UpdatePositionPreview();
        }



        /// <summary>
        /// Gets the viewmodel controller for this window.
        /// </summary>
        public PositionPropertiesViewModel ViewModel
        {
            get { return (PositionPropertiesViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="viewModel"></param>
        public PositionPropertiesWindow(PositionPropertiesViewModel viewModel)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            this.ViewModel = viewModel;

            if (viewModel.ShowPositionProperties)
            {
                this.Title = (viewModel.EditorContext.Count > 1) ?
                    Message.PositionProperties_TitleMultiSelect : Message.PositionProperties_Title;

                //set the help file key for this window.
                HelpFileKeys.SetHelpFile(this, HelpFileKeys.PositionProperties);
            }
            else
            {
                this.Title = (viewModel.EditorContext.Count > 1) ?
                    Message.PositionProperties_ProductTitleMultiSelect : Message.PositionProperties_ProductTitle;

                //set the help file key for this window.
                HelpFileKeys.SetHelpFile(this, HelpFileKeys.ProductProperties);
            }


            AddEventHandlers();

            this.Loaded += PlanItemPropertiesWindow_Loaded;
        }

        /// <summary>
        /// Carries out intial load actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanItemPropertiesWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= PlanItemPropertiesWindow_Loaded;


            if (this.ViewModel.ShowPositionProperties && !this.ViewModel.IsProductSelected)
            {
                this.ViewPositionGeneral.IsChecked = true;
            }
            else
            {
                this.ViewProductDetails.IsChecked = true;
            }

            Dispatcher.BeginInvoke(
                (Action)(() => Mouse.OverrideCursor = null), priority: DispatcherPriority.Background);
        }

        #endregion

        #region Event Handlers

        private void AddEventHandlers()
        {
            this.AddHandler(PlanItemPropertiesProductImage.EditRequestedEvent, (RoutedEventHandler)OnProductImageEditRequested);
            this.AddHandler(PlanItemPropertiesProductImage.ClearRequestedEvent, (RoutedEventHandler)OnProductImageClearRequested);
            this.AddHandler(ColorPickerBox.SelectedColorChangedEvent, (RoutedEventHandler)OnSelectedColorChanged);
        }

        /// <summary>
        /// Called when the position viewer initially loads
        /// </summary>
        private void xPositionPreviewViewer_Loaded(object sender, RoutedEventArgs e)
        {
            this.xPositionPreviewViewer.Loaded -= xPositionPreviewViewer_Loaded;

            PlanLightingHelper.SetupDefaultLighting(this.xPositionPreviewViewer, CameraViewType.Perspective);
            PlanCameraHelper.SetupForCameraType(this.xPositionPreviewViewer, CameraViewType.Perspective);
        }

        /// <summary>
        /// Called whenever the size of the position viewer changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xPositionPreviewViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            PlanCameraHelper.ZoomToFit(this.xPositionPreviewViewer, _positionPreviewModel);
        }

        /// <summary>
        /// Called when the user requests to edit a product image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnProductImageEditRequested(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel != null)
            {
                PlanItemPropertiesProductImage senderControl = (PlanItemPropertiesProductImage)e.OriginalSource;
                this.ViewModel.SetProductImage(senderControl.ImageName);
            }
        }

        /// <summary>
        /// Called when the user requests to clear a product image.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnProductImageClearRequested(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel != null)
            {
                PlanItemPropertiesProductImage senderControl = (PlanItemPropertiesProductImage)e.OriginalSource;
                this.ViewModel.ClearProductImage(senderControl.ImageName);
            }
        }

        /// <summary>
        /// Called whenever the preview view is selected.
        /// </summary>
        private void ViewPositionPreview_Checked(object sender, RoutedEventArgs e)
        {
            if (_positionPreviewModel == null)
            {
                UpdatePositionPreview();
            }
            else
            {
                PlanCameraHelper.ZoomToFit(this.xPositionPreviewViewer, _positionPreviewModel);
            }
        }

        /// <summary>
        /// tracks color changes to keep recent user colors
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSelectedColorChanged(object sender, RoutedEventArgs e)
        {
            var colorPicker = e.OriginalSource as ColorPickerBox;
            if (colorPicker != null)
            {
                App.ViewState.ApplyRecentColor(colorPicker.SelectedColor);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the position preview.
        /// </summary>
        private void UpdatePositionPreview()
        {
            HelixViewport3D viewer = this.xPositionPreviewViewer;

            if (viewer != null && viewer.IsLoaded)
            {
                //clear out any existing children.
                foreach (Visual3D visual in viewer.Children.ToList())
                {
                    if (visual is ModelConstruct3D)
                    {
                        viewer.Children.Remove(visual);
                    }
                }
                _positionPreviewModel = null;


                //add the model.
                if (this.ViewModel != null && this.ViewModel.PositionPreviewData != null)
                {
                    ModelConstruct3DData modelData = this.ViewModel.PositionPreviewData;

                    _positionPreviewModel = new ModelConstruct3D(modelData);

                    modelData.Position = new PointValue(-modelData.Size.Width / 2, -modelData.Size.Height / 2, -modelData.Size.Depth / 2);

                    viewer.Children.Add(_positionPreviewModel);
                    PlanCameraHelper.ZoomToFit(viewer, _positionPreviewModel);
                }
            }

        }

        #endregion

        #region window close

        public Boolean IsClosing { get; private set; }

        /// <summary>
        /// Called when the window close cross has been pressed
        /// </summary>
        /// <param name="e"></param>
        protected override void OnCrossCloseRequested(System.ComponentModel.CancelEventArgs e)
        {
            IsClosing = true;
            this.ViewModel.CancelCommand.Execute();

            base.OnCrossCloseRequested(e);
        }

        /// <summary>
        /// Disposes of the viewmodel when the window has closed.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion

       

    }

}
