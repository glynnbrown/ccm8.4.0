﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.0)

// V8-24953 : L.Hodson ~ Created.
// CCM-25880 : N.Haywood
//  Added SelectMerchGroupCommand
// V8-26338 : A.Kuszyk
//  Added Performance Metric commands and properties.
// V8-26805 : A.Kuszyk
//  Adjusted metric cancel command to revert properties rather than re-inserting copy.
// V8-27624 : A.Silva
//      Added Apply Renumbering Strategy command.
// V8-27553 : L.Ineson
//  Moved add fixture methods to plan view and backboard base methods to fixture view.
// V8-27885 : L.Luong
//  Added Metric ChildChanged
// V8-26850 : A.Probyn
//  Added UpdatePlanogramBounds to DeleteFixture command executed method.
// V8-28444 : M.Shelley
//  Added a change to allow "NewMetric_Executed" to run under a unit test environment
// V8-28551 : L.Ineson
//  Made sure that all event handlers are correctly unsubscribed.

#endregion

#region Version History: (CCM 8.0.1)

// CCM-25334 : D.Pleasance
//  Amended so that changing the fixture depth should change the base depth. 

#endregion

#region Version History: (CCM 8.0.2)

// CCM-25995 : M.Pettit
//  user now has an option to snap all components to nearest notch if they edit a notch value or turn use notches on
// V8-29004 : L.Luong
//  Added MoveFixtureLeft and MoveFixtureRight commands 
//  Removed ApplyRenumberingStrategy command

#endregion

#region Version History: (CCM 8.1.0)

//  V8-29662 : M.Brumby
//      RenumberBays when exiting if the RenumberingStrategy has changed
// V8-29466 : M.Pettit
//  Notch value check is now only displayed to the user if the fixture contains merchandisable components
// V8-30093 : M.Shelley
//  When a new planogram is created, the planogram dimensions are automatically set to the size of the fixtures
// V8-29718 : M.Pettit
//  Edit Metric, View Metric windows were lost behind other windows if user moved focus to another application

#endregion

#region Version History: (CCM 8.1.1)

//  V8-30464 : L.Luong
//      Added Path to get the Directory Path of Planogram
//  V8-29290 : I.George
//      Added warning if cluster doesn't match location code or clusterscheme
// V8-28911 : A.Silva
//  Added RemoveMetricExecuted so that the Planogram's PerformanceData is updated (setting to null the removed metric value for all items).

#endregion

#region Version History: (CCM 8.2.0)
// V8-30687 : L.Ineson
//  Updated constructor to start undo action earlier.
// V8-30759 : J.Pickup
//  Inventory Profile Type updates when removing/editing a metric that isn't the only promotional one.
//  enables the combobox to be disabled. 
// V8-30151 : M.Shelley
//  Changed the properties panel to use the merchandising group selector window
//  from the project Galleria.Ccm.Common.Wpf.Selectors
#endregion

#region Version History: (CCM 8.3.0)
// V8-31833 : L.Ineson
//  Added DuplicateBayLeavingWhiteSpace and DuplicateBayAndAutofill commands.
// V8-29527 : M.Shelley
//  Added support for multiple bay editing
// V8-32143 : A.Silva
//  Amended move bay right and left code.
// V8-32055 : N.Haywood
//  Fixed AddFixtureAfter_Executed to work when there's no fixtures. 
// V8-32059 : N.Haywood
//  Adding fixtures now calls AddFixture method
// V8-32261 : L.Ineson
//  Duplicate leave space is now disabled when fill wide is on.
// V8-32259 : M.Shelley
//  Fixed issues that occurred when a bay is duplicated using the right-click context menu and the
//  newly created bay was showing as "No bay selected" when selected
// V8-32058 : A.Probyn
//  Updated so the visual responds to the backboard being visible or 
//  not, and zooming to fit height when it does.
// V8-32513 : M.Brumby
//  fix for successive bay split
// V8-32736 : L.Ineson
//  Duplicate bay commands now also update bay sequencing if auto renumbering is off.
// V8-32761 : L.Ineson
//  Duplicte bay and leave space no longer combines
// V8-32905 : M.Brumby
//  Fix for empty new bays
#endregion

#endregion

using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Editor.Client.Wpf.Settings;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Enums;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanProperties
{
    /// <summary>
    /// Viewmodel controller for PlanogramPropertiesWindow
    /// </summary>
    /// <remarks>
    /// This is a screen which shows the property information for a given planogram and allow the user
    /// to manage fixtures.
    /// </remarks>
    public sealed class PlanogramPropertiesViewModel : ViewModelAttachedControlObject<PlanogramPropertiesWindow>
    {
        #region Fields

        private Boolean? _dialogResult;
        private PlanogramPerformanceMetricWindow _metricWindow;
        private readonly UserEditorSettingsViewModel _appSettingsView;
        private PlanogramView _planogram;

        private readonly BulkObservableCollection<PlanogramFixtureView> _fixtures = new BulkObservableCollection<PlanogramFixtureView>();
        private ReadOnlyBulkObservableCollection<PlanogramFixtureView> _fixturesRO;
        private PlanogramFixtureView _selectedFixture;
        private PlanogramPerformanceMetric _selectedPerformanceMetric;

        private Boolean _isBackboardRequired;
        private PlanogramComponentView _backboardComponent;
        private Boolean _isBaseRequired;
        private PlanogramComponentView _baseComponent;

        private Boolean _copyAllComponents = false;

        private DisplayUnitOfMeasureCollection _displayUnits;
        private Boolean _isMetricRegularSalesUnit;

        private Boolean _displaySnapToNotchWarningOnClose = false;

        private LocationPlanAssignmentList _planAssignments;
        private DateTime? _dateLive;
        private DateTime? _dateCommunicated;

        private String _path;

        //locations
        private ObservableCollection<LocationInfo> _selectedLocations = new ObservableCollection<LocationInfo>();
        ClusterSchemeInfoListViewModel _clusterSchemeInfoListViewModel = new ClusterSchemeInfoListViewModel();
        private Boolean _isShowWarning;
        private ClusterScheme _selectedClusterScheme;
        private String _tooltipWarningMessage;

        private BulkObservableCollection<PlanogramPropertiesBayVisual> _selectedBayViews = new BulkObservableCollection<PlanogramPropertiesBayVisual>();
        private BulkObservableCollection<PlanogramPropertiesBayVisual> _bayViews = new BulkObservableCollection<PlanogramPropertiesBayVisual>();
        
        private PlanogramFixturePropertiesMultiView _multiFixtureView;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath PlanogramProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.Planogram);
        public static readonly PropertyPath DisplayUnitsProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.DisplayUnits);
        public static readonly PropertyPath FixturesProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.Fixtures);
        public static readonly PropertyPath SelectedFixtureProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.SelectedFixture);
        //public static readonly PropertyPath IsBackboardRequiredProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.IsBackboardRequired);
        //public static readonly PropertyPath BackboardProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.Backboard);
        public static readonly PropertyPath IsBaseRequiredProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.IsBaseRequired);
        public static readonly PropertyPath BaseProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.Base);
        public static readonly PropertyPath SelectedPerformanceMetricProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.SelectedPerformanceMetric);
        public static readonly PropertyPath PerformanceMetricsProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.PerformanceMetrics);
        public static readonly PropertyPath IsMetricRegularSalesUnitProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.IsMetricRegularSalesUnit);
        public static readonly PropertyPath IsRepositoryConnectedProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.IsRepositoryConnected);
        public static readonly PropertyPath DateLiveProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.DateLive);
        public static readonly PropertyPath DateCommunicatedProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.DateCommunicated);
        public static readonly PropertyPath IsShowWarningProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.IsShowWarning);
        public static readonly PropertyPath TooltipWarningMessageProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.TooltipWarningMessage);
        public static readonly PropertyPath PlanogramContainsPromotionalPerformanceMetricProperty = 
                WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.PlanogramContainsPromotionalPerformanceMetric);

        public static readonly PropertyPath SelectedBayViewsProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.SelectedBayViews);
        //public static readonly PropertyPath SelectedBayViewsCountProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.SelectedBayViewsCount);

        public static readonly PropertyPath MultiFixtureViewProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.MultiFixtureView);

        //Commands
        public static readonly PropertyPath AddFixtureAfterCommandProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.AddFixtureAfterCommand);
        public static readonly PropertyPath AddFixtureBeforeCommandProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.AddFixtureBeforeCommand);
        public static readonly PropertyPath DeleteFixtureCommandProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.DeleteFixtureCommand);
        public static readonly PropertyPath OKCommandProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.OKCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath SelectMerchGroupCommandProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.SelectMerchGroupCommand);
        public static readonly PropertyPath NewMetricCommandProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.NewMetricCommand);
        public static readonly PropertyPath ViewMetricCommandProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.ViewMetricCommand);
        public static readonly PropertyPath RemoveMetricCommandProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.RemoveMetricCommand);
        public static readonly PropertyPath MetricSaveCommandProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.MetricSaveCommand);
        public static readonly PropertyPath MetricCancelCommandProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.MetricCancelCommand);
        public static readonly PropertyPath MoveFixtureLeftProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.MoveFixtureLeftCommand);
        public static readonly PropertyPath MoveFixtureRightProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.MoveFixtureRightCommand);
        public static readonly PropertyPath SelectLocationCommandProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.SelectLocationCommand);
        public static readonly PropertyPath SelectClusterSchemeCommandProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.SelectClusterSchemeCommand);
        public static readonly PropertyPath SelectClusterCommandProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.SelectClusterCommand);
        public static readonly PropertyPath DuplicateBayLeavingWhiteSpaceCommandProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.DuplicateBayLeavingWhiteSpaceCommand);
        public static readonly PropertyPath DuplicteBayAndAutofillCommandProperty = WpfHelper.GetPropertyPath<PlanogramPropertiesViewModel>(p => p.DuplicteBayAndAutofillCommand);
        
        #endregion

        #region Properties

        /// <summary>
        /// Returns the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;

                if (this.AttachedControl != null
                    && !this.AttachedControl.IsClosing)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }

        /// <summary>
        /// Returns the planogram model
        /// </summary>
        public PlanogramView Planogram
        {
            get { return _planogram; }
        }

        /// <summary>
        /// Gets/Sets the file path or repository folder path name for the planogram.
        /// </summary>
        public String Path
        {
            get { return _path; }
        }

        /// <summary>
        /// Returns the display unit of measure to use
        /// </summary>
        public DisplayUnitOfMeasureCollection DisplayUnits
        {
            get { return _displayUnits; }
            private set
            {
                _displayUnits = value;
                OnPropertyChanged(DisplayUnitsProperty);
            }
        }

        /// <summary>
        /// Returns a readonly collection of fixtures in the planogram
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramFixtureView> Fixtures
        {
            get
            {
                if (_fixturesRO == null)
                {
                    _fixturesRO = new ReadOnlyBulkObservableCollection<PlanogramFixtureView>(_fixtures);
                }
                return _fixturesRO;
            }
        }

        public BulkObservableCollection<PlanogramPropertiesBayVisual> SelectedBayViews
        {
            get
            {
                if (_selectedBayViews == null)
                {
                    _selectedBayViews = new BulkObservableCollection<PlanogramPropertiesBayVisual>();
                }

                return _selectedBayViews;
            }
        }

        public BulkObservableCollection<PlanogramPropertiesBayVisual> BayViews
        {
            get
            {
                if (_bayViews == null)
                {
                    _bayViews = new BulkObservableCollection<PlanogramPropertiesBayVisual>();
                }

                return _bayViews;
            }
        }

        public PlanogramFixturePropertiesMultiView MultiFixtureView
        {
            get
            {
                return _multiFixtureView;
            }
        }

        /// <summary>
        /// Gets/Sets the currently selected fixture.
        /// </summary>
        public PlanogramFixtureView SelectedFixture
        {
            get { return _selectedFixture; }
            set
            {
                if (_selectedFixture == value) return;

                //before changing the fixture update any lost focus binding sources
                LocalHelper.UpdateFocusedBindingSource(this.AttachedControl);

                _selectedFixture = value;
                OnPropertyChanged(SelectedFixtureProperty);

                OnSelectedFixtureChanged(value);
            }
        }

        /// <summary>
        /// The currently selected Performance Metrics
        /// </summary>
        public PlanogramPerformanceMetric SelectedPerformanceMetric
        {
            get { return _selectedPerformanceMetric; }
            set
            {
                _selectedPerformanceMetric = value;
                OnPropertyChanged(SelectedPerformanceMetricProperty);
            }
        }

        /// <summary>
        /// The Performance Metrics availble on this Planogram
        /// </summary>
        public PlanogramPerformanceMetricList PerformanceMetrics
        {
            get { return Planogram.PerformanceMetrics; }
        }

        /// <summary>
        /// Gets/Sets whether the selected fixture should have a backboard.
        /// </summary>
        //public Boolean IsBackboardRequired
        //{
        //    get { return _isBackboardRequired; }
        //    set
        //    {
        //        _isBackboardRequired = value;
        //        OnPropertyChanged(IsBackboardRequiredProperty);

        //        OnIsBackboardRequiredChanged(value);
        //    }
        //}

        /// <summary>
        /// Returns the backboard component for the selected fixture
        /// </summary>
        //public PlanogramComponentView Backboard
        //{
        //    get { return _backboardComponent; }
        //    private set
        //    {
        //        PlanogramComponentView oldValue = _backboardComponent;

        //        _backboardComponent = value;
        //        OnPropertyChanged(BackboardProperty);

        //        //update backboard required property
        //        _isBackboardRequired = (value != null);
        //        OnPropertyChanged(IsBackboardRequiredProperty);
        //    }
        //}

        /// <summary>
        /// Gets/Sets whether the selected fixture should have a base.
        /// </summary>
        public Boolean IsBaseRequired
        {
            get { return _isBaseRequired; }
            set
            {
                _isBaseRequired = value;
                OnPropertyChanged(IsBaseRequiredProperty);

                OnIsBaseRequiredChanged(value);
            }
        }

        /// <summary>
        /// Gets the base component for the selected fixture
        /// </summary>
        public PlanogramComponentView Base
        {
            get { return _baseComponent; }
            private set
            {
                PlanogramComponentView oldValue = _baseComponent;

                _baseComponent = value;
                OnPropertyChanged(BaseProperty);

                //update base required property
                _isBaseRequired = (value != null);
                OnPropertyChanged(IsBaseRequiredProperty);
            }
        }

        private Boolean PlanogramPerformanceIsValid
        {
            get
            {
                if (Planogram == null || Planogram.Model == null || Planogram.Model.Performance == null) return false;
                return Planogram.Model.Performance.IsValid && Planogram.Model.Performance.Metrics.IsValid;
            }
        }

        public Boolean IsMetricRegularSalesUnit
        {
            get { return _isMetricRegularSalesUnit; }
            set
            {
                _isMetricRegularSalesUnit = value;

                OnPropertyChanged(IsMetricRegularSalesUnitProperty);
            }
        }

        // Check if the Editor is connected to a repository
        public Boolean IsRepositoryConnected
        {
            get { return App.ViewState.IsConnectedToRepository; }
        }

        ///<summary>
        ///Plan assignment DateLive field
        ///</summary> 
        public DateTime? DateLive
        {
            get
            {
                FetchLocationPlanAssignments();
                return _dateLive;
            }
            set
            {
                _dateLive = value;
                OnPropertyChanged(DateLiveProperty);
            }
        }

        /// <summary>
        /// Plan assignment DateCommunicated field
        /// </summary>
        public DateTime? DateCommunicated
        {
            get
            {
                FetchLocationPlanAssignments();
                return _dateCommunicated;
            }
            set
            {
                _dateCommunicated = value;
                OnPropertyChanged(DateCommunicatedProperty);
            }
        }
        /// <summary>
        /// Gets/Sets if the Cluster has changed
        /// </summary>
        public Boolean IsShowWarning
        {
            get { return _isShowWarning; }
            set
            {
                _isShowWarning = value;
                OnPropertyChanged(IsShowWarningProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the message that should be displayed on the tooltip
        /// </summary>
        public String TooltipWarningMessage
        {
            get { return _tooltipWarningMessage; }
            set
            {
                _tooltipWarningMessage = value;
                OnPropertyChanged(TooltipWarningMessageProperty);
            }
        }

        #region Helper Property

        /// <summary>
        /// Returns true if planogram model contains a perfofrmance metric of type 'PromotionalSalesUnits'.
        /// </summary>
        public Boolean PlanogramContainsPromotionalPerformanceMetric
        {
            get
            {
                return Planogram.Model.Performance.Metrics.Any(pm => pm.SpecialType == MetricSpecialType.PromotionalSalesUnits);
            }
        }

        #endregion


        #endregion

        #region Events

        public event EventHandler<EventArgs<PlanogramFixtureView>> FixtureSizeChanged;

        private void OnFixtureSizeChanged(PlanogramFixtureView fixture)
        {
            _planogram.UpdatePlanogramBounds();

            if (FixtureSizeChanged != null)
            {
                FixtureSizeChanged(this, new EventArgs<PlanogramFixtureView>(fixture));
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="planogram">the current planogram</param>
        /// <remarks>Uses App.ViewState.Settings</remarks>
        public PlanogramPropertiesViewModel(PlanogramView planogram)
        {
            _appSettingsView = App.ViewState.Settings;
            _planogram = planogram;

            //start a new edit on the plan before we do anythign to it.
            planogram.BeginUndoableAction(Message.PlanogramProperties_UndoableActionName);


            _displayUnits = DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(planogram.Model);

            _path = planogram.ParentPackageView.GetLocationPath();

            // V8-30093 : Update the planogram size from the fixtures to prevent a new planogram having dimensions of 0
            planogram.Model.UpdateSizeFromFixtures();

            //update the regular sales flag
            OnIsMetricRegularSalesUnitChanged();

            //subscribe to events on the planogram.
            SetPlanEventHandlers(planogram, true);

            //Load from the fixture collection
            Planogram_FixtureCollectionChanged(_planogram.Fixtures, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

            //  Force correct sequence numbers if there are none.
            if (planogram.Fixtures.All(f => f.BaySequenceNumber == 0))
            {
                planogram.Model.RenumberingStrategy.RenumberBays(forced: true);
            }


            //Checks if the Cluster scheme name is valid before loading the window
            if (!String.IsNullOrEmpty(this.Planogram.ClusterSchemeName) && IsRepositoryConnected == true)
            {
                _clusterSchemeInfoListViewModel.FetchForCurrentEntity();

                try
                {
                    ClusterSchemeInfo cSchemeInfo = 
                        _clusterSchemeInfoListViewModel.Model.FirstOrDefault(p => p.Name == this.Planogram.ClusterSchemeName);
                    
                    if (cSchemeInfo != null)
                    {
                        _selectedClusterScheme = ClusterScheme.FetchById(cSchemeInfo.Id);
                    }
                }
                catch (DataPortalException ex)
                {
                    CommonHelper.RecordException(ex);
                }
            }

            if (_multiFixtureView == null)
            {
                _multiFixtureView = new PlanogramFixturePropertiesMultiView(_fixtures);
            }
        }

        #endregion

        #region Commands

        #region SplitBayCommand

        private RelayCommand _splitBayCommand;

        /// <summary>
        /// Adds a new fixture copy after the selected fixture
        /// </summary>
        public RelayCommand SplitBayCommand
        {
            get
            {
                if (_splitBayCommand == null)
                {
                    _splitBayCommand = new RelayCommand(
                        p => SplitBayCommand_Executed(),
                        p => SplitBayCommand_CanExecute())
                    {
                        FriendlyName = Message.SplitBay_Split,
                        FriendlyDescription = Message.SplitBay_Command_Split_Description,
                        SmallIcon = ImageResources.PlanogramProperties_SplitFixture,
                    };
                    base.ViewModelCommands.Add(_splitBayCommand);
                }
                return _splitBayCommand;
            }
        }

        private Boolean SplitBayCommand_CanExecute()
        {
            Boolean enableCommand = true;
            String disabledReason = String.Empty;

            return enableCommand;
        }

        private void SplitBayCommand_Executed()
        {
            base.ShowWaitCursor(true);

            List<PlanogramFixtureView> views = new List<PlanogramFixtureView>();
            if (MultiFixtureView != null)
            {
                views.AddRange(this.MultiFixtureView.SelectedItems);
            }
            else
            {
                views.Add(this.SelectedFixture);
            }
         
            new SplitBayEditorViewModel(this.Planogram, views).ShowDialog();

            base.ShowWaitCursor(false);
        }

        #endregion

        #region AddFixtureAfterCommand

        private RelayCommand _addFixtureAfterCommand;

        /// <summary>
        /// Adds a new fixture copy after the selected fixture
        /// </summary>
        public RelayCommand AddFixtureAfterCommand
        {
            get
            {
                if (_addFixtureAfterCommand == null)
                {
                    _addFixtureAfterCommand = new RelayCommand(
                        p => AddFixtureAfter_Executed(),
                        p => AddFixtureAfter_CanExecute())
                    {
                        FriendlyName = Message.PlanogramProperties_AddFixtureAfter,
                        FriendlyDescription = Message.PlanogramProperties_AddFixtureAfter_Desc,
                        SmallIcon = ImageResources.PlanogramProperties_AddFixtureAfter_16,
                    };
                    base.ViewModelCommands.Add(_addFixtureAfterCommand);
                }
                return _addFixtureAfterCommand;
            }
        }

        private Boolean AddFixtureAfter_CanExecute()
        {
            Boolean enableCommand = true;
            String disabledReason = String.Empty;

            if (this.MultiFixtureView != null &&
                this.MultiFixtureView.SelectedItems != null &&
                this.MultiFixtureView.SelectedItems.Count() != 1)
            {
                enableCommand = false;
                // ToDo: Localise this
                disabledReason = Message.PlanogramProperties_AddFixtureDisabled_MultipleBays;
            }

            _addFixtureAfterCommand.DisabledReason = disabledReason;

            return enableCommand;
        }

        private void AddFixtureAfter_Executed()
        {
            base.ShowWaitCursor(true);

            // Get the currently selected fixture - there should only be one at this stage
            // as the AddFixture buttons are disabled if multiple fixtures are selected
            if (MultiFixtureView != null)
            {
                var currentSelection = this.MultiFixtureView.SelectedItems.FirstOrDefault();

                if (currentSelection == null) return;

                // Reset all current selections
                this.MultiFixtureView.ClearAllSelections();

                this.SelectedFixture = currentSelection;
            }

            PlanogramFixtureView newFixture = AddNewFixture(SelectedFixture, /*insertBefore*/false);

            if (this.MultiFixtureView == null)
            {
                this._multiFixtureView = new PlanogramFixturePropertiesMultiView(new List<PlanogramFixtureView>() { newFixture });
            }
            else
            {
                this.MultiFixtureView.AddFixture(newFixture);
            }

            // And set the new fixture as the only selected item
            newFixture.IsSelected = true;
            this.SelectedFixture = newFixture;

            base.ShowWaitCursor(false);
        }

        #endregion

        #region AddFixtureBeforeCommand

        private RelayCommand _addFixtureBeforeCommand;

        /// <summary>
        /// Adds a new fixture copy before the selected fixture
        /// </summary>
        public RelayCommand AddFixtureBeforeCommand
        {
            get
            {
                if (_addFixtureBeforeCommand == null)
                {
                    _addFixtureBeforeCommand = new RelayCommand(
                        p => AddFixtureBefore_Executed(),
                        p => AddFixtureBefore_CanExecute())
                    {
                        FriendlyName = Message.PlanogramProperties_AddFixtureBefore,
                        FriendlyDescription = Message.PlanogramProperties_AddFixtureBefore_Desc,
                        SmallIcon = ImageResources.PlanogramProperties_AddFixtureBefore_16
                    };
                    base.ViewModelCommands.Add(_addFixtureBeforeCommand);
                }
                return _addFixtureBeforeCommand;
            }
        }

        private Boolean AddFixtureBefore_CanExecute()
        {
            Boolean enableCommand = true;
            String disabledReason = String.Empty;

            if (this.MultiFixtureView != null &&
                this.MultiFixtureView.SelectedItems != null &&
                this.MultiFixtureView.SelectedItems.Count() != 1)
            {
                enableCommand = false;
                // ToDo: Localise this
                disabledReason = Message.PlanogramProperties_AddFixtureDisabled_MultipleBays;
            }

            _addFixtureBeforeCommand.DisabledReason = disabledReason;

            return enableCommand;
        }

        private void AddFixtureBefore_Executed()
        {
            base.ShowWaitCursor(true);

            // Get the currently selected fixture - there should only be one at this stage
            // as the AddFixture buttons are disabled if multiple fixtures are selected
            var currentSelection = this.MultiFixtureView.SelectedItems.FirstOrDefault();

            if (currentSelection == null) return;

            // Reset all current selections
            this.MultiFixtureView.ClearAllSelections();

            this.SelectedFixture = currentSelection;
            PlanogramFixtureView newFixture = AddNewFixture(currentSelection, /*insertBefore*/true);
            this.MultiFixtureView.AddFixture(newFixture);

            // And set the new fixture as the only selected item
            newFixture.IsSelected = true;
            this.SelectedFixture = newFixture;

            base.ShowWaitCursor(false);
        }

        #endregion

        #region DeleteFixtureCommand

        private RelayCommand _deleteFixtureCommand;

        /// <summary>
        /// Deletes the currently selected fixture
        /// </summary>
        public RelayCommand DeleteFixtureCommand
        {
            get
            {
                if (_deleteFixtureCommand == null)
                {
                    _deleteFixtureCommand = new RelayCommand(
                        p => DeleteFixture_Executed(),
                        p => DeleteFixture_CanExecute())
                    {
                        FriendlyName = Message.PlanogramProperties_DeleteFixture,
                        FriendlyDescription = Message.PlanogramProperties_DeleteFixture_Desc,
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_deleteFixtureCommand);
                }
                return _deleteFixtureCommand;
            }
        }

        private Boolean DeleteFixture_CanExecute()
        {
            // Check there is at least one fixture selected
            if (!this.MultiFixtureView.SelectedItems.Any())
            {
                return false;
            }

            // Check that not all the fixtures are selected
            if (this.MultiFixtureView.SelectedItems.Count() == this.Fixtures.Count())
            {
                return false;
            }

            if (this.Fixtures.Count < 2)
            {
                return false;
            }

            return true;
        }

        private void DeleteFixture_Executed()
        {
            base.ShowWaitCursor(true);

            var bayItemsList = this.MultiFixtureView.SelectedItems.ToList();
            foreach (var bayItem in bayItemsList)
            {
                _planogram.RemoveFixture(bayItem);
            }

            this.MultiFixtureView.ClearAllSelections();

            var newSelection = this.Fixtures.OrderBy(f => f.X).LastOrDefault();

            if (newSelection != null)
            {
                newSelection.IsSelected = true;
                this.SelectedFixture = newSelection;
            }

            //V8-26850  : Added update planogram bounds for delete fixture
            _planogram.UpdatePlanogramBounds();

            base.ShowWaitCursor(false);
        }

        #endregion

        #region OKCommand

        private RelayCommand _okCommand;

        /// <summary>
        /// Commits changes and closes the window.
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OK_Executed(),
                        p => OK_CanExecute())
                    {
                        FriendlyName = Message.Generic_OK,
                        InputGestureKey = Key.Enter
                    };
                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }

        private Boolean OK_CanExecute()
        {
            if (Planogram.Inventory.MinShelfLife > 1 || Planogram.Inventory.MinShelfLife < 0)
            {
                _okCommand.DisabledReason = Message.PlanogramProperties_Ok_Disabled_ShelfLifeNotInRange;
                return false;
            }
            return true;
        }

        private void OK_Executed()
        {
            //Snap all components to notches if any notches were edited in this screen
            if (_displaySnapToNotchWarningOnClose)
            {
                CheckAndSnapComponentsToNearestNotch();
            }

            if (Planogram.RenumberingStrategy.IsDirty)
            {
                Planogram.RenumberingStrategy.RenumberBays();
            }

            this.DialogResult = true;

            //end the planogram undo record.
            _planogram.EndUndoableAction();
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels any changes made and closes
        /// the window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel,
                        InputGestureKey = Key.Escape
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            base.ShowWaitCursor(true);

            //cancel any changes
            _planogram.CancelUndoableAction(/*undoLoggedChanges*/true);

            this.DialogResult = false;

            base.ShowWaitCursor(false);
        }

        #endregion

        #region SelectMerchGroupCommand

        private RelayCommand _selectMerchGroupCommand;

        /// <summary>
        /// Cancels any changes made and closes
        /// the window.
        /// </summary>
        public RelayCommand SelectMerchGroupCommand
        {
            get
            {
                if (_selectMerchGroupCommand == null)
                {
                    _selectMerchGroupCommand = new RelayCommand(
                        p => SelectMerchGroup_Executed(),
                        p => SelectMerchGroup_CanExecute())
                    {
                        FriendlyName = Message.PlanogramProperties_SelectMerch_Select,
                        FriendlyDescription = Message.PlanogramProperties_SelectMerch_SelectCategory
                    };
                    base.ViewModelCommands.Add(_selectMerchGroupCommand);
                }
                return _selectMerchGroupCommand;
            }
        }

        private Boolean SelectMerchGroup_CanExecute()
        {
            if (App.ViewState.IsConnectedToRepository)
            {
                return true;
            }
            else
            {
                _selectMerchGroupCommand.DisabledReason = Message.PlanogramProperties_SelectMerch_Disabled_NoConnection;
                return false;
            }
        }

        private void SelectMerchGroup_Executed()
        {
            if (App.ViewState.IsConnectedToRepository)
            {
                //Show product universe selector
                MerchGroupSelectionWindow win = new MerchGroupSelectionWindow();
                win.SelectionMode = System.Windows.Controls.DataGridSelectionMode.Single;
                App.ShowWindow(win, true);

                //If dialog returned true
                if (win.SelectionResult != null && win.SelectionResult.Count > 0)
                {
                    //set category code and name
                    this.Planogram.CategoryCode = win.SelectionResult.First().Code;
                    this.Planogram.CategoryName = win.SelectionResult.First().Name;
                }
            }
        }

        #endregion

        #region NewMetricCommand

        private RelayCommand _newMetricCommand;

        /// <summary>
        /// Creates a new metric
        /// </summary>
        public RelayCommand NewMetricCommand
        {
            get
            {
                if (_newMetricCommand == null)
                {
                    _newMetricCommand = new RelayCommand(
                        p => NewMetric_Executed(),
                        p => NewMetric_CanExecute())
                    {
                        FriendlyName = Message.Generic_Add,
                        FriendlyDescription = Message.PlanogramProperties_AddMetric_Description,
                        SmallIcon = ImageResources.PlanogramProperties_AddPerformanceMetric,
                        DisabledReason = Message.PlanogramProperties_AddMetric_DisabledReason
                    };
                    base.ViewModelCommands.Add(_newMetricCommand);
                }
                return _newMetricCommand;
            }
        }

        private Boolean NewMetric_CanExecute()
        {
            return PlanogramPerformanceIsValid && PerformanceMetrics.Count < 20;
        }

        private void NewMetric_Executed()
        {
            // Adds a new metric.
            SelectedPerformanceMetric = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric();
            SelectedPerformanceMetric.MetricId = (Byte)(PerformanceMetrics.Count + 1);
            PerformanceMetrics.Add(SelectedPerformanceMetric);

            // Check if we are running under a unit test environment
            if (Application.Current != null)
            {
                _metricWindow = new PlanogramPerformanceMetricWindow(this, true);
                if (this.AttachedControl != null)
                {
                    App.ShowWindow(_metricWindow, true);
                }
            }

            OnPropertyChanged(PlanogramContainsPromotionalPerformanceMetricProperty);

            if (!PlanogramContainsPromotionalPerformanceMetric)
            {
                this.Planogram.Inventory.InventoryMetricType = PlanogramInventoryMetricType.RegularSalesUnits;
            }
        }

        #endregion

        #region ViewMetricCommand

        private RelayCommand _viewMetricCommand;

        /// <summary>
        /// Creates a new metric
        /// </summary>
        public RelayCommand ViewMetricCommand
        {
            get
            {
                if (_viewMetricCommand == null)
                {
                    _viewMetricCommand = new RelayCommand(
                        p => ViewMetric_Executed(),
                        p => ViewMetric_CanExecute())
                    {
                        FriendlyName = Message.PlanogramProperties_EditMetric,
                        FriendlyDescription = Message.PlanogramProperties_EditMetric_Description,
                        SmallIcon = ImageResources.PlanogramProperties_ViewPerformanceMetric,
                        DisabledReason = Message.PlanogramProperties_EditMetric_DisabledReason
                    };
                    base.ViewModelCommands.Add(_viewMetricCommand);
                }
                return _viewMetricCommand;
            }
        }

        private Boolean ViewMetric_CanExecute()
        {
            return this.SelectedPerformanceMetric != null;
        }

        private void ViewMetric_Executed()
        {
            _metricWindow = new PlanogramPerformanceMetricWindow(this, false);
            if (this.AttachedControl != null)
            {
                App.ShowWindow(_metricWindow, true);
            }

            OnPropertyChanged(PlanogramContainsPromotionalPerformanceMetricProperty);

            if (!PlanogramContainsPromotionalPerformanceMetric)
            {
                this.Planogram.Inventory.InventoryMetricType = PlanogramInventoryMetricType.RegularSalesUnits;
            }
        }

        #endregion

        #region RemoveMetricCommand

        private RelayCommand _removeMetricCommand;

        /// <summary>
        /// Creates a new metric
        /// </summary>
        public RelayCommand RemoveMetricCommand
        {
            get
            {
                if (_removeMetricCommand == null)
                {
                    _removeMetricCommand = new RelayCommand(
                        p => RemoveMetric_Executed(),
                        p => RemoveMetric_CanExecute())
                    {
                        FriendlyName = Message.Generic_Remove,
                        FriendlyDescription = Message.PlanogramProperties_RemoveMetric_Description,
                        SmallIcon = ImageResources.PlanogramProperties_RemovePerformanceMetric,
                        DisabledReason = Message.PlanogramProperties_RemoveMetric_DisabledReason
                    };
                    base.ViewModelCommands.Add(_removeMetricCommand);
                }
                return _removeMetricCommand;
            }
        }

        private Boolean RemoveMetric_CanExecute()
        {
            return this.SelectedPerformanceMetric != null;
        }

        private void RemoveMetric_Executed()
        {
            Byte metricId = SelectedPerformanceMetric.MetricId;
            foreach (PlanogramPerformanceData data in Planogram.Model.Performance.PerformanceData)
            {
                data.SetPerformanceData(metricId, null);
            }
            this.Planogram.PerformanceMetrics.Remove(this.SelectedPerformanceMetric);
            this.SelectedPerformanceMetric = null;

            OnPropertyChanged(PlanogramContainsPromotionalPerformanceMetricProperty);

            if (!PlanogramContainsPromotionalPerformanceMetric)
            {
                this.Planogram.Inventory.InventoryMetricType = PlanogramInventoryMetricType.RegularSalesUnits;
            }
        }

        #endregion

        #region MetricSaveCommand

        private RelayCommand _metricSaveCommand;

        public RelayCommand MetricSaveCommand
        {
            get
            {
                if (_metricSaveCommand == null)
                {
                    _metricSaveCommand = new RelayCommand(
                        p => MetricSave_Executed(),
                        p => MetricSave_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        DisabledReason = Message.PlanogramProperties_MetricSave_DisabledReason
                    };
                }
                return _metricSaveCommand;
            }
        }

        private Boolean MetricSave_CanExecute()
        {
            if (SelectedPerformanceMetric == null) return false;
            return PlanogramPerformanceIsValid && SelectedPerformanceMetric.IsValid;
        }

        private void MetricSave_Executed()
        {
            if (_metricWindow != null)
            {
                _metricWindow.Close();
                _metricWindow = null;
            }
        }

        #endregion

        #region MetricCancelCommand

        private RelayCommand _metricCancelCommand;

        public RelayCommand MetricCancelCommand
        {
            get
            {
                if (_metricCancelCommand == null)
                {
                    _metricCancelCommand = new RelayCommand(p => MetricCancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                }
                return _metricCancelCommand;
            }
        }

        private void MetricCancel_Executed()
        {
            // If New Metric, remove from list
            if (_metricWindow.NewMetric)
            {
                PerformanceMetrics.Remove(SelectedPerformanceMetric);
                SelectedPerformanceMetric = null;
            }
            else // Replace metric with original
            {
                SelectedPerformanceMetric.CopyValues(_metricWindow.OriginalMetric);
            }

            _metricWindow.Close();
            _metricWindow = null;
        }

        #endregion
        
        #region MoveFixtureLeftCommand

        private RelayCommand _moveFixtureLeftCommand;

        /// <summary>
        /// Moves Selected Fixture Left
        /// </summary>
        public RelayCommand MoveFixtureLeftCommand
        {
            get
            {
                if (_moveFixtureLeftCommand == null)
                {
                    _moveFixtureLeftCommand = new RelayCommand(
                        p => MoveFixtureLeft_Executed(),
                        p => MoveFixtureLeft_CanExecute())
                    {
                        FriendlyName = Message.PlanogramProperties_MoveFixtureLeft,
                        FriendlyDescription = Message.PlanogramProperties_MoveFixtureLeft_Desc,
                        SmallIcon = ImageResources.PlanogramProperties_MoveFixtureLeft_16,
                    };
                    base.ViewModelCommands.Add(_moveFixtureLeftCommand);
                }
                return _moveFixtureLeftCommand;
            }
        }

        private Boolean MoveFixtureLeft_CanExecute()
        {
            if (GetPreviousFixtureByPlanX(SelectedFixture) == null)
            {
                // disabled if there is not fixture to the left
                _moveFixtureLeftCommand.DisabledReason = Message.PlanogramProperties_MoveFixtureLeft_Disabled_NoFixtureLeft;
                return false;
            }

            // Return false if multiple fixtures are selected.
            if (MultiFixtureView != null && MultiFixtureView.SelectedItems.Count() > 1)
            {
                return false;
            }

            return true;
        }

        public void MoveFixtureLeft_Executed()
        {
            ShowWaitCursor(true);

            //Get the fixture that is the closest fixture on the left of the selected fixture.
            PlanogramFixtureView match = GetPreviousFixtureByPlanX(SelectedFixture);

            //Check whether there is a match.
            if (match == null)
            {
                Debug.Fail("PlanogramPropertiesViewModel.MoveFixtureLeft_Executed() was called when there was no fixture to the left.");
                return;
            }
            
            //Can we re calculate the fixture X Positions
            bool fixtureCalculatePositions = _planogram.Model.CanReCalculateFixtureXPositions();
            Dictionary<PlanogramFixtureItem, float> fixtureSpacing = new Dictionary<PlanogramFixtureItem, float>();

            if (fixtureCalculatePositions)
            {
                //Get the current spacing between each fixture
                fixtureSpacing = _planogram.Model.RenumberingStrategy.GetFixtureSpacing();
            }

            //Reorder or swap the fixtures around
            SwapFixtures(SelectedFixture, match);

            if (fixtureCalculatePositions)
            {
                //Recalculate the fixture X positions including fixture spacings
                _planogram.Model.RecalculateFixtureXPositions(fixtureSpacing, true);
            }
            else
            {
                //Continue with the normalise bay function if it cannot be recalculated
                _planogram.Model.NormalizeBays();
            }
            
            //Force the renumbering strategy
            _planogram.Model.RenumberingStrategy.RenumberBays(forced: true);

            //Update the fixture stack on the attached control
            if (AttachedControl != null)
            {
                AttachedControl.UpdateFixtureStack();
            }

            ShowWaitCursor(false);
        }
        
        private static void SwapFixtures(PlanogramFixtureViewModelBase right, PlanogramFixtureViewModelBase left)
        {
            //  Get the bounding boxes.
            RectValue rightBounds = right.GetWorldSpaceBounds();
            Boolean isInvertedRight = right.X.GreaterThan(rightBounds.X);
            RectValue leftBounds = left.GetWorldSpaceBounds();
            Boolean isInvertedLeft = left.X.GreaterThan(leftBounds.X);

            //  Move right to left. All the way to the left, if it is inverted, offset to the right by its width.
            right.X = leftBounds.X;
            if (isInvertedRight) right.X += rightBounds.Width;

            //  Move left to right. All the way to the right, if it is not inverted, offset to the left by its width.
            left.X = rightBounds.X + rightBounds.Width;
            if (!isInvertedLeft) left.X -= leftBounds.Width;
        }

        #endregion

        #region MoveFixtureRightCommand

        private RelayCommand _moveFixtureRightCommand;

        /// <summary>
        /// Moves Selected Fixture Right
        /// </summary>
        public RelayCommand MoveFixtureRightCommand
        {
            get
            {
                if (_moveFixtureRightCommand == null)
                {
                    _moveFixtureRightCommand = new RelayCommand(
                        p => MoveFixtureRight_Executed(),
                        p => MoveFixtureRight_CanExecute())
                    {
                        FriendlyName = Message.PlanogramProperties_MoveFixtureRight,
                        FriendlyDescription = Message.PlanogramProperties_MoveFixtureRight_Desc,
                        SmallIcon = ImageResources.PlanogramProperties_MoveFixtureRight_16
                    };
                    base.ViewModelCommands.Add(_moveFixtureRightCommand);
                }
                return _moveFixtureRightCommand;
            }
        }

        private Boolean MoveFixtureRight_CanExecute()
        {
            if (GetNextFixtureByPlanX(SelectedFixture) == null)
            {
                // disabled if there is not fixture to the right
                _moveFixtureRightCommand.DisabledReason = Message.PlanogramProperties_MoveFixtureRight_Disabled_NoFixtureRight;
                return false;
            }

            // Return false if multiple fixtures are selected.
            if (MultiFixtureView != null && MultiFixtureView.SelectedItems.Count() > 1)
            {
                return false;
            }

            return true;
        }

        public void MoveFixtureRight_Executed()
        {
            ShowWaitCursor(true);

            //Get the fixture that is the closest fixture on the right of the selected fixture.
            PlanogramFixtureView match = GetNextFixtureByPlanX(SelectedFixture);

            //Check whether there is a match.
            if (match == null)
            {
                Debug.Fail("PlanogramPropertiesViewModel.MoveFixtureRight_Executed() was called when there was no fixture to the right.");
                return;
            }

            //Can we re calculate the fixture X Positions
            bool fixtureCalculatePositions = _planogram.Model.CanReCalculateFixtureXPositions();
            Dictionary<PlanogramFixtureItem, float> fixtureSpacing = new Dictionary<PlanogramFixtureItem, float>();

            if (fixtureCalculatePositions)
            {
                //Get the current spacing between each fixture
                fixtureSpacing = _planogram.Model.RenumberingStrategy.GetFixtureSpacing();
            }

            //Reorder or swap the fixtures around
            SwapFixtures(SelectedFixture, match);

            if (fixtureCalculatePositions)
            {
                //Recalculate the fixture X positions including fixture spacings
                _planogram.Model.RecalculateFixtureXPositions(fixtureSpacing, true);
            }
            else
            {
                //Continue with the normalise bay function if it cannot be recalculated
                _planogram.Model.NormalizeBays();
            }

            //Force the renumbering strategy
            _planogram.Model.RenumberingStrategy.RenumberBays(forced: true);

            //Update the fixture stack on the attached control
            if (AttachedControl != null)
            {
                AttachedControl.UpdateFixtureStack();
            }

            ShowWaitCursor(false);
        }
        #endregion

        #region SelectLocationCommand

        private RelayCommand _selectLocationCommand;
        /// <summary>
        /// Command to select location from a window
        /// </summary>
        public RelayCommand SelectLocationCommand
        {
            get
            {
                if (_selectLocationCommand == null)
                {
                    _selectLocationCommand = new RelayCommand(
                      p => SelectLocationCommand_Executed(),
                      p => SelectLocationCommand_CanExecute())
                    {
                        FriendlyName = Message.PlanogramPropertiesSelectLocation
                    };
                    base.ViewModelCommands.Add(_selectLocationCommand);
                }
                return _selectLocationCommand;
            }

        }

        private Boolean SelectLocationCommand_CanExecute()
        {
            if (!App.ViewState.IsConnectedToRepository)
            {
                SelectLocationCommand.DisabledReason = Message.PlanogramProperties_SelectLocationDisabledReason;
                return false;
            }
            return true;
        }

        public void SelectLocationCommand_Executed()
        {
            if (App.ViewState.IsConnectedToRepository)
            {
                LocationHierarchyViewModel locationHierarchyViewModel = new LocationHierarchyViewModel();
                locationHierarchyViewModel.FetchForCurrentEntity();
                LocationInfoListViewModel locationInfoListView = new LocationInfoListViewModel();
                locationInfoListView.FetchAllForEntity();

                var locations = new ObservableCollection<LocationInfo>();
                foreach (var locItem in locationInfoListView.BindableCollection)
                {
                    locations.Add(locItem);
                }
                var selectedLocations = new ObservableCollection<LocationInfo>();

                LocationSelectionViewModel viewModel = new LocationSelectionViewModel(locations, selectedLocations, locationHierarchyViewModel.Model, DataGridSelectionMode.Single);
                CommonHelper.GetWindowService().ShowDialog<LocationSelectionWindow>(viewModel);
                //if a location is selected, Set the name and code of that location
                if (viewModel.SelectedAvailableLocations.Count > 0 && viewModel.DialogResult == true)
                {
                    this.Planogram.LocationCode = viewModel.SelectedAvailableLocations.First().Location.Code;
                    this.Planogram.LocationName = viewModel.SelectedAvailableLocations.First().Location.Name;
                }
                //method to know if changing the selected location will change the selected cluster
                ShowClusterChangeWarning();

            }
        }

        #endregion

        #region SelectClusterSchemeCommand

        private RelayCommand _selectClusterSchemeCommand;
        /// <summary>
        /// Command to select clusterScheme
        /// </summary>
        public RelayCommand SelectClusterSchemeCommand
        {
            get
            {
                if (_selectClusterSchemeCommand == null)
                {
                    _selectClusterSchemeCommand = new RelayCommand(
                      p => SelectClusterSchemeCommand_Executed(),
                      p => SelectClusterSchemeCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                    base.ViewModelCommands.Add(_selectClusterSchemeCommand);
                }
                return _selectClusterSchemeCommand;
            }

        }

        private Boolean SelectClusterSchemeCommand_CanExecute()
        {
            if (!App.ViewState.IsConnectedToRepository)
            {
                SelectClusterSchemeCommand.DisabledReason = Message.SelectedClusterScheme_SelectClusterSchemeDisabledReason;
                return false;
            }
            return true;
        }

        public void SelectClusterSchemeCommand_Executed()
        {
            ClusterSchemeSelectionViewModel viewModel = new ClusterSchemeSelectionViewModel();
            CommonHelper.GetWindowService().ShowDialog<ClusterSchemeSelectionWindow>(viewModel);

            if (viewModel.DialogResult == true)
            {
                // Fetch clusterscheme using the selected clusterSchemeInfo
                ClusterScheme selectedClusterScheme = ClusterScheme.FetchById(viewModel.SelectedClusterSchemeInfo.Id);

                // set the selected clusterScheme name
                this.Planogram.ClusterSchemeName = selectedClusterScheme.Name;
                ShowClusterChangeWarning();
                // Try to populate the cluster if the set location code is in the selected ClusterScheme
                if (this.Planogram.LocationCode != String.Empty)
                {
                    foreach (Cluster cluster in selectedClusterScheme.Clusters)
                    {
                        foreach (ClusterLocation clusterLocation in cluster.Locations)
                        {
                            if (this.Planogram.LocationCode == clusterLocation.Code)
                            {
                                this.Planogram.ClusterName = cluster.Name;
                                IsShowWarning = false;
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region SelectClusterCommand

        private RelayCommand _selectClusterCommand;

        /// <summary>
        /// Command to select a cluster 
        /// </summary>
        public RelayCommand SelectClusterCommand
        {
            get
            {
                if (_selectClusterCommand == null)
                {
                    _selectClusterCommand = new RelayCommand
                        (p => SelectClusterCommand_Executed(),
                        p => SelectClusterCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                    base.ViewModelCommands.Add(_selectClusterCommand);
                }
                return _selectClusterCommand;
            }
        }

        private Boolean SelectClusterCommand_CanExecute()
        {
            if (String.IsNullOrEmpty(this.Planogram.ClusterSchemeName))
            {
                SelectClusterCommand.DisabledReason = Message.PlanogramProperties_NoSelectedClusterScheme;
                return false;
            }

            if (!App.ViewState.IsConnectedToRepository)
            {
                SelectClusterCommand.DisabledReason = Message.PlanogramProperties_NoRepositoryConnection;
                return false;
            }
            return true;
        }

        public void SelectClusterCommand_Executed()
        {
            // load up the available cluster scheme
            _clusterSchemeInfoListViewModel.FetchForCurrentEntity();
            // check that the cluster Scheme name is not empty
            if (this.Planogram.ClusterSchemeName != String.Empty)
            {
                // check that the clusterScheme name entered exists
                if (_clusterSchemeInfoListViewModel.Model.Any(s => s.Name == this.Planogram.ClusterSchemeName))
                {
                    // if name exists then get the clusterScheme with the id
                    ClusterScheme clusterScheme = ClusterScheme.FetchById(_clusterSchemeInfoListViewModel.Model.Where(p => p.Name == this.Planogram.ClusterSchemeName).FirstOrDefault().Id);
                    LocationClusterSelectorViewModel viewModel = new LocationClusterSelectorViewModel(clusterScheme, true);
                    CommonHelper.GetWindowService().ShowDialog<LocationClusterSelectionWindow>(viewModel);

                    //set the cluster name
                    if (viewModel.DialogResult == true)
                    {
                        this.Planogram.ClusterName = viewModel.SelectedCluster.Name;
                        ShowClusterChangeWarning();
                    }
                }
                else
                {
                    // Load up window and display a message that the cluster scheme does not exists
                    LocationClusterSelectorViewModel viewModel = new LocationClusterSelectorViewModel(null, false);
                    CommonHelper.GetWindowService().ShowDialog<LocationClusterSelectionWindow>(viewModel);

                }
            }

        }

        #endregion

        #region DuplicateBayLeavingWhiteSpace
        private RelayCommand _duplicateBayLeavingWhiteSpaceCommand;

        /// <summary>
        ///  Creates a new copy of the currently selected bay and places it after the existing one.
        /// No position facing value changes are made.
        /// </summary>
        public RelayCommand DuplicateBayLeavingWhiteSpaceCommand
        {
            get
            {
                if (_duplicateBayLeavingWhiteSpaceCommand == null)
                {
                    _duplicateBayLeavingWhiteSpaceCommand = new RelayCommand(
                        p => DuplicateBayLeavingWhiteSpace_Executed(),
                        p => DuplicateBayLeavingWhiteSpace_CanExecute())
                        {
                            FriendlyName = Message.PlanogramProperties_DuplicateBayLeavingWhiteSpace,
                            SmallIcon = ImageResources.Add_16
                        };
                    base.ViewModelCommands.Add(_duplicateBayLeavingWhiteSpaceCommand);
                }
                return _duplicateBayLeavingWhiteSpaceCommand;
            }
        }

        private Boolean DuplicateBayLeavingWhiteSpace_CanExecute()
        {
            if (this.SelectedFixture == null) return false;

            // Return false if multiple fixtures are selected.
            if (MultiFixtureView != null && MultiFixtureView.SelectedItems.Count() > 1)
            {
                return false;
            }

            if (this.Planogram.ProductPlacementX == PlanogramProductPlacementXType.FillWide)
            {
                return false;
            }

            return true;
        }

        private void DuplicateBayLeavingWhiteSpace_Executed()
        {
            PlanogramView plan = _planogram;
            plan.BeginUpdate();

            // Get the currently selected fixture - there should only be one at this stage
            // as the AddFixture buttons are disabled if multiple fixtures are selected
            if (MultiFixtureView != null)
            {
                var currentSelection = this.MultiFixtureView.SelectedItems.FirstOrDefault();

                if (currentSelection == null) return;

                // Reset all current selections
                this.MultiFixtureView.ClearAllSelections();

                this.SelectedFixture = currentSelection;
            }

            var origFixture = this.SelectedFixture;

            var newFixture = plan.AddFixtureFromCopy(
                            this.SelectedFixture, /*insertBefore*/false, /*copy components*/true,  /*copy positions*/false);

            //if automatic renumbering is off then we should also correct the bay sequence.
            if (!plan.Model.RenumberingStrategy.IsEnabled)
            {
                newFixture.BaySequenceNumber = (Int16)(origFixture.BaySequenceNumber + 1);

                List<PlanogramFixtureView> orderedBays = plan.Fixtures.Where(f => f != newFixture).OrderBy(f => f.BaySequenceNumber).ToList();
                Boolean offset = false;
                foreach (PlanogramFixtureView bay in orderedBays)
                {
                    if (bay == origFixture)
                    {
                        offset = true;
                    }
                    else if (offset)
                    {
                        bay.BaySequenceNumber += 1;
                    }
                }
            }


            //V8-32761: Changed combine behaviour to stop combine. Leave this in until change is confirmed as ok:
            ////allow components on the new fixture to combine with those on the left
            //foreach (var component in newFixture.EnumerateAllComponents())
            //{
            //    if (component.SubComponents.Count != 1
            //       || component.SubComponents[0].MerchandisingType == PlanogramSubComponentMerchandisingType.None) continue;

            //    if (component.SubComponents[0].CombineType == PlanogramSubComponentCombineType.RightOnly)
            //    {
            //        component.SubComponents[0].CombineType = PlanogramSubComponentCombineType.Both;
            //    }
            //    else if (component.SubComponents[0].CombineType == PlanogramSubComponentCombineType.None)
            //    {
            //        component.SubComponents[0].CombineType = PlanogramSubComponentCombineType.LeftOnly;
            //    }
            //}

            ////allow components on the original fixture to combine right
            //foreach (PlanogramComponentView component in origFixture.EnumerateAllComponents())
            //{
            //    if (component.SubComponents.Count != 1
            //        || component.SubComponents[0].MerchandisingType == PlanogramSubComponentMerchandisingType.None) continue;

            //    if (component.SubComponents[0].CombineType == PlanogramSubComponentCombineType.LeftOnly)
            //    {
            //        component.SubComponents[0].CombineType = PlanogramSubComponentCombineType.Both;
            //    }
            //    else if (component.SubComponents[0].CombineType == PlanogramSubComponentCombineType.None)
            //    {
            //        component.SubComponents[0].CombineType = PlanogramSubComponentCombineType.RightOnly;
            //    }
            //}

            //stop components on the new fixture from combining with others
            foreach (var component in newFixture.EnumerateAllComponents())
            {
                if (component.SubComponents.Count != 1
                   || component.SubComponents[0].MerchandisingType == PlanogramSubComponentMerchandisingType.None) continue;

                component.SubComponents[0].CombineType = PlanogramSubComponentCombineType.None;
            }



            plan.EndUpdate();
            plan.UpdatePlanogramBounds();

            if (this.MultiFixtureView == null)
            {
                this._multiFixtureView = new PlanogramFixturePropertiesMultiView(new List<PlanogramFixtureView>() { newFixture });
            }
            else
            {
                this.MultiFixtureView.AddFixture(newFixture);
            }

            // And set the new fixture as the only selected item
            newFixture.IsSelected = true;
            this.SelectedFixture = newFixture;
        }

        #endregion

        #region DuplicateBayAndAutofill
        private RelayCommand _duplicateBayAndAutofillCommand;

        /// <summary>
        /// Creates a new copy of the currently selected bay and places it after the existing one.
        /// Positions affected by merch groups on the new bay are then autofilled wide.
        /// </summary>
        public RelayCommand DuplicteBayAndAutofillCommand
        {
            get
            {
                if (_duplicateBayAndAutofillCommand == null)
                {
                    _duplicateBayAndAutofillCommand = new RelayCommand(
                        p => DuplicateBayAndAutofill_Executed(),
                        p => DuplicateBayAndAutofill_CanExecute())
                    {
                        FriendlyName = Message.PlanogramProperties_DuplicateBayAndAutofill,
                        SmallIcon = ImageResources.Add_16
                    };
                    base.ViewModelCommands.Add(_duplicateBayAndAutofillCommand);
                }
                return _duplicateBayAndAutofillCommand;
            }
        }

        private Boolean DuplicateBayAndAutofill_CanExecute()
        {
            if (this.SelectedFixture == null) return false;

            // Return false if multiple fixtures are selected.
            if (MultiFixtureView != null && MultiFixtureView.SelectedItems.Count() > 1)
            {
                return false;
            }
            return true;
        }

        private void DuplicateBayAndAutofill_Executed()
        {
            base.ShowWaitCursor(true);

            PlanogramView plan = _planogram;
            plan.BeginUpdate();

            // Get the currently selected fixture - there should only be one at this stage
            // as the AddFixture buttons are disabled if multiple fixtures are selected
            if (MultiFixtureView != null)
            {
                var currentSelection = this.MultiFixtureView.SelectedItems.FirstOrDefault();

                if (currentSelection == null) return;

                // Reset all current selections
                this.MultiFixtureView.ClearAllSelections();

                this.SelectedFixture = currentSelection;
            }

            var origFixture = this.SelectedFixture;

            var newFixture = plan.AddFixtureFromCopy(
                this.SelectedFixture, /*insertBefore*/false, /*copy components*/true, /*copy positions*/false);


            //if automatic renumbering is off then we should also correct the bay sequence.
            if (!plan.Model.RenumberingStrategy.IsEnabled)
            {
                newFixture.BaySequenceNumber = (Int16)(origFixture.BaySequenceNumber + 1);

                List<PlanogramFixtureView> orderedBays = plan.Fixtures.Where(f => f != newFixture).OrderBy(f => f.BaySequenceNumber).ToList();
                Boolean offset = false;
                foreach (PlanogramFixtureView bay in orderedBays)
                {
                    if (bay == origFixture)
                    {
                        offset = true;
                    }
                    else if (offset)
                    {
                        bay.BaySequenceNumber += 1;
                    }
                }
            }

            //allow components on the new fixture to combine with those on the left
            foreach (var component in newFixture.EnumerateAllComponents())
            {
                if (component.SubComponents.Count != 1
                   || component.SubComponents[0].MerchandisingType == PlanogramSubComponentMerchandisingType.None) continue;

                if (component.SubComponents[0].CombineType == PlanogramSubComponentCombineType.RightOnly)
                {
                    component.SubComponents[0].CombineType = PlanogramSubComponentCombineType.Both;
                }
                else if (component.SubComponents[0].CombineType == PlanogramSubComponentCombineType.None)
                {
                    component.SubComponents[0].CombineType = PlanogramSubComponentCombineType.LeftOnly;
                }
            }

            //allow components on the original fixture to combine right
            foreach (PlanogramComponentView component in origFixture.EnumerateAllComponents())
            {
                if (component.SubComponents.Count != 1
                    || component.SubComponents[0].MerchandisingType == PlanogramSubComponentMerchandisingType.None) continue;

                if (component.SubComponents[0].CombineType == PlanogramSubComponentCombineType.LeftOnly)
                {
                    component.SubComponents[0].CombineType = PlanogramSubComponentCombineType.Both;
                }
                else if (component.SubComponents[0].CombineType == PlanogramSubComponentCombineType.None)
                {
                    component.SubComponents[0].CombineType = PlanogramSubComponentCombineType.RightOnly;
                }
            }

            //get a list of all positions affected by this change.
            List<PlanogramSubComponentPlacement> allSubPlacements = plan.Model.GetPlanogramSubComponentPlacements().ToList();

            List<Object> positionIdsToAutofill =
                newFixture.EnumerateAllSubcomponents()
                .SelectMany(s => s.ModelPlacement.GetCombinedWithList(allSubPlacements)
                    .SelectMany(cs => cs.GetPlanogramPositions()
                        .Select(p => p.Id))).Distinct().ToList();

            List<PlanogramPositionView> positionsToAutofill =
                plan.EnumerateAllPositions().Where(p => positionIdsToAutofill.Contains(p.Model.Id)).ToList();


            //autofill all positions
            plan.FillPositionsOnGivenAxis(Framework.Enums.AxisType.X, positionsToAutofill);
           
            plan.EndUpdate();

            if (this.MultiFixtureView == null)
            {
                this._multiFixtureView = new PlanogramFixturePropertiesMultiView(new List<PlanogramFixtureView>() { newFixture });
            }
            else
            {
                this.MultiFixtureView.AddFixture(newFixture);
            }

            // And set the new fixture as the only selected item
            newFixture.IsSelected = true;
            this.SelectedFixture = newFixture;

            base.ShowWaitCursor(false);
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on the planogram
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Planogram_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Galleria.Framework.Planograms.Model.Planogram.LengthUnitsOfMeasureProperty.Name
                || e.PropertyName == Galleria.Framework.Planograms.Model.Planogram.AreaUnitsOfMeasureProperty.Name
                || e.PropertyName == Galleria.Framework.Planograms.Model.Planogram.VolumeUnitsOfMeasureProperty.Name
                || e.PropertyName == Galleria.Framework.Planograms.Model.Planogram.WeightUnitsOfMeasureProperty.Name
                || e.PropertyName == Galleria.Framework.Planograms.Model.Planogram.CurrencyUnitsOfMeasureProperty.Name)
            {
                OnPropertyChanged(DisplayUnitsProperty);
            }
        }

        /// <summary>
        /// Called whenever the planogram fixtures collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Planogram_FixtureCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            //resynch the fixtures collection
            PlanogramFixtureView selectedFixture = this.SelectedFixture;

            if (_fixtures.Count > 0) { _fixtures.Clear(); }
            _fixtures.AddRange(_planogram.Fixtures);

            //if no fixtures exist then add an initial one.
            if (_fixtures.Count == 0)
            {
                this.AddFixtureAfterCommand.Execute();

                //apply default settings to the new fixture
                //this.IsBackboardRequired = _appSettingsView.Model.FixtureHasBackboard;

                if (MultiFixtureView != null)
                {
                    MultiFixtureView.IsBackboardRequired = _appSettingsView.Model.FixtureHasBackboard;
                }

                if (MultiFixtureView != null)
                {
                    //this.IsBaseRequired = _appSettingsView.Model.FixtureHasBase;
                    MultiFixtureView.IsBaseRequired = _appSettingsView.Model.FixtureHasBase;
                }
            }
            else
            {
                if (MultiFixtureView != null)
                {
                    _multiFixtureView.Dispose();
                    _multiFixtureView = new PlanogramFixturePropertiesMultiView(_fixtures);
                    OnPropertyChanged(MultiFixtureViewProperty);
                }
            }

            this.SelectedFixture = (_fixtures.Contains(selectedFixture)) ?
                selectedFixture : _fixtures.FirstOrDefault();
        }

        /// <summary>
        /// Called whenever a child changes on the planogram
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Planogram_ChildChanged(object sender, ViewModelChildChangedEventArgs e)
        {
            if (e.PropertyChangedArgs != null)
            {
                #region Planogram Fixture View
                if (e.ChildObject is PlanogramFixtureView)
                {
                    PlanogramFixtureView fixture = (PlanogramFixtureView)e.ChildObject;

                    if (this._multiFixtureView != null && 
                        this._multiFixtureView.SelectedItems != null && 
                        this._multiFixtureView.SelectedItems.Contains(fixture))
                    {
                        if (e.PropertyChangedArgs.PropertyName == PlanogramFixture.HeightProperty.Name)
                        {
                            if (MultiFixtureView != null)
                            {
                                this.MultiFixtureView.BackboardHeight = fixture.Height;
                            }

                            //update the size of the planogram
                            OnFixtureSizeChanged((PlanogramFixtureView)e.ChildObject);
                        }
                        else if (e.PropertyChangedArgs.PropertyName == PlanogramFixture.WidthProperty.Name)
                        {
                            if (MultiFixtureView != null)
                            {
                                this.MultiFixtureView.BackboardWidth = fixture.Width;

                                this.MultiFixtureView.BaseWidth = fixture.Width;

                                fixture.ResizeComponents();
                                fixture.ResizeAssemblies();
                            }

                            //update the size of the planogram
                            OnFixtureSizeChanged((PlanogramFixtureView)e.ChildObject);
                        }
                        else if (e.PropertyChangedArgs.PropertyName == PlanogramFixture.DepthProperty.Name
                        || e.PropertyChangedArgs.PropertyName == PlanogramFixtureItem.BaySequenceNumberProperty.Name)
                        {
                            if (MultiFixtureView != null )
                            {
                                this.MultiFixtureView.BaseDepth = fixture.Depth;
                            }

                            //update the size of the planogram
                            OnFixtureSizeChanged((PlanogramFixtureView)e.ChildObject);
                        }
                        else if (e.PropertyChangedArgs.PropertyName == PlanogramFixtureView.HasBackboardProperty.Path)
                        {
                            //update the size of the planogram
                            OnFixtureSizeChanged((PlanogramFixtureView)e.ChildObject);
                        }
                    }
                }
                #endregion

                #region Planogram Component View
                else if (e.ChildObject is PlanogramComponentView)
                {
                    if (e.PropertyChangedArgs.PropertyName == PlanogramComponent.DepthProperty.Name)
                    {
                        PlanogramComponentView childComponent = (PlanogramComponentView)e.ChildObject;
                        if (childComponent.ComponentType == PlanogramComponentType.Backboard)
                        {
                            childComponent.Z = -childComponent.Depth;
                        }
                    }
                }
                #endregion

                #region Planogram SubComponent View
                else if (e.ChildObject is PlanogramSubComponentView)
                {
                    //object must be a backboard with notches now turned on
                    PlanogramSubComponentView childSubComponent = (PlanogramSubComponentView)e.ChildObject;

                    //Only inform user if notches are used - they may have just been turned on
                    if (childSubComponent.Component == null) return;

                    //must be a backboard that has been edited
                    if (childSubComponent.Component.ComponentType != PlanogramComponentType.Backboard) return;

                    //and a y-position related property must have been edited
                    if (e.PropertyChangedArgs.PropertyName == PlanogramSubComponent.IsNotchPlacedOnFrontProperty.Name ||
                        e.PropertyChangedArgs.PropertyName == PlanogramSubComponent.NotchSpacingYProperty.Name ||
                        e.PropertyChangedArgs.PropertyName == PlanogramSubComponent.NotchStartYProperty.Name)
                    {
                        //notches must be turned on for this fixture
                        if (!childSubComponent.IsNotchPlacedOnFront)
                        {
                            //if no fixtures have notches turned on, cancel the warning message
                            //Note: We cannot tell individually if a user has turned them on/off/on on multiple 
                            //bays in this window but this the best we can do
                            if (!CheckAtLeastOneBayHasNotchesEnabled()) _displaySnapToNotchWarningOnClose = false;
                            return;
                        }

                        //check fixture has at least one child component that is not a backboard or a base
                        foreach (PlanogramFixtureView fixture in this.Fixtures)
                        {
                            //check fixture has notches tuned on
                            IEnumerable<PlanogramSubComponentView> subComponents = fixture.EnumerateAllSubcomponents();
                            PlanogramSubComponentView backboard = subComponents
                                .Where(s => s.Component != null)
                                .FirstOrDefault(s => s.Component.ComponentType == PlanogramComponentType.Backboard);
                            if (backboard != null)
                            {
                                if (backboard.IsNotchPlacedOnFront)
                                {
                                    foreach (PlanogramComponentView component in fixture.Components)
                                    {
                                        if (component.ComponentModel != null)
                                        {
                                            //only consider components that are not backboards or bases (or carparks :) )
                                            if (component.ComponentModel.IsMerchandisable)
                                            {
                                                // merchandisable components exist so set snap to notch check
                                                _displaySnapToNotchWarningOnClose = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
            }
        }

        /// <summary>
        /// Performs a check to ensure that at least one bay has notches enabled
        /// </summary>
        /// <returns></returns>
        private Boolean CheckAtLeastOneBayHasNotchesEnabled()
        {
            //check notches are on for at least one other fixture in the plan
            Boolean notchesEnabled = false;
            foreach (PlanogramFixtureView fixture in this.Fixtures)
            {
                //check fixture has notches tuned on
                IEnumerable<PlanogramSubComponentView> subComponents = fixture.EnumerateAllSubcomponents();
                PlanogramSubComponentView backboard = subComponents
                    .Where(s => s.Component != null)
                    .FirstOrDefault(s => s.Component.ComponentType == PlanogramComponentType.Backboard);
                if (backboard != null)
                {
                    if (backboard.IsNotchPlacedOnFront)
                    {
                        notchesEnabled = true;
                        break;
                    }
                }
            }
            return notchesEnabled;
        }

        /// <summary>
        /// Called whenever Metrics changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Metrics_ChildChanged(object sender, Csla.Core.ChildChangedEventArgs e)
        {
            if (e.PropertyChangedArgs != null)
            {
                if (e.PropertyChangedArgs.PropertyName == PlanogramPerformanceMetric.SpecialTypeProperty.Name)
                {
                    OnIsMetricRegularSalesUnitChanged();
                }
            }
        }

        /// <summary>
        /// Called whenever the selected fixture changes.
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedFixtureChanged(PlanogramFixtureView newValue)
        {
            if (newValue != null)
            {
                if (this.MultiFixtureView != null)
                {
                    this.MultiFixtureView.UpdateBackboard();
                    this.MultiFixtureView.UpdateBase();
                }
            }
            else
            {
                this.Base = null;
            }
        }

        /// <summary>
        /// Called whenever the value of IsBackboardRequired changes
        /// </summary>
        /// <param name="newValue"></param>
        //private void OnIsBackboardRequiredChanged(Boolean newValue)
        //{
        //    PlanogramComponentView newBackboard = null;

        //    if (this.SelectedFixture != null)
        //    {
        //        newBackboard = this.SelectedFixture.SetBackboard(newValue);
        //    }

        //    //if (this.Backboard != newBackboard)
        //    //{
        //    //    this.Backboard = newBackboard;
        //    //}
        //}

        /// <summary>
        /// Called whenever the value of IsBaseRequired changes
        /// </summary>
        /// <param name="newValue"></param>
        private void OnIsBaseRequiredChanged(Boolean newValue)
        {
            PlanogramComponentView newBase = null;

            if (this.SelectedFixture != null)
            {
                newBase = this.SelectedFixture.SetBase(newValue);
            }

            if (this.Base != newBase)
            {
                this.Base = newBase;
            }
        }

        /// <summary>
        /// Called whenever metrics change
        /// </summary>
        private void OnIsMetricRegularSalesUnitChanged()
        {
            if (this.Planogram == null) return;

            var metric = this.Planogram.Model.Performance.Metrics.FirstOrDefault(m => m.SpecialType == MetricSpecialType.RegularSalesUnits);
            this.IsMetricRegularSalesUnit = (metric != null);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Subscribes/Unsubscribes the plan event handlers.
        /// </summary>
        private void SetPlanEventHandlers(PlanogramView planView, Boolean isSubscribe)
        {
            Debug.Assert(planView != null, "Plan is null");
            if (planView == null) return;

            if (isSubscribe)
            {
                planView.PropertyChanged += Planogram_PropertyChanged;
                planView.ChildChanged += Planogram_ChildChanged;
                planView.PerformanceMetrics.ChildChanged += Metrics_ChildChanged;
                planView.Fixtures.CollectionChanged += Planogram_FixtureCollectionChanged;
            }
            else
            {
                planView.PropertyChanged -= Planogram_PropertyChanged;
                planView.ChildChanged -= Planogram_ChildChanged;
                planView.PerformanceMetrics.ChildChanged -= Metrics_ChildChanged;
                planView.Fixtures.CollectionChanged -= Planogram_FixtureCollectionChanged;
            }
        }

        /// <summary>
        /// Adds a new fixture to the planogram.
        /// </summary>
        /// <param name="fixtureToCopy"></param>
        /// <param name="insertBefore"></param>
        /// <param name="copyAllComponents"></param>
        /// <returns></returns>
        private PlanogramFixtureView AddNewFixture(PlanogramFixtureView fixtureToCopy, Boolean insertBefore)
        {
            PlanogramView plan = _planogram;
            plan.BeginUpdate();

            var newFixture = plan.AddFixtureFromCopy(fixtureToCopy, insertBefore, _copyAllComponents, /*copy positions*/false);

            plan.EndUpdate();
            plan.UpdatePlanogramBounds();
            return newFixture;
        }

        /// <summary>
        /// Displays a warning to the user asking if all components should be snapped to notches.
        /// Update all merchandisable components by snapping to nearest notch if appropriate.
        /// </summary>
        private void CheckAndSnapComponentsToNearestNotch()
        {
            //perform a check to ensure notches are still on for at least 1 fixture
            if (!CheckAtLeastOneBayHasNotchesEnabled()) return;

            //Ask user
            ModalMessage dialog = new ModalMessage
            {
                MessageIcon = ImageResources.Warning_32,
                Header = String.Format(CultureInfo.CurrentCulture,
                    Message.PlanogramProperties_SnapToNotchWarning_Header),
                Description = String.Format(CultureInfo.CurrentCulture,
                    Message.PlanogramProperties_SnapToNotchWarning_Description),
                Button1Content = Message.Generic_Yes,
                Button2Content = Message.Generic_No,
                ButtonCount = 2,
                DefaultButton = ModalMessageButton.Button1,
                Owner = App.MainPageViewModel.AttachedControl,
                WindowStartupLocation = WindowStartupLocation.CenterScreen
            };
            App.ShowWindow(dialog, /*isModal*/true);

            if (dialog.Result == ModalMessageResult.Button1)
            {
                foreach (PlanogramFixtureView fixture in this.Fixtures)
                {
                    //Check the fixture has a backboard ..
                    PlanogramComponentView backboard = fixture.Components
                        .FirstOrDefault(c => c.ComponentType == PlanogramComponentType.Backboard);
                    if (backboard != null)
                    {
                        // and that notches are being used on the backboard, otherwise ignore it
                        if (backboard.SubComponents[0].IsNotchPlacedOnFront)
                        {
                            //snap all components for this fixture to the nearest notch
                            foreach (PlanogramComponentView component in fixture.Components)
                            {
                                if (component.ComponentModel != null)
                                {
                                    //only snap components that are not backboards or bases (or carparks :) )
                                    if (component.ComponentModel.IsMerchandisable)
                                    {
                                        component.SnapToNearestNotch();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Check if the editor is connected to a repository
        /// </summary>
        /// <returns></returns>
        private Boolean CheckRepositoryDetails()
        {
            Boolean connectDetailsSet = false;

            return connectDetailsSet;
        }

        /// <summary>
        /// Retrieve the plan assignment dates : date live and date communicated
        /// </summary>
        private void FetchLocationPlanAssignments()
        {
            if (_planAssignments != null) return;

            LocationPlanAssignmentList planAssignments = null;


            Planogram plan = _planogram.Model;

            //if we are connected to repository and this is 
            // a plan that has come from the repository.
            if (IsRepositoryConnected
                && !plan.IsNew && plan.Id is Int32)
            {
                try
                {
                    planAssignments = LocationPlanAssignmentList.FetchByPlanogramId((Int32)plan.Id);

                    if (planAssignments.Any())
                    {
                        LocationPlanAssignment firstPlan = planAssignments.First();
                        _dateLive = firstPlan.DateLive;
                        _dateCommunicated = firstPlan.DateCommunicated;
                    }
                }
                catch (DataPortalException ex)
                {
                    CommonHelper.RecordException(ex);
                }
            }

            _planAssignments = planAssignments;
        }

        // method that gives a warning if the selected cluster no longer matches the selected location code or cluster scheme
        public void ShowClusterChangeWarning()
        {
            Boolean hasFoundMatch;
            //first check if there's a repository connection and if the clustername is set
            if (!String.IsNullOrEmpty(this.Planogram.ClusterName)
                && IsRepositoryConnected == true)
            {
                if (!String.IsNullOrEmpty(this.Planogram.ClusterSchemeName))
                {
                    // if the clusterscheme is set, check if the name exists in the database
                    _clusterSchemeInfoListViewModel.FetchForCurrentEntity();
                    if (_clusterSchemeInfoListViewModel.Model.Any(p => p.Name == this.Planogram.ClusterSchemeName))
                    {
                        // if the clusterScheme name exists in the database, fetch the details
                        _selectedClusterScheme = ClusterScheme.FetchById(_clusterSchemeInfoListViewModel.Model.Where(p => p.Name == this.Planogram.ClusterSchemeName).FirstOrDefault().Id);

                        // now check if the set clustername matches any cluster name in the selected cluster scheme 
                        if (_selectedClusterScheme.Clusters.Any(p => p.Name == this.Planogram.ClusterName))
                        {
                            hasFoundMatch = true;
                        }
                        else
                        {
                            hasFoundMatch = false;
                        }


                        if (this.Planogram.ClusterName != String.Empty && hasFoundMatch == true)
                        {
                            if (String.IsNullOrEmpty(this.Planogram.LocationCode))
                            {
                                IsShowWarning = false;

                                return;

                            }
                            else
                            {
                                //get the cluster locations for the selected cluster
                                Cluster LocCluster = _selectedClusterScheme.Clusters.FirstOrDefault(p => p.Name == this.Planogram.ClusterName);
                                // Check if any of the location in the selected cluster location matches the set location code
                                foreach (ClusterLocation clusterLocation in LocCluster.Locations)
                                {
                                    if (clusterLocation.Code != this.Planogram.LocationCode)
                                    {
                                        IsShowWarning = true;
                                        TooltipWarningMessage = Message.PlanogramProperties_LocationHasChanged;

                                    }
                                    else
                                    {
                                        IsShowWarning = false;

                                        return;
                                    }
                                }
                            }
                        }

                        else if (this.Planogram.ClusterName != String.Empty && !hasFoundMatch == true)
                        {
                            IsShowWarning = true;
                            TooltipWarningMessage = Message.PlanogramProperties_ClusterSchemeChanged;
                        }

                    }
                    else
                    {
                        // if the cluster scheme name does not exists in the database, then the set cluster will not be found
                        IsShowWarning = true;
                        TooltipWarningMessage = Message.PlanogramProperties_ClusterSchemeChanged;
                    }

                }
                else
                {
                    IsShowWarning = false;
                }
            }
            else
            {
                IsShowWarning = false;
            }
        }

        /// <summary>
        ///     Enumerate in order tuples with all fixture item views and their planogram X Coordinate.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<Tuple<PlanogramFixtureView, Single>> GetFixtureItemsWithPlanX()
        {
            return Fixtures.Select(item => new Tuple<PlanogramFixtureView, Single>(item, item.GetWorldSpaceBounds().X)).OrderBy(tuple => tuple.Item2);
        }

        /// <summary>
        ///     Get the fixture previous to the given one, in plan X coordinate order.
        /// </summary>
        /// <param name="fixtureItemView"></param>
        /// <returns></returns>
        private PlanogramFixtureView GetPreviousFixtureByPlanX(PlanogramFixtureViewModelBase fixtureItemView)
        {
            Single fixturePlanX = fixtureItemView.GetWorldSpaceBounds().X;
            return GetFixtureItemsWithPlanX()
                .Where(tuple => tuple.Item2.LessOrEqualThan(fixturePlanX))
                .Select(tuple => tuple.Item1)
                .LastOrDefault(view => !Equals(view, fixtureItemView));
        }

        /// <summary>
        ///     Get the fixture next to the given one, in plan X coordinate order.
        /// </summary>
        /// <param name="fixtureItemView"></param>
        /// <returns></returns>
        private PlanogramFixtureView GetNextFixtureByPlanX(PlanogramFixtureViewModelBase fixtureItemView)
        {
            Single fixturePlanX = fixtureItemView.GetWorldSpaceBounds().X;
            return GetFixtureItemsWithPlanX()
                .Where(tuple => tuple.Item2.GreaterOrEqualThan(fixturePlanX))
                .Select(tuple => tuple.Item1)
                .FirstOrDefault(view => !Equals(view, fixtureItemView));
        }

        #endregion

        #region Static Helper Methods

        /// <summary>
        ///     Helper method to get the property path for the <see cref="PlanogramPropertiesViewModel" /> property indicated by the
        ///     <paramref name="expression" />.
        /// </summary>
        /// <param name="expression">Lambda expression to access the property.</param>
        /// <returns>
        ///     Returns a <see cref="PropertyPath" /> Object with the path to the property in the
        ///     <paramref name="expression" />.
        /// </returns>
        private static PropertyPath GetPropertyPath(Expression<Func<PlanogramPropertiesViewModel, Object>> expression)
        {
            return WpfHelper.GetPropertyPath(expression);
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    //unsubscribe from the planogam events.
                    SetPlanEventHandlers(_planogram, false);
                }
                base.IsDisposed = true;
            }
        }

        #endregion
        
    }
}
