﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-27589 : A.Silva
//      Created.
// V8-27742 : A.Silva
//      Removed call to check validation warnings when loading for the first time.
// V8-27980 : A.Silva
//      Added IsCorrectedIcons for the warnings list.

#endregion
#region Version History: (CCM v8.1)
// V8-27828 : L.Ineson
//  Added double click handler
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using System.Windows.Controls;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanProperties
{
    /// <summary>
    ///     Interaction logic for PlanogramValidationWarningPanel.xaml
    /// </summary>
    public sealed partial class PlanogramValidationWarningPanel : DockingPanelControl,  IDisposable
    {
        #region Fields

        public static readonly Dictionary<Boolean, ImageSource> IsCorrectedIcons = new Dictionary<Boolean, ImageSource>
        {
            {true, ImageResources.EditorValidation_ItemsOk},
            {false, ImageResources.EditorValidation_ItemsWarning}
        };

        #endregion

        #region Properties

        #region ViewModel

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register(
            "ViewModel", typeof (PlanogramValidationWarningViewModel), typeof (PlanogramValidationWarningPanel),
            new PropertyMetadata(null));

        public PlanogramValidationWarningViewModel ViewModel
        {
            get { return (PlanogramValidationWarningViewModel) GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public PlanogramValidationWarningPanel(PlanogramView plan, PlanItemSelection planItemSelection)
        {
            InitializeComponent();

            ViewModel = new PlanogramValidationWarningViewModel(plan, planItemSelection);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Shows the properties window for the item that the double clicked validation row relates to.
        /// </summary>
        private void xValidationWarningList_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (this.ViewModel == null) return;

            ListBoxItem item = Framework.Controls.Wpf.Helpers.FindVisualAncestor<ListBoxItem>((DependencyObject)e.OriginalSource);
            if (item == null) return;

            ValidationWarningRow row = item.Content as ValidationWarningRow;
            if (row == null) return;

            //show the selected item properties
            MainPageCommands.ShowSelectedItemProperties.Execute();
        }

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            if (!_isDisposed)
            {
                IDisposable disView = this.ViewModel;
                this.ViewModel = null;
                if (disView != null) disView.Dispose();

                GC.SuppressFinalize(this);
                _isDisposed = true;
            }
        }

        #endregion
    }
}