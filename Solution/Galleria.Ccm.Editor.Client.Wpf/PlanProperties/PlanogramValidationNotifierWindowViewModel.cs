﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.20)
// V8-29349 : J.Pickup
//  Created.
// V8-31191 : J.Pickup
//  Added Undo Command, and renamed buttons slightly.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Collections;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanProperties
{
    public sealed class PlanogramValidationNotifierWindowViewModel : ViewModelAttachedControlObject<PlanogramValidationNotifierWindow>
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="planogram">the current planogram</param>
        /// <remarks>Uses App.ViewState.Settings</remarks>
        public PlanogramValidationNotifierWindowViewModel(List<ValidationWarningRow> validationWarningRows)
        {
            ValidationWarningRows.AddRange(validationWarningRows.Where(vwr => vwr.ShowInstantNotificationWarningToUser));
        }

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath ValidationWarningRowsProperty = WpfHelper.GetPropertyPath<PlanogramValidationNotifierWindowViewModel>(o => o.ValidationWarningRows);
        public static readonly PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<PlanogramValidationNotifierWindowViewModel>(o => o.CloseCommand);
        public static readonly PropertyPath UndoAndCloseCommandProperty = WpfHelper.GetPropertyPath<PlanogramValidationNotifierWindowViewModel>(o => o.UndoAndCloseCommand);

        #endregion

        #region Fields

        private BulkObservableCollection<ValidationWarningRow> _validationWarningRows = new BulkObservableCollection<ValidationWarningRow>();

        #endregion

        #region Properties

        /// <summary>
        /// Returns the validation warnings to be displayed.
        /// </summary>
        public BulkObservableCollection<ValidationWarningRow> ValidationWarningRows
        {
            get
            {
                return _validationWarningRows;
            }
            set
            {
                _validationWarningRows = value;
                OnPropertyChanged(ValidationWarningRowsProperty);
            }
        }

        #endregion 

        #region Commands

        #region Close

        private RelayCommand _closeCommand;

        /// <summary>
        /// Closes window. No special logic.
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(
                        o => CloseCommand_Executed())
                    {
                        FriendlyName = Message.PlanogramNotifierWindow_Close
                    };

                    base.ViewModelCommands.Add(_closeCommand);
                }

                return _closeCommand;
            }
        }

        //Basic close. 
        private void CloseCommand_Executed()
        {
            if (this.AttachedControl != null) this.AttachedControl.Close();
        }

        #endregion

        #region UndoAndClose

        private RelayCommand _undoAndCloseCommand;

        /// <summary>
        /// Closes window. Attempts an undo.
        /// </summary>
        public RelayCommand UndoAndCloseCommand
        {
            get
            {
                if (_undoAndCloseCommand == null)
                {
                    _undoAndCloseCommand = new RelayCommand(
                        o => UndoAndCloseCommand_Executed())
                    {
                        FriendlyName = Message.PlanogramNotifierWindow_UndoAndClose
                    };

                    base.ViewModelCommands.Add(_undoAndCloseCommand);
                }

                return _undoAndCloseCommand;
            }
        }

        //Basic close. 
        private void UndoAndCloseCommand_Executed()
        {
            App.MainPageViewModel.SelectedPlanDocument.Planogram.Undo();
               

            if (this.AttachedControl != null) this.AttachedControl.Close();
        }

        #endregion

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
