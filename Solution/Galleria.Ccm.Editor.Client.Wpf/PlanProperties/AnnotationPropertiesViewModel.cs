﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26148 : L.Ineson
//  Created.
#endregion

#endregion

using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Helpers;
using System.Collections.ObjectModel;
using System.Windows.Media;


namespace Galleria.Ccm.Editor.Client.Wpf.PlanProperties
{
    /// <summary>
    /// ViewModel controller for AnnotationPropertiesWindow
    /// </summary>
    public sealed class AnnotationPropertiesViewModel : ViewModelAttachedControlObject<AnnotationPropertiesWindow>
    {

        #region Fields

        private Boolean? _dialogResult;
        private readonly PlanogramView _plan;
        private readonly PlanogramAnnotationMultiView _editorContext;
        private readonly ReadOnlyCollection<FontFamily> _availableFonts;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath EditorContextProperty = WpfHelper.GetPropertyPath<AnnotationPropertiesViewModel>(p => p.EditorContext);
        public static readonly PropertyPath AvailableFontsProperty = WpfHelper.GetPropertyPath<AnnotationPropertiesViewModel>(p => p.AvailableFonts);

        //Commands
        public static readonly PropertyPath OKCommandProperty = WpfHelper.GetPropertyPath<AnnotationPropertiesViewModel>(p => p.OKCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<AnnotationPropertiesViewModel>(p => p.CancelCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;

                if (this.AttachedControl != null
                    && !this.AttachedControl.IsClosing)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }

        /// <summary>
        /// Returns the editor context
        /// </summary>
        public PlanogramAnnotationMultiView EditorContext
        {
            get { return _editorContext; }
        }

        public DisplayUnitOfMeasureCollection DisplayUOMs
        {
            get
            {
                if (_plan != null)
                {
                    return DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(_plan.Model);
                }
                return DisplayUnitOfMeasureCollection.Empty;
            }
        }

        public ReadOnlyCollection<FontFamily> AvailableFonts
        {
            get { return _availableFonts; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="plan">the parent planogram</param>
        /// <param name="selection">the </param>
        public AnnotationPropertiesViewModel(PlanogramAnnotationMultiView selection)
        {
            _plan = selection.Items.First().Planogram;
            _editorContext = selection;

            _availableFonts = Fonts.SystemFontFamilies.OrderBy(f => f.ToString()).ToList().AsReadOnly();

            //Start a multi edit
            selection.IsLoggingUndoableActions = false;
            _plan.BeginUndoableAction(Message.PlanItemProperties_UndoableActionName);

        }

        #endregion

        #region Commands

        #region OK Command

        private RelayCommand _okCommand;

        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OK_Executed())
                    {
                        FriendlyName = Message.Generic_OK,
                        InputGestureKey = Key.Enter
                    };
                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }

        private void OK_Executed()
        {
            if (this.AttachedControl != null)
            {
                LocalHelper.UpdateFocusedBindingSource(this.AttachedControl);
            }


            //complete the plan edit
            _plan.EndUndoableAction();
            _editorContext.IsLoggingUndoableActions = true;

            this.DialogResult = true;
        }

        #endregion

        #region Cancel Command

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels any changes made.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel,
                        InputGestureKey = Key.Escape
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            //cancel the ongoing edit.
            _plan.CancelUndoableAction(/*undoLoggedChanges*/true);
            _editorContext.IsLoggingUndoableActions = true;

            this.DialogResult = false;
        }

        #endregion

        #endregion


        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {

                base.IsDisposed = true;
            }
        }

        #endregion


    }
}
