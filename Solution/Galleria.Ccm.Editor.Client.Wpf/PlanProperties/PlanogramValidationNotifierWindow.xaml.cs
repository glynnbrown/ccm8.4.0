﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.20)
// V8-29349 : J.Pickup
//  Created.
#endregion

#endregion

using System;
using System.Windows;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Threading;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanProperties
{
    /// <summary>
    /// Interaction logic for PlanogramValidationNotifierWindow.xaml
    /// </summary>
    public sealed partial class PlanogramValidationNotifierWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanogramValidationNotifierWindowViewModel), typeof(PlanogramValidationNotifierWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PlanogramValidationNotifierWindow senderControl = (PlanogramValidationNotifierWindow)obj;

            if (e.OldValue != null)
            {
                PlanogramValidationNotifierWindowViewModel oldModel = (PlanogramValidationNotifierWindowViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                PlanogramValidationNotifierWindowViewModel newModel = (PlanogramValidationNotifierWindowViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        /// <summary>
        /// Gets the viewmodel controller for this screen.
        /// </summary>
        public PlanogramValidationNotifierWindowViewModel ViewModel
        {
            get { return (PlanogramValidationNotifierWindowViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion 

        #region Constructor

        public PlanogramValidationNotifierWindow(PlanogramValidationNotifierWindowViewModel viewModel)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            this.ViewModel = viewModel;

            this.Loaded += PlanogramValidationNotifierWindow_Loaded;
        }

        /// <summary>
        /// Carries out initial load actions.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanogramValidationNotifierWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= PlanogramValidationNotifierWindow_Loaded;

            //We want to make it possible to close this window easily because its repetitive and incredibly annoying.
            this.KeyDown += new KeyEventHandler(PlanogramValidationNotifierWindow_KeyDown);

            Dispatcher.BeginInvoke(
                (Action)(() => Mouse.OverrideCursor = null), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        /// <summary>
        /// Make it as easy as possible to close this annoying dialog. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void PlanogramValidationNotifierWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.Close();
            }
        }

        #endregion

        #region Interface

        /// <summary>
        /// Disposes of the viewmodel when the window has closed.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;

                    this.KeyDown -= new KeyEventHandler(PlanogramValidationNotifierWindow_KeyDown);

                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion
    }
}
