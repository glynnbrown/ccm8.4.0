﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26148 : L.Ineson
//  Created.
#endregion

#region Version History : CCM 801
// V8-28760 : A.Kuszyk
//  Amended SetComponentImage_Executed to use session image location.
// V8-26246 : I.George
//  Added CancelComponentImage Command
#endregion

#region Version History : CCM 8.1.1
// V8-29406 : N.Haywood
//  Added file type validation for OpenFileDialog
#endregion

#region Version History : CCM 8.2.0
// V8-31061 : A.Probyn
//  set SupressPlanUpdatesDuringEdit to the appropriate value when opened/closed.
#endregion

#endregion

using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using Microsoft.Win32;
using Galleria.Ccm.Common.Wpf.Helpers;
using System.Collections.Generic;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanProperties
{
    /// <summary>
    /// ViewModel controller for ComponentPropertiesWindow
    /// </summary>
    public sealed class ComponentPropertiesViewModel : ViewModelAttachedControlObject<ComponentPropertiesWindow>
    {

        #region Fields

        private Boolean? _dialogResult;
        private readonly PlanogramView _plan;
        private readonly PlanItemSelection _editorContext;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath ComponentContextProperty = WpfHelper.GetPropertyPath<ComponentPropertiesViewModel>(p => p.ComponentContext);
        public static readonly PropertyPath SubComponentContextProperty = WpfHelper.GetPropertyPath<ComponentPropertiesViewModel>(p => p.SubComponentContext);
        public static readonly PropertyPath AssemblyContextProperty = WpfHelper.GetPropertyPath<ComponentPropertiesViewModel>(p => p.AssemblyContext);
        public static readonly PropertyPath ShowAssemblyPropertiesProperty = WpfHelper.GetPropertyPath<ComponentPropertiesViewModel>(p => p.ShowAssemblyProperties);

        //Commands
        public static readonly PropertyPath SetComponentImageCommandProperty = WpfHelper.GetPropertyPath<ComponentPropertiesViewModel>(p => p.SetComponentImageCommand);
        public static readonly PropertyPath OKCommandProperty = WpfHelper.GetPropertyPath<ComponentPropertiesViewModel>(p => p.OKCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<ComponentPropertiesViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath CancelComponentImageCommandProperty = WpfHelper.GetPropertyPath<ComponentPropertiesViewModel>(p => p.CancelComponentImageCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;

                if (this.AttachedControl != null
                    && !this.AttachedControl.IsClosing)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }

        public PlanItemSelection EditorContext
        {
            get { return _editorContext; }
        }

        /// <summary>
        /// Returns the editor context
        /// </summary>
        public PlanogramComponentMultiView ComponentContext
        {
            get { return _editorContext.ComponentView; }
        }


        /// <summary>
        /// Returns the editor context
        /// </summary>
        public PlanogramSubComponentMultiView SubComponentContext
        {
            get { return _editorContext.ChildSubComponentView; }
        }

        /// <summary>
        /// Returns the editor context
        /// </summary>
        public PlanogramAssemblyMultiView AssemblyContext
        {
            get { return _editorContext.AssemblyView; }
        }

        /// <summary>
        /// Returns true if position properties should be shown
        /// </summary>
        public Boolean ShowAssemblyProperties
        {
            get { return _editorContext.AssemblyView != null; }
        }


        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="plan">the parent planogram</param>
        /// <param name="selection">the </param>
        public ComponentPropertiesViewModel(PlanogramView plan, PlanItemSelection selection)
        {
            _plan = plan;
            _editorContext = selection;

            //Start a multi edit
            selection.IsLoggingUndoableActions = false;
            _plan.BeginUndoableAction(Message.PlanItemProperties_UndoableActionName);

        }

        #endregion

        #region Commands

        #region SetComponentImage Command

        private RelayCommand _setComponentImageCommand;

        /// <summary>
        /// Shows the image selector dialog to select a component image.
        /// </summary>
        public RelayCommand SetComponentImageCommand
        {
            get
            {
                if (_setComponentImageCommand == null)
                {
                    _setComponentImageCommand = new RelayCommand(
                        p => SetComponentImage_Executed())
                        {
                            FriendlyName = Message.Generic_Ellipsis
                        };
                    base.ViewModelCommands.Add(_setComponentImageCommand);
                }
                return _setComponentImageCommand;
            }

        }

        private void SetComponentImage_Executed()
        {
            if (_plan.Images != null)
            {
                PlanogramImage img = null;
                if (this.AttachedControl != null)
                {
                    //Show the open file dialog.
                    OpenFileDialog dlg = new OpenFileDialog();
                    if (Directory.Exists(App.ViewState.SessionImageLocation))
                    {
                        dlg.InitialDirectory = App.ViewState.SessionImageLocation;
                    }
                    dlg.Filter = Message.PlanItemProperties_SelectProductImageFilter;
                    dlg.Multiselect = false;
                    dlg.CheckFileExists = true;

                    if (dlg.ShowDialog(this.AttachedControl) == true)
                    {
                        List<String> extensions = new List<String>(new String[]{".png", ".bmp", ".jpg", ".jpeg"});

                        if (!extensions.Any(s => String.Equals(Path.GetExtension(dlg.FileName), s, StringComparison.OrdinalIgnoreCase)))
                        {
                            ModalMessage msg = new ModalMessage
                                {
                                    Title = Message.General_WrongFileType_Title,
                                    Header = Message.General_WrongFileType_Title,
                                    MessageIcon = ImageResources.Warning_32,
                                    Description = Message.General_WrongFileType_Description,
                                    ButtonCount = 1,
                                    Button1Content = Message.Generic_OK,
                                    DefaultButton = ModalMessageButton.Button1
                                };
                            App.ShowWindow(msg, true);
                        }
                        else
                        {
                            String fileName = dlg.FileName;
                            img = _plan.AddPlanogramImage(File.ReadAllBytes(fileName), fileName, Path.GetFileNameWithoutExtension(fileName));
                            App.ViewState.SessionImageLocation = Path.GetDirectoryName(dlg.FileName);
                        }
                    }
                }

                if (img != null)
                {
                    this.ComponentContext.Image = img;
                }
            }
        }

        #endregion

        #region CancelComponentImageCommand

        private RelayCommand _cancelComponentImageCommand;
        /// <summary>
        /// Cancels any image set on a component
        /// </summary>
        public RelayCommand CancelComponentImageCommand
        {
            get 
            {
                if (_cancelComponentImageCommand == null)
                {
                    _cancelComponentImageCommand = new RelayCommand(
                        p => CancelComponentImageCommand_Executed(),
                        p => CancelComponentImageCommand_CanExecute()
                      ) 
                    {
                        SmallIcon = ImageResources.Delete_16,
                    };
                    base.ViewModelCommands.Add(_cancelComponentImageCommand);
                }
                return _cancelComponentImageCommand;
            }
        }

        private Boolean CancelComponentImageCommand_CanExecute()
        {
            if (this.ComponentContext == null)
            {
                this.CancelComponentImageCommand.DisabledReason = Message.DisabledReason_NoImageSet;
                return false;
            }

            if (!this.ComponentContext.Items.Any())
            {
                this.CancelComponentImageCommand.DisabledReason = Message.DisabledReason_NoImageSet;
                return false;
            }

            if(this.ComponentContext.Image == null)
            {
                this.CancelComponentImageCommand.DisabledReason = Message.DisabledReason_NoImageSet;
                return false;
            }
            return true;
        }

        public void CancelComponentImageCommand_Executed()
        {
            this.ComponentContext.Items.First().SubComponentImage = null;
        }

        #endregion

        #region OK Command

        private RelayCommand _okCommand;

        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OK_Executed(),
                        p => OK_CanExecute())
                    {
                        FriendlyName = Message.Generic_OK,
                        InputGestureKey = Key.Enter
                    };
                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }

        private Boolean OK_CanExecute()
        {
            if (!_plan.Model.IsValid)
            {
                return false;
            }
            return true;
        }

        private void OK_Executed()
        {
            if (this.AttachedControl != null)
            {
                LocalHelper.UpdateFocusedBindingSource(this.AttachedControl);
            }


            //complete the plan edit
            _plan.EndUndoableAction();
            _editorContext.IsLoggingUndoableActions = true;

            this.DialogResult = true;
        }

        #endregion

        #region Cancel Command

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels any changes made.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel,
                        InputGestureKey = Key.Escape
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            //cancel any changes.
            _plan.CancelUndoableAction(/*undoLoggedChanges*/true);
            _editorContext.IsLoggingUndoableActions = true;

            this.DialogResult = false;
        }

        #endregion

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {

                base.IsDisposed = true;
            }
        }

        #endregion


    }
}
