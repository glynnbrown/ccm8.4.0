﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Ineson ~ Created.
#endregion

#region Version History: (CCM 8.1.1)
// V8-30403 : A.Probyn
//  Updated so that the properties refresh when the settings model changes
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Windows;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf.PropertyEditor;
using Galleria.Framework.Model;


namespace Galleria.Ccm.Editor.Client.Wpf.PlanProperties
{
    /// <summary>
    /// Interaction logic for PlanItemPropertiesPanel.xaml
    /// </summary>
    public sealed partial class PlanItemPropertiesPanel : DockingPanelControl, IDisposable
    {
        #region Fields
        private Boolean _areDefinitionsOutdated;
        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanItemSelection), typeof(PlanItemPropertiesPanel),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PlanItemPropertiesPanel senderControl = (PlanItemPropertiesPanel)obj;

            if (e.OldValue != null)
            {
                PlanItemSelection oldModel = (PlanItemSelection)e.OldValue;
                oldModel.SelectionChanged -= senderControl.ViewModel_SelectionChanged;
            }

            if (e.NewValue != null)
            {
                PlanItemSelection newModel = (PlanItemSelection)e.NewValue;
                newModel.SelectionChanged += senderControl.ViewModel_SelectionChanged;
            }

            senderControl.xPropertyGrid.SourceObject = e.NewValue;
            senderControl.UpdatePropertyDefinitions();
        }

       

      

        /// <summary>
        /// Gets/Sets the context.
        /// </summary>
        public PlanItemSelection ViewModel
        {
            get { return (PlanItemSelection)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public PlanItemPropertiesPanel()
        {
            InitializeComponent();

            //Attach to model changed event for settings
            App.ViewState.Settings.ModelChanged += Settings_ModelChanged;

            this.IsVisibleChanged += OnIsVisibleChanged;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when the is visible flag changes.
        /// </summary>
        private void OnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            //update if the definitions are outdated.
            if (this.IsVisible && _areDefinitionsOutdated)
            {
                UpdatePropertyDefinitions();
            }
        }

        /// <summary>
        /// Called whenever the selection changes
        /// </summary>
        private void ViewModel_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            UpdatePropertyDefinitions();
        }

        /// <summary>
        /// Called on property grid right click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xPropertyGrid_MouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (this.ViewModel.Count > 0
                && (this.ViewModel.HasPositions || this.ViewModel.HasComponents))
            {
                Fluent.ContextMenu contextMenu = new Fluent.ContextMenu();

                contextMenu.Items.Add(
                    new Fluent.MenuItem()
                    {
                        Command = MainPageCommands.Options,
                        Header = Message.PlanItemPropertiesPanel_Fields,
                        Icon = MainPageCommands.Options.SmallIcon,
                        CommandParameter = (this.ViewModel.HasPositions) ?
                            OptionsViewModel.OptionsWindowTab.PositionProperties :
                            OptionsViewModel.OptionsWindowTab.ComponentProperties
                    });

                contextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.MousePoint;
                contextMenu.IsOpen = true;
            }
        }

        /// <summary>
        /// Called whenever the settings model is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Settings_ModelChanged(object sender, Framework.ViewModel.ViewStateObjectModelChangedEventArgs<Model.UserEditorSettings> e)
        {
            if (e.NewModel != null)
            {
                UpdatePropertyDefinitions();
            }
        }

        /// <summary>
        /// Called whenver the plan items selection has changed.
        /// </summary>
        private void ViewModel_SelectionChanged(object sender, EventArgs e)
        {
            UpdatePropertyDefinitions();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the property definitions on the grid.
        /// </summary>
        private void UpdatePropertyDefinitions()
        {
            //only update if this panel is actually visible,
            // otherwise just flag the request.
            if (this.IsVisible)
            {
                PlanItemSelection propContext = this.ViewModel;
                PropertyEditor propGrid = this.xPropertyGrid;

                if (propGrid.PropertyDescriptions.Count > 0)
                {
                    propGrid.PropertyDescriptions.Clear();
                }

                //add in property definitions
                if (propContext != null)
                {
                    propGrid.PropertyDescriptions.AddRange(propContext.GetPropertyDefinitions(App.ViewState.Settings.Model));
                }

                _areDefinitionsOutdated = false;
            }
            else
            {
                _areDefinitionsOutdated = true;
            }
        }

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            if (!_isDisposed)
            {
                this.ViewModel = null;

                App.ViewState.Settings.ModelChanged -= Settings_ModelChanged;
                this.IsVisibleChanged -= OnIsVisibleChanged;

                GC.SuppressFinalize(this);
                _isDisposed = true;
            }
        }

        #endregion

    }
}
