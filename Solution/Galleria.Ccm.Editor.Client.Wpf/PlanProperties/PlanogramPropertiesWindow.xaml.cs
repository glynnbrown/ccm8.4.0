﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
// V8-26338 : A.Kuszyk
//  Added OnPerformanceMetricDoubleClick().
#endregion

#region Version History: (CCM 8.02)
// V8-29004 : L.Luong
//  Added Context menu so that user can move fixture left or right
#endregion

#region Version History: (CCM 8.10)
//  V8-29662 : M.Brumby
//      Changed ordering of fixtures to take into account
//      the numbering strategy.
//  V8-30072 : L.Luong
//      Added validation for MinShelfLife
#endregion

#region Version History: (CCM 8.1.1)
//  V8-29290 : I.George
//      Added warning if cluster doesn't match location code or clusterscheme
#endregion

#region Version History : CCM 820
// V8-30791 : D.Pleasance
//  Added Handler for ColorPickerBox.SelectedColorChangedEvent, to track user colors
#endregion

#region Version History : CCM 830
// V8-31833 : L.Ineson
//  Added new duplicate bay commands to context menu.
// V8-29527 : M.Shelley
//  Added support for multiple bay editing
// V8-32097 : M.Shelley
//  Allow the use of the Shift key to select a range of fixtures
//  Prevent the user from de-selecting all bays
// V8-32513 : M.Brumby
//  fix for successive bay split
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanProperties
{
    /// <summary>
    /// Interaction logic for PlanogramPropertiesWindow.xaml
    /// </summary>
    public sealed partial class PlanogramPropertiesWindow : ExtendedRibbonWindow
    {
        #region Fields

        private Boolean _handleEvents = true;
        private PlanogramPropertiesBayVisual _lastSelectedVisual = null;

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanogramPropertiesViewModel), typeof(PlanogramPropertiesWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PlanogramPropertiesWindow senderControl = (PlanogramPropertiesWindow)obj;

            if (e.OldValue != null)
            {
                PlanogramPropertiesViewModel oldModel = (PlanogramPropertiesViewModel)e.OldValue;
                oldModel.AttachedControl = null;

                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
                oldModel.Fixtures.BulkCollectionChanged -= senderControl.ViewModel_FixturesBulkCollectionChanged;
                oldModel.FixtureSizeChanged -= senderControl.ViewModel_FixtureSizeChanged;
            }

            if (e.NewValue != null)
            {
                PlanogramPropertiesViewModel newModel = (PlanogramPropertiesViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
                newModel.Fixtures.BulkCollectionChanged += senderControl.ViewModel_FixturesBulkCollectionChanged;
                newModel.FixtureSizeChanged += senderControl.ViewModel_FixtureSizeChanged;
            }

            senderControl.UpdateFixtureStack();
        }

        /// <summary>
        /// Gets the viewmodel controller for this screen.
        /// </summary>
        public PlanogramPropertiesViewModel ViewModel
        {
            get { return (PlanogramPropertiesViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Recent Colors

        public static readonly DependencyProperty RecentColorsProperty = 
            DependencyProperty.Register("RecentColors", typeof(ReadOnlyBulkObservableCollection<ColorItem>), 
            typeof(PlanogramPropertiesWindow));

        public ReadOnlyBulkObservableCollection<ColorItem> RecentColors
        {
            get{return (ReadOnlyBulkObservableCollection<ColorItem>)GetValue(RecentColorsProperty);}
            private set {SetValue(RecentColorsProperty, value);}
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="plan"></param>
        public PlanogramPropertiesWindow(PlanogramPropertiesViewModel viewModel)
        {
            this.AddHandler(PlanogramPropertiesBayVisual.SelectedEvent, (RoutedEventHandler)PlanogramPropertiesFixtureControl_Selected);
            this.AddHandler(PlanogramPropertiesBayVisual.DeselectedEvent, (RoutedEventHandler)PlanogramPropertiesFixtureControl_Deselected);

            this.AddHandler(ColorPickerBox.SelectedColorChangedEvent, (RoutedEventHandler)OnSelectedColorChanged);

            this.RecentColors = App.ViewState.RecentColors;

            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //set the help file key for this window.
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.PlanogramInfo);

            this.ViewModel = viewModel;

            this.Loaded += PlanogramPropertiesWindow_Loaded;
        }
                
        /// <summary>
        /// Carries out initial load actions.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanogramPropertiesWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= PlanogramPropertiesWindow_Loaded;

            //this.UpdateFixtureStack();

            Dispatcher.BeginInvoke(
                (Action)(() => Mouse.OverrideCursor = null), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on the viewmodel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == PlanogramPropertiesViewModel.SelectedFixtureProperty.Path)
            {
                if (this.ViewModel.BayViews != null)
                {
                    foreach (PlanogramPropertiesBayVisual fixtureControl in this.ViewModel.BayViews)
                    {
                        fixtureControl.IsSelected = (fixtureControl.Fixture == this.ViewModel.SelectedFixture);
                    }
                }
            }
        }

        /// <summary>
        /// Called whenever the fixtures collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_FixturesBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            UpdateFixtureStack();
        }

        /// <summary>
        /// Called whenever a fixture size changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_FixtureSizeChanged(object sender, EventArgs<PlanogramFixtureView> e)
        {
            if (this.ViewModel.BayViews != null)
            {
                List<PlanogramPropertiesBayVisual> fcList = this.ViewModel.BayViews.ToList();
                this.ViewModel.BayViews.Clear();
                IEnumerable<PlanogramPropertiesBayVisual> sequencedFixtures = null;

                if (this.ViewModel.Planogram.RenumberingStrategy.BayComponentXRenumberStrategy == Framework.Planograms.Model.PlanogramRenumberingStrategyXRenumberType.LeftToRight)
                {
                    sequencedFixtures = fcList.OrderBy(v => v.Fixture.BaySequenceNumber);
                }
                else
                {
                    sequencedFixtures = fcList.OrderByDescending(v => v.Fixture.BaySequenceNumber);
                }
                foreach (PlanogramPropertiesBayVisual fixtureControl in sequencedFixtures)
                {
                    this.ViewModel.BayViews.Add(fixtureControl);
                    fixtureControl.UpdateSize();
                }
            }
        }

        private Boolean ItemInRange(PlanogramFixtureView selectedFixture, PlanogramPropertiesBayVisual lastSelectedView, PlanogramFixtureView currentFixture)
        {
            if (selectedFixture == null || lastSelectedView == null || currentFixture == null) return false;

            var lastSelectedFixture = lastSelectedView.Fixture;
            if (lastSelectedFixture == null) return false;

            // Check if the selected fixture is right of the last selected fixture
            Int16 startBay;
            Int16 endBay;
            if (selectedFixture.BaySequenceNumber > lastSelectedFixture.BaySequenceNumber)
            {
                startBay = lastSelectedFixture.BaySequenceNumber;
                endBay = selectedFixture.BaySequenceNumber;
            }
            else
            {
                startBay = selectedFixture.BaySequenceNumber;
                endBay = lastSelectedFixture.BaySequenceNumber;
            }

            return (currentFixture.BaySequenceNumber >= startBay && currentFixture.BaySequenceNumber <= endBay);
        }

        /// <summary>
        /// Called whenever a fixture control is selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanogramPropertiesFixtureControl_Selected(object sender, RoutedEventArgs e)
        {
            PlanogramPropertiesBayVisual senderControl = (PlanogramPropertiesBayVisual)e.OriginalSource;

            if (this.ViewModel == null) return;

            // Reset the selected status of any existing selections
            var bayList = this.ViewModel.SelectedBayViews.ToList();

            // Check if the user has pressed the Control key (either left or right) when selecting fixtures
            // If no control key is pressed, clear any existing selections 
            // If the control key is pressed then check if the fixture is already selected - if so then remove it, otherwise add it to the list

            var selectedFixture = senderControl.Fixture;
            if (Keyboard.Modifiers == ModifierKeys.Control)
            {
                if (!ViewModel.SelectedBayViews.Contains(senderControl))
                {
                    this.ViewModel.SelectedBayViews.Add(senderControl);
                    selectedFixture.IsSelected = true;
                }
                _lastSelectedVisual = senderControl;
            }
            else if (Keyboard.Modifiers == ModifierKeys.Shift && _lastSelectedVisual != null)
            {
                // Don't let the selection changes within this block change the values until
                // all the procssing has finished
                if (_handleEvents)
                {
                    // Code for Shift + Left Click behaviour:
                    //  When Shift is held with the left mouse button the selection range is changed to all items 
                    //  between the currently selected item and the last previously selected item (whether this
                    //  was selected with a normal left click or a Control + left click

                    var lastSaved = _lastSelectedVisual;
                    var bayVisualList = this.ViewModel.BayViews;
                    _handleEvents = false;

                    foreach (PlanogramPropertiesBayVisual bayVisualItem in bayVisualList)
                    {
                        Boolean isInRange = ItemInRange(selectedFixture, lastSaved, bayVisualItem.Fixture);

                        if (isInRange)
                        {
                            if (!ViewModel.SelectedBayViews.Contains(bayVisualItem))
                            {
                                this.ViewModel.SelectedBayViews.Add(bayVisualItem);
                            }
                        }
                        else
                        {
                            if (ViewModel.SelectedBayViews.Contains(bayVisualItem))
                            {
                                this.ViewModel.SelectedBayViews.Remove(bayVisualItem);
                            }
                        }
                    }

                    foreach (PlanogramPropertiesBayVisual bayVisualItem in bayVisualList)
                    {
                        var isInList = this.ViewModel.SelectedBayViews.Contains(bayVisualItem);

                        bayVisualItem.IsSelected = isInList;
                        bayVisualItem.Fixture.IsSelected = isInList;
                    }

                    _lastSelectedVisual = senderControl;
                    _handleEvents = true;
                }
            }
            else
            {
                foreach (PlanogramPropertiesBayVisual bayVisualItem in bayList)
                {
                    if (bayVisualItem == senderControl) continue;

                    this.ViewModel.SelectedBayViews.Remove(bayVisualItem);
                    bayVisualItem.IsSelected = false;
                    if (bayVisualItem.Fixture != null)
                    {
                        bayVisualItem.Fixture.IsSelected = false;
                    }
                }

                // And add the newly selected item
                if (!ViewModel.SelectedBayViews.Contains(senderControl))
                {
                    this.ViewModel.SelectedBayViews.Add(senderControl);
                    senderControl.Fixture.IsSelected = true;
                }

                _lastSelectedVisual = senderControl;
            }
        }

        private void PlanogramPropertiesFixtureControl_Deselected(object sender, RoutedEventArgs e)
        {
            PlanogramPropertiesBayVisual senderControl = (PlanogramPropertiesBayVisual)e.OriginalSource;

            if (this.ViewModel == null) return;

            var selectedFixture = senderControl.Fixture;

            // Check that if there is only one bay selected that this is not de-selected
            if (this.ViewModel.SelectedBayViews.Count == 1 && senderControl == this.ViewModel.SelectedBayViews.First())
            {
                // Re-select the bay as we do not want to de-select all the bays
                senderControl.IsSelected = true;
                senderControl.Fixture.IsSelected = true;
            }
            else if (this.ViewModel.SelectedBayViews.Contains(senderControl))
            {
                this.ViewModel.SelectedBayViews.Remove(senderControl);
                senderControl.Fixture.IsSelected = false;
            }

            _lastSelectedVisual = senderControl;
        }

        private void OnPerformanceMetricDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            var command = ViewModel.ViewMetricCommand;
            if (command.CanExecute()) command.Execute();
        }

        /// <summary>
        /// Called when a preview mouse right button down on the viewer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xFixturesStack_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            PlanogramPropertiesBayVisual senderControl = ((DependencyObject)e.OriginalSource).FindVisualAncestor<PlanogramPropertiesBayVisual>();
            
            if (this.ViewModel != null)
            {
                this.ViewModel.SelectedFixture = senderControl.Fixture;
                
                if (Keyboard.Modifiers == ModifierKeys.None)
                {
                    ShowContextMenu(this.ViewModel.SelectedFixture);
                }

                e.Handled = true;
            }
        }

        /// <summary>
        /// tracks color changes to keep recent user colors
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSelectedColorChanged(object sender, RoutedEventArgs e)
        {
            var colorPicker = e.OriginalSource as ColorPickerBox;
            if (colorPicker != null)
            {
                App.ViewState.ApplyRecentColor(colorPicker.SelectedColor);
            }
        }

        #endregion

        #region Methods

        public void UpdateFixtureStack()
        {
            if (this.ViewModel == null) return;

            ClearOldVisuals();

            Double availableHeight = this.xFixturesStack.Height;
            IEnumerable<PlanogramFixtureView> sequencedFixtures = null;

            if (this.ViewModel != null &&
                this.ViewModel.Planogram != null &&
                this.ViewModel.Planogram.RenumberingStrategy != null &&                
                this.ViewModel.Planogram.RenumberingStrategy.BayComponentXRenumberStrategy == Framework.Planograms.Model.PlanogramRenumberingStrategyXRenumberType.RightToLeft)
            {
                sequencedFixtures = this.ViewModel.Fixtures.OrderByDescending(v => v.BaySequenceNumber);
            }
            else
            {
                sequencedFixtures = this.ViewModel.Fixtures.OrderBy(v => v.BaySequenceNumber);
            }
            foreach (PlanogramFixtureView fixture in sequencedFixtures)
            {
                PlanogramPropertiesBayVisual fixtureControl =
                    new PlanogramPropertiesBayVisual(fixture, availableHeight);

                // ToDo: Sort this to handle multiply selected bays
                if (fixtureControl.Fixture != null && 
                    fixtureControl.Fixture == this.ViewModel.SelectedFixture)
                {
                    fixtureControl.IsSelected = true;

                    if (!this.ViewModel.SelectedBayViews.Contains(fixtureControl))
                    {
                        this.ViewModel.SelectedBayViews.Add(fixtureControl);
                        fixtureControl.Fixture.IsSelected = true;
                        _lastSelectedVisual = fixtureControl;
                    }
                }
                if (!this.ViewModel.BayViews.Contains(fixtureControl))
                {
                    this.ViewModel.BayViews.Add(fixtureControl);
                }
            //    this.xFixturesStack.Children.Add(fixtureControl);
            }
        }

        private void ClearOldVisuals()
        {
            if ( this.ViewModel == null) return;

            //clear down the old visuals
            List<PlanogramPropertiesBayVisual> oldVisuals = this.ViewModel.BayViews.ToList();
           // this.xFixturesStack.Children.Clear();
            this.ViewModel.BayViews.Clear();
            foreach (PlanogramPropertiesBayVisual old in oldVisuals)
            {
                old.Dispose();
            }
        }

        /// <summary>
        /// Shows the context menu for the given fixture package view
        /// </summary>
        /// <param name="fixturePackageView"></param>
        private void ShowContextMenu(PlanogramFixtureView fixtureView)
        {
            Fluent.ContextMenu contextMenu = new Fluent.ContextMenu();
            RelayCommand cmd;

            cmd = this.ViewModel.MoveFixtureLeftCommand;
            contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });

            cmd = this.ViewModel.MoveFixtureRightCommand;
            contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });

            contextMenu.Items.Add(new Separator());

            cmd = this.ViewModel.DuplicateBayLeavingWhiteSpaceCommand;
            contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });

            cmd = this.ViewModel.DuplicteBayAndAutofillCommand;
            contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });

            contextMenu.Items.Add(new Separator());

            cmd = this.ViewModel.SplitBayCommand;
            contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });

            contextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.MousePoint;
            contextMenu.IsOpen = true;
        }

        /// <summary>
        /// checks if the cluster will be valid if a key is pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClusterNameBox_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (ViewModel.Planogram.ClusterName != null)
            {

                ViewModel.ShowClusterChangeWarning();
            }
        }

        /// <summary>
        /// Check if the cluster scheme name is valid if a key is pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClusterSchemeNameTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (ViewModel.Planogram.ClusterName != null)
            {
                ViewModel.ShowClusterChangeWarning();
            }
        }

        /// <summary>
        /// Checks if the location code is valid if a key is pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void locationCodeName_KeyUp(object sender, KeyEventArgs e)
        {
            if (ViewModel.Planogram.ClusterName != null)
            {
                ViewModel.ShowClusterChangeWarning();
            }
        }

        #endregion

        #region window close

        public Boolean IsClosing
        {
            get;
            private set;
        }

        /// <summary>
        /// Called when the window close cross is pressed.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnCrossCloseRequested(System.ComponentModel.CancelEventArgs e)
        {
            IsClosing = true;

            this.ViewModel.CancelCommand.Execute();
            base.OnCrossCloseRequested(e);
        }

        /// <summary>
        /// Called when this window is closing.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            //DialogResult = false;
            if (this.ViewModel.DialogResult == true)
            {
                this.UpdateBindingSources();
            }
        }

        /// <summary>
        /// Disposes of the viewmodel when the window has closed.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            ClearOldVisuals();
            this.RecentColors = null;

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion

    }

    /// <summary>
    /// Binding validation rule to ensure percentage value does not exceed 100%:
    /// </summary>
    public sealed class MinShelfLifeRule : ValidationRule
    {
        #region Constructor
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public MinShelfLifeRule() : base(ValidationStep.CommittedValue, true) { }
        #endregion

        #region Methods
        /// <summary>
        /// Performs the validation of our business rules
        /// </summary>
        /// <param name="value">The value to validate</param>
        /// <param name="cultureInfo">The current culture</param>
        /// <returns>The validation results</returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // assume success
            ValidationResult validationResult = ValidationResult.ValidResult;

            BindingExpression expression = value as BindingExpression;
            if (expression != null)
            {
                var sourceItem = expression.DataItem as PlanogramPropertiesViewModel;
                if (sourceItem != null)
                {
                    Single minShelfLife = sourceItem.Planogram.Model.Inventory.MinShelfLife;

                    // percentage should not be over 100%
                    if (minShelfLife > 1)
                    {
                        validationResult = new ValidationResult(false, Message.PlanogramPropertiesWindow_MinShelfLifeRankValidation);
                    }
                }
            }

            // and return the validation result
            return validationResult;
        }
        #endregion

    }
}
