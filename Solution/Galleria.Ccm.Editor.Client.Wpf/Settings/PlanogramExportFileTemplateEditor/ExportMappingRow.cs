﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31546 : M.Pettit
//  Created
// V8-31547 : M.Pettit
//  Available fields are now ordered by name in dropdown list
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Editor.Client.Wpf.Settings.PlanogramExportFileTemplateEditor
{
    public sealed class ExportMappingRow : INotifyPropertyChanged, IDataErrorInfo
    {
        #region Fields

        private PlanogramExportTemplateMapping _sourceMapping;
        private PlanogramExportTemplateFieldInfo _ccmField;
        private IEnumerable<PlanogramExportTemplateFieldInfo> _availableExternalFields;
        private PlanogramExportTemplateFieldInfo _externalField;
        private Boolean _isDoubleMapped;
        private Boolean _isValid = true;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath FieldFriendlyNameProperty = WpfHelper.GetPropertyPath<ExportMappingRow>(p => p.FieldFriendlyName);
        public static readonly PropertyPath AvailableExternalFieldsProperty = WpfHelper.GetPropertyPath<ExportMappingRow>(p => p.AvailableExternalFields);
        public static readonly PropertyPath ExternalFieldProperty = WpfHelper.GetPropertyPath<ExportMappingRow>(p => p.ExternalField);
        public static readonly PropertyPath IsDoubleMappedProperty = WpfHelper.GetPropertyPath<ExportMappingRow>(p => p.IsDoubleMapped);
        public static readonly PropertyPath IsValidProperty = WpfHelper.GetPropertyPath<ExportMappingRow>(p => p.IsValid);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the type of mapping this row relates to.
        /// </summary>
        public PlanogramFieldMappingType MappingType
        {
            get { return _sourceMapping.FieldType; }
        }

        public String Field
        {
            get { return _sourceMapping.Field; }
        }

        public String FieldFriendlyName
        {
            get { return _ccmField.DisplayName; }
        }

        public IEnumerable<PlanogramExportTemplateFieldInfo> AvailableExternalFields
        {
            get { return _availableExternalFields; }
        }

        public PlanogramExportTemplateFieldInfo ExternalField
        {
            get { return _externalField; }
            set
            {
                _externalField = value;
                OnPropertyChanged(ExternalFieldProperty);

                _sourceMapping.ExternalField = (value != null) ? value.Field : null;
            }
        }

        public Boolean IsDoubleMapped
        {
            get { return _isDoubleMapped; }
            set
            {
                if (_isDoubleMapped != value)
                {
                    _isDoubleMapped = value;
                    OnPropertyChanged(IsDoubleMappedProperty);
                }
            }
        }

        public Boolean IsValid
        {
            get { return _isValid; }
            set
            {
                _isValid = value;
                OnPropertyChanged(IsValidProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public ExportMappingRow(PlanogramExportTemplateMapping mapping, PlanogramExportTemplateFieldInfo ccmField,
            IEnumerable<PlanogramExportTemplateFieldInfo> availableFields, PlanogramExportTemplateFieldInfo selectedField)
        {
            _sourceMapping = mapping;
            _ccmField = ccmField;
            _availableExternalFields = availableFields.OrderBy(f => f.DisplayName);
            this.ExternalField = selectedField;
            IsValid = ValidMapping();
        }

        #endregion

        #region Methods
        private Boolean ValidMapping()
        {
            if ((MappingType == PlanogramFieldMappingType.Product && (Field == "Name" || Field == "Gtin")) ||
                (MappingType == PlanogramFieldMappingType.Component && (Field == "Name")))
            {
                if (this.ExternalField == null)
                {
                    return false;
                }
            }
            return true;
        }
        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion

        #region IDataErrorInfo
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get
            {
                String result = null;
                if (columnName == ExternalFieldProperty.Path)
                {
                    IsValid = ValidMapping();
                    if (!IsValid)
                    {
                        result = Message.PlanogramFileTemplateEditor_MappingRequired;
                    }
                }
                return result;
            }
        }

        #endregion
    }
}
