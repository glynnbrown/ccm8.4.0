﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.Settings.PlanogramExportFileTemplateEditor
{
    /// <summary>
    /// Interaction logic for PlanogramExportFileTemplateEditorWindow.xaml
    /// </summary>
    public sealed partial class PlanogramExportFileTemplateEditorWindow : ExtendedRibbonWindow
    {
        #region Fields
        const String _clearMappingCommandKey = "ClearMappingCommand";
        const String _clearAllMappingsCommandKey = "ClearAllMappingsCommand";
        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanogramExportFileTemplateEditorViewModel), typeof(PlanogramExportFileTemplateEditorWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets/Sets the window viewmodel context
        /// </summary>
        public PlanogramExportFileTemplateEditorViewModel ViewModel
        {
            get { return (PlanogramExportFileTemplateEditorViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PlanogramExportFileTemplateEditorWindow senderControl = (PlanogramExportFileTemplateEditorWindow)obj;

            if (e.OldValue != null)
            {
                PlanogramExportFileTemplateEditorViewModel oldModel = (PlanogramExportFileTemplateEditorViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                senderControl.Resources.Remove(_clearMappingCommandKey);
                senderControl.Resources.Remove(_clearAllMappingsCommandKey);
            }

            if (e.NewValue != null)
            {
                PlanogramExportFileTemplateEditorViewModel newModel = (PlanogramExportFileTemplateEditorViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                senderControl.Resources.Add(_clearMappingCommandKey, newModel.ClearMappingCommand);
                senderControl.Resources.Add(_clearAllMappingsCommandKey, newModel.ClearAllMappingsCommand);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new insancne of this type.
        /// </summary>
        /// <param name="viewModel"></param>
        public PlanogramExportFileTemplateEditorWindow(PlanogramExportFileTemplateEditorViewModel viewModel)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //set the help file key for this window.
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.ImportPlanogram);

            this.ViewModel = viewModel;

            this.Loaded += PlanogramFileTemplateEditor_Loaded;
        }

        /// <summary>
        /// Carries out initial load actions
        /// </summary>
        private void PlanogramFileTemplateEditor_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= PlanogramFileTemplateEditor_Loaded;

            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Window close

        /// <summary>
        /// Disposes of the viewmodel on close
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;
                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion

        #region Events
        private void ExtendedDataGrid_ColumnHeaderDragStarted(object sender, System.Windows.Controls.Primitives.DragStartedEventArgs e)
        {

        }

        private void OnPerformanceMetricDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            var command = ViewModel.ViewMetricCommand;
            if (command.CanExecute()) command.Execute();
        }
        #endregion
    }
}
