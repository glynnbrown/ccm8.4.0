﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31546 : M.Pettit
//  Created
// V8-31547 : M.Pettit
//  Available fields are now ordered by name in dropdown list
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Galleria.Ccm.Model;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Editor.Client.Wpf.Settings.PlanogramExportFileTemplateEditor
{
    public sealed class ExportPerformanceMetricRow : INotifyPropertyChanged, IDataErrorInfo
    {
        #region Fields

        private PlanogramExportTemplatePerformanceMetric _performanceMetric;
        private IEnumerable<PlanogramExportTemplateFieldInfo> _availableExternalFields;
        private PlanogramExportTemplateFieldInfo _externalField;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath AvailableExternalFieldsProperty = WpfHelper.GetPropertyPath<ExportPerformanceMetricRow>(p => p.AvailableExternalFields);
        public static readonly PropertyPath ExternalFieldProperty = WpfHelper.GetPropertyPath<ExportPerformanceMetricRow>(p => p.ExternalField);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the type of mapping this row relates to.
        /// </summary>
        public PlanogramExportTemplatePerformanceMetric PerformanceMetric
        {
            get { return _performanceMetric; }
        }

        public IEnumerable<PlanogramExportTemplateFieldInfo> AvailableExternalFields
        {
            get { return _availableExternalFields; }
        }

        public PlanogramExportTemplateFieldInfo ExternalField
        {
            get { return _externalField; }
            set
            {
                _externalField = value;
                OnPropertyChanged(ExternalFieldProperty);

                _performanceMetric.ExternalField = (value != null) ? value.Field : null;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public ExportPerformanceMetricRow(
            PlanogramExportTemplatePerformanceMetric performanceMetric,
            IEnumerable<PlanogramExportTemplateFieldInfo> availableFields,
            PlanogramExportTemplateFieldInfo selectedField)
        {
            _performanceMetric = performanceMetric;
            _availableExternalFields = availableFields.OrderBy(f => f.DisplayName);
            this.ExternalField = selectedField;
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion

        #region IDataErrorInfo
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get
            {
                String result = null;
                if (columnName == ExternalFieldProperty.Path)
                {
                    if (this.ExternalField == null)
                    {
                        result = Message.PlanogramFileTemplateEditor_MappingRequired;
                    }
                }
                return result;
            }
        }
        #endregion
    }
}
