﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0.0)
// CCM-24265 : N.Haywood
//  Created
// CCM-25854 : N.Haywood
//  Changed SetFieldCommand
// CCM-25897 : N.Haywood
//  Ensured files are unlocked before the window closes
// CCM-25885 : N.Haywood
//  Added friendly description to set all and close
// CCM-25902 : N.Haywood
//  Ensured custom labels couldn't be saved
// CCM-25927 : N.Haywood
//  Made the window close before setting labels instead of after
// V8-26249 : L.Luong
//  Rework on design
#endregion

#region Version History: (CCM 8.0.0)
// V8-28103 : M.Shelley
//  Modified ".." to localised message label
#endregion

#region Version History: (CCM 8.0.3)
//V8-29313 : L.Ineson
//  Added commands to access repository items.
#endregion

#region Version History CCM830
// V8-31699 : A.Heathcote
//   Added "Null" conditions to the OnSetRequested
#endregion

#endregion


using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using Csla.Server;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.Planograms.Controls.Wpf.ViewModel;
using Galleria.Ccm.Common.Wpf.Selectors;
using System.Globalization;
using Galleria.Ccm.Common.Wpf.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.Settings
{
    public sealed class FixtureLabelEditorViewModel : WindowViewModelBase
    {
        #region Fields

        private Object _labelItemId = null; //the item id to set when applying the selected label
        Label _selectedLabel = Label.NewLabel(LabelType.Fixture); //Currently selected label
        private readonly ReadOnlyCollection<String> _availableFonts;
        private String _windowTitle;
        private PlanogramView _activePlanogram;

        private readonly ModelPermission<Label> _itemRepositoryPerms;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath SelectedLabelProperty = WpfHelper.GetPropertyPath<FixtureLabelEditorViewModel>(p => p.SelectedLabel);
        public static readonly PropertyPath OpenFromRepositoryCommandProperty = GetPropertyPath<FixtureLabelEditorViewModel>(p => p.OpenFromRepositoryCommand);
        public static readonly PropertyPath OpenFromFileCommandProperty = GetPropertyPath<FixtureLabelEditorViewModel>(p => p.OpenFromFileCommand);
        public static readonly PropertyPath FontNamesProperty = WpfHelper.GetPropertyPath<FixtureLabelEditorViewModel>(p => p.AvailableFonts);
        public static readonly PropertyPath DisplayTextProperty = WpfHelper.GetPropertyPath<FixtureLabelEditorViewModel>(p => p.DisplayText);
        public static readonly PropertyPath WindowTitleProperty = WpfHelper.GetPropertyPath<FixtureLabelEditorViewModel>(p => p.WindowTitle);

        //Commands
        public static readonly PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<FixtureLabelEditorViewModel>(p => p.OpenCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<FixtureLabelEditorViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath SaveAsCommandProperty = WpfHelper.GetPropertyPath<FixtureLabelEditorViewModel>(p => p.SaveAsCommand);
        public static readonly PropertyPath SaveAsToRepositoryCommandProperty = GetPropertyPath<FixtureLabelEditorViewModel>(p => p.SaveAsToRepositoryCommand);
        public static readonly PropertyPath SaveAsToFileCommandProperty = GetPropertyPath<FixtureLabelEditorViewModel>(p => p.SaveAsToFileCommand);
        public static readonly PropertyPath SetFieldCommandProperty = WpfHelper.GetPropertyPath<FixtureLabelEditorViewModel>(p => p.SetFieldCommand);
        public static readonly PropertyPath ApplyToCurrentViewCommandProperty = WpfHelper.GetPropertyPath<FixtureLabelEditorViewModel>(p => p.ApplyToCurrentViewCommand);
        public static readonly PropertyPath ApplyToAllViewsCommandProperty = WpfHelper.GetPropertyPath<FixtureLabelEditorViewModel>(p => p.ApplyToAllViewsCommand);
        public static readonly PropertyPath ClearCurrentViewCommandProperty = WpfHelper.GetPropertyPath<FixtureLabelEditorViewModel>(p => p.ClearCurrentViewCommand);
        public static readonly PropertyPath ClearAllViewsCommandProperty = WpfHelper.GetPropertyPath<FixtureLabelEditorViewModel>(p => p.ClearAllViewsCommand);

        #endregion

        #region Properties

        /// <summary>
        /// sets the window title for the editor
        /// </summary>
        public String WindowTitle
        {
            get
            {
                if (_windowTitle == null)
                {
                    return Message.LabelEditor_Custom;
                }
                else
                {
                    return _windowTitle;
                }
            }
            set
            {
                _windowTitle = value;
                OnPropertyChanged(WindowTitleProperty);
            }
        }

        /// <summary>
        /// Returns the selected label model.
        /// </summary>
        public Label SelectedLabel
        {
            get
            {
                return _selectedLabel;
            }
            set
            {
                _selectedLabel = value;

                OnPropertyChanged(SelectedLabelProperty);
                OnSelectedLabelChanged(value, SelectedLabel);
            }
        }

        /// <summary>
        /// Returns the collection of available font names.
        /// </summary>
        public ReadOnlyCollection<String> AvailableFonts
        {
            get { return _availableFonts; }
        }


        /// <summary>
        /// Gets/Sets the setting label content.
        /// </summary>
        public String DisplayText
        {
            get
            {
                return
                    (this.SelectedLabel != null) ?
                    ObjectFieldInfo.ReplaceFieldsWithFriendlyPlaceholders(this.SelectedLabel.Text, PlanItemHelper.EnumerateAllFields())
                    : String.Empty;
            }
            set
            {
                if (this.SelectedLabel != null)
                {
                    this.SelectedLabel.Text = ObjectFieldInfo.ReplaceFriendlyPlaceholdersWithFields(value, PlanItemHelper.EnumerateAllFields());
                }
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Event to notify that a label set has been requested.
        /// </summary>
        public event EventHandler<FixtureLabelEditorEventArgs> SetRequested;

        private void OnSetRequested(Label setting, Boolean setToAll)
        {
            if (SetRequested != null)
            {
               if (_labelItemId == null && setting != null)
                {
                    setting.Name = null;
                }
                LabelItem item = null;
                if (setting != null)
                {
                    item = new LabelItem(setting, _labelItemId);
                }
                SetRequested(this, new FixtureLabelEditorEventArgs(item, setToAll));
            }
        }
         
        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="currentActiveSetting">The setting which is set on the active document.</param>
        public FixtureLabelEditorViewModel(LabelItem currentActiveSetting, PlanogramView activePlanogram)
        {
            //get permissions for current repository.
            _itemRepositoryPerms = (App.ViewState.IsConnectedToRepository) ?
                new ModelPermission<Label>(Label.GetUserPermissions())
                : ModelPermission<Label>.DenyAll();

            //fields
            _selectedLabel = Label.NewLabel(LabelType.Fixture);
            MarkAsCustom();

            //fonts
            List<String> fonts = new List<String>();
            foreach (System.Windows.Media.FontFamily fam in System.Windows.Media.Fonts.SystemFontFamilies)
            {
                fonts.Add(fam.ToString());
            }
            _availableFonts = fonts.OrderBy(f => f).ToList().AsReadOnly();

            //check the active setting to determine what should be selected
            if (activePlanogram != null)
            {
                _activePlanogram = activePlanogram;

                //the setting is different to any save so create a new one.
                if (currentActiveSetting != null)
                {
                    SelectedLabel = currentActiveSetting.ToLabel();
                    MarkFromItem(currentActiveSetting);
                }
            }
        }

        #endregion

        #region Event Handler

        /// <summary>
        /// Responds to a change of selected setting.
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        private void OnSelectedLabelChanged(Label oldValue, Label newValue)
        {
            if (oldValue != null)
            {
                oldValue.PropertyChanged -= SelectedLabel_ModelChanged;
            }
            if (newValue != null)
            {
                newValue.PropertyChanged += SelectedLabel_ModelChanged;
            }
            OnPropertyChanged(DisplayTextProperty);
        }

        /// <summary>
        /// Responds to property changes on the selected setting and changes it to custom label.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedLabel_ModelChanged(Object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != Label.TypeProperty.Name)
            {
                _windowTitle = null;
                OnPropertyChanged(WindowTitleProperty);
                OnPropertyChanged(DisplayTextProperty);
            }
        }

        /// <summary>
        /// Carries out actions when the window is closing.
        /// </summary>
        protected override void OnWindowClosing(CancelEventArgs e, Boolean isCrossClick)
        {
            //unlock the selected label first.
            if (_selectedLabel != null
                        && !_selectedLabel.IsNew
                        && _selectedLabel.Id is String)
            {
                Label.UnlockLabelByFileName((String)_selectedLabel.Id);
            }


            base.OnWindowClosing(e, isCrossClick);
        }

        #endregion

        #region Commands

        #region OpenCommand

        private RelayCommand _openCommand;

        /// <summary>
        /// Opens an existing label
        /// </summary>
        public RelayCommand OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand(
                        p => Open_Executed())
                    {
                        FriendlyName = Message.Generic_Open,
                        SmallIcon = ImageResources.Open_16,
                    };
                    RegisterCommand(_openCommand);
                }
                return _openCommand;
            }
        }

        private void Open_Executed()
        {
            if (this.OpenFromRepositoryCommand.CanExecute())
            {
                this.OpenFromRepositoryCommand.Execute();
            }
            else
            {
                this.OpenFromFileCommand.Execute();
            }
        }

        #endregion

        #region OpenFromRepository

        private RelayCommand _openFromRepositoryCommand;

        /// <summary>
        /// Allows the user to select a repository template to load data from.
        /// </summary>
        public RelayCommand OpenFromRepositoryCommand
        {
            get
            {
                if (_openFromRepositoryCommand == null)
                {
                    _openFromRepositoryCommand = new RelayCommand(
                        o => OpenFromRepository_Executed(o),
                        o => OpenFromRepository_CanExecute())
                    {
                        FriendlyName = Message.ValidationTemplateEditor_OpenFromRepository
                    };
                    RegisterCommand(_openFromRepositoryCommand);
                }
                return _openFromRepositoryCommand;
            }
        }

        private Boolean OpenFromRepository_CanExecute()
        {
            //must be connected
            if (!App.ViewState.IsConnectedToRepository)
            {
                this.OpenFromRepositoryCommand.DisabledReason = Message.ValidationTemplateEditor_OpenFromRepository_DisabledReasonNoConnection;
                return false;
            }

            //must have open permission
            if (!_itemRepositoryPerms.CanFetch)
            {
                this.OpenFromRepositoryCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            return true;
        }

        private void OpenFromRepository_Executed(Object args)
        {
            //Get the template id to load.
            Object id = args;
            if (id == null)
            {
                GenericSingleItemSelectorWindow win = new GenericSingleItemSelectorWindow();
                win.ItemSource = LabelInfoList.FetchByEntityId(App.ViewState.EntityId).Where(l=> l.Type == LabelType.Fixture).ToList();
                win.SelectionMode = System.Windows.Controls.DataGridSelectionMode.Single;
                GetWindowService().ShowDialog<GenericSingleItemSelectorWindow>(win);
                if (win.DialogResult != true) return;

                id = win.SelectedItems.Cast<LabelInfo>().First().Id;
            }


            //load the template
            base.ShowWaitCursor(true);
            try
            {
                this.SelectedLabel = Label.FetchById(id);
                MarkAsExisting(this.SelectedLabel);
            }
            catch (Exception ex)
            {
                base.ShowWaitCursor(false);
                LocalHelper.RecordException(ex);
                CommonHelper.GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                return;
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region OpenFromFile

        private RelayCommand _openFromFileCommand;

        /// <summary>
        /// Allows the user to select a file template to load data from.
        /// </summary>
        public RelayCommand OpenFromFileCommand
        {
            get
            {
                if (_openFromFileCommand == null)
                {
                    _openFromFileCommand = new RelayCommand(p => OpenFromFile_Executed(p))
                    {
                        FriendlyName = Message.ValidationTemplateEditor_OpenFromFile
                    };
                    RegisterCommand(_openFromFileCommand);
                }
                return _openFromFileCommand;
            }
        }


        private void OpenFromFile_Executed(Object args)
        {
            //show the file dialog if no args passed in.
            String file = args as String;
            if (String.IsNullOrEmpty(file)) file = LabelUIHelper.ShowOpenFileDialog();
            if (String.IsNullOrEmpty(file)) return;


            base.ShowWaitCursor(true);
            try
            {
                //unlock the original file as we dont need to hold it
                using (Label label = Label.FetchByFilename(file, /*asReadOnly*/true))
                {
                    this.SelectedLabel = label.Copy();
                    MarkAsExisting(label);
                }
            }
            catch (Exception ex)
            {
                base.ShowWaitCursor(false);
                LocalHelper.RecordException(ex);
                CommonHelper.GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                return;
            }
            base.ShowWaitCursor(false);

        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Closes the window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel,
                        SmallIcon = ImageResources.TurnOff_16,
                    };
                    RegisterCommand(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            CloseWindow();
        }

        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;

        /// <summary>
        /// Saves changes made to a new label setting and loads new as current
        /// /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(p))
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        SmallIcon = ImageResources.SaveAs_16,
                        FriendlyDescription = Message.LabelEditor_SaveAs_Desc,
                        Icon = ImageResources.SaveAs_32
                    };
                    RegisterCommand(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }

        private void SaveAs_Executed(object args)
        {
            if (this.SaveAsToRepositoryCommand.CanExecute())
            {
                this.SaveAsToRepositoryCommand.Execute();
            }
            else
            {
                this.SaveAsToFileCommand.Execute();
            }
        }
        #endregion

        #region SaveAsToRepository

        private RelayCommand _saveAsToRepositoryCommand;

        /// <summary>
        /// Saves the currrent template as a new template in the repository.
        /// </summary>
        public RelayCommand SaveAsToRepositoryCommand
        {
            get
            {
                if (_saveAsToRepositoryCommand == null)
                {
                    _saveAsToRepositoryCommand = new RelayCommand(
                            o => SaveAsToRepository_Executed(),
                        o => SaveAsToRepository_CanExecute())
                    {
                        FriendlyName = Message.ValidationTemplateEditor_SaveToRepository
                    };
                    RegisterCommand(_saveAsToRepositoryCommand);

                }
                return _saveAsToRepositoryCommand;
            }
        }

        private Boolean SaveAsToRepository_CanExecute()
        {
            if (!App.ViewState.IsConnectedToRepository)
            {
                this.SaveAsToRepositoryCommand.DisabledReason = Message.DisabledReason_NoRepositoryConnected;
                return false;
            }

            //must have permission
            if (!_itemRepositoryPerms.CanCreate)
            {
                this.SaveAsToRepositoryCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            return true;
        }

        /// <summary>
        ///     Invoked whenever the <see cref="SaveAsToRepositoryCommand" /> is executed.
        /// </summary>
        private void SaveAsToRepository_Executed()
        {
            String copyName;

            LabelInfoList existingItems = LabelInfoList.FetchByEntityId(App.ViewState.EntityId);
            Predicate<String> isUniqueCheck =
               (s) => { return !existingItems.Select(p => p.Name).Contains(s, StringComparer.OrdinalIgnoreCase); };

            Boolean nameAccepted = GetWindowService().PromptForSaveAsName(isUniqueCheck, String.Empty, out copyName);
            if (!nameAccepted) return;

            ShowWaitCursor(true);


            //copy the item and rename
            Label itemCopy = this.SelectedLabel.Copy();
            itemCopy.Name = copyName;


            //Perform the save.
            ShowWaitCursor(true);

            try
            {
                //Make sure the entityId is set
                itemCopy.EntityId = App.ViewState.EntityId;

                this.SelectedLabel = itemCopy.SaveAs();
                MarkAsExisting(this.SelectedLabel);
            }
            catch (Exception ex)
            {
                base.ShowWaitCursor(false);
                LocalHelper.RecordException(ex);
                CommonHelper.GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Save);
                return;
            }

            ShowWaitCursor(false);
        }

        #endregion

        #region SaveToFile


        private RelayCommand _saveAsToFileCommand;

        /// <summary>
        /// Saves the current template as a new file template.
        /// </summary>
        public RelayCommand SaveAsToFileCommand
        {
            get
            {
                if (_saveAsToFileCommand == null)
                {
                    _saveAsToFileCommand = new RelayCommand(p => SaveAsToFile_Executed(p))
                    {
                        FriendlyName = Message.ValidationTemplateEditor_SaveToFile
                    };
                    RegisterCommand(_saveAsToFileCommand);
                }
                return _saveAsToFileCommand;
            }
        }


        private void SaveAsToFile_Executed(Object args)
        {
            Label itemToSave = this.SelectedLabel;

            //get the file path to save to.
            String filePath = args as String;
            if (String.IsNullOrEmpty(filePath)) filePath = LabelUIHelper.ShowSaveAsFileDialog();
            if (String.IsNullOrEmpty(filePath)) return;

            base.ShowWaitCursor(true);

            //save
            try
            {
                itemToSave = itemToSave.SaveAsFile(filePath);
            }
            catch (Exception ex)
            {
                base.ShowWaitCursor(false);
                LocalHelper.RecordException(ex);
                CommonHelper.GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Save);
                return;
            }
            base.ShowWaitCursor(false);

            //reselect the label
            if (itemToSave != null)
            {
                this.SelectedLabel = itemToSave.Copy();
                MarkAsExisting(itemToSave);
            }

            //unlock the file again as we don't need to keep hold of it.
            itemToSave.Dispose();
        }

        #endregion

        #region SetFieldCommand

        private RelayCommand _setFieldCommand;

        /// <summary>
        /// Shows the window to populate the field specified by the param
        /// </summary>
        public RelayCommand SetFieldCommand
        {
            get
            {
                if (_setFieldCommand == null)
                {
                    _setFieldCommand = new RelayCommand(
                        p => SetField_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                        FriendlyDescription = Message.LabelEditor_SetFieldCommand_Desc
                    };
                    RegisterCommand(_setFieldCommand);
                }
                return _setFieldCommand;

            }
        }

        private void SetField_Executed()
        {
            String fieldValue = this.SelectedLabel.Text;

            FieldSelectorViewModel fieldSelectorView =
                new FieldSelectorViewModel(FieldSelectorInputType.Formula, FieldSelectorResolveType.Text,
                    fieldValue, PlanItemHelper.GetPlanFixtureFieldSelectorGroups());

            GetWindowService().ShowDialog<FieldSelectorWindow>(fieldSelectorView);

            if (fieldSelectorView.DialogResult == true)
            {
                this.SelectedLabel.Text = fieldSelectorView.FieldText;
            }
        }

        #endregion

        #region ApplyToCurrentView Command

        private RelayCommand _applyToCurrentViewCommand;

        /// <summary>
        /// Applies the selected highlight to the current view.
        /// </summary>
        public RelayCommand ApplyToCurrentViewCommand
        {
            get
            {
                if (_applyToCurrentViewCommand == null)
                {
                    _applyToCurrentViewCommand = new RelayCommand(
                        p => ApplyToCurrent_Executed())
                    {
                        FriendlyName = Message.HighlightEditor_ApplyToCurrentView,
                    };
                    RegisterCommand(_applyToCurrentViewCommand);
                }
                return _applyToCurrentViewCommand;
            }
        }

        private void ApplyToCurrent_Executed()
        {
            //Check if the label is custom
            if (_windowTitle == null)
            {
                //Sets the custom view 
                this.SelectedLabel.Name = null;
            }

            OnSetRequested(this.SelectedLabel, false);
            CloseWindow();
        }

        #endregion

        #region ApplyToAllViews Command

        private RelayCommand _applyToAllViewsCommand;

        /// <summary>
        /// Applies the selected highlight to all views
        /// of the active planogram.
        /// </summary>
        public RelayCommand ApplyToAllViewsCommand
        {
            get
            {
                if (_applyToAllViewsCommand == null)
                {
                    _applyToAllViewsCommand = new RelayCommand(
                        p => ApplyToAllViews_Executed())
                    {
                        FriendlyName = Message.HighlightEditor_ApplyToAllViews,
                        SmallIcon = ImageResources.HighlightEditor_ApplyToAll_16,
                    };
                    RegisterCommand(_applyToAllViewsCommand);
                }
                return _applyToAllViewsCommand;
            }
        }

        private void ApplyToAllViews_Executed()
        {
            //Check if the label is custom
            if (_windowTitle == null)
            {
                //Sets the custom view 
                this.SelectedLabel.Name = null;

                CancelCommand.Execute();

                OnSetRequested(this.SelectedLabel, true);
            }

            OnSetRequested(this.SelectedLabel, true);
            CloseWindow();
        }
        #endregion

        #region ClearCurrentView Command

        private RelayCommand _clearCurrentViewCommand;

        /// <summary>
        /// Applies the selected highlight to the current view.
        /// </summary>
        public RelayCommand ClearCurrentViewCommand
        {
            get
            {
                if (_clearCurrentViewCommand == null)
                {
                    _clearCurrentViewCommand = new RelayCommand(
                        p => ClearCurrent_Executed())
                    {
                        FriendlyName = Message.HighlightEditor_ClearCurrentView,
                    };
                    RegisterCommand(_clearCurrentViewCommand);
                }
                return _clearCurrentViewCommand;
            }
        }

        private void ClearCurrent_Executed()
        {
            OnSetRequested(null, false);
            CloseWindow();
        }


        #endregion

        #region ClearAllViews Command

        private RelayCommand _clearAllViewsCommand;

        /// <summary>
        /// Applies the selected highlight to all views
        /// of the active planogram.
        /// </summary>
        public RelayCommand ClearAllViewsCommand
        {
            get
            {
                if (_clearAllViewsCommand == null)
                {
                    _clearAllViewsCommand = new RelayCommand(
                        p => ClearAllViews_Executed())
                    {
                        FriendlyName = Message.HighlightEditor_ClearAllViews,
                    };
                    RegisterCommand(_clearAllViewsCommand);
                }
                return _clearAllViewsCommand;
            }
        }

        private void ClearAllViews_Executed()
        {
            OnSetRequested(null, true);
            CloseWindow();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Marks the currently selected label as as existing item.
        /// </summary>
        /// <param name="label"></param>
        private void MarkAsExisting(Label label)
        {
            this.WindowTitle = label.Name;

            //set the item id 
            _labelItemId = label.Id;
        }

        /// <summary>
        /// Marks the currently selected label as as existing item.
        /// </summary>
        /// <param name="label"></param>
        private void MarkFromItem(LabelItem item)
        {
            if (item.Id != null)
            {
                this.WindowTitle = item.Name;
            }
            else
            {
                this.WindowTitle = Message.LabelEditor_Custom;
            }

            //set the item id 
            _labelItemId = item.Id;
        }

        /// <summary>
        /// Marks the currently selected label as custom.
        /// </summary>
        private void MarkAsCustom()
        {
            this.WindowTitle = Message.LabelEditor_Custom;

            //set the item id to null.
            _labelItemId = null;
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!IsDisposed)
            {
                if (disposing)
                {
                    base.DisposeBase();
                }
                IsDisposed = true;
            }
        }

        #endregion
    }

    public sealed class FixtureLabelEditorEventArgs : EventArgs
    {
        #region Properties
        public LabelItem LabelView { get; private set; }
        public Boolean IsSetToAll { get; private set; }
        #endregion

        public FixtureLabelEditorEventArgs(LabelItem label, Boolean isSetToAll)
        {
            this.LabelView = label;
            this.IsSetToAll = isSetToAll;
        }
    }

}
