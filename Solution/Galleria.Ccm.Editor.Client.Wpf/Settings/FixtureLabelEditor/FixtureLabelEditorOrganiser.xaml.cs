﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// CCM-24265 : N.Haywood
//  Created
// V8-26249 : L.Luong
//  Changed to fit new design
#endregion
#endregion


using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Framework.Controls.Wpf;
using DropDownButton = Fluent.DropDownButton;

namespace Galleria.Ccm.Editor.Client.Wpf.Settings
{
    /// <summary>
    /// Interaction logic for FixtureLabelEditorOrganiser.xaml
    /// </summary>
    public sealed partial class FixtureLabelEditorOrganiser : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(FixtureLabelEditorViewModel), typeof(FixtureLabelEditorOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));


        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            FixtureLabelEditorOrganiser senderControl = (FixtureLabelEditorOrganiser)obj;

            if (e.OldValue != null)
            {
                FixtureLabelEditorViewModel oldModel = (FixtureLabelEditorViewModel)e.OldValue;
                oldModel.UnregisterWindowControl();
            }

            if (e.NewValue != null)
            {
                FixtureLabelEditorViewModel newModel = (FixtureLabelEditorViewModel)e.NewValue;
                newModel.RegisterWindowControl(senderControl);
            }
        }

        /// <summary>
        /// Gets/Sets the window viewmodel context
        /// </summary>
        public FixtureLabelEditorViewModel ViewModel
        {
            get { return (FixtureLabelEditorViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public FixtureLabelEditorOrganiser(FixtureLabelEditorViewModel viewModel)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            this.ViewModel = viewModel;

            //set the help file reference 
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.FixtureLabels);

            this.Loaded += ProfileMaintenanceOrganiser_Loaded;
        }

        
        #endregion

        #region Event Handlers

        private void ProfileMaintenanceOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ProfileMaintenanceOrganiser_Loaded;

            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        /// <summary>
        /// Called whenever the label content textbox gets double clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LabelContentTextBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.ViewModel.SetFieldCommand.Execute();
        }

        /// <summary>
        /// Keyboard navigation functionality added to ensure 508 compliance on dropdown buttons
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DropDownButtonNavigation_OnpreviewKeyDown(object sender, KeyEventArgs e)
        {           
            DropDownButton activeButton = sender as DropDownButton;
            if (activeButton == null) return;

            //Expands/collapse the dropdown when the return key is pressed and
            //gives keyboard focus to the first item to allow navigation
            if (activeButton.HasItems && e.Key == Key.Return)
            {
                activeButton.IsDropDownOpen = !activeButton.IsDropDownOpen;
                e.Handled = true;
            }

            //Makes the dropdown menu items navigable with arrow keys
            else if (e.Key == Key.Down || e.Key == Key.Up)
            {
                Boolean up = e.Key == Key.Up;

                if (up && activeButton.Items.CurrentPosition != 0)
                {
                    activeButton.Items.MoveCurrentToPrevious();
                }
                else if (!up && activeButton.Items.CurrentPosition != activeButton.Items.Count - 1)
                {
                    activeButton.Items.MoveCurrentToNext();
                }
                else
                {
                    if (up) activeButton.Items.MoveCurrentToLast();
                    else activeButton.Items.MoveCurrentToFirst();
                }

                //Skips over any separator in the dropdown as it's seen as a menu item 
                //and gets selected when navigating and appears unresponsive
                var menuType = activeButton.Items.CurrentItem.GetType();
                if (menuType == typeof(Separator))
                {
                    if (up) activeButton.Items.MoveCurrentToPrevious();
                    else activeButton.Items.MoveCurrentToNext();
                }

                Keyboard.Focus(activeButton.Items.CurrentItem as MenuItem);
                e.Handled = true;
            }

            //Execute menu item's bound command on pressing space bar
            else if (e.Key == Key.Space && activeButton.IsDropDownOpen)
            {
                MenuItem selectedMenuItem = activeButton.Items.CurrentItem as MenuItem;
                if (selectedMenuItem == null) return;
                if (selectedMenuItem.Command.CanExecute(null)) selectedMenuItem.Command.Execute(null);
                e.Handled = true;
            }
        }

        #endregion

        #region Window close

        /// <summary>
        /// Disposes of the viewmodel on close
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;
                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion
    }
}
