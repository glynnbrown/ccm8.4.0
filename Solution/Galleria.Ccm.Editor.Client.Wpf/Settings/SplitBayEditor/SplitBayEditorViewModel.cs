﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM830
// V8-32360 : M.Brumby
//  [PCR01564] Auto split bays by a fixed value
// V8-32542 : M.Brumby
//  [PCR01564] Split button enable conditions
// V8-32677 : A.Heathcote
//  Added "SplitBayCommand_CanExecute()" condition to
//  disable button if "0" is entered.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using Csla.Server;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Common.Wpf.Misc;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Editor.Client.Wpf.Settings
{
    public sealed class SplitBayEditorViewModel : ViewModelAttachedControlObject<SplitBayEditorWindow>
    {

       private RestructurePlanogramSettings _resturctureSettings = new RestructurePlanogramSettings();
       private PlanogramView _plan;
       private IEnumerable<PlanogramFixtureView> _fixtureItems;

        #region Binding Property Paths

        //Properties
        //public static readonly PropertyPath SelectedLabelProperty = GetPropertyPath<SplitBayEditorViewModel>(p => p.SelectedLabel);
       
        
        #endregion

        #region Properties

       public DisplayUnitOfMeasureCollection DisplayUOMs
       {
           get
           {
               if (_plan != null)
               {
                   return DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(_plan.Model);
               }
               else return DisplayUnitOfMeasureCollection.Empty;
           }
       }

        #region AllowAdvancedBaySplitting
       
        /// <summary>
        /// Gets or sets the AllowAdvancedBaySplitting value
        /// </summary>
        public Boolean AllowAdvancedBaySplitting
        {
            get 
            { 
                return _resturctureSettings.CanRestructure; 
            }
        }
        #endregion

        #region CanSplitComponents
        
        /// <summary>
        /// Gets or sets the CanSplitComponents value
        /// </summary>
        public Boolean CanSplitComponents
        {
            get
            {
                return _resturctureSettings.CanSplitComponents;
            }
            set
            {
                _resturctureSettings.CanSplitComponents = value;
                OnPropertyChanged("CanSplitComponents");
                UpdateHelperProperties();
            }
        }
        #endregion

        #region SplitBaysByComponentStart
       
        /// <summary>
        /// Gets or sets the SplitBaysByComponentStart value
        /// </summary>
        public Boolean SplitBaysByComponentStart
        {
            get
            {
                return _resturctureSettings.SplitByComponentStart;
            }
            set
            {
                _resturctureSettings.SplitByComponentStart = value;
                OnPropertyChanged("SplitBaysByComponentStart");
                UpdateHelperProperties();
            }
        }
        #endregion

        #region SplitBaysByComponentEnd
        
        /// <summary>
        /// Gets or sets the SplitBaysByComponentEnd value
        /// </summary>
        public Boolean SplitBaysByComponentEnd
        {
            get
            {
                return _resturctureSettings.SplitByComponentEnd;
            }
            set
            {
                _resturctureSettings.SplitByComponentEnd = value;
                OnPropertyChanged("SplitBaysByComponentEnd");
                UpdateHelperProperties();
            }
        }
        #endregion

        #region SplitByFixedSize
       
        /// <summary>
        /// Gets or sets the SplitByFixedSize value
        /// </summary>
        public Boolean SplitByFixedSize
        {
            get
            {
                return _resturctureSettings.SplitByFixedSize;
            }
            set
            {
                _resturctureSettings.SplitByFixedSize = value;
                OnPropertyChanged("SplitByFixedSize");
                UpdateHelperProperties();
            }
        }
        #endregion

        #region SplitByRecurringPattern
       
        /// <summary>
        /// Gets or sets the SplitByRecurringPattern value
        /// </summary>
        public Boolean SplitByRecurringPattern
        {
            get
            {
                return _resturctureSettings.SplitByRecurringPattern;
            }
            set
            {
                _resturctureSettings.SplitByRecurringPattern = value;
                OnPropertyChanged("SplitByRecurringPattern");
                UpdateHelperProperties();
            }
        }
        #endregion

        #region SplitByBayCount
        
        /// <summary>
        /// Gets or sets the SplitByBayCount value
        /// </summary>
        public Boolean SplitByBayCount
        {
            get
            {
                return _resturctureSettings.SplitByBayCount;
            }
            set
            {
                _resturctureSettings.SplitByBayCount = value;
                OnPropertyChanged("SplitByBayCount");
                UpdateHelperProperties();
            }
        }
        #endregion

        #region SplitFixedSize
        
        /// <summary>
        /// Gets or sets the SplitFixedSize value
        /// </summary>
        public Double SplitFixedSize
        {
            get
            {
                return _resturctureSettings.FixedSize;
            }
            set
            {
                _resturctureSettings.FixedSize = value;
                OnPropertyChanged("SplitFixedSize");
                UpdateHelperProperties();
            }
        }
        #endregion

        #region SplitRecurringPattern
       
        /// <summary>
        /// Gets or sets the SplitRecurringPattern value
        /// </summary>
        public String SplitRecurringPattern
        {
            get
            {
                return _resturctureSettings.RecurringPattern;
            }
            set
            {
                _resturctureSettings.RecurringPattern = value;
                OnPropertyChanged("SplitRecurringPattern");
                UpdateHelperProperties();
            }
        }
        #endregion

        #region SplitBayCount
       
        /// <summary>
        /// Gets or sets the SplitBayCount value
        /// </summary>
        public Int32 SplitBayCount
        {
            get
            {
                return _resturctureSettings.BayCount;
            }
            set
            {
                _resturctureSettings.BayCount = value;
                OnPropertyChanged("SplitBayCount");
                UpdateHelperProperties();
            }
        }
        #region Helper Properties
        public Boolean EnableSplitComponents
        {
            get
            {
                return true;
            }
        }
        public Boolean EnableSplitBayByComponent
        {
            get
            {
                return !SplitByBayCount;
            }
        }
        public Boolean EnableSplitBayByFixedSize
        {
            get
            {
                return !SplitByBayCount && !SplitByRecurringPattern;
            }
        }
        public Boolean EnableSplitByRecurringPattern
        {
            get
            {
                return !SplitByBayCount && !SplitByFixedSize;
            }
        }
        public Boolean EnableSplitByBayCount
        {
            get
            {
                return !SplitByRecurringPattern && !SplitByFixedSize;
            }
        }

        private void UpdateHelperProperties()
        {
            OnPropertyChanged("EnableSplitComponents");
            OnPropertyChanged("EnableSplitBayByComponent");
            OnPropertyChanged("EnableSplitBayByFixedSize");
            OnPropertyChanged("EnableSplitByRecurringPattern");
            OnPropertyChanged("EnableSplitByBayCount");
        }
        #endregion
        #endregion

        #endregion

        #region Contstructor

        public SplitBayEditorViewModel(PlanogramView plan, IEnumerable<PlanogramFixtureView> fixtureItems)
        {
            _plan = plan;
            _fixtureItems = fixtureItems;
        }

        #endregion

        #region Commands
        #region SplitBayCommand

        private RelayCommand _splitBayCommand;

        /// <summary>
        /// Adds a new fixture copy after the selected fixture
        /// </summary>
        public RelayCommand SplitBayCommand
        {
            get
            {
                if (_splitBayCommand == null)
                {
                    _splitBayCommand = new RelayCommand(
                        p => SplitBayCommand_Executed(),
                        p => SplitBayCommand_CanExecute())
                    {
                        FriendlyName = Message.SplitBay_Command_Split,
                        FriendlyDescription = Message.SplitBay_Command_Split_Description
                    };
                    base.ViewModelCommands.Add(_splitBayCommand);
                }
                return _splitBayCommand;
            }
        }

        private Boolean SplitBayCommand_CanExecute()
        {
            Boolean enableCommand = true;
            String disabledReason = String.Empty;
            _splitBayCommand.DisabledReason = disabledReason;

            if (_plan == null || _fixtureItems == null || !_fixtureItems.Any())
            {
                enableCommand = false;
                _splitBayCommand.DisabledReason = Message.SplitBay_Command_Split_NoFixture;
            }

            //If split by component then other settings don't matter if they aren't set
            if (this.SplitBaysByComponentEnd || this.SplitBaysByComponentStart)
            {
                return enableCommand;
            }

            if (this.SplitByBayCount && this.SplitBayCount <= 1)
            {
                enableCommand = false;
                _splitBayCommand.DisabledReason = Message.SplitBay_Command_Split_BayCountTooLow;
            }
            if (this.SplitByFixedSize && this.SplitFixedSize <= 0)
            {
                enableCommand = false;
                _splitBayCommand.DisabledReason = Message.SplitBay_Command_Split_FixtedSizeTooSmall;
            }
            if (this.SplitByRecurringPattern && String.IsNullOrWhiteSpace(this.SplitRecurringPattern))
            {
                enableCommand = false;
                _splitBayCommand.DisabledReason = Message.SplitBay_Command_Split_RecurringPatternEmpty;
            }

            if (this.SplitRecurringPattern.Equals("0"))
            {
                enableCommand = false;
                _splitBayCommand.DisabledReason = Message.SplitBay_Command_Split_RecurringPatternCannotBeZero;
            }
            return enableCommand;
        }

        private void SplitBayCommand_Executed()
        {
            base.ShowWaitCursor(true);

            foreach (PlanogramFixtureView item in _fixtureItems.ToList())
            {
                _resturctureSettings.Restructure(_plan.Model, item.FixtureItemModel);
            }

            base.ShowWaitCursor(false);

            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Adds a new fixture copy after the selected fixture
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => CancelCommand_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }


        private void CancelCommand_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion
        #endregion

        #region Methods

        #endregion
        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!IsDisposed)
            {
                if (disposing)
                {
                    
                }
                IsDisposed = true;
            }
        }

        #endregion
    }
}
