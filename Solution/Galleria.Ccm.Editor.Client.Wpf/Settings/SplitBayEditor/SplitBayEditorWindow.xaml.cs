﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.Settings
{
    /// <summary>
    /// Interaction logic for SplitBayEditorWindow.xaml
    /// </summary>
    public partial class SplitBayEditorWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(SplitBayEditorViewModel), typeof(SplitBayEditorWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            SplitBayEditorWindow senderControl = (SplitBayEditorWindow)obj;

            if (e.OldValue != null)
            {
                SplitBayEditorViewModel oldModel = (SplitBayEditorViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                SplitBayEditorViewModel newModel = (SplitBayEditorViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        ///<summary>
        /// Gets/Sets the window viewmodel context
        /// </summary> 
        public SplitBayEditorViewModel ViewModel
        {
            get { return (SplitBayEditorViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        public SplitBayEditorWindow(SplitBayEditorViewModel viewModel)
        {
            InitializeComponent();
            this.ViewModel = viewModel;

            this.Loaded += SplitBayEditorWindow_Loaded;
        }

        /// <summary>
        /// Carries out initial load actions.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SplitBayEditorWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= SplitBayEditorWindow_Loaded;

            Dispatcher.BeginInvoke(
                (Action)(() => Mouse.OverrideCursor = null), priority: System.Windows.Threading.DispatcherPriority.Background);
        }
    }
}
