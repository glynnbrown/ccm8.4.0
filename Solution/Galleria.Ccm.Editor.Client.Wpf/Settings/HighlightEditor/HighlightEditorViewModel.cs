﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
// L.Hodson ~ Created.
// V8-24265 J.Pickup
//  Now reselects labels correctly after actions have occurred.
// V8-25436 : L.Luong
//  Changed to fit new design
// V8-27238 : I.George
// ~ Added IsPlanogramActiveProperty
// V8-27117 : J.Pickup
// ~ SelectedHighlight_ChildChanged handler modified so that the preview value updates the source grouping property.
// V8-28056 : J.Pickup
// ~ I have reworked the updating of GroupNames for Characteristics so it goes off order instead of name. 
// V8-28175 : A.Kuszyk
//  Passed active planogram through to field selector groups to ensure that performance fields have relevant
//  metric names.
// V8-27600 : N.Haywood
//  Made the groups keep their settings on save
// V8-28538 : D.Pleasance
//  Amended to lookup index of highlight groups rather than using the order value which lead to incorrect name being updated.
// V8-28746 : I.George
//  Added Save_CanExecute method to ensure save is valid before it's done
#endregion

#region Version History: (CCM 8.0.1)
// V8-28103 : M.Shelley
//  Modified ".." to localised message label
#endregion

#region Version History: (CCM 8.0.3)
//V8-29313 : L.Ineson
//  Added commands to access repository items.
#endregion

#region Version History: (CCM 8.1.0)
// CCM-30043 : N.Haywood
//  Made CreateSettingGroups clear all old groups
#endregion

#region Version History CCM830
// V8-31699 : A.Heathcote
//  Made changes to the events region 
#endregion
#endregion

using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using Csla.Core;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Model;
using Galleria.Framework.ViewModel;
using System.Collections.Generic;
using System.Windows.Controls;
using Csla;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Common.Wpf.Misc;
using CommonImageResources = Galleria.Ccm.Common.Wpf.Resources.ImageResources;
using CommonMessage = Galleria.Ccm.Common.Wpf.Resources.Language.Message;
using static Galleria.Ccm.Common.Wpf.Resources.Language.Message;

namespace Galleria.Ccm.Editor.Client.Wpf.Settings.HighlightEditor
{
    /// <summary>
    /// Viewmodel controller for the HighlightEditor window.
    /// </summary>
    public sealed class HighlightEditorViewModel : WindowViewModelBase, IHighlightSetupViewModel
    {
        #region Fields

        private String _windowTitle;
        private PlanogramView _activePlanogram;
        private Boolean _isPlanogramActive = false;

        private readonly ModelPermission<Highlight> _itemRepositoryPerms;
        private Object _highlightItemId = null; //the item id to set when applying the selected highlight
        private Highlight _selectedHighlight; // Currently selected highlight

        private readonly BulkObservableCollection<HighlightFilter> _currentHighlightFilters = new BulkObservableCollection<HighlightFilter>();
        private ReadOnlyBulkObservableCollection<HighlightFilter> _currentHighlightFiltersRO;
        private readonly HighlightCharacteristicGroupingCollection _characteristicGroups = new HighlightCharacteristicGroupingCollection();
        
        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath SelectedHighlightProperty = GetPropertyPath<HighlightEditorViewModel>(p => p.SelectedHighlight);
        public static readonly PropertyPath CurrentHighlightFiltersProperty = GetPropertyPath<HighlightEditorViewModel>(p => p.CurrentHighlightFilters);
        public static readonly PropertyPath CharacteristicGroupsProperty = GetPropertyPath<HighlightEditorViewModel>(p => p.CharacteristicGroups);
        public static readonly PropertyPath WindowTitleProperty = GetPropertyPath<HighlightEditorViewModel>(p => p.WindowTitle);
        public static readonly PropertyPath IsPlanogramActiveProperty = GetPropertyPath<HighlightEditorViewModel>(p => p.IsPlanogramActive);

        //Commands
        public static readonly PropertyPath OpenCommandProperty = GetPropertyPath<HighlightEditorViewModel>(p => p.OpenCommand);
        public static readonly PropertyPath OpenFromRepositoryCommandProperty = GetPropertyPath<HighlightEditorViewModel>(p => p.OpenFromRepositoryCommand);
        public static readonly PropertyPath OpenFromFileCommandProperty = GetPropertyPath<HighlightEditorViewModel>(p => p.OpenFromFileCommand);
        public static readonly PropertyPath SaveAsCommandProperty = GetPropertyPath<HighlightEditorViewModel>(p => p.SaveAsCommand);
        public static readonly PropertyPath SaveAsToRepositoryCommandProperty = GetPropertyPath<HighlightEditorViewModel>(p => p.SaveAsToRepositoryCommand);
        public static readonly PropertyPath SaveAsToFileCommandProperty = GetPropertyPath<HighlightEditorViewModel>(p => p.SaveAsToFileCommand);
        public static readonly PropertyPath ApplyToCurrentViewCommandProperty = GetPropertyPath<HighlightEditorViewModel>(p => p.ApplyToCurrentViewCommand);
        public static readonly PropertyPath ApplyToAllViewsCommandProperty = GetPropertyPath<HighlightEditorViewModel>(p => p.ApplyToAllViewsCommand);
        public static readonly PropertyPath ClearCurrentViewCommandProperty = GetPropertyPath<HighlightEditorViewModel>(p => p.ClearCurrentViewCommand);
        public static readonly PropertyPath ClearAllViewsCommandProperty = GetPropertyPath<HighlightEditorViewModel>(p => p.ClearAllViewsCommand);
        public static readonly PropertyPath CancelCommandProperty = GetPropertyPath<HighlightEditorViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath SetFieldCommandProperty = GetPropertyPath<HighlightEditorViewModel>(p => p.SetFieldCommand);
        public static readonly PropertyPath SetFilterFieldCommandProperty = GetPropertyPath<HighlightEditorViewModel>(p => p.SetFilterFieldCommand);
        public static readonly PropertyPath RemoveFilterCommandProperty = GetPropertyPath<HighlightEditorViewModel>(p => p.RemoveFilterCommand);
        public static readonly PropertyPath AddCharacteristicCommandProperty = GetPropertyPath<HighlightEditorViewModel>(p => p.AddCharacteristicCommand);
        public static readonly PropertyPath SetCharacteristicRuleFieldCommandProperty = GetPropertyPath<HighlightEditorViewModel>(p => p.SetCharacteristicRuleFieldCommand);
        public static readonly PropertyPath RemoveCharacteristicCommandProperty = GetPropertyPath<HighlightEditorViewModel>(p => p.RemoveCharacteristicCommand);
        public static readonly PropertyPath RemoveCharacteristicRuleCommandProperty = GetPropertyPath<HighlightEditorViewModel>(p => p.RemoveCharacteristicRuleCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the currently selected Highlight
        /// </summary>
        public Highlight SelectedHighlight
        {
            get
            {
                return _selectedHighlight;
            }
            set
            {
                var oldValue = _selectedHighlight;
                _selectedHighlight = value;

                OnPropertyChanged(SelectedHighlightProperty);
                OnSelectedHighlightChanged(oldValue, _selectedHighlight);
            }
        }

        /// <summary>
        /// Returns the collection of filters for the current highlight.
        /// </summary>
        public ReadOnlyBulkObservableCollection<HighlightFilter> CurrentHighlightFilters
        {
            get
            {
                if (_currentHighlightFiltersRO == null)
                {
                    _currentHighlightFiltersRO = new ReadOnlyBulkObservableCollection<HighlightFilter>(_currentHighlightFilters);
                }
                return _currentHighlightFiltersRO;
            }
        }

        /// <summary>
        /// Returns the collection of characteristics for the current highlight.
        /// </summary>
        public HighlightCharacteristicGroupingCollection CharacteristicGroups
        {
            get { return _characteristicGroups; }
        }

        /// <summary>
        /// Gets the window title for the editor
        /// </summary>
        public String WindowTitle
        {
            get { return _windowTitle; }
            private set
            {
                _windowTitle = value;
                OnPropertyChanged(WindowTitleProperty);
            }
        }

        /// <summary>
        /// Sets a text if the planogram is not active
        /// </summary>
        public Boolean IsPlanogramActive
        {
            get { return _isPlanogramActive; }
            private set
            {
                _isPlanogramActive = value;
                OnPropertyChanged(IsPlanogramActiveProperty);
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Event to notify that a highlight set has been requested.
        /// </summary>
        public event EventHandler<HighlightEditorEventArgs> SetRequested;

        private void OnSetRequested(Highlight setting, Boolean setToAll)
        {
            if (SetRequested != null)
            {
                if (_highlightItemId == null)
                    {
                    setting.Name = null;
                    }
                
                HighlightItem item = null;
                if (setting != null)
                {
                    item = new HighlightItem(setting, _highlightItemId);
                }

                SetRequested(this, new HighlightEditorEventArgs(item, setToAll));
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="currentActiveSetting"></param>
        /// <param name="activePlanogram"></param>
        public HighlightEditorViewModel(HighlightItem currentActiveSetting, PlanogramView activePlanogram)
        {
            //get permissions for current repository.
            _itemRepositoryPerms = (App.ViewState.IsConnectedToRepository) ?
                new ModelPermission<Highlight>(Highlight.GetUserPermissions())
                : ModelPermission<Highlight>.DenyAll();



            _selectedHighlight = Highlight.NewHighlight();
            MarkAsCustom();


            _currentHighlightFilters.BulkCollectionChanged += CurrentHighlightFilters_BulkCollectionChanged;
            _characteristicGroups.BulkCollectionChanged += CharacteristicGroups_BulkCollectionChanged;
            _selectedHighlight.PropertyChanged += SelectedHighlight_PropertyChanged;

            _activePlanogram = activePlanogram;

            //check the active setting to determine what should be selected
            if (activePlanogram != null)
            {
                if (currentActiveSetting != null)
                {
                    this.SelectedHighlight = currentActiveSetting.ToHighlight();
                    MarkFromItem(currentActiveSetting);
                }
                else
                {
                    this.SelectedHighlight = _selectedHighlight;
                    MarkAsCustom();
                }
                IsPlanogramActive = false;
            }
            else
            {
                IsPlanogramActive = true;
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when the selected setting changes
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        private void OnSelectedHighlightChanged(Highlight oldValue, Highlight newValue)
        {
            if (oldValue != null)
            {
                oldValue.PropertyChanged -= SelectedHighlight_PropertyChanged;
                oldValue.ChildChanged -= SelectedHighlight_ChildChanged;
                oldValue.Filters.BulkCollectionChanged -= SelectedHighlight_FiltersBulkCollectionChanged;
                _characteristicGroups.Dettach();
            }

            if (newValue != null)
            {
                newValue.PropertyChanged += SelectedHighlight_PropertyChanged;
                newValue.ChildChanged += SelectedHighlight_ChildChanged;
                newValue.Filters.BulkCollectionChanged += SelectedHighlight_FiltersBulkCollectionChanged;
                _characteristicGroups.Attach(newValue.Characteristics);

                CreateSettingGroups(newValue, _activePlanogram, /*clearExisting*/false);

            }

            SelectedHighlight_FiltersBulkCollectionChanged(newValue, new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));

            EnsureBlankRows();
        }

        /// <summary>
        /// Reponds to a property change on the selected setting.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedHighlight_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != Highlight.NameProperty.Name)
            {
                if (e.PropertyName == Highlight.MethodTypeProperty.Name)
                {
                    this.SelectedHighlight.Groups.Clear();
                    EnsureBlankRows();
                }
                CreateSettingGroups(this.SelectedHighlight, _activePlanogram, /*ClearExisting*/false);
            }
        }

        /// <summary>
        /// Called when a child of the selected setting changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedHighlight_ChildChanged(object sender, ChildChangedEventArgs e)
        {
            Boolean updateGroups = false;

            if (e.PropertyChangedArgs != null)
            {
                if (e.ChildObject is HighlightFilter)
                {
                    updateGroups = true;
                    MarkAsCustom();

                    if (e.PropertyChangedArgs.PropertyName == HighlightFilter.FieldProperty.Name)
                    {
                        EnsureBlankRows();
                    }
                }
                else if (e.ChildObject is HighlightCharacteristicRule)
                {
                    updateGroups = true;
                    MarkAsCustom();

                    if (e.PropertyChangedArgs.PropertyName == HighlightCharacteristicRule.FieldProperty.Name)
                    {
                        EnsureBlankRows();
                    }
                }
                else if (e.ChildObject is HighlightGroup)
                {
                    MarkAsCustom();
                }
            }
            else if (e.CollectionChangedArgs != null)
            {
                if (e.ChildObject is HighlightFilterList
                    || e.ChildObject is HighlightCharacteristicList
                    || e.ChildObject is HighlightCharacteristicRuleList)
                {
                    updateGroups = true;
                }

            }

            if (e.ChildObject is HighlightCharacteristic //HighlightCharacteristicGrouping
                && e.PropertyChangedArgs != null && SelectedHighlight != null && SelectedHighlight.MethodType == HighlightMethodType.Characteristic)
            {
                if (e.PropertyChangedArgs.PropertyName == HighlightCharacteristic.NameProperty.Name)
                {
                    Int32 idx = SelectedHighlight.Characteristics.IndexOf((HighlightCharacteristic)e.ChildObject);
                    HighlightGroup hg = SelectedHighlight.Groups[idx];

                    hg.DisplayName = e.ChildObject.ToString();
                }
            }

            if (e.ChildObject is HighlightGroup //HighlightGrouping
                && e.PropertyChangedArgs != null && SelectedHighlight != null && SelectedHighlight.MethodType == HighlightMethodType.Characteristic)
            {
                if (e.PropertyChangedArgs.PropertyName == HighlightGroup.DisplayNameProperty.Name)
                {
                    Int32 idx = SelectedHighlight.Groups.IndexOf((HighlightGroup)e.ChildObject);

                    HighlightGroup hg = SelectedHighlight.Groups[idx];
                    HighlightCharacteristicGrouping hcg = CharacteristicGroups.Count >= (idx + 1) ? CharacteristicGroups[idx] : null;

                    if (hcg != null)
                    {
                        hcg.Model.Name = ((HighlightGroup)e.ChildObject).DisplayName;
                    }

                    //Make sure we update displayed preview object also
                    hg.Name = ((HighlightGroup)e.ChildObject).DisplayName;
                }
            }

            //update the preview
            if (updateGroups)
            {
                CreateSettingGroups(this.SelectedHighlight, _activePlanogram, /*ClearExisting*/false);
            }
        }

        /// <summary>
        /// Called whenever the current highlight filters collection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedHighlight_FiltersBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (HighlightFilter h in e.ChangedItems)
                    {
                        if (!_currentHighlightFilters.Contains(h))
                        {
                            _currentHighlightFilters.Add(h);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (HighlightFilter h in e.ChangedItems)
                    {
                        _currentHighlightFilters.Remove(h);
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        _currentHighlightFilters.Clear();
                        if (sender != null)
                        {
                            foreach (HighlightFilter h in ((Highlight)sender).Filters)
                            {
                                _currentHighlightFilters.Add(h);
                            }
                        }
                    }
                    break;
            }

            //ensure there is still a blank row in the collection
            EnsureBlankRows();
        }

        /// <summary>
        /// Called whenever a property changes on a highlight filter.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HighlightFilter_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            HighlightFilter filter = (HighlightFilter)sender;
            if (filter.Parent == null)
            {
                if (!String.IsNullOrEmpty(filter.Field))
                {
                    //the filter was the blank row which is now no longer blank
                    // so add to the main collection.
                    this.SelectedHighlight.Filters.Add(filter);
                }
            }
        }

        /// <summary>
        /// Called whenever the CurrentHighlightFilters collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CurrentHighlightFilters_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (HighlightFilter h in e.ChangedItems)
                    {
                        h.PropertyChanged += HighlightFilter_PropertyChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (HighlightFilter h in e.ChangedItems)
                    {
                        h.PropertyChanged -= HighlightFilter_PropertyChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    foreach (HighlightFilter h in e.ChangedItems)
                    {
                        h.PropertyChanged -= HighlightFilter_PropertyChanged;
                    }
                    foreach (HighlightFilter h in _currentHighlightFilters)
                    {
                        h.PropertyChanged += HighlightFilter_PropertyChanged;
                    }
                    break;
            }
        }

        /// <summary>
        /// Called whenever the CharacteristicGroups collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CharacteristicGroups_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Remove:
                case NotifyCollectionChangedAction.Reset:
                    if (e.ChangedItems != null)
                    {
                        foreach (HighlightCharacteristicGrouping group in e.ChangedItems)
                        {
                            group.Dispose();
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Called when the window is closing
        /// </summary>
        protected override void OnWindowClosing(CancelEventArgs e, Boolean isCrossClick)
        {
            //Unlock the currently selected highlight file
            if (_selectedHighlight != null
                && !_selectedHighlight.IsNew
                && _selectedHighlight.Id is String)
            {
                Highlight.UnlockHighlightByFileName((String)_selectedHighlight.Id);
            }

            base.OnWindowClosing(e, isCrossClick);
        }

        #endregion

        #region Commands

        #region Open Command

        private RelayCommand _openCommand;

        /// <summary>
        /// Opens a highlight from a directory
        /// </summary>
        public RelayCommand OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand(
                        p => Open_Executed())
                    {
                        FriendlyName = CommonMessage.Generic_Open,
                        SmallIcon = CommonImageResources.Open_16,
                    };
                    RegisterCommand(_openCommand);
                }
                return _openCommand;
            }
        }

        private void Open_Executed()
        {
            if (this.OpenFromRepositoryCommand.CanExecute())
            {
                this.OpenFromRepositoryCommand.Execute();
            }
            else
            {
                this.OpenFromFileCommand.Execute();
            }

        }

        #endregion

        #region OpenFromRepository

        private RelayCommand _openFromRepositoryCommand;

        /// <summary>
        /// Allows the user to select a repository template to load data from.
        /// </summary>
        public RelayCommand OpenFromRepositoryCommand
        {
            get
            {
                if (_openFromRepositoryCommand == null)
                {
                    _openFromRepositoryCommand = new RelayCommand(
                        o => OpenFromRepository_Executed(o),
                        o => OpenFromRepository_CanExecute())
                    {
                        FriendlyName = CommonMessage.Generic_OpenFromRepository
                    };
                    RegisterCommand(_openFromRepositoryCommand);
                }
                return _openFromRepositoryCommand;
            }
        }

        private Boolean OpenFromRepository_CanExecute()
        {
            //must be connected
            if (!App.ViewState.IsConnectedToRepository)
            {
                this.OpenFromRepositoryCommand.DisabledReason = CommonMessage.Generic_NoRepositoryConnection;
                return false;
            }

            //must have open permission
            if (!_itemRepositoryPerms.CanFetch)
            {
                this.OpenFromRepositoryCommand.DisabledReason = CommonMessage.Generic_NoFetchPermission;
                return false;
            }

            return true;
        }

        private void OpenFromRepository_Executed(Object args)
        {
            //Get the template id to load.
            Object id = args;
            if (id == null && !GetUserSelectionForProductHighlightTemplate(out id)) return;

            LoadProductHighlightTemplate(id);
        }

        #endregion

        #region OpenFromFile

        private RelayCommand _openFromFileCommand;

        /// <summary>
        /// Allows the user to select a file template to load data from.
        /// </summary>
        public RelayCommand OpenFromFileCommand
        {
            get
            {
                if (_openFromFileCommand == null)
                {
                    _openFromFileCommand = new RelayCommand(p => OpenFromFile_Executed(p))
                    {
                        FriendlyName = CommonMessage.Generic_OpenFromFile
                    };
                    RegisterCommand(_openFromFileCommand);
                }
                return _openFromFileCommand;
            }
        }


        private void OpenFromFile_Executed(Object args)
        {
            //show the file dialog if no args passed in.
            String file = args as String;
            if (String.IsNullOrEmpty(file)) file = HighlightUIHelper.ShowOpenFileDialog();
            if (String.IsNullOrEmpty(file)) return;

            //Fetch the highlight as readonly.
            base.ShowWaitCursor(true);
            try
            {
                //unlock the original file as we dont need to hold it
                using (Highlight label = Highlight.FetchByFilename(file, /*asReadOnly*/true))
                {
                    this.SelectedHighlight = label.Copy();
                    MarkAsExisting(label);
                }
            }
            catch (Exception ex)
            {
                base.ShowWaitCursor(false);
                LocalHelper.RecordException(ex);
                CommonHelper.GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                return;
            }
            base.ShowWaitCursor(false);

        }

        #endregion

        #region SaveAs Command

        private RelayCommand _saveAsCommand;

        /// <summary>
        /// Saves changes made to a new highlight setting and loads new as current
        /// /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(p),
                        p => Save_CanExecute())
                    {
                        FriendlyName = CommonMessage.Generic_SaveAs,
                        SmallIcon = CommonImageResources.SaveAs_16,
                        FriendlyDescription = Message.LabelEditor_SaveAs_Desc,
                        Icon = CommonImageResources.SaveAs_32
                    };
                    RegisterCommand(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }

        private Boolean Save_CanExecute()
        {
            if (!this.SelectedHighlight.Characteristics.IsValid)
            {

                this.SaveAsCommand.DisabledReason = CommonMessage.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            return true;
        }

        private void SaveAs_Executed(object args)
        {
            if (this.SaveAsToRepositoryCommand.CanExecute())
            {
                this.SaveAsToRepositoryCommand.Execute();
            }
            else
            {
                this.SaveAsToFileCommand.Execute();
            }
        }

        #endregion

        #region SaveAsToRepository

        private RelayCommand _saveAsToRepositoryCommand;

        /// <summary>
        /// Saves the currrent template as a new template in the repository.
        /// </summary>
        public RelayCommand SaveAsToRepositoryCommand
        {
            get
            {
                if (_saveAsToRepositoryCommand == null)
                {
                    _saveAsToRepositoryCommand = new RelayCommand(
                            o => SaveAsToRepository_Executed(),
                        o => SaveAsToRepository_CanExecute())
                    {
                        FriendlyName = CommonMessage.Generic_SaveToRepository
                    };
                    RegisterCommand(_saveAsToRepositoryCommand);

                }
                return _saveAsToRepositoryCommand;
            }
        }

        private Boolean SaveAsToRepository_CanExecute()
        {
            if (!App.ViewState.IsConnectedToRepository)
            {
                this.SaveAsToRepositoryCommand.DisabledReason = CommonMessage.Generic_NoRepositoryConnection;
                return false;
            }

            //must have permission
            if (!_itemRepositoryPerms.CanCreate)
            {
                this.SaveAsToRepositoryCommand.DisabledReason = CommonMessage.Generic_NoCreatePermission;
                return false;
            }

            return true;
        }

        /// <summary>
        ///     Invoked whenever the <see cref="SaveAsToRepositoryCommand" /> is executed.
        /// </summary>
        private void SaveAsToRepository_Executed()
        {
            String copyName;

            HighlightInfoList existingItems = HighlightInfoList.FetchByEntityId(App.ViewState.EntityId);
            Predicate<String> isUniqueCheck =
               (s) => { return !existingItems.Select(p => p.Name).Contains(s, StringComparer.OrdinalIgnoreCase); };

            Boolean nameAccepted = GetWindowService().PromptForSaveAsName(isUniqueCheck, String.Empty, out copyName);
            if (!nameAccepted) return;

            ShowWaitCursor(true);

            //copy the item and rename
            Highlight itemCopy = this.SelectedHighlight.Copy();
            itemCopy.Name = copyName;

            //Perform the save.
            ShowWaitCursor(true);

            try
            {
                //Make sure the entityId is set
                itemCopy.EntityId = App.ViewState.EntityId;

                this.SelectedHighlight = itemCopy.SaveAs();
                MarkAsExisting(this.SelectedHighlight);
            }
            catch (Exception ex)
            {
                base.ShowWaitCursor(false);
                LocalHelper.RecordException(ex);
                CommonHelper.GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Save);
                return;
            }

            ShowWaitCursor(false);
        }

        #endregion

        #region SaveToFile


        private RelayCommand _saveAsToFileCommand;

        /// <summary>
        /// Saves the current template as a new file template.
        /// </summary>
        public RelayCommand SaveAsToFileCommand
        {
            get
            {
                if (_saveAsToFileCommand == null)
                {
                    _saveAsToFileCommand = new RelayCommand(p => SaveAsToFile_Executed(p))
                    {
                        FriendlyName = CommonMessage.Generic_SaveToFile
                    };
                    RegisterCommand(_saveAsToFileCommand);
                }
                return _saveAsToFileCommand;
            }
        }

        private void SaveAsToFile_Executed(Object args)
        {
            Highlight itemToSave = this.SelectedHighlight;

            //get the file path to save to.
            String filePath = args as String;
            if (String.IsNullOrEmpty(filePath)) filePath = HighlightUIHelper.ShowSaveAsFileDialog();
            if (String.IsNullOrEmpty(filePath)) return;


            // save to file
            base.ShowWaitCursor(true);
            try
            {
                itemToSave = itemToSave.SaveAsFile(filePath);
            }
            catch (Exception ex)
            {
                base.ShowWaitCursor(false);
                LocalHelper.RecordException(ex);
                CommonHelper.GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Save);
                return;
            }
            base.ShowWaitCursor(false);

            //reselect the label
            if (itemToSave != null)
            {
                this.SelectedHighlight = itemToSave.Copy();
                MarkAsExisting(itemToSave);
            }

            //unlock the file again as we don't need to keep hold of it.
            itemToSave.Dispose();
        }

        #endregion

        #region ApplyToCurrentView Command

        private RelayCommand _applyToCurrentViewCommand;

        /// <summary>
        /// Applies the selected highlight to the current view.
        /// </summary>
        public RelayCommand ApplyToCurrentViewCommand
        {
            get
            {
                if (_applyToCurrentViewCommand == null)
                {
                    _applyToCurrentViewCommand = new RelayCommand(
                        p => ApplyToCurrent_Executed(),
                        p => ApplyToCurrent_CanExecute())
                    {
                        FriendlyName = Message.HighlightEditor_ApplyToCurrentView,
                    };
                    RegisterCommand(_applyToCurrentViewCommand);
                }
                return _applyToCurrentViewCommand;
            }
        }

        private Boolean ApplyToCurrent_CanExecute()
        {
            if (_activePlanogram == null)
            {
                return false;
            }

            return true;
        }

        private void ApplyToCurrent_Executed()
        {
            base.ShowWaitCursor(true);

            //Check if the highlight is custom
            if (_windowTitle == null)
            {
                //Sets the custom view 
                this.SelectedHighlight.Name = null;

                CancelCommand.Execute();
                OnSetRequested(this.SelectedHighlight, false);
            }
            else
            {
                CancelCommand.Execute();
                OnSetRequested(this.SelectedHighlight, false);
            }
            base.ShowWaitCursor(false);
        }

        #endregion

        #region ApplyToAllViews Command

        private RelayCommand _applyToAllViewsCommand;

        /// <summary>
        /// Applies the selected highlight to all views
        /// of the active planogram.
        /// </summary>
        public RelayCommand ApplyToAllViewsCommand
        {
            get
            {
                if (_applyToAllViewsCommand == null)
                {
                    _applyToAllViewsCommand = new RelayCommand(
                        p => ApplyToAllViews_Executed(),
                        p => ApplyToAllViews_CanExecute())
                    {
                        FriendlyName = Message.HighlightEditor_ApplyToAllViews,
                        SmallIcon = ImageResources.HighlightEditor_ApplyToAll_16,
                    };
                    RegisterCommand(_applyToAllViewsCommand);
                }
                return _applyToAllViewsCommand;
            }
        }

        private Boolean ApplyToAllViews_CanExecute()
        {
            if (_activePlanogram == null)
            {
                return false;
            }

            return true;
        }

        private void ApplyToAllViews_Executed()
        {
            base.ShowWaitCursor(true);

            //Check if the highlight is custom
            if (_windowTitle == null)
            {
                //Sets the custom view 
                this.SelectedHighlight.Name = null;

                CancelCommand.Execute();
                OnSetRequested(this.SelectedHighlight, true);
            }
            else
            {
                CancelCommand.Execute();
                OnSetRequested(this.SelectedHighlight, true);
            }
            base.ShowWaitCursor(false);
        }
        #endregion

        #region ClearCurrentView Command

        private RelayCommand _clearCurrentViewCommand;

        /// <summary>
        /// Applies the selected highlight to the current view.
        /// </summary>
        public RelayCommand ClearCurrentViewCommand
        {
            get
            {
                if (_clearCurrentViewCommand == null)
                {
                    _clearCurrentViewCommand = new RelayCommand(
                        p => ClearCurrent_Executed(),
                        p => ClearCurrent_CanExecute())
                    {
                        FriendlyName = Message.HighlightEditor_ClearCurrentView,
                    };
                    RegisterCommand(_clearCurrentViewCommand);
                }
                return _clearCurrentViewCommand;
            }
        }

        private Boolean ClearCurrent_CanExecute()
        {
            if (_activePlanogram == null)
            {
                return false;
            }

            return true;
        }

        private void ClearCurrent_Executed()
        {
            OnSetRequested(null, false);
            CloseWindow();
        }


        #endregion

        #region ClearAllViews Command

        private RelayCommand _clearAllViewsCommand;

        /// <summary>
        /// Applies the selected highlight to all views
        /// of the active planogram.
        /// </summary>
        public RelayCommand ClearAllViewsCommand
        {
            get
            {
                if (_clearAllViewsCommand == null)
                {
                    _clearAllViewsCommand = new RelayCommand(
                        p => ClearAllViews_Executed(),
                        p => ClearAllViews_CanExecute())
                    {
                        FriendlyName = Message.HighlightEditor_ClearAllViews,
                    };
                    RegisterCommand(_clearAllViewsCommand);
                }
                return _clearAllViewsCommand;
            }
        }

        private Boolean ClearAllViews_CanExecute()
        {
            if (_activePlanogram == null)
            {
                return false;
            }

            return true;
        }

        private void ClearAllViews_Executed()
        {
            OnSetRequested(null, true);
            CloseWindow();
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Closes the window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = CommonMessage.Generic_Cancel,
                    };
                    RegisterCommand(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {

            OnSelectedHighlightChanged(this.SelectedHighlight, null);

            _currentHighlightFilters.BulkCollectionChanged -= CurrentHighlightFilters_BulkCollectionChanged;
            _characteristicGroups.BulkCollectionChanged -= CharacteristicGroups_BulkCollectionChanged;
            _characteristicGroups.Dispose();

            if (_selectedHighlight != null)
            {
                if (!_selectedHighlight.IsNew)
                {
                    if (_selectedHighlight.Id is String)
                    {
                        Highlight.UnlockHighlightByFileName((String)_selectedHighlight.Id);
                    }
                }
            }
            CloseWindow();


        }

        #endregion

        #region SetFieldCommand

        private RelayCommand _setFieldCommand;

        /// <summary>
        /// Shows the window to populate the field specified by the param
        /// PARM: The field property info.
        /// </summary>
        public RelayCommand SetFieldCommand
        {
            get
            {
                if (_setFieldCommand == null)
                {
                    _setFieldCommand = new RelayCommand(
                        p => SetField_Executed(p),
                        p => SetField_CanExecute(p))
                    {
                        FriendlyName = CommonMessage.Generic_Ellipsis,
                        FriendlyDescription = CommonMessage.HighlightSetupContent_SetField_Desc
                    };
                    RegisterCommand(_setFieldCommand);
                }
                return _setFieldCommand;

            }
        }

        private Boolean SetField_CanExecute(Object args)
        {
            if (this.SelectedHighlight == null)
            {
                return false;
            }

            if (args == null)
            {
                return false;
            }

            return true;
        }

        private void SetField_Executed(Object args)
        {
            Csla.Core.IPropertyInfo fieldPropertyInfo = args as Csla.Core.IPropertyInfo;
            if (fieldPropertyInfo != null)
            {

                String fieldValue = null;
                if (fieldPropertyInfo == Highlight.Field1Property)
                {
                    fieldValue = this.SelectedHighlight.Field1;
                }
                else if (fieldPropertyInfo == Highlight.Field2Property)
                {
                    fieldValue = this.SelectedHighlight.Field2;
                }

                if (_activePlanogram != null)
                {
                    FieldSelectorViewModel viewModel = new FieldSelectorViewModel(
                        FieldSelectorInputType.Formula, FieldSelectorResolveType.SingleValue,
                        fieldValue, PlanItemHelper.GetPlanogramFieldSelectorGroups(_activePlanogram.Model));
                    
                    GetWindowService().ShowDialog<FieldSelectorWindow>(viewModel);


                    if (viewModel.DialogResult == true)
                    {
                        if (fieldPropertyInfo == Highlight.Field1Property)
                        {
                            this.SelectedHighlight.Field1 = viewModel.FieldText;
                        }
                        else if (fieldPropertyInfo == Highlight.Field2Property)
                        {
                            this.SelectedHighlight.Field2 = viewModel.FieldText;
                        }
                        if (_activePlanogram == null)
                        {
                            IsPlanogramActive = true;
                        }
                        else
                        {
                            IsPlanogramActive = false;
                        }
                    }
                }
            }
        }

        #endregion

        #region SetFilterFieldCommand

        private RelayCommand _setFilterFieldCommand;

        /// <summary>
        /// Shows the window to populate the group field
        /// </summary>
        public RelayCommand SetFilterFieldCommand
        {
            get
            {
                if (_setFilterFieldCommand == null)
                {
                    _setFilterFieldCommand = new RelayCommand(
                        p => SetFilterField_Executed(p),
                        p => SetFilterField_CanExecute(p))
                    {
                        FriendlyName = CommonMessage.Generic_Ellipsis,
                        FriendlyDescription = CommonMessage.HighlightSetupContent_SetField_Desc
                    };
                    RegisterCommand(_setFilterFieldCommand);
                }
                return _setFilterFieldCommand;

            }
        }

        private Boolean SetFilterField_CanExecute(Object args)
        {
            if (args == null)
            {
                return false;
            }

            return true;
        }

        private void SetFilterField_Executed(Object args)
        {
            HighlightFilter filter = args as HighlightFilter;
            if (filter != null)
            {
                FieldSelectorViewModel viewModel = new FieldSelectorViewModel(
                       FieldSelectorInputType.Formula,
                       FieldSelectorResolveType.SingleValue,
                       filter.Field, PlanItemHelper.GetPlanogramFieldSelectorGroups(_activePlanogram.Model));

                GetWindowService().ShowDialog<FieldSelectorWindow>(viewModel);

                if (viewModel.DialogResult == true)
                {
                    filter.Field = viewModel.FieldText;
                }

            }
        }

        #endregion

        #region RemoveFilterCommand

        private RelayCommand _removeFilterCommand;

        /// <summary>
        /// Removes the given filter.
        /// </summary>
        public RelayCommand RemoveFilterCommand
        {
            get
            {
                if (_removeFilterCommand == null)
                {
                    _removeFilterCommand = new RelayCommand(
                        p => RemoveFilter_Executed(p),
                        p => RemoveFilter_CanExecute(p))
                    {
                        FriendlyName = CommonMessage.Generic_Remove,
                        SmallIcon = CommonImageResources.Delete_16
                    };
                    RegisterCommand(_removeFilterCommand);
                }
                return _removeFilterCommand;
            }
        }

        private Boolean RemoveFilter_CanExecute(Object args)
        {
            if (args == null)
            {
                return false;
            }
            return true;
        }

        private void RemoveFilter_Executed(Object args)
        {
            HighlightFilter filter = args as HighlightFilter;
            if (filter != null)
            {
                Highlight parentSetting = filter.Parent;
                if (parentSetting != null)
                {
                    parentSetting.Filters.Remove(filter);
                }
            }

            EnsureBlankRows();
        }

        #endregion

        #region AddCharacteristicCommand

        private RelayCommand _addCharacteristicCommand;

        /// <summary>
        /// Adds a new item for characteristic analysis.
        /// </summary>
        public RelayCommand AddCharacteristicCommand
        {
            get
            {
                if (_addCharacteristicCommand == null)
                {
                    _addCharacteristicCommand = new RelayCommand(
                        p => AddCharacteristic_Executed(),
                        p => AddCharacteristic_CanExecute())
                    {
                        FriendlyName = CommonMessage.Generic_Add,
                        SmallIcon = CommonImageResources.Add_16
                    };
                    RegisterCommand(_addCharacteristicCommand);
                }
                return _addCharacteristicCommand;
            }
        }

        private Boolean AddCharacteristic_CanExecute()
        {
            if (this.SelectedHighlight == null)
            {
                return false;
            }
            return true;
        }

        private void AddCharacteristic_Executed()
        {
            Int32 groupNum = this.SelectedHighlight.Characteristics.Count + 1;

            HighlightCharacteristic newCharacteristic = HighlightCharacteristic.NewHighlightCharacteristic();
            newCharacteristic.Name = String.Format(CultureInfo.CurrentCulture, CommonMessage.HighlightSetupContent_NewCharacteristicNameFormat, groupNum);
            this.SelectedHighlight.Characteristics.Add(newCharacteristic);
        }

        #endregion

        #region SetCharacteristicRuleFieldCommand

        private RelayCommand _setCharacteristicRuleFieldCommand;

        /// <summary>
        /// Sets the field value for the given aspect rule
        /// PARAM: HighlightCharacteristicRule
        /// </summary>
        public RelayCommand SetCharacteristicRuleFieldCommand
        {
            get
            {
                if (_setCharacteristicRuleFieldCommand == null)
                {
                    _setCharacteristicRuleFieldCommand = new RelayCommand(
                        p => SetCharacteristicRuleField_Executed(p))
                    {
                        FriendlyName = CommonMessage.Generic_Ellipsis,
                        FriendlyDescription = CommonMessage.HighlightSetupContent_SetField_Desc
                    };
                    RegisterCommand(_setCharacteristicRuleFieldCommand);
                }
                return _setCharacteristicRuleFieldCommand;

            }
        }

        private void SetCharacteristicRuleField_Executed(Object args)
        {
            HighlightCharacteristicRule rule = args as HighlightCharacteristicRule;
            if (rule != null)
            {

                FieldSelectorViewModel viewModel = new FieldSelectorViewModel(
                    FieldSelectorInputType.Formula,
                    FieldSelectorResolveType.SingleValue,
                    rule.Field, PlanItemHelper.GetPlanogramFieldSelectorGroups(_activePlanogram.Model));

                GetWindowService().ShowDialog<FieldSelectorWindow>(viewModel);

                if (viewModel.DialogResult == true)
                {
                    rule.Field = viewModel.FieldText;
                }

            }
        }

        #endregion

        #region RemoveCharacteristicCommand

        private RelayCommand _removeCharacteristicCommand;

        /// <summary>
        /// Removes the given aspect
        ///  PARAM: HighlightCharacteristic
        /// </summary>
        public RelayCommand RemoveCharacteristicCommand
        {
            get
            {
                if (_removeCharacteristicCommand == null)
                {
                    _removeCharacteristicCommand = new RelayCommand(
                        p => RemoveCharacteristic_Executed(p),
                        p => RemoveCharacteristic_CanExecute(p))
                    {
                        FriendlyName = CommonMessage.Generic_Delete,
                        SmallIcon = CommonImageResources.Delete_16
                    };
                    RegisterCommand(_removeCharacteristicCommand);
                }
                return _removeCharacteristicCommand;
            }
        }

        private Boolean RemoveCharacteristic_CanExecute(Object args)
        {
            if (args == null)
            {
                return false;
            }

            //Cannot remove the last group
            if (this.SelectedHighlight.Characteristics.Count == 1)
            {
                return false;
            }

            return true;
        }

        private void RemoveCharacteristic_Executed(Object args)
        {
            HighlightCharacteristic modelToRemove = null;

            HighlightCharacteristicGrouping view = args as HighlightCharacteristicGrouping;
            if (view != null)
            {
                modelToRemove = view.Model;
            }
            else
            {
                modelToRemove = args as HighlightCharacteristic;
            }

            if (modelToRemove != null)
            {
                modelToRemove.Parent.Characteristics.Remove(modelToRemove);
            }
        }

        #endregion

        #region RemoveCharacteristicRuleCommand

        private RelayCommand _removeCharacteristicRuleCommand;

        /// <summary>
        /// Removes the given filter.
        /// </summary>
        public RelayCommand RemoveCharacteristicRuleCommand
        {
            get
            {
                if (_removeCharacteristicRuleCommand == null)
                {
                    _removeCharacteristicRuleCommand = new RelayCommand(
                        p => RemoveCharacteristicRule_Executed(p),
                        p => RemoveCharacteristicRule_CanExecute(p))
                    {
                        FriendlyName = CommonMessage.Generic_Remove,
                        SmallIcon = CommonImageResources.Delete_16
                    };
                    RegisterCommand(_removeCharacteristicRuleCommand);
                }
                return _removeCharacteristicRuleCommand;
            }
        }

        private Boolean RemoveCharacteristicRule_CanExecute(Object args)
        {
            if (args == null)
            {
                return false;
            }
            return true;
        }

        private void RemoveCharacteristicRule_Executed(Object args)
        {
            HighlightCharacteristicRule rule = args as HighlightCharacteristicRule;
            if (rule != null)
            {
                HighlightCharacteristic parentSetting = rule.Parent;
                if (parentSetting != null)
                {
                    parentSetting.Rules.Remove(rule);
                }
            }

            EnsureBlankRows();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Adds a blank rows if required.
        /// </summary>
        private void EnsureBlankRows()
        {
            Highlight highlight = this.SelectedHighlight;
            if (highlight != null)
            {
                //add a new blank filter if required.
                if (_currentHighlightFilters.Count == 0 ||
                    !_currentHighlightFilters.Any(c => c.Parent == null))
                {
                    _currentHighlightFilters.Add(HighlightFilter.NewHighlightFilter());
                }

                if (highlight.MethodType == HighlightMethodType.Characteristic)
                {
                    if (highlight.Characteristics.Count == 0)
                    {
                        this.AddCharacteristicCommand.Execute();
                    }

                }
            }
        }

        /// <summary>
        /// Marks the currently selected highlight as as existing item.
        /// </summary>
        /// <param name="highlight"></param>
        private void MarkAsExisting(Highlight highlight)
        {
            this.WindowTitle = highlight.Name;

            //set the item id 
            _highlightItemId = highlight.Id;
        }

        /// <summary>
        /// Marks the currently selected highlight as as existing item.
        /// </summary>
        /// <param name="highlight"></param>
        private void MarkFromItem(HighlightItem item)
        {
            if (item.Id != null)
            {
                this.WindowTitle = item.Name;
            }
            else
            {
                this.WindowTitle = Message.LabelEditor_Custom;
            }

            //set the item id 
            _highlightItemId = item.Id;
        }

        /// <summary>
        /// Marks the currently selected highlight as custom.
        /// </summary>
        private void MarkAsCustom()
        {
            this.WindowTitle = Message.LabelEditor_Custom;

            //set the item id to null.
            _highlightItemId = null;
        }

        /// <summary>
        /// Processes the settings groups for the given setting.
        /// </summary>
        /// <param name="setting"></param>
        private static void CreateSettingGroups(Highlight setting, PlanogramView plan, Boolean clearExisting)
        {
            //only process if we have a setting and an active plan.
            if (setting != null && plan != null)
            {
                if (clearExisting)
                {
                    setting.Groups.Clear();
                }


                //TODO: Derive what items we are highlighting from the setting.
                // For now however we shall just do positions.
                List<PlanogramPosition> processItems = plan.Model.Positions.ToList();

                HighlightItem highlightItem = new HighlightItem(setting);
                List<PlanogramHighlightResultGroup> expectedGroups = PlanogramHighlightHelper.DetermineHighlightGroups(processItems, highlightItem);

                //Create group
                List<HighlightGroup> oldGroups = setting.Groups.ToList();
                setting.Groups.Clear();

                Int32 orderNumber = 1;
                Int32 unspecifiedLabelCount = 0;

                foreach (PlanogramHighlightResultGroup entry in expectedGroups)
                {
                    HighlightGroup group = HighlightGroup.NewHighlightGroup();

                    //Ensure group labels are unique. First is called "Unspecified", subsequent are "Unspecified (1)" etc
                    if (String.IsNullOrEmpty(entry.Label.Trim()))
                    {
                        String unspecifiedLabel = CommonMessage.HighlightSetupContent_UnspecifiedGroupLabel;
                        if (unspecifiedLabelCount > 0)
                        {
                            unspecifiedLabel = String.Format("{0} ({1})", unspecifiedLabel, unspecifiedLabelCount);
                        }
                        group.Name = unspecifiedLabel;
                        unspecifiedLabelCount++;
                    }
                    else
                    {
                        group.Name = entry.Label;
                    }
                    group.FillColour = entry.Colour;
                    setting.Groups.Add(group);
                    
                    group.Order = orderNumber;
                    orderNumber++;
                }

            //setting.Groups.RemoveList(oldGroups);
            }
        }

        /// <summary>
        /// Get the user's selection of Product Highlight template to load.
        /// </summary>
        /// <param name="id">Identifier for the Product Highlight template to be loaded.</param>
        /// <returns></returns>
        /// <remarks>Opens a new <see cref="GenericSingleItemSelectorWindow"/> for the user to select a template from.
        /// <para />Will return <c>false</c> if there was a network connection issue.
        /// </remarks>
        private Boolean GetUserSelectionForProductHighlightTemplate(out Object id)
        {
            id = null;

            HighlightInfoList infos;
            if (!TryFetchHighlightInfos(out infos)) return false;

            var win = new GenericSingleItemSelectorWindow
                      {
                          ItemSource = infos,
                          SelectionMode = DataGridSelectionMode.Single
                      };
            GetWindowService().ShowDialog<GenericSingleItemSelectorWindow>(win);
            if (win.DialogResult != true) return false;

            id = win.SelectedItems.Cast<HighlightInfo>().First().Id;
            return true;
        }

        /// <summary>
        /// Try to fetch the <see cref="HighlightInfoList"/> from the repository.
        /// </summary>
        /// <param name="infos">The collection of <see cref="HighlightInfo"/> items.</param>
        /// <returns></returns>
        /// <remarks>If a <see cref="DataPortalException"/> is thrown, notifies the user, records the exception and closes the repository connection.</remarks>
        private Boolean TryFetchHighlightInfos(out HighlightInfoList infos)
        {
            infos = null;

            ShowWaitCursor(true);
            try
            {
                infos = HighlightInfoList.FetchByEntityId(App.ViewState.EntityId);
            }
            catch (DataPortalException ex)
            {
                ShowWaitCursor(false);
                GetWindowService().ShowErrorMessage(Application_Database_ConnectionLostHeader, Application_Database_ConnectionLostDescription);
                RecordException(ex);
                App.ViewState.IsConnectedToRepository = false;
                return false;
            }
            ShowWaitCursor(false);

            return true;
        }

        /// <summary>
        /// Load the <see cref="Highlight"/> matching the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The id that identifies the <see cref="Highlight"/> that is to be loaded.</param>
        private void LoadProductHighlightTemplate(Object id)
        {
            ShowWaitCursor(true);
            try
            {
                SelectedHighlight = Highlight.FetchById(id);
                MarkAsExisting(SelectedHighlight);
            }
            catch (Exception ex)
            {
                ShowWaitCursor(false);
                RecordException(ex);
                GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                return;
            }

            ShowWaitCursor(false);
        }

        #endregion

        #region IDispose

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    OnSelectedHighlightChanged(this.SelectedHighlight, null);

                    _currentHighlightFilters.BulkCollectionChanged -= CurrentHighlightFilters_BulkCollectionChanged;
                    _characteristicGroups.BulkCollectionChanged -= CharacteristicGroups_BulkCollectionChanged;

                    _characteristicGroups.Dispose();


                    DisposeBase();
                }

                IsDisposed = true;
            }
        }

        #endregion

    }


    /// <summary>
    /// Event args used by the HighlightEditor window.
    /// </summary>
    public sealed class HighlightEditorEventArgs : EventArgs
    {
        #region Properties
        public HighlightItem HighlightView { get; private set; }
        public Boolean IsSetToAll { get; private set; }
        #endregion

        public HighlightEditorEventArgs(HighlightItem label, Boolean isSetToAll)
        {
            this.HighlightView = label;
            this.IsSetToAll = isSetToAll;
        }
    }

}
