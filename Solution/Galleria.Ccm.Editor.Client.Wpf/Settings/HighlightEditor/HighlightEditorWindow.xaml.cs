﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.Settings.HighlightEditor
{
    /// <summary>
    /// Interaction logic for HighlightEditorWindow.xaml
    /// </summary>
    public sealed partial class HighlightEditorWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(HighlightEditorViewModel), typeof(HighlightEditorWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public HighlightEditorViewModel ViewModel
        {
            get { return (HighlightEditorViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            HighlightEditorWindow senderControl = (HighlightEditorWindow)obj;

            if (e.OldValue != null)
            {
                HighlightEditorViewModel oldModel = (HighlightEditorViewModel)e.OldValue;
                oldModel.UnregisterWindowControl();
            }

            if (e.NewValue != null)
            {
                HighlightEditorViewModel newModel = (HighlightEditorViewModel)e.NewValue;
                newModel.RegisterWindowControl(senderControl);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public HighlightEditorWindow(HighlightEditorViewModel vm)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            this.ViewModel = vm;

            //set the help file reference 
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.Highlights);

            this.Loaded += HighlightEditor_Loaded;
        }

        #endregion

        #region Event Handlers

        private void HighlightEditor_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= HighlightEditor_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region window close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion


    }
}
