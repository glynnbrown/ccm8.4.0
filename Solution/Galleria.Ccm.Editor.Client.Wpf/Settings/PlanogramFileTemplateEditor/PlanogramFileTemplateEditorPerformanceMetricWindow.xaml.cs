﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.1)
// V8-28622 : D.Pleasance
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Helpers;
using System.Windows.Threading;
using Galleria.Ccm.Editor.Client.Wpf.Settings.PlanogramFileTemplateEditor;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Editor.Client.Wpf.Settings.PlanogramFileTemplateEditor
{
    /// <summary>
    /// Interaction logic for PlanogramFileTemplateEditorPerformanceMetricWindow.xaml
    /// </summary>
    public partial class PlanogramFileTemplateEditorPerformanceMetricWindow : ExtendedRibbonWindow
    {
        #region Constructors
        public PlanogramFileTemplateEditorPerformanceMetricWindow(PlanogramFileTemplateEditorViewModel viewModel, Boolean newMetric)
        {
            NewMetric = newMetric;
            ViewModel = viewModel;
            if (!newMetric)
            {
                OriginalMetric = ViewModel.SelectedPerformanceMetric.PerformanceMetric.Copy();
            }
            InitializeComponent();
        } 
        #endregion

        #region Properties
        public PlanogramFileTemplateEditorViewModel ViewModel { get; private set; }
        public PlanogramImportTemplatePerformanceMetric OriginalMetric { get; private set; }
        public Boolean NewMetric { get; private set; }
        #endregion

        #region Window Close

        public Boolean IsClosing
        {
            get;
            private set;
        }

        /// <summary>
        /// Called when the window close cross is pressed.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnCrossCloseRequested(System.ComponentModel.CancelEventArgs e)
        {
            IsClosing = true;

            this.ViewModel.MetricCancelCommand.Execute();
            base.OnCrossCloseRequested(e);
        }

        /// <summary>
        /// Called when this window is closing.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);
        }

        /// <summary>
        /// Disposes of the viewmodel when the window has closed.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
        }
        #endregion
    }
}