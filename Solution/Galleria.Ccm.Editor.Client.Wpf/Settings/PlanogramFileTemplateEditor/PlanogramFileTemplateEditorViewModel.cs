﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27804 : L.Ineson
//  Created
// V8-28219 : N.Haywood
//  changed PlanogramImportFileType.Spaceman to SpacemanV9
#endregion
#region Version History: (CCM 8.0.1)
// V8-28622 : D.Pleasance
//  Added PlanogramImportTemplatePerformanceMetrics
// V8-28909 : D.Pleasance
//  Amended so that read only metric id is updated upon removal of metrics
#endregion
#region Version History: (CCM 8.1.0)
// V8-29678 : D.Pleasance
//  Amended to validate required mappings
#endregion

#region Version History: CCM820

// V8-30756 : A.Silva
//  Added code to manage ProSpace and Apollo extensions.
// V8-30725 : M.Brumby
//  PCR01417 Import from Apollo
// V8-31079 : M.Brumby
//  Set default templates to the correct plan type
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows;
using Csla.Server;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Planograms.External;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Common.Wpf.Helpers;
using System.IO;
using CommonMessage = Galleria.Ccm.Common.Wpf.Resources.Language.Message;

namespace Galleria.Ccm.Editor.Client.Wpf.Settings.PlanogramFileTemplateEditor
{
    /// <summary>
    /// Viewmodel controller for PlanogramFileTemplateEditorWindow
    /// </summary>
    public sealed class PlanogramFileTemplateEditorViewModel : ViewModelAttachedControlObject<PlanogramFileTemplateEditorWindow>
    {
        public const string DefaultName = "<New>";
        #region Fields

        private Boolean? _dialogResult;

        private readonly ModelPermission<PlanogramImportTemplate> _itemRepositoryPerms;

        private PlanogramImportTemplate _selectedItem;
        private PlanogramFileTemplateEditorPerformanceMetricWindow _metricWindow;
        private PerformanceMetricRow _selectedPerformanceMetric;
        private String _mappingTemplateName;

        private ReadOnlyCollection<String> _availableVersions;
        private Dictionary<PlanogramFieldMappingType, ReadOnlyCollection<PlanogramImportTemplateFieldInfo>> _externalFieldDict;

        private readonly BulkObservableCollection<MappingRow> _mappingRows = new BulkObservableCollection<MappingRow>();
        private readonly BulkObservableCollection<PerformanceMetricRow> _performanceMetricRows = new BulkObservableCollection<PerformanceMetricRow>();
        private ReadOnlyBulkObservableCollection<PerformanceMetricRow> _performanceMetricRowsRO;

        #endregion

        #region Binding Property paths

        // properties
        public static readonly PropertyPath SelectedItemProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.SelectedItem);
        public static readonly PropertyPath MappingTemplateNameProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.MappingTemplateName);
        public static readonly PropertyPath SelectedFileTypeProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.SelectedFileType);
        public static readonly PropertyPath AvailableVersionsProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.AvailableVersions);
        public static readonly PropertyPath SelectedVersionProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.SelectedVersion);
        public static readonly PropertyPath ProductMappingsProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.ProductMappings);
        public static readonly PropertyPath ComponentMappingsProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.ComponentMappings);
        public static readonly PropertyPath BayMappingsProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.BayMappings);
        public static readonly PropertyPath PlanogramMappingsProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.PlanogramMappings);
        public static readonly PropertyPath PerformanceMetricsProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.PerformanceMetrics);
        public static readonly PropertyPath SelectedPerformanceMetricProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.SelectedPerformanceMetric);
        
        //Commands
        public static readonly PropertyPath ClearAllCommandProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.ClearAllMappingsCommand);
        public static readonly PropertyPath ClearMappingCommandProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.ClearMappingCommand);
        public static readonly PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.OpenCommand);
        public static readonly PropertyPath OpenFromFileCommandProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.OpenFromFileCommand);
        public static readonly PropertyPath OpenFromRepositoryCommandProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.OpenFromRepositoryCommand);
        public static readonly PropertyPath SaveAsCommandProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.SaveAsCommand);
        public static readonly PropertyPath SaveAsToRepositoryCommandProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.SaveAsToRepositoryCommand);
        public static readonly PropertyPath SaveAsToFileCommandProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.SaveAsToFileCommand);
        public static readonly PropertyPath OKCommandProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.OKCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath NewMetricCommandProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.NewMetricCommand);
        public static readonly PropertyPath ViewMetricCommandProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.ViewMetricCommand);
        public static readonly PropertyPath RemoveMetricCommandProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.RemoveMetricCommand);
        public static readonly PropertyPath MetricSaveCommandProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.MetricSaveCommand);
        public static readonly PropertyPath MetricCancelCommandProperty = WpfHelper.GetPropertyPath<PlanogramFileTemplateEditorViewModel>(p => p.MetricCancelCommand);

        #endregion

        #region Properties

        public DisplayUnitOfMeasureCollection DisplayUOMs
        {
            get
            {
                return App.ViewState.DisplayUnits;
            }
        }
        /// <summary>
        /// Returns the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;

                if (this.AttachedControl != null
                    /*&& !this.AttachedControl.IsClosing*/)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }

        /// <summary>
        /// Gets/Sets the currently selected template
        /// </summary>
        public PlanogramImportTemplate SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                PlanogramImportTemplate oldValue = _selectedItem;

                _selectedItem = value;
                OnPropertyChanged(SelectedItemProperty);

                OnSelectedItemChanged(oldValue, value);
            }
        }

        
        /// <summary>
        /// Returns a readonly collection of performance metrics
        /// </summary>
        public ReadOnlyBulkObservableCollection<PerformanceMetricRow> PerformanceMetrics
        {
            get
            {
                if (_performanceMetricRowsRO == null)
                {
                    _performanceMetricRowsRO = new ReadOnlyBulkObservableCollection<PerformanceMetricRow>(_performanceMetricRows);
                }
                return _performanceMetricRowsRO;
            }
        }

        /// <summary>
        /// The currently selected Performance Metrics
        /// </summary>
        public PerformanceMetricRow SelectedPerformanceMetric
        {
            get { return _selectedPerformanceMetric; }
            set
            {
                _selectedPerformanceMetric = value;
                OnPropertyChanged(SelectedPerformanceMetricProperty);
            }
        }

        /// <summary>
        /// Returns the name of the mapping template in use
        /// or custom.
        /// </summary>
        public String MappingTemplateName
        {
            get { return _mappingTemplateName; }
            private set
            {
                _mappingTemplateName = value;
                OnPropertyChanged(MappingTemplateNameProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the selected file type.
        /// </summary>
        public PlanogramImportFileType SelectedFileType
        {
            get
            {
                if (SelectedItem == null) return PlanogramImportFileType.SpacemanV9;
                return SelectedItem.FileType;
            }
            set
            {
                if (SelectedItem == null) return;

                SelectedItem.FileType = value;
                OnPropertyChanged(SelectedFileTypeProperty);
            }
        }

        /// <summary>
        /// Returns the collection of versions available for the selected file type.
        /// </summary>
        public ReadOnlyCollection<String> AvailableVersions
        {
            get { return _availableVersions; }
            private set
            {
                _availableVersions = value;
                OnPropertyChanged(AvailableVersionsProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the selected version
        /// </summary>
        public String SelectedVersion
        {
            get
            {
                if (SelectedItem == null) return String.Empty;
                return SelectedItem.FileVersion;
            }
            set
            {
                if (SelectedItem == null) return;
                SelectedItem.FileVersion = value;
                OnPropertyChanged(SelectedVersionProperty);
            }
        }

        /// <summary>
        /// Returns the collection of product mappings
        /// </summary>
        public IEnumerable<MappingRow> ProductMappings
        {
            get
            {
                return _mappingRows.Where(m => m.MappingType == PlanogramFieldMappingType.Product).ToList();
            }
        }

        /// <summary>
        /// Returns the collection of component mappings
        /// </summary>
        public IEnumerable<MappingRow> ComponentMappings
        {
            get
            {
                return _mappingRows.Where(m => m.MappingType == PlanogramFieldMappingType.Component).ToList();
            }
        }

        /// <summary>
        /// Returns the collection of bay mappings
        /// </summary>
        public IEnumerable<MappingRow> BayMappings
        {
            get
            {
                return _mappingRows.Where(m => m.MappingType == PlanogramFieldMappingType.Fixture).ToList();
            }
        }

        /// <summary>
        /// Returns the collection of planogram mappings.
        /// </summary>
        public IEnumerable<MappingRow> PlanogramMappings
        {
            get
            {
                return _mappingRows.Where(m => m.MappingType == PlanogramFieldMappingType.Planogram).ToList();
            }
        }
        
        #endregion

        #region Constructor

        /// <summary>
        ///     Create a new instance of <see cref="PlanogramFileTemplateEditorViewModel"/>.
        /// </summary>
        /// <param name="initialFilePath">The <c>Path</c> to the file containing the Planogram File Template definition that will be edited.</param>
        /// <param name="fileType">The <see cref="PlanogramImportFileType"/> of the template to be edited.</param>
        public PlanogramFileTemplateEditorViewModel(String initialFilePath, PlanogramImportFileType fileType)
        {
            //get permissions for current repository.
            _itemRepositoryPerms = (App.ViewState.IsConnectedToRepository) ?
                new ModelPermission<PlanogramImportTemplate>(PlanogramImportTemplate.GetUserPermissions())
                : ModelPermission<PlanogramImportTemplate>.DenyAll(); 
            
            MappingTemplateName = DefaultName;

            //if we have an initial file specified then open it.
            if (!String.IsNullOrEmpty(initialFilePath) && System.IO.File.Exists(initialFilePath))
            {
                OpenCommand.Execute(initialFilePath);
                if (SelectedItem.FileType != fileType)
                {
                    SelectedItem = null;
                    MappingTemplateName = DefaultName;
                }
            }

            //  Check to confirm there is a selected item, if not, create a new one for the correct file type.
            if (SelectedItem == null)
            {
                SelectedItem = PlanogramImportTemplate.NewPlanogramImportTemplate(fileType, null);
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the selected item changes.
        /// </summary>
        /// <param name="value"></param>
        private void OnSelectedItemChanged(PlanogramImportTemplate oldValue, PlanogramImportTemplate newValue)
        {
            //clear out the old mapping rows
            _mappingRows.Clear();
            _performanceMetricRows.Clear();

            if (oldValue != null)
            {
                oldValue.ChildChanged -= SelectedItem_ChildChanged;
            }

            if (newValue != null)
            {
                //ensure that all mappble ccm fields have a value
                newValue.UpdateFromCcmFieldList();

                OnPropertyChanged(SelectedFileTypeProperty);
                OnSelectedFileTypeChanged(this.SelectedFileType);

                UpdateDoubleMappingFlags();

                newValue.ChildChanged += SelectedItem_ChildChanged;
            }
            else
            {
                //fire off the property changes for the mapping rows so that
                // the grids disconnect from then. 
                //If we dont do this then the screen memory leaks.
                OnPropertyChanged(PlanogramMappingsProperty);
                OnPropertyChanged(BayMappingsProperty);
                OnPropertyChanged(ComponentMappingsProperty);
                OnPropertyChanged(ProductMappingsProperty);
            }
        }

        /// <summary>
        /// Called whenever a child object of the selected item changes.
        /// </summary>
        private void SelectedItem_ChildChanged(object sender, Csla.Core.ChildChangedEventArgs e)
        {
            if (e.PropertyChangedArgs != null
                && e.PropertyChangedArgs.PropertyName == PlanogramImportTemplateMapping.ExternalFieldProperty.Name)
            {
                this.MappingTemplateName = Message.PlanogramFileTemplateEditor_CustomTemplate;
                UpdateDoubleMappingFlags();
            }
        }

        /// <summary>
        /// Called whenever the selected file type changes.
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedFileTypeChanged(PlanogramImportFileType newValue)
        {
            String curSelectedVersion = SelectedVersion;

            //update the list of available versions and selected version.
            switch (newValue)
            {
                case PlanogramImportFileType.SpacemanV9:
                    AvailableVersions = new ReadOnlyCollection<String>(SpacemanFieldHelper.AvailableVersions);
                    break;

                case PlanogramImportFileType.ProSpace:
                    AvailableVersions = new ReadOnlyCollection<String>(ProSpaceImportHelper.AvailableVersions);
                    break;

                case PlanogramImportFileType.Apollo:
                    AvailableVersions = new ReadOnlyCollection<String>(ApolloImportHelper.AvailableVersions);
                    break;

                default:
                    Debug.Fail("Unknown Import File Type when changing the selected file type. (OnSelectedFileTypeChanged)");
                    return;
            }

            //  Update the selected file version.
            SelectedItem.FileVersion = (AvailableVersions.Contains(curSelectedVersion)) ? curSelectedVersion : AvailableVersions.LastOrDefault();
            OnPropertyChanged(SelectedVersionProperty);

            //get the external field list
            RefreshMappingRows();
        }

        #endregion

        #region Commands

        #region ClearAllMappingsCommand

        private RelayCommand _clearAllMappingsCommand;

        /// <summary>
        /// Clears all assigned mappings
        /// </summary>
        public RelayCommand ClearAllMappingsCommand
        {
            get
            {
                if (_clearAllMappingsCommand == null)
                {
                    _clearAllMappingsCommand = new RelayCommand(
                        p => ClearAllMappings_Executed())
                    {
                        FriendlyName = Message.PlanogramFileTemplateEditor_ClearAllMappings
                    };
                    base.ViewModelCommands.Add(_clearAllMappingsCommand);
                }
                return _clearAllMappingsCommand;
            }
        }

        private void ClearAllMappings_Executed()
        {
            foreach (MappingRow row in _mappingRows)
            {
                row.ExternalField = null;
            }
        }

        #endregion

        #region ClearMappingCommand

        private RelayCommand _clearMappingCommand;

        /// <summary>
        /// Clears the mapping of the given row.
        /// </summary>
        public RelayCommand ClearMappingCommand
        {
            get
            {
                if (_clearMappingCommand == null)
                {
                    _clearMappingCommand = new RelayCommand(
                        p => ClearMapping_Executed(p))
                    {
                        FriendlyName = Message.PlanogramFileTemplateEditor_ClearMapping,
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_clearMappingCommand);
                }
                return _clearMappingCommand;
            }
        }

        private void ClearMapping_Executed(Object args)
        {
            MappingRow row = args as MappingRow;
            if (row == null) return;

            row.ExternalField = null;
        }

        #endregion

        #region OpenCommand

        private RelayCommand _openCommand;

        /// <summary>
        /// Opens an existing label
        /// </summary>
        public RelayCommand OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand(
                        p => Open_Executed(p))
                    {
                        FriendlyName = Message.Generic_Open,
                        SmallIcon = ImageResources.Open_16,
                    };
                    base.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }


        private void Open_Executed(object args)
        {
            String file = args as String;
            if (this.AttachedControl != null)
            {
                //show dialog to get file
                var dialog = new System.Windows.Forms.OpenFileDialog()
                {
                    Filter = String.Format(CultureInfo.InvariantCulture, Message.PlanogramFileTemplateEditor_ImportFilter, PlanogramImportTemplate.FileExtension),
                    CheckFileExists = true,
                    InitialDirectory = App.ViewState.GetSessionDirectory(Ccm.Common.Wpf.Helpers.SessionDirectory.PlanogramFileTemplate),
                };
                if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;

                file = dialog.FileName;

                //update the session directory.
                App.ViewState.SetSessionDirectory(SessionDirectory.PlanogramFileTemplate, Path.GetDirectoryName(file));
            }
            else
            {
                //testing
                file = (String)args;
            }

            base.ShowWaitCursor(true);
            try
            {
                //unlock the original file as we dont need to hold it
                using (PlanogramImportTemplate item = PlanogramImportTemplate.FetchByFilename(file, /*asReadOnly*/true))
                {
                    this.SelectedItem = item.Copy();
                    this.MappingTemplateName = file;                    
                }
            }
            catch (DataPortalException ex)
            {
                base.ShowWaitCursor(false);
                App.ShowWindow(
                    new ModalMessage()
                    {
                        MessageIcon = ImageResources.Warning_32,
                        Description = ex.GetBaseException().Message,
                        ButtonCount = 1,
                        Button1Content = Message.Generic_OK
                    }, this.AttachedControl, true);
            }
            base.ShowWaitCursor(false);
        }

        #endregion

        #region OpenFromFileCommand

        private RelayCommand _openFromFileCommand;

        /// <summary>
        /// Opens an existing template from a file source
        /// </summary>
        public RelayCommand OpenFromFileCommand
        {
            get
            {
                if (_openFromFileCommand == null)
                {
                    _openFromFileCommand = new RelayCommand(
                        p => OpenFromFile_Executed(p))
                    {
                        FriendlyName = CommonMessage.Generic_OpenFromFile
                    };
                    base.ViewModelCommands.Add(_openFromFileCommand);
                }
                return _openFromFileCommand;
            }
        }

        private void OpenFromFile_Executed(object args)
        {
            String file = args as String;
            if (this.AttachedControl != null)
            {
                //show dialog to get file
                var dialog = new System.Windows.Forms.OpenFileDialog()
                {
                    Filter = String.Format(CultureInfo.InvariantCulture, Message.PlanogramFileTemplateEditor_ImportFilter, PlanogramImportTemplate.FileExtension),
                    CheckFileExists = true,
                    InitialDirectory = App.ViewState.GetSessionDirectory(Ccm.Common.Wpf.Helpers.SessionDirectory.PlanogramFileTemplate),
                };
                if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;

                file = dialog.FileName;

                //update the session directory.
                App.ViewState.SetSessionDirectory(SessionDirectory.PlanogramFileTemplate, Path.GetDirectoryName(file));
            }
            else
            {
                //testing
                file = (String)args;
            }

            base.ShowWaitCursor(true);
            try
            {
                //unlock the original file as we dont need to hold it
                using (PlanogramImportTemplate item = PlanogramImportTemplate.FetchByFilename(file, /*asReadOnly*/true))
                {
                    this.SelectedItem = item.Copy();
                    this.MappingTemplateName = file;
                }
            }
            catch (DataPortalException ex)
            {
                base.ShowWaitCursor(false);
                App.ShowWindow(
                    new ModalMessage()
                    {
                        MessageIcon = ImageResources.Warning_32,
                        Description = ex.GetBaseException().Message,
                        ButtonCount = 1,
                        Button1Content = Message.Generic_OK
                    }, this.AttachedControl, true);
            }
            base.ShowWaitCursor(false);
        }

        #endregion

        #region OpenFromRepositoryCommand

        private RelayCommand _openFromRepositoryCommand;

        /// <summary>
        /// Opens an existing template from the database
        /// </summary>
        public RelayCommand OpenFromRepositoryCommand
        {
            get
            {
                if (_openFromRepositoryCommand == null)
                {
                    _openFromRepositoryCommand = new RelayCommand(
                        p => OpenFromRepository_Executed(p),
                        p => OpenFromRepository_CanExecute())
                    {
                        FriendlyName = CommonMessage.Generic_OpenFromRepository
                    };
                    base.ViewModelCommands.Add(_openFromRepositoryCommand);
                }
                return _openFromRepositoryCommand;
            }
        }

        private Boolean OpenFromRepository_CanExecute()
        {
            //must be connected
            if (!App.ViewState.IsConnectedToRepository)
            {
                this.OpenFromRepositoryCommand.DisabledReason = CommonMessage.Generic_NoRepositoryConnection;
                return false;
            }

            //must have open permission
            if (!_itemRepositoryPerms.CanFetch)
            {
                this.OpenFromRepositoryCommand.DisabledReason = CommonMessage.Generic_NoFetchPermission;
                return false;
            }

            return true;
        }

        private void OpenFromRepository_Executed(object args)
        {
            //Get the template id to load.
            Object id = args;
            if (id == null)
            {
                GenericSingleItemSelectorWindow win = new GenericSingleItemSelectorWindow();
                win.ItemSource = PlanogramImportTemplateInfoList.FetchByEntityId(App.ViewState.EntityId);
                win.SelectionMode = System.Windows.Controls.DataGridSelectionMode.Single;
                CommonHelper.GetWindowService().ShowDialog<GenericSingleItemSelectorWindow>(win);
                if (win.DialogResult != true) return;

                id = win.SelectedItems.Cast<PlanogramImportTemplateInfo>().First().Id;
            }

            base.ShowWaitCursor(true);

            //unlock the original file as we dont need to hold it
            PlanogramImportTemplate item = PlanogramImportTemplate.FetchById(id);
            this.SelectedItem = item;
            this.MappingTemplateName = item.Name;

            base.ShowWaitCursor(false);
        }

        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;

        /// <summary>
        /// Saves changes made to a new label setting and loads new as current
        /// /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(p))
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        SmallIcon = ImageResources.SaveAs_16,
                        FriendlyDescription = Message.LabelEditor_SaveAs_Desc,
                        Icon = ImageResources.SaveAs_32
                    };
                    base.ViewModelCommands.Add(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }

        private void SaveAs_Executed(object args)
        {
            PlanogramImportTemplate itemToSave = this.SelectedItem;

            String filePath = args as String;
            if (String.IsNullOrEmpty(filePath))
            {
                //show dialog to get path
                var dialog = new System.Windows.Forms.SaveFileDialog()
                {
                    InitialDirectory = App.ViewState.GetSessionDirectory(Ccm.Common.Wpf.Helpers.SessionDirectory.PlanogramFileTemplate),
                    Filter = String.Format(CultureInfo.InvariantCulture, Message.PlanogramFileTemplateEditor_ImportFilter, PlanogramImportTemplate.FileExtension),
                    OverwritePrompt = true
                };
                if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
                filePath = dialog.FileName;

                //update the session directory
                App.ViewState.SetSessionDirectory(SessionDirectory.PlanogramFileTemplate, Path.GetDirectoryName(filePath));
            }

            base.ShowWaitCursor(true);

            //save
            try
            {
                itemToSave = itemToSave.SaveAsFile(filePath);
            }
            catch (DataPortalException ex)
            {
                base.ShowWaitCursor(false);

                App.ShowWindow(
                new ModalMessage()
                {
                    MessageIcon = ImageResources.Warning_32,
                    Description = ex.GetBaseException().Message,
                    ButtonCount = 1,
                    Button1Content = Message.Generic_OK
                }, this.AttachedControl, true);

                base.ShowWaitCursor(true);
            }
            base.ShowWaitCursor(false);

            //reselect the label
            if (itemToSave != null)
            {
                this.SelectedItem = itemToSave.Copy();
                this.MappingTemplateName = filePath;
            }

            //unlock the file again as we don't need to keep hold of it.
            itemToSave.Dispose();
        }

        #endregion
        
        #region SaveToFile

        private RelayCommand _saveAsToFileCommand;

        /// <summary>
        /// Saves the current template as a new file template.
        /// </summary>
        public RelayCommand SaveAsToFileCommand
        {
            get
            {
                if (_saveAsToFileCommand == null)
                {
                    _saveAsToFileCommand = new RelayCommand(p => SaveAsToFile_Executed(p))
                    {
                        FriendlyName = CommonMessage.Generic_SaveToFile
                    };
                    base.ViewModelCommands.Add(_saveAsToFileCommand);
                }
                return _saveAsToFileCommand;
            }
        }

        private void SaveAsToFile_Executed(Object args)
        {
            PlanogramImportTemplate itemToSave = this.SelectedItem;

            String filePath = args as String;
            if (String.IsNullOrEmpty(filePath))
            {
                //show dialog to get path
                var dialog = new System.Windows.Forms.SaveFileDialog()
                {
                    InitialDirectory = App.ViewState.GetSessionDirectory(Ccm.Common.Wpf.Helpers.SessionDirectory.PlanogramFileTemplate),
                    Filter = String.Format(CultureInfo.InvariantCulture, Message.PlanogramFileTemplateEditor_ImportFilter, PlanogramImportTemplate.FileExtension),
                    OverwritePrompt = true
                };
                if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
                filePath = dialog.FileName;

                //update the session directory
                App.ViewState.SetSessionDirectory(SessionDirectory.PlanogramFileTemplate, Path.GetDirectoryName(filePath));
            }

            base.ShowWaitCursor(true);

            //save
            try
            {
                itemToSave = itemToSave.SaveAsFile(filePath);
            }
            catch (DataPortalException ex)
            {
                base.ShowWaitCursor(false);

                App.ShowWindow(
                    new ModalMessage()
                    {
                        MessageIcon = ImageResources.Warning_32,
                        Description = ex.GetBaseException().Message,
                        ButtonCount = 1,
                        Button1Content = Message.Generic_OK
                    }, this.AttachedControl, true);

                base.ShowWaitCursor(true);
            }
            base.ShowWaitCursor(false);

            //reselect the label
            if (itemToSave != null)
            {
                this.SelectedItem = itemToSave.Copy();
                this.MappingTemplateName = filePath;
            }

            //unlock the file again as we don't need to keep hold of it.
            itemToSave.Dispose();
        }

        #endregion

        #region SaveAsToRepository

        private RelayCommand _saveAsToRepositoryCommand;

        /// <summary>
        /// Saves the currrent template as a new template in the repository.
        /// </summary>
        public RelayCommand SaveAsToRepositoryCommand
        {
            get
            {
                if (_saveAsToRepositoryCommand == null)
                {
                    _saveAsToRepositoryCommand = new RelayCommand(
                            o => SaveAsToRepository_Executed(),
                        o => SaveAsToRepository_CanExecute())
                    {
                        FriendlyName = CommonMessage.Generic_SaveToRepository
                    };
                    base.ViewModelCommands.Add(_saveAsToRepositoryCommand);

                }
                return _saveAsToRepositoryCommand;
            }
        }

        private Boolean SaveAsToRepository_CanExecute()
        {
            if (!App.ViewState.IsConnectedToRepository)
            {
                this.SaveAsToRepositoryCommand.DisabledReason = CommonMessage.Generic_NoRepositoryConnection;
                return false;
            }

            //must have permission
            if (!_itemRepositoryPerms.CanCreate)
            {
                this.SaveAsToRepositoryCommand.DisabledReason = CommonMessage.Generic_NoCreatePermission;
                return false;
            }

            return true;
        }

        /// <summary>
        ///     Invoked whenever the <see cref="SaveAsToRepositoryCommand" /> is executed.
        /// </summary>
        private void SaveAsToRepository_Executed()
        {
            String copyName;

            PlanogramImportTemplateInfoList existingItems = PlanogramImportTemplateInfoList.FetchByEntityId(App.ViewState.EntityId);
            Predicate<String> isUniqueCheck =
               (s) => { return !existingItems.Select(p => p.Name).Contains(s, StringComparer.OrdinalIgnoreCase); };

            Boolean nameAccepted = CommonHelper.GetWindowService().PromptForSaveAsName(isUniqueCheck, String.Empty, out copyName);
            if (!nameAccepted) return;

            ShowWaitCursor(true);

            //copy the item and rename
            PlanogramImportTemplate itemCopy = this.SelectedItem.Copy();
            itemCopy.Name = copyName;

            //Perform the save.
            ShowWaitCursor(true);

            try
            {
                //Make sure the entityId is set
                itemCopy.EntityId = App.ViewState.EntityId;

                this.SelectedItem = itemCopy.SaveAs();
                //MarkAsExisting(this.SelectedItem);
            }
            catch (Exception ex)
            {
                base.ShowWaitCursor(false);
                CommonHelper.RecordException(ex);
                CommonHelper.GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Save);
                return;
            }

            ShowWaitCursor(false);
        }

        #endregion

        #region OkCommand

        private RelayCommand _okCommand;

        /// <summary>
        /// Oks this window.
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OK_Executed(),
                        p => OK_CanExecute())
                        {
                            FriendlyName = Message.Generic_OK
                        };
                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }

        private Boolean OK_CanExecute()
        {
            //must be valid

            MappingRow row = _mappingRows.Where(p => !p.IsValid).FirstOrDefault();

            if (row != null)
            {
                this.OKCommand.DisabledReason = String.Format(Message.PlanogramFileTemplateEditor_OkDisabledReason, row.FieldFriendlyName);
                return false;
            }

            return true;
        }

        private void OK_Executed()
        {
            this.DialogResult = true;
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Closes the window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel,
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #region NewMetricCommand

        private RelayCommand _newMetricCommand;

        /// <summary>
        /// Creates a new metric
        /// </summary>
        public RelayCommand NewMetricCommand
        {
            get
            {
                if (_newMetricCommand == null)
                {
                    _newMetricCommand = new RelayCommand(
                        p => NewMetric_Executed(),
                        p => NewMetric_CanExecute())
                    {
                        FriendlyName = Message.Generic_Add,
                        FriendlyDescription = Message.PlanogramProperties_AddMetric_Description,
                        SmallIcon = ImageResources.PlanogramProperties_AddPerformanceMetric,
                        DisabledReason = Message.PlanogramProperties_AddMetric_DisabledReason
                    };
                    base.ViewModelCommands.Add(_newMetricCommand);
                }
                return _newMetricCommand;
            }
        }

        private Boolean NewMetric_CanExecute()
        {
            return PerformanceMetrics.Count < Framework.Planograms.Constants.MaximumStandardGlobalMetricsPerPerformanceSource;
        }

        private void NewMetric_Executed()
        {
             //Adds a new metric.
            SelectedPerformanceMetric = new PerformanceMetricRow(PlanogramImportTemplatePerformanceMetric.NewPlanogramImportTemplatePerformanceMetric(), _externalFieldDict[PlanogramFieldMappingType.Performance], null);
            SelectedPerformanceMetric.PerformanceMetric.MetricId = (Byte)(PerformanceMetrics.Count + 1);
            this.SelectedItem.PerformanceMetrics.Add(SelectedPerformanceMetric.PerformanceMetric);
            _performanceMetricRows.Add(SelectedPerformanceMetric);

             //Check if we are running under a unit test environment
            if (Application.Current != null)
            {
                _metricWindow = new PlanogramFileTemplateEditorPerformanceMetricWindow(this, true);
                _metricWindow.Owner = this.AttachedControl;
                _metricWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                _metricWindow.ShowDialog();
            }
        }

        #endregion

        #region ViewMetricCommand

        private RelayCommand _viewMetricCommand;

        /// <summary>
        /// Creates a new metric
        /// </summary>
        public RelayCommand ViewMetricCommand
        {
            get
            {
                if (_viewMetricCommand == null)
                {
                    _viewMetricCommand = new RelayCommand(
                        p => ViewMetric_Executed(),
                        p => ViewMetric_CanExecute())
                    {
                        FriendlyName = Message.PlanogramProperties_EditMetric,
                        FriendlyDescription = Message.PlanogramProperties_EditMetric_Description,
                        SmallIcon = ImageResources.PlanogramProperties_ViewPerformanceMetric,
                        DisabledReason = Message.PlanogramProperties_EditMetric_DisabledReason
                    };
                    base.ViewModelCommands.Add(_viewMetricCommand);
                }
                return _viewMetricCommand;
            }
        }

        private Boolean ViewMetric_CanExecute()
        {
            return this.SelectedPerformanceMetric != null;
        }

        private void ViewMetric_Executed()
        {
            _metricWindow = new PlanogramFileTemplateEditorPerformanceMetricWindow(this, false);
            _metricWindow.Owner = this.AttachedControl;
            _metricWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            _metricWindow.ShowDialog();
        }

        #endregion

        #region RemoveMetricCommand

        private RelayCommand _removeMetricCommand;

        /// <summary>
        /// Creates a new metric
        /// </summary>
        public RelayCommand RemoveMetricCommand
        {
            get
            {
                if (_removeMetricCommand == null)
                {
                    _removeMetricCommand = new RelayCommand(
                        p => RemoveMetric_Executed(),
                        p => RemoveMetric_CanExecute())
                    {
                        FriendlyName = Message.Generic_Remove,
                        FriendlyDescription = Message.PlanogramProperties_RemoveMetric_Description,
                        SmallIcon = ImageResources.PlanogramProperties_RemovePerformanceMetric,
                        DisabledReason = Message.PlanogramProperties_RemoveMetric_DisabledReason
                    };
                    base.ViewModelCommands.Add(_removeMetricCommand);
                }
                return _removeMetricCommand;
            }
        }

        private Boolean RemoveMetric_CanExecute()
        {
            return this.SelectedPerformanceMetric != null;
        }

        private void RemoveMetric_Executed()
        {
            this.SelectedItem.PerformanceMetrics.Remove(this.SelectedPerformanceMetric.PerformanceMetric);
            _performanceMetricRows.Remove(this.SelectedPerformanceMetric);

            Byte metricId = 1;
            foreach (PerformanceMetricRow perfRow in _performanceMetricRows)
            {
                perfRow.PerformanceMetric.MetricId = metricId;
                metricId++;
            }

            this.SelectedPerformanceMetric = null;
        }

        #endregion

        #region MetricSaveCommand

        private RelayCommand _metricSaveCommand;

        public RelayCommand MetricSaveCommand
        {
            get
            {
                if (_metricSaveCommand == null)
                {
                    _metricSaveCommand = new RelayCommand(
                        p => MetricSave_Executed(),
                        p => MetricSave_CanExecute())
                    {
                        FriendlyName = Message.Generic_ApplyAndClose,
                        DisabledReason = Message.PlanogramProperties_MetricSave_DisabledReason
                    };
                }
                return _metricSaveCommand;
            }
        }

        private Boolean MetricSave_CanExecute()
        {
            if (SelectedPerformanceMetric == null) return false;
            return SelectedPerformanceMetric.PerformanceMetric.IsValid;
        }

        private void MetricSave_Executed()
        {
            if (_metricWindow != null)
            {
                _metricWindow.Close();
                _metricWindow = null;
            }
        }

        #endregion

        #region MetricCancelCommand

        private RelayCommand _metricCancelCommand;

        public RelayCommand MetricCancelCommand
        {
            get
            {
                if (_metricCancelCommand == null)
                {
                    _metricCancelCommand = new RelayCommand(p => MetricCancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                }
                return _metricCancelCommand;
            }
        }

        private void MetricCancel_Executed()
        {
            // If New Metric, remove from list
            if (_metricWindow.NewMetric)
            {
                this.SelectedItem.PerformanceMetrics.Remove(SelectedPerformanceMetric.PerformanceMetric);
                _performanceMetricRows.Remove(this.SelectedPerformanceMetric);
                SelectedPerformanceMetric = null;
            }
            else // Replace metric with original
            {
                SelectedPerformanceMetric.PerformanceMetric.CopyValues(_metricWindow.OriginalMetric);
                PlanogramImportTemplateFieldInfo planogramImportTemplateFieldInfo = SelectedPerformanceMetric.AvailableExternalFields.Where(p => p.Field == _metricWindow.OriginalMetric.ExternalField).FirstOrDefault();
                if (planogramImportTemplateFieldInfo != null && SelectedPerformanceMetric.ExternalField != planogramImportTemplateFieldInfo)
                {
                    SelectedPerformanceMetric.ExternalField = planogramImportTemplateFieldInfo;
                }
            }

            _metricWindow.Close();
            _metricWindow = null;
        }

        #endregion

        #endregion

        #region Methods

        private void RefreshMappingRows()
        {
            if (_mappingRows.Count > 0) _mappingRows.Clear();
            if (_performanceMetricRows.Count > 0) _performanceMetricRows.Clear();

            //update the external field dict
            var dict = new Dictionary<PlanogramFieldMappingType, ReadOnlyCollection<PlanogramImportTemplateFieldInfo>>();

            PlanogramImportTemplateFieldInfoList externalFieldList =
                PlanogramImportTemplateFieldInfoList.NewPlanogramImportTemplateFieldInfoList(this.SelectedFileType, this.SelectedVersion);

            foreach (var mapGroup in externalFieldList.GroupBy(f => f.FieldType))
            {
                dict.Add(mapGroup.Key, mapGroup.ToList().AsReadOnly());
            }
            _externalFieldDict = dict;
            
            if (this.SelectedItem != null)
            {
                var ccmFieldList = PlanogramImportTemplateFieldInfoList.NewPlanogramImportTemplateFieldInfoList(null, null);

                foreach (var mappingGroup in this.SelectedItem.Mappings.GroupBy(g => g.FieldType))
                {
                    ReadOnlyCollection<PlanogramImportTemplateFieldInfo> availableExternal = _externalFieldDict[mappingGroup.Key];
                    var externalLookup = availableExternal.ToDictionary(f => f.Field);

                    foreach (PlanogramImportTemplateMapping mapping in mappingGroup)
                    {
                        PlanogramImportTemplateFieldInfo ccmField = ccmFieldList.FirstOrDefault(c => c.Field == mapping.Field && c.FieldType == mapping.FieldType);
                        if (ccmField == null) continue;

                        PlanogramImportTemplateFieldInfo externalField = null;
                        externalLookup.TryGetValue(mapping.ExternalField, out externalField);
                        _mappingRows.Add(new MappingRow(mapping, ccmField, availableExternal, externalField));

                    }
                }

                ReadOnlyCollection<PlanogramImportTemplateFieldInfo> availablePerformanceExternal = _externalFieldDict[PlanogramFieldMappingType.Performance];
                var externalPerformanceLookup = availablePerformanceExternal.ToDictionary(f => f.Field);

                // create performance rows
                foreach (PlanogramImportTemplatePerformanceMetric performanceMetric in this.SelectedItem.PerformanceMetrics)
                {
                    PlanogramImportTemplateFieldInfo externalField = null;
                    externalPerformanceLookup.TryGetValue(performanceMetric.ExternalField, out externalField);
                    _performanceMetricRows.Add(new PerformanceMetricRow(performanceMetric, availablePerformanceExternal, externalField));
                }
            }

            //fire off all related property changes
            OnPropertyChanged(PlanogramMappingsProperty);
            OnPropertyChanged(BayMappingsProperty);
            OnPropertyChanged(ComponentMappingsProperty);
            OnPropertyChanged(ProductMappingsProperty);
        }

        /// <summary>
        /// Identifies double assignments and updates
        /// the double mapping flag for all rows.
        /// </summary>
        private void UpdateDoubleMappingFlags()
        {
           String[] doubleMaps = 
               _mappingRows.GroupBy(m => (m.ExternalField != null)? m.ExternalField.Field : null)
               .Where(g => g.Key != null && g.Count() > 1).Select(g => g.Key).ToArray();

           foreach (MappingRow row in _mappingRows)
           {
               row.IsDoubleMapped = (row.ExternalField != null && doubleMaps.Contains(row.ExternalField.Field));
           }
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!IsDisposed)
            {
                if (disposing)
                {
                    OnSelectedItemChanged(this.SelectedItem, null);
                }
                IsDisposed = true;
            }
        }

        #endregion
    }
}
