﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.1)
// V8-28622 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Galleria.Ccm.Model;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Editor.Client.Wpf.Settings.PlanogramFileTemplateEditor
{
    public sealed class PerformanceMetricRow : INotifyPropertyChanged, IDataErrorInfo
    {
        #region Fields

        private PlanogramImportTemplatePerformanceMetric _performanceMetric;
        private IEnumerable<PlanogramImportTemplateFieldInfo> _availableExternalFields;
        private PlanogramImportTemplateFieldInfo _externalField;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath AvailableExternalFieldsProperty = WpfHelper.GetPropertyPath<PerformanceMetricRow>(p => p.AvailableExternalFields);
        public static readonly PropertyPath ExternalFieldProperty = WpfHelper.GetPropertyPath<PerformanceMetricRow>(p => p.ExternalField);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the type of mapping this row relates to.
        /// </summary>
        public PlanogramImportTemplatePerformanceMetric PerformanceMetric
        {
            get { return _performanceMetric; }
        }

        public IEnumerable<PlanogramImportTemplateFieldInfo> AvailableExternalFields
        {
            get { return _availableExternalFields; }
        }

        public PlanogramImportTemplateFieldInfo ExternalField
        {
            get { return _externalField; }
            set
            {
                _externalField = value;
                OnPropertyChanged(ExternalFieldProperty);

                _performanceMetric.ExternalField = (value != null) ? value.Field : null;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public PerformanceMetricRow(PlanogramImportTemplatePerformanceMetric performanceMetric, IEnumerable<PlanogramImportTemplateFieldInfo> availableFields, PlanogramImportTemplateFieldInfo selectedField)
        {
            _performanceMetric = performanceMetric;
            _availableExternalFields = availableFields;
            this.ExternalField = selectedField;
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion

        #region IDataErrorInfo
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get
            {
                String result = null;
                if (columnName == ExternalFieldProperty.Path)
                {
                    if (this.ExternalField == null)
                    {
                        result = Message.PlanogramFileTemplateEditor_MappingRequired;
                    }
                }
                return result;
            }
        }
        #endregion
    }
}
