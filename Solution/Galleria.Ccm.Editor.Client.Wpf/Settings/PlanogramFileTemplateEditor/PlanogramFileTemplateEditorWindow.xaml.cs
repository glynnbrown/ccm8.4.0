﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27804 : L.Ineson
//  Created
// V8-28599 : N.Haywood
//  Added ExtendedDataGrid_ColumnHeaderDragStarted
#endregion
#region Version History: (CCM 8.0.1)
// V8-28622 : D.Pleasance
//  Added PlanogramImportTemplatePerformanceMetrics
#endregion

#region Version History: (CCM 8.1.0)
// V8-30196 : M.Pettit
//  Added Help link
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.Settings.PlanogramFileTemplateEditor
{
    /// <summary>
    /// Interaction logic for PlanogramFileTemplateEditorWindow.xaml
    /// </summary>
    public sealed partial class PlanogramFileTemplateEditorWindow : ExtendedRibbonWindow
    {
        #region Fields
        const String _clearMappingCommandKey = "ClearMappingCommand";
        const String _clearAllMappingsCommandKey = "ClearAllMappingsCommand";
        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanogramFileTemplateEditorViewModel), typeof(PlanogramFileTemplateEditorWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets/Sets the window viewmodel context
        /// </summary>
        public PlanogramFileTemplateEditorViewModel ViewModel
        {
            get { return (PlanogramFileTemplateEditorViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PlanogramFileTemplateEditorWindow senderControl = (PlanogramFileTemplateEditorWindow)obj;

            if (e.OldValue != null)
            {
                PlanogramFileTemplateEditorViewModel oldModel = (PlanogramFileTemplateEditorViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                senderControl.Resources.Remove(_clearMappingCommandKey);
                senderControl.Resources.Remove(_clearAllMappingsCommandKey);
            }

            if (e.NewValue != null)
            {
                PlanogramFileTemplateEditorViewModel newModel = (PlanogramFileTemplateEditorViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                senderControl.Resources.Add(_clearMappingCommandKey, newModel.ClearMappingCommand);
                senderControl.Resources.Add(_clearAllMappingsCommandKey, newModel.ClearAllMappingsCommand);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new insancne of this type.
        /// </summary>
        /// <param name="viewModel"></param>
        public PlanogramFileTemplateEditorWindow(PlanogramFileTemplateEditorViewModel viewModel)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //set the help file key for this window.
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.ImportPlanogram);

            this.ViewModel = viewModel;

            this.Loaded += PlanogramFileTemplateEditor_Loaded;
        }

        /// <summary>
        /// Carries out initial load actions
        /// </summary>
        private void PlanogramFileTemplateEditor_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= PlanogramFileTemplateEditor_Loaded;

            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Window close

        /// <summary>
        /// Disposes of the viewmodel on close
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;
                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion

        #region Events
        private void ExtendedDataGrid_ColumnHeaderDragStarted(object sender, System.Windows.Controls.Primitives.DragStartedEventArgs e)
        {

        }

        private void OnPerformanceMetricDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            var command = ViewModel.ViewMetricCommand;
            if (command.CanExecute()) command.Execute();
        }
        #endregion
    }
}
