﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27804 : L.Ineson
//  Created
#endregion
#region Version History: (CCM 8.1.0)
// V8-29678 : D.Pleasance
//  Amended to validate required mappings
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Editor.Client.Wpf.Settings.PlanogramFileTemplateEditor
{
    public sealed class MappingRow : INotifyPropertyChanged, IDataErrorInfo
    {
        #region Fields

        private PlanogramImportTemplateMapping _sourceMapping;
        private PlanogramImportTemplateFieldInfo _ccmField;
        private IEnumerable<PlanogramImportTemplateFieldInfo> _availableExternalFields;
        private PlanogramImportTemplateFieldInfo _externalField;
        private Boolean _isDoubleMapped;
        private Boolean _isValid = true;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath FieldFriendlyNameProperty = WpfHelper.GetPropertyPath<MappingRow>(p => p.FieldFriendlyName);
        public static readonly PropertyPath AvailableExternalFieldsProperty = WpfHelper.GetPropertyPath<MappingRow>(p => p.AvailableExternalFields);
        public static readonly PropertyPath ExternalFieldProperty = WpfHelper.GetPropertyPath<MappingRow>(p => p.ExternalField);
        public static readonly PropertyPath IsDoubleMappedProperty = WpfHelper.GetPropertyPath<MappingRow>(p => p.IsDoubleMapped);
        public static readonly PropertyPath IsValidProperty = WpfHelper.GetPropertyPath<MappingRow>(p => p.IsValid);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the type of mapping this row relates to.
        /// </summary>
        public PlanogramFieldMappingType MappingType
        {
            get { return _sourceMapping.FieldType; }
        }

        public String Field
        {
            get { return _sourceMapping.Field; }
        }

        public String FieldFriendlyName
        {
            get { return _ccmField.DisplayName; }
        }

        public IEnumerable<PlanogramImportTemplateFieldInfo> AvailableExternalFields
        {
            get { return _availableExternalFields; }
        }

        public PlanogramImportTemplateFieldInfo ExternalField
        {
            get { return _externalField; }
            set
            {
                _externalField = value;
                OnPropertyChanged(ExternalFieldProperty);

                _sourceMapping.ExternalField = (value != null) ? value.Field : null;
            }
        }

        public Boolean IsDoubleMapped
        {
            get { return _isDoubleMapped; }
            set
            {
                if (_isDoubleMapped != value)
                {
                    _isDoubleMapped = value;
                    OnPropertyChanged(IsDoubleMappedProperty);
                }
            }
        }

        public Boolean IsValid
        {
            get { return _isValid; }
            set
            {
                _isValid = value;
                OnPropertyChanged(IsValidProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public MappingRow(PlanogramImportTemplateMapping mapping, PlanogramImportTemplateFieldInfo ccmField,
            IEnumerable<PlanogramImportTemplateFieldInfo> availableFields, PlanogramImportTemplateFieldInfo selectedField)
        {
            _sourceMapping = mapping;
            _ccmField = ccmField;
            _availableExternalFields = availableFields;
            this.ExternalField = selectedField;
            IsValid = ValidMapping();
        }

        #endregion

        #region Methods
        private Boolean ValidMapping()
        {
            if ((MappingType == PlanogramFieldMappingType.Product && (Field == "Name" || Field == "Gtin")) ||
                (MappingType == PlanogramFieldMappingType.Component && (Field == "Name")))
            {
                if (this.ExternalField == null)
                {
                    return false;
                }
            }
            return true;
        }
        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion

        #region IDataErrorInfo
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get
            {
                String result = null;
                if (columnName == ExternalFieldProperty.Path)
                {
                    IsValid = ValidMapping();
                    if (!IsValid)
                    {
                        result = Message.PlanogramFileTemplateEditor_MappingRequired;
                    }
                }
                return result;
            }
        }

        #endregion
    }
}
