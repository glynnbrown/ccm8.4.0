﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// J.Pickup ~ Created.
// V8-26248 : L.Luong
//  Changed to fit new design

#endregion
#region Version History: (CCM 8.03)
// V8-29313 : L.Ineson
//  Brought into line with new standards.
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.Settings
{
    /// <summary>
    /// Interaction logic for ProductLabelEditorBackstageOpen.xaml
    /// </summary>
    public sealed partial class ProductLabelEditorOrganiser : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ProductLabelEditorViewModel), typeof(ProductLabelEditorOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ProductLabelEditorOrganiser senderControl = (ProductLabelEditorOrganiser)obj;

            if (e.OldValue != null)
            {
                ProductLabelEditorViewModel oldModel = (ProductLabelEditorViewModel)e.OldValue;
                oldModel.UnregisterWindowControl();
            }

            if (e.NewValue != null)
            {
                ProductLabelEditorViewModel newModel = (ProductLabelEditorViewModel)e.NewValue;
                newModel.RegisterWindowControl(senderControl);
            }
        }

        ///<summary>
        /// Gets/Sets the window viewmodel context
        /// </summary> 
        public ProductLabelEditorViewModel ViewModel
        {
            get { return (ProductLabelEditorViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructors

         /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public ProductLabelEditorOrganiser(ProductLabelEditorViewModel viewModel)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            this.ViewModel = viewModel;

            //set the help file reference 
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.ProductLabels);

            this.Loaded += ProfileMaintenanceOrganiser_Loaded;
        }

        #endregion

        #region Event Handlers

        private void ProfileMaintenanceOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ProfileMaintenanceOrganiser_Loaded;

            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        /// <summary>
        /// Called whenever the label content textbox gets double clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LabelContentTextBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.ViewModel.SetFieldCommand.Execute();
        }

        #endregion

        #region Window close

        /// <summary>
        /// Disposes of the viewmodel on close
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;
                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion
    }
}
