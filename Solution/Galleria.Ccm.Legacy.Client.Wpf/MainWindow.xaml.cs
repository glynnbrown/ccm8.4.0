﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Galleria.Ccm.Legacy;
using LV7 = Galleria.Ccm.Legacy.V7;
using LMSP = Galleria.Ccm.Legacy.Msp;
using Galleria.Framework.Processes;
using Galleria.Ccm.Legacy.Model;
using System.Threading;

namespace Galleria.Ccm.Legacy.Client.Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            
            xServerName.Text = Properties.Settings.Default.ServerName;
            xDatabaseName.Text = Properties.Settings.Default.DatabaseName;
            xOutputPath.Text = Properties.Settings.Default.OutputPath;
            xMspOutputPath.Text = Properties.Settings.Default.MspOutputPath;
            xImportPath.Text = Properties.Settings.Default.ImportPath;

            if (String.IsNullOrWhiteSpace(xOutputPath.Text))
            {
                String path = System.IO.Path.GetTempPath() + @"LegacyPogs\";
                xOutputPath.Text = path;
            }

            if (String.IsNullOrWhiteSpace(xMspOutputPath.Text))
            {
                String path = System.IO.Path.GetTempPath() + @"LegacyPogs\";
                xMspOutputPath.Text = path;
            }

            xCSVs.AppendText(Properties.Settings.Default.CategoryReviewIdsCSV);

            xMspCheckBox.IsChecked = Properties.Settings.Default.PlacedProductsOnly;
        }

        private Boolean NotInt(String val)
        {
            Int32 x;
            return !Int32.TryParse(val, out x);
        }
        private String CleanCSV(String csv)
        {
            String output = String.Empty;
            String text = csv.Replace(Environment.NewLine, ",");
            String[] split = text.Split(',');

            foreach (String val in split)
            {
                if (String.IsNullOrWhiteSpace(val)) continue;
                if (NotInt(val)) continue;
                if (String.IsNullOrWhiteSpace(output))
                {
                    output = val;
                }
                else
                {
                    output = String.Format("{0},{1}", output, val);
                }
            }

            while (output.Length > 0 && output.Last() == ',')
            {
                output = text.Remove(output.Length - 1);
            }

            return output;
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (xV7.IsSelected)
            {
               ProcessV7Plans();
            }
            else
            {
                ProcessMspPlans();
            }
        }
        private void ProcessV7Plans()
        {
             try
                {
                    this.xTabControl.IsEnabled = false;
                    this.xProcessButton.IsEnabled = false;
                    this.xProcessButton.Visibility = System.Windows.Visibility.Collapsed;
                    V7.ConvertProcess process = new V7.ConvertProcess();
                    process.ServerName = xServerName.Text;
                    process.DatabaseName = xDatabaseName.Text;
                    process.Client = GeneralHelpers.StringToClientType(Properties.Settings.Default.Client);
                    process.ProcessThreads = Properties.Settings.Default.ProcessThreads;
                    process.OutputPath = xOutputPath.Text;
                    var textRange = new TextRange(xCSVs.Document.ContentStart, xCSVs.Document.ContentEnd);
                    String text = textRange.Text.Replace(Environment.NewLine, ",");
                    text = CleanCSV(text);
                    Properties.Settings.Default["CategoryReviewIdsCSV"] = text;
                    Properties.Settings.Default["ServerName"] = process.ServerName;
                    Properties.Settings.Default["DatabaseName"] = process.DatabaseName;
                    Properties.Settings.Default["OutputPath"] = process.OutputPath;

                    Properties.Settings.Default.Save();
                    process.CategoryReviewIdsCSV = text;
                    process.OnProcessUpdate += Process_OnProcessUpdate;
                    startTime = DateTime.Now;
                    ThreadPool.QueueUserWorkItem(ProcessV7, process);

                }
                catch
                {
                    this.xTabControl.IsEnabled = true;
                    this.xProcessButton.IsEnabled = true;
                    this.xProcessButton.Visibility = System.Windows.Visibility.Visible;
                    this.xStatus.Text = String.Empty;
                }
        }
        private void ProcessMspPlans()
        {
            try
            {
                this.xTabControl.IsEnabled = false;
                this.xProcessButton.IsEnabled = false;
                this.xProcessButton.Visibility = System.Windows.Visibility.Collapsed;
                LMSP.ConvertProcess process = new LMSP.ConvertProcess();
                process.ImportPath = xImportPath.Text;
                process.Client = GeneralHelpers.StringToClientType(Properties.Settings.Default.Client);
                process.ProcessThreads = Properties.Settings.Default.ProcessThreads;
                process.PlacedProductsOnly = xMspCheckBox.IsChecked.Value;
                process.OutputPath = xMspOutputPath.Text;
                var textRange = new TextRange(xCSVs.Document.ContentStart, xCSVs.Document.ContentEnd);
                String text = textRange.Text.Replace(Environment.NewLine, ",");
                text = CleanCSV(text);
                Properties.Settings.Default["CategoryReviewIdsCSV"] = text;
                Properties.Settings.Default["ImportPath"] = process.ImportPath;
                Properties.Settings.Default["MspOutputPath"] = process.OutputPath;
                Properties.Settings.Default["PlacedProductsOnly"] = process.PlacedProductsOnly;

                Properties.Settings.Default.Save();
                process.CategoryReviewIdsCSV = text;
                process.OnProcessUpdate += Process_OnProcessUpdate;
                startTime = DateTime.Now;
                ThreadPool.QueueUserWorkItem(ProcessMSP, process);

            }
            catch
            {
                this.xTabControl.IsEnabled = true;
                this.xProcessButton.IsEnabled = true;
                this.xProcessButton.Visibility = System.Windows.Visibility.Visible;
                this.xStatus.Text = String.Empty;
            }
        }

        private DateTime startTime = DateTime.Now;
        void Process_OnProcessUpdate(PlanType planType, Int32 currentValue, Int32 maxValue, Int32 totalCurrentValue, Int32 totalMaxValue)
        {
            this.xStatus.Dispatcher.Invoke((Action)(() => {
                if (currentValue > 0 && totalCurrentValue > 0)
                {
                    TimeSpan diff = DateTime.Now - startTime;
                    TimeSpan perPlan = new TimeSpan((Int64)((diff.Ticks / totalCurrentValue) * (totalMaxValue - totalCurrentValue)));
                    xStatus.Text = String.Format(@"Processing {0} Plans {1}/{2} - Total {3}/{4} est Time {5:hh\:mm\:ss}", planType, currentValue, maxValue, totalCurrentValue, totalMaxValue, perPlan);
                }
                else
                {
                    xStatus.Text = String.Format("Processing {0} Plans {1}/{2} - Total {3}/{4}", planType, currentValue, maxValue, totalCurrentValue, totalMaxValue);
                }
            }));
            
        }
        void Process_OnProcessUpdate(Int32 currentValue, Int32 maxValue, Int32 totalCurrentValue)
        {
            this.xStatus.Dispatcher.Invoke((Action)(() =>
            {
                if (totalCurrentValue > 0)
                {
                    Single estTotal = ((Single)totalCurrentValue / (currentValue > 0 ? currentValue : 1)) * maxValue;
                    TimeSpan diff = DateTime.Now - startTime;
                    TimeSpan perPlan = new TimeSpan((Int64)((diff.Ticks / totalCurrentValue) * (estTotal - totalCurrentValue)));
                    xStatus.Text = String.Format(@"Processing Files {0}/{1} - Total Plans {2} est Time {3:hh\:mm\:ss}", currentValue, maxValue, totalCurrentValue, perPlan);
                }
                else
                {
                    xStatus.Text = String.Format("Processing Files {0}/{1} - Total Plans {2}", currentValue, maxValue, totalCurrentValue);
                }
            }));

        }

        private void ProcessV7(Object state)
        {

            ProcessFactory.Execute(state as LV7.ConvertProcess);
            this.xProcessButton.Dispatcher.Invoke((Action)(() => {
                (state as LV7.ConvertProcess).OnProcessUpdate -= Process_OnProcessUpdate;
                this.xTabControl.IsEnabled = true;
                this.xProcessButton.IsEnabled = true;
                this.xProcessButton.Visibility = System.Windows.Visibility.Visible;
                this.xStatus.Text = String.Empty;
            }));
            

        }

        private void ProcessMSP(Object state)
        {

            ProcessFactory.Execute(state as LMSP.ConvertProcess);
            this.xProcessButton.Dispatcher.Invoke((Action)(() =>
            {
                (state as LMSP.ConvertProcess).OnProcessUpdate -= Process_OnProcessUpdate;
                this.xTabControl.IsEnabled = true;
                this.xProcessButton.IsEnabled = true;
                this.xProcessButton.Visibility = System.Windows.Visibility.Visible;
                this.xStatus.Text = String.Empty;
            }));


        }
    }
}
