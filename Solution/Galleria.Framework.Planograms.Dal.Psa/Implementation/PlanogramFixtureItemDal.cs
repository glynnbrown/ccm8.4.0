﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820

// V8-30754 : A.Silva
//  Created.

#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramFixtureItemDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramFixtureItemDal : DalBase, IPlanogramFixtureItemDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch a list of <see cref="PlanogramFixtureItemDto"/> that match the given <paramref name="planogramId" />.
        /// </summary>
        /// <param name="planogramId">Id of the <c>Planogram</c> that the <c>Fixture Item</c> instances belong to.</param>
        /// <returns>A new list of <c>DTO</c> instances representing each <c>Fixture Item</c> in a particular <c>Planogram</c>.</returns>
        /// <remarks>If no <c>Planogram</c> is found for the given <paramref name="planogramId" />, an empty list will be returned.</remarks>
        public IEnumerable<PlanogramFixtureItemDto> FetchByPlanogramId(Object planogramId)
        {
            return DalContext.DalCache.GetPlanogramFixtureItemDtos(planogramId);
        }

        #endregion

        #region IPlanogramFixtureItemDal Members

        public void Insert(PlanogramFixtureItemDto dto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramFixtureItemDto>(dto);
        }

        public void Update(PlanogramFixtureItemDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void DeleteById(Object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Insert(IEnumerable<PlanogramFixtureItemDto> dtos)
        {
            foreach (PlanogramFixtureItemDto dto in dtos)
            {
                Insert(dto);
            }
        }

        public void Update(IEnumerable<PlanogramFixtureItemDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramFixtureItemDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramFixtureItemDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}