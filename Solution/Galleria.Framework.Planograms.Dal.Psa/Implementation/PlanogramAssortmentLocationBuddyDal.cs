﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// CCM-31550 : A.Probyn
//  Created
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    public class PlanogramAssortmentLocationBuddyDal : DalBase, IPlanogramAssortmentLocationBuddyDal
    {
        #region Fetch

        public IEnumerable<PlanogramAssortmentLocationBuddyDto> FetchByPlanogramAssortmentId(Object assortmentId)
        {
            return DalContext.DalCache.GetPlanogramAssortmentLocationBuddyDtos(assortmentId);
        }

        #endregion

        #region Insert

        public void Insert(PlanogramAssortmentLocationBuddyDto dto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramAssortmentLocationBuddyDto>(dto);
        }

        public void Insert(IEnumerable<PlanogramAssortmentLocationBuddyDto> dtos)
        {
            foreach (PlanogramAssortmentLocationBuddyDto dto in dtos)
            {
                Insert(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramAssortmentLocationBuddyDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Update(IEnumerable<PlanogramAssortmentLocationBuddyDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramAssortmentLocationBuddyDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramAssortmentLocationBuddyDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}
