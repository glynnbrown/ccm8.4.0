﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    public class PlanogramValidationTemplateGroupMetricDal : DalBase, IPlanogramValidationTemplateGroupMetricDal
    {
        #region Fetch

        public IEnumerable<PlanogramValidationTemplateGroupMetricDto> FetchByPlanogramValidationTemplateGroupId(object planogramValidationTemplateGroupId)
        {
            return new List<PlanogramValidationTemplateGroupMetricDto>();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramValidationTemplateGroupMetricDto dto)
        {
            //not needed
        }

        public void Insert(IEnumerable<PlanogramValidationTemplateGroupMetricDto> dtos)
        {
            ///not needed
        }

        #endregion

        #region Update

        public void Update(PlanogramValidationTemplateGroupMetricDto dto)
        {
            //not needed
        }

        public void Update(IEnumerable<PlanogramValidationTemplateGroupMetricDto> dtos)
        {
            //not needed
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            //not needed
        }

        public void Delete(PlanogramValidationTemplateGroupMetricDto dto)
        {
            //not needed
        }

        public void Delete(IEnumerable<PlanogramValidationTemplateGroupMetricDto> dtos)
        {
            //not needed
        }

        #endregion
    }
}