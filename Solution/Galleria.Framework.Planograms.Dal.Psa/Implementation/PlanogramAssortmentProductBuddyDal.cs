﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// CCM-31550 : A.Probyn
//  Created
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    public class PlanogramAssortmentProductBuddyDal : DalBase, IPlanogramAssortmentProductBuddyDal
    {
        #region Fetch

        public IEnumerable<PlanogramAssortmentProductBuddyDto> FetchByPlanogramAssortmentId(Object assortmentId)
        {
            return DalContext.DalCache.GetPlanogramAssortmentProductBuddyDtos(assortmentId);
        }

        #endregion

        #region Insert

        public void Insert(PlanogramAssortmentProductBuddyDto dto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramAssortmentProductBuddyDto>(dto);
        }

        public void Insert(IEnumerable<PlanogramAssortmentProductBuddyDto> dtos)
        {
            foreach (PlanogramAssortmentProductBuddyDto dto in dtos)
            {
                Insert(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramAssortmentProductBuddyDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Update(IEnumerable<PlanogramAssortmentProductBuddyDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramAssortmentProductBuddyDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramAssortmentProductBuddyDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}
