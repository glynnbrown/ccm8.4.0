﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820

// V8-30754 : A.Silva
//  Created.

#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramConsumerDecisionTreeNodeDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramConsumerDecisionTreeNodeDal : DalBase, IPlanogramConsumerDecisionTreeNodeDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch the <see cref="PlanogramConsumerDecisionTreeDto"/> that matches the given <paramref name="planogramConsumerDecisionTreeId" />.
        /// </summary>
        /// <param name="planogramConsumerDecisionTreeId">Id of the <c>Planogram</c> that the <c>ConsumerDecisionTree</c> instance belongs to.</param>
        /// <returns>A new <c>DTO</c> instance representing the <c>ConsumerDecisionTree</c> in a particular <c>Planogram</c>.</returns>
        /// <remarks>This implementation just creates an empty <c>ConsumerDecisionTree</c> instance for the given <paramerf name="planogramId" />.</remarks>
        public IEnumerable<PlanogramConsumerDecisionTreeNodeDto> FetchByPlanogramConsumerDecisionTreeId(Object planogramConsumerDecisionTreeId)
        {
            return DalContext.DalCache.GetPlanogramConsumerDecisionTreeNodeDtos(planogramConsumerDecisionTreeId);
        }

        #endregion

        #region IPlanogramConsumerDecisionTreeNodeDal Members

        public void Insert(PlanogramConsumerDecisionTreeNodeDto dto)
        {
            //not needed
        }

        public void Update(PlanogramConsumerDecisionTreeNodeDto dto)
        {
            //not needed
        }

        public void DeleteById(Object id)
        {
            //not needed
        }

        void IBatchDal<PlanogramConsumerDecisionTreeNodeDto>.Insert(PlanogramConsumerDecisionTreeNodeDto dto)
        {
            Insert(dto);
        }

        public void Insert(IEnumerable<PlanogramConsumerDecisionTreeNodeDto> dtos)
        {
            //not needed
        }

        void IBatchDal<PlanogramConsumerDecisionTreeNodeDto>.Update(PlanogramConsumerDecisionTreeNodeDto dto)
        {
            Update(dto);
        }

        public void Update(IEnumerable<PlanogramConsumerDecisionTreeNodeDto> dtos)
        {
            //not needed
        }

        public void Delete(PlanogramConsumerDecisionTreeNodeDto dto)
        {
            //not needed
        }

        public void Delete(IEnumerable<PlanogramConsumerDecisionTreeNodeDto> dtos)
        {
            //not needed
        }

        #endregion
    }
}