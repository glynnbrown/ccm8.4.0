﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820

// V8-30754 : A.Silva
//  Created.

#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramEventLogDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramEventLogDal : DalBase, IPlanogramEventLogDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch the list of <see cref="PlanogramEventLogDto"/> that matches the given <paramref name="planogramId" />.
        /// </summary>
        /// <param name="planogramId">Id of the <c>Planogram</c> that the <c>Event Log</c> instances belong to.</param>
        /// <returns>A new list of <c>DTO</c> instances representing the <c>Event Log</c> in a particular <c>Planogram</c>.</returns>
        /// <remarks>This implementation just creates an empty list of <c>Event Log</c> instances for the given <paramerf name="planogramId" />.</remarks>
        public IEnumerable<PlanogramEventLogDto> FetchByPlanogramId(Object planogramId)
        {
            return DalContext.DalCache.GetPlanogramEventLogDtos(planogramId);
        }

        #endregion

        #region IPlanogramEventLogDal Members

        public void Insert(PlanogramEventLogDto dto)
        {
            //not needed
        }

        public void Update(PlanogramEventLogDto dto)
        {
            //not needed
        }

        public void DeleteById(Object id)
        {
            //not needed
        }

        void IBatchDal<PlanogramEventLogDto>.Insert(PlanogramEventLogDto dto)
        {
            Insert(dto);
        }

        public void Insert(IEnumerable<PlanogramEventLogDto> dtos)
        {
            //not needed
        }

        void IBatchDal<PlanogramEventLogDto>.Update(PlanogramEventLogDto dto)
        {
            Update(dto);
        }

        public void Update(IEnumerable<PlanogramEventLogDto> dtos)
        {
            //not needed
        }

        public void Delete(PlanogramEventLogDto dto)
        {
            //not needed
        }

        public void Delete(IEnumerable<PlanogramEventLogDto> dtos)
        {
            //not needed
        }

        #endregion
    }
}