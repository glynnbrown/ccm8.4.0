﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// CCM-31551 : A.Probyn
//  Created
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    public class PlanogramAssortmentInventoryRuleDal : DalBase, IPlanogramAssortmentInventoryRuleDal
    {
        #region Fetch

        public IEnumerable<PlanogramAssortmentInventoryRuleDto> FetchByPlanogramAssortmentId(Object assortmentId)
        {
            return DalContext.DalCache.GetPlanogramAssortmentInventoryRuleDtos(assortmentId);
        }

        #endregion

        #region Insert

        public void Insert(PlanogramAssortmentInventoryRuleDto dto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramAssortmentInventoryRuleDto>(dto);
        }

        public void Insert(IEnumerable<PlanogramAssortmentInventoryRuleDto> dtos)
        {
            foreach (PlanogramAssortmentInventoryRuleDto dto in dtos)
            {
                Insert(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramAssortmentInventoryRuleDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Update(IEnumerable<PlanogramAssortmentInventoryRuleDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramAssortmentInventoryRuleDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramAssortmentInventoryRuleDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}
