﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820

// V8-30754 : A.Silva
//  Created.

#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramFixtureAssemblyDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramFixtureAssemblyDal : DalBase, IPlanogramFixtureAssemblyDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch a list of <see cref="PlanogramFixtureAssemblyDto"/> that match the given <paramref name="planogramFixtureId" />.
        /// </summary>
        /// <param name="planogramFixtureId">Id of the <c>Planogram Fixture</c> that the <c>Assembly</c> instances belong to.</param>
        /// <returns>A new list of <c>DTO</c> instances representing each <c>Assembly</c> in a particular <c>Planogram Fixture</c>.</returns>
        /// <remarks>An empty list will be returned.</remarks>
        public IEnumerable<PlanogramFixtureAssemblyDto> FetchByPlanogramFixtureId(Object planogramFixtureId)
        {
            return DalContext.DalCache.GetPlanogramFixtureAssemblyDtos(planogramFixtureId);
        }

        #endregion

        #region IPlanogramFixtureAssemblyDal Members

        public void Insert(PlanogramFixtureAssemblyDto dto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramFixtureAssemblyDto>(dto);
        }

        public void Update(PlanogramFixtureAssemblyDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void DeleteById(Object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Insert(IEnumerable<PlanogramFixtureAssemblyDto> dtos)
        {
            foreach (PlanogramFixtureAssemblyDto dto in dtos)
            {
                Insert(dto);
            }
        }

        public void Update(IEnumerable<PlanogramFixtureAssemblyDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramFixtureAssemblyDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramFixtureAssemblyDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}