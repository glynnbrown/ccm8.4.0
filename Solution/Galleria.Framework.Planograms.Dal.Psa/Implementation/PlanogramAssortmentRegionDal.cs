﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820

// V8-30754 : A.Silva
//  Created.

#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramAssortmentRegionDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramAssortmentRegionDal : DalBase, IPlanogramAssortmentRegionDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch a list of <see cref="PlanogramAssortmentRegionDto"/> that match the given <paramref name="assortmentId" />.
        /// </summary>
        /// <param name="assortmentId">Id of the <c>Assortment</c> that the <c>Assortment Region</c> instances belong to.</param>
        /// <returns>A new list of <c>DTO</c> instances representing each <c>Assortment Region</c> in a particular <c>Assortment</c>.</returns>
        /// <remarks>This implementation just returns an empty list.</remarks>
        public IEnumerable<PlanogramAssortmentRegionDto> FetchByPlanogramAssortmentId(Object assortmentId)
        {
            return DalContext.DalCache.GetPlanogramAssortmentRegionDtos(assortmentId);
        }

        #endregion

        #region IPlanogramAssortmentRegionDal Members

        public void Insert(PlanogramAssortmentRegionDto dto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramAssortmentRegionDto>(dto);
        }

        public void Update(PlanogramAssortmentRegionDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void DeleteById(Object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Insert(IEnumerable<PlanogramAssortmentRegionDto> dtos)
        {
            foreach (PlanogramAssortmentRegionDto dto in dtos)
            {
                Insert(dto);
            }
        }

        public void Update(IEnumerable<PlanogramAssortmentRegionDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramAssortmentRegionDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramAssortmentRegionDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}