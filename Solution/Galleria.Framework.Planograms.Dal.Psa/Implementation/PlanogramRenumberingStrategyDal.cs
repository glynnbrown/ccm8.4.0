﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820

// V8-30754 : A.Silva
//  Created.

#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.External;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramRenumberingStrategyDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramRenumberingStrategyDal : DalBase, IPlanogramRenumberingStrategyDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch the <see cref="PlanogramRenumberingStrategyDto"/> that matches the given <paramref name="planogramId" />.
        /// </summary>
        /// <param name="planogramId">Id of the <c>Planogram</c> that the <c>Renumbering Strategy</c> instance belongs to.</param>
        /// <returns>A new list of <c>DTO</c> instances representing each <c>Renumbering Strategy</c> in a particular <c>Planogram</c>.</returns>
        /// <remarks>This implementation just creates an empty <c>Renumbering Strategy</c> instance for the given <paramerf name="planogramId" />.</remarks>
        public PlanogramRenumberingStrategyDto FetchByPlanogramId(Object planogramId)
        {
            return DalContext.DalCache.GetRenumberingStrategyDto(planogramId);
        }

        #endregion

        #region IPlanogramRenumberingStrategyDal Members

        public void Insert(PlanogramRenumberingStrategyDto dto)
        {
            //not needed
        }

        public void Update(PlanogramRenumberingStrategyDto dto)
        {
            //not needed
        }

        public void DeleteById(Object id)
        {
            //not needed
        }

        public void Insert(IEnumerable<PlanogramRenumberingStrategyDto> dtos)
        {
            //not needed
        }

        public void Update(IEnumerable<PlanogramRenumberingStrategyDto> dtos)
        {
            //not needed
        }

        public void Delete(PlanogramRenumberingStrategyDto dto)
        {
            //not needed
        }

        public void Delete(IEnumerable<PlanogramRenumberingStrategyDto> dtos)
        {
            //not needed
        }

        #endregion
    }
}