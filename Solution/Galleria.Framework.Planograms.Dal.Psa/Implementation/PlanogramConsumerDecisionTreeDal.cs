﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820

// V8-30754 : A.Silva
//  Created.

#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramConsumerDecisionTreeDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramConsumerDecisionTreeDal : DalBase, IPlanogramConsumerDecisionTreeDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch the <see cref="PlanogramConsumerDecisionTreeDto"/> that matches the given <paramref name="planogramId" />.
        /// </summary>
        /// <param name="planogramId">Id of the <c>Planogram</c> that the <c>ConsumerDecisionTree</c> instance belongs to.</param>
        /// <returns>A new <c>DTO</c> instance representing the <c>ConsumerDecisionTree</c> in a particular <c>Planogram</c>.</returns>
        /// <remarks>This implementation just creates an empty <c>ConsumerDecisionTree</c> instance for the given <paramerf name="planogramId" />.</remarks>
        public PlanogramConsumerDecisionTreeDto FetchByPlanogramId(Object planogramId)
        {
            return DalContext.DalCache.GetPlanogramConsumerDecisionTreeDto(planogramId);
        }

        #endregion

        #region IPlanogramConsumerDecisionTreeDal Members

        public void Insert(PlanogramConsumerDecisionTreeDto dto)
        {
            //not needed
        }

        public void Update(PlanogramConsumerDecisionTreeDto dto)
        {
            //not needed
        }

        public void DeleteById(Object id)
        {
            //not needed
        }

        public void Insert(IEnumerable<PlanogramConsumerDecisionTreeDto> dtos)
        {
            //not needed
        }

        public void Update(IEnumerable<PlanogramConsumerDecisionTreeDto> dtos)
        {
            //not needed
        }

        public void Delete(PlanogramConsumerDecisionTreeDto dto)
        {
            //not needed
        }

        public void Delete(IEnumerable<PlanogramConsumerDecisionTreeDto> dtos)
        {
            //not needed
        }

        #endregion
    }
}