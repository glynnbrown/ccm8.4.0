﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820

// V8-30754 : A.Silva
//  Created.

#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramComponentDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramComponentDal : DalBase, IPlanogramComponentDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch a list of <see cref="PlanogramComponentDto"/> that match the given <paramref name="planogramId" />.
        /// </summary>
        /// <param name="planogramId">Id of the <c>Planogram</c> that the <c>Component</c> instances belong to.</param>
        /// <returns>A new list of <c>DTO</c> instances representing each <c>Component</c> in a particular <c>Planogram</c>.</returns>
        /// <remarks>If no <c>Planogram</c> is found for the given <paramref name="planogramId" />, an empty list will be returned.</remarks>
        public IEnumerable<PlanogramComponentDto> FetchByPlanogramId(Object planogramId)
        {
            return DalContext.DalCache.GetPlanogramComponentDtos(planogramId);
        }

        #endregion

        #region Implementation of IPlanogramComponentDal

        public void Insert(PlanogramComponentDto dto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramComponentDto>(dto);     
        }

        public void Update(PlanogramComponentDto dto)
        {
            Debug.Assert(false, "Not Implemented");            
        }

        public void DeleteById(Object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Insert(IEnumerable<PlanogramComponentDto> dtos)
        {
            foreach (PlanogramComponentDto dto in dtos)
            {
                Insert(dto);
            }
        }

        public void Update(IEnumerable<PlanogramComponentDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramComponentDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramComponentDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}