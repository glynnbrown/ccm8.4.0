﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    public class PlanogramAssortmentRegionProductDal : DalBase, IPlanogramAssortmentRegionProductDal
    {
        #region Fetch

        public IEnumerable<PlanogramAssortmentRegionProductDto> FetchByPlanogramAssortmentRegionId(object id)
        {
            return new List<PlanogramAssortmentRegionProductDto>();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramAssortmentRegionProductDto dto)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        public void Insert(IEnumerable<PlanogramAssortmentRegionProductDto> dtos)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        #endregion

        #region Update

        public void Update(PlanogramAssortmentRegionProductDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Update(IEnumerable<PlanogramAssortmentRegionProductDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramAssortmentRegionProductDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramAssortmentRegionProductDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}
