﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820

// V8-30754 : A.Silva
//  Created.

#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramMetadataImageDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramMetadataImageDal : DalBase, IPlanogramMetadataImageDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch a list of <see cref="PlanogramMetadataImageDto"/> that match the given <paramref name="planogramId" />.
        /// </summary>
        /// <param name="planogramId">Id of the <c>Planogram</c> that the <c>Image</c> instances belong to.</param>
        /// <returns>A new list of <c>DTO</c> instances representing each <c>Image</c> in a particular <c>Planogram</c>.</returns>
        /// <remarks>This implementation just returns an empty list.</remarks>
        public IEnumerable<PlanogramMetadataImageDto> FetchByPlanogramId(Object planogramId)
        {
            return DalContext.DalCache.GetPlanogramMetadataImageDtos(planogramId);
        }

        #endregion

        #region IPlanogramMetadataImageDal Members

        public void Insert(PlanogramMetadataImageDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Update(PlanogramMetadataImageDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void DeleteById(Object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Insert(IEnumerable<PlanogramMetadataImageDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Update(IEnumerable<PlanogramMetadataImageDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramMetadataImageDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramMetadataImageDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}