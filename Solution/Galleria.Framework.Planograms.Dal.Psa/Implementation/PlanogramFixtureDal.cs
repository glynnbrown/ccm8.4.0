﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820

// V8-30754 : A.Silva
//  Created.

#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramFixtureDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramFixtureDal : DalBase, IPlanogramFixtureDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch a list of <see cref="PlanogramFixtureDto"/> that match the given <paramref name="planogramId" />.
        /// </summary>
        /// <param name="planogramId">Id of the <c>Planogram</c> that the <c>Fixture</c> instances belong to.</param>
        /// <returns>A new list of <c>DTO</c> instances representing each <c>Fixture</c> in a particular <c>Planogram</c>.</returns>
        /// <remarks>If no <c>Planogram</c> is found for the given <paramref name="planogramId" />, an empty list will be returned.</remarks>
        public IEnumerable<PlanogramFixtureDto> FetchByPlanogramId(Object planogramId)
        {
            return DalContext.DalCache.GetPlanogramFixtureDtos(planogramId);
        }

        #endregion

        #region IPlanogramFixtureDal Members

        public void Insert(PlanogramFixtureDto dto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramFixtureDto>(dto);
        }

        public void Update(PlanogramFixtureDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void DeleteById(Object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Insert(IEnumerable<PlanogramFixtureDto> dtos)
        {
            foreach (PlanogramFixtureDto dto in dtos)
            {
                Insert(dto);
            }
        }

        public void Update(IEnumerable<PlanogramFixtureDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramFixtureDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramFixtureDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}