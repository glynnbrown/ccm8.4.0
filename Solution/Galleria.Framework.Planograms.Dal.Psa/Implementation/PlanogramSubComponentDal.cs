﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820

// V8-30754 : A.Silva
//  Created.

#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramSubComponentDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramSubComponentDal : DalBase, IPlanogramSubComponentDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch a list of <see cref="PlanogramSubComponentDto"/> that match the given <paramref name="planogramComponentId" />.
        /// </summary>
        /// <param name="planogramComponentId">Id of the <c>Planogram Sequence</c> that the <c>Sequence Group</c> instances belong to.</param>
        /// <returns>A new list of <c>DTO</c> instances representing each <c>SubComponent</c> in a particular <c>Planogram</c>.</returns>
        /// <remarks>This implementation just returns an empty list.</remarks>
        public IEnumerable<PlanogramSubComponentDto> FetchByPlanogramComponentId(Object planogramComponentId)
        {
            return DalContext.DalCache.GetPlanogramSubComponentDtos(planogramComponentId);
        }

        #endregion

        #region IPlanogramSubComponentDal Members

        public void Insert(PlanogramSubComponentDto dto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramSubComponentDto>(dto);
        }

        public void Update(PlanogramSubComponentDto dto)
        {
            //not used
        }

        public void DeleteById(Object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Insert(IEnumerable<PlanogramSubComponentDto> dtos)
        {
            foreach (PlanogramSubComponentDto dto in dtos)
            {
                Insert(dto);
            }
        }

        public void Update(IEnumerable<PlanogramSubComponentDto> dtos)
        {
            foreach (PlanogramSubComponentDto dto in dtos)
            {
                Update(dto);
            }
        }

        public void Delete(PlanogramSubComponentDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramSubComponentDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}