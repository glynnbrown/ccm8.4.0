﻿#region Header Information

// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800

// CCM-24290 : N.Foster
//  Created

#endregion

#region Version History : CCM820

// V8-30754 : A.Silva
//  Implemented FetchByPackageId.

#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramDal : DalBase, IPlanogramDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch a list of <see cref="PlanogramDto"/> that match the given <paramref name="packageId" />.
        /// </summary>
        /// <param name="packageId">Id of the <c>Package</c> that the <c>Planogram</c> instances belong to.</param>
        /// <returns>A new list of <c>DTO</c> instances representing each <c>Planogram</c> in a particular <c>Package</c>.</returns>
        /// <remarks>If no <c>Package</c> is found for the given <paramref name="packageId" />, an empty list will be returned.</remarks>
        public IEnumerable<PlanogramDto> FetchByPackageId(Object packageId)
        {
            return DalContext.DalCache.GetPlanogramDtos(packageId);
        }

        #endregion

        #region IPlanogramDal Members
        
        public void Insert(PackageDto packageDto, PlanogramDto planogramDto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramDto>(planogramDto, packageDto);
        }

        public void Insert(PlanogramDto planogramDto)
        {
            Insert(null, planogramDto);
        }

        public void Update(PackageDto packageDto, PlanogramDto planogramDto)
        {
            this.DalContext.DalCache.InsertPlanogramDto(packageDto, planogramDto);
        }

        public void Update(PlanogramDto planogramDto)
        {
            Update(null, planogramDto);
        }

        public void DeleteById(Object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}