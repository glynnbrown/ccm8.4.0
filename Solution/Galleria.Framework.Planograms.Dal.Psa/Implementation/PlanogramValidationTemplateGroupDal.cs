﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820

// V8-30754 : A.Silva
//  Created.

#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramValidationTemplateGroupDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramValidationTemplateGroupDal : DalBase, IPlanogramValidationTemplateGroupDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch a list of <see cref="PlanogramValidationTemplateGroupDto"/> that match the given <paramref name="planogramValidationTemplateId" />.
        /// </summary>
        /// <param name="planogramValidationTemplateId">Id of the <c>Planogram ValidationTemplate</c> that the <c>ValidationTemplate Group</c> instances belong to.</param>
        /// <returns>A new list of <c>DTO</c> instances representing each <c>ValidationTemplateGroup</c> in a particular <c>Planogram</c>.</returns>
        /// <remarks>This implementation just returns an empty list.</remarks>
        public IEnumerable<PlanogramValidationTemplateGroupDto> FetchByPlanogramValidationTemplateId(Object planogramValidationTemplateId)
        {
            return DalContext.DalCache.GetPlanogramValidationTemplateGroupDtos(planogramValidationTemplateId);
        }

        #endregion

        #region IPlanogramValidationTemplateGroupDal Members

        public void Insert(PlanogramValidationTemplateGroupDto dto)
        {
            //not needed
        }

        public void Update(PlanogramValidationTemplateGroupDto dto)
        {
            //not needed
        }

        public void DeleteById(Object id)
        {
            //not needed
        }

        public void Insert(IEnumerable<PlanogramValidationTemplateGroupDto> dtos)
        {
            //not needed
        }

        public void Update(IEnumerable<PlanogramValidationTemplateGroupDto> dtos)
        {
            //not needed
        }

        public void Delete(PlanogramValidationTemplateGroupDto dto)
        {
            //not needed
        }

        public void Delete(IEnumerable<PlanogramValidationTemplateGroupDto> dtos)
        {
            //not needed
        }

        #endregion
    }
}