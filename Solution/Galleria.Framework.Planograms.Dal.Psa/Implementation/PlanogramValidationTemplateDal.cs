﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820

// V8-30754 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramValidationTemplateDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramValidationTemplateDal : DalBase, IPlanogramValidationTemplateDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch the <see cref="PlanogramValidationTemplateDto"/> that matches the given <paramref name="planogramId" />.
        /// </summary>
        /// <param name="planogramId">Id of the <c>Planogram</c> that the <c>Validation Template</c> instance belongs to.</param>
        /// <returns>A new list of <c>DTO</c> instances representing each <c>Validation Template</c> in a particular <c>Planogram</c>.</returns>
        /// <remarks>This implementation just creates an empty <c>Validation Template</c> instance for the given <paramerf name="planogramId" />.</remarks>
        public PlanogramValidationTemplateDto FetchByPlanogramId(Object planogramId)
        {
            return DalContext.DalCache.GetValidationTemplateDto(planogramId);
        }

        #endregion

        #region IPlanogramValidationTemplateDal Members

        public void Insert(PlanogramValidationTemplateDto dto)
        {
            //not needed
        }

        public void Update(PlanogramValidationTemplateDto dto)
        {
            //not needed
        }

        public void DeleteById(Object id)
        {
            //not needed
        }

        public void Insert(IEnumerable<PlanogramValidationTemplateDto> dtos)
        {
            //not needed
        }

        public void Update(IEnumerable<PlanogramValidationTemplateDto> dtos)
        {
            //not needed
        }

        public void Delete(PlanogramValidationTemplateDto dto)
        {
            //not needed
        }

        public void Delete(IEnumerable<PlanogramValidationTemplateDto> dtos)
        {
            //not needed
        }

        #endregion
    }
}