﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820
// V8-30754 : A.Silva
//  Created.
// V8-31456 : N.Foster
//  Added method to fetch a batch of custom attributes
#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="ICustomAttributeDataDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class CustomAttributeDataDal : DalBase, ICustomAttributeDataDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch the <see cref="CustomAttributeDataDto"/> that matches the given <paramref name="parentType" /> and <paramref name="parentId"/>.
        /// </summary>
        /// <param name="parentType">Type of parent that the <c>Custom Attribute Data</c> instance belongs to.</param>
        /// <param name="parentId">Id of the parent that the <c>Custom Attribute Data</c> instance belongs to.</param>
        /// <returns>A new <c>DTO</c> instance representing the <c>Custom Attribut eData</c> in a particular container.</returns>
        /// <remarks>If no <c>Custom Attribute Data</c> is found, an empty DTO will be returned.</remarks>
        public CustomAttributeDataDto FetchByParentTypeParentId(Byte parentType, Object parentId)
        {
            CustomAttributeDataDto dto = DalContext.DalCache.GetCustomAttributeDataDto(parentType, parentId);
            if (dto == null) throw new DtoDoesNotExistException();

            return dto;
        }

        /// <summary>
        /// Returns all custom attribute data for the specified parent ids
        /// </summary>
        public IEnumerable<CustomAttributeDataDto> FetchByParentTypeParentIds(Byte parentType, IEnumerable<Object> parentIds)
        {
            HashSet<Object> parentLookup = new HashSet<Object>();
            foreach (Object parentId in parentIds) parentLookup.Add(parentId);

            List<CustomAttributeDataDto> dtos = new List<CustomAttributeDataDto>();
            foreach (CustomAttributeDataDto dto in this.DalContext.DalCache.GetCustomAttributeDataDtos())
            {
                if ((dto.ParentType == parentType) && (parentLookup.Contains(dto.ParentId)))
                {
                    dtos.Add(dto);
                }
            }
            return dtos;
        }

        #endregion

        #region ICustomAttributeDataDal Members

        public void Insert(CustomAttributeDataDto dto)
        {
            this.DalContext.DalCache.InsertDto<CustomAttributeDataDto>(dto);
        }

        public void Update(CustomAttributeDataDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Upsert(IEnumerable<CustomAttributeDataDto> dtoList, CustomAttributeDataIsSetDto isSetDto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void DeleteById(Object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Insert(IEnumerable<CustomAttributeDataDto> dtos)
        {
            foreach (CustomAttributeDataDto dto in dtos)
            {
                Insert(dto);
            }
        }

        public void Update(IEnumerable<CustomAttributeDataDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(CustomAttributeDataDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<CustomAttributeDataDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}