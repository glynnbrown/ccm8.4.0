﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820

// V8-30754 : A.Silva
//  Created.

#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramPerformanceDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramPerformanceDal : DalBase, IPlanogramPerformanceDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch the <see cref="PlanogramPerformanceDto"/> that matches the given <paramref name="planogramId" />.
        /// </summary>
        /// <param name="planogramId">Id of the <c>Planogram</c> that the <c>Performance</c> instance belongs to.</param>
        /// <returns>A new <c>DTO</c> instance representing the <c>Performance</c> in a particular <c>Planogram</c>.</returns>
        /// <remarks>This implementation just creates an empty <c>Performance</c> instance for the given <paramerf name="planogramId" />.</remarks>
        public PlanogramPerformanceDto FetchByPlanogramId(Object planogramId)
        {
            return DalContext.DalCache.GetPlanogramPerformanceDto(planogramId);
        }

        #endregion

        #region IPlanogramPerformanceDal Members

        public void Insert(PlanogramPerformanceDto dto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramPerformanceDto>(dto);         
        }

        public void Update(PlanogramPerformanceDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void DeleteById(Object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Insert(IEnumerable<PlanogramPerformanceDto> dtos)
        {
            foreach (PlanogramPerformanceDto dto in dtos)
            {
                Insert(dto);
            }
        }

        public void Update(IEnumerable<PlanogramPerformanceDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramPerformanceDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramPerformanceDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}