﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    public class PlanogramComparisonDal : DalBase, IPlanogramComparisonDal
    {
        #region Fetch

        public PlanogramComparisonDto FetchByPlanogramId(object id)
        {
            throw new DtoDoesNotExistException();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramComparisonDto dto)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        public void Insert(IEnumerable<PlanogramComparisonDto> dtos)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        #endregion

        #region Update

        public void Update(PlanogramComparisonDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Update(IEnumerable<PlanogramComparisonDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramComparisonDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramComparisonDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}
