﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820

// V8-30754 : A.Silva
//  Created.

#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramAssortmentProductDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramAssortmentProductDal : DalBase, IPlanogramAssortmentProductDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch a list of <see cref="PlanogramAssortmentProductDto"/> that match the given <paramref name="assortmentId" />.
        /// </summary>
        /// <param name="assortmentId">Id of the <c>Assortment</c> that the <c>Assortment Product</c> instances belong to.</param>
        /// <returns>A new list of <c>DTO</c> instances representing each <c>Assortment Product</c> in a particular <c>Assortment</c>.</returns>
        /// <remarks>This implementation just returns an empty list.</remarks>
        public IEnumerable<PlanogramAssortmentProductDto> FetchByPlanogramAssortmentId(Object assortmentId)
        {
            return DalContext.DalCache.GetPlanogramAssortmentProductDtos(assortmentId);
        }

        #endregion

        #region Implementation of IPlanogramAssortmentProductDal

        public void Insert(PlanogramAssortmentProductDto dto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramAssortmentProductDto>(dto);
        }

        public void Update(PlanogramAssortmentProductDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void DeleteById(Object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Insert(IEnumerable<PlanogramAssortmentProductDto> dtos)
        {
            foreach (PlanogramAssortmentProductDto dto in dtos)
            {
                Insert(dto);
            }
        }

        public void Update(IEnumerable<PlanogramAssortmentProductDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramAssortmentProductDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramAssortmentProductDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}