﻿#region Header Information

// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800

// CCM-24290 : N.Foster
//  Created
// V8-25916 : N.Foster
//  Changed LockById to return success or failure
// V8-27919 : L.Ineson
//  Added fetchArgument to FetchById

#endregion

#region Version History: CCM820

// V8-30754 : A.Silva
//  Refactored FetchById(Object, Object) so that an unsuccesful call to GetDto throws a DtoDoesNotExistException.
// V8-31093 : A.Silva
//  Refefactored to use ImportContext.
// V8-31175 : A.Silva
//  Removed references to plan name as that is handled in the dal cache now.

#endregion


#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.External;
using Galleria.Framework.Planograms.Model;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPackageDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PackageDal : DalBase, IPackageDal
    {
        #region Lock

        /// <summary>
        ///     Locks the specified item
        /// </summary>
        public Byte LockById(Object id, Object userId, Byte lockType, Boolean readOnly)
        {
            var successful = true;
            try
            {
                DalContext.DalCache.Open(readOnly);
            }
            catch
            {
                successful = false;
            }
            return (Byte) (successful ? 1 : 0);
        }

        #endregion

        #region Unlock

        /// <summary>
        ///     Unlocks the specified item
        /// </summary>
        public Byte UnlockById(Object id, Object lockUserId, Byte lockType, Boolean lockReadOnly)
        {
            // ensure that the id matches the file
            if (!id.Equals(DalContext.DalCache.FilePath)) throw new DtoDoesNotExistException();

            // open the file in the cache
            DalContext.DalCache.Close();

            // reutrn success
            return 1; // Success
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Returns the specified dto from the data source
        /// </summary>
        public PackageDto FetchById(Object id)
        {
            return FetchById(id, new List<PlanogramFieldMappingDto>());
        }

        /// <summary>
        ///     Returns the specified dto from the data source
        /// </summary>
        public PackageDto FetchById(Object id, Object fetchArgument)
        {
            //  Get the Custom Field Mappings from the argument if there are any...
            var args = fetchArgument as Object[];
            ImportContext context;
            if (args != null)
            {
                context = args.OfType<ImportContext>().FirstOrDefault() ?? new ImportContext(PlanogramSettings.NewPlanogramSettings(), new List<PlanogramFieldMappingDto>(), new List<PlanogramMetricMappingDto>());
            }
            else
            {
                context = new ImportContext(PlanogramSettings.NewPlanogramSettings(), new List<PlanogramFieldMappingDto>(), new List<PlanogramMetricMappingDto>());
            }
            
            //  Refresh the cache before loading the package in case the custom mappings have changed.
            DalContext.DalCache.RefreshCache(context.FieldMappings, context.MetricMappings);

            //  Set the optional field mappings.
            PackageDto dto = DalContext.DalCache.GetPackageDto(id);
            if (dto == null) throw new DtoDoesNotExistException();

            return dto;
        }

        public IList<Object> FetchIdsExceptFor(IEnumerable<string> ucrs)
        {
            //  This method is not implemented for this DAL, however the interface requires it. 
            //  Returning an empty list for now is always going to represent no non matching UCRs.
            return new List<Object>();
        }

        public IList<Object> FetchByUcrsDateLastModified(IEnumerable<String> ucrs, DateTime dateLastModified)
        {
            //  This method is not implemented for this DAL, however the interface requires it. 
            //  Returning an empty list for now is always going to represent no non matching UCRs.
            return new List<Object>();
        }

        public IList<Object> FetchDeletedByUcrs(IEnumerable<String> ucrs)
        {
            //  This method is not implemented for this DAL, however the interface requires it. 
            //  Returning an empty list for now is always going to represent no non matching UCRs.
            return new List<Object>();
        }

        #endregion

            #region IPackageDal Members

        public void Insert(PackageDto dto)
        {
            this.DalContext.DalCache.InsertDto<PackageDto>(dto);
        }

        public void Update(PackageDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void DeleteById(Object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public Byte UpdatePlanogramAttributes(IEnumerable<PlanogramAttributesDto> planAttributesDtoList)
        {
            Debug.Assert(false, "Not Implemented");
            return 0;
        }

        #endregion
    }
}
