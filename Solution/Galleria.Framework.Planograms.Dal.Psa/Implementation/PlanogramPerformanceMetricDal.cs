﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820

// V8-30754 : A.Silva
//  Created.

#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramPerformanceMetricDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramPerformanceMetricDal : DalBase, IPlanogramPerformanceMetricDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch a list of <see cref="PlanogramPerformanceMetricDto"/> that match the given <paramref name="planogramId" />.
        /// </summary>
        /// <param name="planogramId">Id of the <c>Planogram</c> that the <c>Image</c> instances belong to.</param>
        /// <returns>A new list of <c>DTO</c> instances representing each <c>Image</c> in a particular <c>Planogram</c>.</returns>
        /// <remarks>This implementation just returns an empty list.</remarks>
        public IEnumerable<PlanogramPerformanceMetricDto> FetchByPlanogramPerformanceId(Object planogramId)
        {
            return DalContext.DalCache.GetPlanogramPerformanceMetricDtos(planogramId);
        }

        #endregion

        #region IPlanogramPerformanceMetricDal Members

        public void Insert(PlanogramPerformanceMetricDto dto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramPerformanceMetricDto>(dto);
        }

        public void Update(PlanogramPerformanceMetricDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void DeleteById(Object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Insert(IEnumerable<PlanogramPerformanceMetricDto> dtos)
        {
            foreach (PlanogramPerformanceMetricDto dto in dtos)
            {
                Insert(dto);
            }
        }

        public void Update(IEnumerable<PlanogramPerformanceMetricDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramPerformanceMetricDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramPerformanceMetricDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}