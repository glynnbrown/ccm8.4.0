﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820

// V8-30754 : A.Silva
//  Created.

#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramImageDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramImageDal : DalBase, IPlanogramImageDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch a list of <see cref="PlanogramImageDto"/> that match the given <paramref name="planogramId" />.
        /// </summary>
        /// <param name="planogramId">Id of the <c>Planogram</c> that the <c>Image</c> instances belong to.</param>
        /// <returns>A new list of <c>DTO</c> instances representing each <c>Image</c> in a particular <c>Planogram</c>.</returns>
        /// <remarks>This implementation just returns an empty list.</remarks>
        public IEnumerable<PlanogramImageDto> FetchByPlanogramId(Object planogramId)
        {
            return DalContext.DalCache.GetPlanogramImageDtos(planogramId);
        }

        #endregion

        #region IPlanogramImageDal Members

        public void Insert(PlanogramImageDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Update(PlanogramImageDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void DeleteById(Object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Insert(IEnumerable<PlanogramImageDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Update(IEnumerable<PlanogramImageDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramImageDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramImageDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}