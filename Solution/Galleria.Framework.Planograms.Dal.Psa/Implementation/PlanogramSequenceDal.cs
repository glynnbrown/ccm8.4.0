﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820

// V8-30754 : A.Silva
//  Created.

#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramSequenceDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramSequenceDal : DalBase, IPlanogramSequenceDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch the <see cref="PlanogramSequenceDto"/> that matches the given <paramref name="planogramId" />.
        /// </summary>
        /// <param name="planogramId">Id of the <c>Planogram</c> that the <c>Renumbering Strategy</c> instance belongs to.</param>
        /// <returns>A new list of <c>DTO</c> instances representing each <c>Renumbering Strategy</c> in a particular <c>Planogram</c>.</returns>
        /// <remarks>This implementation just creates an empty <c>Renumbering Strategy</c> instance for the given <paramerf name="planogramId" />.</remarks>
        public PlanogramSequenceDto FetchByPlanogramId(Object planogramId)
        {
            return DalContext.DalCache.GetSequenceDto(planogramId);
        }

        #endregion

        #region IPlanogramSequenceDal Members

        public void Insert(PlanogramSequenceDto dto)
        {
            //not used
        }

        public void Update(PlanogramSequenceDto dto)
        {
            //not used
        }

        public void DeleteById(Object id)
        {
            //not used
        }

        public void Insert(IEnumerable<PlanogramSequenceDto> dtos)
        {
            //not used
        }

        public void Update(IEnumerable<PlanogramSequenceDto> dtos)
        {
            //not used
        }

        public void Delete(PlanogramSequenceDto dto)
        {
            //not used
        }

        public void Delete(IEnumerable<PlanogramSequenceDto> dtos)
        {
            //not used
        }

        #endregion
    }
}