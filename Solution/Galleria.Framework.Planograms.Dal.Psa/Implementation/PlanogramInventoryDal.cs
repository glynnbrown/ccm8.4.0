﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820

// V8-30754 : A.Silva
//  Created.

#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramInventoryDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramInventoryDal : DalBase, IPlanogramInventoryDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch the <see cref="PlanogramInventoryDto"/> that matches the given <paramref name="planogramId" />.
        /// </summary>
        /// <param name="planogramId">Id of the <c>Planogram</c> that the <c>Inventory</c> instance belongs to.</param>
        /// <returns>A new <c>DTO</c> instance representing the <c>Inventory</c> in a particular <c>Planogram</c>.</returns>
        /// <remarks>This implementation just creates an empty <c>Inventory</c> instance for the given <paramerf name="planogramId" />.</remarks>
        public PlanogramInventoryDto FetchByPlanogramId(Object planogramId)
        {
            return DalContext.DalCache.GetPlanogramInventoryDto(planogramId);
        }

        #endregion

        #region IPlanogramInventoryDal Members

        public void Insert(PlanogramInventoryDto dto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramInventoryDto>(dto);
        }

        public void Update(PlanogramInventoryDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void DeleteById(Object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Insert(IEnumerable<PlanogramInventoryDto> dtos)
        {
            foreach (PlanogramInventoryDto dto in dtos)
            {
                Insert(dto);
            }
        }

        public void Update(IEnumerable<PlanogramInventoryDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramInventoryDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramInventoryDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}