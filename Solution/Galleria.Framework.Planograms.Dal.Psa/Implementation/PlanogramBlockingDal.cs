﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820

// V8-30754 : A.Silva
//  Created.

#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramBlockingDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramBlockingDal : DalBase, IPlanogramBlockingDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch a list of <see cref="PlanogramBlockingDto"/> that match the given <paramref name="planogramId" />.
        /// </summary>
        /// <param name="planogramId">Id of the <c>Planogram</c> that the <c>Blocking</c> instances belong to.</param>
        /// <returns>A new list of <c>DTO</c> instances representing each <c>Blocking</c> in a particular <c>Planogram</c>.</returns>
        /// <remarks>This implementation just returns an empty list.</remarks>
        public IEnumerable<PlanogramBlockingDto> FetchByPlanogramId(Object planogramId)
        {
            return DalContext.DalCache.GetPlanogramBlockingDtos(planogramId);
        }

        #endregion

        #region IPlanogramBlockingDal Members

        public void Insert(PlanogramBlockingDto dto)
        {
            //not used
        }

        public void Update(PlanogramBlockingDto dto)
        {
            //not used
        }

        public void DeleteById(Object id)
        {
            //not used
        }

        public void Insert(IEnumerable<PlanogramBlockingDto> dtos)
        {
            //not used
        }

        public void Update(IEnumerable<PlanogramBlockingDto> dtos)
        {
            //not used
        }

        public void Delete(PlanogramBlockingDto dto)
        {
            //not used
        }

        public void Delete(IEnumerable<PlanogramBlockingDto> dtos)
        {
            //not used
        }

        #endregion
    }
}