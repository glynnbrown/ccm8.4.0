﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820

// V8-30754 : A.Silva
//  Created.

#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.External;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramAssortmentDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramAssortmentDal : DalBase, IPlanogramAssortmentDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch the <see cref="PlanogramAssortmentDto"/> that matched the given <paramref name="planogramId" />.
        /// </summary>
        /// <param name="planogramId">Id of the <c>Planogram</c> that the <c>Assortment</c> instance belongs to.</param>
        /// <returns>A new <c>DTO</c> instance representing the <c>Assortment</c> in a particular <c>Planogram</c>.</returns>
        /// <remarks>This implementation just creates an empty <c>Assortment</c> instance for the given <paramerf name="planogramId" />.</remarks>
        public PlanogramAssortmentDto FetchByPlanogramId(Object planogramId)
        {
            return DalContext.DalCache.GetAssortmentDto(planogramId);
        }

        #endregion

        #region IPlanogramAssortmentDal Members

        public void Insert(PlanogramAssortmentDto dto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramAssortmentDto>(dto);
        }

        public void Update(PlanogramAssortmentDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void DeleteById(Object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Insert(IEnumerable<PlanogramAssortmentDto> dtos)
        {
            foreach (PlanogramAssortmentDto dto in dtos)
            {
                Insert(dto);
            }
        }

        public void Update(IEnumerable<PlanogramAssortmentDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramAssortmentDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramAssortmentDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}
