﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820

// V8-30754 : A.Silva
//  Created.

#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramSequenceGroupDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramSequenceGroupDal : DalBase, IPlanogramSequenceGroupDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch a list of <see cref="PlanogramSequenceGroupDto"/> that match the given <paramref name="planogramSequenceId" />.
        /// </summary>
        /// <param name="planogramSequenceId">Id of the <c>Planogram Sequence</c> that the <c>Sequence Group</c> instances belong to.</param>
        /// <returns>A new list of <c>DTO</c> instances representing each <c>SequenceGroup</c> in a particular <c>Planogram</c>.</returns>
        /// <remarks>This implementation just returns an empty list.</remarks>
        public IEnumerable<PlanogramSequenceGroupDto> FetchByPlanogramSequenceId(Object planogramSequenceId)
        {
            return DalContext.DalCache.GetPlanogramSequenceGroupDtos(planogramSequenceId);
        }

        #endregion

        #region IPlanogramSequenceGroupDal Members

        public void Insert(PlanogramSequenceGroupDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Update(PlanogramSequenceGroupDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void DeleteById(Object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Insert(IEnumerable<PlanogramSequenceGroupDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Update(IEnumerable<PlanogramSequenceGroupDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramSequenceGroupDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramSequenceGroupDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}