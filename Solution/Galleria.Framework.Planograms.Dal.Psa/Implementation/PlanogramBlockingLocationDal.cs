﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa.Implementation
{
    public class PlanogramBlockingLocationDal : DalBase, IPlanogramBlockingLocationDal
    {
        #region Fetch

        public IEnumerable<PlanogramBlockingLocationDto> FetchByPlanogramBlockingId(object id)
        {
            return new List<PlanogramBlockingLocationDto>();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramBlockingLocationDto dto)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        public void Insert(IEnumerable<PlanogramBlockingLocationDto> dtos)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        #endregion

        #region Update

        public void Update(PlanogramBlockingLocationDto dto)
        {
            //not used
        }

        public void Update(IEnumerable<PlanogramBlockingLocationDto> dtos)
        {
            //not used
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            //not used
        }

        public void Delete(PlanogramBlockingLocationDto dto)
        {
            //not used
        }

        public void Delete(IEnumerable<PlanogramBlockingLocationDto> dtos)
        {
            //not used
        }

        #endregion
    }
}

