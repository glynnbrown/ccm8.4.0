﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
#endregion
#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Psa
{
    /// <summary>
    /// For this dal, the factory represents
    /// a single file on the file system. The
    /// DalCache object is used to cache
    /// and coordinate data between multiple
    /// dal contexts
    /// </summary>
    public class DalFactory : DalFactoryBase
    {
        #region Fields
        private Object _lock = new Object(); // object used for locking
        private DalCache _dalCache; // holds the dal cache
        #endregion

        #region Constructors
        public DalFactory() : base() { }
        public DalFactory(DalFactoryConfigElement dalFactoryConfig) : base(dalFactoryConfig) { }
        #endregion

        #region Methods
        /// <summary>
        /// Creates a new dal context using the default configuration
        /// </summary>
        /// <returns>A new dal context</returns>
        public override IDalContext CreateContext()
        {
            // create the dal cache if required
            if (_dalCache == null)
            {
                lock (_lock)
                {
                    if (_dalCache == null)
                    {
                        _dalCache = new DalCache(this.DalFactoryConfig);
                    }
                }
            }
            
            // return a new dal context
            return new DalContext(_dalCache);
        }

        /// <summary>
        /// Creates a new dal context using the provided configuration
        /// </summary>
        /// <param name="dalFactoryConfig">The dal configuration</param>
        /// <returns>A new dal context</returns>
        public override IDalContext CreateContext(DalFactoryConfigElement dalFactoryConfig)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
