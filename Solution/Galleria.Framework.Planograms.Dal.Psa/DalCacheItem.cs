﻿#region Header Information

// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800

// CCM-24290 : N.Foster
//  Created

#endregion

#region Version History: CCM820

// V8-30754 : A.Silva
//  Added Decoding methods for each dto to be generated.
// V8-31019 : A.Silva
//  Added missing Apply Custom Mappings to Fixtures, Components and products.
// V8-31028 : A.Silva
//  Added correct mapping for pegboard Merch Constraint values.
// V8-31036 : A.Silva
//  Correctly imported the backboard depth
// V8-31032 : A.Silva
//  Added new values found in more recent versions of the ProSpace files for the CanCombine property.
// V8-31031 : A.Silva
//  Removed importing transparency for risers as it does not work correctly.
// V8-31050 : A.Silva
//  Imported correct size for Chest components.
// V8-31030 : A.Silva
//  Added import of Annotations.
// V8-31023 : A.Silva
//  Added support for single planogram access to ProSpace files.
// V8-31036 : A.Silva
//  Amended Backboard Component Creation so that the correct Depth is imported.
// V8-31144 : A.Silva
//  Amended Y coordinate for Annotations that needed to be offset by the annotation's height.
// V8-31149 : A.Silva
//  Imported correct colors for components. Added Fill Pattern import for components.
// V8-31179 : A.Silva
//  Corrected an issue with some dto lookups (productdtolist) not being cleared when resetting the dalcacheitem. It will be cleared now forcing a refresh of the data.
// V8-31182 : A.Silva
//  Added calculated peg depth only to products that are at least on one hang or hang from below subcomponent.
// V8-31170 : A.Silva
//  Added import rule to set Merchandising Strategy to Even if the Size Type is Spaced Out Facings.
// V8-31189 : A.Silva
//  Added file version check and warning to DecodePackageDto().
// V8-31145 : A.Silva
//  Added colour contrast correction when importing annotations with the same back and for colours.
// V8-31217 : A.Silva
//  Commented out the transpacerency values for components and colors just to make sure they do not cause issues.
// V8-31157 : A.Silva
//  Amended SetNotchValues so that they are more accurately imported.
// V8-31137 : A.Silva
//  Amended SetMerchConstraint1Values to account for slotwall-like components.
// V8-31244 : A.Silva
//  Added mapping of LocationId and PositionId to PositionSequenceNumber and ComponentSequenceNumber respectively.
// V8-31220 : A.Silva
//  Corrected the Fixture and Componentes coordinate offset calculation to be accurate.
// V8-31260 : A.Silva
//  Added importing Merchandising Style for X, Y, Z Blocks.
// V8-31271 : A.Silva
//  Amended the way the products are looked up when processing positions and performance so that they do not depend on the mapping, but just on the actual values of the file.
// V8-31276 : A.Silva
//  Added logging of GTIN duplicates and handling.
// V8-31281 : A.Silva
//  When importing Bars no peg data is imported for them, and the pegY coordinate is set to the JDA Space planning default if 0.
// V8-30809 : A.Silva
//  Refactored the parsing of values from the imported files.
// V8-31311 : A.Silva
//  Added calculated values for PerformanceSales and PerformanceProfit fields.
// V8-31303 : A.Silva
//  Amended calculation of Unit Cost from Case Cost.
// V8-31351 : A.Silva
//  Corrected how border sizes are determined depending in the measure system used in the planogram.
//  Amended default colour of riser.
// V8-31137 : A.Silva
//  Amended the way the MerchConstraintRow1Height is set so that if it would look "bad" because it is too high, it gets siezed down until it does not look so bad.
// V8-31422 : A.Silva
//  Amended Profit calculation to use correct unit cost.
// V8-31538 : A.Silva
//  Amended decoding of the source file to make sure we get all the nice characters. taken into account the special codes \" and \r\n.

#endregion

#region Version History: CCM830

// V8-31531 : A.Heathcote
//  Removed IsTrayProduct
// V8-31875 : A.Silva
//  Amended NewProductKey, NewPositionProductKey and NewPerformanceProductKey to avoid some hash collisions.
// V8-31878 : A.Silva
//  Amended DataFields to use the extension helper method to split considering escape characters.
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
// V8-32304 : M.Brumby
//  Defensive coding for performance encode
// V8-32313 : M.Brumby
//  Combine fix
// V8-32335 : M.Brumby
//  Removed some default performance mappings
// V8-32333 : M.Brumby
//  Made degrees 0 to 180 -180 to 0 instead of 0 to 360
// V8-32352 : M.Brumby
//  Added inital merch style to product export
// V8-32373 : M.Brumby
//  Exported position sequence to fix stackinig in multiple directions.
// V8-32368 : M.Brumby
// Front and back caps reversed on export, this has now been reveresed
// V8-32363 : M.Brumby
//  Exported date fields are not converted to PSA format.
// V8-32467 : M.Brumby
//  Added warning for Point of Purchase positons being placed as unit
// V8-32528 : M.Brumby
//  Ensure imported annotation height is positive.
// V8-32517 : M.Brumby
//  Apply squeeze/expand to unit merchandising style only
//  Added product front overhang
//  added dot fill style conversion
// V8-32641 : M.Brumby
//  import squeeze set to true only if the merch "size" of either X, Y or Z of a component is set to squeeze.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.External;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Planograms.Dal.Psa.Resources;

namespace Galleria.Framework.Planograms.Dal.Psa
{
    #region InstanceCache class

    public class InstanceCache
    {
        private readonly DalCacheItem _dalCacheItem;

        public InstanceCache(DalCacheItem dalCacheItem)
        {
            _dalCacheItem = dalCacheItem;
        }

        /// <summary>
        ///     Get the cached instance of the given type.
        /// </summary>
        /// <typeparam name="T">Type of the instance to be retrieved.</typeparam>
        /// <returns></returns>
        internal T Get<T>() where T : class
        {
            Object dto;
            _dalCacheItem.Dtos.TryGetValue(typeof (T), out dto);
            return dto as T;
        }

        /// <summary>
        ///     Set, or update, the cached instance of the given type.
        /// </summary>
        /// <typeparam name="T">Type of the instance to be set.</typeparam>
        /// <param name="instance">The actual instance to be cached.</param>
        /// <returns></returns>
        public void Set<T>(T instance)
        {
            _dalCacheItem.Dtos[typeof (T)] = instance;
        }

        public void Clear()
        {
            _dalCacheItem.Dtos.Clear();
        }
    }

    #endregion

    /// <summary>
    ///     Items used to cache data within the dal
    /// </summary>
    public class DalCacheItem : IDisposable
    {
        #region Constants

        private const Char Delimiter = ','; // the text delimiter
        private const String MetricPropertyNameFormat = "P{0}";

        #endregion

        #region Fields

        private static readonly Object NextIdLock = new Object(); // object used for locking
        private static Int32 _nextId; // holds the next id number
        private DalCacheItem _parent; // holds the parent item
        private String _data; // holds the file data

        /// <summary>
        ///     Holds the dtos per type of dto associated with this cache item.
        /// </summary>
        private readonly Dictionary<Type, Object> _dtos = new Dictionary<Type, Object>();

        private Dictionary<DalCacheItemType, List<DalCacheItem>> _children = new Dictionary<DalCacheItemType, List<DalCacheItem>>();
        private readonly InstanceCache _instanceCache;
        private readonly DalCache _dalCache;
        private List<String> _dataFields;

        private Dictionary<Object, List<PlanogramProductDto>> _planogramProductDtoById;
        private Dictionary<Object, DalCacheItem> _performanceCacheItems;

        /// <summary>
        ///     Stores the cache item type for this dal cache item (lazy loaded through the CacheItemType property).
        /// </summary>
        private DalCacheItemType? _cacheItemType;

        private String _fileVersion;
        private DalCacheItem _planogramProductForPerformance;
        private PlanogramLengthUnitOfMeasureType _planogramLengthUnitOfMeasure = PlanogramLengthUnitOfMeasureType.Unknown;

        private Dictionary<Object, List<PlanogramProductDto>> PlanogramProductDtoById
        {
            get
            {
                if (_planogramProductDtoById != null)
                {
                    return _planogramProductDtoById;
                }
                else
                {
                    List<DalCacheItem> productCacheItems = GetProductCacheItems();
                    Dictionary<Object, List<PlanogramProductDto>> planogramProductDtoById = productCacheItems
                        .ToDictionary(NewProductKey, item => item.DecodePlanogramProductDtos());
                    return _planogramProductDtoById = planogramProductDtoById;
                }
            }
        }

        private DalCacheItem PlanogramProductForPerformance
        {
            get { return _planogramProductForPerformance ?? (_planogramProductForPerformance = GetPlanogramProductDtoForPerformance()); }
        }

        private static Object NewProductKey(DalCacheItem item)
        {
            return HashCode.Start
                           .Hash(item.ExtractValue<String>(ProSpaceImportHelper.ProductId))
                           .Hash(item.ExtractValue<String>(ProSpaceImportHelper.ProductUpc))
                           .GetHashCode();
        }

        private static Object NewPositionProductKey(DalCacheItem item)
        {
            return HashCode.Start
                           .Hash(item.ExtractValue<String>(ProSpaceImportHelper.PositionProductId))
                           .Hash(item.ExtractValue<String>(ProSpaceImportHelper.PositionProductUpc))
                           .GetHashCode();
        }

        private static Object NewPerformanceProductKey(DalCacheItem item)
        {
            return HashCode.Start
                           .Hash(item.ExtractValue<String>(ProSpaceImportHelper.PerformanceId))
                           .Hash(item.ExtractValue<String>(ProSpaceImportHelper.PerformanceUpc))
                           .GetHashCode();
        }

        private List<String> DataFields
        {
            get
            {
                return _dataFields ?? (_dataFields = _data.SplitBy(',').ToList());
            }
        }

        #endregion

        #region Properties

        public DalCacheItem Parent
        {
            get { return _parent; }
            internal set { _parent = value; }
        }

        /// <summary>
        ///     Returns the item id
        /// </summary>
        public String Id { get; private set; }

        /// <summary>
        /// Id used for special occasions, like subcomponents
        /// </summary>
        public String SubId { get; set; }


        public Object ExportOrder { get; set; }
        /// <summary>
        ///     Returns the data for this item
        /// </summary>
        public String Data
        {
            get
            {
                if ((_data == null) &&
                    (_dataFields != null))
                    _data = Encode();
                return _data;
            }
        }

        public Boolean IsBackBoardOrBase
        {
            get
            {
                return Dtos.Any(d => d.Value is PlanogramComponentDto ?
                    (d.Value as PlanogramComponentDto).ComponentType == (Byte)PlanogramComponentType.Backboard ||
                    (d.Value as PlanogramComponentDto).ComponentType == (Byte)PlanogramComponentType.Base 
                    : false);
            }
        }

        public Boolean IsBackBoard
        {
            get
            {
                return Dtos.Any(d => d.Value is PlanogramComponentDto ?
                    (d.Value as PlanogramComponentDto).ComponentType == (Byte)PlanogramComponentType.Backboard
                    : false);
            }
        }

        public Boolean IsBase
        {
            get
            {
                return Dtos.Any(d => d.Value is PlanogramComponentDto ?
                    (d.Value as PlanogramComponentDto).ComponentType == (Byte)PlanogramComponentType.Base
                    : false);
            }
        }
        /// <summary>
        ///     Returns the children of this
        /// </summary>
        public Dictionary<DalCacheItemType, List<DalCacheItem>> Children { get { return _children; } }

        private InstanceCache InstanceCache { get { return _instanceCache; } }
        public Dictionary<Type, Object> Dtos { get { return _dtos; } }

        private Dictionary<Object, DalCacheItem> PerformanceCacheItems
        {
            get { return _performanceCacheItems ?? (_performanceCacheItems = InitializePerformanceCacheItems()); }
        }

        #endregion

        #region Constructors

        /// <summary>
        ///     Creates a new instance of this type
        /// </summary>
        public DalCacheItem(DalCacheItem parent, String id, DalCache dalCache, DalCacheItemType cacheItemType)
        {
            _cacheItemType = cacheItemType;

            _dalCache = dalCache;
            _instanceCache = new InstanceCache(this);

            // generate an id for this item
            Id = id ?? GetNextId();

            // store a reference to the parent
            _parent = parent;

            _dataFields = new List<String>() { _cacheItemType.ToString() };
        }

        /// <summary>
        ///     Creates a new instance of this type
        /// </summary>
        public DalCacheItem(String id, Stream fileStream, DalCache dalCache)
        {
            _dalCache = dalCache;
            _instanceCache = new InstanceCache(this);
            using (TextReader textReader = new EndOfLineStreamReader(fileStream))
            {
                String data = null;
                Initialize(null, id, textReader, ref data);
            }
        }

        /// <summary>
        ///     Creates a new instance of this type
        /// </summary>
        private DalCacheItem(DalCacheItem parent, TextReader textReader, DalCache dalCache, ref String data)
        {
            _dalCache = dalCache;
            _instanceCache = new InstanceCache(this);
            Initialize(parent, null, textReader, ref data);
        }

        #endregion

        #region Methods

        #region Initialization

        /// <summary>
        ///     Initializes the dal cache item
        ///     loading its data and its children
        /// </summary>
        private void Initialize(DalCacheItem parent, String id, TextReader textReader, ref String data)
        {
            // store a reference to the parent
            _parent = parent;

            // generate an id for this item
            Id = id ?? GetNextId();

            // now parse the data 
            while ((data != null) ||
                   (textReader.Peek() >= 0))
            {
                // read a line from the file
                if (data == null) data = textReader.ReadLine();

                if (data != null &&
                    data.StartsWith("; Version"))
                {
                    String version = data.Replace("; Version ", String.Empty);
                    Int32 lastDot = version.LastIndexOf(".", StringComparison.Ordinal);
                    _fileVersion = lastDot != -1 ? version.Substring(0, lastDot) : version;
                }

                // get the item type from the data
                DalCacheItemType itemType = DalCacheItemTypeHelper.GetTypeFor(data);

                _data = data;
                data = null;

                // take acton based on the item type
                switch (itemType)
                {
                    case DalCacheItemType.Project:
                        InitializeChildren(this, DalCacheItemType.Product, textReader, ref data);
                        InitializeChildren(this, DalCacheItemType.Planogram, textReader, ref data);
                        break;
                    case DalCacheItemType.Planogram:
                        InitializeChildren(this, DalCacheItemType.Performance, textReader, ref data);
                        InitializeChildren(this, DalCacheItemType.Segment, textReader, ref data);
                        InitializeChildren(this, DalCacheItemType.Fixture, textReader, ref data);
                        InitializeChildren(this, DalCacheItemType.Drawing, textReader, ref data);
                        break;
                    case DalCacheItemType.Fixture:
                        InitializeChildren(this, DalCacheItemType.Position, textReader, ref data);
                        break;
                }
                if (itemType != DalCacheItemType.Unknown) break;
            }
        }

        /// <summary>
        ///     Initializes all children of the specified type
        /// </summary>
        private void InitializeChildren(DalCacheItem parent, DalCacheItemType itemType, TextReader textReader, ref String data)
        {
            // verify we have created a list to hold the children
            if (!_children.ContainsKey(itemType))
                _children.Add(itemType, new List<DalCacheItem>());

            // now process all items in the file until we hit an item type
            // that is not what we are expecting
            while ((data != null) ||
                   (textReader.Peek() >= 0))
            {
                DalCacheItemType currentType;
                data = ReadNextKnownItem(textReader, data, out currentType);
                
                // determine if the item type matches
                // that which we are expecting
                if (currentType == itemType)
                {
                    // the item matches the type we are expecting
                    // so create a new item and add to the child list
                    _children[itemType].Add(new DalCacheItem(parent, textReader, _dalCache, ref data));
                }
                else
                    break;
            }
        }

        private static String ReadNextKnownItem(TextReader textReader, String data, out DalCacheItemType type)
        {
            type = data == null ? DalCacheItemType.Unknown : DalCacheItemTypeHelper.GetTypeFor(data);

            while (data == null && textReader.Peek() >= 0)
            {
                data = textReader.ReadLine();
                type = DalCacheItemTypeHelper.GetTypeFor(data);
                if (type == DalCacheItemType.Unknown)
                {
                    data = null;
                }
            }

            return data;
        }

        public void RefreshCache()
        {
            foreach (DalCacheItem cacheItem in _children.SelectMany(pair => pair.Value))
            {
                cacheItem.RefreshCache();
            }
            _planogramProductDtoById = null;
            _performanceCacheItems = null;
            _planogramLengthUnitOfMeasure = PlanogramLengthUnitOfMeasureType.Unknown;
            InstanceCache.Clear();
        }

        #endregion

        public T GetDto<T>()
        {
            Object dto = null;
            if (typeof (T) == typeof (PackageDto))
                dto = DecodePackageDto();
            else if (typeof (T) == typeof (PlanogramDto))
                dto = DecodePlanogramDto();
            else if (typeof (T) == typeof (PlanogramFixtureItemDto))
                dto = DecodePlanogramFixtureItemDto();
            else if (typeof (T) == typeof (PlanogramFixtureDto))
                dto = DecodePlanogramFixtureDto();
            else if (typeof (T) == typeof (PlanogramAssemblyDto))
                dto = DecodePlanogramAssemblyDto();
            else if (typeof (T) == typeof (PlanogramComponentDto))
                dto = DecodePlanogramComponentDto();
            else if (typeof (T) == typeof (List<PlanogramComponentDto>))
                dto = DecodePlanogramComponentDtosInSegment();
            else if (typeof (T) == typeof (PlanogramPositionDto))
                dto = DecodePlanogramPositionDto();
            else if (typeof (T) == typeof (List<PlanogramProductDto>))
                dto = DecodePlanogramProductDtos();
            else if (typeof (T) == typeof (PlanogramFixtureComponentDto))
                dto = DecodePlanogramFixtureComponentDto();
            else if (typeof (T) == typeof (List<PlanogramFixtureComponentDto>))
                dto = DecodePlanogramFixtureComponentDtosInSegment();
            else if (typeof (T) == typeof (PlanogramSubComponentDto))
                dto = DecodePlanogramSubComponentDto();
            else if (typeof (T) == typeof (List<PlanogramSubComponentDto>))
                dto = DecodePlanogramSubComponentDtosInSegment();
            else if (typeof (T) == typeof (CustomAttributeDataDto))
                dto = DecodeCustomAttributeDataDto();
            else if (typeof (T) == typeof (List<CustomAttributeDataDto>))
                dto = DecodeCustomAttributeDataDtos();
            else if (typeof(T) == typeof(PlanogramAnnotationDto))
                dto = DecodePlanogramAnnotationDto();
            else if (typeof(T) == typeof(PlanogramPerformanceDto))
                dto = DecodePlanogramPerformanceDto();
            else if (typeof(T) == typeof(List<PlanogramPerformanceDataDto>))
                dto = DecodePlanogramPerformanceDataDto();
            else if (typeof(T) == typeof(List<PlanogramPerformanceMetricDto>))
                dto = DecodePlanogramPerformanceMetricDto();
            else if (typeof(T) == typeof(PlanogramInventoryDto))
                dto = DecodePlanogramInventoryDto();

            if (dto == null) return default(T);

            T typedDto;
            try
            {
                typedDto = (T) Convert.ChangeType(dto, typeof (T));
            }
            catch (Exception e)
            {
                Debug.Fail("Unable to convert the DTO to the expected type.");
                return default(T);
            }
            return typedDto;
        }

        /// <summary>
        /// Inserts a dto into this dal cache item if the type does not exist.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dto"></param>
        /// <returns>true on success, false if the dto type is already loaded</returns>
        public Boolean InsertDto<T>(T dto)
        {
            if (this.Dtos.Any(d => d.Value is T))
            {
                return false;
            }
            else
            {
                this.Dtos.Add(typeof(T), dto);
                return true;
            }
        }

        /// <summary>
        /// Inserts the dtos from a cache item into this one, ignoring
        /// any types that already exist.
        /// </summary>
        /// <param name="sourceItem"></param>
        public void InsertDtos(DalCacheItem sourceItem)
        {
            foreach (var dtoCopy in sourceItem.Dtos)
            {
                if (!this.Dtos.Any(d => d.Key.Equals(dtoCopy.Key)))
                {
                    Dtos[dtoCopy.Key] = dtoCopy.Value;
                }
            }
        }

        public void EncodeDto(Object dto)
        {
            Type t = dto.GetType();

            if (t == typeof(PackageDto))
                EncodePackageDto(dto as PackageDto);
            else if (t == typeof(PlanogramFixtureItemDto))
                EncodePlanogramFixtureItemDto(dto as PlanogramFixtureItemDto);
            else if (t == typeof(PlanogramFixtureDto))
                EncodePlanogramFixtureDto(dto as PlanogramFixtureDto);
            else if (t == typeof(PlanogramFixtureItemDto))
                EncodePlanogramFixtureItemDto(dto as PlanogramFixtureItemDto);
            else if (t == typeof(PlanogramComponentDto))
                EncodePlanogramComponentDto(dto as PlanogramComponentDto);
            else if (t == typeof(PlanogramSubComponentDto))
                EncodePlanogramSubComponentDto(dto as PlanogramSubComponentDto);
            else if (t == typeof(PlanogramFixtureComponentDto))
                EncodePlanogramFixtureComponentDto(dto as PlanogramFixtureComponentDto);
            else if (t == typeof(PlanogramProductDto))
                EncodePlanogramProductDto(dto as PlanogramProductDto);
            //else if (typeof(T) == typeof(PlanogramAssemblyDto))
            //    EncodePlanogramAssemblyDto(dto as PlanogramAssemblyDto);
            
            //else if (typeof(T) == typeof(List<PlanogramComponentDto>))
            //    EncodePlanogramComponentDtosInSegment(dto as List<PlanogramComponentDto>);

            //else if (typeof(T) == typeof(List<PlanogramProductDto>))
            //    EncodePlanogramProductDtos(dto as List<PlanogramProductDto>);
            
            //else if (typeof(T) == typeof(List<PlanogramFixtureComponentDto>))
            //    EncodePlanogramFixtureComponentDtosInSegment(dto as List<PlanogramFixtureComponentDto>);
            
            //else if (typeof(T) == typeof(List<PlanogramSubComponentDto>))
            //    EncodePlanogramSubComponentDtosInSegment(dto as List<PlanogramSubComponentDto>);
            //else if (typeof(T) == typeof(CustomAttributeDataDto))
            //    EncodeCustomAttributeDataDto(dto as CustomAttributeDataDto);
            //else if (typeof(T) == typeof(List<CustomAttributeDataDto>))
            //    EncodeCustomAttributeDataDtos(dto as List<CustomAttributeDataDto>);
            //else if (typeof(T) == typeof(PlanogramAnnotationDto))
            //    EncodePlanogramAnnotationDto(dto as PlanogramAnnotationDto);
            //else if (typeof(T) == typeof(PlanogramPerformanceDto))
            //    EncodePlanogramPerformanceDto(dto as PlanogramPerformanceDto);
            //else if (typeof(T) == typeof(List<PlanogramPerformanceDataDto>))
            //    EncodePlanogramPerformanceDataDto(dto as List<PlanogramPerformanceDataDto>);
            //else if (typeof(T) == typeof(List<PlanogramPerformanceMetricDto>))
            //    EncodePlanogramPerformanceMetricDto(dto as List<PlanogramPerformanceMetricDto>);
            //else if (typeof(T) == typeof(PlanogramInventoryDto))
            //    EncodePlanogramInventoryDto(dto as PlanogramInventoryDto);


        }
        #region Encoding/Decoding

        /// <summary>
        ///     Encodes a dto into the file data
        /// </summary>
        private String Encode()
        {
            switch(this.CacheItemType)
            {
                case DalCacheItemType.Planogram:
                    EncodePlanogram();
                    break;
                case DalCacheItemType.Segment:
                    EncodePSASegment();
                    break;
                case DalCacheItemType.Fixture:
                    if (!EncodePSAFixture()) return String.Empty;
                    break;
                case DalCacheItemType.Position:
                    EncodePlanogramPosition();
                    break;
                case DalCacheItemType.Drawing:
                    EncodePlanogramAnnotation();
                    break;
                case DalCacheItemType.Performance:
                    EncodePlanogramPerformance();
                    break;
                default:
                    foreach (var dto in _dtos)
                    {
                        EncodeDto(dto.Value);
                    }
                    break;
            }

            return String.Join(",", _dataFields);
        }

        #region Package

        /// <summary>
        ///     Decodes a package dto.
        /// </summary>
        private PackageDto DecodePackageDto()
        {
            //  Check that this is invoked for a Project DAL cache item.
            if (!IsType(DalCacheItemType.Project))
            {
                Debug.Fail("DecodePackageDto should be invoked only for Project DAL cache items.");
                return null;
            }

            //  Check the version is the right one...
            if (!ProSpaceImportHelper.SupportedVersions.Any(version => String.Equals(version, _fileVersion)))
                AddEventLogEntry(String.Format(Message.Import_VersionMismatch_Description, _fileVersion),
                                 String.Format(Message.Import_VersionMismatch_Content, ProSpaceImportHelper.SupportedVersions.Last()),
                                 PlanogramEventLogEntryType.Information);

            //  Try fetching already existing data first.
            var packageDto = InstanceCache.Get<PackageDto>();
            if (packageDto != null) return packageDto;

            packageDto = new PackageDto
                         {
                             Id = Id,
                             Name = DecodePackageName()
                         };
            InstanceCache.Set(packageDto);
            return packageDto;
        }

        /// <summary>
        ///     Encodes a package dto
        /// </summary>
        private void EncodePackageDto(PackageDto dto)
        {
            InsertValue(ProSpaceImportHelper.ProjectName, dto.Name);
            InsertValue(ProSpaceImportHelper.ProjectPlanogramSpecificInventory, 1);

            if (this.Children.ContainsKey(DalCacheItemType.Planogram))
            {
                foreach (DalCacheItem planogram in this.Children[DalCacheItemType.Planogram])
                {
                    if (planogram.Dtos.ContainsKey(typeof(PlanogramDto)))
                    {
                        PlanogramDto planDto = planogram.Dtos[typeof(PlanogramDto)] as PlanogramDto;
                        if (planDto != null)
                        {
                            PlanogramLengthUnitOfMeasureType uom     = (PlanogramLengthUnitOfMeasureType)planDto.LengthUnitsOfMeasure;
                            switch (uom)
                            {
                                case PlanogramLengthUnitOfMeasureType.Centimeters:
                                    InsertValue(ProSpaceImportHelper.ProjectMeasurement, 1);
                                    break;
                                case PlanogramLengthUnitOfMeasureType.Inches:
                                    InsertValue(ProSpaceImportHelper.ProjectMeasurement, 0);
                                    break;
                                case PlanogramLengthUnitOfMeasureType.Unknown:
                                    break;
                            }
                            break;
                        }

                    }
                }
            }
        }

        private String DecodePackageName()
        {
            //  Check that this is invoked for a Project DAL cache item.
            if (!IsType(DalCacheItemType.Project))
            {
                Debug.Fail("DecodePackageName should be invoked only for Project DAL cache items.");
                return null;
            }

            //  Get the name source value from the Project data.
            var nameSourceValue = ExtractValue<String>(ProSpaceImportHelper.ProjectName);

            //  Validate the name source value.
            if (!String.IsNullOrEmpty(nameSourceValue)) return nameSourceValue;

            //  Get the name source value from the first planogram if there is one.
            //  Access the raw data and not the DTO as it needs the package DTO to create the Planogram DTO.
            DalCacheItem firstPlanogram = _children[DalCacheItemType.Planogram].FirstOrDefault();
            if (firstPlanogram != null)
                nameSourceValue = firstPlanogram.ExtractValue<String>(ProSpaceImportHelper.PlanogramName);

            //  Validate the name source value
            return !String.IsNullOrEmpty(nameSourceValue) ? nameSourceValue : Message.Planogram_DefaultPlanName;
        }

        #endregion

        #region Planogram

        /// <summary>
        ///     Decodes a planogram dto
        /// </summary>
        private PlanogramDto DecodePlanogramDto()
        {
            //  Check that this is invoked for a Planogram DAL cache item.
            if (!IsType(DalCacheItemType.Planogram))
            {
                Debug.Fail("DecodePlanogramDto should be invoked only for Planogram DAL cache items.");
                return null;
            }

            //  Try fetching already existing data first.
            var dto = InstanceCache.Get<PlanogramDto>();
            if (dto != null) return dto;

            var packageDto = _parent.GetDto<PackageDto>();
            dto =
                new PlanogramDto
                {
                    Id = Id,
                    PackageId = packageDto.Id,
                    UniqueContentReference = Guid.NewGuid().ToString(),
                    Name = DecodePlanogramName(),
                    Height = ExtractValue<Single>(ProSpaceImportHelper.PlanogramHeight),
                    Width = ExtractValue<Single>(ProSpaceImportHelper.PlanogramWidth),
                    Depth = ExtractValue<Single>(ProSpaceImportHelper.PlanogramDepth),
                    ProductPlacementX = (Byte) PlanogramProductPlacementXType.Manual,
                    ProductPlacementY = (Byte) PlanogramProductPlacementYType.Manual,
                    ProductPlacementZ = (Byte) PlanogramProductPlacementZType.Manual,
                    SourcePath = (String) packageDto.Id,
                    LengthUnitsOfMeasure = (Byte)ExtractedPlanogramLengthUnitOfMeasureType
                };

            ApplyMappings(dto, _dalCache.FieldMappings, PlanogramFieldMappingType.Planogram);
            _parent.ApplyMappings(dto, _dalCache.FieldMappings, PlanogramFieldMappingType.Planogram);

            if (!ValidateDto(dto)) return null;

            InstanceCache.Set(dto);
            return dto;
        }

        private String DecodePlanogramName()
        {
            //  Check that this is invoked for a Planogram DAL cache item.
            if (!IsType(DalCacheItemType.Planogram))
            {
                Debug.Fail("DecodePlanogramDto should be invoked only for Planogram DAL cache items.");
                return null;
            }

            var sourceValue = ExtractValue<String>(ProSpaceImportHelper.PlanogramName);
            if (String.IsNullOrWhiteSpace(sourceValue)) sourceValue = Message.Planogram_DefaultPlanName;
            return sourceValue;
        }

        /// <summary>
        ///     Encodes a planogram dto
        /// </summary>
        private void EncodePlanogram()
        {
            Object objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramDto), out objectDto);
            PlanogramDto dto = objectDto as PlanogramDto;

            objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramInventoryDto), out objectDto);
            PlanogramInventoryDto inventoryDto = objectDto as PlanogramInventoryDto;
            if (inventoryDto != null)
            {
                InsertValue(ProSpaceImportHelper.PlanogramCaseMultiple, inventoryDto.MinCasePacks);
                InsertValue(ProSpaceImportHelper.PlanogramDaysSupply, inventoryDto.MinDos);
                InsertValue(ProSpaceImportHelper.PlanogramInventoryDaysSupply, inventoryDto.IsDosValidated ? 1 : 0);
                InsertValue(ProSpaceImportHelper.PlanogramInventoryCaseMultiple, inventoryDto.IsCasePacksValidated ? 1 : 0);                
            }


            InsertValue(ProSpaceImportHelper.PlanogramName, dto.Name);
            InsertValue(ProSpaceImportHelper.PlanogramHeight, dto.Height);
            InsertValue(ProSpaceImportHelper.PlanogramWidth, dto.Width);
            InsertValue(ProSpaceImportHelper.PlanogramDepth, dto.Depth);

            try
            {
              List<DalCacheItem> segments =  this.Children[DalCacheItemType.Segment]
                     .OrderBy(s => (s.Dtos[typeof(PlanogramFixtureItemDto)] as PlanogramFixtureItemDto).BaySequenceNumber).ToList();
              Single calculatedWidth = segments.Sum(s => (s.Dtos[typeof(PlanogramFixtureDto)] as PlanogramFixtureDto).Width);

              

                //get the first segment fixture id to use to get the backboard and base
                Object firstFixtureId = this.Children[DalCacheItemType.Segment]
                     .OrderBy(s => (s.Dtos[typeof(PlanogramFixtureItemDto)] as PlanogramFixtureItemDto).BaySequenceNumber)
                     .Select(s => (s.Dtos[typeof(PlanogramFixtureItemDto)] as PlanogramFixtureItemDto).PlanogramFixtureId).First();

                PlanogramSubComponentDto baseDto = null;
                PlanogramSubComponentDto backBoardDto = null;
                DalCacheItem backBoardItem = this.Children[DalCacheItemType.Fixture].FirstOrDefault(f => f.IsBackBoard && (f.Dtos[typeof(PlanogramFixtureComponentDto)] as PlanogramFixtureComponentDto).PlanogramFixtureId.Equals(firstFixtureId));

                Single allbackboardWidths = this.Children[DalCacheItemType.Fixture].Where(f => f.IsBackBoard && f.Dtos.ContainsKey(typeof(PlanogramSubComponentDto)))
                                            .Sum(b => (b.Dtos[typeof(PlanogramSubComponentDto)] as PlanogramSubComponentDto).Width);
                Single allBaseWidths = this.Children[DalCacheItemType.Fixture].Where(f => f.IsBase && f.Dtos.ContainsKey(typeof(PlanogramSubComponentDto)))
                                        .Sum(b => (b.Dtos[typeof(PlanogramSubComponentDto)] as PlanogramSubComponentDto).Width);

                calculatedWidth = Math.Max(calculatedWidth, allbackboardWidths);
                calculatedWidth = Math.Max(calculatedWidth, allBaseWidths);
                InsertValue(ProSpaceImportHelper.PlanogramWidth, calculatedWidth);

                if (backBoardItem != null)
                {
                    objectDto = null;
                    backBoardItem.Dtos.TryGetValue(typeof(PlanogramSubComponentDto), out objectDto);
                    backBoardDto = objectDto as PlanogramSubComponentDto;
                    
                }
                DalCacheItem baseItem = this.Children[DalCacheItemType.Fixture].FirstOrDefault(f => f.IsBase && (f.Dtos[typeof(PlanogramFixtureComponentDto)] as PlanogramFixtureComponentDto).PlanogramFixtureId.Equals(firstFixtureId));
                if (baseItem != null)
                {
                    objectDto = null;
                    baseItem.Dtos.TryGetValue(typeof(PlanogramSubComponentDto), out objectDto);
                    baseDto = objectDto as PlanogramSubComponentDto;
                    
                }

                //Insert base details
                if (baseDto != null)
                {
                    InsertValue(ProSpaceImportHelper.PlanogramDrawBase, 1);
                    InsertValue(ProSpaceImportHelper.PlanogramBaseHeight, baseDto.Height);
                    //1 base for the whole plan so the width has to go all the way
                    InsertValue(ProSpaceImportHelper.PlanogramBaseWidth, calculatedWidth); 
                    InsertValue(ProSpaceImportHelper.PlanogramBaseDepth, baseDto.Depth);
                    InsertValue(ProSpaceImportHelper.PlanogramBaseColor, DecodeColour(baseDto.FillColourFront));
                }
                else
                {
                    InsertValue(ProSpaceImportHelper.PlanogramDrawBase, 0);
                }

                //Insert backboard details
                if (backBoardDto != null)
                {
                    InsertValue(ProSpaceImportHelper.PlanogramDrawBack, 1);
                    InsertValue(ProSpaceImportHelper.PlanogramBackDepth, backBoardDto.Depth);
                    InsertValue(ProSpaceImportHelper.PlanogramColor, DecodeColour(backBoardDto.FillColourFront));
                    InsertValue(ProSpaceImportHelper.PlanogramNotchSpacing, backBoardDto.NotchSpacingY);
                    InsertValue(ProSpaceImportHelper.PlanogramNotchWidth, backBoardDto.NotchWidth);
                    InsertValue(ProSpaceImportHelper.PlanogramNotchOffset, backBoardDto.NotchStartY);
                    InsertValue(ProSpaceImportHelper.PlanogramDrawNotches, backBoardDto.IsNotchPlacedOnFront ? 1 : 0);
                    InsertValue(ProSpaceImportHelper.PlanogramNotchColor, DecodeColour(backBoardDto.FillColourFront));
                    if (backBoardDto.NotchStyleType == (Byte)PlanogramSubComponentNotchStyleType.SingleRectangle)
                    {
                        InsertValue(ProSpaceImportHelper.PlanogramDoubleNotches, 0);
                    }
                    else
                    {
                        InsertValue(ProSpaceImportHelper.PlanogramDoubleNotches, 1);
                    }
                }
                else
                {
                    InsertValue(ProSpaceImportHelper.PlanogramDrawBack, 0);
                }
            }
            catch
            {
                InsertValue(ProSpaceImportHelper.PlanogramDrawBase, 0);
                InsertValue(ProSpaceImportHelper.PlanogramDrawBack, 0);
            }

            ApplyExportMappings(dto, dto.Id, this._dalCache.ExportMappingContext.PlanogramMappings, CustomAttributeDataPlanogramParentType.Planogram);
        }

        #endregion

        #region PlanogramFixtureItem

        private PlanogramFixtureItemDto DecodePlanogramFixtureItemDto()
        {
            //  Check that this is invoked for a Segment DAL cache item.
            if (!IsType(DalCacheItemType.Segment))
            {
                Debug.Fail("DecodePlanogramFixtureItemDto should be invoked only for Segment DAL cache items.");
                return null;
            }

            //  Try fetching already existing data first.
            var planogramFixtureItemDto = InstanceCache.Get<PlanogramFixtureItemDto>();
            if (planogramFixtureItemDto != null) return planogramFixtureItemDto;

            var planogramDto = _parent.GetDto<PlanogramDto>();
            var fixtureDto = GetDto<PlanogramFixtureDto>();
            List<DalCacheItem> segmentCacheItems = GetSegmentCacheItems();
            PointValue segmentCoordinates = DecodeSegmentCoordinates(segmentCacheItems);
            var angle = ExtractValue<Single>(ProSpaceImportHelper.SegmentAngle);
            planogramFixtureItemDto = new PlanogramFixtureItemDto
                                      {
                                          Id = Id,
                                          PlanogramId = planogramDto.Id,
                                          PlanogramFixtureId = fixtureDto.Id,
                                          X = segmentCoordinates.X,
                                          Y = segmentCoordinates.Y,
                                          Z = segmentCoordinates.Z,
                                          Angle = angle,
                                          Slope = 0,
                                          BaySequenceNumber = (Int16) (segmentCacheItems.IndexOf(this) + 1)
                                      };

            InstanceCache.Set(planogramFixtureItemDto);
            return planogramFixtureItemDto;
        }

        private void EncodePlanogramFixtureItemDto(PlanogramFixtureItemDto dto)
        {
            InsertValue(ProSpaceImportHelper.SegmentAngle, dto.Angle);
            InsertValue(ProSpaceImportHelper.SegmentX, dto.X);
            InsertValue(ProSpaceImportHelper.SegmentY, dto.Y);
            InsertValue(ProSpaceImportHelper.SegmentZ, dto.Z);
           
            ////  Check that this is invoked for a Segment DAL cache item.
            //if (!IsType(DalCacheItemType.Segment))
            //{
            //    Debug.Fail("DecodeSegmentCoordinates should be invoked only for Segment DAL cache items.");
            //    return default(PointValue);
            //}

            //Boolean has3DPlacementData = segmentCacheItems != null && segmentCacheItems.Any(item => item.Has3DPlacementData());
            //var x = ExtractValue<Single>(has3DPlacementData ? ProSpaceImportHelper.SegmentX : ProSpaceImportHelper.SegmentOffsetX);
            //var y = ExtractValue<Single>(has3DPlacementData ? ProSpaceImportHelper.SegmentY : ProSpaceImportHelper.SegmentOffsetY);
            //var z = ExtractValue<Single>(ProSpaceImportHelper.SegmentZ);
            //return new PointValue(x, y, z);
        }

        #endregion

        #region PlanogramFixture

        private void EncodePlanogramFixtureDto(PlanogramFixtureDto dto)
        {
            InsertValue(ProSpaceImportHelper.SegmentHeight, dto.Height);
            InsertValue(ProSpaceImportHelper.SegmentWidth, dto.Width);
            InsertValue(ProSpaceImportHelper.SegmentDepth, dto.Depth);
            InsertValue(ProSpaceImportHelper.SegmentName, dto.Name);

        }

        private PlanogramFixtureDto DecodePlanogramFixtureDto()
        {
            //  Check that this is invoked for a Segment DAL cache item.
            if (!IsType(DalCacheItemType.Segment))
            {
                Debug.Fail("DecodePlanogramFixtureDto should be invoked only for Segment DAL cache items.");
                return null;
            }

            //  Try fetching already existing data first.
            var dto = InstanceCache.Get<PlanogramFixtureDto>();
            if (dto != null) return dto;

            var planogramDto = _parent.GetDto<PlanogramDto>();
            //  Get the size values or default to the planogram.
            var height = ExtractValue<Single>(ProSpaceImportHelper.SegmentHeight);
            if (height.EqualTo(0)) height = planogramDto.Height;
            var width = ExtractValue<Single>(ProSpaceImportHelper.SegmentWidth);
            if (width.EqualTo(0)) width = planogramDto.Width;
            var depth = ExtractValue<Single>(ProSpaceImportHelper.SegmentDepth);
            if (depth.EqualTo(0)) depth = planogramDto.Depth;
            dto =
                new PlanogramFixtureDto
                {
                    Id = Id,
                    PlanogramId = planogramDto.Id,
                    Name = DecodePlanogramFixtureName(),
                    Height = height,
                    Width = width,
                    Depth = depth
                };

            ApplyMappings(dto, _dalCache.FieldMappings, PlanogramFieldMappingType.Fixture);

            if (!ValidateDto(dto)) return null;

            InstanceCache.Set(dto);
            return dto;
        }

        private String DecodePlanogramFixtureName()
        {
            //  Check that this is invoked for a Segment DAL cache item.
            if (!IsType(DalCacheItemType.Segment))
            {
                Debug.Fail("DecodePlanogramFixtureName should be invoked only for Segment DAL cache items.");
                return null;
            }

            var sourceValue = ExtractValue<String>(ProSpaceImportHelper.SegmentName);
            if (String.IsNullOrWhiteSpace(sourceValue)) sourceValue = Message.PlanogramFixture_Name_Default;
            return sourceValue;
        }

        #endregion

        #region PlanogramAssembly

        private PlanogramAssemblyDto DecodePlanogramAssemblyDto()
        {
            //  Try fetching already existing data first.
            var planogramDto = _parent.GetDto<PlanogramDto>();
            var dto = new PlanogramAssemblyDto
                      {
                          Id = Id,
                          PlanogramId = planogramDto.Id
                      };
            return dto;
        }

        #endregion

        #region PlanogramComponent

        private void EncodePlanogramComponentDto(PlanogramComponentDto dto)
        {
            InsertValue(ProSpaceImportHelper.FixtureName, dto.Name);
            InsertValue(ProSpaceImportHelper.FixtureHeight, dto.Height);
            InsertValue(ProSpaceImportHelper.FixtureWidth, dto.Width);
            InsertValue(ProSpaceImportHelper.FixtureDepth, dto.Depth);
            InsertValue(ProSpaceImportHelper.FixtureType, (Int32)ProSpaceComponentType.Shelf);
        }

        private PlanogramComponentDto DecodePlanogramComponentDto()
        {
            //  Check that this is invoked for a Fixture DAL cache item.
            if (!IsType(DalCacheItemType.Fixture))
            {
                Debug.Fail("DecodePlanogramComponentDto should be invoked only for Fixture DAL cache items.");
                return null;
            }

            //  Try fetching already existing data first.
            var dto = InstanceCache.Get<PlanogramComponentDto>();
            if (dto != null) return dto;

            var planogramDto = _parent.GetDto<PlanogramDto>();
            if (planogramDto == null) return null;
            var elementType = (ProSpaceComponentType)ExtractValue<Byte>(ProSpaceImportHelper.FixtureType);
            WidthHeightDepthValue componentSize = DecodeComponentSize(elementType, planogramDto);
            PlanogramComponentType componentType = elementType.ToPlanogramComponentType();
            dto =
                new PlanogramComponentDto
                {
                    Id = Id,
                    PlanogramId = planogramDto.Id,
                    Name = DecodePlanogramComponentName(),
                    Height = componentSize.Height,
                    Width = componentSize.Width,
                    Depth = componentSize.Depth,
                    ComponentType = (Byte) componentType,
                    IsMerchandisedTopDown = IsMerchandisedTopDown(elementType)
                };

            ApplyMappings(dto, _dalCache.FieldMappings, PlanogramFieldMappingType.Component);

            if (!ValidateDto(dto)) return null;

            InstanceCache.Set(dto);
            return dto;
        }

        private String DecodePlanogramComponentName()
        {
            //  Check that this is invoked for a Fixture DAL cache item.
            if (!IsType(DalCacheItemType.Fixture))
            {
                Debug.Fail("DecodePlanogramComponentDto should be invoked only for Fixture DAL cache items.");
                return null;
            }

            var sourceValue = ExtractValue<String>(ProSpaceImportHelper.FixtureName);
            if (String.IsNullOrWhiteSpace(sourceValue)) sourceValue = Message.PlanogramComponent_Name_Default;
            return sourceValue;
        }

        /// <summary>
        ///     Obtains the correct values for size in a component.
        /// </summary>
        /// <param name="elementType"></param>
        /// <param name="planogramDto">The planogram this component belongs to in case default values are needed.</param>
        /// <returns>The size (height, width, depth) of the component.</returns>
        private WidthHeightDepthValue DecodeComponentSize(ProSpaceComponentType elementType, PlanogramDto planogramDto)
        {
            //  Check that this is invoked for a Fixture DAL cache item.
            if (!IsType(DalCacheItemType.Fixture))
            {
                Debug.Fail("DecodeComponentSize should be invoked only for Fixture DAL cache items.");
                return default(WidthHeightDepthValue);
            }

            Single height = ExtractValue<Single>(ProSpaceImportHelper.FixtureHeight);
            if (elementType == ProSpaceComponentType.Bin ||
                elementType == ProSpaceComponentType.Chest) 
                height += ExtractValue<Single>(ProSpaceImportHelper.FixtureWallHeight);

            if (height.EqualTo(0)) height = 4;
            var width = ExtractValue<Single>(ProSpaceImportHelper.FixtureWidth);
            if (width.EqualTo(0)) width = planogramDto.Width;
            var depth = ExtractValue<Single>(ProSpaceImportHelper.FixtureDepth);
            if (depth.EqualTo(0)) depth = planogramDto.Depth;
            return new WidthHeightDepthValue(width, height, depth);
        }

        private static Boolean IsMerchandisedTopDown(ProSpaceComponentType elementType)
        {
            return elementType.ToPlanogramComponentType() == PlanogramComponentType.Chest;
        }

        private List<PlanogramComponentDto> DecodePlanogramComponentDtosInSegment()
        {
            //  Try fetching already existing data first.
            var planogramComponentDtos = InstanceCache.Get<List<PlanogramComponentDto>>();
            if (planogramComponentDtos != null) return planogramComponentDtos;

            planogramComponentDtos = new List<PlanogramComponentDto>();

            //  Check that this is invoked for a Segment DAL cache item.
            if (!IsType(DalCacheItemType.Segment))
            {
                Debug.Fail("DecodePlanogramComponentDtosInFixture should be invoked only for Segment DAL cache items.");
                return planogramComponentDtos;
            }

            Int16 baySequenceNumber = GetDto<PlanogramFixtureItemDto>().BaySequenceNumber;
            var planogram = _parent.GetDto<PlanogramDto>();
            var width = ExtractValue<Single>(ProSpaceImportHelper.SegmentWidth);
            //  Add a base if it is showing.
            if (_parent.ExtractValue<Boolean>(ProSpaceImportHelper.PlanogramDrawBase))
                planogramComponentDtos.Add(NewBasePlanogramComponentDto(planogram, baySequenceNumber, width));

            //  Add a backboard if it is showing.
            if (_parent.ExtractValue<Boolean>(ProSpaceImportHelper.PlanogramDrawBack))
                planogramComponentDtos.Add(NewBackboardPlanogramComponentDto(planogram, baySequenceNumber, width));

            InstanceCache.Set(planogramComponentDtos);
            return planogramComponentDtos;
        }

        private PlanogramComponentDto NewBackboardPlanogramComponentDto(PlanogramDto planogram, Int16 baySequenceNumber, Single width)
        {
            return new PlanogramComponentDto
                   {
                       Id = GetBackboardId(),
                       PlanogramId = planogram.Id,
                       Name = String.Format("Backboard{0}", baySequenceNumber),
                       Height = DecodeFixtureHeight(this, _parent),
                       Width = width,
                       Depth = DecodeBackDepth(this, _parent),
                       ComponentType = (Byte) PlanogramComponentType.Backboard
                   };
        }

        private PlanogramComponentDto NewBasePlanogramComponentDto(PlanogramDto planogram, Int16 baySequenceNumber, Single width)
        {
            return new PlanogramComponentDto
                   {
                       Id = GetBaseId(),
                       PlanogramId = planogram.Id,
                       Name = String.Format("Base{0}", baySequenceNumber),
                       Height = _parent.ExtractValue<Single>(ProSpaceImportHelper.PlanogramBaseHeight),
                       Width = width,
                       Depth = DecodeBaseDepth(this, _parent),
                       ComponentType = (Byte) PlanogramComponentType.Base
                   };
        }

        #endregion

        #region PlanogramPosition
        private WidthHeightDepthValue GetFacingSize(PlanogramPositionMerchandisingStyle merchStyle, PlanogramProductDto productDto)
        {
            switch (merchStyle)
            {
                case PlanogramPositionMerchandisingStyle.Alternate:
                    return new WidthHeightDepthValue(productDto.AlternateWidth, productDto.AlternateHeight, productDto.AlternateDepth);                    
                case PlanogramPositionMerchandisingStyle.Case:
                    return new WidthHeightDepthValue(productDto.CaseWidth, productDto.CaseHeight, productDto.CaseDepth);                    
                case PlanogramPositionMerchandisingStyle.Display:
                    return new WidthHeightDepthValue(productDto.DisplayWidth, productDto.DisplayHeight, productDto.DisplayDepth);                    
                case PlanogramPositionMerchandisingStyle.PointOfPurchase:
                    return new WidthHeightDepthValue(productDto.PointOfPurchaseWidth, productDto.PointOfPurchaseHeight, productDto.PointOfPurchaseDepth);                    
                case PlanogramPositionMerchandisingStyle.Tray:
                    return new WidthHeightDepthValue(productDto.TrayWidth, productDto.TrayHeight, productDto.TrayDepth);                   
                case PlanogramPositionMerchandisingStyle.Unit:
                case PlanogramPositionMerchandisingStyle.Default:
                default:
                    return GetFacingSize(productDto);
                    
            }
        }
        private WidthHeightDepthValue GetFacingSize(PlanogramProductDto productDto)
        {
            PlanogramProductMerchandisingStyle merchStyle = (PlanogramProductMerchandisingStyle)productDto.MerchandisingStyle;
            switch (merchStyle)
            {
                case PlanogramProductMerchandisingStyle.Alternate:
                    return new WidthHeightDepthValue(productDto.AlternateWidth, productDto.AlternateHeight, productDto.AlternateDepth);
                case PlanogramProductMerchandisingStyle.Case:
                    return new WidthHeightDepthValue(productDto.CaseWidth, productDto.CaseHeight, productDto.CaseDepth);
                case PlanogramProductMerchandisingStyle.Display:
                    return new WidthHeightDepthValue(productDto.DisplayWidth, productDto.DisplayHeight, productDto.DisplayDepth);
                case PlanogramProductMerchandisingStyle.PointOfPurchase:
                    return new WidthHeightDepthValue(productDto.PointOfPurchaseWidth, productDto.PointOfPurchaseHeight, productDto.PointOfPurchaseDepth);
                case PlanogramProductMerchandisingStyle.Tray:
                    return new WidthHeightDepthValue(productDto.TrayWidth, productDto.TrayHeight, productDto.TrayDepth);
                case PlanogramProductMerchandisingStyle.Unit:
                default:
                    return new WidthHeightDepthValue(productDto.Width, productDto.Height, productDto.Depth);

            }
        }


        private PlanogramPositionDto DecodePlanogramPositionDto()
        {
            //  Check that this is invoked for a Position DAL cache item.
            if (!IsType(DalCacheItemType.Position))
            {
                Debug.Fail("DecodePlanogramPositionDto should be invoked only for Position DAL cache items.");
                return null;
            }

            //  Try fetching already existing data first.
            var planogramPositionDto = InstanceCache.Get<PlanogramPositionDto>();
            if (planogramPositionDto != null) return planogramPositionDto;

            //  Create the planogram product dtos for each of the existing planograms.
            var planogramDto = _parent._parent.GetDto<PlanogramDto>();
            var fixtureComponentDto = _parent.GetDto<PlanogramFixtureComponentDto>();
            var subComponentDto = _parent.GetDto<PlanogramSubComponentDto>();
            PlanogramProductDto productDto = GetPlanogramProductDto(this);
            PointValue posCoordinates = DecodePositionCoordinates();
            RotationValue posRotation = DecodePositionRotation();
            WideHighDeepValue mainBlockFacings = DecodeMainBlockFacings();
            PlanogramPositionMerchandisingStyle mainMerchStyle = ((ProSpaceMerchandisingType)ExtractValue<Int32>(ProSpaceImportHelper.PositionMerchStyle)).ToPlanogramPositionMerchandisingStyle();
            PlanogramPositionMerchandisingStyle MerchandisingStyleX = ExtractValue<Boolean>(ProSpaceImportHelper.PositionMerchXIsTrayBroken) ? PlanogramPositionMerchandisingStyle.Unit : mainMerchStyle;
            PlanogramPositionMerchandisingStyle MerchandisingStyleY = ExtractValue<Boolean>(ProSpaceImportHelper.PositionMerchYIsTrayBroken) ? PlanogramPositionMerchandisingStyle.Unit : mainMerchStyle;
            PlanogramPositionMerchandisingStyle MerchandisingStyleZ = ExtractValue<Boolean>(ProSpaceImportHelper.PositionMerchZIsTrayBroken) ? PlanogramPositionMerchandisingStyle.Unit : mainMerchStyle;

            WidthHeightDepthValue normalFacingSize = GetFacingSize(mainMerchStyle, productDto);
            WidthHeightDepthValue xFacingSize = GetFacingSize(MerchandisingStyleX, productDto);
            WidthHeightDepthValue yFacingSize = GetFacingSize(MerchandisingStyleY, productDto);
            WidthHeightDepthValue zFacingSize = GetFacingSize(MerchandisingStyleZ, productDto);

            WideHighDeepValue xBlockFacings = DecodeXBlockFacings(normalFacingSize, xFacingSize, mainBlockFacings);
            WideHighDeepValue yBlockFacings = DecodeYBlockFacings(normalFacingSize, yFacingSize, mainBlockFacings);
            WideHighDeepValue zBlockFacings = DecodeZBlockFacings(normalFacingSize, zFacingSize, mainBlockFacings);
            
            var merchandisingStyle = (Byte)mainMerchStyle;
            planogramPositionDto =
                new PlanogramPositionDto
                {
                    Id = Id,
                    PlanogramId = planogramDto.Id,
                    PlanogramFixtureItemId = GetContainingSegmentCacheItem(_parent).Id,
                    PlanogramFixtureComponentId = fixtureComponentDto.Id,
                    PlanogramSubComponentId = subComponentDto.Id,
                    PlanogramProductId = productDto.Id,
                    X = posCoordinates.X,
                    Y = posCoordinates.Y,
                    Z = posCoordinates.Z,
                    Slope = posRotation.Slope,
                    Angle = posRotation.Angle,
                    Roll = posRotation.Roll,

                    OrientationType = (Byte)DecodePositionOrientationType(ProSpaceImportHelper.PositionOrientation),
                    FacingsWide = mainBlockFacings.Wide,
                    FacingsHigh = mainBlockFacings.High,
                    FacingsDeep = mainBlockFacings.Deep,

                    OrientationTypeX = (Byte)DecodePositionOrientationType(ProSpaceImportHelper.PositionXCapOrientation),
                    IsXPlacedLeft = ExtractValue<Boolean>(ProSpaceImportHelper.PositionXCapReversed),
                    FacingsXWide = xBlockFacings.Wide,
                    FacingsXHigh = xBlockFacings.High,
                    FacingsXDeep = xBlockFacings.Deep,

                    OrientationTypeY = (Byte)DecodePositionOrientationType(ProSpaceImportHelper.PositionYCapOrientation),
                    IsYPlacedBottom = ExtractValue<Boolean>(ProSpaceImportHelper.PositionYCapReversed),
                    FacingsYWide = yBlockFacings.Wide,
                    FacingsYHigh = yBlockFacings.High,
                    FacingsYDeep = yBlockFacings.Deep,

                    IsZPlacedFront = !ExtractValue<Boolean>(ProSpaceImportHelper.PositionZCapReversed),
                    OrientationTypeZ = (Byte)DecodePositionOrientationType(ProSpaceImportHelper.PositionZCapOrientation),
                    FacingsZWide = zBlockFacings.Wide,
                    FacingsZHigh = zBlockFacings.High,
                    FacingsZDeep = zBlockFacings.Deep,

                    MerchandisingStyle = merchandisingStyle,
                    MerchandisingStyleX = (Byte)MerchandisingStyleX,
                    MerchandisingStyleY = (Byte)MerchandisingStyleY,
                    MerchandisingStyleZ = (Byte)MerchandisingStyleZ,

                    PositionSequenceNumber = ExtractValue<Int16?>(ProSpaceImportHelper.PositionLocationId),
                    SequenceX = ExtractValue<Byte>(ProSpaceImportHelper.PositionRankX),
                    SequenceY = ExtractValue<Byte>(ProSpaceImportHelper.PositionRankY),
                    SequenceZ = ExtractValue<Byte>(ProSpaceImportHelper.PositionRankZ)
                };

            CalculatePegDepthForProduct(productDto, subComponentDto, mainBlockFacings);
            CheckPegCoordinatesForProduct(productDto, subComponentDto, (PlanogramLengthUnitOfMeasureType) planogramDto.LengthUnitsOfMeasure);

            //  Check Overrides for Product values
            CheckMaxStackOverride(productDto);
            CheckMaxDeepOverride(productDto);
            CheckMinDeepOverride(productDto);
            CheckMaxTopCapOverride(productDto);
            CheckMaxRightCapOverride(productDto);

            //  Update the product's nesting values, in case they should not be applied.
            if (ExtractValue<Boolean>(ProSpaceImportHelper.PositionXCapNested) && productDto.NestingWidth.GreaterThan(0) && ExtractValue<Int32>(ProSpaceImportHelper.PositionXCapNum) == 0)
                productDto.NestingWidth = 0;
            if (ExtractValue<Boolean>(ProSpaceImportHelper.PositionYCapNested) && productDto.NestingHeight.GreaterThan(0) && ExtractValue<Int32>(ProSpaceImportHelper.PositionYCapNum) == 0)
                productDto.NestingHeight = 0;
            if (ExtractValue<Boolean>(ProSpaceImportHelper.PositionZCapNested) && productDto.NestingDepth.GreaterThan(0) && ExtractValue<Int32>(ProSpaceImportHelper.PositionZCapNum) == 0)
                productDto.NestingDepth = 0;

            InstanceCache.Set(planogramPositionDto);
            return planogramPositionDto;
        }

        private void CheckPegCoordinatesForProduct(PlanogramProductDto productDto, PlanogramSubComponentDto subComponentDto, PlanogramLengthUnitOfMeasureType lengthUnitsOfMeasure)
        {
            //  Check that this is invoked for a Position DAL cache item.
            if (!IsType(DalCacheItemType.Position))
            {
                Debug.Fail("CheckPegCoordinatesForProduct should be invoked only for Position DAL cache items.");
                return;
            }

            if (subComponentDto.MerchandisingType == (Byte) PlanogramSubComponentMerchandisingType.Hang && productDto.PegY.EqualTo(0))
            {
                productDto.PegY = lengthUnitsOfMeasure == PlanogramLengthUnitOfMeasureType.Centimeters ? 0.63F : 0.25F;
            }
        }

        private void CalculatePegDepthForProduct(PlanogramProductDto productDto, PlanogramSubComponentDto subComponentDto, WideHighDeepValue mainBlockFacings)
        {
            // If the sub component is not hang or hang from bottom OR
            //  the product has no pegholes... just return.
            if (subComponentDto.MerchandisingType != (Byte) PlanogramSubComponentMerchandisingType.Hang &&
                subComponentDto.MerchandisingType != (Byte) PlanogramSubComponentMerchandisingType.HangFromBottom ||
                productDto.NumberOfPegHoles == 0) return;

            //  Make the peg as deep as needed for this position if it is not deep enough already.
            Single requiredPegDepth = GetOrientatedSize(mainBlockFacings, GetFrontFacingSize(productDto)).Depth;
            if (productDto.PegDepth.LessThan(requiredPegDepth))
                productDto.PegDepth = requiredPegDepth;
        }

        private void CheckMaxStackOverride(PlanogramProductDto productDto)
        {
            //  Check that this is invoked for a Position DAL cache item.
            if (!IsType(DalCacheItemType.Position))
            {
                Debug.Fail("CheckMaxStackOverride should be invoked only for Position DAL cache items.");
                return;
            }

            var overrideValue = ExtractValue<Int32>(ProSpaceImportHelper.PositionMerchYMax);
            if (overrideValue == -1) return;

            //  Ensure the override value is valid and
            //  check if the override value is "worse" than the product value (to avoid conflicts with other positions for the same product).
            if (overrideValue < Byte.MinValue ||
                overrideValue > Byte.MaxValue ||
                overrideValue < productDto.MaxStack) return;

            productDto.MaxStack = (Byte) overrideValue;
        }

        private void CheckMaxDeepOverride(PlanogramProductDto productDto)
        {
            //  Check that this is invoked for a Position DAL cache item.
            if (!IsType(DalCacheItemType.Position))
            {
                Debug.Fail("CheckMaxDeepOverride should be invoked only for Position DAL cache items.");
                return;
            }

            var overrideValue = ExtractValue<Int32>(ProSpaceImportHelper.PositionMerchZMax);
            if (overrideValue == -1) return;

            //  Ensure the override value is valid and
            //  check if the override value is "worse" than the product value (to avoid conflicts with other positions for the same product).
            if (overrideValue < Byte.MinValue ||
                overrideValue > Byte.MaxValue ||
                overrideValue < productDto.MaxDeep) return;

            productDto.MaxDeep = (Byte)overrideValue;
        }

        private void CheckMinDeepOverride(PlanogramProductDto productDto)
        {
            //  Check that this is invoked for a Position DAL cache item.
            if (!IsType(DalCacheItemType.Position))
            {
                Debug.Fail("CheckMinDeepOverride should be invoked only for Position DAL cache items.");
                return;
            }

            var overrideValue = ExtractValue<Int32>(ProSpaceImportHelper.PositionMerchZMin);
            if (overrideValue == -1) return;

            //  Ensure the override value is valid and
            //  check if the override value is "worse" than the product value (to avoid conflicts with other positions for the same product).
            if (overrideValue < Byte.MinValue ||
                overrideValue > Byte.MaxValue ||
                overrideValue > productDto.MinDeep) return;

            productDto.MinDeep = (Byte)overrideValue;
        }

        private void CheckMaxTopCapOverride(PlanogramProductDto productDto)
        {
            //  Check that this is invoked for a Position DAL cache item.
            if (!IsType(DalCacheItemType.Position))
            {
                Debug.Fail("CheckMaxTopCapOverride should be invoked only for Position DAL cache items.");
                return;
            }

            var overrideValue = ExtractValue<Int32>(ProSpaceImportHelper.PositionMerchYCaps);
            if (overrideValue == -1) return;

            //  Ensure the override value is valid and
            //  check if the override value is "worse" than the product value (to avoid conflicts with other positions for the same product).
            if (overrideValue < Byte.MinValue ||
                overrideValue > Byte.MaxValue ||
                overrideValue < productDto.MaxTopCap) return;

            productDto.MaxTopCap = (Byte)overrideValue;
        }

        private void CheckMaxRightCapOverride(PlanogramProductDto productDto)
        {
            //  Check that this is invoked for a Position DAL cache item.
            if (!IsType(DalCacheItemType.Position))
            {
                Debug.Fail("CheckMaxRightCapOverride should be invoked only for Position DAL cache items.");
                return;
            }

            var overrideValue = ExtractValue<Int32>(ProSpaceImportHelper.PositionMerchXCaps);
            if (overrideValue == -1) return;

            //  Ensure the override value is valid and
            //  check if the override value is "worse" than the product value (to avoid conflicts with other positions for the same product).
            if (overrideValue < Byte.MinValue ||
                overrideValue > Byte.MaxValue ||
                overrideValue < productDto.MaxRightCap) return;

            productDto.MaxRightCap = (Byte)overrideValue;
        }

        private WideHighDeepValue DecodeZBlockFacings(WidthHeightDepthValue normalFacingSize, WidthHeightDepthValue zBlockFacingSize, WideHighDeepValue mainBlockFacings)
        {
            //  Check that this is invoked for a Position DAL cache item.
            if (!IsType(DalCacheItemType.Position))
            {
                Debug.Fail("DecodeZBlockFacings should be invoked only for Position DAL cache items.");
                return new WideHighDeepValue();
            }

            var isNested = ExtractValue<Boolean>(ProSpaceImportHelper.PositionZCapNested);
            if (isNested) return new WideHighDeepValue();

            var capsZDeep = ExtractValue<Int16>(ProSpaceImportHelper.PositionZCapNum);
            if (capsZDeep == 0) { return new WideHighDeepValue(); }

            //  Calculate main block size.
            PlanogramProductOrientationType productOrientationType =
                DecodePositionOrientationType(ProSpaceImportHelper.PositionOrientation).ToProductOrientationType();
            PlanogramProductOrientationType productOrientationTypeZ =
                DecodePositionOrientationType(ProSpaceImportHelper.PositionZCapOrientation).ToProductOrientationType();

            WidthHeightDepthValue orientatedSize = ProductOrientationHelper.GetOrientatedSize(productOrientationType, normalFacingSize).MultiplyBy(mainBlockFacings);
            WideHighDeepValue orientatedUnitsZ = orientatedSize.DivideBy(ProductOrientationHelper.GetOrientatedSize(productOrientationTypeZ, zBlockFacingSize));
            return new WideHighDeepValue(orientatedUnitsZ.Wide, orientatedUnitsZ.High, capsZDeep);
        }

        private WideHighDeepValue DecodeYBlockFacings(WidthHeightDepthValue normalFacingSize, WidthHeightDepthValue yBlockFacingSize, WideHighDeepValue mainBlockFacings)
        {
            //  Check that this is invoked for a Position DAL cache item.
            if (!IsType(DalCacheItemType.Position))
            {
                Debug.Fail("DecodeYBlockFacings should be invoked only for Position DAL cache items.");
                return new WideHighDeepValue();
            }

            var isNested = ExtractValue<Boolean>(ProSpaceImportHelper.PositionYCapNested);
            if (isNested) return new WideHighDeepValue();

            var capsYHigh = ExtractValue<Int16>(ProSpaceImportHelper.PositionYCapNum);
            if (capsYHigh == 0) {return new WideHighDeepValue();}

            //  Calculate main block size.
            WidthHeightDepthValue orientatedSize = GetOrientatedSize(mainBlockFacings, normalFacingSize);
            PlanogramProductOrientationType productOrientationTypeY =
                DecodePositionOrientationType(ProSpaceImportHelper.PositionYCapOrientation).ToProductOrientationType();

            WideHighDeepValue orientatedUnitsY = orientatedSize.DivideBy(ProductOrientationHelper.GetOrientatedSize(productOrientationTypeY, yBlockFacingSize));
            return new WideHighDeepValue(orientatedUnitsY.Wide, capsYHigh, orientatedUnitsY.Deep);
        }

        private WidthHeightDepthValue GetOrientatedSize(WideHighDeepValue mainBlockFacings, WidthHeightDepthValue normalFacingSize)
        {
            PlanogramProductOrientationType productOrientationType =
                DecodePositionOrientationType(ProSpaceImportHelper.PositionOrientation).ToProductOrientationType();
            WidthHeightDepthValue orientatedSize = ProductOrientationHelper.GetOrientatedSize(productOrientationType, normalFacingSize).MultiplyBy(mainBlockFacings);
            return orientatedSize;
        }

        private static WidthHeightDepthValue GetFrontFacingSize(PlanogramProductDto productDto)
        {
            return new WidthHeightDepthValue(productDto.Width, productDto.Height, productDto.Depth);
        }

        private WideHighDeepValue DecodeXBlockFacings(WidthHeightDepthValue normalFacingSize, WidthHeightDepthValue xBlockFacingSize, WideHighDeepValue mainBlockFacings)
        {
            //  Check that this is invoked for a Position DAL cache item.
            if (!IsType(DalCacheItemType.Position))
            {
                Debug.Fail("DecodeXBlockFacings should be invoked only for Position DAL cache items.");
                return new WideHighDeepValue();
            }

            var isNested = ExtractValue<Boolean>(ProSpaceImportHelper.PositionXCapNested);
            if (isNested) return new WideHighDeepValue();

            var capsXWide = ExtractValue<Int16>(ProSpaceImportHelper.PositionXCapNum);
            if (capsXWide == 0) { return new WideHighDeepValue(); }

            //  Calculate main block size.
            PlanogramProductOrientationType productOrientationType =
                DecodePositionOrientationType(ProSpaceImportHelper.PositionOrientation).ToProductOrientationType();
            PlanogramProductOrientationType productOrientationTypeX =
                DecodePositionOrientationType(ProSpaceImportHelper.PositionXCapOrientation).ToProductOrientationType();

            WidthHeightDepthValue orientatedSize = ProductOrientationHelper.GetOrientatedSize(productOrientationType, normalFacingSize).MultiplyBy(mainBlockFacings);
            WideHighDeepValue orientatedUnitsX = orientatedSize.DivideBy(ProductOrientationHelper.GetOrientatedSize(productOrientationTypeX, xBlockFacingSize));
            return new WideHighDeepValue(capsXWide, orientatedUnitsX.High, orientatedUnitsX.Deep);
        }

        private WideHighDeepValue DecodeMainBlockFacings()
        {
            //  Check that this is invoked for a Position DAL cache item.
            if (!IsType(DalCacheItemType.Position))
            {
                Debug.Fail("DecodeMainBlockFacings should be invoked only for Position DAL cache items.");
                return new WideHighDeepValue();
            }

            var wide = ExtractValue<Int16>(ProSpaceImportHelper.PositionHFacings);
            var isNestedX = ExtractValue<Boolean>(ProSpaceImportHelper.PositionXCapNested);
            if (isNestedX) wide += ExtractValue<Int16>(ProSpaceImportHelper.PositionXCapNum);

            var high = ExtractValue<Int16>(ProSpaceImportHelper.PositionVFacings);
            var isNestedY = ExtractValue<Boolean>(ProSpaceImportHelper.PositionYCapNested);
            if (isNestedY) high += ExtractValue<Int16>(ProSpaceImportHelper.PositionYCapNum);

            var deep = ExtractValue<Int16>(ProSpaceImportHelper.PositionDFacings);
            var isNestedZ = ExtractValue<Boolean>(ProSpaceImportHelper.PositionZCapNested);
            if (isNestedZ) deep += ExtractValue<Int16>(ProSpaceImportHelper.PositionZCapNum);

            return new WideHighDeepValue(wide, high, deep);
        }

        /// <summary>
        ///     Decode this instance's data and obtain the matching <see cref="PlanogramPositionOrientationType"/>.
        /// </summary>
        /// <param name="fieldIndex"></param>
        /// <returns>The value corresponding to this instance's data, OR Default if unable to decode it.</returns>
        private PlanogramPositionOrientationType DecodePositionOrientationType(String fieldIndex)
        {
            var match = PlanogramPositionOrientationType.Default;

            //  Check that this is invoked for a Position DAL cache item.
            if (!IsType(DalCacheItemType.Position))
            {
                Debug.Fail("DecodePositionOrientantionType should be invoked only for Position DAL cache items.");
                return match;
            }

            var source = ExtractValue<Int32>(fieldIndex);
            if (source < 0) return match;

            switch ((ProSpaceOrientationType)source)
            {
                case ProSpaceOrientationType.Front:
                    match = PlanogramPositionOrientationType.Front0;
                    break;
                case ProSpaceOrientationType.Front90:
                    match = PlanogramPositionOrientationType.Front90;
                    break;
                case ProSpaceOrientationType.Side:
                    match = PlanogramPositionOrientationType.Left0;
                    break;
                case ProSpaceOrientationType.Side90:
                    match = PlanogramPositionOrientationType.Left90;
                    break;
                case ProSpaceOrientationType.Top:
                    match = PlanogramPositionOrientationType.Top0;
                    break;
                case ProSpaceOrientationType.Top90:
                    match = PlanogramPositionOrientationType.Top90;
                    break;
                case ProSpaceOrientationType.Back:
                    match = PlanogramPositionOrientationType.Back0;
                    break;
                case ProSpaceOrientationType.Back90:
                    match = PlanogramPositionOrientationType.Back90;
                    break;
                case ProSpaceOrientationType.Right:
                    match = PlanogramPositionOrientationType.Right0;
                    break;
                case ProSpaceOrientationType.Right90:
                    match = PlanogramPositionOrientationType.Right90;
                    break;
                case ProSpaceOrientationType.Base:
                    match = PlanogramPositionOrientationType.Bottom0;
                    break;
                case ProSpaceOrientationType.Base90:
                    match = PlanogramPositionOrientationType.Bottom90;
                    break;
                case ProSpaceOrientationType.Front180:
                    match = PlanogramPositionOrientationType.Front180;
                    break;
                case ProSpaceOrientationType.Front270:
                    match = PlanogramPositionOrientationType.Front270;
                    break;
                case ProSpaceOrientationType.Side180:
                    match = PlanogramPositionOrientationType.Left180;
                    break;
                case ProSpaceOrientationType.Side270:
                    match = PlanogramPositionOrientationType.Left270;
                    break;
                case ProSpaceOrientationType.Top180:
                    match = PlanogramPositionOrientationType.Top180;
                    break;
                case ProSpaceOrientationType.Top270:
                    match = PlanogramPositionOrientationType.Top270;
                    break;
                case ProSpaceOrientationType.Back180:
                    match = PlanogramPositionOrientationType.Back180;
                    break;
                case ProSpaceOrientationType.Back270:
                    match = PlanogramPositionOrientationType.Back270;
                    break;
                case ProSpaceOrientationType.Right180:
                    match = PlanogramPositionOrientationType.Right180;
                    break;
                case ProSpaceOrientationType.Right270:
                    match = PlanogramPositionOrientationType.Right270;
                    break;
                case ProSpaceOrientationType.Base180:
                    match = PlanogramPositionOrientationType.Bottom180;
                    break;
                case ProSpaceOrientationType.Base270:
                    match = PlanogramPositionOrientationType.Bottom270;
                    break;
                default:
                    Debug.Fail("Unknown value when calling DecodePositionOrientantionType.");
                    break;
            }

            return match;
        }

        private RotationValue DecodePositionRotation()
        {
            //  Check that this is invoked for a Position DAL cache item.
            if (!IsType(DalCacheItemType.Position))
            {
                Debug.Fail("DecodePositionRotation should be invoked only for Position DAL cache items.");
                return default(RotationValue);
            }

            Single angle = SpacePlanningImportHelper.ConvertToRadians(ExtractValue<Single>(ProSpaceImportHelper.PositionAngle));
            Single slope = SpacePlanningImportHelper.ConvertToRadians(ExtractValue<Single>(ProSpaceImportHelper.PositionSlope));
            Single roll = SpacePlanningImportHelper.ConvertToRadians(ExtractValue<Single>(ProSpaceImportHelper.PositionRoll));

            DalCacheItem fixtureCacheItem = _parent;
            if (fixtureCacheItem == null) return new RotationValue(angle, slope, roll);

            //  Make the rotation relative to the Component this position is on.
            RotationValue segmentRotation = fixtureCacheItem.DecodeFixtureRotation();
            angle -= segmentRotation.Angle;
            slope -= segmentRotation.Slope;
            roll -= segmentRotation.Roll;
            return new RotationValue(angle, slope, roll);
        }

        private RotationValue DecodeFixtureRotation()
        {
            //  Check that this is invoked for a Fixture DAL cache item.
            if (!IsType(DalCacheItemType.Fixture))
            {
                Debug.Fail("DecodeFixtureRotation should be invoked only for Fixture DAL cache items.");
                return default(RotationValue);
            }

            Single angle = SpacePlanningImportHelper.ConvertToRadians(ExtractValue<Single>(ProSpaceImportHelper.FixtureAngle));
            Single slope = SpacePlanningImportHelper.ConvertToRadians(ExtractValue<Single>(ProSpaceImportHelper.FixtureSlope));
            Single roll = SpacePlanningImportHelper.ConvertToRadians(ExtractValue<Single>(ProSpaceImportHelper.FixtureRoll));

            DalCacheItem segmentCacheItem = GetContainingSegmentCacheItem(this);
            if (segmentCacheItem == null) return new RotationValue(angle, slope, roll);

            //  Make the rotation relative to the Fixture this position is on.
            RotationValue segmentRotation = segmentCacheItem.DecodeSegmentRotation();
            angle -= segmentRotation.Angle;
            slope -= segmentRotation.Slope;
            roll -= segmentRotation.Roll;
            return new RotationValue(angle, slope, roll);
        }

        private RotationValue DecodeSegmentRotation()
        {
            //  Check that this is invoked for a Position DAL cache item.
            if (!IsType(DalCacheItemType.Segment))
            {
                Debug.Fail("DecodeSegmentRotation should be invoked only for Segment DAL cache items.");
                return default(RotationValue);
            }

            Boolean has3DPlacementData = GetSegmentCacheItems().Any(item => item.Has3DPlacementData());
            Single angle = SpacePlanningImportHelper.ConvertToRadians(ExtractValue<Single>(ProSpaceImportHelper.SegmentAngle));
            var slope = 0.0F;
            var roll = 0.0F;

            return new RotationValue(angle, slope, roll);
        }

        private PointValue DecodePositionCoordinates()
        {
            //  Check that this is invoked for a Position DAL cache item.
            if (!IsType(DalCacheItemType.Position))
            {
                Debug.Fail("DecodePositionCoordinates should be invoked only for Position DAL cache items.");
                return default(PointValue);
            }

            var x = ExtractValue<Single>(ProSpaceImportHelper.PositionX);
            var y = ExtractValue<Single>(ProSpaceImportHelper.PositionY);
            var z = ExtractValue<Single>(ProSpaceImportHelper.PositionZ);

            DalCacheItem fixtureCacheItem = _parent;
            if (fixtureCacheItem == null) return new PointValue(x, y, z);

            //  Make the coordinates relative to the Component this position is on.
            x -= fixtureCacheItem.ExtractValue<Single>(ProSpaceImportHelper.FixtureX);
            y -= fixtureCacheItem.ExtractValue<Single>(ProSpaceImportHelper.FixtureY);
            z -= fixtureCacheItem.ExtractValue<Single>(ProSpaceImportHelper.FixtureZ);
            return new PointValue(x, y, z);
        }

        private PointValue DecodeFixtureCoordinates()
        {
            //  Check that this is invoked for a Fixture DAL cache item.
            if (!IsType(DalCacheItemType.Fixture))
            {
                Debug.Fail("DecodeFixtureCoordinates should be invoked only for Fixture DAL cache items.");
                return default(PointValue);
            }

            var x = ExtractValue<Single>(ProSpaceImportHelper.FixtureX);
            var y = ExtractValue<Single>(ProSpaceImportHelper.FixtureY);
            var z = ExtractValue<Single>(ProSpaceImportHelper.FixtureZ);

            DalCacheItem segmentCacheItem = GetContainingSegmentCacheItem(this);
            if (segmentCacheItem == null) return new PointValue(x, y, z);

            PointValue segmentCoordinates = segmentCacheItem.DecodeSegmentCoordinates();
            //  Make the coordinates relative to the Fixture this position is on.
            x -= segmentCoordinates.X;
            y -= segmentCoordinates.Y;
            z -= segmentCoordinates.Z;
            return new PointValue(x, y, z);
        }

        private PointValue DecodeAnnotationCoordinates()
        {
            //  Check that this is invoked for a Drawing DAL cache item.
            if (!IsType(DalCacheItemType.Drawing))
            {
                Debug.Fail("DecodeAnnotationCoordinates should be invoked only for Drawing DAL cache items.");
                return default(PointValue);
            }

            var x = ExtractValue<Single>(ProSpaceImportHelper.DrawingX);
            //  NB: Y Coordinate refers to the top of the annotation in ProSpace, so it needs to be lowered by the height of the drawing.
            //      Unless the drawing height is positive then its like CCM
            Single drawingHeight = ExtractValue<Single>(ProSpaceImportHelper.DrawingHeight);
            Single y = ExtractValue<Single>(ProSpaceImportHelper.DrawingY) + (drawingHeight > 0 ? 0 : drawingHeight);
            //  NB: Z coordinates are tricky as they may be 0 and in JDA they are still rendered topmost, 
            //  just move it to the fron of the fixture.
            var z = 0.0F; //ExtractValue<Single>(ProSpaceImportHelper.DrawingZ);

            DalCacheItem segmentCacheItem = GetContainingSegmentCacheItem(this);
            if (segmentCacheItem == null) return new PointValue(x, y, z);

            PointValue segmentCoordinates = segmentCacheItem.DecodeSegmentCoordinates();
            //  Make the coordinates relative to the Fixture this position is on.
            x -= segmentCoordinates.X;
            y -= segmentCoordinates.Y;
            //x -= segmentCacheItem.ExtractValue<Single>(ProSpaceImportHelper.SegmentX) + segmentCacheItem.ExtractValue<Single>(ProSpaceImportHelper.SegmentOffsetX);
            //y -= segmentCacheItem.ExtractValue<Single>(ProSpaceImportHelper.SegmentY) + segmentCacheItem.ExtractValue<Single>(ProSpaceImportHelper.SegmentOffsetY);

            //  Move the Z coordinate to the front of the planogram.
            DalCacheItem planogramCacheItem = GetParentCacheItemOfType(DalCacheItemType.Planogram);
            if (planogramCacheItem != null) z += planogramCacheItem.ExtractValue<Single>(ProSpaceImportHelper.PlanogramDepth);
            return new PointValue(x, y, z);
        }

        private PointValue DecodeSegmentCoordinates(IEnumerable<DalCacheItem> segmentCacheItems = null)
        {
            //  Check that this is invoked for a Segment DAL cache item.
            if (!IsType(DalCacheItemType.Segment))
            {
                Debug.Fail("DecodeSegmentCoordinates should be invoked only for Segment DAL cache items.");
                return default(PointValue);
            }

            Boolean has3DPlacementData = segmentCacheItems != null && segmentCacheItems.Any(item => item.Has3DPlacementData());
            var x = ExtractValue<Single>(has3DPlacementData ? ProSpaceImportHelper.SegmentX : ProSpaceImportHelper.SegmentOffsetX);
            var y = ExtractValue<Single>(has3DPlacementData ? ProSpaceImportHelper.SegmentY : ProSpaceImportHelper.SegmentOffsetY);
            var z = ExtractValue<Single>(ProSpaceImportHelper.SegmentZ);
            return new PointValue(x, y, z);
        }

        private PlanogramProductDto GetPlanogramProductDto(DalCacheItem positionCacheItem)
        {
            //  Check that this is invoked for a Position DAL cache item.
            if (!IsType(DalCacheItemType.Position))
            {
                Debug.Fail("GetPlanogramProductDto should be invoked only for Position DAL cache items.");
                return null;
            }

            var planogramDto = positionCacheItem._parent._parent.GetDto<PlanogramDto>();

            //  Find the matching Product Cache Item.
            PlanogramProductDto match = null;
            List<PlanogramProductDto> matches;
            PlanogramProductDtoById.TryGetValue(NewPositionProductKey(positionCacheItem), out matches);
            if (matches != null) match = matches.FirstOrDefault(dto => Equals(dto.PlanogramId, planogramDto.Id));
            if (match != null) return match;

            Debug.Fail("GetPlanogramProductDto could not find the product matching this position.");
            return null;
        }

        #endregion

        #region PlanogramProduct

        private List<PlanogramProductDto> DecodePlanogramProductDtos()
        {
            //  Check that this is invoked for a Product DAL cache item.
            if (!IsType(DalCacheItemType.Product))
            {
                Debug.Fail("DecodePlanogramProductDtos should be invoked only for Product DAL cache items.");
                return new List<PlanogramProductDto>();
            }

            //  Try fetching already existing data first.
            var planogramProductDtos = InstanceCache.Get<List<PlanogramProductDto>>();
            if (planogramProductDtos != null) return planogramProductDtos;

            //  Create the planogram product dtos for each of the existing planograms.
            List<DalCacheItem> planogramCacheItems = _parent.Children[DalCacheItemType.Planogram];
            planogramProductDtos = planogramCacheItems.Select(CreatePlanogramProductDto).ToList();

            InstanceCache.Set(planogramProductDtos);
            return planogramProductDtos;
        }

        private PlanogramProductDto CreatePlanogramProductDto(DalCacheItem item)
        {
            //  Check that this is invoked for a Product DAL cache item.
            if (!IsType(DalCacheItemType.Product))
            {
                Debug.Fail("CreatePlanogramProductDto should be invoked only for Product DAL cache items.");
                return null;
            }

            //  Try getting the planogram relevant copy of the product.
            String planogramId = item.Id;
            PlanogramProductDto dto = null;
            var dtos = InstanceCache.Get<List<PlanogramProductDto>>();
            if (dtos == null)
            {
                dtos = new List<PlanogramProductDto>();
                InstanceCache.Set(dtos);
            }
            dto = dtos.FirstOrDefault(productDto => Equals(productDto.PlanogramId, planogramId));
            if (dto != null) return dto;

            //  Try fetching the template product and cloning it.
            dto = InstanceCache.Get<PlanogramProductDto>();
            if (dto != null)
            {
                PlanogramProductDto cloneDto = ClonePlanogramProductDto(dto, planogramId);
                List<String> productCloneGtins;
                if (!_dalCache.AssignedProductGtinsByPlanogramId.TryGetValue(planogramId, out productCloneGtins))
                {
                    productCloneGtins = new List<String>();
                    _dalCache.AssignedProductGtinsByPlanogramId[planogramId] = productCloneGtins;
                }
                SpacePlanningImportHelper.ValidatePlanogramProductBusinessRules(_dalCache.PlanogramEventLogDtos, cloneDto, productCloneGtins);
                return cloneDto;
            }

            //  Create the template product and return it.
            WidthHeightDepthValue nestingSize = DecodeNestingSize();
            PlanogramProductMerchandisingStyle merchandisingStyle = DecodeProductMerchandisingStyle((ProSpaceMerchandisingType) ExtractValue<Int32>(ProSpaceImportHelper.ProductDefaultMerchStyle));
            dto = new PlanogramProductDto
                  {
                      Id = Id,
                      PlanogramId = planogramId,
                      Gtin = ExtractValue<String>(ProSpaceImportHelper.ProductUpc),
                      Name = ExtractValue<String>(ProSpaceImportHelper.ProductName),

                      Height = ExtractValue<Single>(ProSpaceImportHelper.ProductHeight),
                      Width = ExtractValue<Single>(ProSpaceImportHelper.ProductWidth),
                      Depth = ExtractValue<Single>(ProSpaceImportHelper.ProductDepth),

                      DisplayHeight = ExtractValue<Single>(ProSpaceImportHelper.ProductDisplayHeight),
                      DisplayWidth = ExtractValue<Single>(ProSpaceImportHelper.ProductDisplayWidth),
                      DisplayDepth = ExtractValue<Single>(ProSpaceImportHelper.ProductDisplayDepth),

                      TrayHeight = ExtractValue<Single>(ProSpaceImportHelper.ProductTrayHeight),
                      TrayWidth = ExtractValue<Single>(ProSpaceImportHelper.ProductTrayWidth),
                      TrayDepth = ExtractValue<Single>(ProSpaceImportHelper.ProductTrayDepth),
                      TrayHigh = ExtractValue<Byte>(ProSpaceImportHelper.ProductTrayNumberHigh),
                      TrayWide = ExtractValue<Byte>(ProSpaceImportHelper.ProductTrayNumberWide),
                      TrayDeep = ExtractValue<Byte>(ProSpaceImportHelper.ProductTrayNumberDeep),
                      TrayPackUnits = ExtractValue<Byte>(ProSpaceImportHelper.ProductTrayTotalNumber),

                      CaseHeight = ExtractValue<Single>(ProSpaceImportHelper.ProductCaseHeight),
                      CaseWidth = ExtractValue<Single>(ProSpaceImportHelper.ProductCaseWidth),
                      CaseDepth = ExtractValue<Single>(ProSpaceImportHelper.ProductCaseDepth),
                      CaseHigh = ExtractValue<Byte>(ProSpaceImportHelper.ProductCaseNumberHigh),
                      CaseWide = ExtractValue<Byte>(ProSpaceImportHelper.ProductCaseNumberWide),
                      CaseDeep = ExtractValue<Byte>(ProSpaceImportHelper.ProductCaseNumberDeep),
                      CasePackUnits = ExtractValue<Byte>(ProSpaceImportHelper.ProductCaseTotalNumber),

                      AlternateHeight = ExtractValue<Single>(ProSpaceImportHelper.ProductAlternateHeight),
                      AlternateWidth = ExtractValue<Single>(ProSpaceImportHelper.ProductAlternateWidth),
                      AlternateDepth = ExtractValue<Single>(ProSpaceImportHelper.ProductAlternateDepth),

                      NumberOfPegHoles = ExtractValue<Byte>(ProSpaceImportHelper.ProductPegholes),
                      PegX = ExtractValue<Single>(ProSpaceImportHelper.ProductPegholeX),
                      PegX2 = ExtractValue<Single>(ProSpaceImportHelper.ProductPeghole2X),
                      PegX3 = ExtractValue<Single>(ProSpaceImportHelper.ProductPeghole3X),
                      PegY = ExtractValue<Single>(ProSpaceImportHelper.ProductPegholeY),
                      PegY2 = ExtractValue<Single>(ProSpaceImportHelper.ProductPeghole2Y),
                      PegY3 = ExtractValue<Single>(ProSpaceImportHelper.ProductPeghole3Y),

                      SqueezeHeight = ExtractValue<Single>(ProSpaceImportHelper.ProductMinimumSqueezeFactorY),
                      SqueezeWidth = ExtractValue<Single>(ProSpaceImportHelper.ProductMinimumSqueezeFactorX),
                      SqueezeDepth = ExtractValue<Single>(ProSpaceImportHelper.ProductMinimumSqueezeFactorZ),

                      NestingHeight = nestingSize.Height,
                      NestingWidth = nestingSize.Width,
                      NestingDepth = nestingSize.Depth,

                      MaxStack = DecodeMaxStack(item),
                      MaxDeep = DecodeMaxDeep(item),
                      MinDeep = DecodeMinDeep(item),
                      MaxTopCap = DecodeMaxTopCap(item),
                      MaxRightCap = DecodeMaxRightCap(item),

                      StatusType = (Byte) PlanogramProductStatusType.Active,
                      MerchandisingStyle = (Byte) merchandisingStyle,
                      StyleNumber = ExtractValue<Int16>(ProSpaceImportHelper.ProductPackageStyle),
                      ShapeType = (Byte) DecodeShapeType((ProSpaceShapeType) ExtractValue<Int32>(ProSpaceImportHelper.ProductPackageStyle)),
                      FillPatternType = DecodeFillPatternType((ProSpaceFillPatternType) ExtractValue<Int32>(ProSpaceImportHelper.ProductFillPattern)),
                      FillColour = SpacePlanningImportHelper.DecodeColour(ExtractValue<Single>(ProSpaceImportHelper.ProductColor)),
                  };

            SpacePlanningImportHelper.SetDefaultCanBreakTrayValues(dto);
            SpacePlanningImportHelper.DecodeTrayThickness(dto);
            ApplyMappings(dto, _dalCache.FieldMappings, PlanogramFieldMappingType.Product);
            List<String> productGtins;
            if (!_dalCache.AssignedProductGtinsByPlanogramId.TryGetValue(planogramId, out productGtins))
            {
                productGtins = new List<String>();
                _dalCache.AssignedProductGtinsByPlanogramId[planogramId] = productGtins;
            }
            SpacePlanningImportHelper.ValidatePlanogramProductBusinessRules(_dalCache.PlanogramEventLogDtos, dto, productGtins);

            InstanceCache.Set(dto);
            return dto;
        }

        private static PlanogramProductDto ClonePlanogramProductDto(PlanogramProductDto dto, Object planogramId)
        {
            PlanogramProductDto planogramProductDto = SpacePlanningImportHelper.Clone(dto);
            planogramProductDto.PlanogramId = planogramId;
            return planogramProductDto;
        }

        private static PlanogramProductShapeType DecodeShapeType(ProSpaceShapeType value)
        {
            switch (value)
            {
                case ProSpaceShapeType.Box:
                case ProSpaceShapeType.Jar:
                case ProSpaceShapeType.Can:
                case ProSpaceShapeType.Roll:
                case ProSpaceShapeType.Loose:
                case ProSpaceShapeType.Holed:
                case ProSpaceShapeType.Hairpin:
                case ProSpaceShapeType.Clothing:
                    return PlanogramProductShapeType.Box;
                case ProSpaceShapeType.Bottle:
                    return PlanogramProductShapeType.Bottle;
                default:
                    Debug.Fail("Unknown ProSpaceShapeType value when calling DecodeShapeType.");
                    return PlanogramProductShapeType.Box;
            }
        }

        private WidthHeightDepthValue DecodeNestingSize()
        {
            //  Check that this is invoked for a Product DAL cache item.
            if (!IsType(DalCacheItemType.Product))
            {
                Debug.Fail("DecodeNestingSize should be invoked only for Product DAL cache items.");
                return new WidthHeightDepthValue(0, 0, 0);
            }

            var height = ExtractValue<Single>(ProSpaceImportHelper.ProductYNesting);
            if (!height.EqualTo(0)) return new WidthHeightDepthValue(0, height, 0);
            var width = ExtractValue<Single>(ProSpaceImportHelper.ProductXNesting);
            if (!width.EqualTo(0)) return new WidthHeightDepthValue(width,0,0);
            var depth = ExtractValue<Single>(ProSpaceImportHelper.ProductZNesting);
            return new WidthHeightDepthValue(0, 0, depth);
        }

        /// <summary>
        ///     Decodes the Max Stack value for the current ProductCacheItem, using defaults if necessary.
        /// </summary>
        /// <param name="planogramCacheItem">The Planogram Cache Item this product is being created for, to obtain the right defaults if necessary.</param>
        /// <returns>A Byte value with the applicable Max Stack for the product, without considering Position overrides.</returns>
        private Byte DecodeMaxStack(DalCacheItem planogramCacheItem)
        {
            //  Check that this is invoked for a Product DAL cache item.
            if (!IsType(DalCacheItemType.Product))
            {
                Debug.Fail("DecodeMaxStack should be invoked only for Product DAL cache items.");
                return 0;
            }

            //  First try to get the value for the product.
            var sourceValue = ExtractValue<Int32>(ProSpaceImportHelper.ProductMerchYMax);

            //  If necessary default to the value on the Planogram.
            if (sourceValue == -1) sourceValue = planogramCacheItem.ExtractValue<Int32>(ProSpaceImportHelper.PlanogramMerchYMax);

            //  If necessary default to the value on the Project.
            if (sourceValue == -1) sourceValue = planogramCacheItem.GetProjectCacheItem().ExtractValue<Int32>(ProSpaceImportHelper.ProjectMerchYMax);

            //  Make sure we have a valid value.
            if (sourceValue < Byte.MinValue || sourceValue > Byte.MaxValue) sourceValue = 0;

            //  Return the Max Stack value.
            return (Byte) sourceValue;
        }

        /// <summary>
        ///     Decodes the Max Deep value for the current ProductCacheItem, using defaults if necessary.
        /// </summary>
        /// <param name="planogramCacheItem">The Planogram Cache Item this product is being created for, to obtain the right defaults if necessary.</param>
        /// <returns>A Byte value with the applicable Max Deep for the product, without considering Position overrides.</returns>
        private Byte DecodeMaxDeep(DalCacheItem planogramCacheItem)
        {
            //  Check that this is invoked for a Product DAL cache item.
            if (!IsType(DalCacheItemType.Product))
            {
                Debug.Fail("DecodeMaxDeep should be invoked only for Product DAL cache items.");
                return 0;
            }

            //  First try to get the value for the product.
            var sourceValue = ExtractValue<Int32>(ProSpaceImportHelper.ProductMerchZMax);

            //  If necessary default to the value on the Planogram.
            if (sourceValue == -1) sourceValue = planogramCacheItem.ExtractValue<Int32>(ProSpaceImportHelper.PlanogramMerchZMax);

            //  If necessary default to the value on the Project.
            if (sourceValue == -1) sourceValue = planogramCacheItem.GetProjectCacheItem().ExtractValue<Int32>(ProSpaceImportHelper.ProjectMerchZMax);

            //  Make sure we have a valid value.
            if (sourceValue < Byte.MinValue || sourceValue > Byte.MaxValue) sourceValue = 0;

            //  Return the Max Stack value.
            return (Byte)sourceValue;
        }

        /// <summary>
        ///     Decodes the Min Deep value for the current ProductCacheItem, using defaults if necessary.
        /// </summary>
        /// <param name="planogramCacheItem">The Planogram Cache Item this product is being created for, to obtain the right defaults if necessary.</param>
        /// <returns>A Byte value with the applicable Min Deep for the product, without considering Position overrides.</returns>
        private Byte DecodeMinDeep(DalCacheItem planogramCacheItem)
        {
            //  Check that this is invoked for a Product DAL cache item.
            if (!IsType(DalCacheItemType.Product))
            {
                Debug.Fail("DecodeMinDeep should be invoked only for Product DAL cache items.");
                return 0;
            }

            //  First try to get the value for the product.
            var sourceValue = ExtractValue<Int32>(ProSpaceImportHelper.ProductMerchZMin);

            //  If necessary default to the value on the Planogram.
            if (sourceValue == -1) sourceValue = planogramCacheItem.ExtractValue<Int32>(ProSpaceImportHelper.PlanogramMerchZMin);

            //  If necessary default to the value on the Project.
            if (sourceValue == -1) sourceValue = planogramCacheItem.GetProjectCacheItem().ExtractValue<Int32>(ProSpaceImportHelper.ProjectMerchZMin);

            //  Make sure we have a valid value.
            if (sourceValue < Byte.MinValue || sourceValue > Byte.MaxValue) sourceValue = 0;

            //  Return the Max Stack value.
            return (Byte)sourceValue;
        }

        /// <summary>
        ///     Decodes the Max Top Cap value for the current ProductCacheItem, using defaults if necessary.
        /// </summary>
        /// <param name="planogramCacheItem">The Planogram Cache Item this product is being created for, to obtain the right defaults if necessary.</param>
        /// <returns>A Byte value with the applicable Max Top Cap for the product, without considering Position overrides.</returns>
        private Byte DecodeMaxTopCap(DalCacheItem planogramCacheItem)
        {
            //  Check that this is invoked for a Product DAL cache item.
            if (!IsType(DalCacheItemType.Product))
            {
                Debug.Fail("DecodeMaxTopCap should be invoked only for Product DAL cache items.");
                return 0;
            }

            //  First try to get the value for the product.
            var sourceValue = ExtractValue<Int32>(ProSpaceImportHelper.ProductMerchYCaps);

            //  If necessary default to the value on the Planogram.
            if (sourceValue == -1) sourceValue = planogramCacheItem.ExtractValue<Int32>(ProSpaceImportHelper.PlanogramMerchYCaps);

            //  If necessary default to the value on the Project.
            if (sourceValue == -1) sourceValue = planogramCacheItem.GetProjectCacheItem().ExtractValue<Int32>(ProSpaceImportHelper.ProjectMerchYCaps);

            //  Make sure we have a valid value.
            if (sourceValue < Byte.MinValue || sourceValue > Byte.MaxValue) sourceValue = 0;

            //  Return the Max Stack value.
            return (Byte)sourceValue;
        }

        /// <summary>
        ///     Decodes the Max Right Cap value for the current ProductCacheItem, using defaults if necessary.
        /// </summary>
        /// <param name="planogramCacheItem">The Planogram Cache Item this product is being created for, to obtain the right defaults if necessary.</param>
        /// <returns>A Byte value with the applicable Max Right Cap for the product, without considering Position overrides.</returns>
        private Byte DecodeMaxRightCap(DalCacheItem planogramCacheItem)
        {
            //  Check that this is invoked for a Product DAL cache item.
            if (!IsType(DalCacheItemType.Product))
            {
                Debug.Fail("DecodeMaxRightCap should be invoked only for Product DAL cache items.");
                return 0;
            }

            //  First try to get the value for the product.
            var sourceValue = ExtractValue<Int32>(ProSpaceImportHelper.ProductMerchXCaps);

            //  If necessary default to the value on the Planogram.
            if (sourceValue == -1) sourceValue = planogramCacheItem.ExtractValue<Int32>(ProSpaceImportHelper.PlanogramMerchXCaps);

            //  If necessary default to the value on the Project.
            if (sourceValue == -1) sourceValue = planogramCacheItem.GetProjectCacheItem().ExtractValue<Int32>(ProSpaceImportHelper.ProjectMerchXCaps);

            //  Make sure we have a valid value.
            if (sourceValue < Byte.MinValue || sourceValue > Byte.MaxValue) sourceValue = 0;

            //  Return the Max Stack value.
            return (Byte)sourceValue;
        }

        #endregion

        #region PlanogramFixtureComponent

        private void EncodePlanogramFixtureComponentDto(PlanogramFixtureComponentDto dto)
        {
            InsertValue(ProSpaceImportHelper.FixtureSlope, dto.Slope);
            InsertValue(ProSpaceImportHelper.FixtureAngle, dto.Angle);
            InsertValue(ProSpaceImportHelper.FixtureRoll, dto.Roll);
            InsertValue(ProSpaceImportHelper.FixturePositionId, dto.ComponentSequenceNumber);

            var x = dto.X;
            var y = dto.Y;
            var z = dto.Z;

            Object output = null;

            DalCacheItem segment = this._dalCache.GetSegmentCacheItem(dto.PlanogramFixtureId.ToString());

            if (segment != null && segment.Dtos.TryGetValue(typeof(PlanogramFixtureItemDto), out output))
            {
                PlanogramFixtureItemDto fixtureItemDto = output as PlanogramFixtureItemDto;
                x += fixtureItemDto.X;
                y += fixtureItemDto.Y;
                z += fixtureItemDto.Z;
            }
            InsertValue(ProSpaceImportHelper.FixtureX, x);
            InsertValue(ProSpaceImportHelper.FixtureY, y);
            InsertValue(ProSpaceImportHelper.FixtureZ, z);
        }

        private PlanogramFixtureComponentDto DecodePlanogramFixtureComponentDto()
        {
            //  Check that this is invoked for a Fixture DAL cache item.
            if (!IsType(DalCacheItemType.Fixture))
            {
                Debug.Fail("DecodePlanogramFixtureComponentDto should be invoked only for Fixture DAL cache items.");
                return null;
            }
            
            //  Try fetching already existing data first.
            var planogramFixtureComponentDto = InstanceCache.Get<PlanogramFixtureComponentDto>();
            if (planogramFixtureComponentDto != null) return planogramFixtureComponentDto;

            DalCacheItem segmentDalCacheItem = GetContainingSegmentCacheItem(this);

            if (segmentDalCacheItem == null)
                throw new NullReferenceException(
                    String.Format("DecodePlanogramFixtureComponentDto could not find the parent Fixture element for:{0}{1}",
                                  Environment.NewLine,
                                  _data));

            var planogramComponentDto = GetDto<PlanogramComponentDto>();
            PointValue fixtureCoordinates = DecodeFixtureCoordinates();
            var planogramFixtureSlope = ExtractValue<Single>(ProSpaceImportHelper.FixtureSlope);
            var planogramFixtureAngle = ExtractValue<Single>(ProSpaceImportHelper.FixtureAngle);
            var planogramFixtureRoll = ExtractValue<Single>(ProSpaceImportHelper.FixtureRoll);
            planogramFixtureComponentDto = new PlanogramFixtureComponentDto
                                           {
                                               Id = Id,
                                               PlanogramFixtureId = segmentDalCacheItem.Id,
                                               PlanogramComponentId = planogramComponentDto.Id,
                                               X = fixtureCoordinates.X,
                                               Y = fixtureCoordinates.Y,
                                               Z = fixtureCoordinates.Z,
                                               Slope = SpacePlanningImportHelper.ConvertToRadians(planogramFixtureSlope),
                                               Angle = SpacePlanningImportHelper.ConvertToRadians(planogramFixtureAngle),
                                               Roll = SpacePlanningImportHelper.ConvertToRadians(planogramFixtureRoll), 
                                               ComponentSequenceNumber = ExtractValue<Int16?>(ProSpaceImportHelper.FixturePositionId)
                                           };

            //  Cache the result for subsequent accesses and return the dtos.
            InstanceCache.Set(planogramFixtureComponentDto);
            return planogramFixtureComponentDto;
        }

        private List<PlanogramFixtureComponentDto> DecodePlanogramFixtureComponentDtosInSegment()
        {
            //  Check that this is invoked for a Segment DAL cache item.
            if (!IsType(DalCacheItemType.Segment))
            {
                Debug.Fail("DecodePlanogramFixtureComponentDtosInFixture should be invoked only for Segment DAL cache items.");
                return new List<PlanogramFixtureComponentDto>();
            }

            //  Try fetching already existing data first.
            var planogramFixtureComponentDtos = InstanceCache.Get<List<PlanogramFixtureComponentDto>>();
            if (planogramFixtureComponentDtos != null) return planogramFixtureComponentDtos;

            planogramFixtureComponentDtos = new List<PlanogramFixtureComponentDto>();

            String planogramData = _parent.Data;
            //  Add a base if it is showing.
            if (_parent.ExtractValue<Boolean>(ProSpaceImportHelper.PlanogramDrawBase))
                planogramFixtureComponentDtos.Add(CreateBasePlanogramFixtureComponentDto());

            //  Add a backboard if it is showing.
            if (_parent.ExtractValue<Boolean>(ProSpaceImportHelper.PlanogramDrawBack))
                planogramFixtureComponentDtos.Add(CreateBackboardPlanogramFixtureComponentDto());

            //  Cache the result for subsequent accesses and return the dtos.
            InstanceCache.Set(planogramFixtureComponentDtos);
            return planogramFixtureComponentDtos;
        }

        private PlanogramFixtureComponentDto CreateBasePlanogramFixtureComponentDto()
        {
            Object baseId = GetBaseId();
            return new PlanogramFixtureComponentDto
                   {
                       Id = baseId,
                       PlanogramFixtureId = Id,
                       PlanogramComponentId = baseId,
                       X = 0.0F,
                       Y = 0.0F,
                       Z = 0.0F,
                       Slope = 0.0F,
                       Angle = 0.0F,
                       Roll = 0.0F
                   };
        }

        private PlanogramFixtureComponentDto CreateBackboardPlanogramFixtureComponentDto()
        {
            DalCacheItem planogramCacheItem = _parent;
            Object backboardId = GetBackboardId();
            return new PlanogramFixtureComponentDto
                   {
                       Id = backboardId,
                       PlanogramFixtureId = Id,
                       PlanogramComponentId = backboardId,
                       X = 0.0F,
                       Y = 0.0F,
                       Z = -DecodeBackDepth(this, planogramCacheItem),
                       Slope = 0.0F,
                       Angle = 0.0F,
                       Roll = 0.0F
                   };
        }

        #endregion

        #region PlanogramSubComponent

        private UInt32 DecodeColour(Int32 value)
        {

            var r = (Byte)((value >> 0x10) & 0xff);
            var g = (Byte)((value >> 8) & 0xff);
            var b = (Byte)(value & 0xff);
            var a = (Byte)((value >> 0x18) & 0xff);

            if (a == 0) 
            {
                r = 255;
                b = 255;
                g = 255;
                a = 255; 
            }

            UInt32 uint32Color =
                                 (((UInt32)b) << 16) |
                                 (((UInt32)g) << 8) |
                                 r;

            return uint32Color;
        }
        private void EncodePlanogramSubComponentDto(PlanogramSubComponentDto dto)
        {
           
            InsertValue(ProSpaceImportHelper.FixtureColour, DecodeColour(dto.FillColourFront));
        }
        private PlanogramSubComponentDto DecodePlanogramSubComponentDto()
        {
            //  Check that this is invoked for a Fixture DAL cache item.
            if (!IsType(DalCacheItemType.Fixture))
            {
                Debug.Fail("DecodePlanogramFixtureComponentDtosInFixture should be invoked only for Fixture DAL cache items.");
                return null;
            }

            //  Try fetching already existing data first.
            var dto = InstanceCache.Get<PlanogramSubComponentDto>();
            if (dto != null) return dto;
            ProSpaceMerchSizeType sizeTypeX = (ProSpaceMerchSizeType)ExtractValue<Byte>(ProSpaceImportHelper.FixtureMerchXSize);
            ProSpaceMerchSizeType sizeTypeY = (ProSpaceMerchSizeType)ExtractValue<Byte>(ProSpaceImportHelper.FixtureMerchYSize);
            ProSpaceMerchSizeType sizeTypeZ = (ProSpaceMerchSizeType)ExtractValue<Byte>(ProSpaceImportHelper.FixtureMerchZSize);
            Boolean canSqueeze = sizeTypeX == ProSpaceMerchSizeType.ExpandSqueeze ||
                                sizeTypeY == ProSpaceMerchSizeType.ExpandSqueeze ||
                                sizeTypeZ == ProSpaceMerchSizeType.ExpandSqueeze;

            var planogramComponentDto = GetDto<PlanogramComponentDto>();
            dto =
                new PlanogramSubComponentDto
                {
                    Id = Id,
                    PlanogramComponentId = Id,
                    Name = DecodePlanogramSubComponentName(planogramComponentDto),
                    Height = planogramComponentDto.Height,
                    Width = planogramComponentDto.Width,
                    Depth = planogramComponentDto.Depth,

                    LineColour = SpacePlanningImportHelper.DefaultLineColour,
                    LineThickness = SpacePlanningImportHelper.GetDefaultLineThickness(ExtractedPlanogramLengthUnitOfMeasureType),
                    
                    HasCollisionDetection = DecodeCollisionDetection(this),
                    IsProductOverlapAllowed = true,
                    IsProductSqueezeAllowed = canSqueeze,
                    IsVisible = true,
                    
                    MerchandisingStrategyX = (Byte) DecodeMerchandisingStrategyX(),
                    MerchandisingStrategyY = (Byte) DecodeMerchandisingStrategyY(),
                    MerchandisingStrategyZ = (Byte) DecodeMerchandisingStrategyZ(),

                    DividerObstructionFillColour = -65536,
                    DividerObstructionFillPattern = 0,
                };

            //  Decode type dependent values.
            var proSpaceComponentType = (ProSpaceComponentType)ExtractValue<Byte>(ProSpaceImportHelper.FixtureType);
            dto.MerchandisingType = (Byte) DecodeSubComponentMerchandisingType(proSpaceComponentType);
            SetFillColorOrDefault(dto, proSpaceComponentType);
            SetFillPatternOrDefault(dto);
            SetMerchandisingSpace(dto, proSpaceComponentType);

            //  Decode type unique values.
            switch (proSpaceComponentType)
            {
                case ProSpaceComponentType.Shelf:
                case ProSpaceComponentType.PolygonalShelf:
                    SetRiserValues(dto);
                    SetOverhangValues(dto);
                    SetDividerValues(dto);
                    dto.CombineType =
                        (Byte) ((ProSpaceFixtureCombineType) ExtractValue<Int32>(ProSpaceImportHelper.FixtureCanCombine)).ToSubComponentCombineType();
                    break;
                case ProSpaceComponentType.Chest:
                case ProSpaceComponentType.Bin:
                    SetFaceThicknessValues(dto);
                    SetDividerValues(dto, true);
                    dto.CombineType =
                        (Byte) ((ProSpaceFixtureCombineType) ExtractValue<Int32>(ProSpaceImportHelper.FixtureCanCombine)).ToSubComponentCombineType();
                    break;
                case ProSpaceComponentType.Bar:
                    SetOverhangValues(dto);
                    //  Only set the Merch Constraints for row height and width (size of peg).
                    dto.MerchConstraintRow1Height = SpacePlanningImportHelper.GetDefaultMerchConstraintRow1Height(ExtractedPlanogramLengthUnitOfMeasureType);
                    dto.MerchConstraintRow1Width = SpacePlanningImportHelper.GetDefaultMerchConstraintRow1Width(ExtractedPlanogramLengthUnitOfMeasureType);
                    break;
                case ProSpaceComponentType.Pegboard:
                    SetOverhangValues(dto);
                    SetMerchConstraint1Values(dto, planogramComponentDto);
                    break;
                case ProSpaceComponentType.MultiRowPegboard:
                    SetOverhangValues(dto);
                    SetMerchConstraint1Values(dto, planogramComponentDto);
                    SetMerchConstraint2Values(dto);
                    break;
                case ProSpaceComponentType.Rod:
                case ProSpaceComponentType.LateralRod:
                case ProSpaceComponentType.CurvedRod:
                case ProSpaceComponentType.Obstruction:
                case ProSpaceComponentType.Sign:
                case ProSpaceComponentType.GravityFeed:
                    break;
                default:
                    Debug.Fail("Unknown ProSpaceComponentType when calling DecodePlanogramSubComponentDto.");
                    break;
            }

            //  Cache the result for subsequent accesses and return the dtos.
            InstanceCache.Set(dto);
            return dto;
        }

        private void SetFaceThicknessValues(PlanogramSubComponentDto planogramSubComponentDto)
        {
            planogramSubComponentDto.FaceThicknessBottom = ExtractValue<Single>(ProSpaceImportHelper.FixtureHeight);
            var frontBackValue = ExtractValue<Single>(ProSpaceImportHelper.FixtureWallDepth);
            planogramSubComponentDto.FaceThicknessFront = frontBackValue;
            planogramSubComponentDto.FaceThicknessBack = frontBackValue;
            var leftRightValue = ExtractValue<Single>(ProSpaceImportHelper.FixtureWallWidth);
            planogramSubComponentDto.FaceThicknessLeft = leftRightValue;
            planogramSubComponentDto.FaceThicknessRight = leftRightValue;
        }

        private void SetMerchConstraint1Values(PlanogramSubComponentDto planogramSubComponentDto, PlanogramComponentDto componentDto)
        {
            planogramSubComponentDto.MerchConstraintRow1StartX = ExtractValue<Single>(ProSpaceImportHelper.FixtureXStart);
            planogramSubComponentDto.MerchConstraintRow1SpacingX = ExtractValue<Single>(ProSpaceImportHelper.FixtureXSpacing);
            planogramSubComponentDto.MerchConstraintRow1StartY = ExtractValue<Single>(ProSpaceImportHelper.FixtureYStart);
            var spacingY = ExtractValue<Single>(ProSpaceImportHelper.FixtureYSpacing);
            if (spacingY.EqualTo(0))
            {
                spacingY = SpacePlanningImportHelper.GetDefaultJdaMerchConstraintRow1SpacingY(ExtractedPlanogramLengthUnitOfMeasureType);
                if (planogramSubComponentDto.MerchConstraintRow1SpacingX.EqualTo(0))
                    componentDto.ComponentType = (Byte) PlanogramComponentType.SlotWall;
            }
            planogramSubComponentDto.MerchConstraintRow1SpacingY = spacingY;
            Single constraintRow1Height = SpacePlanningImportHelper.GetDefaultMerchConstraintRow1Height(ExtractedPlanogramLengthUnitOfMeasureType);
            planogramSubComponentDto.MerchConstraintRow1Height = spacingY.LessOrEqualThan(constraintRow1Height) ? spacingY/2 : constraintRow1Height;
            planogramSubComponentDto.MerchConstraintRow1Width = SpacePlanningImportHelper.GetDefaultMerchConstraintRow1Width(ExtractedPlanogramLengthUnitOfMeasureType);
        }

        private void SetMerchConstraint2Values(PlanogramSubComponentDto planogramSubComponentDto)
        {
            planogramSubComponentDto.MerchConstraintRow2StartX = ExtractValue<Single>(ProSpaceImportHelper.FixtureXStart2);
            planogramSubComponentDto.MerchConstraintRow2SpacingX = ExtractValue<Single>(ProSpaceImportHelper.FixtureXSpacing2);
            planogramSubComponentDto.MerchConstraintRow2StartY = ExtractValue<Single>(ProSpaceImportHelper.FixtureYStart);
            var spacingY = ExtractValue<Single>(ProSpaceImportHelper.FixtureYSpacing);
            if (spacingY.EqualTo(0)) spacingY = SpacePlanningImportHelper.GetDefaultJdaMerchConstraintRow1SpacingY(ExtractedPlanogramLengthUnitOfMeasureType);
            planogramSubComponentDto.MerchConstraintRow2SpacingY = spacingY;
            Single constraintRow2Height = SpacePlanningImportHelper.GetDefaultMerchConstraintRow1Height(ExtractedPlanogramLengthUnitOfMeasureType);
            planogramSubComponentDto.MerchConstraintRow2Height = spacingY.LessOrEqualThan(constraintRow2Height) ? spacingY / 2 : constraintRow2Height;
            planogramSubComponentDto.MerchConstraintRow2Width = SpacePlanningImportHelper.GetDefaultMerchConstraintRow1Width(ExtractedPlanogramLengthUnitOfMeasureType);
        }

        private void SetDividerValues(PlanogramSubComponentDto planogramSubComponentDto, Boolean isChest = false)
        {
            planogramSubComponentDto.IsDividerObstructionAtStart = ExtractValue<Boolean>(ProSpaceImportHelper.FixtureDividerAtStart);
            planogramSubComponentDto.IsDividerObstructionAtEnd = ExtractValue<Boolean>(ProSpaceImportHelper.FixtureDividerAtEnd);
            planogramSubComponentDto.DividerObstructionWidth = ExtractValue<Single>(ProSpaceImportHelper.FixtureDividerWidth);
            planogramSubComponentDto.DividerObstructionHeight = ExtractValue<Single>(ProSpaceImportHelper.FixtureDividerHeight);
            planogramSubComponentDto.DividerObstructionDepth = ExtractValue<Single>(ProSpaceImportHelper.FixtureDividerDepth);
            planogramSubComponentDto.DividerObstructionStartX = ExtractValue<Single>(ProSpaceImportHelper.FixtureXStart);
            planogramSubComponentDto.DividerObstructionStartY = isChest ? 0.0F : ExtractValue<Single>(ProSpaceImportHelper.FixtureYStart);
            planogramSubComponentDto.DividerObstructionStartZ = isChest ? ExtractValue<Single>(ProSpaceImportHelper.FixtureYStart) : 0.0F;
            planogramSubComponentDto.DividerObstructionSpacingX = ExtractValue<Single>(ProSpaceImportHelper.FixtureXSpacing);
            planogramSubComponentDto.DividerObstructionSpacingY = isChest ? 0.0F : ExtractValue<Single>(ProSpaceImportHelper.FixtureYSpacing);
            planogramSubComponentDto.DividerObstructionSpacingZ = isChest ? ExtractValue<Single>(ProSpaceImportHelper.FixtureYSpacing) : 0.0F;
            planogramSubComponentDto.IsDividerObstructionByFacing = ExtractValue<Boolean>(ProSpaceImportHelper.FixtureDividerBetweenFacings);
        }

        private void SetOverhangValues(PlanogramSubComponentDto planogramSubComponentDto)
        {
            planogramSubComponentDto.LeftOverhang = ExtractValue<Single>(ProSpaceImportHelper.FixtureLeftOverhang);
            planogramSubComponentDto.RightOverhang = ExtractValue<Single>(ProSpaceImportHelper.FixtureRightOverhang);
            planogramSubComponentDto.FrontOverhang = ExtractValue<Single>(ProSpaceImportHelper.FixtureFrontOverhang);
            planogramSubComponentDto.BackOverhang = ExtractValue<Single>(ProSpaceImportHelper.FixtureBackOverhang);
            planogramSubComponentDto.TopOverhang = ExtractValue<Single>(ProSpaceImportHelper.FixtureUpperOverhang);
            planogramSubComponentDto.BottomOverhang = ExtractValue<Single>(ProSpaceImportHelper.FixtureLowerOverhang);
        }

        private void SetRiserValues(PlanogramSubComponentDto planogramSubComponentDto)
        {
            var riserHeight = ExtractValue<Single>(ProSpaceImportHelper.FixtureGrilleHeight);
            if (!riserHeight.GreaterThan(0)) return;

            planogramSubComponentDto.RiserHeight = riserHeight;
            planogramSubComponentDto.RiserThickness = SpacePlanningImportHelper.GetDefaultRiserThickness(ExtractedPlanogramLengthUnitOfMeasureType);
            //  NB For now, ignore transparency as it is causing all sorts of issues.
            //planogramSubComponentDto.RiserTransparencyPercent = (Int32) ExtractValue<Single>(ProSpaceImportHelper.FixtureTransparency);
            planogramSubComponentDto.RiserColour = SpacePlanningImportHelper.DefaultRiserColour;
            planogramSubComponentDto.IsRiserPlacedOnFront = true;
        }

        private void SetMerchandisingSpace(PlanogramSubComponentDto dto, ProSpaceComponentType type)
        {
            switch (type)
            {
                case ProSpaceComponentType.Shelf:
                case ProSpaceComponentType.Chest:
                case ProSpaceComponentType.Bin:
                case ProSpaceComponentType.PolygonalShelf:
                case ProSpaceComponentType.Rod:
                case ProSpaceComponentType.LateralRod:
                case ProSpaceComponentType.CurvedRod:
                    dto.MerchandisableHeight = ExtractValue<Single>(ProSpaceImportHelper.FixtureMerch);
                    break;
                case ProSpaceComponentType.Bar:
                case ProSpaceComponentType.Pegboard:
                case ProSpaceComponentType.MultiRowPegboard:
                    dto.MerchandisableDepth = ExtractValue<Single>(ProSpaceImportHelper.FixtureMerch);
                    break;
                case ProSpaceComponentType.Obstruction:
                case ProSpaceComponentType.Sign:
                case ProSpaceComponentType.GravityFeed:
                    break;
                default:
                    Debug.Fail("Unknown ProSpaceComponentType when calling SetMerchandisingSpace.");
                    break;
            }
        }

        private void SetFillColorOrDefault(PlanogramSubComponentDto planogramSubComponentDto, ProSpaceComponentType type)
        {
            var fillColour = ExtractValue<Int32>(ProSpaceImportHelper.FixtureColour);
            if (fillColour == -1)
            {
                if (!ProSpaceImportHelper.DefaultFillColourByProSpaceComponentType.TryGetValue(type, out fillColour))
                {
                    Debug.Fail("Unknown ProSpaceComponentType when calling SetFillColourValues.");
                    fillColour = SpacePlanningImportHelper.DefaultShelfFillColour;
                }
            }
            else
            {
                fillColour = SpacePlanningImportHelper.DecodeColour(fillColour);
            }

            planogramSubComponentDto.FillColourBack = fillColour;
            planogramSubComponentDto.FillColourBottom = fillColour;
            planogramSubComponentDto.FillColourFront = fillColour;
            planogramSubComponentDto.FillColourLeft = fillColour;
            planogramSubComponentDto.FillColourRight = fillColour;
            planogramSubComponentDto.FillColourTop = fillColour;
        }

        private void SetFillPatternOrDefault(PlanogramSubComponentDto dto)
        {
            var source = ExtractValue<Int32>(ProSpaceImportHelper.FixtureFillPattern);
            Byte fillPattern;
            if (source == -1)
            {
                fillPattern = SpacePlanningImportHelper.DefaultFillPatternType;
            }
            else
            {
                fillPattern = (Byte) ((ProSpaceFillPatternType) source).ToPlanogramSubComponentFillPatternType();
            }
            dto.FillPatternTypeFront = fillPattern;
            dto.FillPatternTypeBack = fillPattern;
            dto.FillPatternTypeTop = fillPattern;
            dto.FillPatternTypeBottom = fillPattern;
            dto.FillPatternTypeLeft = fillPattern;
            dto.FillPatternTypeRight = fillPattern;
        }

        #region Merch X decoding

        private PlanogramSubComponentXMerchStrategyType DecodeMerchandisingStrategyX()
        {
            if (DecodeMerchXSize() == ProSpaceMerchSizeType.SpaceOutFacings) return PlanogramSubComponentXMerchStrategyType.Even;

            ProSpaceMerchPlacementType placementType = DecodeMerchXPlacement();
            ProSpaceMerchDirectionType directionType = DecodeMerchXDirection();
            switch (placementType)
            {
                case ProSpaceMerchPlacementType.Default:
                    //  Should not be default at this point, DecodeMerchXPlacement should always return a specific type.
                    break;
                case ProSpaceMerchPlacementType.Manual:
                    return PlanogramSubComponentXMerchStrategyType.Manual;
                case ProSpaceMerchPlacementType.Edge:
                case ProSpaceMerchPlacementType.Stacked:
                    switch (directionType)
                    {
                        case ProSpaceMerchDirectionType.Default:
                            //  Should not be default at this point, DecodeMerchXDirection should always return a specific type.
                            break;
                        case ProSpaceMerchDirectionType.Normal:
                            return placementType == ProSpaceMerchPlacementType.Edge
                                       ? PlanogramSubComponentXMerchStrategyType.Left
                                       : PlanogramSubComponentXMerchStrategyType.LeftStacked;
                        case ProSpaceMerchDirectionType.Reverse:
                            return placementType == ProSpaceMerchPlacementType.Edge
                                       ? PlanogramSubComponentXMerchStrategyType.Right
                                       : PlanogramSubComponentXMerchStrategyType.RightStacked;
                        default:
                            //  Unknown value.
                            break;
                    }
                    break;
                case ProSpaceMerchPlacementType.Spread:
                    return PlanogramSubComponentXMerchStrategyType.Even;
                default:
                    //  Unknown value.
                    break;
            }

            return PlanogramSubComponentXMerchStrategyType.Manual;
        }

        private ProSpaceMerchSizeType DecodeMerchXSize()
        {
            var fieldIndexByItemType =
                new Dictionary<DalCacheItemType, String>
                {
                    {DalCacheItemType.Fixture, ProSpaceImportHelper.FixtureMerchXSize},
                    {DalCacheItemType.Planogram, ProSpaceImportHelper.PlanogramMerchXSize},
                    {DalCacheItemType.Project, ProSpaceImportHelper.ProjectMerchXSize}
                };
            String fieldIndex;
            if (!fieldIndexByItemType.TryGetValue(CacheItemType, out fieldIndex))
                return ProSpaceMerchSizeType.Normal;

            var value = (ProSpaceMerchSizeType)ExtractValue<Int32>(fieldIndex);
            if (value != ProSpaceMerchSizeType.Default) return value;

            DalCacheItem defaultValuesCacheItem = GetDefaultValuesCacheItem(fieldIndexByItemType.Select(pair => pair.Key).ToList());
            return defaultValuesCacheItem != null ? defaultValuesCacheItem.DecodeMerchXSize() : ProSpaceMerchSizeType.Normal;
        }

        private ProSpaceMerchDirectionType DecodeMerchXDirection()
        {
            var fieldIndexByItemType =
                new Dictionary<DalCacheItemType, String>
                {
                    {DalCacheItemType.Fixture, ProSpaceImportHelper.FixtureMerchXDirection},
                    {DalCacheItemType.Planogram, ProSpaceImportHelper.PlanogramMerchXDirection},
                    {DalCacheItemType.Project, ProSpaceImportHelper.ProjectMerchXDirection}
                };
            String fieldIndex;
            if (!fieldIndexByItemType.TryGetValue(CacheItemType, out fieldIndex))
                return ProSpaceMerchDirectionType.Normal;

            var value = (ProSpaceMerchDirectionType) ExtractValue<Int32>(fieldIndex);
            if (value != ProSpaceMerchDirectionType.Default) return value;

            DalCacheItem defaultValuesCacheItem = GetDefaultValuesCacheItem(fieldIndexByItemType.Select(pair => pair.Key).ToList());
            return defaultValuesCacheItem != null ? defaultValuesCacheItem.DecodeMerchXDirection() : ProSpaceMerchDirectionType.Normal;
        }

        private ProSpaceMerchPlacementType DecodeMerchXPlacement()
        {
            var fieldIndexByItemType =
                new Dictionary<DalCacheItemType, String>
                {
                    {DalCacheItemType.Fixture, ProSpaceImportHelper.FixtureMerchXPlacement},
                    {DalCacheItemType.Planogram, ProSpaceImportHelper.PlanogramMerchXPlacement},
                    {DalCacheItemType.Project, ProSpaceImportHelper.ProjectMerchXPlacement},
                };
            String fieldIndex;
            if (!fieldIndexByItemType.TryGetValue(CacheItemType, out fieldIndex))
                return ProSpaceMerchPlacementType.Manual;

            var value = (ProSpaceMerchPlacementType) ExtractValue<Int32>(fieldIndex);
            if (value != ProSpaceMerchPlacementType.Default) return value;

            DalCacheItem defaultValuesCacheItem = GetDefaultValuesCacheItem(fieldIndexByItemType.Select(pair => pair.Key).ToList());
            return defaultValuesCacheItem != null ? defaultValuesCacheItem.DecodeMerchXPlacement() : ProSpaceMerchPlacementType.Manual;
        }

        #endregion

        #region Merch Y decoding

        private PlanogramSubComponentYMerchStrategyType DecodeMerchandisingStrategyY()
        {
            if (DecodeMerchYSize() == ProSpaceMerchSizeType.SpaceOutFacings) return PlanogramSubComponentYMerchStrategyType.Even;

            ProSpaceMerchPlacementType placementType = DecodeMerchYPlacement();
            ProSpaceMerchDirectionType directionType = DecodeMerchYDirection();
            switch (placementType)
            {
                case ProSpaceMerchPlacementType.Default:
                    //  Should not be default at this point, DecodeMerchYPlacement should always return a specific type.
                    break;
                case ProSpaceMerchPlacementType.Manual:
                    return PlanogramSubComponentYMerchStrategyType.Manual;
                case ProSpaceMerchPlacementType.Edge:
                case ProSpaceMerchPlacementType.Stacked:
                    switch (directionType)
                    {
                        case ProSpaceMerchDirectionType.Default:
                            //  Should not be default at this point, DecodeMerchYDirection should always return a specific type.
                            break;
                        case ProSpaceMerchDirectionType.Normal:
                            return placementType == ProSpaceMerchPlacementType.Edge
                                       ? PlanogramSubComponentYMerchStrategyType.Bottom
                                       : PlanogramSubComponentYMerchStrategyType.BottomStacked;
                        case ProSpaceMerchDirectionType.Reverse:
                            return placementType == ProSpaceMerchPlacementType.Edge
                                       ? PlanogramSubComponentYMerchStrategyType.Top
                                       : PlanogramSubComponentYMerchStrategyType.TopStacked;
                        default:
                            //  Unknown value.
                            break;
                    }
                    break;
                case ProSpaceMerchPlacementType.Spread:
                    return PlanogramSubComponentYMerchStrategyType.Even;
                default:
                    //  Unknown value.
                    break;
            }

            return PlanogramSubComponentYMerchStrategyType.Manual;
        }

        private ProSpaceMerchSizeType DecodeMerchYSize()
        {
            var fieldIndexByItemType =
                new Dictionary<DalCacheItemType, String>
                {
                    {DalCacheItemType.Fixture, ProSpaceImportHelper.FixtureMerchYSize},
                    {DalCacheItemType.Planogram, ProSpaceImportHelper.PlanogramMerchYSize},
                    {DalCacheItemType.Project, ProSpaceImportHelper.ProjectMerchYSize}
                };
            String fieldIndex;
            if (!fieldIndexByItemType.TryGetValue(CacheItemType, out fieldIndex))
                return ProSpaceMerchSizeType.Normal;

            var value = (ProSpaceMerchSizeType)ExtractValue<Int32>(fieldIndex);
            if (value != ProSpaceMerchSizeType.Default) return value;

            DalCacheItem defaultValuesCacheItem = GetDefaultValuesCacheItem(fieldIndexByItemType.Select(pair => pair.Key).ToList());
            return defaultValuesCacheItem != null ? defaultValuesCacheItem.DecodeMerchYSize() : ProSpaceMerchSizeType.Normal;
        }

        private ProSpaceMerchDirectionType DecodeMerchYDirection()
        {
            var fieldIndeYByItemType =
                new Dictionary<DalCacheItemType, String>
                {
                    {DalCacheItemType.Fixture, ProSpaceImportHelper.FixtureMerchYDirection},
                    {DalCacheItemType.Planogram, ProSpaceImportHelper.PlanogramMerchYDirection},
                    {DalCacheItemType.Project, ProSpaceImportHelper.ProjectMerchYDirection}
                };
            String fieldIndeY;
            if (!fieldIndeYByItemType.TryGetValue(CacheItemType, out fieldIndeY))
                return ProSpaceMerchDirectionType.Normal;

            var value = (ProSpaceMerchDirectionType)ExtractValue<Int32>(fieldIndeY);
            if (value != ProSpaceMerchDirectionType.Default) return value;

            DalCacheItem defaultValuesCacheItem = GetDefaultValuesCacheItem(fieldIndeYByItemType.Select(pair => pair.Key).ToList());
            return defaultValuesCacheItem != null ? defaultValuesCacheItem.DecodeMerchYDirection() : ProSpaceMerchDirectionType.Normal;
        }

        private ProSpaceMerchPlacementType DecodeMerchYPlacement()
        {
            var fieldIndeYByItemType =
                new Dictionary<DalCacheItemType, String>
                {
                    {DalCacheItemType.Fixture, ProSpaceImportHelper.FixtureMerchYPlacement},
                    {DalCacheItemType.Planogram, ProSpaceImportHelper.PlanogramMerchYPlacement},
                    {DalCacheItemType.Project, ProSpaceImportHelper.ProjectMerchYPlacement},
                };
            String fieldIndeY;
            if (!fieldIndeYByItemType.TryGetValue(CacheItemType, out fieldIndeY))
                return ProSpaceMerchPlacementType.Manual;

            var value = (ProSpaceMerchPlacementType)ExtractValue<Int32>(fieldIndeY);
            if (value != ProSpaceMerchPlacementType.Default) return value;

            DalCacheItem defaultValuesCacheItem = GetDefaultValuesCacheItem(fieldIndeYByItemType.Select(pair => pair.Key).ToList());
            return defaultValuesCacheItem != null ? defaultValuesCacheItem.DecodeMerchYPlacement() : ProSpaceMerchPlacementType.Manual;
        }

        #endregion

        #region Merch Z decoding

        private PlanogramSubComponentZMerchStrategyType DecodeMerchandisingStrategyZ()
        {
            if (DecodeMerchZSize() == ProSpaceMerchSizeType.SpaceOutFacings) return PlanogramSubComponentZMerchStrategyType.Even;

            ProSpaceMerchPlacementType placementType = DecodeMerchZPlacement();
            ProSpaceMerchDirectionType directionType = DecodeMerchZDirection();
            switch (placementType)
            {
                case ProSpaceMerchPlacementType.Default:
                    //  Should not be default at this point, DecodeMerchZPlacement should always return a specific type.
                    break;
                case ProSpaceMerchPlacementType.Manual:
                    return PlanogramSubComponentZMerchStrategyType.Manual;
                case ProSpaceMerchPlacementType.Edge:
                case ProSpaceMerchPlacementType.Stacked:
                    switch (directionType)
                    {
                        case ProSpaceMerchDirectionType.Default:
                            //  Should not be default at this point, DecodeMerchZDirection should always return a specific type.
                            break;
                        case ProSpaceMerchDirectionType.Normal:
                            return placementType == ProSpaceMerchPlacementType.Edge
                                       ? PlanogramSubComponentZMerchStrategyType.Back
                                       : PlanogramSubComponentZMerchStrategyType.BackStacked;
                        case ProSpaceMerchDirectionType.Reverse:
                            return placementType == ProSpaceMerchPlacementType.Edge
                                       ? PlanogramSubComponentZMerchStrategyType.Front
                                       : PlanogramSubComponentZMerchStrategyType.FrontStacked;
                        default:
                            //  Unknown value.
                            break;
                    }
                    break;
                case ProSpaceMerchPlacementType.Spread:
                    return PlanogramSubComponentZMerchStrategyType.Even;
                default:
                    //  Unknown value.
                    break;
            }

            return PlanogramSubComponentZMerchStrategyType.Manual;
        }

        private ProSpaceMerchSizeType DecodeMerchZSize()
        {
            var fieldIndexByItemType =
                new Dictionary<DalCacheItemType, String>
                {
                    {DalCacheItemType.Fixture, ProSpaceImportHelper.FixtureMerchZSize},
                    {DalCacheItemType.Planogram, ProSpaceImportHelper.PlanogramMerchZSize},
                    {DalCacheItemType.Project, ProSpaceImportHelper.ProjectMerchZSize}
                };
            String fieldIndex;
            if (!fieldIndexByItemType.TryGetValue(CacheItemType, out fieldIndex))
                return ProSpaceMerchSizeType.Normal;

            var value = (ProSpaceMerchSizeType)ExtractValue<Int32>(fieldIndex);
            if (value != ProSpaceMerchSizeType.Default) return value;

            DalCacheItem defaultValuesCacheItem = GetDefaultValuesCacheItem(fieldIndexByItemType.Select(pair => pair.Key).ToList());
            return defaultValuesCacheItem != null ? defaultValuesCacheItem.DecodeMerchZSize() : ProSpaceMerchSizeType.Normal;
        }

        private ProSpaceMerchDirectionType DecodeMerchZDirection()
        {
            var fieldIndeZByItemType =
                new Dictionary<DalCacheItemType, String>
                {
                    {DalCacheItemType.Fixture, ProSpaceImportHelper.FixtureMerchZDirection},
                    {DalCacheItemType.Planogram, ProSpaceImportHelper.PlanogramMerchZDirection},
                    {DalCacheItemType.Project, ProSpaceImportHelper.ProjectMerchZDirection}
                };
            String fieldIndeZ;
            if (!fieldIndeZByItemType.TryGetValue(CacheItemType, out fieldIndeZ))
                return ProSpaceMerchDirectionType.Normal;

            var value = (ProSpaceMerchDirectionType)ExtractValue<Int32>(fieldIndeZ);
            if (value != ProSpaceMerchDirectionType.Default) return value;

            DalCacheItem defaultValuesCacheItem = GetDefaultValuesCacheItem(fieldIndeZByItemType.Select(pair => pair.Key).ToList());
            return defaultValuesCacheItem != null ? defaultValuesCacheItem.DecodeMerchZDirection() : ProSpaceMerchDirectionType.Normal;
        }

        private ProSpaceMerchPlacementType DecodeMerchZPlacement()
        {
            var fieldIndeZByItemType =
                new Dictionary<DalCacheItemType, String>
                {
                    {DalCacheItemType.Fixture, ProSpaceImportHelper.FixtureMerchZPlacement},
                    {DalCacheItemType.Planogram, ProSpaceImportHelper.PlanogramMerchZPlacement},
                    {DalCacheItemType.Project, ProSpaceImportHelper.ProjectMerchZPlacement},
                };
            String fieldIndeZ;
            if (!fieldIndeZByItemType.TryGetValue(CacheItemType, out fieldIndeZ))
                return ProSpaceMerchPlacementType.Manual;

            var value = (ProSpaceMerchPlacementType)ExtractValue<Int32>(fieldIndeZ);
            if (value != ProSpaceMerchPlacementType.Default) return value;

            DalCacheItem defaultValuesCacheItem = GetDefaultValuesCacheItem(fieldIndeZByItemType.Select(pair => pair.Key).ToList());
            return defaultValuesCacheItem != null ? defaultValuesCacheItem.DecodeMerchZPlacement() : ProSpaceMerchPlacementType.Manual;
        }

        #endregion

        /// <summary>
        ///     Decodes the <c>Planogram Sub Component</c> information stored in the <c>Segment data</c>.
        /// </summary>
        /// <remarks>
        ///     <c>Segments</c> have the information needed to create the <c>Base</c> and <c>Backboard</c> sub components. 
        /// <para />
        ///     If the <c>DAL cache item</c> does NOT contain <c>Segment data</c> an empty list will be returned.
        /// </remarks>
        /// <returns>A collection with the sub components generated from the Segment data.</returns>
        private List<PlanogramSubComponentDto> DecodePlanogramSubComponentDtosInSegment()
        {
            //  Check that this is invoked for a Segment DAL cache item.
            if (!IsType(DalCacheItemType.Segment))
            {
                Debug.Fail("DecodePlanogramSubComponentDtosInFixture should be invoked only for Segment DAL cache items.");
                return new List<PlanogramSubComponentDto>();
            }
            
            //  Try fetching already existing data first.
            var planogramFixtureComponentDtos = InstanceCache.Get<List<PlanogramSubComponentDto>>();
            if (planogramFixtureComponentDtos != null) return planogramFixtureComponentDtos;

            planogramFixtureComponentDtos = new List<PlanogramSubComponentDto>();

            //  Add a base if it is showing.
            if (_parent.ExtractValue<Boolean>(ProSpaceImportHelper.PlanogramDrawBase))
                planogramFixtureComponentDtos.Add(CreateBasePlanogramSubComponentDto());

            //  Add a backboard if it is showing.
            if (_parent.ExtractValue<Boolean>(ProSpaceImportHelper.PlanogramDrawBack))
                planogramFixtureComponentDtos.Add(CreateBackboardPlanogramSubComponentDto());

            //  Cache the result for subsequent accesses and return the dtos.
            InstanceCache.Set(planogramFixtureComponentDtos);
            return planogramFixtureComponentDtos;
        }

        private PlanogramSubComponentDto CreateBackboardPlanogramSubComponentDto()
        {
            Object backboardId = GetBackboardId();
            Byte fillPatternType = SpacePlanningImportHelper.DefaultFillPatternType;
            Int32 fillColour = SpacePlanningImportHelper.DecodeColour(_parent.ExtractValue<Single>(ProSpaceImportHelper.PlanogramColor));
            DalCacheItem planogramCacheItem = _parent;
            var dto =
                new PlanogramSubComponentDto
                {
                    Id = backboardId,
                    PlanogramComponentId = backboardId,
                    Name = String.Format(Message.JdaImport_BackboardDefaultNameMask, backboardId),
                    Height = DecodeFixtureHeight(this, planogramCacheItem),
                    Width = ExtractValue<Single>(ProSpaceImportHelper.SegmentWidth),
                    Depth = DecodeBackDepth(this, planogramCacheItem),
                    X = 0.0F,
                    Y = 0.0F,
                    Z = 0.0F,
                    Slope = 0.0F,
                    Angle = 0.0F,
                    Roll = 0.0F,
                    ShapeType = (Byte) PlanogramSubComponentShapeType.Box,
                    IsVisible = true,
                    HasCollisionDetection = true,
                    FillPatternTypeFront = fillPatternType,
                    FillPatternTypeBack = fillPatternType,
                    FillPatternTypeTop = fillPatternType,
                    FillPatternTypeBottom = fillPatternType,
                    FillPatternTypeLeft = fillPatternType,
                    FillPatternTypeRight = fillPatternType,
                    FillColourFront = fillColour,
                    FillColourBack = fillColour,
                    FillColourTop = fillColour,
                    FillColourBottom = fillColour,
                    FillColourLeft = fillColour,
                    FillColourRight = fillColour,
                    LineColour = SpacePlanningImportHelper.DefaultBackboardLineColour,
                    LineThickness = SpacePlanningImportHelper.GetDefaultLineThickness(ExtractedPlanogramLengthUnitOfMeasureType),
                    MerchandisingType = (Byte) PlanogramSubComponentMerchandisingType.None
                };
            if (!planogramCacheItem.ExtractValue<Boolean>(ProSpaceImportHelper.PlanogramDrawNotches)) return dto;

            //  Notch information if applicable.
            SetNotchValues(dto, planogramCacheItem);
            return dto;
        }

        private void SetNotchValues(PlanogramSubComponentDto dto, DalCacheItem planogramCacheItem)
        {
            var notchWidth = planogramCacheItem.ExtractValue<Single>(ProSpaceImportHelper.PlanogramNotchWidth);
            dto.NotchStartX = 0;
            dto.NotchStartY = planogramCacheItem.ExtractValue<Single>(ProSpaceImportHelper.PlanogramNotchOffset);
            dto.NotchSpacingY = planogramCacheItem.ExtractValue<Single>(ProSpaceImportHelper.PlanogramNotchSpacing);
            dto.NotchWidth = notchWidth;
            dto.NotchHeight = SpacePlanningImportHelper.GetDefaultNotchHeight(ExtractedPlanogramLengthUnitOfMeasureType);
            dto.IsNotchPlacedOnFront = planogramCacheItem.ExtractValue<Boolean>(ProSpaceImportHelper.PlanogramDrawNotches);
            if (!planogramCacheItem.ExtractValue<Boolean>(ProSpaceImportHelper.PlanogramDoubleNotches))
                dto.NotchStyleType = (Byte) PlanogramSubComponentNotchStyleType.SingleRectangle;
            else
            {
                dto.NotchStyleType = (Byte) PlanogramSubComponentNotchStyleType.DoubleRectangle;
                //  NB Apparently JDA's double notches do not have Spacing X.
                dto.NotchSpacingX = 0; //SpacePlanningImportHelper.GetDefaultNotchSpacingX(ExtractedPlanogramLengthUnitOfMeasureType);
            }
        }

        private PlanogramSubComponentDto CreateBasePlanogramSubComponentDto()
        {
            Object baseId = GetBaseId();
            Int32 colour = SpacePlanningImportHelper.DecodeColour(_parent.ExtractValue<Single>(ProSpaceImportHelper.PlanogramBaseColor));
            return new PlanogramSubComponentDto
                   {
                       Id = baseId,
                       PlanogramComponentId = baseId,
                       Name =  String.Format(Message.JdaImport_BaseDefaultNameMask, baseId),
                       Height = _parent.ExtractValue<Single>(ProSpaceImportHelper.PlanogramBaseHeight),
                       Width = ExtractValue<Single>(ProSpaceImportHelper.SegmentWidth),
                       Depth = DecodeBaseDepth(this, _parent),
                       X = 0.0F,
                       Y = 0.0F,
                       Z = 0.0F,
                       Slope = 0.0F,
                       Angle = 0.0F,
                       Roll = 0.0F,
                       ShapeType = (Byte)PlanogramSubComponentShapeType.Box,
                       IsVisible = true,
                       HasCollisionDetection = true,
                       FillPatternTypeFront = SpacePlanningImportHelper.DefaultFillPatternType,
                       FillPatternTypeBack = SpacePlanningImportHelper.DefaultFillPatternType,
                       FillPatternTypeTop = SpacePlanningImportHelper.DefaultFillPatternType,
                       FillPatternTypeBottom = SpacePlanningImportHelper.DefaultFillPatternType,
                       FillPatternTypeLeft = SpacePlanningImportHelper.DefaultFillPatternType,
                       FillPatternTypeRight = SpacePlanningImportHelper.DefaultFillPatternType,
                       FillColourFront = colour,
                       FillColourBack = colour,
                       FillColourTop = colour,
                       FillColourBottom = colour,
                       FillColourLeft = colour,
                       FillColourRight = colour,
                       LineColour = SpacePlanningImportHelper.BaseDefaultLineColour,
                       LineThickness = SpacePlanningImportHelper.GetDefaultLineThickness(ExtractedPlanogramLengthUnitOfMeasureType),
                       MerchandisingType = (Byte) PlanogramSubComponentMerchandisingType.None
                   };
        }

        #endregion

        #region CustomAttributeData

        private CustomAttributeDataDto DecodeCustomAttributeDataDto()
        {
            //  Check that this is invoked for Planogram, Product and Fixture DAL cache items.
            if (!IsType(DalCacheItemType.Planogram) && !IsType(DalCacheItemType.Product) && !IsType(DalCacheItemType.Fixture))
            {
                Debug.Fail("DecodeCustomAttributeDataDto should be invoked only for Planogram, Product and Fixture DAL cache items.");
                return null;
            }

            //  Try fetching already existing data first.
            var customAttributeDataDto = InstanceCache.Get<CustomAttributeDataDto>();
            if (customAttributeDataDto != null) return customAttributeDataDto;

            //  Create the custom attribute data dto.
            customAttributeDataDto = new CustomAttributeDataDto
                                           {
                                               Id = Id,
                                               ParentType = (Byte) GetCustomAttributeDataParentType(CacheItemType),
                                               ParentId = Id
                                           };

            InstanceCache.Set(customAttributeDataDto);
            return customAttributeDataDto;
        }

        private List<CustomAttributeDataDto> DecodeCustomAttributeDataDtos()
        {
            var dtos = new List<CustomAttributeDataDto>();
            foreach (DalCacheItem item in _children.SelectMany(i => i.Value))
            {
                dtos.AddRange(item.DecodeCustomAttributeDataDtos());
            }
            if (!IsType(DalCacheItemType.Planogram) &&
                !IsType(DalCacheItemType.Product) &&
                !IsType(DalCacheItemType.Fixture)) return dtos;

            CustomAttributeDataDto dto = DecodeCustomAttributeDataDto();
            if (dto != null) dtos.Add(dto);

            return dtos;
        }

        #endregion

        #region PlanogramAnnotation

        private PlanogramAnnotationDto DecodePlanogramAnnotationDto()
        {
            //  Check that this is invoked for a Drawing DAL cache item.
            if (!IsType(DalCacheItemType.Drawing))
            {
                Debug.Fail("DecodePlanogramAnnotationDto should be invoked only for Drawing DAL cache items.");
                return null;
            }

            //  Try fetching already existing data first.
            var planogramAnnotationDto = InstanceCache.Get<PlanogramAnnotationDto>();
            if (planogramAnnotationDto != null) return planogramAnnotationDto;

            //  Process only Text type drawings.
            if ((ProSpaceDrawingType) ExtractValue<Int32>(ProSpaceImportHelper.DrawingType) == ProSpaceDrawingType.Text)
            {
                var fixtureItemDto = GetContainingSegmentCacheItem(this).GetDto<PlanogramFixtureItemDto>();
                PointValue origin = DecodeAnnotationCoordinates();

                Int32 fontColour = SpacePlanningImportHelper.DecodeColour(ExtractValue<Int32>(ProSpaceImportHelper.DrawingColor));
                Int32 backgroundColour = SpacePlanningImportHelper.DecodeColour(ExtractValue<Int32>(ProSpaceImportHelper.DrawingBackColor));
                if (Equals(fontColour, backgroundColour)) fontColour = SpacePlanningImportHelper.GetContrastColour(backgroundColour);
                planogramAnnotationDto =
                    new PlanogramAnnotationDto
                    {
                        Id = Id,
                        PlanogramId = fixtureItemDto.PlanogramId,
                        PlanogramFixtureItemId = fixtureItemDto.Id,
                        AnnotationType = (Byte) PlanogramAnnotationType.TextBox,
                        X = origin.X,
                        Y = origin.Y,
                        Z = origin.Z,
                        Height = Math.Abs(ExtractValue<Single>(ProSpaceImportHelper.DrawingHeight)),
                        Width = ExtractValue<Single>(ProSpaceImportHelper.DrawingWidth),
                        Depth = ExtractValue<Single>(ProSpaceImportHelper.DrawingDepth),
                        Text = ExtractValue<String>(ProSpaceImportHelper.DrawingString),
                        FontColour = fontColour,
                        BackgroundColour = backgroundColour,
                        CanReduceFontToFit = (ProSpaceScaleType) ExtractValue<Int32>(ProSpaceImportHelper.DrawingScale) != ProSpaceScaleType.Fixed,
                        FontName = ExtractValue<String>(ProSpaceImportHelper.DrawingFontFaceName),
                        FontSize = ExtractValue<Single>(ProSpaceImportHelper.DrawingFontHeight),
                        BorderColour = fontColour,
                        BorderThickness = 0.3F
                    };
            }

            InstanceCache.Set(planogramAnnotationDto);
            return planogramAnnotationDto;
        }


        private void EncodePlanogramAnnotation()
        {
            Object objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramComponentDto), out objectDto);
            PlanogramComponentDto componentDto = objectDto as PlanogramComponentDto;

            objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramFixtureItemDto), out objectDto);
            PlanogramFixtureItemDto fixtureItemDto = objectDto as PlanogramFixtureItemDto;

            objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramFixtureComponentDto), out objectDto);
            PlanogramFixtureComponentDto fixtureComponentDto = objectDto as PlanogramFixtureComponentDto;

            objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramFixtureAssemblyDto), out objectDto);
            PlanogramFixtureAssemblyDto fixutreAssemblyDto = objectDto as PlanogramFixtureAssemblyDto;

            objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramAssemblyComponentDto), out objectDto);
            PlanogramAssemblyComponentDto assemblyComponentDto = objectDto as PlanogramAssemblyComponentDto;

            objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramSubComponentDto), out objectDto);
            PlanogramSubComponentDto subComponentDto = objectDto as PlanogramSubComponentDto;

            objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramAnnotationDto), out objectDto);
            PlanogramAnnotationDto annotationDto = objectDto as PlanogramAnnotationDto;

            #region Postion XYZ
            Single x = 0, y = 0, z = 0;
            DalCacheItem segment = null;

            if (fixtureComponentDto != null)
            {

                x = fixtureComponentDto.X + subComponentDto.X + annotationDto.X ?? 0;
                y = fixtureComponentDto.Y + subComponentDto.Y + annotationDto.Y ?? 0;
                z = fixtureComponentDto.Z + subComponentDto.Z + annotationDto.Z ?? 0;

                segment = this._dalCache.GetSegmentCacheItem(fixtureComponentDto.PlanogramFixtureId.ToString());
            }
            else if (assemblyComponentDto != null && fixutreAssemblyDto != null)
            {
                x = fixutreAssemblyDto.X + assemblyComponentDto.X + subComponentDto.X + annotationDto.X ?? 0;
                y = fixutreAssemblyDto.Y + assemblyComponentDto.Y + subComponentDto.Y + annotationDto.Y ?? 0;
                z = fixutreAssemblyDto.Z + assemblyComponentDto.Z + subComponentDto.Z + annotationDto.Z ?? 0;

                segment = this._dalCache.GetSegmentCacheItem(fixutreAssemblyDto.PlanogramFixtureId.ToString());
            }
            else
            {
                x = annotationDto.X ?? 0;
                y = annotationDto.Y ?? 0;
                z = annotationDto.Z ?? 0;
            }

            
            if (fixtureItemDto != null)
            {
                x += fixtureItemDto.X;
                y += fixtureItemDto.Y;
                z += fixtureItemDto.Z;
            }

            InsertValue(ProSpaceImportHelper.DrawingX, x);
            InsertValue(ProSpaceImportHelper.DrawingY, y);
            InsertValue(ProSpaceImportHelper.DrawingZ, z);
            #endregion
            InsertValue(ProSpaceImportHelper.DrawingType, (Int32)ProSpaceDrawingType.Text);
            InsertValue(ProSpaceImportHelper.DrawingHeight, annotationDto.Height);
            InsertValue(ProSpaceImportHelper.DrawingWidth, annotationDto.Width);
            InsertValue(ProSpaceImportHelper.DrawingDepth, annotationDto.Depth);
            InsertValue(ProSpaceImportHelper.DrawingString, annotationDto.Text.Replace(Environment.NewLine, @"\r\n"));
            InsertValue(ProSpaceImportHelper.DrawingColor, DecodeColour(annotationDto.FontColour));
            InsertValue(ProSpaceImportHelper.DrawingBackColor, DecodeColour(annotationDto.BackgroundColour));
            InsertValue(ProSpaceImportHelper.DrawingScale, (Int32)ProSpaceScaleType.Sized);            
            InsertValue(ProSpaceImportHelper.DrawingFontFaceName, annotationDto.FontName);
            InsertValue(ProSpaceImportHelper.DrawingFontHeight, annotationDto.FontSize);
                              
        }
        #endregion

        #region PlanogramPerformance

        private void EncodePlanogramPerformance()
        {
            Object objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramProductDto), out objectDto);
            PlanogramProductDto productDto = objectDto as PlanogramProductDto;
            
            objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramPerformanceDataDto), out objectDto);
            PlanogramPerformanceDataDto performanceDto = objectDto as PlanogramPerformanceDataDto;

            if (productDto == null || performanceDto == null) return;

            InsertValue(ProSpaceImportHelper.PerformanceUpc, productDto.Gtin);
            InsertValue(ProSpaceImportHelper.PerformanceId, performanceDto.Id);
            InsertValue(ProSpaceImportHelper.PerformanceCaseCost, productDto.CaseCost);
            InsertValue(ProSpaceImportHelper.PerformancePrice, productDto.SellPrice);
            InsertValue(ProSpaceImportHelper.PerformanceUnitCost, productDto.CostPrice);
            InsertValue(ProSpaceImportHelper.PerformanceDaysSupply, performanceDto.AchievedDos);
            InsertValue(ProSpaceImportHelper.PerformanceCaseMultiple, performanceDto.AchievedCasePacks);
            InsertValue(ProSpaceImportHelper.PerformanceUnitMovement, performanceDto.UnitsSoldPerDay);
            InsertValue(ProSpaceImportHelper.PerformanceMinimumUnits, performanceDto.MinimumInventoryUnits);
            
            foreach (var mapping in this._dalCache.ExportMappingContext.MetricMappings)
            {
                PropertyInfo property = typeof(PlanogramPerformanceDataDto).GetProperty(String.Format("P{0}", mapping.MetricId));

                if (property == null) continue;
                InsertValue(mapping.Source, property.GetValue(performanceDto, null));
            }

        }

        private PlanogramPerformanceDto DecodePlanogramPerformanceDto()
        {
            //  Check that this is invoked for a Planogram DAL cache item.
            if (!IsType(DalCacheItemType.Planogram))
            {
                Debug.Fail("DecodePlanogramPerformanceDto should be invoked only for Planogram DAL cache items.");
                return null;
            }

            //  Try fetching already existing data first.
            var planogramPerformanceDto = InstanceCache.Get<PlanogramPerformanceDto>();
            if (planogramPerformanceDto != null) return planogramPerformanceDto;

            planogramPerformanceDto =
                new PlanogramPerformanceDto
                {
                    Id = Id,
                    PlanogramId = Id,
                    Name = Message.PlanogramPerformance_Name_Default
                };

            InstanceCache.Set(planogramPerformanceDto);
            return planogramPerformanceDto;
        }

        private List<PlanogramPerformanceDataDto> DecodePlanogramPerformanceDataDto()
        {
            //  Check that this is invoked for a Product DAL cache item.
            if (!IsType(DalCacheItemType.Product))
            {
                Debug.Fail("DecodePlanogramPerformanceDataDto should be invoked only for Product DAL cache items.");
                return null;
            }

            //  Try fetching already existing data first.
            var dtos = InstanceCache.Get<List<PlanogramPerformanceDataDto>>();
            if (dtos != null) return dtos;

            //  Create the planogram product dtos for each of the existing planograms.
            List<DalCacheItem> planogramCacheItems = _parent.Children[DalCacheItemType.Planogram];
            dtos = planogramCacheItems.Select(CreatePlanogramPerformanceDataDto).ToList();

            InstanceCache.Set(dtos);
            return dtos;
        }

        private PlanogramPerformanceDataDto CreatePlanogramPerformanceDataDto(DalCacheItem item)
        {
            //  Check that this is invoked for a Product DAL cache item.
            if (!IsType(DalCacheItemType.Product))
            {
                Debug.Fail("CreatePlanogramProductDto should be invoked only for Product DAL cache items.");
                return null;
            }

            //  Try fetching the template performance data first and cloning it.
            var dto = InstanceCache.Get<PlanogramPerformanceDataDto>();
            String planogramPerformanceId = item.Id;
            if (dto != null) return ClonePlanogramPerformanceDataDto(dto, planogramPerformanceId);

            dto =
                new PlanogramPerformanceDataDto
                {
                    Id = Id,
                    PlanogramPerformanceId = planogramPerformanceId,
                    PlanogramProductId = Id
                };

            //  Apply metrics from each possible source.
            DalCacheItem performanceCacheItem;
            item.PerformanceCacheItems.TryGetValue(NewProductKey(this), out performanceCacheItem);
            if (performanceCacheItem != null)
            {
                performanceCacheItem.ApplyMetricMapping(dto, _dalCache.MetricMappings);
            }
            InstanceCache.Set(dto);
            return dto;
        }

        private static PlanogramPerformanceDataDto ClonePlanogramPerformanceDataDto(PlanogramPerformanceDataDto dto, Object planogramPerformanceId)
        {
            PlanogramPerformanceDataDto planogramProductDto = SpacePlanningImportHelper.Clone(dto);
            planogramProductDto.PlanogramPerformanceId = planogramPerformanceId;
            return planogramProductDto;
        }

        /// <summary>
        ///     Decodes <see cref="PlanogramPerformanceMetricDto" /> information from a Planogram entry in a ProSpace file.
        /// </summary>
        /// <returns>A <c>DTO</c> with the data for this entry's <c>Planogram Performance Metric</c>.</returns>
        /// <remarks>
        ///     The <c>DTO</c> is lazy created, and it will be retrieved from the cache once created until the cache is cleared.
        ///     <para />
        ///     If no parent <c>Planogram Performance</c> is found, <c>null</c> will be returned, without caching the result.
        /// </remarks>
        private List<PlanogramPerformanceMetricDto> DecodePlanogramPerformanceMetricDto()
        {
            //  Check that this is invoked for a Planogram DAL cache item.
            if (!IsType(DalCacheItemType.Planogram))
            {
                Debug.Fail("DecodePlanogramPerformanceMetricDto should be invoked only for Planogram DAL cache items.");
                return null;
            }

            //  Try fetching already existing data first.
            var dtos = InstanceCache.Get<List<PlanogramPerformanceMetricDto>>();
            if (dtos != null) return dtos;

            //  Get the Planogram Performance Dto for this Planogram Performance Metric Dto.
            var planogramPerformanceDto = GetDto<PlanogramPerformanceDto>();
            if (planogramPerformanceDto == null) return null;

            dtos = _dalCache.MetricMappings.Select(CreatePlanogramPerformanceMetricDto).ToList();
            InstanceCache.Set(dtos);
            return dtos;
        }

        /// <summary>
        ///     Create a <see cref="PlanogramPerformanceMetricDto"/> from the given <paramref name="mapping"/>.
        /// </summary>
        /// <param name="mapping">The values mapped for a specific <c>Planogram Performance Metric</c>.</param>
        /// <returns>A new instance of <see cref="PlanogramPerformanceMetricDto"/> with data extracted from the <paramref name="mapping"/>.</returns>
        private PlanogramPerformanceMetricDto CreatePlanogramPerformanceMetricDto(PlanogramMetricMappingDto mapping)
        {
            return new PlanogramPerformanceMetricDto
                   {
                       Id = IdentityHelper.GetNextInt32(),
                       PlanogramPerformanceId = Id,
                       Name = mapping.Name,
                       Description = mapping.Description,
                       MetricId = mapping.MetricId,
                       Direction = mapping.Direction,
                       MetricType = mapping.MetricType,
                       SpecialType = mapping.SpecialType,
                       AggregationType = mapping.AggregationType
                   };
        }

        private DalCacheItem GetPlanogramProductDtoForPerformance()
        {
            //  Check that this is invoked for a Performance DAL cache item.
            if (!IsType(DalCacheItemType.Performance))
            {
                Debug.Fail("GetPlanogramProductDtoForPerformance should be invoked only for Performance DAL cache items.");
                return null;
            }

            Object productKey = NewPerformanceProductKey(this);
            DalCacheItem match = GetProductCacheItems().FirstOrDefault(i => Equals(NewProductKey(i), productKey));
            if (match == null)
            {
                Debug.Fail("GetPlanogramProductDtoForPerformance could not find the corresponding product for the planogram performance.");
                return null;
            }

            return match;
        }

        #endregion

        #region Planogram Inventory

        private PlanogramInventoryDto DecodePlanogramInventoryDto()
        {
            //  Check that this is invoked for a Planogram DAL cache item.
            if (!IsType(DalCacheItemType.Planogram))
            {
                Debug.Fail("DecodePlanogramInventoryDto should be invoked only for Planogram DAL cache items.");
                return null;
            }

            //  Try fetching already existing data first.
            var dto = InstanceCache.Get<PlanogramInventoryDto>();
            if (dto != null) return dto;

            //  Determine whether to use Project or Planogram inventory.
            var isPlanogramInventory = ExtractValue<Boolean>(ProSpaceImportHelper.ProjectPlanogramSpecificInventory);
            dto = new PlanogramInventoryDto
            {
                Id = IdentityHelper.GetNextInt32(),
                PlanogramId = Id,
                InventoryMetricType = (Byte) PlanogramInventoryMetricType.RegularSalesUnits,
                DaysOfPerformance = _parent.ExtractValue<Single>(ProSpaceImportHelper.ProjectMovementPeriod),
                MinCasePacks = DecodeMinCasePacks(isPlanogramInventory),
                IsCasePacksValidated = DecodeIsCasePacksValidated(isPlanogramInventory),
                MinDos = DecodeMinDos(isPlanogramInventory),
                IsDosValidated = DecodeIsDosValidated(isPlanogramInventory)
            };

            InstanceCache.Set(dto);
            return dto;
        }

        private Boolean DecodeIsDosValidated(Boolean isPlanogramInventory)
        {
            //  Check that this is invoked for a Planogram DAL cache item.
            if (!IsType(DalCacheItemType.Planogram))
            {
                Debug.Fail("DecodeIsDosValidated should be invoked only for Planogram DAL cache items.");
                return false;
            }

            //  Determine the source of the value.
            return isPlanogramInventory
                       ? ExtractValue<Boolean>(ProSpaceImportHelper.PlanogramInventoryDaysSupply)
                       : _parent.ExtractValue<Boolean>(ProSpaceImportHelper.ProjectInventoryModelDaysOfSupply);
        }

        private Single DecodeMinDos(Boolean isPlanogramInventory)
        {
            //  Check that this is invoked for a Planogram DAL cache item.
            if (!IsType(DalCacheItemType.Planogram))
            {
                Debug.Fail("DecodeMinDos should be invoked only for Planogram DAL cache items.");
                return 0.0F;
            }

            //  Determine the source of the value.
            return isPlanogramInventory
                       ? ExtractValue<Single>(ProSpaceImportHelper.PlanogramDaysSupply)
                       : _parent.ExtractValue<Single>(ProSpaceImportHelper.ProjectDaysOfSupply);
        }

        private Boolean DecodeIsCasePacksValidated(Boolean isPlanogramInventory)
        {
            //  Check that this is invoked for a Planogram DAL cache item.
            if (!IsType(DalCacheItemType.Planogram))
            {
                Debug.Fail("DecodeIsCasePacksValidated should be invoked only for Planogram DAL cache items.");
                return false;
            }

            //  Determine the source of the value.
            return isPlanogramInventory
                       ? ExtractValue<Boolean>(ProSpaceImportHelper.PlanogramInventoryCaseMultiple)
                       : _parent.ExtractValue<Boolean>(ProSpaceImportHelper.ProjectInventoryModelCaseMultiple);
        }

        private Single DecodeMinCasePacks(Boolean isPlanogramInventory)
        {
            //  Check that this is invoked for a Planogram DAL cache item.
            if (!IsType(DalCacheItemType.Planogram))
            {
                Debug.Fail("DecodeMinCasePacks should be invoked only for Planogram DAL cache items.");
                return 0.0F;
            }

            //  Determine the source of the value.
            return isPlanogramInventory
                       ? ExtractValue<Single>(ProSpaceImportHelper.PlanogramCaseMultiple)
                       : _parent.ExtractValue<Single>(ProSpaceImportHelper.ProjectCaseMultiple);
        }

        #endregion

        #endregion

        #region Export Encoders

        private Boolean EncodePSAFixture()
        {
            Object objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramComponentDto), out objectDto);
            PlanogramComponentDto componentDto = objectDto as PlanogramComponentDto;

            objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramFixtureComponentDto), out objectDto);
            PlanogramFixtureComponentDto fixtureComponentDto = objectDto as PlanogramFixtureComponentDto;

            
            objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramFixtureAssemblyDto), out objectDto);
            PlanogramFixtureAssemblyDto fixutreAssemblyDto = objectDto as PlanogramFixtureAssemblyDto;

            objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramAssemblyComponentDto), out objectDto);
            PlanogramAssemblyComponentDto assemblyComponentDto = objectDto as PlanogramAssemblyComponentDto;

            objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramSubComponentDto), out objectDto);
            PlanogramSubComponentDto subComponentDto = objectDto as PlanogramSubComponentDto;

            if (subComponentDto == null || (fixtureComponentDto == null && (assemblyComponentDto == null || fixutreAssemblyDto == null)) || componentDto == null) return false;

            ProSpaceComponentType componentType = EncodeProSpaceComponentType(subComponentDto);
            InsertValue(ProSpaceImportHelper.FixtureType, (Int32)componentType); //TODO
            InsertValue(ProSpaceImportHelper.FixtureName, componentDto.Name);
            InsertValue(ProSpaceImportHelper.FixtureHeight, subComponentDto.Height);
            InsertValue(ProSpaceImportHelper.FixtureWidth, subComponentDto.Width);
            InsertValue(ProSpaceImportHelper.FixtureDepth, subComponentDto.Depth);
            Single x = 0, y = 0, z = 0;
            DalCacheItem segment = null;
            if (fixtureComponentDto != null)
            {
                InsertValue(ProSpaceImportHelper.FixtureAssembly, String.Format("{0}-{1}", componentDto.Name, _dalCache.GetFriendlyComponentId(componentDto.Id)));
                //this is unlikely to work very well in all situations
                InsertValue(ProSpaceImportHelper.FixtureSlope, ConvertToDegrees(fixtureComponentDto.Slope + subComponentDto.Slope));
                InsertValue(ProSpaceImportHelper.FixtureAngle, ConvertToDegrees(fixtureComponentDto.Angle + subComponentDto.Angle));
                InsertValue(ProSpaceImportHelper.FixtureRoll, ConvertToDegrees(fixtureComponentDto.Roll + subComponentDto.Roll));

                InsertValue(ProSpaceImportHelper.FixturePositionId, fixtureComponentDto.ComponentSequenceNumber);

                x = fixtureComponentDto.X + subComponentDto.X;
                y = fixtureComponentDto.Y + subComponentDto.Y;
                z = fixtureComponentDto.Z + subComponentDto.Z;

                segment = this._dalCache.GetSegmentCacheItem(fixtureComponentDto.PlanogramFixtureId.ToString());
            }
            else if (assemblyComponentDto != null && fixutreAssemblyDto != null)
            {
                InsertValue(ProSpaceImportHelper.FixtureAssembly, String.Format(Language.AssemblyName, _dalCache.GetFriendlyAssemblyId(fixutreAssemblyDto.Id)));
                //this is unlikely to work very well in all situations
                InsertValue(ProSpaceImportHelper.FixtureSlope, ConvertToDegrees(fixutreAssemblyDto.Slope + assemblyComponentDto.Slope + subComponentDto.Slope));
                InsertValue(ProSpaceImportHelper.FixtureAngle, ConvertToDegrees(fixutreAssemblyDto.Angle + assemblyComponentDto.Angle + subComponentDto.Angle));
                InsertValue(ProSpaceImportHelper.FixtureRoll, ConvertToDegrees(fixutreAssemblyDto.Roll + assemblyComponentDto.Roll + subComponentDto.Roll));

                InsertValue(ProSpaceImportHelper.FixturePositionId, assemblyComponentDto.ComponentSequenceNumber);

                x = fixutreAssemblyDto.X + assemblyComponentDto.X + subComponentDto.X;
                y = fixutreAssemblyDto.Y + assemblyComponentDto.Y + subComponentDto.Y;
                z = fixutreAssemblyDto.Z + assemblyComponentDto.Z + subComponentDto.Z;

                segment = this._dalCache.GetSegmentCacheItem(fixutreAssemblyDto.PlanogramFixtureId.ToString());
            }
            Object output = null;

            

            if (segment != null && segment.Dtos.TryGetValue(typeof(PlanogramFixtureItemDto), out output))
            {
                PlanogramFixtureItemDto fixtureItemDto = output as PlanogramFixtureItemDto;
                x += fixtureItemDto.X;
                y += fixtureItemDto.Y;
                z += fixtureItemDto.Z;
            }
            InsertValue(ProSpaceImportHelper.FixtureX, x);
            InsertValue(ProSpaceImportHelper.FixtureY, y);
            InsertValue(ProSpaceImportHelper.FixtureZ, z);

            InsertValue(ProSpaceImportHelper.FixtureColour, DecodeColour(subComponentDto.FillColourFront));
            InsertValue(ProSpaceImportHelper.FixtureFillPattern, (Int32)EncodeProSpaceFillPatternType((PlanogramSubComponentFillPatternType)subComponentDto.FillPatternTypeFront));
            InsertValue(ProSpaceImportHelper.FixtureMerch, EncodePSAFixtureMerch(componentType, subComponentDto));

            PlanogramSubComponentXMerchStrategyType strategyX = (PlanogramSubComponentXMerchStrategyType)subComponentDto.MerchandisingStrategyX;
            PlanogramSubComponentYMerchStrategyType strategyY = (PlanogramSubComponentYMerchStrategyType)subComponentDto.MerchandisingStrategyY;
            PlanogramSubComponentZMerchStrategyType strategyZ = (PlanogramSubComponentZMerchStrategyType)subComponentDto.MerchandisingStrategyZ;

            #region  Stacking checks
            //Apparently we are just going to let prospace have a fit with stacking in multiple directions
            //poor prospace :(
            //Boolean xStacked = (strategyX == PlanogramSubComponentXMerchStrategyType.LeftStacked ||
            //    strategyX == PlanogramSubComponentXMerchStrategyType.RightStacked);
            //Boolean yStacked = (strategyY == PlanogramSubComponentYMerchStrategyType.BottomStacked ||
            //    strategyY == PlanogramSubComponentYMerchStrategyType.TopStacked);
            //Boolean zStacked = (strategyZ == PlanogramSubComponentZMerchStrategyType.FrontStacked ||
            //   strategyZ == PlanogramSubComponentZMerchStrategyType.BackStacked);

            ////prospace can't handle stacking in two directions so we give a warning
            //if ((xStacked && (yStacked || zStacked)) ||
            //    (yStacked && (xStacked || zStacked)) ||
            //        (zStacked && (yStacked || xStacked)))
            //{
            //    this._dalCache.LogWarning(Language.ExportLog_MerchandisingWarning, String.Format(Language.ExportLog_MerchandisingWarning_MultiStack, componentDto.Name), PlanogramEventLogAffectedType.Components);
            //}
            //if (xStacked && (yStacked || zStacked))
            //{
            //    strategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            //}
            //if (yStacked && (xStacked || zStacked))
            //{
            //    strategyY = PlanogramSubComponentYMerchStrategyType.Manual;
            //}
            //if (zStacked && (yStacked || xStacked))
            //{
            //    strategyZ = PlanogramSubComponentZMerchStrategyType.Manual;
            //}
            #endregion

            InsertValue(ProSpaceImportHelper.FixtureMerchXPlacement, (Int32)EncodeMerchPlacementX(strategyX));
            InsertValue(ProSpaceImportHelper.FixtureMerchYPlacement, (Int32)EncodeMerchPlacementY(strategyY));
            InsertValue(ProSpaceImportHelper.FixtureMerchZPlacement, (Int32)EncodeMerchPlacementZ(strategyZ));
            
            InsertValue(ProSpaceImportHelper.FixtureMerchXNumber, 1); //We set the number of units wide
            InsertValue(ProSpaceImportHelper.FixtureMerchYNumber, 1); //We set the number of units high
            InsertValue(ProSpaceImportHelper.FixtureMerchZNumber, 1); //We set the number of units deep

            InsertValue(ProSpaceImportHelper.FixtureMerchXDirection, EncodeMerchDirectionX((PlanogramSubComponentXMerchStrategyType)subComponentDto.MerchandisingStrategyX));
            InsertValue(ProSpaceImportHelper.FixtureMerchYDirection, EncodeMerchDirectionY((PlanogramSubComponentYMerchStrategyType)subComponentDto.MerchandisingStrategyY));
            InsertValue(ProSpaceImportHelper.FixtureMerchZDirection, EncodeMerchDirectionZ((PlanogramSubComponentZMerchStrategyType)subComponentDto.MerchandisingStrategyZ));

            if (subComponentDto.IsProductSqueezeAllowed)
            {
                InsertValue(ProSpaceImportHelper.FixtureMerchXSize, (Int32)ProSpaceMerchSizeType.ExpandSqueeze);
                InsertValue(ProSpaceImportHelper.FixtureMerchYSize, (Int32)ProSpaceMerchSizeType.ExpandSqueeze);
                InsertValue(ProSpaceImportHelper.FixtureMerchZSize, (Int32)ProSpaceMerchSizeType.ExpandSqueeze);
            }
            else
            {
                InsertValue(ProSpaceImportHelper.FixtureMerchXSize, (Int32)ProSpaceMerchSizeType.Normal);
                InsertValue(ProSpaceImportHelper.FixtureMerchYSize, (Int32)ProSpaceMerchSizeType.Normal);
                InsertValue(ProSpaceImportHelper.FixtureMerchZSize, (Int32)ProSpaceMerchSizeType.Normal);
            }
            //  Encode type unique values.
            switch (componentType)
            {
                case ProSpaceComponentType.Shelf:
                case ProSpaceComponentType.PolygonalShelf:
                    if (subComponentDto.IsRiserPlacedOnFront)
                    {
                        InsertValue(ProSpaceImportHelper.FixtureGrilleHeight, subComponentDto.RiserHeight);
                    }
                    EncodeOverhangValues(subComponentDto);
                    EncodeDividerValues(subComponentDto);

                    InsertValue(ProSpaceImportHelper.FixtureCanCombine, (Byte)((PlanogramSubComponentCombineType)subComponentDto.CombineType).ToFixtureCombineType());                  
                    break;
                case ProSpaceComponentType.Chest:
                case ProSpaceComponentType.Bin:
                    EncodeFaceThicknessValues(subComponentDto);
                    EncodeDividerValues(subComponentDto, true);
                    InsertValue(ProSpaceImportHelper.FixtureCanCombine, (Byte)((PlanogramSubComponentCombineType)subComponentDto.CombineType).ToFixtureCombineType());
                    break;
                case ProSpaceComponentType.Bar:
                    EncodeOverhangValues(subComponentDto);
                    //No peg values for this in prospace?????
                    break;
                case ProSpaceComponentType.Pegboard:
                    EncodeOverhangValues(subComponentDto);
                    EncodeMerchConstraint1Values(subComponentDto);
                    this._dalCache.LogWarning(Language.ExportLog_MerchandisingWarning, String.Format(Language.ExportLog_MerchandisingWarning_GapDefaults, componentDto.Name), PlanogramEventLogAffectedType.Components);
                    break;
                case ProSpaceComponentType.MultiRowPegboard:
                    EncodeOverhangValues(subComponentDto);
                    EncodeMerchConstraint1Values(subComponentDto);
                    EncodeMerchConstraint2Values(subComponentDto);
                    this._dalCache.LogWarning(Language.ExportLog_MerchandisingWarning, String.Format(Language.ExportLog_MerchandisingWarning_GapDefaults, componentDto.Name), PlanogramEventLogAffectedType.Components);
                    break;
                case ProSpaceComponentType.Rod:
                    break;
                case ProSpaceComponentType.LateralRod:
                case ProSpaceComponentType.CurvedRod:
                case ProSpaceComponentType.Obstruction:
                case ProSpaceComponentType.Sign:
                case ProSpaceComponentType.GravityFeed:
                    break;
                default:
                    Debug.Fail("Unknown ProSpaceComponentType when calling DecodePlanogramSubComponentDto.");
                    break;
            }

            ApplyExportMappings(componentDto, componentDto.Id, this._dalCache.ExportMappingContext.ComponentMappings, CustomAttributeDataPlanogramParentType.PlanogramComponent);


            return true;
        }

        private Int32 EncodeMerchDirectionZ(PlanogramSubComponentZMerchStrategyType merchtype)
        {
            switch (merchtype)
            {
                case PlanogramSubComponentZMerchStrategyType.Back:
                case PlanogramSubComponentZMerchStrategyType.BackStacked:
                    return 0;
                case PlanogramSubComponentZMerchStrategyType.Front:
                case PlanogramSubComponentZMerchStrategyType.FrontStacked:
                    return 1;
                case PlanogramSubComponentZMerchStrategyType.Even:
                case PlanogramSubComponentZMerchStrategyType.Manual:
                default:
                    return -1;
            }
        }
        private Int32 EncodeMerchDirectionX(PlanogramSubComponentXMerchStrategyType merchtype)
        {
            switch (merchtype)
            {
                case PlanogramSubComponentXMerchStrategyType.Left:
                case PlanogramSubComponentXMerchStrategyType.LeftStacked:
                    return 0;
                case PlanogramSubComponentXMerchStrategyType.Right:
                case PlanogramSubComponentXMerchStrategyType.RightStacked:
                    return 1;
                case PlanogramSubComponentXMerchStrategyType.Even:
                case PlanogramSubComponentXMerchStrategyType.Manual:
                default:
                    return -1;
            }
        }
        private Int32 EncodeMerchDirectionY(PlanogramSubComponentYMerchStrategyType merchtype)
        {
            switch (merchtype)
            {
                case PlanogramSubComponentYMerchStrategyType.Bottom:
                case PlanogramSubComponentYMerchStrategyType.BottomStacked:
                    return 0;
                case PlanogramSubComponentYMerchStrategyType.Top:
                case PlanogramSubComponentYMerchStrategyType.TopStacked:
                    return 1;
                case PlanogramSubComponentYMerchStrategyType.Even:
                case PlanogramSubComponentYMerchStrategyType.Manual:
                default:
                    return -1;
            }
        }

        private ProSpaceMerchPlacementType EncodeMerchPlacementZ(PlanogramSubComponentZMerchStrategyType merchtype)
        {
            switch (merchtype)
            {
                case PlanogramSubComponentZMerchStrategyType.Back:
                case PlanogramSubComponentZMerchStrategyType.Front:
                    return ProSpaceMerchPlacementType.Edge;
                case PlanogramSubComponentZMerchStrategyType.FrontStacked:
                case PlanogramSubComponentZMerchStrategyType.BackStacked:
                    return ProSpaceMerchPlacementType.Stacked;
                case PlanogramSubComponentZMerchStrategyType.Even:
                    return ProSpaceMerchPlacementType.Spread;
                case PlanogramSubComponentZMerchStrategyType.Manual:
                    return ProSpaceMerchPlacementType.Manual;
                default:
                    return ProSpaceMerchPlacementType.Default;
            }
        }
        private ProSpaceMerchPlacementType EncodeMerchPlacementX(PlanogramSubComponentXMerchStrategyType merchtype)
        {
            switch (merchtype)
            {
                case PlanogramSubComponentXMerchStrategyType.Left:
                case PlanogramSubComponentXMerchStrategyType.Right:
                    return ProSpaceMerchPlacementType.Edge;
                case PlanogramSubComponentXMerchStrategyType.LeftStacked:
                case PlanogramSubComponentXMerchStrategyType.RightStacked:
                    return ProSpaceMerchPlacementType.Stacked;
                case PlanogramSubComponentXMerchStrategyType.Even:
                    return ProSpaceMerchPlacementType.Spread;
                case PlanogramSubComponentXMerchStrategyType.Manual:
                    return ProSpaceMerchPlacementType.Manual;
                default:
                    return ProSpaceMerchPlacementType.Default;
            }
        }
        private ProSpaceMerchPlacementType EncodeMerchPlacementY(PlanogramSubComponentYMerchStrategyType merchtype)
        {
            switch (merchtype)
            {
                case PlanogramSubComponentYMerchStrategyType.Top:
                case PlanogramSubComponentYMerchStrategyType.Bottom:
                    return ProSpaceMerchPlacementType.Edge;
                case PlanogramSubComponentYMerchStrategyType.TopStacked:
                case PlanogramSubComponentYMerchStrategyType.BottomStacked:
                    return ProSpaceMerchPlacementType.Stacked;
                case PlanogramSubComponentYMerchStrategyType.Even:
                    return ProSpaceMerchPlacementType.Spread;
                case PlanogramSubComponentYMerchStrategyType.Manual:
                    return ProSpaceMerchPlacementType.Manual;
                default:
                    return ProSpaceMerchPlacementType.Default;
            }
        }

        
        private void EncodeDividerValues(PlanogramSubComponentDto planogramSubComponentDto, Boolean isChest = false)
        {
            InsertValue(ProSpaceImportHelper.FixtureDividerAtStart, planogramSubComponentDto.IsDividerObstructionAtStart ? 1 : 0);
            InsertValue(ProSpaceImportHelper.FixtureDividerAtEnd, planogramSubComponentDto.IsDividerObstructionAtEnd ? 1 : 0);
            InsertValue(ProSpaceImportHelper.FixtureDividerWidth, planogramSubComponentDto.DividerObstructionWidth);
            InsertValue(ProSpaceImportHelper.FixtureDividerHeight, planogramSubComponentDto.DividerObstructionHeight);
            InsertValue(ProSpaceImportHelper.FixtureDividerDepth, planogramSubComponentDto.DividerObstructionDepth);
            InsertValue(ProSpaceImportHelper.FixtureXStart, planogramSubComponentDto.DividerObstructionStartX);
            InsertValue(ProSpaceImportHelper.FixtureYStart, isChest ? planogramSubComponentDto.DividerObstructionStartZ : planogramSubComponentDto.DividerObstructionStartY);
            InsertValue(ProSpaceImportHelper.FixtureXSpacing, planogramSubComponentDto.DividerObstructionSpacingX);
            InsertValue(ProSpaceImportHelper.FixtureYSpacing, isChest ? planogramSubComponentDto.DividerObstructionSpacingZ : planogramSubComponentDto.DividerObstructionSpacingY);
            InsertValue(ProSpaceImportHelper.FixtureDividerBetweenFacings, planogramSubComponentDto.IsDividerObstructionByFacing ? 1 : 0);
        }

        private void EncodeOverhangValues(PlanogramSubComponentDto planogramSubComponentDto)
        {
            InsertValue(ProSpaceImportHelper.FixtureLeftOverhang, planogramSubComponentDto.LeftOverhang);
            InsertValue(ProSpaceImportHelper.FixtureRightOverhang, planogramSubComponentDto.RightOverhang);
            InsertValue(ProSpaceImportHelper.FixtureFrontOverhang, planogramSubComponentDto.FrontOverhang);
            InsertValue(ProSpaceImportHelper.FixtureBackOverhang, planogramSubComponentDto.BackOverhang);
            InsertValue(ProSpaceImportHelper.FixtureUpperOverhang, planogramSubComponentDto.TopOverhang);
            InsertValue(ProSpaceImportHelper.FixtureLowerOverhang, planogramSubComponentDto.BottomOverhang);
        }

        public Single ConvertToDegrees(Single radians)
        {
            if (radians.EqualTo(0)) return 0;

            Single output = Convert.ToSingle(MathHelper.DegreesPerRadian * radians);
            if(output > 180)
            {
                output -= 360;
            }
            return output;
        }
        private void EncodeMerchConstraint1Values(PlanogramSubComponentDto planogramSubComponentDto)
        {
            InsertValue(ProSpaceImportHelper.FixtureXStart, planogramSubComponentDto.MerchConstraintRow1StartX);
            InsertValue(ProSpaceImportHelper.FixtureXSpacing, planogramSubComponentDto.MerchConstraintRow1SpacingX);
            InsertValue(ProSpaceImportHelper.FixtureYStart, planogramSubComponentDto.MerchConstraintRow1StartY);
            InsertValue(ProSpaceImportHelper.FixtureYSpacing, planogramSubComponentDto.MerchConstraintRow1SpacingY);
        }

        private void EncodeMerchConstraint2Values(PlanogramSubComponentDto planogramSubComponentDto)
        {
            InsertValue(ProSpaceImportHelper.FixtureXStart2, planogramSubComponentDto.MerchConstraintRow2StartX);
            InsertValue(ProSpaceImportHelper.FixtureXSpacing2, planogramSubComponentDto.MerchConstraintRow2SpacingX);
            InsertValue(ProSpaceImportHelper.FixtureYStart, planogramSubComponentDto.MerchConstraintRow2StartY);
            InsertValue(ProSpaceImportHelper.FixtureYSpacing, planogramSubComponentDto.MerchConstraintRow2SpacingY);
        }


        private void EncodeFaceThicknessValues(PlanogramSubComponentDto planogramSubComponentDto)
        {
            InsertValue(ProSpaceImportHelper.FixtureWallHeight, planogramSubComponentDto.Height - planogramSubComponentDto.FaceThicknessBottom);
            InsertValue(ProSpaceImportHelper.FixtureHeight, planogramSubComponentDto.FaceThicknessBottom);
            InsertValue(ProSpaceImportHelper.FixtureWallDepth, Math.Min(planogramSubComponentDto.FaceThicknessFront, planogramSubComponentDto.FaceThicknessBack));
            InsertValue(ProSpaceImportHelper.FixtureWallWidth, Math.Min(planogramSubComponentDto.FaceThicknessLeft, planogramSubComponentDto.FaceThicknessRight));
        }

        private Single EncodePSAFixtureMerch(ProSpaceComponentType type, PlanogramSubComponentDto dto)
        {
            switch (type)
            {
                case ProSpaceComponentType.Shelf:
                case ProSpaceComponentType.Chest:
                case ProSpaceComponentType.Bin:
                case ProSpaceComponentType.PolygonalShelf:
                case ProSpaceComponentType.Rod:
                case ProSpaceComponentType.LateralRod:
                case ProSpaceComponentType.CurvedRod:
                   return dto.MerchandisableHeight;                    
                case ProSpaceComponentType.Bar:
                case ProSpaceComponentType.Pegboard:
                case ProSpaceComponentType.MultiRowPegboard:
                   return dto.MerchandisableDepth;
                case ProSpaceComponentType.Obstruction:
                case ProSpaceComponentType.Sign:
                case ProSpaceComponentType.GravityFeed:
                default:
                   return 0;
            }
        }

        private ProSpaceFillPatternType EncodeProSpaceFillPatternType(PlanogramSubComponentFillPatternType source)
        {
            switch (source)
            {
                case PlanogramSubComponentFillPatternType.Border:
                    return ProSpaceFillPatternType.Border;
                case PlanogramSubComponentFillPatternType.Crosshatch:
                    return ProSpaceFillPatternType.Crosshatch;
                case PlanogramSubComponentFillPatternType.DiagonalDown:
                    return ProSpaceFillPatternType.DiagonalDown;
                case PlanogramSubComponentFillPatternType.DiagonalUp:
                    return ProSpaceFillPatternType.DiagonalUp;
                case PlanogramSubComponentFillPatternType.Dotted:
                    return ProSpaceFillPatternType.Dots;
                case PlanogramSubComponentFillPatternType.Horizontal:
                    return ProSpaceFillPatternType.HorizontalLines;
                case PlanogramSubComponentFillPatternType.Solid:
                    return ProSpaceFillPatternType.Solid;
                case PlanogramSubComponentFillPatternType.Vertical:
                    return ProSpaceFillPatternType.VerticalLines;

                default:
                    return ProSpaceFillPatternType.Solid;
            }
        }
        private ProSpaceFillPatternType EncodeProSpaceFillPatternType(PlanogramProductFillPatternType source)
        {
            switch (source)
            {
                case PlanogramProductFillPatternType.Border:
                    return ProSpaceFillPatternType.Border;
                case PlanogramProductFillPatternType.Crosshatch:
                    return ProSpaceFillPatternType.Crosshatch;
                case PlanogramProductFillPatternType.DiagonalDown:
                    return ProSpaceFillPatternType.DiagonalDown;
                case PlanogramProductFillPatternType.DiagonalUp:
                    return ProSpaceFillPatternType.DiagonalUp;
                case PlanogramProductFillPatternType.Dotted:
                    return ProSpaceFillPatternType.Dots;
                case PlanogramProductFillPatternType.Horizontal:
                    return ProSpaceFillPatternType.HorizontalLines;
                case PlanogramProductFillPatternType.Solid:
                    return ProSpaceFillPatternType.Solid;
                case PlanogramProductFillPatternType.Vertical:
                    return ProSpaceFillPatternType.VerticalLines;

                default:
                    return ProSpaceFillPatternType.Solid;
            }
        }

        

        private ProSpaceComponentType EncodeProSpaceComponentType(PlanogramSubComponentDto sub)
        {
            switch ((PlanogramSubComponentMerchandisingType)sub.MerchandisingType)
            {
                case PlanogramSubComponentMerchandisingType.Hang:
                    if (sub.MerchConstraintRow1SpacingX == 0 && sub.MerchConstraintRow1SpacingY == 0)
                    {
                        return ProSpaceComponentType.Bar;
                    }

                    if (sub.MerchConstraintRow2SpacingY > 0)
                    {
                        return ProSpaceComponentType.MultiRowPegboard;
                    }
                    return ProSpaceComponentType.Pegboard;

                case PlanogramSubComponentMerchandisingType.HangFromBottom:
                    return ProSpaceComponentType.Rod;
                case PlanogramSubComponentMerchandisingType.None:
                    return ProSpaceComponentType.Obstruction;
                case PlanogramSubComponentMerchandisingType.Stack:
                    if (sub.FaceThicknessBottom > 0)
                    {
                        return ProSpaceComponentType.Chest;
                    }
                    else
                    {
                        return ProSpaceComponentType.Shelf;
                    }
                default:
                    return ProSpaceComponentType.Obstruction;  
            }
        }


        private void EncodePSASegment()
        {
            Object objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramFixtureDto), out objectDto);
            PlanogramFixtureDto fixtureDto = objectDto as PlanogramFixtureDto;

            objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramFixtureItemDto), out objectDto);
            PlanogramFixtureItemDto fixtureItemDto = objectDto as PlanogramFixtureItemDto;

            Single width = fixtureDto.Width;
            PlanogramSubComponentDto baseDto = null;
            PlanogramSubComponentDto backBoardDto = null;
            DalCacheItem planItem = this._dalCache.GetPlanogramCacheItem(fixtureItemDto.PlanogramId.ToString());
            if (planItem != null && planItem.Children.ContainsKey(DalCacheItemType.Fixture))
            {
                DalCacheItem backBoardItem = planItem.Children[DalCacheItemType.Fixture]
                    .FirstOrDefault(f => f.IsBackBoard && f.Dtos.ContainsKey(typeof(PlanogramFixtureComponentDto)) &&
                        (f.Dtos[typeof(PlanogramFixtureComponentDto)] as PlanogramFixtureComponentDto).PlanogramFixtureId.Equals(fixtureDto.Id));
                if (backBoardItem != null)
                {
                    objectDto = null;
                    backBoardItem.Dtos.TryGetValue(typeof(PlanogramSubComponentDto), out objectDto);
                    backBoardDto = objectDto as PlanogramSubComponentDto;

                    if(backBoardDto.Width.GreaterThan(width))
                    {
                        width = backBoardDto.Width;
                        this._dalCache.LogWarning(Language.ExportLog_MerchandisingWarning, String.Format(Language.ExportLog_MerchandisingWarning_BaySizeIncrease_BackBoard, fixtureDto.Name), PlanogramEventLogAffectedType.Components);
                    }

                    

                }
                DalCacheItem baseItem = planItem.Children[DalCacheItemType.Fixture]
                    .FirstOrDefault(f => f.IsBase && f.Dtos.ContainsKey(typeof(PlanogramFixtureComponentDto)) &&
                        (f.Dtos[typeof(PlanogramFixtureComponentDto)] as PlanogramFixtureComponentDto).PlanogramFixtureId.Equals(fixtureDto.Id));
                if (baseItem != null)
                {
                    objectDto = null;
                    baseItem.Dtos.TryGetValue(typeof(PlanogramSubComponentDto), out objectDto);
                    baseDto = objectDto as PlanogramSubComponentDto;

                    if (baseDto.Width.GreaterThan(width))
                    {
                        width = baseDto.Width;
                        this._dalCache.LogWarning(Language.ExportLog_MerchandisingWarning, String.Format(Language.ExportLog_MerchandisingWarning_BaySizeIncrease_Base, fixtureDto.Name), PlanogramEventLogAffectedType.Components);
                    }
                }
            }

            InsertValue(ProSpaceImportHelper.SegmentAngle, ConvertToDegrees(fixtureItemDto.Angle));
            InsertValue(ProSpaceImportHelper.SegmentX, fixtureItemDto.X);
            InsertValue(ProSpaceImportHelper.SegmentY, fixtureItemDto.Y);
            InsertValue(ProSpaceImportHelper.SegmentZ, fixtureItemDto.Z);
            InsertValue(ProSpaceImportHelper.SegmentOffsetX, fixtureItemDto.X);
            InsertValue(ProSpaceImportHelper.SegmentOffsetY, fixtureItemDto.Y);
            InsertValue(ProSpaceImportHelper.SegmentHeight, fixtureDto.Height);
            InsertValue(ProSpaceImportHelper.SegmentWidth, width);
            InsertValue(ProSpaceImportHelper.SegmentDepth, fixtureDto.Depth);
            InsertValue(ProSpaceImportHelper.SegmentName, fixtureDto.Name);

            ApplyExportMappings(fixtureDto, fixtureDto.Id, this._dalCache.ExportMappingContext.FixtureMappings, CustomAttributeDataPlanogramParentType.Planogram);
        }

        private void EncodePlanogramProductDto(PlanogramProductDto dto)
        {
            InsertValue(ProSpaceImportHelper.ProductId, dto.Id);
            InsertValue(ProSpaceImportHelper.ProductUpc, dto.Gtin);
            InsertValue(ProSpaceImportHelper.ProductName, dto.Name);
            InsertValue(ProSpaceImportHelper.ProductHeight, dto.Height);
            InsertValue(ProSpaceImportHelper.ProductWidth, dto.Width);
            InsertValue(ProSpaceImportHelper.ProductDepth, dto.Depth);

            InsertValue(ProSpaceImportHelper.ProductDisplayHeight, dto.DisplayHeight);
            InsertValue(ProSpaceImportHelper.ProductDisplayWidth, dto.DisplayWidth);
            InsertValue(ProSpaceImportHelper.ProductDisplayDepth, dto.DisplayDepth);

            InsertValue(ProSpaceImportHelper.ProductTrayHeight, dto.TrayHeight);
            InsertValue(ProSpaceImportHelper.ProductTrayWidth, dto.TrayWidth);
            InsertValue(ProSpaceImportHelper.ProductTrayDepth, dto.TrayDepth);
            InsertValue(ProSpaceImportHelper.ProductTrayNumberHigh, dto.TrayHigh);
            InsertValue(ProSpaceImportHelper.ProductTrayNumberWide, dto.TrayWide);
            InsertValue(ProSpaceImportHelper.ProductTrayNumberDeep, dto.TrayDeep);
            InsertValue(ProSpaceImportHelper.ProductTrayTotalNumber, dto.TrayPackUnits);

            InsertValue(ProSpaceImportHelper.ProductCaseHeight, dto.CaseHeight);
            InsertValue(ProSpaceImportHelper.ProductCaseWidth, dto.CaseWidth);
            InsertValue(ProSpaceImportHelper.ProductCaseDepth, dto.CaseDepth);
            InsertValue(ProSpaceImportHelper.ProductCaseNumberHigh, dto.CaseHigh);
            InsertValue(ProSpaceImportHelper.ProductCaseNumberWide, dto.CaseWide);
            InsertValue(ProSpaceImportHelper.ProductCaseNumberDeep, dto.CaseDeep);
            InsertValue(ProSpaceImportHelper.ProductCaseTotalNumber, dto.CasePackUnits);

            InsertValue(ProSpaceImportHelper.ProductAlternateHeight, dto.AlternateHeight);
            InsertValue(ProSpaceImportHelper.ProductAlternateWidth, dto.AlternateWidth);
            InsertValue(ProSpaceImportHelper.ProductAlternateDepth, dto.AlternateDepth);

            InsertValue(ProSpaceImportHelper.ProductPegholes, dto.NumberOfPegHoles);
            InsertValue(ProSpaceImportHelper.ProductPegholeX, dto.PegX);
            InsertValue(ProSpaceImportHelper.ProductPeghole2X, dto.PegX2);
            InsertValue(ProSpaceImportHelper.ProductPeghole3X, dto.PegX3);
            InsertValue(ProSpaceImportHelper.ProductPegholeY, dto.PegY);
            InsertValue(ProSpaceImportHelper.ProductPeghole2Y, dto.PegY2);
            InsertValue(ProSpaceImportHelper.ProductPeghole3Y, dto.PegY3);

            InsertValue(ProSpaceImportHelper.ProductMinimumSqueezeFactorY, dto.SqueezeHeight == 0 ? 1 : dto.SqueezeHeight);
            InsertValue(ProSpaceImportHelper.ProductMinimumSqueezeFactorX, dto.SqueezeWidth == 0 ? 1 : dto.SqueezeWidth);
            InsertValue(ProSpaceImportHelper.ProductMinimumSqueezeFactorZ, dto.SqueezeDepth == 0 ? 1 : dto.SqueezeDepth);

            InsertValue(ProSpaceImportHelper.ProductYNesting, dto.NestingHeight);
            InsertValue(ProSpaceImportHelper.ProductXNesting, dto.NestingWidth);
            InsertValue(ProSpaceImportHelper.ProductZNesting, dto.NestingDepth);

            InsertValue(ProSpaceImportHelper.ProductMerchYMax, dto.MaxStack);
            InsertValue(ProSpaceImportHelper.ProductMerchYCaps, dto.MaxTopCap);
            InsertValue(ProSpaceImportHelper.ProductMerchZMax, dto.MaxDeep);
            InsertValue(ProSpaceImportHelper.ProductMerchZMin, dto.MinDeep);
            InsertValue(ProSpaceImportHelper.ProductMerchXCaps, dto.MaxRightCap);
            InsertValue(ProSpaceImportHelper.ProductPackageStyle, dto.StyleNumber);
            InsertValue(ProSpaceImportHelper.ProductPackageStyle, (Int32)EncodeShapeType((PlanogramProductShapeType)dto.ShapeType));
            InsertValue(ProSpaceImportHelper.ProductFillPattern, (Int32)EncodeProSpaceFillPatternType((PlanogramProductFillPatternType)dto.FillPatternType));
            InsertValue(ProSpaceImportHelper.ProductColor, DecodeColour(dto.FillColour));
            InsertValue(ProSpaceImportHelper.ProductFingerSpaceX, dto.FingerSpaceToTheSide);
            InsertValue(ProSpaceImportHelper.ProductFingerSpaceY, dto.FingerSpaceAbove);
            InsertValue(ProSpaceImportHelper.ProductFrontOverhang, dto.FrontOverhang);
            InsertValue(ProSpaceImportHelper.ProductSqueezeOnlyUnitMerchandising, 1);
            
            InsertValue(ProSpaceImportHelper.ProductDefaultMerchStyle, (Byte)EncodeProductMerchandisingStyle((PlanogramProductMerchandisingStyle)dto.MerchandisingStyle, dto));

            ApplyExportMappings(dto, dto.Id, this._dalCache.ExportMappingContext.ProductMappings, CustomAttributeDataPlanogramParentType.PlanogramProduct);
        }

        private static ProSpaceShapeType EncodeShapeType(PlanogramProductShapeType value)
        {
            switch (value)
            {
                case PlanogramProductShapeType.Box:
                    return ProSpaceShapeType.Box;
                case PlanogramProductShapeType.Bottle:
                    return ProSpaceShapeType.Bottle;
                default:
                    Debug.Fail("Unknown ProSpaceShapeType value when calling DecodeShapeType.");
                    return ProSpaceShapeType.Box;
            }
        }

        private void EncodePlanogramPosition()
        {
            Object objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramComponentDto), out objectDto);
            PlanogramComponentDto componentDto = objectDto as PlanogramComponentDto;

            objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramFixtureComponentDto), out objectDto);
            PlanogramFixtureComponentDto fixtureComponentDto = objectDto as PlanogramFixtureComponentDto;

            objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramFixtureAssemblyDto), out objectDto);
            PlanogramFixtureAssemblyDto fixutreAssemblyDto = objectDto as PlanogramFixtureAssemblyDto;

            objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramAssemblyComponentDto), out objectDto);
            PlanogramAssemblyComponentDto assemblyComponentDto = objectDto as PlanogramAssemblyComponentDto;

            objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramSubComponentDto), out objectDto);
            PlanogramSubComponentDto subComponentDto = objectDto as PlanogramSubComponentDto;

            objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramPositionDto), out objectDto);
            PlanogramPositionDto positionDto = objectDto as PlanogramPositionDto;

            objectDto = null;
            this.Dtos.TryGetValue(typeof(PlanogramProductDto), out objectDto);
            PlanogramProductDto productDto = objectDto as PlanogramProductDto;


            InsertValue(ProSpaceImportHelper.PositionProductUpc, productDto.Gtin);
            InsertValue(ProSpaceImportHelper.PositionProductId, positionDto.PlanogramProductId);
            InsertValue(ProSpaceImportHelper.PositionLocationId, positionDto.PositionSequenceNumber);
            #region Postion XYZ
            Single x = 0, y = 0, z = 0;
            DalCacheItem segment = null;
           
            if (fixtureComponentDto != null)
            {

                x = fixtureComponentDto.X + subComponentDto.X + positionDto.X;
                y = fixtureComponentDto.Y + subComponentDto.Y + positionDto.Y;
                z = fixtureComponentDto.Z + subComponentDto.Z + positionDto.Z;

                segment = this._dalCache.GetSegmentCacheItem(fixtureComponentDto.PlanogramFixtureId.ToString());
            }
            else if (assemblyComponentDto != null && fixutreAssemblyDto != null)
            {
                x = fixutreAssemblyDto.X + assemblyComponentDto.X + subComponentDto.X + positionDto.X;
                y = fixutreAssemblyDto.Y + assemblyComponentDto.Y + subComponentDto.Y + positionDto.Y;
                z = fixutreAssemblyDto.Z + assemblyComponentDto.Z + subComponentDto.Z + positionDto.Z;

                segment = this._dalCache.GetSegmentCacheItem(fixutreAssemblyDto.PlanogramFixtureId.ToString());
            }

            Object output = null;
            if (segment != null && segment.Dtos.TryGetValue(typeof(PlanogramFixtureItemDto), out output))
            {
                PlanogramFixtureItemDto fixtureItemDto = output as PlanogramFixtureItemDto;
                x += fixtureItemDto.X;
                y += fixtureItemDto.Y;
                z += fixtureItemDto.Z;
            }

            InsertValue(ProSpaceImportHelper.PositionX, x);
            InsertValue(ProSpaceImportHelper.PositionY, y);
            InsertValue(ProSpaceImportHelper.PositionZ, z);
            #endregion                      
            InsertValue(ProSpaceImportHelper.PositionOrientation, (Int32)EncodePositionOrientationType((PlanogramPositionOrientationType)positionDto.OrientationType, productDto));
            InsertValue(ProSpaceImportHelper.PositionXCapOrientation, (Int32)EncodePositionOrientationType((PlanogramPositionOrientationType)positionDto.OrientationTypeX, productDto));
            InsertValue(ProSpaceImportHelper.PositionYCapOrientation, (Int32)EncodePositionOrientationType((PlanogramPositionOrientationType)positionDto.OrientationTypeY, productDto));
            InsertValue(ProSpaceImportHelper.PositionZCapOrientation, (Int32)EncodePositionOrientationType((PlanogramPositionOrientationType)positionDto.OrientationTypeZ, productDto));
            InsertValue(ProSpaceImportHelper.PositionXCapReversed, positionDto.IsXPlacedLeft ? 1 : 0);
            InsertValue(ProSpaceImportHelper.PositionYCapReversed, positionDto.IsYPlacedBottom ? 1 : 0);
            //there just has to be one that has to be different!
            InsertValue(ProSpaceImportHelper.PositionZCapReversed, positionDto.IsZPlacedFront ? 0 : 1);

            PlanogramPositionMerchandisingStyle mainMerchStyle = (PlanogramPositionMerchandisingStyle)positionDto.MerchandisingStyle;
            PlanogramPositionMerchandisingStyle xMerchStyle = (PlanogramPositionMerchandisingStyle)positionDto.MerchandisingStyleX;
            PlanogramPositionMerchandisingStyle yMerchStyle = (PlanogramPositionMerchandisingStyle)positionDto.MerchandisingStyleY;
            PlanogramPositionMerchandisingStyle zMerchStyle = (PlanogramPositionMerchandisingStyle)positionDto.MerchandisingStyleX;
            InsertValue(ProSpaceImportHelper.PositionMerchStyle, (Int32)EncodePositionMerchandisingStyle((PlanogramPositionMerchandisingStyle)positionDto.MerchandisingStyle, productDto));
            Int32 xTrayBroken = 0;
            Int32 yTrayBroken = 0;
            Int32 zTrayBroken = 0;
            if (mainMerchStyle == PlanogramPositionMerchandisingStyle.Tray || mainMerchStyle == PlanogramPositionMerchandisingStyle.Case)
            {
                if (xMerchStyle == PlanogramPositionMerchandisingStyle.Unit ||
                    (xMerchStyle == PlanogramPositionMerchandisingStyle.Default && productDto.MerchandisingStyle == (Byte)PlanogramProductMerchandisingStyle.Unit))
                {
                    xTrayBroken = 1;
                }
                if (yMerchStyle == PlanogramPositionMerchandisingStyle.Unit ||
                    (yMerchStyle == PlanogramPositionMerchandisingStyle.Default && productDto.MerchandisingStyle == (Byte)PlanogramProductMerchandisingStyle.Unit))
                {
                    yTrayBroken = 1;
                }
                if (zMerchStyle == PlanogramPositionMerchandisingStyle.Unit ||
                    (zMerchStyle == PlanogramPositionMerchandisingStyle.Default && productDto.MerchandisingStyle == (Byte)PlanogramProductMerchandisingStyle.Unit))
                {
                    zTrayBroken = 1;
                }
            }
            InsertValue(ProSpaceImportHelper.PositionMerchXIsTrayBroken, xTrayBroken);
            InsertValue(ProSpaceImportHelper.PositionMerchYIsTrayBroken, yTrayBroken);
            InsertValue(ProSpaceImportHelper.PositionMerchZIsTrayBroken, zTrayBroken);

            
            //Rank isn't rank it's sequence.. go figure :/
            InsertValue(ProSpaceImportHelper.PositionRankX, positionDto.SequenceX);
            InsertValue(ProSpaceImportHelper.PositionRankY, positionDto.SequenceY);
            InsertValue(ProSpaceImportHelper.PositionRankZ, positionDto.SequenceZ);

            #region Main Facings
            if (productDto.NestingWidth > 0)
            {
                InsertValue(ProSpaceImportHelper.PositionHFacings, 1);
                InsertValue(ProSpaceImportHelper.PositionXCapNum, positionDto.FacingsWide - 1);
                InsertValue(ProSpaceImportHelper.PositionXCapNested, 1);
                InsertValue(ProSpaceImportHelper.PositionXCapOrientation, (Int32)EncodePositionOrientationType((PlanogramPositionOrientationType)positionDto.OrientationType, productDto));
            }
            else
            {
                InsertValue(ProSpaceImportHelper.PositionHFacings, positionDto.FacingsWide);
                InsertValue(ProSpaceImportHelper.PositionXCapNested, 0);
            }

            if (productDto.NestingHeight > 0)
            {
                InsertValue(ProSpaceImportHelper.PositionVFacings, 1);
                InsertValue(ProSpaceImportHelper.PositionYCapNum, positionDto.FacingsHigh - 1);
                InsertValue(ProSpaceImportHelper.PositionYCapNested, 1);
                InsertValue(ProSpaceImportHelper.PositionYCapOrientation, (Int32)EncodePositionOrientationType((PlanogramPositionOrientationType)positionDto.OrientationType, productDto));
            }
            else
            {
                InsertValue(ProSpaceImportHelper.PositionVFacings, positionDto.FacingsHigh);
                InsertValue(ProSpaceImportHelper.PositionYCapNested, 0);
            }

            if (productDto.NestingDepth > 0)
            {
                InsertValue(ProSpaceImportHelper.PositionDFacings, 1);
                InsertValue(ProSpaceImportHelper.PositionZCapNum, positionDto.FacingsDeep - 1);
                InsertValue(ProSpaceImportHelper.PositionZCapNested, 1);
                InsertValue(ProSpaceImportHelper.PositionZCapOrientation, (Int32)EncodePositionOrientationType((PlanogramPositionOrientationType)positionDto.OrientationType, productDto));
            }
            else
            {
                InsertValue(ProSpaceImportHelper.PositionDFacings, positionDto.FacingsDeep);
                InsertValue(ProSpaceImportHelper.PositionZCapNested, 0);
            }
            #endregion

            #region Cap Facings
            if (productDto.NestingWidth == 0 &&
                positionDto.FacingsXHigh > 0 &&
                positionDto.FacingsXWide > 0 && 
                positionDto.FacingsXDeep > 0)
            {
                InsertValue(ProSpaceImportHelper.PositionXCapNum, positionDto.FacingsXWide);
                this._dalCache.LogWarning(Language.ExportLog_CappingWarningX, String.Format(Language.ExportLog_CappingWarning_Xcap, productDto.Gtin, productDto.Name), PlanogramEventLogAffectedType.Positions);
            }

            if (productDto.NestingHeight == 0 &&
                positionDto.FacingsYHigh > 0 &&
                positionDto.FacingsYWide > 0 &&
                positionDto.FacingsYDeep > 0)
            {
                InsertValue(ProSpaceImportHelper.PositionYCapNum, positionDto.FacingsYHigh);
                this._dalCache.LogWarning(Language.ExportLog_CappingWarningY, String.Format(Language.ExportLog_CappingWarning_Ycap, productDto.Gtin, productDto.Name), PlanogramEventLogAffectedType.Positions);
            }

            if (productDto.NestingDepth == 0 &&
                positionDto.FacingsZHigh > 0 &&
                positionDto.FacingsZWide > 0 &&
                positionDto.FacingsZDeep > 0)
            {
                InsertValue(ProSpaceImportHelper.PositionZCapNum, positionDto.FacingsZDeep);
                this._dalCache.LogWarning(Language.ExportLog_CappingWarningZ, String.Format(Language.ExportLog_CappingWarning_Zcap, productDto.Gtin, productDto.Name), PlanogramEventLogAffectedType.Positions);
            }
            #endregion

            if (productDto != null && subComponentDto != null &&
                productDto.FingerSpaceAbove > 0)
            {
                ProSpaceComponentType componentType = EncodeProSpaceComponentType(subComponentDto);
                if (componentType == ProSpaceComponentType.Pegboard)
                {
                    this._dalCache.LogWarning(Language.ExportLog_FingerSpaceWarning, String.Format(Language.ExportLog_FingerSpaceWarning_Top_Pegboard, productDto.Gtin, productDto.Name), PlanogramEventLogAffectedType.Positions);
                }
            }
        }

        private ProSpaceOrientationType EncodePositionOrientationType(PlanogramPositionOrientationType source, PlanogramProductDto productDto)
        {
            switch (source)
            {
                case PlanogramPositionOrientationType.Front0:
                    return ProSpaceOrientationType.Front;
                case PlanogramPositionOrientationType.Front90:
                    return ProSpaceOrientationType.Front90;
                case PlanogramPositionOrientationType.Front180:
                    return ProSpaceOrientationType.Front180;
                case PlanogramPositionOrientationType.Front270:
                    return ProSpaceOrientationType.Front270;
                case PlanogramPositionOrientationType.Back0:
                    return ProSpaceOrientationType.Back;
                case PlanogramPositionOrientationType.Back90:
                    return ProSpaceOrientationType.Back90;
                case PlanogramPositionOrientationType.Back180:
                    return ProSpaceOrientationType.Back180;
                case PlanogramPositionOrientationType.Back270:
                    return ProSpaceOrientationType.Back270;
                case PlanogramPositionOrientationType.Right0:
                    return ProSpaceOrientationType.Right;
                case PlanogramPositionOrientationType.Right90:
                    return ProSpaceOrientationType.Right90;
                case PlanogramPositionOrientationType.Right180:
                    return ProSpaceOrientationType.Right180;
                case PlanogramPositionOrientationType.Right270:
                    return ProSpaceOrientationType.Right270;
                case PlanogramPositionOrientationType.Left0:
                    return ProSpaceOrientationType.Side;
                case PlanogramPositionOrientationType.Left90:
                    return ProSpaceOrientationType.Side90;
                case PlanogramPositionOrientationType.Left180:
                    return ProSpaceOrientationType.Side180;
                case PlanogramPositionOrientationType.Left270:
                    return ProSpaceOrientationType.Side270;
                case PlanogramPositionOrientationType.Top0:
                    return ProSpaceOrientationType.Top;
                case PlanogramPositionOrientationType.Top90:
                    return ProSpaceOrientationType.Top90;
                case PlanogramPositionOrientationType.Top180:
                    return ProSpaceOrientationType.Top180;
                case PlanogramPositionOrientationType.Top270:
                    return ProSpaceOrientationType.Top270;
                case PlanogramPositionOrientationType.Bottom0:
                    return ProSpaceOrientationType.Base;
                case PlanogramPositionOrientationType.Bottom90:
                    return ProSpaceOrientationType.Base90;
                case PlanogramPositionOrientationType.Bottom180:
                    return ProSpaceOrientationType.Base180;
                case PlanogramPositionOrientationType.Bottom270:
                    return ProSpaceOrientationType.Base270;
                case PlanogramPositionOrientationType.Default:
                default:
                    if(productDto != null)
                    {
                        return EncodeProductOrientationType((PlanogramProductOrientationType)productDto.OrientationType);
                    }
                    else
                    {
                        return ProSpaceOrientationType.Front;
                    }
            }

        }

        private ProSpaceOrientationType EncodeProductOrientationType(PlanogramProductOrientationType source)
        {
            switch (source)
            {
                case PlanogramProductOrientationType.Front0:
                    return ProSpaceOrientationType.Front;
                case PlanogramProductOrientationType.Front90:
                    return ProSpaceOrientationType.Front90;
                case PlanogramProductOrientationType.Front180:
                    return ProSpaceOrientationType.Front180;
                case PlanogramProductOrientationType.Front270:
                    return ProSpaceOrientationType.Front270;
                case PlanogramProductOrientationType.Back0:
                    return ProSpaceOrientationType.Back;
                case PlanogramProductOrientationType.Back90:
                    return ProSpaceOrientationType.Back90;
                case PlanogramProductOrientationType.Back180:
                    return ProSpaceOrientationType.Back180;
                case PlanogramProductOrientationType.Back270:
                    return ProSpaceOrientationType.Back270;
                case PlanogramProductOrientationType.Right0:
                    return ProSpaceOrientationType.Right;
                case PlanogramProductOrientationType.Right90:
                    return ProSpaceOrientationType.Right90;
                case PlanogramProductOrientationType.Right180:
                    return ProSpaceOrientationType.Right180;
                case PlanogramProductOrientationType.Right270:
                    return ProSpaceOrientationType.Right270;
                case PlanogramProductOrientationType.Left0:
                    return ProSpaceOrientationType.Side;
                case PlanogramProductOrientationType.Left90:
                    return ProSpaceOrientationType.Side90;
                case PlanogramProductOrientationType.Left180:
                    return ProSpaceOrientationType.Side180;
                case PlanogramProductOrientationType.Left270:
                    return ProSpaceOrientationType.Side270;
                case PlanogramProductOrientationType.Top0:
                    return ProSpaceOrientationType.Top;
                case PlanogramProductOrientationType.Top90:
                    return ProSpaceOrientationType.Top90;
                case PlanogramProductOrientationType.Top180:
                    return ProSpaceOrientationType.Top180;
                case PlanogramProductOrientationType.Top270:
                    return ProSpaceOrientationType.Top270;
                case PlanogramProductOrientationType.Bottom0:
                    return ProSpaceOrientationType.Base;
                case PlanogramProductOrientationType.Bottom90:
                    return ProSpaceOrientationType.Base90;
                case PlanogramProductOrientationType.Bottom180:
                    return ProSpaceOrientationType.Base180;
                case PlanogramProductOrientationType.Bottom270:
                    return ProSpaceOrientationType.Base270;
                default:
                        return ProSpaceOrientationType.Front;
                    
            }

        }

        private ProSpaceMerchandisingType EncodePositionMerchandisingStyle(PlanogramPositionMerchandisingStyle value, PlanogramProductDto productDto)
        {
            switch (value)
            {
                case PlanogramPositionMerchandisingStyle.Default:
                    if (productDto != null)
                    {
                        return EncodeProductMerchandisingStyle((PlanogramProductMerchandisingStyle)productDto.MerchandisingStyle, productDto);
                    }
                    return ProSpaceMerchandisingType.Unit;
                case PlanogramPositionMerchandisingStyle.Tray:
                    return ProSpaceMerchandisingType.Tray;
                case PlanogramPositionMerchandisingStyle.Case:
                    return ProSpaceMerchandisingType.Case;
                case PlanogramPositionMerchandisingStyle.Display:
                    return ProSpaceMerchandisingType.Display;
                case PlanogramPositionMerchandisingStyle.Alternate:
                    return ProSpaceMerchandisingType.Alternate;
                case PlanogramPositionMerchandisingStyle.Unit:
                    return ProSpaceMerchandisingType.Unit;
                case PlanogramPositionMerchandisingStyle.PointOfPurchase:
                    this._dalCache.LogWarning(Language.ExportLog_MerchandisingWarning,
                        String.Format(Language.ExportLog_MerchandisingWarning_MerchandisingStyleChanged, productDto.Gtin, productDto.Name,
                        PlanogramPositionMerchandisingStyleHelper.FriendlyNames[PlanogramPositionMerchandisingStyle.Unit],
                        PlanogramPositionMerchandisingStyleHelper.FriendlyNames[value]), PlanogramEventLogAffectedType.Positions);
                    return ProSpaceMerchandisingType.Unit;
                default:
                    Debug.Fail("Unknown ProSpaceMerchandisingType value when calling DecodeProductMerchandisingStyle.");
                    return ProSpaceMerchandisingType.Unit;
            }
        }
        private ProSpaceMerchandisingType EncodeProductMerchandisingStyle(PlanogramProductMerchandisingStyle value, PlanogramProductDto productDto)
        {
            switch (value)
            {
                case PlanogramProductMerchandisingStyle.Tray:
                    return ProSpaceMerchandisingType.Tray;
                case PlanogramProductMerchandisingStyle.Case:
                    return ProSpaceMerchandisingType.Case;
                case PlanogramProductMerchandisingStyle.Display:
                    return ProSpaceMerchandisingType.Display;
                case PlanogramProductMerchandisingStyle.Alternate:
                    return ProSpaceMerchandisingType.Alternate;
                case PlanogramProductMerchandisingStyle.Unit:
                    return ProSpaceMerchandisingType.Unit;
                case PlanogramProductMerchandisingStyle.PointOfPurchase:
                    this._dalCache.LogWarning(Language.ExportLog_MerchandisingWarning,
                        String.Format(Language.ExportLog_MerchandisingWarning_MerchandisingStyleChanged, productDto.Gtin, productDto.Name,
                        PlanogramProductMerchandisingStyleHelper.FriendlyNames[PlanogramProductMerchandisingStyle.Unit],
                        PlanogramProductMerchandisingStyleHelper.FriendlyNames[value]), PlanogramEventLogAffectedType.Positions);

                    return ProSpaceMerchandisingType.Unit;
                default:
                    Debug.Fail("Unknown ProSpaceMerchandisingType value when calling DecodeProductMerchandisingStyle.");
                    return ProSpaceMerchandisingType.Unit;
            }
        }

        #endregion

        #region Helper Methods

        #region Extract Values Methods

        private T ExtractValue<T>(String propertyName)
        {
            Type type = typeof(T);
            Object extractValue = ExtractValue(propertyName, type);
            if (extractValue == null &&
                !(type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>)))
                extractValue = default(T);
            return (T)extractValue;
        }

        private Object ExtractValue(String propertyName, Type propertyType)
        {
            if (String.IsNullOrEmpty(_data))
                return null; // Invalid data.

            List<String> datum = DataFields;

            //  Validate the index.
            //  NB the property name should be FIELDSECTIONXXX, with XXX being the actual index. get just the last three digits, accounting for smaller names (say XXX).
            Int32 index;
            if (!Int32.TryParse(propertyName.Substring(Math.Max(propertyName.Length - 3, 0)), out index))
                return GetCalculatedField(propertyName);

            if (index >= datum.Count)
                return null; // Index out of bounds.

            Boolean success;
            Object value = SpacePlanningImportHelper.ParseValue(datum[index], propertyType, out success);
            if (success) return value;

            //  Log the conversion error.
            ObjectFieldInfo info;
            ProSpaceImportHelper.AllFieldInfos.TryGetValue(propertyName, out info);
            String friendlyName = propertyName;
            if (info != null)
            {
                friendlyName = info.FieldFriendlyName;
                //AddEventLogEntry(String.Format("Unknown field ({0})", propertyName), String.Format("Unable to convert the source value as the field was unknown when importing {0}.", GetFriendlyId()));
            }

            AddEventLogEntry(String.Format(Message.Import_ConversionError_Description, friendlyName, propertyType.Name),
                             String.Format(Message.Import_ConversionError_Content, GetFriendlyId()));
            return value;
        }

        private void InsertValue(String propertyName, Object value)
        {
            //Boolean values should be stored as ints
            if (value is Boolean) value = (Boolean)value ? 1 : 0;            

            //if Date then convert to stupid date
            if (value != null && value is DateTime) value = ((DateTime)value).ToOADate();
            
            _dataFields = _dataFields ?? new List<String>();

            //  Validate the index.
            //  NB the property name should be FIELDSECTIONXXX, with XXX being the actual index. get just the last three digits, accounting for smaller names (say XXX).
            Int32 index;
            if (!Int32.TryParse(propertyName.Substring(Math.Max(propertyName.Length - 3, 0)), out index)) return;
            //    return GetCalculatedField(propertyName);

            if (_dataFields.Count <= index)
            {
                for (Int32 x = _dataFields.Count; x <= index; x++)
                {
                    _dataFields.Add(String.Empty);
                }
            }

            _dataFields[index] = Convert.ToString(value);

           // _data = String.Join(",", datum);

        }

        private PlanogramLengthUnitOfMeasureType ExtractedPlanogramLengthUnitOfMeasureType
        {
            get
            {
                if (_planogramLengthUnitOfMeasure == PlanogramLengthUnitOfMeasureType.Unknown)
                    _planogramLengthUnitOfMeasure =
                        ConvertTo<PlanogramLengthUnitOfMeasureType>(GetProjectCacheItem().ExtractValue<Byte>(ProSpaceImportHelper.ProjectMeasurement));
                return _planogramLengthUnitOfMeasure;
            }
        }

        private Single ExtractPerformancePrice()
        {
            Single value;
            switch ((JdaSpacePlanningUsePerformanceValueType)_parent._parent.ExtractValue<Int32>(ProSpaceImportHelper.ProjectUsePerformancePrice))
            {
                case JdaSpacePlanningUsePerformanceValueType.No:
                    value = PlanogramProductForPerformance.ExtractValue<Single>(ProSpaceImportHelper.ProductPrice);
                    break;
                case JdaSpacePlanningUsePerformanceValueType.Yes:
                    value = ExtractValue<Single>(ProSpaceImportHelper.PerformancePrice);
                    break;
                default:
                    value = ExtractValue<Single>(ProSpaceImportHelper.PerformancePrice);
                    if (value.EqualTo(0)) value = PlanogramProductForPerformance.ExtractValue<Single>(ProSpaceImportHelper.ProductPrice);
                    break;
            }
            return value;
        }

        private Single ExtractPerformanceUnitMovement()
        {
            Single value;
            switch ((JdaSpacePlanningUsePerformanceValueType)_parent._parent.ExtractValue<Int32>(ProSpaceImportHelper.ProjectUsePerformanceMovement))
            {
                case JdaSpacePlanningUsePerformanceValueType.No:
                    value = PlanogramProductForPerformance.ExtractValue<Single>(ProSpaceImportHelper.ProductUnitMovement);
                    break;
                case JdaSpacePlanningUsePerformanceValueType.Yes:
                    value = ExtractValue<Single>(ProSpaceImportHelper.PerformanceUnitMovement);
                    break;
                default:
                    value = ExtractValue<Single>(ProSpaceImportHelper.PerformanceUnitMovement);
                    if (value.EqualTo(0)) value = PlanogramProductForPerformance.ExtractValue<Single>(ProSpaceImportHelper.ProductUnitMovement);
                    break;
            }
            return value;
        }

        private Single ExtractPerformanceCaseCost()
        {
            Single value;
            switch ((JdaSpacePlanningUsePerformanceValueType)_parent._parent.ExtractValue<Int32>(ProSpaceImportHelper.ProjectUsePerformanceCost))
            {
                case JdaSpacePlanningUsePerformanceValueType.No:
                    value = PlanogramProductForPerformance.ExtractValue<Single>(ProSpaceImportHelper.ProductCaseCost);
                    break;
                case JdaSpacePlanningUsePerformanceValueType.Yes:
                    value = ExtractValue<Single>(ProSpaceImportHelper.PerformanceCaseCost);
                    break;
                default:
                    value = ExtractValue<Single>(ProSpaceImportHelper.PerformanceCaseCost);
                    if (value.EqualTo(0)) value = PlanogramProductForPerformance.ExtractValue<Single>(ProSpaceImportHelper.ProductCaseCost);
                    break;
            }
            return value;
        }

        #endregion

        #region Calculated Fields Methods

        private Object GetCalculatedField(String propertyName)
        {
            if (propertyName == ProSpaceImportHelper.PerformanceSales) return CalculatePerformanceSales();
            if (propertyName == ProSpaceImportHelper.PerformanceProfit) return CalculatePerformanceProfit();
            if (propertyName == ProSpaceImportHelper.PerformanceUnitCost) return CalculatePerformanceUnitCost();
            if (propertyName == ProSpaceImportHelper.ProductUnitCost) return CalculateProductUnitCost();
            return null; // Invalid index
        }

        private Single CalculatePerformanceProfit()
        {
            //  Check that this is invoked for a Performance DAL cache item.
            if (!IsType(DalCacheItemType.Performance))
            {
                Debug.Fail("CalculatePerformanceProfit should be invoked only for Performance DAL cache items.");
                return 0.0F;
            }

            return CalculatePerformanceSales() - (CalculatePerformanceUnitCost() * ExtractPerformanceUnitMovement());
        }

        private Single CalculatePerformanceSales()
        {
            //  Check that this is invoked for a Performance DAL cache item.
            if (!IsType(DalCacheItemType.Performance))
            {
                Debug.Fail("CalculatePerformanceSales should be invoked only for Performance DAL cache items.");
                return 0.0F;
            }

            //  Determine the source for the values.
            return ExtractPerformanceUnitMovement() * ExtractPerformancePrice();
        }

        private Single CalculateProductUnitCost()
        {
            //  Check that this is invoked for a Product DAL cache item.
            if (!IsType(DalCacheItemType.Product))
            {
                Debug.Fail("CalculateProductUnitCost should be invoked only for Product DAL cache items.");
                return 0.0F;
            }

            var caseTotalNumber = ExtractValue<Single>(ProSpaceImportHelper.ProductCaseTotalNumber);
            if (caseTotalNumber.LessOrEqualThan(0)) caseTotalNumber = 1;
            return ExtractValue<Single>(ProSpaceImportHelper.ProductCaseCost)/caseTotalNumber;
        }

        private Single CalculatePerformanceUnitCost()
        {
            //  Check that this is invoked for a Performance DAL cache item.
            if (!IsType(DalCacheItemType.Performance))
            {
                Debug.Fail("CalculatePerformanceUnitCost should be invoked only for Performance DAL cache items.");
                return 0.0F;
            }

            var caseTotalNumber = PlanogramProductForPerformance.ExtractValue<Single>(ProSpaceImportHelper.ProductCaseTotalNumber);
            if (caseTotalNumber.LessOrEqualThan(0)) caseTotalNumber = 1;
            return ExtractPerformanceCaseCost() / caseTotalNumber;
        }

        #endregion

        private Boolean IsType(DalCacheItemType type)
        {
            return Equals(CacheItemType, type);
        }

        private DalCacheItemType CacheItemType { get { return (DalCacheItemType) (_cacheItemType ?? (_cacheItemType = GetDalCacheItemType())); } }

        private DalCacheItemType GetDalCacheItemType()
        {
            String match =
                Enum.GetNames(typeof (DalCacheItemType)).FirstOrDefault(name => _data.StartsWith(name, StringComparison.InvariantCultureIgnoreCase));
            DalCacheItemType result;
            return Enum.TryParse(match, true, out result) ? result : DalCacheItemType.Unknown;
        }

        private static CustomAttributeDataPlanogramParentType GetCustomAttributeDataParentType(DalCacheItemType cacheItemType)
        {
            var parentTypeByItemType =
                new Dictionary<DalCacheItemType, CustomAttributeDataPlanogramParentType>
                {
                    {DalCacheItemType.Planogram, CustomAttributeDataPlanogramParentType.Planogram},
                    {DalCacheItemType.Product, CustomAttributeDataPlanogramParentType.PlanogramProduct},
                    {DalCacheItemType.Fixture, CustomAttributeDataPlanogramParentType.PlanogramComponent},
                };
            CustomAttributeDataPlanogramParentType itemType;
            if (!parentTypeByItemType.TryGetValue(cacheItemType, out itemType))
                itemType = CustomAttributeDataPlanogramParentType.Planogram;

            return itemType;
        }

        private static DalCacheItem GetContainingSegmentCacheItem(DalCacheItem fixtureCacheItem)
        {
            DalCacheItemType itemType = fixtureCacheItem.CacheItemType;
            DalCacheItem planogramCacheItem = fixtureCacheItem._parent;
            Single itemX;
            switch (itemType)
            {
                case DalCacheItemType.Fixture:
                    itemX = fixtureCacheItem.ExtractValue<Single>(ProSpaceImportHelper.FixtureX);
                    break;
                case DalCacheItemType.Drawing:
                    itemX = fixtureCacheItem.ExtractValue<Single>(ProSpaceImportHelper.DrawingX);
                    break;
                case DalCacheItemType.Unknown:
                case DalCacheItemType.Project:
                case DalCacheItemType.Product:
                case DalCacheItemType.Planogram:
                case DalCacheItemType.Performance:
                case DalCacheItemType.Segment:
                case DalCacheItemType.Position:
                case DalCacheItemType.Point3D:
                default:
                    throw new ArgumentOutOfRangeException();
            }
            var notchBarWidth = planogramCacheItem.ExtractValue<Single>(ProSpaceImportHelper.PlanogramNotchWidth);
            Dictionary<Single, DalCacheItem> planogramFixtureCacheItemByX =
                planogramCacheItem.Children[DalCacheItemType.Segment]
                    .ToDictionary(item => item.ExtractValue<Single>(ProSpaceImportHelper.SegmentOffsetX) - notchBarWidth);
            IOrderedEnumerable<KeyValuePair<Single, DalCacheItem>> matches =
                planogramFixtureCacheItemByX.Where(pair => pair.Key.LessOrEqualThan(itemX)).OrderByDescending(pair => pair.Key);
            return matches.Any() ? matches.First().Value : planogramFixtureCacheItemByX.Select(pair => pair.Value).First();
        }

        private List<DalCacheItem> GetSegmentCacheItems(Boolean all = false)
        {
            //  Find the containing PlanogramCacheItem.
            DalCacheItem root = this;
            while (!root.IsType(DalCacheItemType.Project))
            {
                //  If this is a PlanogramCacheItem, do not try to go up, just return the subset of SegmentCacheItems that belong to it.
                if (!all && root.IsType(DalCacheItemType.Planogram)) break;

                //  Continue searching while there is a parent.
                root = root._parent;
                if (root != null) continue;

                //  Something went horribly wrong, there should be at least one Project CacheItem... Oh, well... just return an empty collection.
                Debug.Fail("GetSegmentCacheItems could not find the containing ProjectCacheItem. There should always be one.");
                return new List<DalCacheItem>();
            }

            //  Return the contained Product CacheItems.
            if (root.Children != null)
            {
                if (root.Children.ContainsKey(DalCacheItemType.Planogram))
                    return root.Children[DalCacheItemType.Planogram]
                        .Where(item => item.Children != null && item.Children.ContainsKey(DalCacheItemType.Segment))
                        .SelectMany(item => item.Children[DalCacheItemType.Segment])
                        .ToList();
                if (root.Children.ContainsKey(DalCacheItemType.Segment))
                    return root.Children[DalCacheItemType.Segment];
            }

            //  What!? No Children or no Segment Children!!! Oh, well... just return an empty collection.
            Debug.Fail("GetSegmentCacheItems could not find the any children or the SegmentCacheItems children in the ProjectCacheItem or PlanogramCacheItem. There should always be both.");
            return new List<DalCacheItem>();
        }

        private List<DalCacheItem> GetProductCacheItems()
        {
            DalCacheItem root = GetProjectCacheItem();
            if (root == null) return new List<DalCacheItem>();

            //  Return the contained Product CacheItems.
            if (root.Children != null &&
                root.Children.ContainsKey(DalCacheItemType.Product)) return root.Children[DalCacheItemType.Product].ToList();

            //  What!? No Children or no Product Children!!! Oh, well... just return an empty collection.
            Debug.Fail("GetProductCacheItems could not find the any children or the ProductCacheItems children in the ProjectCacheItem. There should always be both.");
            return new List<DalCacheItem>();
        }

        /// <summary>
        ///     Get the containing cache item that is of the type next to the current instance's in the given <paramref name="defaultValuePath"/>.
        /// </summary>
        /// <param name="defaultValuePath"></param>
        /// <returns></returns>
        private DalCacheItem GetDefaultValuesCacheItem(IList<DalCacheItemType> defaultValuePath)
        {
            Int32 currentIndex = defaultValuePath.IndexOf(CacheItemType) + 1;

            return defaultValuePath.Count > currentIndex ? GetParentCacheItemOfType(defaultValuePath[currentIndex]) : null;
        }

        private DalCacheItem GetParentCacheItemOfType(DalCacheItemType type)
        {
            //  Find the containing CacheItem with the given type.
            DalCacheItem root = this;
            while (!root.IsType(type))
            {
                //  Continue searching while there is a parent.
                root = root._parent;
                if (root != null) continue;

                //  Something went horribly wrong, there should be at least one CacheItem... Oh, well... just return an empty result.
                //Debug.Fail("GetParentCacheItemOfType could not find the containing type.");
                return null;
            }
            return root;
        }

        private DalCacheItem GetProjectCacheItem()
        {
            //  Find the containing ProjectCacheItem.
            DalCacheItem root = this;
            while (!root.IsType(DalCacheItemType.Project))
            {
                //  Continue searching while there is a parent.
                root = root._parent;
                if (root != null) continue;

                //  Something went horribly wrong, there should be at least one Project CacheItem... Oh, well... just return an empty collection.
                Debug.Fail("GetProductCacheItems could not find the containing ProjectCacheItem. There should always be one.");
                return null;
            }
            return root;
        }

        private String GetBackboardId()
        {
            return String.Format(Message.JdaImport_BackboardDefaultNameMask, Id);
        }

        private String GetBaseId()
        {
            return String.Format(Message.JdaImport_BaseDefaultNameMask, Id);
        }

        /// <summary>
        ///     Returns the next id
        /// </summary>
        private static String GetNextId()
        {
            lock (NextIdLock)
            {
                _nextId++;
                return _nextId.ToString();
            }
        }

        private String GetFriendlyId()
        {
            switch (CacheItemType)
            {
                case DalCacheItemType.Unknown:
                    return Message.JdaImport_UnknownCacheItemTypeMask;
                case DalCacheItemType.Project:
                    return String.Format(Message.JdaImport_ProjectCacheItemTypeMask, ExtractValue<String>(ProSpaceImportHelper.ProjectName));
                case DalCacheItemType.Product:
                    return String.Format(Message.JdaImport_ProductCacheItemTypeMask, ExtractValue<String>(ProSpaceImportHelper.ProjectName));
                case DalCacheItemType.Planogram:
                    return String.Format(Message.JdaImport_PlanogramCacheItemTypeMask, ExtractValue<String>(ProSpaceImportHelper.PlanogramName));
                case DalCacheItemType.Performance:
                    return String.Format(Message.JdaImport_PerformanceCacheItemTypeMask, ExtractValue<String>(ProSpaceImportHelper.PerformanceUpc));
                case DalCacheItemType.Segment:
                    return String.Format(Message.JdaImport_FixtureCacheItemTypeMask, ExtractValue<String>(ProSpaceImportHelper.SegmentName));
                case DalCacheItemType.Fixture:
                    return String.Format(Message.JdaImport_ComponentCacheItemTypeMask, ExtractValue<String>(ProSpaceImportHelper.FixtureName));
                case DalCacheItemType.Position:
                    return String.Format(Message.JdaImport_PositionCacheItemTypeMask, ExtractValue<String>(ProSpaceImportHelper.PositionProductUpc));
                case DalCacheItemType.Drawing:
                    return String.Format(Message.JdaImport_DrawingCacheItemTypeMask, ExtractValue<String>(ProSpaceImportHelper.DrawingString));
                case DalCacheItemType.Point3D:
                    return Message.JdaImport_Point3DCacheItemTypeMask;
                default:
                    return Message.JdaImport_UnknownCacheItemTypeMask;
            }
        }

        private void AddEventLogEntry(String description, String content, PlanogramEventLogEntryType entryType = PlanogramEventLogEntryType.Warning)
        {
            DalCacheItem planogramCacheItem = GetParentCacheItemOfType(DalCacheItemType.Planogram) ??
                                              GetParentCacheItemOfType(DalCacheItemType.Project);
            SpacePlanningImportHelper.CreateErrorEventLogEntry(planogramCacheItem.Id, _dalCache.PlanogramEventLogDtos, description, content, entryType, PlanogramEventLogEventType.Import, GetAffectedType(), 1);
        }

        private PlanogramEventLogAffectedType GetAffectedType()
        {
            PlanogramEventLogAffectedType affectedType;
            switch (CacheItemType)
            {
                case DalCacheItemType.Product:
                    affectedType = PlanogramEventLogAffectedType.Products;
                    break;
                case DalCacheItemType.Performance:
                    affectedType = PlanogramEventLogAffectedType.Performance;
                    break;
                case DalCacheItemType.Segment:
                case DalCacheItemType.Fixture:
                case DalCacheItemType.Drawing:
                    affectedType = PlanogramEventLogAffectedType.Components;
                    break;
                case DalCacheItemType.Position:
                    affectedType = PlanogramEventLogAffectedType.Positions;
                    break;
                default:
                    affectedType = PlanogramEventLogAffectedType.Planogram;
                    break;
            }
            return affectedType;
        }

        private T ConvertTo<T>(Byte source)
        {
            Object result = default(T);
            if (typeof (T) == typeof (PlanogramLengthUnitOfMeasureType))
                ConvertToLengthUom(source, out result);
            return (T) Convert.ChangeType(result, typeof (T));
        }

        private void ConvertToLengthUom(Byte source, out Object result)
        {
            switch (source)
            {
                case 0:
                    result = PlanogramLengthUnitOfMeasureType.Inches;
                    break;
                case 1:
                    result = PlanogramLengthUnitOfMeasureType.Centimeters;
                    break;
                default:
                    result = PlanogramLengthUnitOfMeasureType.Unknown;
                    AddEventLogEntry(String.Format(Message.JdaImport_UnknownLenghtMeassureUnitType_Description, source), Message.JdaImport_UnknownLenghtMeassureUnitType_Content);
                    break;
            }
        }

        /// <summary>
        ///     Check whether the given dalcacheitem has any 3D data value set.
        /// </summary>
        /// <param name="segmentCacheItem"></param>
        /// <returns></returns>
        private Boolean Has3DPlacementData()
        {
            //  Check that this is invoked for a Segment DAL cache item.
            if (!IsType(DalCacheItemType.Segment))
            {
                Debug.Fail("Has3DPlacementData should be invoked only for Segment DAL cache items.");
                return false;
            }

            //  The segment cache item contains 3D placement data if it is a segment and at least one of the 3DPlacement fields is not 0.
            return (!ExtractValue<Single>(ProSpaceImportHelper.SegmentX).EqualTo(0) ||
                    !ExtractValue<Single>(ProSpaceImportHelper.SegmentY).EqualTo(0) ||
                    !ExtractValue<Single>(ProSpaceImportHelper.SegmentZ).EqualTo(0) ||
                    !ExtractValue<Single>(ProSpaceImportHelper.SegmentAngle).EqualTo(0));
        }

        private String DecodePlanogramSubComponentName(PlanogramComponentDto planogramComponentDto)
        {
            String name = planogramComponentDto.Name;
            if (String.IsNullOrWhiteSpace(name)) name = String.Format(Message.JdaImport_SubcomponentNameMask, Id);
            return name;
        }

        private static Single DecodeFixtureHeight(DalCacheItem segmentCacheItem, DalCacheItem planogramCacheItem)
        {
            var result = segmentCacheItem.ExtractValue<Single>(ProSpaceImportHelper.SegmentHeight);
            if (result.EqualTo(0))
                result = planogramCacheItem.ExtractValue<Single>(ProSpaceImportHelper.PlanogramHeight);
            return result;
        }

        private static Single DecodeFixtureDepth(DalCacheItem segmentCacheItem, DalCacheItem planogramCacheItem)
        {
            var result = segmentCacheItem.ExtractValue<Single>(ProSpaceImportHelper.SegmentDepth);
            if (result.EqualTo(0))
                result = planogramCacheItem.ExtractValue<Single>(ProSpaceImportHelper.PlanogramDepth);
            return result;
        }

        private static Single DecodeBaseDepth(DalCacheItem segmentCacheItem, DalCacheItem planogramCacheItem)
        {
            var result = planogramCacheItem.ExtractValue<Single>(ProSpaceImportHelper.PlanogramBaseDepth);
            if (result.EqualTo(0))
                result = DecodeFixtureDepth(segmentCacheItem, planogramCacheItem);
            return result;
        }

        private Single DecodeBackDepth(DalCacheItem segmentCacheItem, DalCacheItem planogramCacheItem)
        {
            var result = planogramCacheItem.ExtractValue<Single>(ProSpaceImportHelper.PlanogramBackDepth);
            if (result.EqualTo(0)) result = SpacePlanningImportHelper.GetDefaultBackboardDepth(ExtractedPlanogramLengthUnitOfMeasureType);
            return result;
        }

        private static Boolean DecodeCollisionDetection(DalCacheItem fixtureCacheItem)
        {
            return fixtureCacheItem.ExtractValue<Boolean>(ProSpaceImportHelper.FixtureDetectOtherFixtures) ||
                   fixtureCacheItem.ExtractValue<Boolean>(ProSpaceImportHelper.FixtureDetectPositionsOnOtherFixtures);
        }

        private static PlanogramSubComponentMerchandisingType DecodeSubComponentMerchandisingType(ProSpaceComponentType value)
        {
            switch (value)
            {
                case ProSpaceComponentType.Shelf:
                case ProSpaceComponentType.Chest:
                case ProSpaceComponentType.Bin:
                case ProSpaceComponentType.PolygonalShelf:
                    return PlanogramSubComponentMerchandisingType.Stack;
                case ProSpaceComponentType.Rod:
                case ProSpaceComponentType.LateralRod:
                case ProSpaceComponentType.CurvedRod:
                    return PlanogramSubComponentMerchandisingType.HangFromBottom;
                case ProSpaceComponentType.Bar:
                case ProSpaceComponentType.Pegboard:
                case ProSpaceComponentType.MultiRowPegboard:
                    return PlanogramSubComponentMerchandisingType.Hang;
                default:
                    Debug.Fail("Unknown ProSpaceComponentType value when calling DecodeSubComponentMerchandisingType.");
                    return PlanogramSubComponentMerchandisingType.None;
            }
        }

        private static PlanogramProductMerchandisingStyle DecodeProductMerchandisingStyle(ProSpaceMerchandisingType value)
        {
            switch (value)
            {
                case ProSpaceMerchandisingType.Default:
                    //  Get the value from the fixture this product is on.
                    return PlanogramProductMerchandisingStyle.Unit;
                case ProSpaceMerchandisingType.Tray:
                    return PlanogramProductMerchandisingStyle.Tray;
                case ProSpaceMerchandisingType.Case:
                    return PlanogramProductMerchandisingStyle.Case;
                case ProSpaceMerchandisingType.Display:
                    return PlanogramProductMerchandisingStyle.Display;
                case ProSpaceMerchandisingType.Alternate:
                    return PlanogramProductMerchandisingStyle.Alternate;
                case ProSpaceMerchandisingType.Unit:
                case ProSpaceMerchandisingType.Loose:
                case ProSpaceMerchandisingType.LogStack:
                    return PlanogramProductMerchandisingStyle.Unit;
                default:
                    Debug.Fail("Unknown ProSpaceMerchandisingType value when calling DecodeProductMerchandisingStyle.");
                    return PlanogramProductMerchandisingStyle.Unit;
            }
        }

        private static Byte DecodeFillPatternType(ProSpaceFillPatternType value)
        {
            var result = PlanogramProductFillPatternType.Solid;

            switch (value)
            {
                case ProSpaceFillPatternType.DiagonalDown:
                case ProSpaceFillPatternType.DashDiagonalDown:
                case ProSpaceFillPatternType.LightDashDiagonalDown:
                case ProSpaceFillPatternType.StrongDashDiagonalDown:
                    result = PlanogramProductFillPatternType.DiagonalDown;
                    break;
                case ProSpaceFillPatternType.DiagonalUp:
                case ProSpaceFillPatternType.DashDiagonalUp:
                case ProSpaceFillPatternType.LightDashDiagonalUp:
                case ProSpaceFillPatternType.StrongDashDiagonalUp:
                    result = PlanogramProductFillPatternType.DiagonalUp;
                    break;
                case ProSpaceFillPatternType.Crosshatch:
                case ProSpaceFillPatternType.Reticle:
                case ProSpaceFillPatternType.Bricks:
                case ProSpaceFillPatternType.Scales:
                case ProSpaceFillPatternType.Chainmail:
                    result = PlanogramProductFillPatternType.Crosshatch;
                    break;
                case ProSpaceFillPatternType.Border:
                    result = PlanogramProductFillPatternType.Border;
                    break;
                case ProSpaceFillPatternType.Dots:
                case ProSpaceFillPatternType.Xes:
                case ProSpaceFillPatternType.Circles:
                case ProSpaceFillPatternType.Triangles:
                case ProSpaceFillPatternType.Squares:
                case ProSpaceFillPatternType.Ces:
                case ProSpaceFillPatternType.Angles:
                    result = PlanogramProductFillPatternType.Dotted;
                    break;
                case ProSpaceFillPatternType.HorizontalWaves:
                case ProSpaceFillPatternType.HorizontalLines:
                case ProSpaceFillPatternType.HorizontalDashLanes:
                case ProSpaceFillPatternType.HorizontalDashLines:
                    result = PlanogramProductFillPatternType.Horizontal;
                    break;
                case ProSpaceFillPatternType.VerticalWaves:
                case ProSpaceFillPatternType.VerticalLines:
                case ProSpaceFillPatternType.VerticalDashLanes:
                case ProSpaceFillPatternType.VerticalDashLines:
                    result = PlanogramProductFillPatternType.Vertical;
                    break;
                case ProSpaceFillPatternType.Solid:
                default:
                    break;
            }

            return (Byte)result;
        }

        /// <summary>
        ///     Apply the mappings indicated that are present on this dalcacheitem.
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="mappings"></param>
        /// <param name="mappingType"></param>
        /// <remarks>
        ///     If the mapping refers to another dal cache item type (say, Product001 and this is a Planogram) the mapping will be
        ///     ignored.
        ///     <para />
        ///     This method must be called from each dal cache item that can be referenced in the mappings.
        /// </remarks>
        private void ApplyMappings(Object dto, IEnumerable<PlanogramFieldMappingDto> mappings, PlanogramFieldMappingType mappingType)
        {
            Type type = dto.GetType();

            IEnumerable<PlanogramFieldMappingDto> mappingsForType = mappings.Where(mappingDto => mappingDto.Type == (Byte) mappingType);
            mappingsForType = mappingsForType.Where(mappingDto => mappingDto.Source.StartsWith(CacheItemType.ToString()));
            foreach (PlanogramFieldMappingDto mapping in mappingsForType)
            {
                Object instance = dto;
                PropertyInfo property = type.GetProperty(mapping.Target);

                if (property == null &&
                    mapping.Target.Contains(PlanogramProduct.CustomAttributesProperty.Name))
                {
                    String[] columnMapping = mapping.Target.Split('.');
                    property = typeof(CustomAttributeDataDto).GetProperty(columnMapping[1]);

                    if (property == null) continue;

                    instance = GetDto<CustomAttributeDataDto>();
                }

                if (property == null) continue;

                Object value = ExtractValue(mapping.Source, property.PropertyType);

                property.SetValue(instance, value, null);
            }
        }

        /// <summary>
        ///     Apply the performance mappings indicated that are present on this dalcacheitem.
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="mappings"></param>
        /// <remarks>
        ///     If the mapping refers to another dal cache item type (say, Performance002 and this is a Product) the mapping will be
        ///     ignored.
        ///     <para />
        ///     This method must be called from each dal cache item that can be referenced in the mappings.
        /// </remarks>
        private void ApplyMetricMapping(Object dto, IEnumerable<PlanogramMetricMappingDto> mappings)
        {
            Type type = dto.GetType();

            IEnumerable<PlanogramMetricMappingDto> mappingsForType = mappings.Where(mappingDto => mappingDto.Source.StartsWith(CacheItemType.ToString()));
            foreach (PlanogramMetricMappingDto mapping in mappingsForType)
            {
                String propertyName = String.Format(MetricPropertyNameFormat, mapping.MetricId);
                PropertyInfo target = type.GetProperty(propertyName);
                if (target == null) continue;

                Object value = ExtractValue(mapping.Source, target.PropertyType);
                target.SetValue(dto, value, null);
            }
        }

        private void ApplyExportMappings(Object dto, Object id, IEnumerable<PlanogramFieldMappingDto> mappings, CustomAttributeDataPlanogramParentType customType)
        {
            Type type = dto.GetType();
            
            foreach (PlanogramFieldMappingDto mapping in mappings)
            {
               
                PropertyInfo property = type.GetProperty(mapping.Source);
                if (property == null &&
                    mapping.Source.Contains(PlanogramProduct.CustomAttributesProperty.Name))
                {
                    String[] columnMapping = mapping.Source.Split('.');
                    property = typeof(CustomAttributeDataDto).GetProperty(columnMapping[1]);

                    if (property == null) continue;
                    CustomAttributeDataDto attribute = this._dalCache.GetCustomAttributeDataDto((Byte)customType, id, false);

                    if (attribute != null)
                    {
                        InsertValue(mapping.Target, property.GetValue(attribute, null));
                    }
                }
                else
                {
                    if (property == null) continue;
                    InsertValue(mapping.Target, property.GetValue(dto, null));
                }
            }
        }
        private static Boolean ValidateDto(PlanogramComponentDto dto)
        {
            //  Make sure the dto is valid. No point in importing something they cannot save right away.
            if (String.IsNullOrWhiteSpace(dto.Name)) dto.Name = dto.Id.ToString();

            return true;
        }

        private static Boolean ValidateDto(PlanogramDto dto)
        {
            //  Make sure the dto is valid. No point in importing something they cannot save right away.
            if (String.IsNullOrWhiteSpace(dto.Name)) dto.Name = Message.Planogram_DefaultPlanName;

            return true;
        }

        private static Boolean ValidateDto(PlanogramFixtureDto dto)
        {
            //  Make sure the dto is valid. No point in importing something they cannot save right away.
            if (String.IsNullOrWhiteSpace(dto.Name)) dto.Name = Message.PlanogramFixture_Name_Default;

            return true;
        }

        private Dictionary<Object, DalCacheItem> InitializePerformanceCacheItems()
        {
            //  Check that this is invoked for a Planogram DAL cache item.
            if (!IsType(DalCacheItemType.Planogram))
            {
                Debug.Fail("InitializePerformanceCacheItems should be invoked only for Planogram DAL cache items.");
                return new Dictionary<Object, DalCacheItem>();
            }

            return Children[DalCacheItemType.Performance].ToDictionary(NewPerformanceProductKey, item => item);
        }

        #endregion

        #region Dispose

        /// <summary>
        ///     Called when this instance is disposing
        /// </summary>
        public void Dispose()
        {
            foreach (List<DalCacheItem> childList in _children.Values)
            {
                foreach (DalCacheItem child in childList)
                    child.Dispose();
            }
            _parent = null;
            Id = null;
            _data = null;
            _children = null;


            _dataFields = null;
            _planogramProductDtoById = null;
            _performanceCacheItems = null;
            _cacheItemType = null;
            _planogramProductForPerformance = null;
        }

        #endregion

        #endregion
    }
}