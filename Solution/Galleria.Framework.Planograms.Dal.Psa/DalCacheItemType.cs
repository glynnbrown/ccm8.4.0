﻿#region Header Information

// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
#endregion

#region Version History: CCM820

// V8-30724 : A.Silva
//  Added JDA import related methods to fetch the type of cache item.
// V8-31030 : A.Silva
//  Added Drawing related Enums.

#endregion

#endregion

using System;
using System.Linq;

namespace Galleria.Framework.Planograms.Dal.Psa
{
    /// <summary>
    /// Indicates the dal cache item type
    /// </summary>
    public enum DalCacheItemType
    {
        Unknown,
        Project,
        Product,
        Planogram,
        Performance,
        Segment,
        Fixture,
        Position,
        Drawing,
        Point3D // 3DPoint in the file
    }

    internal static class DalCacheItemTypeHelper
    {
        /// <summary>
        ///     Returns the section identifier for the <param name="source" /> as it appears on the <c>ProSpace</c> file.
        /// </summary>
        /// <param name="source">The type of DAL cache item.</param>
        /// <returns></returns>
        private static String SectionName(this DalCacheItemType source)
        {
            switch (source)
            {
                case DalCacheItemType.Unknown:
                    return "Unknown";
                case DalCacheItemType.Project:
                    return "Project";
                case DalCacheItemType.Product:
                    return "Product";
                case DalCacheItemType.Planogram:
                    return "Planogram";
                case DalCacheItemType.Performance:
                    return "Performance";
                case DalCacheItemType.Segment:
                    return "Segment";
                case DalCacheItemType.Fixture:
                    return "Fixture";
                case DalCacheItemType.Position:
                    return "Position";
                case DalCacheItemType.Drawing:
                    return "Drawing";
                //  NB: not yet importing polygon shapes so this one stays commented to ignore those lines.
                //case DalCacheItemType.Point3D:
                //    return "3DPoint";
                default:
                    return "Unknown";
            }
        }

        internal static Boolean IsTypeFor(this DalCacheItemType source, String data)
        {
            return String.Equals(data, source.SectionName(), StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        ///     Determines the type of item held within a string
        /// </summary>
        internal static DalCacheItemType GetTypeFor(String data)
        {
            if (String.IsNullOrEmpty(data)) return DalCacheItemType.Unknown;
            data = data.Split(',')[0];
            return Enum.GetValues(typeof(DalCacheItemType)).Cast<DalCacheItemType>().FirstOrDefault(itemType => itemType.IsTypeFor(data));
        }

    }
}
