﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
#endregion
#endregion

using Galleria.Framework.Dal;

namespace Galleria.Framework.Planograms.Dal.Psa
{
    public class DalBase : Galleria.Framework.Dal.DalBase
    {
        #region Methods
        /// <summary>
        /// The context for this dal
        /// </summary>
        public DalContext DalContext
        {
            get { return (DalContext)((IDal)this).DalContext; }
            set { ((IDal)this).DalContext = value; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Called when disposing of this instance
        /// </summary>
        public override void Dispose()
        {
            // Nothing to do
        }
        #endregion
    }
}
