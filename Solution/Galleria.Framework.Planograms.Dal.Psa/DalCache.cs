﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
#endregion
#region Version History: CCM820
// V8-30754 : A.Silva
//  Added DTO Cache lists for each section in the JDA Space Planning file.
// V8-31030 : A.Silva
//  Added Dto getching for Annotations.
// V8-31023 : A.Silva
//  Added code where necessary to allow accesing the JDA Space Planning file as if it only contained one planogram.
// V8-31093 : A.Silva
//  refactored to use ImportContext.
// V8-31276 : A.Silva
//  Added logging of GTIN duplicates and handling.
// V8-31360 : A.Silva
//  Amended Open so that files are open read only which will allow opening them even if in use.
// V8-31456 : N.Foster
//  Added method to fetch a batch of custom attributes
#endregion

#region Version History : CCM830
// V8-31546 : M.Brumby
//  PCR01420 Export to JDA
// V8-32283 : M.Pettit
//  Added start/complete logs messages
// V8-32304 : M.Brumby
//  Performance key changed to id rather than planogramid
// V8-32353 : M.Brumby
//  Temp file creation
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.External;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Planograms.Dal.Psa.Resources;

namespace Galleria.Framework.Planograms.Dal.Psa
{
    /// <summary>
    /// Class used to cache and hold information
    /// relating to a dal factory
    /// </summary>
    public class DalCache : IDisposable
    {
        #region Fields
        private readonly object _lock = new object(); // Object used for locking
        private readonly string _filePath; // Holds the full file path
        private FileStream _fileStream; // The file stream to the file
        private DalCacheItem _packageItem; // Holds the root package item
        private ExportContext _exportContext = new ExportContext();
        private readonly string _planName; // The name of the plan currently active in the JDA Space Planning file (if accessing in one plan per file mode).

        #region Dto Lookup Dictionaries

        private bool _dtosPreFetched;

        private readonly List<CustomAttributeDataDto> _customAttributeDataDtos = new List<CustomAttributeDataDto>();
        private readonly Dictionary<object, PackageDto> _packageDtos = new Dictionary<object, PackageDto>();
        private readonly Dictionary<object, List<PlanogramAnnotationDto>> _planogramAnnotationDtos = new Dictionary<object, List<PlanogramAnnotationDto>>();
        private readonly Dictionary<object, List<PlanogramAssemblyDto>> _planogramAssemblyDtos = new Dictionary<object, List<PlanogramAssemblyDto>>();
        private readonly Dictionary<object, PlanogramAssortmentDto> _assortmentDtos = new Dictionary<object, PlanogramAssortmentDto>();
        private readonly Dictionary<object, List<PlanogramAssortmentInventoryRuleDto>> _planogramAssortmentInventoryRuleDtos = new Dictionary<object, List<PlanogramAssortmentInventoryRuleDto>>();
        private readonly Dictionary<object, List<PlanogramAssortmentLocationBuddyDto>> _planogramAssortmentLocationBuddyDtos = new Dictionary<object, List<PlanogramAssortmentLocationBuddyDto>>();
        private readonly Dictionary<object, List<PlanogramAssortmentProductBuddyDto>> _planogramAssortmentProductBuddyDtos = new Dictionary<object, List<PlanogramAssortmentProductBuddyDto>>();
        private readonly Dictionary<object, List<PlanogramAssortmentLocalProductDto>> _planogramAssortmentLocalProductDtos = new Dictionary<object, List<PlanogramAssortmentLocalProductDto>>();
        private readonly Dictionary<object, List<PlanogramAssortmentProductDto>> _planogramAssortmentProductDtos = new Dictionary<object, List<PlanogramAssortmentProductDto>>();
        private readonly Dictionary<object, List<PlanogramAssortmentRegionDto>> _planogramAssortmentRegionDtos = new Dictionary<object, List<PlanogramAssortmentRegionDto>>();
        private readonly Dictionary<object, List<PlanogramBlockingDto>> _planogramBlockingDtos = new Dictionary<object, List<PlanogramBlockingDto>>();
        private readonly Dictionary<object, List<PlanogramComponentDto>> _planogramComponentDtos = new Dictionary<object, List<PlanogramComponentDto>>();
        private readonly Dictionary<object, PlanogramConsumerDecisionTreeDto> _planogramConsumerDecisionTreeDtos = new Dictionary<object, PlanogramConsumerDecisionTreeDto>();
        private readonly Dictionary<object, List<PlanogramConsumerDecisionTreeLevelDto>> _planogramConsumerDecisionTreeLevelDtos = new Dictionary<object, List<PlanogramConsumerDecisionTreeLevelDto>>();
        private readonly Dictionary<object, List<PlanogramConsumerDecisionTreeNodeDto>> _planogramConsumerDecisionTreeNodeDtos = new Dictionary<object, List<PlanogramConsumerDecisionTreeNodeDto>>();
        private readonly Dictionary<object, List<PlanogramDto>> _planogramDtos = new Dictionary<object, List<PlanogramDto>>();
        private readonly List<PlanogramEventLogDto> _planogramEventLogDtos = new List<PlanogramEventLogDto>();
        private readonly Dictionary<object, List<PlanogramFixtureAssemblyDto>> _planogramFixtureAssemblyDtos = new Dictionary<object, List<PlanogramFixtureAssemblyDto>>();
        private readonly Dictionary<object, List<PlanogramFixtureComponentDto>> _planogramFixtureComponentDtos = new Dictionary<object, List<PlanogramFixtureComponentDto>>();
        private readonly Dictionary<object, List<PlanogramFixtureDto>> _planogramFixtureDtos = new Dictionary<object, List<PlanogramFixtureDto>>();
        private readonly Dictionary<object, List<PlanogramFixtureItemDto>> _planogramFixtureItemDtos = new Dictionary<object, List<PlanogramFixtureItemDto>>();
        private readonly Dictionary<object, List<PlanogramImageDto>> _planogramImageDtos = new Dictionary<object, List<PlanogramImageDto>>();
        private readonly Dictionary<object, PlanogramInventoryDto> _planogramInventoryDtos = new Dictionary<object, PlanogramInventoryDto>();
        private readonly Dictionary<object, List<PlanogramMetadataImageDto>> _planogramMetadataImageDtos = new Dictionary<object, List<PlanogramMetadataImageDto>>();
        private readonly Dictionary<object, PlanogramPerformanceDto> _planogramPerformanceDtos = new Dictionary<object, PlanogramPerformanceDto>();
        private readonly Dictionary<object, List<PlanogramPerformanceDataDto>> _planogramPerformanceDataDtos = new Dictionary<object, List<PlanogramPerformanceDataDto>>();
        private readonly Dictionary<object, List<PlanogramPerformanceMetricDto>> _planogramPerformanceMetricDtos = new Dictionary<object, List<PlanogramPerformanceMetricDto>>();
        private readonly Dictionary<object, List<PlanogramPositionDto>> _planogramPositionDtos = new Dictionary<object, List<PlanogramPositionDto>>();
        private readonly Dictionary<object, List<PlanogramProductDto>> _planogramProductDtos = new Dictionary<object, List<PlanogramProductDto>>();
        private readonly Dictionary<object, PlanogramRenumberingStrategyDto> _planogramRenumberingStrategyDtos = new Dictionary<object, PlanogramRenumberingStrategyDto>();
        private readonly Dictionary<object, PlanogramSequenceDto> _planogramSequenceDto = new Dictionary<object, PlanogramSequenceDto>();
        private readonly Dictionary<object, List<PlanogramSequenceGroupDto>> _planogramSequenceGroupDtos = new Dictionary<object, List<PlanogramSequenceGroupDto>>();
        private readonly Dictionary<object, List<PlanogramSubComponentDto>> _planogramSubComponentDtos = new Dictionary<object, List<PlanogramSubComponentDto>>();
        private readonly Dictionary<object, PlanogramValidationTemplateDto> _planogramValidationTemplateDto = new Dictionary<object, PlanogramValidationTemplateDto>();
        private readonly Dictionary<object, List<PlanogramValidationTemplateGroupDto>> _planogramValidationTemplateGroupDtos = new Dictionary<object, List<PlanogramValidationTemplateGroupDto>>();        
        private readonly Dictionary<object, List<string>> _assignedProductGtinsByPlanogramId = new Dictionary<object, List<string>>();

        #endregion

        private readonly Dictionary<object, int> _friendlyComponentIDs = new Dictionary<object, int>();
        private readonly Dictionary<object, int> _friendlyAssemblyIDs = new Dictionary<object, int>();

        #endregion

        #region Properties
        /// <summary>
        /// Returns the path to the cache file
        /// </summary>
        public string FilePath
        {
            get { return _filePath; }
        }

        public ExportContext ExportMappingContext
        {
            get
            {
                return _exportContext;
            }
            set
            {
                _exportContext = value;
            }
        }

        /// <summary>
        /// Returns the root package item
        /// </summary>
        public DalCacheItem PackageItem
        {
            get
            {
                // create the package item if required
                if (_packageItem == null)
                    lock (_lock)
                        if (_packageItem == null)
                            _packageItem = new DalCacheItem(_filePath, _fileStream, this);

                // return the package item
                return _packageItem;
            }
        }

        private List<DalCacheItem> PlanogramCacheItems
        {
            get
            {
                return PackageItem != null && PackageItem.Children.ContainsKey(DalCacheItemType.Planogram)
                           ? PackageItem.Children[DalCacheItemType.Planogram]
                           : new List<DalCacheItem>();
            }
        }

        private List<DalCacheItem> FixtureCacheItems
        {
            get
            {
                return PlanogramCacheItems != null
                           ? PlanogramCacheItems.Where(item => item.Children.ContainsKey(DalCacheItemType.Fixture))
                                                .SelectMany(item => item.Children[DalCacheItemType.Fixture]).ToList()
                           : new List<DalCacheItem>();
            }
        }

        private List<DalCacheItem> SegmentCacheItems
        {
            get
            {
                return PlanogramCacheItems != null
                           ? PlanogramCacheItems.Where(item => item.Children.ContainsKey(DalCacheItemType.Segment))
                                                .SelectMany(item => item.Children[DalCacheItemType.Segment]).ToList()
                           : new List<DalCacheItem>();
            }
        }

        private List<DalCacheItem> ProductCacheItems
        {
            get
            {
                return PackageItem != null && PackageItem.Children.ContainsKey(DalCacheItemType.Product)
                           ? PackageItem.Children[DalCacheItemType.Product]
                           : new List<DalCacheItem>();
            }
        }

        public ReadOnlyCollection<PlanogramFieldMappingDto> FieldMappings { get; set; }
        public ReadOnlyCollection<PlanogramMetricMappingDto> MetricMappings { get; set; }

        public List<PlanogramEventLogDto> PlanogramEventLogDtos { get { return _planogramEventLogDtos; } }
        public Dictionary<object, List<string>> AssignedProductGtinsByPlanogramId { get { return _assignedProductGtinsByPlanogramId; } }

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public DalCache(DalFactoryConfigElement dalFactoryConfig)
        {
            _filePath = dalFactoryConfig.Name;
            int planNameIndex = _filePath.IndexOf("::", StringComparison.InvariantCultureIgnoreCase);
            if (planNameIndex == -1) return;

            //  Split the plan name and the real file path.
            _planName = _filePath.Substring(planNameIndex + 2, _filePath.Length - planNameIndex - 2);
            _filePath = _filePath.Substring(0, planNameIndex);
        }

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is disposed
        /// </summary>
        public void Dispose()
        {
            Close();
        }

        /// <summary>
        /// Opens and locks the file ready for reading
        /// </summary>
        public void Open(bool readOnly)
        {
            lock (_lock)
            {
                ClearDtos(true);

                //  Open the file in readonly mode.
                if (_fileStream == null)
                {
                    if (readOnly)
                    {
                        _fileStream = new FileStream(_filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    }
                    else
                    {
                        //create a backup of the existing file, if any
                        SpacePlanningExportHelper.CreateExportFileBackup(_filePath);

                        _fileStream = new FileStream(_filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
                    }
                }
                else
                {
                    _fileStream.Close();

                    if (readOnly)
                    {
                        _fileStream = new FileStream(_filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    }
                    else
                    {
                        //create a backup of the existing file, if any
                        SpacePlanningExportHelper.CreateExportFileBackup(_filePath);

                        _fileStream = new FileStream(_filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
                    }
                }
            }
        }

        /// <summary>
        /// Closes and releases the cached file
        /// </summary>
        public void Close()
        {
            lock (_lock)
            {
                // dispose of the file stream
                if (_fileStream != null) _fileStream.Dispose();
                _fileStream = null;

                // dispose of the cache items
                if (_packageItem != null) _packageItem.Dispose();
                _packageItem = null;

                ClearDtos(true);

                if (ExportMappingContext.ExportEvents.Any(e => (PlanogramEventLogEntryType)e.EntryType == PlanogramEventLogEntryType.Error))
                {
                    //Restore the original file, if any, to its location
                    SpacePlanningExportHelper.RestoreExportFileBackup(_filePath);
                }
                else
                {
                    //Delete the old backup file as it is no longer required
                    SpacePlanningExportHelper.DeleteExportFileBackup(_filePath);
                }
            }
        }

        private void ClearDtos(bool resetPreFetched)
        {
            if (resetPreFetched) _dtosPreFetched = false;
            
            _customAttributeDataDtos.Clear();
            _packageDtos.Clear();
            _planogramAnnotationDtos.Clear();
            _planogramAssemblyDtos.Clear();
            _assortmentDtos.Clear();
            _planogramAssortmentInventoryRuleDtos.Clear();
            _planogramAssortmentLocationBuddyDtos.Clear();
            _planogramAssortmentProductBuddyDtos.Clear();
            _planogramAssortmentLocalProductDtos.Clear();
            _planogramAssortmentProductDtos.Clear();
            _planogramAssortmentRegionDtos.Clear();
            _planogramBlockingDtos.Clear();
            _planogramComponentDtos.Clear();
            _planogramConsumerDecisionTreeDtos.Clear();
            _planogramConsumerDecisionTreeLevelDtos.Clear();
            _planogramConsumerDecisionTreeNodeDtos.Clear();
            _planogramDtos.Clear();
            _planogramEventLogDtos.Clear();
            _planogramFixtureAssemblyDtos.Clear();
            _planogramFixtureComponentDtos.Clear();
            _planogramFixtureDtos.Clear();
            _planogramFixtureItemDtos.Clear();
            _planogramImageDtos.Clear();
            _planogramInventoryDtos.Clear();
            _planogramMetadataImageDtos.Clear();
            _planogramPerformanceDtos.Clear();
            _planogramPerformanceDataDtos.Clear();
            _planogramPerformanceMetricDtos.Clear();
            _planogramPositionDtos.Clear();
            _planogramProductDtos.Clear();
            _planogramRenumberingStrategyDtos.Clear();
            _planogramSequenceDto.Clear();
            _planogramSequenceGroupDtos.Clear();
            _planogramSubComponentDtos.Clear();
            _planogramValidationTemplateDto.Clear();
            _planogramValidationTemplateGroupDtos.Clear();
            _assignedProductGtinsByPlanogramId.Clear();
            
        }

        internal void LogWarning(string description, string content, PlanogramEventLogAffectedType affectedType)
        {
            if (ExportMappingContext.ViewErrorLogsOnCompletion)
            {
                SpacePlanningExportHelper.CreateEventLogEntry(PackageItem.Id, ExportMappingContext.ExportEvents, description, content, PlanogramEventLogEntryType.Warning, affectedType);
            }
        }

        /// <summary>
        /// Log message to the export logs
        /// </summary>
        /// <param name="description"></param>
        /// <param name="content"></param>
        /// <param name="affectedType"></param>
        /// <param name="entryType"></param>
        private void AddExportEventLogEntry(string description, string content, PlanogramEventLogAffectedType affectedType, PlanogramEventLogEntryType entryType = PlanogramEventLogEntryType.Warning)
        {
            SpacePlanningExportHelper.CreateEventLogEntry(PackageItem.Id, ExportMappingContext.ExportEvents, description, content, entryType, PlanogramEventLogEventType.Export, affectedType, 1);
        }

        /// <summary>
        /// Gets the planogram name of the first source planogram to export
        /// </summary>
        /// <returns>The planogram name</returns>
        private string GetSourcePlanogramName()
        {
            var firstPlanogram = PackageItem.Children[DalCacheItemType.Planogram].FirstOrDefault();
            if (firstPlanogram != null && firstPlanogram.Dtos.ContainsKey(typeof(PlanogramDto)))
            {
                var planDto = firstPlanogram.Dtos[typeof(PlanogramDto)] as PlanogramDto;
                if (planDto != null)
                {
                    return planDto.Name;
                }
            }
            //if we didn't find it, default to the exported plan file name
            return _planName;
        }

        public void ExportPackage()
        {
            try
            {
                lock (_lock)
                {
                    //Log start
                    var sourceplanogramName = GetSourcePlanogramName();
                    AddExportEventLogEntry(
                        Language.ExportLog_StartingExport_Description,
                        string.Format(Language.ExportLog_StartingExport_Content, sourceplanogramName),
                        PlanogramEventLogAffectedType.Planogram, PlanogramEventLogEntryType.Information);

                    //Don't export if there are already errors
                    if (!ExportMappingContext.ExportEvents.Any(e => (PlanogramEventLogEntryType)e.EntryType == PlanogramEventLogEntryType.Error))
                    {
                        using (TextWriter textWriter = new StreamWriter(_fileStream))
                        {
                            //Create the default header info for a prospace file
                            textWriter.WriteLine("PROSPACE SCHEMATIC FILE");
                            textWriter.WriteLine($"; Version {ProSpaceImportHelper.SupportedVersions.First()}");

                            //Write the package data line
                            textWriter.WriteLine(_packageItem.Data);

                            //If we have products write a data line for each product.
                            //This must happen right after the package line has been written
                            if (_packageItem.Children.ContainsKey(DalCacheItemType.Product))
                            {
                                foreach (var product in _packageItem.Children[DalCacheItemType.Product])
                                {
                                    textWriter.WriteLine(product.Data);
                                }
                            }

                            //If we have performance write out a data line for each performance record that has been
                            //created agains the package. Performance can be stored at the package level where it will
                            //be used by all planograms or at the planogram level.
                            if (_packageItem.Children.ContainsKey(DalCacheItemType.Performance) && ExportMappingContext.IncludePerformanceData)
                            {
                                foreach (var performance in _packageItem.Children[DalCacheItemType.Performance])
                                {
                                    textWriter.WriteLine(performance.Data);
                                }
                            }

                            //If we have planograms then write out all the data in the planogram
                            if (_packageItem.Children.ContainsKey(DalCacheItemType.Planogram))
                            {
                                foreach (var item in _packageItem.Children[DalCacheItemType.Planogram])
                                {
                                    //write the planogram data line out
                                    textWriter.WriteLine(item.Data);

                                    //If this planogram has performance then write a line out for each record
                                    //this must come right after the planogram data line if there are any records.
                                    if (item.Children.ContainsKey(DalCacheItemType.Performance) && ExportMappingContext.IncludePerformanceData)
                                    {
                                        foreach (var performance in item.Children[DalCacheItemType.Performance])
                                        {
                                            textWriter.WriteLine(performance.Data);
                                        }
                                    }

                                    //if we have segments (bays) then write out all the segment data lines in one go.
                                    //You would think the fixture (subComponents) records would be under their related
                                    //segment but they are not. All segments must be written out in a group together.
                                    if (item.Children.ContainsKey(DalCacheItemType.Segment))
                                    {
                                        foreach (var segment in item.Children[DalCacheItemType.Segment].OrderBy(p => p.ExportOrder))
                                        {
                                            textWriter.WriteLine(segment.Data);
                                        }
                                    }
                                    else
                                    {
                                        //if we have no segments then just skip out of this planogram.
                                        continue;
                                    }

                                    //If we have fixtures (subComponents) then write out a line for each one.
                                    //each fixture will have any positions writen out directly under its data
                                    //line.
                                    if (item.Children.ContainsKey(DalCacheItemType.Fixture))
                                    {
                                        //Anything that isn't a backboard or base as these are integrated into the planogram info
                                        foreach (var fixture in item.Children[DalCacheItemType.Fixture].Where(f => !f.IsBackBoardOrBase))
                                        {
                                            //write out the fixture data line
                                            textWriter.WriteLine(fixture.Data);

                                            //if we have no positions skip to the next fixture
                                            if (!fixture.Children.ContainsKey(DalCacheItemType.Position)) continue;

                                            //write out any position lines. These must be directly under their related fixture,
                                            //unlike the relationship between segments and fixgtures.
                                            foreach (var position in fixture.Children[DalCacheItemType.Position].OrderBy(p => p.ExportOrder))
                                            {
                                                textWriter.WriteLine(position.Data);
                                            }
                                        }
                                    }

                                    //if we have any drawings (textboxes) then these must all be written out
                                    //at the end of the planogram information.
                                    if (item.Children.ContainsKey(DalCacheItemType.Drawing))
                                    {
                                        foreach (var drawing in item.Children[DalCacheItemType.Drawing])
                                        {
                                            textWriter.WriteLine(drawing.Data);
                                        }
                                    }
                                }
                            }

                        }
                    }

                    //Log end
                    AddExportEventLogEntry(
                        Language.ExportLog_ExportComplete_Description,
                        string.Format(Language.ExportLog_ExportComplete_Content, sourceplanogramName, FilePath),
                        PlanogramEventLogAffectedType.Planogram, PlanogramEventLogEntryType.Information);

                }
            }
            catch (Exception ex)
            {
                if (ExportMappingContext.ViewErrorLogsOnCompletion)
                {
                    //Log Errors
                    SpacePlanningExportHelper.CreateErrorEventLogEntry(PackageItem.Id, ExportMappingContext.ExportEvents, ex.Source, ex.Message, PlanogramEventLogAffectedType.Planogram);
                }
                else
                {
                    //Kill off the file stream just to be sure
                    _fileStream?.Dispose();
                    _fileStream = null;

                    //re-throw the error so it is handled normally
                    throw;
                }
            }
            finally
            {
                Close();
            }
        }

        public void RefreshCache(ReadOnlyCollection<PlanogramFieldMappingDto> fieldMappings,
                                 ReadOnlyCollection<PlanogramMetricMappingDto> metricMappings)
        {
            FieldMappings = fieldMappings;
            MetricMappings = metricMappings;
            if (_packageItem == null) return;

            ClearDtos(false);

            _packageItem.RefreshCache();
        }

        #endregion

        #region Dto Access Helpers

        public CustomAttributeDataDto GetCustomAttributeDataDto(byte parentType, object parentId, bool canCreate = true)
        {
            if (!_customAttributeDataDtos.Any() && canCreate)
                _customAttributeDataDtos.AddRange(PackageItem.GetDto<List<CustomAttributeDataDto>>());

            return _customAttributeDataDtos.FirstOrDefault(dto => dto.ParentType == parentType && Equals(dto.ParentId, parentId)) ??
                   new CustomAttributeDataDto { Id = IdentityHelper.GetNextInt32(), ParentType = parentType, ParentId = parentId };
        }

        public IEnumerable<CustomAttributeDataDto> GetCustomAttributeDataDtos()
        {
            if (!_customAttributeDataDtos.Any())
                _customAttributeDataDtos.AddRange(PackageItem.GetDto<List<CustomAttributeDataDto>>());

            return _customAttributeDataDtos;
        }

        public void InsertCustomAttributeDataDto(CustomAttributeDataDto dto)
        {
            _customAttributeDataDtos.Add(dto);
        }

        public PackageDto GetPackageDto(object packageId)
        {
            PackageDto dto;
            if (_packageDtos.TryGetValue(packageId, out dto)) return dto;

            dto = PackageItem.GetDto<PackageDto>();
            _packageDtos.Add(packageId, dto);

            if (_dtosPreFetched) return dto;

            // Prefetch the dtos for this package.
            string id = PackageItem.Id;
            PrefetchDtosForPackageId(id);
            _dtosPreFetched = true;

            return dto;
        }

        public void InsertPackageDto(PackageDto dto)
        {
            ExportMappingContext = (dto.FieldMappings as ExportContext) ?? new ExportContext();

            string packageId = dto.Id.ToString();
            if (_packageDtos.Keys.Any(k => k.Equals(packageId))) throw new Exception("Item already exists");

            _packageItem?.Dispose();
            _packageItem = new DalCacheItem(null, packageId, this, DalCacheItemType.Project);
            _packageItem.InsertDto(dto);
            _packageDtos.Add(packageId, dto);
        }


        private void PrefetchDtosForPackageId(string id)
        {

            foreach (object planogramId in GetPlanogramDtos(id).Select(dto => dto.Id))
            {
                GetCustomAttributeDataDto((byte)CustomAttributeDataPlanogramParentType.Planogram, planogramId);

                foreach (object dtoId in GetPlanogramProductDtos(planogramId).Select(dto => dto.Id))
                    GetCustomAttributeDataDto((byte)CustomAttributeDataPlanogramParentType.PlanogramProduct, dtoId);

                foreach (object planogramFixtureDtoId in GetPlanogramFixtureDtos(planogramId).Select(dto => dto.Id))
                {
                    GetPlanogramFixtureAssemblyDtos(planogramFixtureDtoId);
                    GetPlanogramFixtureComponentDtos(planogramFixtureDtoId);
                }

                GetPlanogramFixtureItemDtos(planogramId);

                foreach (object dtoId in GetPlanogramComponentDtos(planogramId).Select(dto => dto.Id))
                {
                    GetCustomAttributeDataDto((byte)CustomAttributeDataPlanogramParentType.PlanogramComponent, dtoId);
                    GetPlanogramSubComponentDtos(dtoId);
                }

                GetAssemblyDtos(planogramId);

                object assortmentDtoId = GetAssortmentDto(planogramId).Id;
                GetPlanogramAssortmentLocalProductDtos(assortmentDtoId);
                GetPlanogramAssortmentProductDtos(assortmentDtoId);
                GetPlanogramAssortmentRegionDtos(assortmentDtoId);

                GetPlanogramAnnotationDtos(planogramId);

                GetPlanogramBlockingDtos(planogramId);

                object planogramConsumerDecisionTreeDtoId = GetPlanogramConsumerDecisionTreeDto(planogramId).Id;
                GetPlanogramConsumerDecisionTreeLevelDtos(planogramConsumerDecisionTreeDtoId);
                foreach (object dtoId in GetPlanogramConsumerDecisionTreeNodeDtos(planogramConsumerDecisionTreeDtoId).Select(dto => dto.Id))
                    GetPlanogramConsumerDecisionTreeNodeProductDtos(dtoId);

                GetPlanogramPositionDtos(planogramId);

                GetPlanogramInventoryDto(planogramId);

                GetPlanogramImageDtos(planogramId);
                GetPlanogramMetadataImageDtos(planogramId);

                object planogramPerformanceDtoId = GetPlanogramPerformanceDto(planogramId).Id;
                GetPlanogramPerformanceDataDtos(planogramPerformanceDtoId);
                GetPlanogramPerformanceMetricDtos(planogramPerformanceDtoId);


                object planogramSequenceDtoId = GetSequenceDto(planogramId).Id;
                GetPlanogramSequenceGroupDtos(planogramSequenceDtoId);

                object planogramValidationTemplateDtoId = GetValidationTemplateDto(planogramId).Id;
                GetPlanogramValidationTemplateGroupDtos(planogramValidationTemplateDtoId);

                GetRenumberingStrategyDto(planogramId);

                // Fetch the Event Logs last to get all created with any DTO.
                GetPlanogramEventLogDtos(planogramId);
            }

            ClearDtos(false);
        }

        public IEnumerable<PlanogramAnnotationDto> GetPlanogramAnnotationDtos(object planogramId)
        {
            List<PlanogramAnnotationDto> dtos;
            if (_planogramAnnotationDtos.TryGetValue(planogramId, out dtos)) return dtos;

            dtos = GetTextCacheItems(planogramId).Select(item => item.GetDto<PlanogramAnnotationDto>()).Where(item => item != null).ToList();
            _planogramAnnotationDtos.Add(planogramId, dtos);
            return dtos;
        }

        public void InsertPlanogramAnnotationDto(PlanogramAnnotationDto dto)
        {
            DalCacheItem annotationItem = new DalCacheItem(null, dto.Id.ToString(), this, DalCacheItemType.Drawing);
            annotationItem.InsertDto(dto);

            if (dto.PlanogramFixtureItemId != null)
            {
                DalCacheItem segmentItem = GetSegmentCacheItemBySub(dto.PlanogramFixtureItemId.ToString());
                if (segmentItem != null)
                {
                    annotationItem.InsertDtos(segmentItem);
                }
            }
            if (dto.PlanogramSubComponentId != null)
            {
                DalCacheItem componentItem = GetFixtureCacheItemSubId(dto.PlanogramSubComponentId.ToString());
                if (componentItem != null)
                {
                    annotationItem.InsertDtos(componentItem);
                }
            }

            DalCacheItem planogramItem = GetPlanogramCacheItem(dto.PlanogramId.ToString());
            if (planogramItem != null)
            {
                List<DalCacheItem> items;
                if (planogramItem.Children.TryGetValue(DalCacheItemType.Drawing, out items))
                {
                    items.Add(annotationItem);
                }
                else
                {
                    planogramItem.Children[DalCacheItemType.Drawing] = new List<DalCacheItem>() { annotationItem };
                }
            }
        }

        public IEnumerable<PlanogramAssemblyDto> GetAssemblyDtos(object planogramId)
        {
            List<PlanogramAssemblyDto> dtos;
            if (_planogramAssemblyDtos.TryGetValue(planogramId, out dtos)) return dtos;

            //  NB: Not importing any assemblies for now at least.
            dtos = new List<PlanogramAssemblyDto>(); //GetFixtureCacheItems(planogramId).Select(item => item.GetDto<PlanogramAssemblyDto>()).Where(item => item != null).ToList();
            _planogramAssemblyDtos.Add(planogramId, dtos);
            return dtos;
        }

        public PlanogramAssortmentDto GetAssortmentDto(object planogramId)
        {
            PlanogramAssortmentDto dto;
            if (_assortmentDtos.TryGetValue(planogramId, out dto)) return dto;

            dto = new PlanogramAssortmentDto
            {
                Id = IdentityHelper.GetNextInt32(),
                PlanogramId = planogramId,
                Name = "Default Assortment"
            };
            _assortmentDtos.Add(planogramId, dto);
            return dto;
        }

        public void InsertPlanogramAssortmentDto(PlanogramAssortmentDto dto)
        {
            PlanogramAssortmentDto testDto;
            if (_assortmentDtos.TryGetValue(dto.PlanogramId, out testDto)) return;
            _assortmentDtos.Add(dto.PlanogramId, dto);
        }

        public IEnumerable<PlanogramAssortmentLocalProductDto> GetPlanogramAssortmentLocalProductDtos(object assortmentId)
        {
            List<PlanogramAssortmentLocalProductDto> dtos;
            if (_planogramAssortmentLocalProductDtos.TryGetValue(assortmentId, out dtos)) return dtos;

            dtos = new List<PlanogramAssortmentLocalProductDto>();
            _planogramAssortmentLocalProductDtos.Add(assortmentId, dtos);
            return dtos;
        }

        public void InsertPlanogramAssortmentLocalProductDto(PlanogramAssortmentLocalProductDto dto)
        {
            List<PlanogramAssortmentLocalProductDto> dtos;
            if (_planogramAssortmentLocalProductDtos.TryGetValue(dto.PlanogramAssortmentId, out dtos))
            {
                dtos.Add(dto);
            }
            else
            {
                _planogramAssortmentLocalProductDtos[dto.PlanogramAssortmentId] = new List<PlanogramAssortmentLocalProductDto>() { dto };
            }
        }

        public IEnumerable<PlanogramAssortmentInventoryRuleDto> GetPlanogramAssortmentInventoryRuleDtos(object assortmentId)
        {
            List<PlanogramAssortmentInventoryRuleDto> dtos;
            if (_planogramAssortmentInventoryRuleDtos.TryGetValue(assortmentId, out dtos)) return dtos;

            dtos = new List<PlanogramAssortmentInventoryRuleDto>();
            _planogramAssortmentInventoryRuleDtos.Add(assortmentId, dtos);
            return dtos;
        }

        public void InsertPlanogramAssortmentInventoryRuleDto(PlanogramAssortmentInventoryRuleDto dto)
        {
            List<PlanogramAssortmentInventoryRuleDto> dtos;
            if (_planogramAssortmentInventoryRuleDtos.TryGetValue(dto.PlanogramAssortmentId, out dtos))
            {
                dtos.Add(dto);
            }
            else
            {
                _planogramAssortmentInventoryRuleDtos[dto.PlanogramAssortmentId] = new List<PlanogramAssortmentInventoryRuleDto>() { dto };
            }
        }

        public IEnumerable<PlanogramAssortmentProductBuddyDto> GetPlanogramAssortmentProductBuddyDtos(object assortmentId)
        {
            List<PlanogramAssortmentProductBuddyDto> dtos;
            if (_planogramAssortmentProductBuddyDtos.TryGetValue(assortmentId, out dtos)) return dtos;

            dtos = new List<PlanogramAssortmentProductBuddyDto>();
            _planogramAssortmentProductBuddyDtos.Add(assortmentId, dtos);
            return dtos;
        }

        public void InsertPlanogramAssortmentProductBuddyDto(PlanogramAssortmentProductBuddyDto dto)
        {
            List<PlanogramAssortmentProductBuddyDto> dtos;
            if (_planogramAssortmentProductBuddyDtos.TryGetValue(dto.PlanogramAssortmentId, out dtos))
            {
                dtos.Add(dto);
            }
            else
            {
                _planogramAssortmentProductBuddyDtos[dto.PlanogramAssortmentId] = new List<PlanogramAssortmentProductBuddyDto>() { dto };
            }
        }

        public IEnumerable<PlanogramAssortmentLocationBuddyDto> GetPlanogramAssortmentLocationBuddyDtos(object assortmentId)
        {
            List<PlanogramAssortmentLocationBuddyDto> dtos;
            if (_planogramAssortmentLocationBuddyDtos.TryGetValue(assortmentId, out dtos)) return dtos;

            dtos = new List<PlanogramAssortmentLocationBuddyDto>();
            _planogramAssortmentLocationBuddyDtos.Add(assortmentId, dtos);
            return dtos;
        }

        public void InsertPlanogramAssortmentLocationBuddyDto(PlanogramAssortmentLocationBuddyDto dto)
        {
            List<PlanogramAssortmentLocationBuddyDto> dtos;
            if (_planogramAssortmentLocationBuddyDtos.TryGetValue(dto.PlanogramAssortmentId, out dtos))
            {
                dtos.Add(dto);
            }
            else
            {
                _planogramAssortmentLocationBuddyDtos[dto.PlanogramAssortmentId] = new List<PlanogramAssortmentLocationBuddyDto>() { dto };
            }
        }

        public IEnumerable<PlanogramAssortmentProductDto> GetPlanogramAssortmentProductDtos(object assortmentId)
        {
            List<PlanogramAssortmentProductDto> dtos;
            if (_planogramAssortmentProductDtos.TryGetValue(assortmentId, out dtos)) return dtos;

            dtos = new List<PlanogramAssortmentProductDto>();
            _planogramAssortmentProductDtos.Add(assortmentId, dtos);
            return dtos;
        }

        public void InsertPlanogramAssortmentProductDto(PlanogramAssortmentProductDto dto)
        {
            List<PlanogramAssortmentProductDto> dtos;
            if (_planogramAssortmentProductDtos.TryGetValue(dto.PlanogramAssortmentId, out dtos))
            {
                dtos.Add(dto);
            }
            else
            {
                _planogramAssortmentProductDtos[dto.PlanogramAssortmentId] = new List<PlanogramAssortmentProductDto>() { dto };
            }
        }

        public IEnumerable<PlanogramAssortmentRegionDto> GetPlanogramAssortmentRegionDtos(object assortmentId)
        {
            List<PlanogramAssortmentRegionDto> dtos;
            if (_planogramAssortmentRegionDtos.TryGetValue(assortmentId, out dtos)) return dtos;

            dtos = new List<PlanogramAssortmentRegionDto>();
            _planogramAssortmentRegionDtos.Add(assortmentId, dtos);
            return dtos;
        }

        public void InsertPlanogramAssortmentRegionDto(PlanogramAssortmentRegionDto dto)
        {
            List<PlanogramAssortmentRegionDto> dtos;
            if (_planogramAssortmentRegionDtos.TryGetValue(dto.PlanogramAssortmentId, out dtos))
            {
                dtos.Add(dto);
            }
            else
            {
                _planogramAssortmentRegionDtos[dto.PlanogramAssortmentId] = new List<PlanogramAssortmentRegionDto>() { dto };
            }
        }

        public IEnumerable<PlanogramBlockingDto> GetPlanogramBlockingDtos(object planogramId)
        {
            List<PlanogramBlockingDto> dtos;
            if (_planogramBlockingDtos.TryGetValue(planogramId, out dtos)) return dtos;

            dtos = new List<PlanogramBlockingDto>();
            _planogramBlockingDtos.Add(planogramId, dtos);
            return dtos;
        }

        public IEnumerable<PlanogramComponentDto> GetPlanogramComponentDtos(object planogramId)
        {
            List<PlanogramComponentDto> dtos;
            if (_planogramComponentDtos.TryGetValue(planogramId, out dtos)) return dtos;

            IEnumerable<PlanogramComponentDto> planogramComponentDtosInFixture = GetSegmentCacheItems(planogramId).SelectMany(item => item.GetDto<List<PlanogramComponentDto>>()).Where(dto => dto != null);
            IEnumerable<PlanogramComponentDto> planogramComponentDtos = GetFixtureCacheItems(planogramId).Select(item => item.GetDto<PlanogramComponentDto>()).Where(item => item != null);
            dtos = planogramComponentDtosInFixture.Union(planogramComponentDtos).ToList();
            _planogramComponentDtos.Add(planogramId, dtos);
            return dtos;
        }

        public void InsertPlanogramComponentDto(PlanogramComponentDto dto)
        {
            string planogramId = dto.PlanogramId.ToString();
            DalCacheItem planItem = GetPlanogramCacheItem(planogramId);

            List<PlanogramComponentDto> dtos;
            if (_planogramComponentDtos.TryGetValue(planogramId, out dtos))
            {
                dtos.Add(dto);
            }
            else
            {
                _planogramComponentDtos[planogramId] = new List<PlanogramComponentDto>() { dto };
            }

            string componentId = dto.Id.ToString();
            DalCacheItem componentItem = GetFixtureCacheItem(componentId);
            if (componentItem == null)
            {
                componentItem = new DalCacheItem(planItem, componentId, this, DalCacheItemType.Fixture);
            }
            componentItem.InsertDto(dto);


            List<DalCacheItem> items;
            if (planItem.Children.TryGetValue(DalCacheItemType.Fixture, out items))
            {
                items.Add(componentItem);
            }
            else
            {
                planItem.Children[DalCacheItemType.Fixture] = new List<DalCacheItem>() { componentItem };
            }
        }

        public PlanogramConsumerDecisionTreeDto GetPlanogramConsumerDecisionTreeDto(object planogramId)
        {
            PlanogramConsumerDecisionTreeDto dto;
            if (_planogramConsumerDecisionTreeDtos.TryGetValue(planogramId, out dto)) return dto;

            dto = new PlanogramConsumerDecisionTreeDto
            {
                Id = IdentityHelper.GetNextInt32(),
                Name = "Default Consumer Decision Tree",
                PlanogramId = planogramId
            };
            _planogramConsumerDecisionTreeDtos.Add(planogramId, dto);
            return dto;
        }

        public IEnumerable<PlanogramConsumerDecisionTreeLevelDto> GetPlanogramConsumerDecisionTreeLevelDtos(object consumerDecisionTreeId)
        {
            List<PlanogramConsumerDecisionTreeLevelDto> dtos;
            if (_planogramConsumerDecisionTreeLevelDtos.TryGetValue(consumerDecisionTreeId, out dtos)) return dtos;

            dtos = new List<PlanogramConsumerDecisionTreeLevelDto>
                   {
                       new PlanogramConsumerDecisionTreeLevelDto
                       {
                           Id = IdentityHelper.GetNextInt32(),
                           Name = "Root",
                           PlanogramConsumerDecisionTreeId = consumerDecisionTreeId
                       }
                   };
            _planogramConsumerDecisionTreeLevelDtos.Add(consumerDecisionTreeId, dtos);
            return dtos;
        }

        public IEnumerable<PlanogramConsumerDecisionTreeNodeDto> GetPlanogramConsumerDecisionTreeNodeDtos(object consumerDecisionTreeId)
        {
            List<PlanogramConsumerDecisionTreeNodeDto> dtos;
            if (_planogramConsumerDecisionTreeNodeDtos.TryGetValue(consumerDecisionTreeId, out dtos)) return dtos;

            List<PlanogramConsumerDecisionTreeLevelDto> levelDtos;
            _planogramConsumerDecisionTreeLevelDtos.TryGetValue(consumerDecisionTreeId, out levelDtos);
            dtos = levelDtos != null
                       ? levelDtos.Select(item => new PlanogramConsumerDecisionTreeNodeDto
                       {
                           Id = IdentityHelper.GetNextInt32(),
                           Name = "All Products",
                           PlanogramConsumerDecisionTreeId = consumerDecisionTreeId,
                           PlanogramConsumerDecisionTreeLevelId = item.Id
                       }).ToList()
                       : new List<PlanogramConsumerDecisionTreeNodeDto>();
            _planogramConsumerDecisionTreeNodeDtos.Add(consumerDecisionTreeId, dtos);
            return dtos;
        }

        public IEnumerable<PlanogramDto> GetPlanogramDtos(object packageId)
        {
            if (string.IsNullOrEmpty(packageId as string)) return new List<PlanogramDto>();

            List<PlanogramDto> dtos;
            if (_planogramDtos.TryGetValue(packageId, out dtos)) return dtos;

            dtos = PlanogramCacheItems.Select(planogramItem => planogramItem.GetDto<PlanogramDto>()).Where(item => item != null && (_planName == null || string.Equals(item.Name, _planName, StringComparison.InvariantCultureIgnoreCase))).ToList();
            dtos.Reverse();

            _planogramDtos.Add(packageId, dtos);
            return dtos;
        }

        public void InsertPlanogramDto(PackageDto packageDto, PlanogramDto dto)
        {
            string packageId = packageDto.Id.ToString();

            if (_packageItem != null)
            {
                string planId = dto.Id.ToString();
                DalCacheItem planItem = new DalCacheItem(_packageItem, planId, this, DalCacheItemType.Planogram);
                planItem.InsertDto(dto);

                List<PlanogramDto> dtos;
                if (_planogramDtos.TryGetValue(packageId, out dtos))
                {
                    dtos.Add(dto);
                }
                else
                {
                    _planogramDtos[packageId] = new List<PlanogramDto>() { dto };
                }

                List<DalCacheItem> items;
                if (_packageItem.Children.TryGetValue(DalCacheItemType.Planogram, out items))
                {
                    items.Add(planItem);
                }
                else
                {
                    _packageItem.Children[DalCacheItemType.Planogram] = new List<DalCacheItem>() { planItem };
                }
            }
        }

        public IEnumerable<PlanogramEventLogDto> GetPlanogramEventLogDtos(object planogramId)
        {
            //  Get the planogram events and those that are common to all in the package.
            object id = planogramId;
            return PlanogramEventLogDtos.Where(dto => Equals(dto.PlanogramId, planogramId) || Equals(dto.PlanogramId, PackageItem.Id)).Select(arg => CloneForPlanogramId(arg, id));
        }

        private static PlanogramEventLogDto CloneForPlanogramId(PlanogramEventLogDto arg, object planogramId)
        {
            PlanogramEventLogDto logDto = SpacePlanningImportHelper.Clone(arg);
            logDto.PlanogramId = planogramId;
            return logDto;
        }

        public IEnumerable<PlanogramFixtureAssemblyDto> GetPlanogramFixtureAssemblyDtos(object planogramFixtureId)
        {
            List<PlanogramFixtureAssemblyDto> dtos;
            if (_planogramFixtureAssemblyDtos.TryGetValue(planogramFixtureId, out dtos)) return dtos;

            dtos = new List<PlanogramFixtureAssemblyDto>();
            _planogramFixtureAssemblyDtos.Add(planogramFixtureId, dtos);
            return dtos;
        }

        public IEnumerable<PlanogramFixtureComponentDto> GetPlanogramFixtureComponentDtos(object planogramFixtureId)
        {
            List<PlanogramFixtureComponentDto> dtos;
            if (_planogramFixtureComponentDtos.TryGetValue(planogramFixtureId, out dtos)) return dtos;

            dtos = new List<PlanogramFixtureComponentDto>();

            //  Get the base and backboard for the fixture.
            DalCacheItem planogramFixtureCacheItem = GetSegmentCacheItem(planogramFixtureId);
            if (planogramFixtureCacheItem != null)
            {
                //  Add the base if there is one.
                dtos.AddRange(planogramFixtureCacheItem.GetDto<List<PlanogramFixtureComponentDto>>().Where(item => item != null));
                //  Get all the components that belong to the fixture.
                dtos.AddRange(PlanogramCacheItems
                                  .SelectMany(item => item.Children[DalCacheItemType.Fixture])
                                  .Select(item => item.GetDto<PlanogramFixtureComponentDto>())
                                  .Where(item => item != null && Equals(item.PlanogramFixtureId, planogramFixtureId)));
            }
            _planogramFixtureComponentDtos.Add(planogramFixtureId, dtos);
            return dtos;
        }

        public void InsertPlanogramFixtureComponentDto(PlanogramFixtureComponentDto dto)
        {
            string fixtureId = dto.PlanogramFixtureId.ToString();
            DalCacheItem planItem = GetSegmentCacheItem(fixtureId).Parent;
            List<PlanogramFixtureComponentDto> dtos;
            if (_planogramFixtureComponentDtos.TryGetValue(fixtureId, out dtos))
            {
                dtos.Add(dto);
            }
            else
            {
                _planogramFixtureComponentDtos[fixtureId] = new List<PlanogramFixtureComponentDto>() { dto };
            }

            string componentId = dto.PlanogramComponentId.ToString();
            DalCacheItem componentItem = GetFixtureCacheItem(componentId);
            if (componentItem == null)
            {
                componentItem = new DalCacheItem(planItem, componentId, this, DalCacheItemType.Fixture);
                List<DalCacheItem> items;
                if (planItem.Children.TryGetValue(DalCacheItemType.Fixture, out items))
                {
                    items.Add(componentItem);
                }
                else
                {
                    planItem.Children[DalCacheItemType.Fixture] = new List<DalCacheItem>() { componentItem };
                }
            }
            componentItem.InsertDto(dto);
            componentItem.ExportOrder = dto.X;


        }

        public IEnumerable<PlanogramFixtureDto> GetPlanogramFixtureDtos(object planogramId)
        {
            List<PlanogramFixtureDto> dtos;
            if (_planogramFixtureDtos.TryGetValue(planogramId, out dtos)) return dtos;

            dtos = GetSegmentCacheItems(planogramId).Select(item => item.GetDto<PlanogramFixtureDto>()).Where(item => item != null).ToList();
            _planogramFixtureDtos.Add(planogramId, dtos);
            return dtos;
        }

        public void InsertPlanogramFixtureDto(PlanogramFixtureDto dto)
        {
            string planogramId = dto.PlanogramId.ToString();

            //DalCacheItem planItem = GetPlanogramCacheItem(planogramId);

            List<PlanogramFixtureDto> dtos;
            if (_planogramFixtureDtos.TryGetValue(planogramId, out dtos))
            {
                dtos.Add(dto);
            }
            else
            {
                _planogramFixtureDtos[planogramId] = new List<PlanogramFixtureDto>() { dto };
            }
        }

        public IEnumerable<PlanogramFixtureItemDto> GetPlanogramFixtureItemDtos(object planogramId)
        {
            List<PlanogramFixtureItemDto> dtos;
            if (_planogramFixtureItemDtos.TryGetValue(planogramId, out dtos)) return dtos;

            dtos = GetSegmentCacheItems(planogramId).Select(item => item.GetDto<PlanogramFixtureItemDto>()).Where(item => item != null).ToList();
            _planogramFixtureItemDtos.Add(planogramId, dtos);
            return dtos;
        }

        public void InsertPlanogramFixtureItemDto(PlanogramFixtureItemDto dto)
        {
            string planogramId = dto.PlanogramId.ToString();
            string fixtureId = dto.PlanogramFixtureId.ToString();

            List<PlanogramFixtureItemDto> dtos;
            if (_planogramFixtureItemDtos.TryGetValue(planogramId, out dtos))
            {
                dtos.Add(dto);
            }
            else
            {
                _planogramFixtureItemDtos[planogramId] = new List<PlanogramFixtureItemDto>() { dto };
            }

            DalCacheItem planItem = GetPlanogramCacheItem(planogramId);
            DalCacheItem fixtureItem = new DalCacheItem(planItem, fixtureId, this, DalCacheItemType.Segment);
            fixtureItem.InsertDto(dto);
            fixtureItem.ExportOrder = dto.X;
            fixtureItem.SubId = dto.Id.ToString();

            List<PlanogramFixtureDto> fixtureDtos;
            if (_planogramFixtureDtos.TryGetValue(planogramId, out fixtureDtos))
            {
                foreach (PlanogramFixtureDto fixture in _planogramFixtureDtos[planogramId].Where(f => f.Id.Equals(dto.PlanogramFixtureId)))
                {
                    fixtureItem.InsertDto(fixture);
                }
            }

            List<DalCacheItem> items;
            if (planItem.Children.TryGetValue(DalCacheItemType.Segment, out items))
            {
                items.Add(fixtureItem);
            }
            else
            {
                planItem.Children[DalCacheItemType.Segment] = new List<DalCacheItem>() { fixtureItem };
            }
        }

        public IEnumerable<PlanogramImageDto> GetPlanogramImageDtos(object planogramId)
        {
            List<PlanogramImageDto> dtos;
            if (_planogramImageDtos.TryGetValue(planogramId, out dtos)) return dtos;

            dtos = new List<PlanogramImageDto>();
            _planogramImageDtos.Add(planogramId, dtos);
            return dtos;
        }

        public PlanogramInventoryDto GetPlanogramInventoryDto(object planogramId)
        {
            PlanogramInventoryDto dto;
            if (_planogramInventoryDtos.TryGetValue(planogramId, out dto)) return dto;

            DalCacheItem planogramCacheItem = PlanogramCacheItems.FirstOrDefault(item => Equals(item.Id, planogramId));
            if (planogramCacheItem == null) return null;

            dto = planogramCacheItem.GetDto<PlanogramInventoryDto>();
            _planogramInventoryDtos.Add(planogramId, dto);
            return dto;
        }

        public void InsertPlanogramInventoryDto(PlanogramInventoryDto dto)
        {
            DalCacheItem planogramCacheItem = GetPlanogramCacheItem(dto.PlanogramId.ToString());
            planogramCacheItem.InsertDto(dto);
        }

        public IEnumerable<PlanogramMetadataImageDto> GetPlanogramMetadataImageDtos(object planogramId)
        {
            List<PlanogramMetadataImageDto> dtos;
            if (_planogramMetadataImageDtos.TryGetValue(planogramId, out dtos)) return dtos;

            dtos = new List<PlanogramMetadataImageDto>();
            _planogramMetadataImageDtos.Add(planogramId, dtos);
            return dtos;
        }

        public PlanogramPerformanceDto GetPlanogramPerformanceDto(object planogramId)
        {
            PlanogramPerformanceDto dto;
            if (_planogramPerformanceDtos.TryGetValue(planogramId, out dto)) return dto;

            DalCacheItem planogramCacheItem = PlanogramCacheItems.FirstOrDefault(item => Equals(item.Id, planogramId));
            if (planogramCacheItem == null) return null;

            dto = planogramCacheItem.GetDto<PlanogramPerformanceDto>();
            _planogramPerformanceDtos.Add(planogramId, dto);
            return dto;
        }

        public void InsertPlanogramPerformanceDto(PlanogramPerformanceDto dto)
        {
            if (!_planogramPerformanceDtos.ContainsKey(dto.Id))
            {
                _planogramPerformanceDtos.Add(dto.Id, dto);
            }
        }

        public IEnumerable<PlanogramPerformanceDataDto> GetPlanogramPerformanceDataDtos(object performanceId)
        {
            List<PlanogramPerformanceDataDto> dtos;
            if (_planogramPerformanceDataDtos.TryGetValue(performanceId, out dtos)) return dtos;

            dtos = ProductCacheItems.SelectMany(item => item.GetDto<List<PlanogramPerformanceDataDto>>()).Where(dto => dto != null && Equals(dto.PlanogramPerformanceId, performanceId)).ToList();
            _planogramPerformanceDataDtos.Add(performanceId, dtos);
            return dtos;
        }

        public void InsertPlanogramPerformanceDataDto(PlanogramPerformanceDataDto dto)
        {
            List<PlanogramPerformanceDataDto> dtos;
            if (_planogramPerformanceDataDtos.TryGetValue(dto.PlanogramPerformanceId, out dtos))
            {
                dtos.Add(dto);
            }
            else
            {
                _planogramPerformanceDataDtos[dto.PlanogramPerformanceId] = new List<PlanogramPerformanceDataDto>() { dto };
            }

            DalCacheItem performanceItem = new DalCacheItem(null, dto.Id.ToString(), this, DalCacheItemType.Performance);
            performanceItem.InsertDto(dto);

            DalCacheItem parentItem = _packageItem;

            PlanogramPerformanceDto performanceDto;
            if (_planogramPerformanceDtos.TryGetValue(dto.PlanogramPerformanceId, out performanceDto))
            {
                parentItem = GetPlanogramCacheItem(performanceDto.PlanogramId.ToString());

                List<PlanogramProductDto> productDtos;
                if (_planogramProductDtos.TryGetValue(parentItem.Id, out productDtos))
                {
                    PlanogramProductDto productDto = productDtos.FirstOrDefault(p => Equals(p.Id, dto.PlanogramProductId));
                    if (productDto != null)
                    {
                        performanceItem.InsertDto(productDto);
                    }
                }
            }

            List<DalCacheItem> items;
            if (parentItem.Children.TryGetValue(DalCacheItemType.Performance, out items))
            {
                items.Add(performanceItem);
            }
            else
            {
                parentItem.Children[DalCacheItemType.Performance] = new List<DalCacheItem>() { performanceItem };
            }
        }

        public IEnumerable<PlanogramPerformanceMetricDto> GetPlanogramPerformanceMetricDtos(object performanceId)
        {
            List<PlanogramPerformanceMetricDto> dtos;
            if (_planogramPerformanceMetricDtos.TryGetValue(performanceId, out dtos)) return dtos;

            DalCacheItem planogramCacheItem = PlanogramCacheItems.FirstOrDefault(item => Equals(item.Id, performanceId));
            if (planogramCacheItem == null) return null;

            dtos = planogramCacheItem.GetDto<List<PlanogramPerformanceMetricDto>>();
            _planogramPerformanceMetricDtos.Add(performanceId, dtos);
            return dtos;
        }

        public void InsertPlanogramPerformanceMetricDto(PlanogramPerformanceMetricDto dto)
        {
            List<PlanogramPerformanceMetricDto> dtos;
            if (_planogramPerformanceMetricDtos.TryGetValue(dto.PlanogramPerformanceId, out dtos))
            {
                dtos.Add(dto);
            }
            else
            {
                _planogramPerformanceMetricDtos[dto.PlanogramPerformanceId] = new List<PlanogramPerformanceMetricDto>() { dto };
            }
        }

        public IEnumerable<PlanogramPositionDto> GetPlanogramPositionDtos(object planogramId)
        {
            List<PlanogramPositionDto> dtos;
            if (_planogramPositionDtos.TryGetValue(planogramId, out dtos)) return dtos;

            DalCacheItem planogramCacheItem =
                PlanogramCacheItems.FirstOrDefault(item => Equals(item.GetDto<PlanogramDto>().Id, planogramId));
            dtos = planogramCacheItem != null
                       ? planogramCacheItem.Children[DalCacheItemType.Fixture].SelectMany(fixture => fixture.Children[DalCacheItemType.Position].Select(item => item.GetDto<PlanogramPositionDto>())).Where(item => item != null).ToList()
                       : new List<PlanogramPositionDto>();
            _planogramPositionDtos.Add(planogramId, dtos);
            return dtos;
        }

        private readonly Dictionary<string, List<DalCacheItem>> _orphanedPositions = new Dictionary<string, List<DalCacheItem>>();
        public void InsertPlanogramPositionDto(PlanogramPositionDto dto)
        {
            string packageId = dto.PlanogramId.ToString();

            if (_packageItem != null)
            {
                List<PlanogramPositionDto> dtos;
                if (_planogramPositionDtos.TryGetValue(packageId, out dtos))
                {
                    dtos.Add(dto);
                }
                else
                {
                    _planogramPositionDtos[packageId] = new List<PlanogramPositionDto>() { dto };
                }

                DalCacheItem componentItem = GetFixtureCacheItemSubId(dto.PlanogramSubComponentId.ToString());
                PlanogramProductDto productDto = _planogramProductDtos[packageId].FirstOrDefault(p => Equals(p.Id, dto.PlanogramProductId));

                if (componentItem != null && productDto != null)
                {
                    DalCacheItem positionItem = new DalCacheItem(componentItem, dto.Id.ToString(), this, DalCacheItemType.Position);
                    positionItem.InsertDto(dto);
                    positionItem.InsertDto(productDto);
                    positionItem.InsertDtos(componentItem);
                    positionItem.ExportOrder = dto.X;

                    List<DalCacheItem> items;
                    if (componentItem.Children.TryGetValue(DalCacheItemType.Position, out items))
                    {
                        items.Add(positionItem);
                    }
                    else
                    {
                        componentItem.Children[DalCacheItemType.Position] = new List<DalCacheItem>() { positionItem };
                    }
                }
                else if (productDto != null)
                {
                    DalCacheItem positionItem = new DalCacheItem(null, dto.Id.ToString(), this, DalCacheItemType.Position);
                    positionItem.InsertDto(dto);
                    positionItem.InsertDto(productDto);
                    positionItem.ExportOrder = dto.X;

                    List<DalCacheItem> items;
                    if (_orphanedPositions.TryGetValue(dto.PlanogramSubComponentId.ToString(), out items))
                    {
                        items.Add(positionItem);
                    }
                    else
                    {
                        _orphanedPositions[dto.PlanogramSubComponentId.ToString()] = new List<DalCacheItem>() { positionItem };
                    }
                }
            }


        }

        public IEnumerable<PlanogramProductDto> GetPlanogramProductDtos(object planogramId)
        {
            List<PlanogramProductDto> dtos;
            if (_planogramProductDtos.TryGetValue(planogramId, out dtos)) return dtos;

            dtos = ProductCacheItems.SelectMany(item => item.GetDto<List<PlanogramProductDto>>()).Where(dto => dto != null && Equals(dto.PlanogramId, planogramId)).ToList();

            _planogramProductDtos.Add(planogramId, dtos);
            return dtos;
        }

        public void InsertPlanogramProductDto(PlanogramProductDto dto)
        {
            string packageId = dto.PlanogramId.ToString();

            if (_packageItem != null)
            {
                string productId = dto.Id.ToString();
                DalCacheItem planItem = new DalCacheItem(_packageItem, productId, this, DalCacheItemType.Product);
                planItem.InsertDto(dto);

                List<PlanogramProductDto> dtos;
                if (_planogramProductDtos.TryGetValue(packageId, out dtos))
                {
                    dtos.Add(dto);
                }
                else
                {
                    _planogramProductDtos[packageId] = new List<PlanogramProductDto>() { dto };
                }

                List<DalCacheItem> items;
                if (_packageItem.Children.TryGetValue(DalCacheItemType.Product, out items))
                {
                    items.Add(planItem);
                }
                else
                {
                    _packageItem.Children[DalCacheItemType.Product] = new List<DalCacheItem>() { planItem };
                }
            }
        }

        public PlanogramRenumberingStrategyDto GetRenumberingStrategyDto(object planogramId)
        {
            PlanogramRenumberingStrategyDto dto;
            if (_planogramRenumberingStrategyDtos.TryGetValue(planogramId, out dto)) return dto;

            dto = new PlanogramRenumberingStrategyDto
            {
                Id = IdentityHelper.GetNextInt32(),
                PlanogramId = planogramId,
                Name = Message.PlanogramRenumberingStrategy_DefaultName,
                BayComponentXRenumberStrategyPriority = 1,
                BayComponentYRenumberStrategyPriority = 2,
                BayComponentZRenumberStrategyPriority = 3,
                PositionXRenumberStrategyPriority = 1,
                PositionYRenumberStrategyPriority = 2,
                PositionZRenumberStrategyPriority = 3,
                BayComponentXRenumberStrategy = (byte)PlanogramRenumberingStrategyXRenumberType.LeftToRight,
                BayComponentYRenumberStrategy = (byte)PlanogramRenumberingStrategyYRenumberType.BottomToTop,
                BayComponentZRenumberStrategy = (byte)PlanogramRenumberingStrategyZRenumberType.FrontToBack,
                PositionXRenumberStrategy = (byte)PlanogramRenumberingStrategyXRenumberType.LeftToRight,
                PositionYRenumberStrategy = (byte)PlanogramRenumberingStrategyYRenumberType.BottomToTop,
                PositionZRenumberStrategy = (byte)PlanogramRenumberingStrategyZRenumberType.FrontToBack,
                RestartComponentRenumberingPerBay = true,
                IgnoreNonMerchandisingComponents = true,
                RestartPositionRenumberingPerComponent = true,
                UniqueNumberMultiPositionProductsPerComponent = false,
                ExceptAdjacentPositions = false
            };
            _planogramRenumberingStrategyDtos.Add(planogramId, dto);
            return dto;
        }

        public PlanogramSequenceDto GetSequenceDto(object planogramId)
        {
            PlanogramSequenceDto dto;
            if (_planogramSequenceDto.TryGetValue(planogramId, out dto)) return dto;

            dto = new PlanogramSequenceDto
            {
                Id = IdentityHelper.GetNextInt32(),
                PlanogramId = planogramId,
            };
            _planogramSequenceDto.Add(planogramId, dto);
            return dto;
        }

        public IEnumerable<PlanogramSequenceGroupDto> GetPlanogramSequenceGroupDtos(object planogramSequenceId)
        {
            List<PlanogramSequenceGroupDto> dtos;
            if (_planogramSequenceGroupDtos.TryGetValue(planogramSequenceId, out dtos)) return dtos;

            dtos = new List<PlanogramSequenceGroupDto>();
            _planogramSequenceGroupDtos.Add(planogramSequenceId, dtos);
            return dtos;
        }

        public IEnumerable<PlanogramSubComponentDto> GetPlanogramSubComponentDtos(object planogramComponentId)
        {
            List<PlanogramSubComponentDto> dtos;
            if (_planogramSubComponentDtos.TryGetValue(planogramComponentId, out dtos)) return dtos;

            dtos = new List<PlanogramSubComponentDto>();
            DalCacheItem fixtureCacheItem = GetFixtureCacheItem(planogramComponentId);
            PlanogramSubComponentDto planogramSubComponentDto = null;
            if (fixtureCacheItem == null)
            {
                var componentId = planogramComponentId as string;
                if (string.IsNullOrEmpty(componentId)) Debug.Fail("Unknown ID when finding a Fixture Cache Item. The item could not be found.");
                else
                {
                    string baseDefaultMask = Message.JdaImport_BaseDefaultNameMask.Replace("{0}", string.Empty);
                    string backboardDefaultMask = Message.JdaImport_BackboardDefaultNameMask.Replace("{0}", string.Empty);
                    fixtureCacheItem = GetSegmentCacheItem(componentId.Replace(baseDefaultMask, string.Empty).Replace(backboardDefaultMask, string.Empty));
                    if (fixtureCacheItem == null) Debug.Fail("Unknown ID when finding a Fixture Cache Item. The item could not be found.");
                    else
                    {
                        planogramSubComponentDto =
                            fixtureCacheItem.GetDto<List<PlanogramSubComponentDto>>().FirstOrDefault(dto => dto != null && Equals(dto.Id, planogramComponentId));
                    }
                }
            }
            else
                planogramSubComponentDto = fixtureCacheItem.GetDto<PlanogramSubComponentDto>();

            if (planogramSubComponentDto != null) dtos.Add(planogramSubComponentDto);
            _planogramSubComponentDtos.Add(planogramComponentId, dtos);
            return dtos;
        }

        public void InsertPlanogramSubComponentDto(PlanogramSubComponentDto dto)
        {
            string planogramComponentId = dto.PlanogramComponentId.ToString();

            List<PlanogramSubComponentDto> dtos;
            if (_planogramSubComponentDtos.TryGetValue(planogramComponentId, out dtos))
            {
                dtos.Add(dto);
            }
            else
            {
                _planogramSubComponentDtos[planogramComponentId] = new List<PlanogramSubComponentDto>() { dto };
            }

            string componentId = dto.PlanogramComponentId.ToString();
            DalCacheItem componentItem = GetFixtureCacheItem(componentId);

            if (componentItem != null)
            {
                DalCacheItem planItem = GetPlanogramCacheItem(componentItem.Parent.Id);

                //if we have a subcomponent already in the item then we want
                //to make a copy of it excluding the subcomponent
                if (!componentItem.InsertDto(dto))
                {
                    //sub component happens last so we should be fine copying making a new cache item here
                    DalCacheItem copyItem = new DalCacheItem(componentItem.Parent, dto.Id.ToString(), this, DalCacheItemType.Fixture);
                    copyItem.InsertDto(dto);
                    copyItem.InsertDtos(componentItem);
                    copyItem.SubId = dto.Id.ToString();

                    if (componentItem.Parent != null)
                    {
                        List<DalCacheItem> items;
                        if (planItem.Children.TryGetValue(DalCacheItemType.Fixture, out items))
                        {
                            items.Add(copyItem);
                        }
                        else
                        {
                            planItem.Children[DalCacheItemType.Fixture] = new List<DalCacheItem>() { copyItem };
                        }
                    }

                    List<DalCacheItem> positionItems;
                    if (_orphanedPositions.TryGetValue(copyItem.SubId, out positionItems))
                    {
                        foreach (DalCacheItem positionItem in positionItems.ToList())
                        {
                            positionItem.Parent = copyItem;
                            positionItem.InsertDtos(copyItem);

                            List<DalCacheItem> positionItems2;
                            if (copyItem.Children.TryGetValue(DalCacheItemType.Position, out positionItems2))
                            {
                                positionItems2.Add(positionItem);
                            }
                            else
                            {
                                copyItem.Children[DalCacheItemType.Position] = new List<DalCacheItem>() { positionItem };
                            }

                            positionItems.Remove(positionItem);
                        }
                    }
                }
                else
                {
                    componentItem.SubId = dto.Id.ToString();
                    List<DalCacheItem> positionItems;
                    if (_orphanedPositions.TryGetValue(componentItem.SubId, out positionItems))
                    {
                        foreach (DalCacheItem positionItem in positionItems.ToList())
                        {
                            positionItem.Parent = componentItem;
                            positionItem.InsertDtos(componentItem);

                            List<DalCacheItem> positionItems2;
                            if (componentItem.Children.TryGetValue(DalCacheItemType.Position, out positionItems2))
                            {
                                positionItems2.Add(positionItem);
                            }
                            else
                            {
                                componentItem.Children[DalCacheItemType.Position] = new List<DalCacheItem>() { positionItem };
                            }

                            positionItems.Remove(positionItem);
                        }
                    }
                }
            }



        }

        public PlanogramValidationTemplateDto GetValidationTemplateDto(object planogramId)
        {
            PlanogramValidationTemplateDto dto;
            if (_planogramValidationTemplateDto.TryGetValue(planogramId, out dto)) return dto;

            dto = new PlanogramValidationTemplateDto
            {
                Id = IdentityHelper.GetNextInt32(),
                PlanogramId = planogramId,
                Name = Message.PlanogramValidationTemplate_Name_Default
            };
            _planogramValidationTemplateDto.Add(planogramId, dto);
            return dto;
        }

        public IEnumerable<PlanogramValidationTemplateGroupDto> GetPlanogramValidationTemplateGroupDtos(object planogramValidationTemplateId)
        {
            List<PlanogramValidationTemplateGroupDto> dtos;
            if (_planogramValidationTemplateGroupDtos.TryGetValue(planogramValidationTemplateId, out dtos)) return dtos;

            dtos = new List<PlanogramValidationTemplateGroupDto>();
            _planogramValidationTemplateGroupDtos.Add(planogramValidationTemplateId, dtos);
            return dtos;
        }

        public IEnumerable<PlanogramConsumerDecisionTreeNodeProductDto> GetPlanogramConsumerDecisionTreeNodeProductDtos(object planogramConsumerDecisionTreeNodeId)
        {
            return new List<PlanogramConsumerDecisionTreeNodeProductDto>();
        }

        public void InsertPlanogramFixtureAssemblyDto(PlanogramFixtureAssemblyDto dto)
        {
            string assemblyId = dto.PlanogramAssemblyId.ToString();
            List<DalCacheItem> componentItems = GetFixtureCacheItemsBySubId(assemblyId);
            foreach (DalCacheItem item in componentItems)
            {
                item.InsertDto(dto);
            }
        }

        public void InsertPlanogramAssemblyDto(PlanogramAssemblyDto dto)
        {
            //do nothing at all.. HA!       

        }

        public void InsertPlanogramAssemblyComponentDto(PlanogramAssemblyComponentDto dto)
        {
            string componentId = dto.PlanogramComponentId.ToString();
            DalCacheItem componentItem = GetFixtureCacheItem(componentId);
            componentItem.InsertDto(dto);
            componentItem.SubId = dto.PlanogramAssemblyId.ToString();
        }

        #endregion

        #region DalCacheItem Helpers
        public int GetFriendlyComponentId(object componentId)
        {
            lock (_lock)
            {
                int id;
                if (!_friendlyComponentIDs.TryGetValue(componentId, out id))
                {
                    if (_friendlyComponentIDs.Count > 0)
                    {
                        id = _friendlyComponentIDs.Values.Max() + 1;
                    }
                    else
                    {
                        id = 1;
                    }
                    _friendlyComponentIDs[componentId] = id;
                }

                return id;
            }
        }

        public int GetFriendlyAssemblyId(object assemblyId)
        {
            lock (_lock)
            {
                int id;
                if (!_friendlyAssemblyIDs.TryGetValue(assemblyId, out id))
                {
                    if (_friendlyAssemblyIDs.Count > 0)
                    {
                        id = _friendlyAssemblyIDs.Values.Max() + 1;
                    }
                    else
                    {
                        id = 1;
                    }
                    _friendlyAssemblyIDs[assemblyId] = id;
                }

                return id;
            }
        }

        internal DalCacheItem GetPlanogramCacheItem(object id)
        {
            return PlanogramCacheItems.FirstOrDefault(item => Equals(item.Id, id));
        }

        private List<DalCacheItem> GetSegmentCacheItems(object planogramId)
        {
            DalCacheItem planogramCacheItem = GetPlanogramCacheItem(planogramId);
            return planogramCacheItem != null
                       ? planogramCacheItem.Children[DalCacheItemType.Segment]
                       : new List<DalCacheItem>();
        }

        internal DalCacheItem GetSegmentCacheItem(object id)
        {
            return SegmentCacheItems.FirstOrDefault(item => Equals(item.Id, id));
        }

        internal DalCacheItem GetSegmentCacheItemBySub(object subId)
        {
            return SegmentCacheItems.FirstOrDefault(item => Equals(item.SubId, subId));
        }

        private List<DalCacheItem> GetFixtureCacheItems(object planogramId)
        {
            DalCacheItem planogramCacheItem = GetPlanogramCacheItem(planogramId);
            return planogramCacheItem != null
                       ? planogramCacheItem.Children[DalCacheItemType.Fixture]
                       : new List<DalCacheItem>();
        }

        private DalCacheItem GetFixtureCacheItem(object id)
        {
            return FixtureCacheItems.FirstOrDefault(item => Equals(item.Id, id));
        }

        private DalCacheItem GetFixtureCacheItemSubId(object subId)
        {
            return FixtureCacheItems.FirstOrDefault(item => Equals(item.SubId, subId));
        }

        private List<DalCacheItem> GetFixtureCacheItemsBySubId(object subId)
        {
            return FixtureCacheItems.Where(item => Equals(item.SubId, subId)).ToList();
        }

        private List<DalCacheItem> GetTextCacheItems(object planogramId)
        {
            DalCacheItem planogramCacheItem = GetPlanogramCacheItem(planogramId);
            return planogramCacheItem != null
                       ? planogramCacheItem.Children[DalCacheItemType.Drawing]
                       : new List<DalCacheItem>();
        }

        /// <summary>
        /// Inserts a dto into the cache
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dto"></param>
        /// <param name="packageDto">only needed for planogramdto insert</param>
        public void InsertDto<T>(T dto, PackageDto packageDto = null)
        {
            try
            {
                if (dto is PackageDto)
                {
                    InsertPackageDto(dto as PackageDto);
                    return;
                }

                if (dto is CustomAttributeDataDto)
                {
                    InsertCustomAttributeDataDto(dto as CustomAttributeDataDto);
                    return;
                }

                if (dto is PlanogramAnnotationDto)
                {
                    InsertPlanogramAnnotationDto(dto as PlanogramAnnotationDto);
                    return;
                }

                if (dto is PlanogramAssemblyComponentDto)
                {
                    InsertPlanogramAssemblyComponentDto(dto as PlanogramAssemblyComponentDto);
                    return;
                }

                if (dto is PlanogramAssemblyDto)
                {
                    InsertPlanogramAssemblyDto(dto as PlanogramAssemblyDto);
                    return;
                }

                if (dto is PlanogramAssortmentDto)
                {
                    InsertPlanogramAssortmentDto(dto as PlanogramAssortmentDto);
                    return;
                }

                if (dto is PlanogramAssortmentInventoryRuleDto)
                {
                    InsertPlanogramAssortmentInventoryRuleDto(dto as PlanogramAssortmentInventoryRuleDto);
                    return;
                }

                if (dto is PlanogramAssortmentLocalProductDto)
                {
                    InsertPlanogramAssortmentLocalProductDto(dto as PlanogramAssortmentLocalProductDto);
                    return;
                }

                if (dto is PlanogramAssortmentLocationBuddyDto)
                {
                    InsertPlanogramAssortmentLocationBuddyDto(dto as PlanogramAssortmentLocationBuddyDto);
                    return;
                }

                if (dto is PlanogramAssortmentProductBuddyDto)
                {
                    InsertPlanogramAssortmentProductBuddyDto(dto as PlanogramAssortmentProductBuddyDto);
                    return;
                }

                if (dto is PlanogramAssortmentProductDto)
                {
                    InsertPlanogramAssortmentProductDto(dto as PlanogramAssortmentProductDto);
                    return;
                }

                if (dto is PlanogramAssortmentRegionDto)
                {
                    InsertPlanogramAssortmentRegionDto(dto as PlanogramAssortmentRegionDto);
                    return;
                }

                if (dto is PlanogramComponentDto)
                {
                    InsertPlanogramComponentDto(dto as PlanogramComponentDto);
                    return;
                }

                if (dto is PlanogramDto)
                {
                    InsertPlanogramDto(packageDto, dto as PlanogramDto);
                    return;
                }

                if (dto is PlanogramFixtureAssemblyDto)
                {
                    InsertPlanogramFixtureAssemblyDto(dto as PlanogramFixtureAssemblyDto);
                    return;
                }

                if (dto is PlanogramFixtureComponentDto)
                {
                    InsertPlanogramFixtureComponentDto(dto as PlanogramFixtureComponentDto);
                    return;
                }

                if (dto is PlanogramFixtureDto)
                {
                    InsertPlanogramFixtureDto(dto as PlanogramFixtureDto);
                    return;
                }

                if (dto is PlanogramFixtureItemDto)
                {
                    InsertPlanogramFixtureItemDto(dto as PlanogramFixtureItemDto);
                    return;
                }

                if (dto is PlanogramInventoryDto)
                {
                    InsertPlanogramInventoryDto(dto as PlanogramInventoryDto);
                    return;
                }

                if (dto is PlanogramPerformanceDto)
                {
                    InsertPlanogramPerformanceDto(dto as PlanogramPerformanceDto);
                    return;
                }

                if (dto is PlanogramPerformanceDataDto)
                {
                    InsertPlanogramPerformanceDataDto(dto as PlanogramPerformanceDataDto);
                    return;
                }

                if (dto is PlanogramPerformanceMetricDto)
                {
                    InsertPlanogramPerformanceMetricDto(dto as PlanogramPerformanceMetricDto);
                    return;
                }

                if (dto is PlanogramPositionDto)
                {
                    InsertPlanogramPositionDto(dto as PlanogramPositionDto);
                    return;
                }

                if (dto is PlanogramProductDto)
                {
                    InsertPlanogramProductDto(dto as PlanogramProductDto);
                    return;
                }

                if (dto is PlanogramSubComponentDto)
                {
                    InsertPlanogramSubComponentDto(dto as PlanogramSubComponentDto);
                    return;
                }
            }
            catch (Exception ex)
            {
                if (ExportMappingContext.ViewErrorLogsOnCompletion)
                {
                    //log exception
                    SpacePlanningExportHelper.CreateErrorEventLogEntry<T>(PackageItem.Id, ExportMappingContext.ExportEvents, ex.Message);
                }
                else
                {
                    //kill off the file stream just to be sure.
                    _fileStream?.Dispose();
                    _fileStream = null;

                    //re-throw
                    throw;
                }
                return;
            }

            Debug.Assert(false, "Dto type not supported");
        }
        #endregion

    }
}