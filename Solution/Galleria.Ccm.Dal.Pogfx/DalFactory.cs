﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-24979 : L.Hodson
//  Created
// V8-26156 : N.Foster
//  Converted to use framework classed
#endregion
#endregion

using System;
using Galleria.Framework.Dal.Configuration;

namespace Galleria.Ccm.Dal.Pogfx
{
    public class DalFactory : Galleria.Framework.Dal.Gbf.DalFactoryBase<DalCache>
    {
        #region Properties
        /// <summary>
        /// Returns the database type name
        /// </summary>
        public override String DatabaseTypeName
        {
            get { return "Galleria Fixture File"; }
        }
        #endregion

        #region Constructors
        public DalFactory() : base() { }
        public DalFactory(DalFactoryConfigElement dalFactoryConfig) : base(dalFactoryConfig) { }
        #endregion
    }
}
