﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Ineson
//		Created
// V8-25873 : A.Probyn
//      Added new FixturePackage CustomAttribute1-5
// V8-26091 : L.Ineson
//  Added additional subcomponent face thick properties.
#endregion
#region Version History: (CCM CCM802)
// V8-29023 : M.Pettit
//		Added FixtureSubComponentMerchandisableDepth
#endregion
#region Version History: (CCM CCM830)
// V8-32524 : L.Ineson
//  Added FixtureAnnotation fields.
#endregion
#endregion

using System;

namespace Galleria.Ccm.Dal.Pogfx.Schema
{
    internal partial class FieldNames
    {
        #region SchemaVersion

        public const String SchemaVersionMajorVersion = "MajorVersion";
        public const String SchemaVersionMinorVersion = "MinorVersion";

        #endregion

        #region FixturePackage

        public const String FixturePackageId = "Id";
        public const String FixturePackageName = "Name";
        public const String FixturePackageDescription = "Description";
        public const String FixturePackageHeight = "Height";
        public const String FixturePackageWidth = "Width";
        public const String FixturePackageDepth = "Depth";
        public const String FixturePackageFixtureCount = "FixtureCount";
        public const String FixturePackageAssemblyCount = "AssemblyCount";
        public const String FixturePackageComponentCount = "ComponentCount";
        public const String FixturePackageThumbnailImageData = "ThumbnailImageData";
        public const String FixturePackageLengthUnitsOfMeasure = "LengthUnitsOfMeasure";
        public const String FixturePackageAreaUnitsOfMeasure = "AreaUnitsOfMeasure";
        public const String FixturePackageVolumeUnitsOfMeasure = "VolumeUnitsOfMeasure";
        public const String FixturePackageWeightUnitsOfMeasure = "WeightUnitsOfMeasure";
        public const String FixturePackageAngleUnitsOfMeasure = "AngleUnitsOfMeasure";
        public const String FixturePackageCurrencyUnitsOfMeasure = "CurrencyUnitsOfMeasure";
        public const String FixturePackageDateCreated = "DateCreated";
        public const String FixturePackageDateLastModified = "DateLastModified";
        public const String FixturePackageCreatedBy = "CreatedBy";
        public const String FixturePackageLastModifiedBy = "LastModifiedBy";
        public const String FixturePackageCustomAttribute1 = "CustomAttribute1";
        public const String FixturePackageCustomAttribute2 = "CustomAttribute2";
        public const String FixturePackageCustomAttribute3 = "CustomAttribute3";
        public const String FixturePackageCustomAttribute4 = "CustomAttribute4";
        public const String FixturePackageCustomAttribute5 = "CustomAttribute5";


        #endregion

        #region Fixture

        public const String FixtureId = "Id";
        public const String FixtureFixturePackageId = "FixturePackageId";
        public const String FixtureName = "Name";
        public const String FixtureHeight = "Height";
        public const String FixtureWidth = "Width";
        public const String FixtureDepth = "Depth";
        public const String FixtureNumberOfAssemblies = "NumberOfAssemblies";
        public const String FixtureNumberOfMerchandisedSubComponents = "NumberOfMerchandisedSubComponents";
        public const String FixtureTotalFixtureCost = "TotalFixtureCost";


        #endregion

        #region FixtureAnnotation
        public const String FixtureAnnotationId = "Id";
        public const String FixtureAnnotationRowVersion = "RowVersion";
        public const String FixtureAnnotationFixturePackageId = "FixturePackageId";
        public const String FixtureAnnotationFixtureItemId = "FixtureItemId";
        public const String FixtureAnnotationFixtureAssemblyItemId = "FixtureAssemblyItemId";
        public const String FixtureAnnotationFixtureComponentItemId = "FixtureComponentItemId";
        public const String FixtureAnnotationFixtureAssemblyComponentId = "FixtureAssemblyComponentId";
        public const String FixtureAnnotationFixtureSubComponentId = "FixtureSubComponentId";
        public const String FixtureAnnotationAnnotationType = "AnnotationType";
        public const String FixtureAnnotationText = "Text";
        public const String FixtureAnnotationX = "X";
        public const String FixtureAnnotationY = "Y";
        public const String FixtureAnnotationZ = "Z";
        public const String FixtureAnnotationSlope = "Slope";
        public const String FixtureAnnotationAngle = "Angle";
        public const String FixtureAnnotationRoll = "Roll";
        public const String FixtureAnnotationHeight = "Height";
        public const String FixtureAnnotationWidth = "Width";
        public const String FixtureAnnotationDepth = "Depth";
        public const String FixtureAnnotationBorderColour = "BorderColour";
        public const String FixtureAnnotationBorderThickness = "BorderThickness";
        public const String FixtureAnnotationBackgroundColour = "BackgroundColour";
        public const String FixtureAnnotationFontColour = "FontColour";
        public const String FixtureAnnotationFontSize = "FontSize";
        public const String FixtureAnnotationFontName = "FontName";
        public const String FixtureAnnotationCanReduceFontToFit = "CanReduceFontToFit";
        public const String FixtureAnnotationExtendedData = "ExtendedData";
        #endregion

        #region FixtureAssembly

        public const String FixtureAssemblyId = "Id";
        public const String FixtureAssemblyFixturePackageId = "FixturePackageId";
        public const String FixtureAssemblyName = "Name";
        public const String FixtureAssemblyHeight = "Height";
        public const String FixtureAssemblyWidth = "Width";
        public const String FixtureAssemblyDepth = "Depth";
        public const String FixtureAssemblyNumberOfComponents = "NumberOfComponents";
        public const String FixtureAssemblyTotalComponentCost = "TotalComponentCost";

        #endregion

        #region FixtureAssemblyComponent


        public const String FixtureAssemblyComponentId = "Id";
        public const String FixtureAssemblyComponentFixtureAssemblyId = "FixtureAssemblyId";
        public const String FixtureAssemblyComponentFixtureComponentId = "FixtureComponentId";
        public const String FixtureAssemblyComponentX = "X";
        public const String FixtureAssemblyComponentY = "Y";
        public const String FixtureAssemblyComponentZ = "Z";
        public const String FixtureAssemblyComponentSlope = "Slope";
        public const String FixtureAssemblyComponentAngle = "Angle";
        public const String FixtureAssemblyComponentRoll = "Roll";
        public const String FixtureAssemblyComponentIsBay = "IsBay";

        #endregion

        #region FixtureAssemblyItem

        public const String FixtureAssemblyItemId = "Id";
        public const String FixtureAssemblyItemFixtureId = "FixtureId";
        public const String FixtureAssemblyItemFixtureAssemblyId = "FixtureAssemblyId";
        public const String FixtureAssemblyItemX = "X";
        public const String FixtureAssemblyItemY = "Y";
        public const String FixtureAssemblyItemZ = "Z";
        public const String FixtureAssemblyItemSlope = "Slope";
        public const String FixtureAssemblyItemAngle = "Angle";
        public const String FixtureAssemblyItemRoll = "Roll";

        #endregion

        #region FixtureComponent

        public const String FixtureComponentId = "Id";
        public const String FixtureComponentFixturePackageId = "FixturePackageId";
        public const String FixtureComponentMesh3DId = "Mesh3DId";
        public const String FixtureComponentImageIdFront = "ImageIdFront";
        public const String FixtureComponentImageIdBack = "ImageIdBack";
        public const String FixtureComponentImageIdTop = "ImageIdTop";
        public const String FixtureComponentImageIdBottom = "ImageIdBottom";
        public const String FixtureComponentImageIdLeft = "ImageIdLeft";
        public const String FixtureComponentImageIdRight = "ImageIdRight";
        public const String FixtureComponentName = "Name";
        public const String FixtureComponentHeight = "Height";
        public const String FixtureComponentWidth = "Width";
        public const String FixtureComponentDepth = "Depth";
        public const String FixtureComponentNumberOfSubComponents = "NumberOfSubComponents";
        public const String FixtureComponentNumberOfMerchandisedSubComponents = "NumberOfMerchandisedSubComponents";
        public const String FixtureComponentNumberOfShelfEdgeLabels = "NumberOfShelfEdgeLabels";
        public const String FixtureComponentIsMoveable = "IsMoveable";
        public const String FixtureComponentIsDisplayOnly = "IsDisplayOnly";
        public const String FixtureComponentCanAttachShelfEdgeLabel = "CanAttachShelfEdgeLabel";
        public const String FixtureComponentRetailerReferenceCode = "RetailerReferenceCode";
        public const String FixtureComponentBarCode = "BarCode";
        public const String FixtureComponentManufacturer = "Manufacturer";
        public const String FixtureComponentManufacturerPartName = "ManufacturerPartName";
        public const String FixtureComponentManufacturerPartNumber = "ManufacturerPartNumber";
        public const String FixtureComponentSupplierName = "SupplierName";
        public const String FixtureComponentSupplierPartNumber = "SupplierPartNumber";
        public const String FixtureComponentSupplierCostPrice = "SupplierCostPrice";
        public const String FixtureComponentSupplierDiscount = "SupplierDiscount";
        public const String FixtureComponentSupplierLeadTime = "SupplierLeadTime";
        public const String FixtureComponentMinPurchaseQty = "MinPurchaseQty";
        public const String FixtureComponentWeightLimit = "WeightLimit";
        public const String FixtureComponentWeight = "Weight";
        public const String FixtureComponentVolume = "Volume";
        public const String FixtureComponentDiameter = "Diameter";
        public const String FixtureComponentCapacity = "Capacity";
        public const String FixtureComponentComponentType = "ComponentType";
        public const String FixtureComponentIsMerchandisedTopDown = "IsMerchandisedTopDown";

        #endregion

        #region FixtureComponentItem

        public const String FixtureComponentItemId = "Id";
        public const String FixtureComponentItemFixtureId = "FixtureId";
        public const String FixtureComponentItemFixtureComponentId = "FixtureComponentId";
        public const String FixtureComponentItemX = "X";
        public const String FixtureComponentItemY = "Y";
        public const String FixtureComponentItemZ = "Z";
        public const String FixtureComponentItemSlope = "Slope";
        public const String FixtureComponentItemAngle = "Angle";
        public const String FixtureComponentItemRoll = "Roll";

        #endregion

        #region FixtureImage

        public const String FixtureImageId = "Id";
        public const String FixtureImageFixturePackageId = "FixturePackageId";
        public const String FixtureImageFileName = "FileName";
        public const String FixtureImageDescription = "Description";
        public const String FixtureImageImageData = "ImageData";

        #endregion

        #region FixtureItem

        public const String FixtureItemId = "Id";
        public const String FixtureItemFixturePackageId = "FixturePackageId";
        public const String FixtureItemFixtureId = "FixtureId";
        public const String FixtureItemX = "X";
        public const String FixtureItemY = "Y";
        public const String FixtureItemZ = "Z";
        public const String FixtureItemSlope = "Slope";
        public const String FixtureItemAngle = "Angle";
        public const String FixtureItemRoll = "Roll";

        #endregion

        #region FixtureSubComponent

        public const String FixtureSubComponentId = "Id";
        public const String FixtureSubComponentFixtureComponentId = "FixtureComponentId";
        public const String FixtureSubComponentMesh3DId = "Mesh3DId";
        public const String FixtureSubComponentImageIdFront = "ImageIdFront";
        public const String FixtureSubComponentImageIdBack = "ImageIdBack";
        public const String FixtureSubComponentImageIdTop = "ImageIdTop";
        public const String FixtureSubComponentImageIdBottom = "ImageIdBottom";
        public const String FixtureSubComponentImageIdLeft = "ImageIdLeft";
        public const String FixtureSubComponentImageIdRight = "ImageIdRight";
        public const String FixtureSubComponentName = "Name";
        public const String FixtureSubComponentHeight = "Height";
        public const String FixtureSubComponentWidth = "Width";
        public const String FixtureSubComponentDepth = "Depth";
        public const String FixtureSubComponentX = "X";
        public const String FixtureSubComponentY = "Y";
        public const String FixtureSubComponentZ = "Z";
        public const String FixtureSubComponentSlope = "Slope";
        public const String FixtureSubComponentAngle = "Angle";
        public const String FixtureSubComponentRoll = "Roll";
        public const String FixtureSubComponentShapeType = "ShapeType";
        public const String FixtureSubComponentMerchandisableHeight = "MerchandisableHeight";
        public const String FixtureSubComponentMerchandisableDepth = "MerchandisableDepth";
        public const String FixtureSubComponentIsVisible = "IsVisible";
        public const String FixtureSubComponentHasCollisionDetection = "HasCollisionDetection";
        public const String FixtureSubComponentFillPatternTypeFront = "FillPatternTypeFront";
        public const String FixtureSubComponentFillPatternTypeBack = "FillPatternTypeBack";
        public const String FixtureSubComponentFillPatternTypeTop = "FillPatternTypeTop";
        public const String FixtureSubComponentFillPatternTypeBottom = "FillPatternTypeBottom";
        public const String FixtureSubComponentFillPatternTypeLeft = "FillPatternTypeLeft";
        public const String FixtureSubComponentFillPatternTypeRight = "FillPatternTypeRight";
        public const String FixtureSubComponentFillColourFront = "FillColourFront";
        public const String FixtureSubComponentFillColourBack = "FillColourBack";
        public const String FixtureSubComponentFillColourTop = "FillColourTop";
        public const String FixtureSubComponentFillColourBottom = "FillColourBottom";
        public const String FixtureSubComponentFillColourLeft = "FillColourLeft";
        public const String FixtureSubComponentFillColourRight = "FillColourRight";
        public const String FixtureSubComponentLineColour = "LineColour";
        public const String FixtureSubComponentTransparencyPercentFront = "TransparencyPercentFront";
        public const String FixtureSubComponentTransparencyPercentBack = "TransparencyPercentBack";
        public const String FixtureSubComponentTransparencyPercentTop = "TransparencyPercentTop";
        public const String FixtureSubComponentTransparencyPercentBottom = "TransparencyPercentBottom";
        public const String FixtureSubComponentTransparencyPercentLeft = "TransparencyPercentLeft";
        public const String FixtureSubComponentTransparencyPercentRight = "TransparencyPercentRight";
        public const String FixtureSubComponentFaceThicknessFront = "FaceThicknessFront";
        public const String FixtureSubComponentFaceThicknessBack = "FaceThicknessBack";
        public const String FixtureSubComponentFaceThicknessTop = "FaceThicknessTop";
        public const String FixtureSubComponentFaceThicknessBottom = "FaceThicknessBottom";
        public const String FixtureSubComponentFaceThicknessLeft = "FaceThicknessLeft";
        public const String FixtureSubComponentFaceThicknessRight = "FaceThicknessRight";
        public const String FixtureSubComponentRiserHeight = "RiserHeight";
        public const String FixtureSubComponentRiserThickness = "RiserThickness";
        public const String FixtureSubComponentIsRiserPlacedOnFront = "IsRiserPlacedOnFront";
        public const String FixtureSubComponentIsRiserPlacedOnBack = "IsRiserPlacedOnBack";
        public const String FixtureSubComponentIsRiserPlacedOnLeft = "IsRiserPlacedOnLeft";
        public const String FixtureSubComponentIsRiserPlacedOnRight = "IsRiserPlacedOnRight";
        public const String FixtureSubComponentRiserFillPatternType = "RiserFillPatternType";
        public const String FixtureSubComponentRiserColour = "RiserColour";
        public const String FixtureSubComponentRiserTransparencyPercent = "RiserTransparencyPercent";
        public const String FixtureSubComponentNotchStartX = "NotchStartX";
        public const String FixtureSubComponentNotchSpacingX = "NotchSpacingX";
        public const String FixtureSubComponentNotchStartY = "NotchStartY";
        public const String FixtureSubComponentNotchSpacingY = "NotchSpacingY";
        public const String FixtureSubComponentNotchHeight = "NotchHeight";
        public const String FixtureSubComponentNotchWidth = "NotchWidth";
        public const String FixtureSubComponentIsNotchPlacedOnFront = "IsNotchPlacedOnFront";
        public const String FixtureSubComponentIsNotchPlacedOnBack = "IsNotchPlacedOnBack";
        public const String FixtureSubComponentIsNotchPlacedOnLeft = "IsNotchPlacedOnLeft";
        public const String FixtureSubComponentIsNotchPlacedOnRight = "IsNotchPlacedOnRight";
        public const String FixtureSubComponentNotchStyleType = "NotchStyleType";
        public const String FixtureSubComponentDividerObstructionHeight = "DividerObstructionHeight";
        public const String FixtureSubComponentDividerObstructionWidth = "DividerObstructionWidth";
        public const String FixtureSubComponentDividerObstructionDepth = "DividerObstructionDepth";
        public const String FixtureSubComponentDividerObstructionStartX = "DividerObstructionStartX";
        public const String FixtureSubComponentDividerObstructionSpacingX = "DividerObstructionSpacingX";
        public const String FixtureSubComponentDividerObstructionStartY = "DividerObstructionStartY";
        public const String FixtureSubComponentDividerObstructionSpacingY = "DividerObstructionSpacingY";
        public const String FixtureSubComponentDividerObstructionStartZ = "DividerObstructionStartZ";
        public const String FixtureSubComponentDividerObstructionSpacingZ = "DividerObstructionSpacingZ";
        public const String FixtureSubComponentIsDividerObstructionAtStart = "IsDividerObstructionAtStart";
        public const String FixtureSubComponentIsDividerObstructionAtEnd = "IsDividerObstructionAtEnd";
        public const String FixtureSubComponentIsDividerObstructionByFacing = "IsDividerObstructionByFacing";
        public const String FixtureSubComponentDividerObstructionFillColour = "DividerObstructionFillColour";
        public const String FixtureSubComponentDividerObstructionFillPattern = "DividerObstructionFillPattern";
        public const String FixtureSubComponentMerchConstraintRow1StartX = "MerchConstraintRow1StartX";
        public const String FixtureSubComponentMerchConstraintRow1SpacingX = "MerchConstraintRow1SpacingX";
        public const String FixtureSubComponentMerchConstraintRow1StartY = "MerchConstraintRow1StartY";
        public const String FixtureSubComponentMerchConstraintRow1SpacingY = "MerchConstraintRow1SpacingY";
        public const String FixtureSubComponentMerchConstraintRow1Height = "MerchConstraintRow1Height";
        public const String FixtureSubComponentMerchConstraintRow1Width = "MerchConstraintRow1Width";
        public const String FixtureSubComponentMerchConstraintRow2StartX = "MerchConstraintRow2StartX";
        public const String FixtureSubComponentMerchConstraintRow2SpacingX = "MerchConstraintRow2SpacingX";
        public const String FixtureSubComponentMerchConstraintRow2StartY = "MerchConstraintRow2StartY";
        public const String FixtureSubComponentMerchConstraintRow2SpacingY = "MerchConstraintRow2SpacingY";
        public const String FixtureSubComponentMerchConstraintRow2Height = "MerchConstraintRow2Height";
        public const String FixtureSubComponentMerchConstraintRow2Width = "MerchConstraintRow2Width";
        public const String FixtureSubComponentLineThickness = "LineThickness";
        public const String FixtureSubComponentMerchandisingType = "MerchandisingType";
        public const String FixtureSubComponentCombineType = "CombineType";
        public const String FixtureSubComponentIsProductOverlapAllowed = "IsProductOverlapAllowed";
        public const String FixtureSubComponentIsProductSqueezeAllowed = "IsProductSqueezeAllowed";
        public const String FixtureSubComponentMerchandisingStrategyX = "MerchandisingStrategyX";
        public const String FixtureSubComponentMerchandisingStrategyY = "MerchandisingStrategyY";
        public const String FixtureSubComponentMerchandisingStrategyZ = "MerchandisingStrategyZ";
        public const String FixtureSubComponentLeftOverhang = "LeftOverhang";
        public const String FixtureSubComponentRightOverhang = "RightOverhang";
        public const String FixtureSubComponentFrontOverhang = "FrontOverhang";
        public const String FixtureSubComponentBackOverhang = "BackOverhang";
        public const String FixtureSubComponentTopOverhang = "TopOverhang";
        public const String FixtureSubComponentBottomOverhang = "BottomOverhang";

        #endregion
    }
}
