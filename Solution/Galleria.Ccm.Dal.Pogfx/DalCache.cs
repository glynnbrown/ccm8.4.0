﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM CCM800)
// V8-24979 : L.Hodson
//	Created
// V8-26156 : N.Foster
//  Converted to use framework classes
#endregion
#region Version History: (CCM CCM830)
// CCM-32524 : L.Ineson
//		Added FixtureAnnotations
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Pogfx.Implementation.Items;
using Galleria.Framework.IO;

namespace Galleria.Ccm.Dal.Pogfx
{
    /// <summary>
    /// The DalCache is used to store DTO instances in memory so that they can be fetched/inserted/updated quickly and
    /// so that DAL operations can be commited to the underlying FIX file only when a DalContext transaction is 
    /// committed.
    /// </summary>
    public class DalCache : Galleria.Framework.Dal.Gbf.DalCacheBase<DalCache>
    {
        #region Fields
        private SingleItemCache<FixturePackageDto, FixturePackageDtoItem> _package;
        private TwoLayerItemCache<FixtureItemDto, FixtureItemDtoItem> _fixtureItems;
        private TwoLayerItemCache<FixtureDto, FixtureDtoItem> _fixtures;
        private TwoLayerItemCache<FixtureAssemblyItemDto, FixtureAssemblyItemDtoItem> _fixtureAssemblyItems;
        private TwoLayerItemCache<FixtureComponentItemDto, FixtureComponentItemDtoItem> _fixtureComponentItems;
        private TwoLayerItemCache<FixtureAssemblyDto, FixtureAssemblyDtoItem> _fixtureAssemblies;
        private TwoLayerItemCache<FixtureAssemblyComponentDto, FixtureAssemblyComponentDtoItem> _fixtureAssemblyComponents;
        private TwoLayerItemCache<FixtureComponentDto, FixtureComponentDtoItem> _fixtureComponents;
        private TwoLayerItemCache<FixtureSubComponentDto, FixtureSubComponentDtoItem> _fixtureSubComponents;
        private TwoLayerItemCache<FixtureImageDto, FixtureImageDtoItem> _fixtureImages;
        private TwoLayerItemCache<FixtureAnnotationDto, FixtureAnnotationDtoItem> _fixtureAnnotations;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public DalCache(DalFactory dalFactory, Boolean readOnly)
            : base(dalFactory, readOnly)
        {
            _package =
                CreateSingleItemCache<FixturePackageDto, FixturePackageDtoItem>(
                    (UInt16)SectionType.FixturePackage,
                    delegate(FixturePackageDto dto)
                    {
                        return new FixturePackageDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new FixturePackageDtoItem(item);
                    });
            _fixtureItems =
                CreateTwoLayerItemCache<FixtureItemDto, FixtureItemDtoItem>(
                    (UInt16)SectionType.FixtureItems,
                    delegate(FixtureItemDto dto)
                    {
                        return new FixtureItemDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new FixtureItemDtoItem(item);
                    });
            _fixtures =
                CreateTwoLayerItemCache<FixtureDto, FixtureDtoItem>(
                    (UInt16)SectionType.Fixtures,
                    delegate(FixtureDto dto)
                    {
                        return new FixtureDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new FixtureDtoItem(item);
                    });
            _fixtureAssemblyItems =
                CreateTwoLayerItemCache<FixtureAssemblyItemDto, FixtureAssemblyItemDtoItem>(
                    (UInt16)SectionType.FixtureAssemblyItems,
                    delegate(FixtureAssemblyItemDto dto)
                    {
                        return new FixtureAssemblyItemDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new FixtureAssemblyItemDtoItem(item);
                    });
            _fixtureComponentItems =
                CreateTwoLayerItemCache<FixtureComponentItemDto, FixtureComponentItemDtoItem>(
                    (UInt16)SectionType.FixtureComponentItems,
                    delegate(FixtureComponentItemDto dto)
                    {
                        return new FixtureComponentItemDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new FixtureComponentItemDtoItem(item);
                    });
            _fixtureAssemblies =
                CreateTwoLayerItemCache<FixtureAssemblyDto, FixtureAssemblyDtoItem>(
                    (UInt16)SectionType.FixtureAssemblies,
                    delegate(FixtureAssemblyDto dto)
                    {
                        return new FixtureAssemblyDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new FixtureAssemblyDtoItem(item);
                    });
            _fixtureAssemblyComponents =
                CreateTwoLayerItemCache<FixtureAssemblyComponentDto, FixtureAssemblyComponentDtoItem>(
                    (UInt16)SectionType.FixtureAssemblyComponents,
                    delegate(FixtureAssemblyComponentDto dto)
                    {
                        return new FixtureAssemblyComponentDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new FixtureAssemblyComponentDtoItem(item);
                    });
            _fixtureComponents =
                CreateTwoLayerItemCache<FixtureComponentDto, FixtureComponentDtoItem>(
                    (UInt16)SectionType.FixtureComponents,
                    delegate(FixtureComponentDto dto)
                    {
                        return new FixtureComponentDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new FixtureComponentDtoItem(item);
                    });
            _fixtureSubComponents =
                CreateTwoLayerItemCache<FixtureSubComponentDto, FixtureSubComponentDtoItem>(
                    (UInt16)SectionType.FixtureSubComponents,
                    delegate(FixtureSubComponentDto dto)
                    {
                        return new FixtureSubComponentDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new FixtureSubComponentDtoItem(item);
                    });
            _fixtureImages =
                CreateTwoLayerItemCache<FixtureImageDto, FixtureImageDtoItem>(
                    (UInt16)SectionType.FixtureImages,
                    delegate(FixtureImageDto dto)
                    {
                        return new FixtureImageDtoItem(dto);
                    },
                    delegate(GalleriaBinaryFile.IItem item)
                    {
                        return new FixtureImageDtoItem(item);
                    });
            _fixtureAnnotations =
               CreateTwoLayerItemCache<FixtureAnnotationDto, FixtureAnnotationDtoItem>(
                   (UInt16)SectionType.FixtureAnnotations,
                   delegate(FixtureAnnotationDto dto)
                   {
                       return new FixtureAnnotationDtoItem(dto);
                   },
                   delegate(GalleriaBinaryFile.IItem item)
                   {
                       return new FixtureAnnotationDtoItem(item);
                   });
        }
        #endregion

        #region Properties

        public SingleItemCache<FixturePackageDto, FixturePackageDtoItem> FixturePackage
        {
            get { return _package; }
        }

        public TwoLayerItemCache<FixtureItemDto, FixtureItemDtoItem> FixtureItems
        {
            get { return _fixtureItems; }
        }

        public TwoLayerItemCache<FixtureDto, FixtureDtoItem> Fixtures
        {
            get { return _fixtures; }
        }

        public TwoLayerItemCache<FixtureAssemblyItemDto, FixtureAssemblyItemDtoItem> FixtureAssemblyItems
        {
            get { return _fixtureAssemblyItems; }
        }

        public TwoLayerItemCache<FixtureComponentItemDto, FixtureComponentItemDtoItem> FixtureComponentItems
        {
            get { return _fixtureComponentItems; }
        }

        public TwoLayerItemCache<FixtureAssemblyDto, FixtureAssemblyDtoItem> FixtureAssemblies
        {
            get { return _fixtureAssemblies; }
        }

        public TwoLayerItemCache<FixtureAssemblyComponentDto, FixtureAssemblyComponentDtoItem> FixtureAssemblyComponents
        {
            get { return _fixtureAssemblyComponents; }
        }

        public TwoLayerItemCache<FixtureComponentDto, FixtureComponentDtoItem> FixtureComponents
        {
            get { return _fixtureComponents; }
        }

        public TwoLayerItemCache<FixtureSubComponentDto, FixtureSubComponentDtoItem> FixtureSubComponents
        {
            get { return _fixtureSubComponents; }
        }

        public TwoLayerItemCache<FixtureImageDto, FixtureImageDtoItem> FixtureImages
        {
            get { return _fixtureImages; }
        }

        public TwoLayerItemCache<FixtureAnnotationDto, FixtureAnnotationDtoItem> FixtureAnnotations
        {
            get { return _fixtureAnnotations; }
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Called when committing this data
        /// </summary>
        protected override void OnCommit()
        {
            if (_package.Fetch() == null)
            {
                base.DeleteFile();
            }
            else
            {
                base.OnCommit();
            }
        }
        #endregion
    }
}
