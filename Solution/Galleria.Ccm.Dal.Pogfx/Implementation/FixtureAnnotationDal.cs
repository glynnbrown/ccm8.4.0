﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-32524 : L.Ineson
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Pogfx.Resources;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal.Gbf;

namespace Galleria.Ccm.Dal.Pogfx.Implementation
{

    public class FixtureAnnotationDal : DalBase<DalCache>, IFixtureAnnotationDal
    {
        #region IFixtureAssemblyComponentDal Members

        /// <summary>
        /// Fetches all items with a given FixturePackageId
        /// </summary>
        public IEnumerable<FixtureAnnotationDto> FetchByFixturePackageId(Object fixturePackageId)
        {
            return DalCache.FixtureAnnotations.FetchByParentId(fixturePackageId);
        }

        /// <summary>
        /// Inserts an item, assigning it the next available integer ID.
        /// </summary>
        public void Insert(FixtureAnnotationDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = (Int32)DalCache.FixtureAnnotations.Insert(dto);
        }

        /// <summary>
        /// Finds and updates an item, if it exists
        /// </summary>
        public void Update(FixtureAnnotationDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.FixtureAnnotations.Update(dto);
        }

        /// <summary>
        /// Finds and deletes an item by ID, if it exists
        /// </summary>
        public void DeleteById(Int32 id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.FixtureAnnotations.Delete(id);
        }

        #endregion

    }
}
