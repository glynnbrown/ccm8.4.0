﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal.Gbf;
using Galleria.Ccm.Dal.Pogfx.Resources;

namespace Galleria.Ccm.Dal.Pogfx.Implementation
{
    public class FixtureComponentDal : DalBase<DalCache>, IFixtureComponentDal
    {
        #region IFixtureComponentDal Members

        /// <summary>
        /// Fetches all items with a given FixturePackageId
        /// </summary>
        public IEnumerable<FixtureComponentDto> FetchByFixturePackageId(Object fixturePackageId)
        {
            return DalCache.FixtureComponents.FetchByParentId((Int32)fixturePackageId);
        }

        /// <summary>
        /// Inserts a item, assigning it the next available integer ID.
        /// </summary>
        public void Insert(FixtureComponentDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = (Int32)DalCache.FixtureComponents.Insert(dto);
        }

        /// <summary>
        /// Finds and updates an item by ID, if it already exists.
        /// </summary>
        public void Update(FixtureComponentDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.FixtureComponents.Update(dto);
        }

        /// <summary>
        /// Finds and deletes an item by ID, if it exists
        /// </summary>
        public void DeleteById(Int32 id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.FixtureComponents.Delete(id);
        }

        #endregion
    }
}
