﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Pogfx.Schema;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;

namespace Galleria.Ccm.Dal.Pogfx.Implementation.Items
{
    public class FixtureAssemblyComponentDtoItem : FixtureAssemblyComponentDto, IChildDtoItem<FixtureAssemblyComponentDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new FixtureAssemblyComponentDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public FixtureAssemblyComponentDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureAssemblyComponentId));
            FixtureAssemblyId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureAssemblyComponentFixtureAssemblyId));
            FixtureComponentId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureAssemblyComponentFixtureComponentId));
            X = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAssemblyComponentX));
            Y = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAssemblyComponentY));
            Z = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAssemblyComponentZ));
            Slope = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAssemblyComponentSlope));
            Angle = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAssemblyComponentAngle));
            Roll = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAssemblyComponentRoll));
        }

        /// <summary>
        /// Creates a new FixtureAssemblyComponentDtoItem and populates its properties with values taken from a 
        /// FixtureAssemblyComponentDto.
        /// </summary>
        public FixtureAssemblyComponentDtoItem(FixtureAssemblyComponentDto dto)
        {
            Id = dto.Id;
            FixtureAssemblyId = dto.FixtureAssemblyId;
            FixtureComponentId = dto.FixtureComponentId;
            X = dto.X;
            Y = dto.Y;
            Z = dto.Z;
            Slope = dto.Slope;
            Angle = dto.Angle;
            Roll = dto.Roll;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<FixtureAssemblyComponentDto>.Id
        {
            get { return Id; }
            set { Id = (Int32)value; }
        }

        Boolean IChildDtoItem<FixtureAssemblyComponentDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<FixtureAssemblyComponentDto>.ParentId
        {
            get { return (Int32)FixtureAssemblyId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public FixtureAssemblyComponentDto CopyDto()
        {
            return new FixtureAssemblyComponentDto()
            {
                Id = this.Id,
                FixtureAssemblyId = this.FixtureAssemblyId,
                FixtureComponentId = this.FixtureComponentId,
                X = this.X,
                Y = this.Y,
                Z = this.Z,
                Slope = this.Slope,
                Angle = this.Angle,
                Roll = this.Roll
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.FixtureAssemblyComponentId:
                    return Id;
                case FieldNames.FixtureAssemblyComponentFixtureAssemblyId:
                    return FixtureAssemblyId;
                case FieldNames.FixtureAssemblyComponentFixtureComponentId:
                    return FixtureComponentId;
                case FieldNames.FixtureAssemblyComponentX:
                    return X;
                case FieldNames.FixtureAssemblyComponentY:
                    return Y;
                case FieldNames.FixtureAssemblyComponentZ:
                    return Z;
                case FieldNames.FixtureAssemblyComponentSlope:
                    return Slope;
                case FieldNames.FixtureAssemblyComponentAngle:
                    return Angle;
                case FieldNames.FixtureAssemblyComponentRoll:
                    return Roll;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
