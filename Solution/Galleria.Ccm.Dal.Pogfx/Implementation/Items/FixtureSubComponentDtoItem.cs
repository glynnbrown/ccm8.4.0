﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Ineson
//		Created
// CCM-26091 : L.Ineson
//  Added more face thick properties
#endregion
#region Version History: (CCM CCM802)
// V8-29023 : M.Pettit
//		Added MerchandisableDepth
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Pogfx.Schema;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;

namespace Galleria.Ccm.Dal.Pogfx.Implementation.Items
{
    public class FixtureSubComponentDtoItem : FixtureSubComponentDto, IChildDtoItem<FixtureSubComponentDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new FixtureSubComponentDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public FixtureSubComponentDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentId));
            FixtureComponentId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentFixtureComponentId));
            Mesh3DId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentMesh3DId));
            ImageIdFront = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentImageIdFront));
            ImageIdBack = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentImageIdBack));
            ImageIdTop = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentImageIdTop));
            ImageIdBottom = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentImageIdBottom));
            ImageIdLeft = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentImageIdLeft));
            ImageIdRight = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentImageIdRight));
            Name = (String)item.GetItemPropertyValue(FieldNames.FixtureSubComponentName);
            Height = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentHeight));
            Width = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentWidth));
            Depth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentDepth));
            X = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentX));
            Y = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentY));
            Z = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentZ));
            Slope = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentSlope));
            Angle = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentAngle));
            Roll = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentRoll));
            ShapeType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.FixtureSubComponentShapeType));
            MerchandisableHeight = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentMerchandisableHeight));
            MerchandisableDepth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentMerchandisableDepth));
            IsVisible = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.FixtureSubComponentIsVisible));
            HasCollisionDetection = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.FixtureSubComponentHasCollisionDetection));
            FillPatternTypeFront = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.FixtureSubComponentFillPatternTypeFront));
            FillPatternTypeBack = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.FixtureSubComponentFillPatternTypeBack));
            FillPatternTypeTop = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.FixtureSubComponentFillPatternTypeTop));
            FillPatternTypeBottom = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.FixtureSubComponentFillPatternTypeBottom));
            FillPatternTypeLeft = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.FixtureSubComponentFillPatternTypeLeft));
            FillPatternTypeRight = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.FixtureSubComponentFillPatternTypeRight));
            FillColourFront = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentFillColourFront));
            FillColourBack = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentFillColourBack));
            FillColourTop = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentFillColourTop));
            FillColourBottom = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentFillColourBottom));
            FillColourLeft = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentFillColourLeft));
            FillColourRight = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentFillColourRight));
            LineColour = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentLineColour));
            TransparencyPercentFront = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentTransparencyPercentFront));
            TransparencyPercentBack = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentTransparencyPercentBack));
            TransparencyPercentTop = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentTransparencyPercentTop));
            TransparencyPercentBottom = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentTransparencyPercentBottom));
            TransparencyPercentLeft = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentTransparencyPercentLeft));
            TransparencyPercentRight = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentTransparencyPercentRight));
            FaceThicknessFront = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentFaceThicknessFront));
            FaceThicknessBack = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentFaceThicknessBack));
            FaceThicknessTop = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentFaceThicknessTop));
            FaceThicknessBottom = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentFaceThicknessBottom));
            FaceThicknessLeft = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentFaceThicknessLeft));
            FaceThicknessRight = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentFaceThicknessRight));
            RiserHeight = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentRiserHeight));
            RiserThickness = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentRiserThickness));
            IsRiserPlacedOnFront = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.FixtureSubComponentIsRiserPlacedOnFront));
            IsRiserPlacedOnBack = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.FixtureSubComponentIsRiserPlacedOnBack));
            IsRiserPlacedOnLeft = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.FixtureSubComponentIsRiserPlacedOnLeft));
            IsRiserPlacedOnRight = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.FixtureSubComponentIsRiserPlacedOnRight));
            RiserFillPatternType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.FixtureSubComponentRiserFillPatternType));
            RiserColour = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentRiserColour));
            RiserTransparencyPercent = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentRiserTransparencyPercent));
            NotchStartX = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentNotchStartX));
            NotchSpacingX = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentNotchSpacingX));
            NotchStartY = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentNotchStartY));
            NotchSpacingY = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentNotchSpacingY));
            NotchHeight = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentNotchHeight));
            NotchWidth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentNotchWidth));
            IsNotchPlacedOnFront = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.FixtureSubComponentIsNotchPlacedOnFront));
            IsNotchPlacedOnBack = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.FixtureSubComponentIsNotchPlacedOnBack));
            IsNotchPlacedOnLeft = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.FixtureSubComponentIsNotchPlacedOnLeft));
            IsNotchPlacedOnRight = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.FixtureSubComponentIsNotchPlacedOnRight));
            NotchStyleType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.FixtureSubComponentNotchStyleType));
            DividerObstructionHeight = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentDividerObstructionHeight));
            DividerObstructionWidth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentDividerObstructionWidth));
            DividerObstructionDepth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentDividerObstructionDepth));
            DividerObstructionStartX = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentDividerObstructionStartX));
            DividerObstructionSpacingX = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentDividerObstructionSpacingX));
            DividerObstructionStartY = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentDividerObstructionStartY));
            DividerObstructionSpacingY = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentDividerObstructionSpacingY));
            DividerObstructionStartZ = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentDividerObstructionStartZ));
            DividerObstructionSpacingZ = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentDividerObstructionSpacingZ));
            IsDividerObstructionAtStart = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.FixtureSubComponentIsDividerObstructionAtStart));
            IsDividerObstructionAtEnd = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.FixtureSubComponentIsDividerObstructionAtEnd));
            IsDividerObstructionByFacing = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.FixtureSubComponentIsDividerObstructionByFacing));
            DividerObstructionFillColour = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureSubComponentDividerObstructionFillColour));
            DividerObstructionFillPattern = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.FixtureSubComponentDividerObstructionFillPattern));
            MerchConstraintRow1StartX = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentMerchConstraintRow1StartX));
            MerchConstraintRow1SpacingX = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentMerchConstraintRow1SpacingX));
            MerchConstraintRow1StartY = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentMerchConstraintRow1StartY));
            MerchConstraintRow1SpacingY = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentMerchConstraintRow1SpacingY));
            MerchConstraintRow1Height = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentMerchConstraintRow1Height));
            MerchConstraintRow1Width = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentMerchConstraintRow1Width));
            MerchConstraintRow2StartX = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentMerchConstraintRow2StartX));
            MerchConstraintRow2SpacingX = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentMerchConstraintRow2SpacingX));
            MerchConstraintRow2StartY = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentMerchConstraintRow2StartY));
            MerchConstraintRow2SpacingY = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentMerchConstraintRow2SpacingY));
            MerchConstraintRow2Height = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentMerchConstraintRow2Height));
            MerchConstraintRow2Width = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentMerchConstraintRow2Width));
            LineThickness = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentLineThickness));
            MerchandisingType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.FixtureSubComponentMerchandisingType));
            CombineType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.FixtureSubComponentCombineType));
            IsProductOverlapAllowed = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.FixtureSubComponentIsProductOverlapAllowed));
            IsProductSqueezeAllowed = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.FixtureSubComponentIsProductSqueezeAllowed));
            MerchandisingStrategyX = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.FixtureSubComponentMerchandisingStrategyX));
            MerchandisingStrategyY = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.FixtureSubComponentMerchandisingStrategyY));
            MerchandisingStrategyZ = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.FixtureSubComponentMerchandisingStrategyZ));
            LeftOverhang = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentLeftOverhang));
            RightOverhang = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentRightOverhang));
            FrontOverhang = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentFrontOverhang));
            BackOverhang = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentBackOverhang));
            TopOverhang = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentTopOverhang));
            BottomOverhang = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureSubComponentBottomOverhang));

        }

        /// <summary>
        /// Creates a new FixtureSubComponentDtoItem and populates its properties with values taken from a 
        /// FixtureSubComponentDto.
        /// </summary>
        public FixtureSubComponentDtoItem(FixtureSubComponentDto dto)
        {
            Id = dto.Id;
            FixtureComponentId = dto.FixtureComponentId;
            Mesh3DId = dto.Mesh3DId;
            ImageIdFront = dto.ImageIdFront;
            ImageIdBack = dto.ImageIdBack;
            ImageIdTop = dto.ImageIdTop;
            ImageIdBottom = dto.ImageIdBottom;
            ImageIdLeft = dto.ImageIdLeft;
            ImageIdRight = dto.ImageIdRight;
            Name = dto.Name;
            Height = dto.Height;
            Width = dto.Width;
            Depth = dto.Depth;
            X = dto.X;
            Y = dto.Y;
            Z = dto.Z;
            Slope = dto.Slope;
            Angle = dto.Angle;
            Roll = dto.Roll;
            ShapeType = dto.ShapeType;
            MerchandisableHeight = dto.MerchandisableHeight;
            MerchandisableDepth = dto.MerchandisableDepth;
            IsVisible = dto.IsVisible;
            HasCollisionDetection = dto.HasCollisionDetection;
            FillPatternTypeFront = dto.FillPatternTypeFront;
            FillPatternTypeBack = dto.FillPatternTypeBack;
            FillPatternTypeTop = dto.FillPatternTypeTop;
            FillPatternTypeBottom = dto.FillPatternTypeBottom;
            FillPatternTypeLeft = dto.FillPatternTypeLeft;
            FillPatternTypeRight = dto.FillPatternTypeRight;
            FillColourFront = dto.FillColourFront;
            FillColourBack = dto.FillColourBack;
            FillColourTop = dto.FillColourTop;
            FillColourBottom = dto.FillColourBottom;
            FillColourLeft = dto.FillColourLeft;
            FillColourRight = dto.FillColourRight;
            LineColour = dto.LineColour;
            TransparencyPercentFront = dto.TransparencyPercentFront;
            TransparencyPercentBack = dto.TransparencyPercentBack;
            TransparencyPercentTop = dto.TransparencyPercentTop;
            TransparencyPercentBottom = dto.TransparencyPercentBottom;
            TransparencyPercentLeft = dto.TransparencyPercentLeft;
            TransparencyPercentRight = dto.TransparencyPercentRight;
            FaceThicknessFront = dto.FaceThicknessFront;
            FaceThicknessBack = dto.FaceThicknessBack;
            FaceThicknessTop = dto.FaceThicknessTop;
            FaceThicknessBottom = dto.FaceThicknessBottom;
            FaceThicknessLeft = dto.FaceThicknessLeft;
            FaceThicknessRight = dto.FaceThicknessRight;
            RiserHeight = dto.RiserHeight;
            RiserThickness = dto.RiserThickness;
            IsRiserPlacedOnFront = dto.IsRiserPlacedOnFront;
            IsRiserPlacedOnBack = dto.IsRiserPlacedOnBack;
            IsRiserPlacedOnLeft = dto.IsRiserPlacedOnLeft;
            IsRiserPlacedOnRight = dto.IsRiserPlacedOnRight;
            RiserFillPatternType = dto.RiserFillPatternType;
            RiserColour = dto.RiserColour;
            RiserTransparencyPercent = dto.RiserTransparencyPercent;
            NotchStartX = dto.NotchStartX;
            NotchSpacingX = dto.NotchSpacingX;
            NotchStartY = dto.NotchStartY;
            NotchSpacingY = dto.NotchSpacingY;
            NotchHeight = dto.NotchHeight;
            NotchWidth = dto.NotchWidth;
            IsNotchPlacedOnFront = dto.IsNotchPlacedOnFront;
            IsNotchPlacedOnBack = dto.IsNotchPlacedOnBack;
            IsNotchPlacedOnLeft = dto.IsNotchPlacedOnLeft;
            IsNotchPlacedOnRight = dto.IsNotchPlacedOnRight;
            NotchStyleType = dto.NotchStyleType;
            DividerObstructionHeight = dto.DividerObstructionHeight;
            DividerObstructionWidth = dto.DividerObstructionWidth;
            DividerObstructionDepth = dto.DividerObstructionDepth;
            DividerObstructionStartX = dto.DividerObstructionStartX;
            DividerObstructionSpacingX = dto.DividerObstructionSpacingX;
            DividerObstructionStartY = dto.DividerObstructionStartY;
            DividerObstructionSpacingY = dto.DividerObstructionSpacingY;
            DividerObstructionStartZ = dto.DividerObstructionStartZ;
            DividerObstructionSpacingZ = dto.DividerObstructionSpacingZ;
            IsDividerObstructionAtStart = dto.IsDividerObstructionAtStart;
            IsDividerObstructionAtEnd = dto.IsDividerObstructionAtEnd;
            IsDividerObstructionByFacing = dto.IsDividerObstructionByFacing;
            DividerObstructionFillColour = dto.DividerObstructionFillColour;
            DividerObstructionFillPattern = dto.DividerObstructionFillPattern;
            MerchConstraintRow1StartX = dto.MerchConstraintRow1StartX;
            MerchConstraintRow1SpacingX = dto.MerchConstraintRow1SpacingX;
            MerchConstraintRow1StartY = dto.MerchConstraintRow1StartY;
            MerchConstraintRow1SpacingY = dto.MerchConstraintRow1SpacingY;
            MerchConstraintRow1Height = dto.MerchConstraintRow1Height;
            MerchConstraintRow1Width = dto.MerchConstraintRow1Width;
            MerchConstraintRow2StartX = dto.MerchConstraintRow2StartX;
            MerchConstraintRow2SpacingX = dto.MerchConstraintRow2SpacingX;
            MerchConstraintRow2StartY = dto.MerchConstraintRow2StartY;
            MerchConstraintRow2SpacingY = dto.MerchConstraintRow2SpacingY;
            MerchConstraintRow2Height = dto.MerchConstraintRow2Height;
            MerchConstraintRow2Width = dto.MerchConstraintRow2Width;
            LineThickness = dto.LineThickness;
            MerchandisingType = dto.MerchandisingType;
            CombineType = dto.CombineType;
            IsProductOverlapAllowed = dto.IsProductOverlapAllowed;
            IsProductSqueezeAllowed = dto.IsProductSqueezeAllowed;
            MerchandisingStrategyX = dto.MerchandisingStrategyX;
            MerchandisingStrategyY = dto.MerchandisingStrategyY;
            MerchandisingStrategyZ = dto.MerchandisingStrategyZ;
            LeftOverhang = dto.LeftOverhang;
            RightOverhang = dto.RightOverhang;
            FrontOverhang = dto.FrontOverhang;
            BackOverhang = dto.BackOverhang;
            TopOverhang = dto.TopOverhang;
            BottomOverhang = dto.BottomOverhang;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<FixtureSubComponentDto>.Id
        {
            get { return Id; }
            set { Id = (Int32)value; }
        }

        Boolean IChildDtoItem<FixtureSubComponentDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<FixtureSubComponentDto>.ParentId
        {
            get { return (Int32)FixtureComponentId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public FixtureSubComponentDto CopyDto()
        {
            return new FixtureSubComponentDto()
            {
                Id = this.Id,
                FixtureComponentId = this.FixtureComponentId,
                Mesh3DId = this.Mesh3DId,
                ImageIdFront = this.ImageIdFront,
                ImageIdBack = this.ImageIdBack,
                ImageIdTop = this.ImageIdTop,
                ImageIdBottom = this.ImageIdBottom,
                ImageIdLeft = this.ImageIdLeft,
                ImageIdRight = this.ImageIdRight,
                Name = this.Name,
                Height = this.Height,
                Width = this.Width,
                Depth = this.Depth,
                X = this.X,
                Y = this.Y,
                Z = this.Z,
                Slope = this.Slope,
                Angle = this.Angle,
                Roll = this.Roll,
                ShapeType = this.ShapeType,
                MerchandisableHeight = this.MerchandisableHeight,
                MerchandisableDepth = this.MerchandisableDepth,
                IsVisible = this.IsVisible,
                HasCollisionDetection = this.HasCollisionDetection,
                FillPatternTypeFront = this.FillPatternTypeFront,
                FillPatternTypeBack = this.FillPatternTypeBack,
                FillPatternTypeTop = this.FillPatternTypeTop,
                FillPatternTypeBottom = this.FillPatternTypeBottom,
                FillPatternTypeLeft = this.FillPatternTypeLeft,
                FillPatternTypeRight = this.FillPatternTypeRight,
                FillColourFront = this.FillColourFront,
                FillColourBack = this.FillColourBack,
                FillColourTop = this.FillColourTop,
                FillColourBottom = this.FillColourBottom,
                FillColourLeft = this.FillColourLeft,
                FillColourRight = this.FillColourRight,
                LineColour = this.LineColour,
                TransparencyPercentFront = this.TransparencyPercentFront,
                TransparencyPercentBack = this.TransparencyPercentBack,
                TransparencyPercentTop = this.TransparencyPercentTop,
                TransparencyPercentBottom = this.TransparencyPercentBottom,
                TransparencyPercentLeft = this.TransparencyPercentLeft,
                TransparencyPercentRight = this.TransparencyPercentRight,
                FaceThicknessFront = this.FaceThicknessFront,
                FaceThicknessBack = this.FaceThicknessBack,
                FaceThicknessTop = this.FaceThicknessTop,
                FaceThicknessBottom = this.FaceThicknessBottom,
                FaceThicknessLeft = this.FaceThicknessLeft,
                FaceThicknessRight = this.FaceThicknessRight,
                RiserHeight = this.RiserHeight,
                RiserThickness = this.RiserThickness,
                IsRiserPlacedOnFront = this.IsRiserPlacedOnFront,
                IsRiserPlacedOnBack = this.IsRiserPlacedOnBack,
                IsRiserPlacedOnLeft = this.IsRiserPlacedOnLeft,
                IsRiserPlacedOnRight = this.IsRiserPlacedOnRight,
                RiserFillPatternType = this.RiserFillPatternType,
                RiserColour = this.RiserColour,
                RiserTransparencyPercent = this.RiserTransparencyPercent,
                NotchStartX = this.NotchStartX,
                NotchSpacingX = this.NotchSpacingX,
                NotchStartY = this.NotchStartY,
                NotchSpacingY = this.NotchSpacingY,
                NotchHeight = this.NotchHeight,
                NotchWidth = this.NotchWidth,
                IsNotchPlacedOnFront = this.IsNotchPlacedOnFront,
                IsNotchPlacedOnBack = this.IsNotchPlacedOnBack,
                IsNotchPlacedOnLeft = this.IsNotchPlacedOnLeft,
                IsNotchPlacedOnRight = this.IsNotchPlacedOnRight,
                NotchStyleType = this.NotchStyleType,
                DividerObstructionHeight = this.DividerObstructionHeight,
                DividerObstructionWidth = this.DividerObstructionWidth,
                DividerObstructionDepth = this.DividerObstructionDepth,
                DividerObstructionStartX = this.DividerObstructionStartX,
                DividerObstructionSpacingX = this.DividerObstructionSpacingX,
                DividerObstructionStartY = this.DividerObstructionStartY,
                DividerObstructionSpacingY = this.DividerObstructionSpacingY,
                DividerObstructionStartZ = this.DividerObstructionStartZ,
                DividerObstructionSpacingZ = this.DividerObstructionSpacingZ,
                DividerObstructionFillColour = this.DividerObstructionFillColour,
                DividerObstructionFillPattern = this.DividerObstructionFillPattern,
                MerchConstraintRow1StartX = this.MerchConstraintRow1StartX,
                MerchConstraintRow1SpacingX = this.MerchConstraintRow1SpacingX,
                MerchConstraintRow1StartY = this.MerchConstraintRow1StartY,
                MerchConstraintRow1SpacingY = this.MerchConstraintRow1SpacingY,
                MerchConstraintRow1Height = this.MerchConstraintRow1Height,
                MerchConstraintRow1Width = this.MerchConstraintRow1Width,
                MerchConstraintRow2StartX = this.MerchConstraintRow2StartX,
                MerchConstraintRow2SpacingX = this.MerchConstraintRow2SpacingX,
                MerchConstraintRow2StartY = this.MerchConstraintRow2StartY,
                MerchConstraintRow2SpacingY = this.MerchConstraintRow2SpacingY,
                MerchConstraintRow2Height = this.MerchConstraintRow2Height,
                MerchConstraintRow2Width = this.MerchConstraintRow2Width,
                LineThickness = this.LineThickness,
                MerchandisingType = this.MerchandisingType,
                CombineType = this.CombineType,
                IsProductOverlapAllowed = this.IsProductOverlapAllowed,
                IsProductSqueezeAllowed = this.IsProductSqueezeAllowed,
                MerchandisingStrategyX = this.MerchandisingStrategyX,
                MerchandisingStrategyY = this.MerchandisingStrategyY,
                MerchandisingStrategyZ = this.MerchandisingStrategyZ,
                LeftOverhang = this.LeftOverhang,
                RightOverhang = this.RightOverhang,
                FrontOverhang = this.FrontOverhang,
                BackOverhang = this.BackOverhang,
                TopOverhang = this.TopOverhang,
                BottomOverhang = this.BottomOverhang,
                IsDividerObstructionAtStart = this.IsDividerObstructionAtStart,
                IsDividerObstructionAtEnd = this.IsDividerObstructionAtEnd,
                IsDividerObstructionByFacing = this.IsDividerObstructionByFacing
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.FixtureSubComponentId:
                    return Id;
                case FieldNames.FixtureSubComponentFixtureComponentId:
                    return FixtureComponentId;
                case FieldNames.FixtureSubComponentMesh3DId:
                    return Mesh3DId;
                case FieldNames.FixtureSubComponentImageIdFront:
                    return ImageIdFront;
                case FieldNames.FixtureSubComponentImageIdBack:
                    return ImageIdBack;
                case FieldNames.FixtureSubComponentImageIdTop:
                    return ImageIdTop;
                case FieldNames.FixtureSubComponentImageIdBottom:
                    return ImageIdBottom;
                case FieldNames.FixtureSubComponentImageIdLeft:
                    return ImageIdLeft;
                case FieldNames.FixtureSubComponentImageIdRight:
                    return ImageIdRight;
                case FieldNames.FixtureSubComponentName:
                    return Name;
                case FieldNames.FixtureSubComponentHeight:
                    return Height;
                case FieldNames.FixtureSubComponentWidth:
                    return Width;
                case FieldNames.FixtureSubComponentDepth:
                    return Depth;
                case FieldNames.FixtureSubComponentX:
                    return X;
                case FieldNames.FixtureSubComponentY:
                    return Y;
                case FieldNames.FixtureSubComponentZ:
                    return Z;
                case FieldNames.FixtureSubComponentSlope:
                    return Slope;
                case FieldNames.FixtureSubComponentAngle:
                    return Angle;
                case FieldNames.FixtureSubComponentRoll:
                    return Roll;
                case FieldNames.FixtureSubComponentShapeType:
                    return ShapeType;
                case FieldNames.FixtureSubComponentMerchandisableHeight:
                    return MerchandisableHeight;
                case FieldNames.FixtureSubComponentMerchandisableDepth:
                    return MerchandisableDepth;
                case FieldNames.FixtureSubComponentIsVisible:
                    return IsVisible;
                case FieldNames.FixtureSubComponentHasCollisionDetection:
                    return HasCollisionDetection;
                case FieldNames.FixtureSubComponentFillPatternTypeFront:
                    return FillPatternTypeFront;
                case FieldNames.FixtureSubComponentFillPatternTypeBack:
                    return FillPatternTypeBack;
                case FieldNames.FixtureSubComponentFillPatternTypeTop:
                    return FillPatternTypeTop;
                case FieldNames.FixtureSubComponentFillPatternTypeBottom:
                    return FillPatternTypeBottom;
                case FieldNames.FixtureSubComponentFillPatternTypeLeft:
                    return FillPatternTypeLeft;
                case FieldNames.FixtureSubComponentFillPatternTypeRight:
                    return FillPatternTypeRight;
                case FieldNames.FixtureSubComponentFillColourFront:
                    return FillColourFront;
                case FieldNames.FixtureSubComponentFillColourBack:
                    return FillColourBack;
                case FieldNames.FixtureSubComponentFillColourTop:
                    return FillColourTop;
                case FieldNames.FixtureSubComponentFillColourBottom:
                    return FillColourBottom;
                case FieldNames.FixtureSubComponentFillColourLeft:
                    return FillColourLeft;
                case FieldNames.FixtureSubComponentFillColourRight:
                    return FillColourRight;
                case FieldNames.FixtureSubComponentLineColour:
                    return LineColour;
                case FieldNames.FixtureSubComponentTransparencyPercentFront:
                    return TransparencyPercentFront;
                case FieldNames.FixtureSubComponentTransparencyPercentBack:
                    return TransparencyPercentBack;
                case FieldNames.FixtureSubComponentTransparencyPercentTop:
                    return TransparencyPercentTop;
                case FieldNames.FixtureSubComponentTransparencyPercentBottom:
                    return TransparencyPercentBottom;
                case FieldNames.FixtureSubComponentTransparencyPercentLeft:
                    return TransparencyPercentLeft;
                case FieldNames.FixtureSubComponentTransparencyPercentRight:
                    return TransparencyPercentRight;
                case FieldNames.FixtureSubComponentFaceThicknessFront:
                    return FaceThicknessFront;
                case FieldNames.FixtureSubComponentFaceThicknessBack:
                    return FaceThicknessBack;
                case FieldNames.FixtureSubComponentFaceThicknessTop:
                    return FaceThicknessTop;
                case FieldNames.FixtureSubComponentFaceThicknessBottom:
                    return FaceThicknessBottom;
                case FieldNames.FixtureSubComponentFaceThicknessLeft:
                    return FaceThicknessLeft;
                case FieldNames.FixtureSubComponentFaceThicknessRight:
                    return FaceThicknessRight;
                case FieldNames.FixtureSubComponentRiserHeight:
                    return RiserHeight;
                case FieldNames.FixtureSubComponentRiserThickness:
                    return RiserThickness;
                case FieldNames.FixtureSubComponentIsRiserPlacedOnFront:
                    return IsRiserPlacedOnFront;
                case FieldNames.FixtureSubComponentIsRiserPlacedOnBack:
                    return IsRiserPlacedOnBack;
                case FieldNames.FixtureSubComponentIsRiserPlacedOnLeft:
                    return IsRiserPlacedOnLeft;
                case FieldNames.FixtureSubComponentIsRiserPlacedOnRight:
                    return IsRiserPlacedOnRight;
                case FieldNames.FixtureSubComponentRiserFillPatternType:
                    return RiserFillPatternType;
                case FieldNames.FixtureSubComponentRiserColour:
                    return RiserColour;
                case FieldNames.FixtureSubComponentRiserTransparencyPercent:
                    return RiserTransparencyPercent;
                case FieldNames.FixtureSubComponentNotchStartX:
                    return NotchStartX;
                case FieldNames.FixtureSubComponentNotchSpacingX:
                    return NotchSpacingX;
                case FieldNames.FixtureSubComponentNotchStartY:
                    return NotchStartY;
                case FieldNames.FixtureSubComponentNotchSpacingY:
                    return NotchSpacingY;
                case FieldNames.FixtureSubComponentNotchHeight:
                    return NotchHeight;
                case FieldNames.FixtureSubComponentNotchWidth:
                    return NotchWidth;
                case FieldNames.FixtureSubComponentIsNotchPlacedOnFront:
                    return IsNotchPlacedOnFront;
                case FieldNames.FixtureSubComponentIsNotchPlacedOnBack:
                    return IsNotchPlacedOnBack;
                case FieldNames.FixtureSubComponentIsNotchPlacedOnLeft:
                    return IsNotchPlacedOnLeft;
                case FieldNames.FixtureSubComponentIsNotchPlacedOnRight:
                    return IsNotchPlacedOnRight;
                case FieldNames.FixtureSubComponentNotchStyleType:
                    return NotchStyleType;
                case FieldNames.FixtureSubComponentDividerObstructionHeight:
                    return DividerObstructionHeight;
                case FieldNames.FixtureSubComponentDividerObstructionWidth:
                    return DividerObstructionWidth;
                case FieldNames.FixtureSubComponentDividerObstructionDepth:
                    return DividerObstructionDepth;
                case FieldNames.FixtureSubComponentDividerObstructionStartX:
                    return DividerObstructionStartX;
                case FieldNames.FixtureSubComponentDividerObstructionSpacingX:
                    return DividerObstructionSpacingX;
                case FieldNames.FixtureSubComponentDividerObstructionStartY:
                    return DividerObstructionStartY;
                case FieldNames.FixtureSubComponentDividerObstructionSpacingY:
                    return DividerObstructionSpacingY;
                case FieldNames.FixtureSubComponentDividerObstructionStartZ:
                    return DividerObstructionStartZ;
                case FieldNames.FixtureSubComponentDividerObstructionSpacingZ:
                    return DividerObstructionSpacingZ;
                case FieldNames.FixtureSubComponentDividerObstructionFillColour:
                    return DividerObstructionFillColour;
                case FieldNames.FixtureSubComponentDividerObstructionFillPattern:
                    return DividerObstructionFillPattern;
                case FieldNames.FixtureSubComponentMerchConstraintRow1StartX:
                    return MerchConstraintRow1StartX;
                case FieldNames.FixtureSubComponentMerchConstraintRow1SpacingX:
                    return MerchConstraintRow1SpacingX;
                case FieldNames.FixtureSubComponentMerchConstraintRow1StartY:
                    return MerchConstraintRow1StartY;
                case FieldNames.FixtureSubComponentMerchConstraintRow1SpacingY:
                    return MerchConstraintRow1SpacingY;
                case FieldNames.FixtureSubComponentMerchConstraintRow1Height:
                    return MerchConstraintRow1Height;
                case FieldNames.FixtureSubComponentMerchConstraintRow1Width:
                    return MerchConstraintRow1Width;
                case FieldNames.FixtureSubComponentMerchConstraintRow2StartX:
                    return MerchConstraintRow2StartX;
                case FieldNames.FixtureSubComponentMerchConstraintRow2SpacingX:
                    return MerchConstraintRow2SpacingX;
                case FieldNames.FixtureSubComponentMerchConstraintRow2StartY:
                    return MerchConstraintRow2StartY;
                case FieldNames.FixtureSubComponentMerchConstraintRow2SpacingY:
                    return MerchConstraintRow2SpacingY;
                case FieldNames.FixtureSubComponentMerchConstraintRow2Height:
                    return MerchConstraintRow2Height;
                case FieldNames.FixtureSubComponentMerchConstraintRow2Width:
                    return MerchConstraintRow2Width;
                case FieldNames.FixtureSubComponentLineThickness:
                    return LineThickness;
                case FieldNames.FixtureSubComponentMerchandisingType:
                    return MerchandisingType;
                case FieldNames.FixtureSubComponentCombineType:
                    return CombineType;
                case FieldNames.FixtureSubComponentIsProductOverlapAllowed:
                    return IsProductOverlapAllowed;
                case FieldNames.FixtureSubComponentIsProductSqueezeAllowed:
                    return IsProductSqueezeAllowed;
                case FieldNames.FixtureSubComponentMerchandisingStrategyX:
                    return MerchandisingStrategyX;
                case FieldNames.FixtureSubComponentMerchandisingStrategyY:
                    return MerchandisingStrategyY;
                case FieldNames.FixtureSubComponentMerchandisingStrategyZ:
                    return MerchandisingStrategyZ;
                case FieldNames.FixtureSubComponentLeftOverhang:
                    return LeftOverhang;
                case FieldNames.FixtureSubComponentRightOverhang:
                    return RightOverhang;
                case FieldNames.FixtureSubComponentFrontOverhang:
                    return FrontOverhang;
                case FieldNames.FixtureSubComponentBackOverhang:
                    return BackOverhang;
                case FieldNames.FixtureSubComponentTopOverhang:
                    return TopOverhang;
                case FieldNames.FixtureSubComponentBottomOverhang:
                    return BottomOverhang;
                case FieldNames.FixtureSubComponentIsDividerObstructionAtStart:
                    return IsDividerObstructionAtStart;
                case FieldNames.FixtureSubComponentIsDividerObstructionAtEnd:
                    return IsDividerObstructionAtEnd;
                case FieldNames.FixtureSubComponentIsDividerObstructionByFacing:
                    return IsDividerObstructionByFacing;
                default:
                    return null;
            }
        }

        #endregion

        #endregion

    }
}
