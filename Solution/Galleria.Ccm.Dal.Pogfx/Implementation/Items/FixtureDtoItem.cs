﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Pogfx.Schema;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;

namespace Galleria.Ccm.Dal.Pogfx.Implementation.Items
{
    public class FixtureDtoItem : FixtureDto, IChildDtoItem<FixtureDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new FixtureDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public FixtureDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureId));
            FixturePackageId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureFixturePackageId));
            Name = (String)item.GetItemPropertyValue(FieldNames.FixtureName);
            Height = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureHeight));
            Width = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureWidth));
            Depth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureDepth));
            NumberOfAssemblies = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.FixtureNumberOfAssemblies));
            NumberOfMerchandisedSubComponents = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.FixtureNumberOfMerchandisedSubComponents));
            TotalFixtureCost = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureTotalFixtureCost));
        }

        /// <summary>
        /// Creates a new FixtureDtoItem and populates its properties with values taken from a 
        /// FixtureDto.
        /// </summary>
        public FixtureDtoItem(FixtureDto dto)
        {
            Id = dto.Id;
            FixturePackageId = dto.FixturePackageId;
            Name = dto.Name;
            Height = dto.Height;
            Width = dto.Width;
            Depth = dto.Depth;
            NumberOfAssemblies = dto.NumberOfAssemblies;
            NumberOfMerchandisedSubComponents = dto.NumberOfMerchandisedSubComponents;
            TotalFixtureCost = dto.TotalFixtureCost;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<FixtureDto>.Id
        {
            get { return Id; }
            set { Id = (Int32)value; }
        }

        Boolean IChildDtoItem<FixtureDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<FixtureDto>.ParentId
        {
            get { return (Int32)FixturePackageId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public FixtureDto CopyDto()
        {
            return new FixtureDto()
            {
                Id = this.Id,
                FixturePackageId = this.FixturePackageId,
                Name = this.Name,
                Height = this.Height,
                Width = this.Width,
                Depth = this.Depth,
                NumberOfAssemblies = this.NumberOfAssemblies,
                NumberOfMerchandisedSubComponents = this.NumberOfMerchandisedSubComponents,
                TotalFixtureCost = this.TotalFixtureCost,
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.FixtureId:
                    return Id;
                case FieldNames.FixtureFixturePackageId:
                    return FixturePackageId;
                case FieldNames.FixtureName:
                    return Name;
                case FieldNames.FixtureHeight:
                    return Height;
                case FieldNames.FixtureWidth:
                    return Width;
                case FieldNames.FixtureDepth:
                    return Depth;
                case FieldNames.FixtureNumberOfAssemblies:
                    return NumberOfAssemblies;
                case FieldNames.FixtureNumberOfMerchandisedSubComponents:
                    return NumberOfMerchandisedSubComponents;
                case FieldNames.FixtureTotalFixtureCost:
                    return TotalFixtureCost;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
