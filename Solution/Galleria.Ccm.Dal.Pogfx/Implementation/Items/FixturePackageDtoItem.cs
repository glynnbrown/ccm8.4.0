﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
// V8-25873 : A.Probyn
//  Added Custom Attribute 1 - 5
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Pogfx.Schema;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;

namespace Galleria.Ccm.Dal.Pogfx.Implementation.Items
{
    public class FixturePackageDtoItem : FixturePackageDto, IParentDtoItem<FixturePackageDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new FixturePackageDtoItem and populates its properties with values taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public FixturePackageDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixturePackageId));
            Name = (String)item.GetItemPropertyValue(FieldNames.FixturePackageName);
            Height = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixturePackageHeight));
            Width = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixturePackageWidth));
            Depth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixturePackageDepth));
            FixtureCount = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixturePackageFixtureCount));
            AssemblyCount = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixturePackageAssemblyCount));
            ComponentCount = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixturePackageComponentCount));
            LengthUnitsOfMeasure = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.FixturePackageLengthUnitsOfMeasure));
            AreaUnitsOfMeasure = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.FixturePackageAreaUnitsOfMeasure));
            VolumeUnitsOfMeasure = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.FixturePackageVolumeUnitsOfMeasure));
            WeightUnitsOfMeasure = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.FixturePackageWeightUnitsOfMeasure));
            AngleUnitsOfMeasure = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.FixturePackageAngleUnitsOfMeasure));
            CurrencyUnitsOfMeasure = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.FixturePackageCurrencyUnitsOfMeasure));
            Description = (String)item.GetItemPropertyValue(FieldNames.FixturePackageDescription);
            DateCreated = GalleriaBinaryFileItemHelper.GetDateTime(item.GetItemPropertyValue(FieldNames.FixturePackageDateCreated));
            DateLastModified = GalleriaBinaryFileItemHelper.GetDateTime(item.GetItemPropertyValue(FieldNames.FixturePackageDateLastModified));
            CreatedBy = (String)item.GetItemPropertyValue(FieldNames.FixturePackageCreatedBy);
            LastModifiedBy = (String)item.GetItemPropertyValue(FieldNames.FixturePackageLastModifiedBy);
            ThumbnailImageData = (Byte[])item.GetItemPropertyValue(FieldNames.FixturePackageThumbnailImageData);
            CustomAttribute1 = (String)item.GetItemPropertyValue(FieldNames.FixturePackageCustomAttribute1);
            CustomAttribute2 = (String)item.GetItemPropertyValue(FieldNames.FixturePackageCustomAttribute2);
            CustomAttribute3 = (String)item.GetItemPropertyValue(FieldNames.FixturePackageCustomAttribute3);
            CustomAttribute4 = (String)item.GetItemPropertyValue(FieldNames.FixturePackageCustomAttribute4);
            CustomAttribute5 = (String)item.GetItemPropertyValue(FieldNames.FixturePackageCustomAttribute5);
        }

        /// <summary>
        /// Creates a new FixturePackageDtoItem and populates its properties with values taken from a FixturePackageDto.
        /// </summary>
        public FixturePackageDtoItem(FixturePackageDto dto)
        {
            Id = dto.Id;
            Name = dto.Name;
            Height = dto.Height;
            Width = dto.Width;
            Depth = dto.Depth;
            FixtureCount = dto.FixtureCount;
            AssemblyCount = dto.AssemblyCount;
            ComponentCount = dto.ComponentCount;
            LengthUnitsOfMeasure = dto.LengthUnitsOfMeasure;
            AreaUnitsOfMeasure = dto.AreaUnitsOfMeasure;
            VolumeUnitsOfMeasure = dto.VolumeUnitsOfMeasure;
            WeightUnitsOfMeasure = dto.WeightUnitsOfMeasure;
            AngleUnitsOfMeasure = dto.AngleUnitsOfMeasure;
            CurrencyUnitsOfMeasure = dto.CurrencyUnitsOfMeasure;
            Description = dto.Description;
            DateCreated = dto.DateCreated;
            DateLastModified = dto.DateLastModified;
            CreatedBy = dto.CreatedBy;
            LastModifiedBy = dto.LastModifiedBy;
            ThumbnailImageData = dto.ThumbnailImageData;
            CustomAttribute1 = dto.CustomAttribute1;
            CustomAttribute2 = dto.CustomAttribute2;
            CustomAttribute3 = dto.CustomAttribute3;
            CustomAttribute4 = dto.CustomAttribute4;
            CustomAttribute5 = dto.CustomAttribute5;
        }

        #endregion

        #region Public Properties

        Object IParentDtoItem<FixturePackageDto>.Id
        {
            get { return Id; }
            set { Id = (Int32)value; }
        }

        Boolean IParentDtoItem<FixturePackageDto>.AutoIncrementId
        {
            get { return true; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public FixturePackageDto CopyDto()
        {
            return new FixturePackageDto()
            {
                Id = this.Id,
                Name = this.Name,
                Description = this.Description,
                Height = this.Height,
                Width = this.Width,
                Depth = this.Depth,
                FixtureCount = this.FixtureCount,
                AssemblyCount = this.AssemblyCount,
                ComponentCount = this.ComponentCount,
                LengthUnitsOfMeasure = this.LengthUnitsOfMeasure,
                AreaUnitsOfMeasure = this.AreaUnitsOfMeasure,
                VolumeUnitsOfMeasure = this.VolumeUnitsOfMeasure,
                WeightUnitsOfMeasure = this.WeightUnitsOfMeasure,
                AngleUnitsOfMeasure = this.AngleUnitsOfMeasure,
                CurrencyUnitsOfMeasure = this.CurrencyUnitsOfMeasure,
                DateCreated = this.DateCreated,
                DateLastModified = this.DateLastModified,
                CreatedBy = this.CreatedBy,
                LastModifiedBy = this.LastModifiedBy,
                ThumbnailImageData = this.ThumbnailImageData,
                CustomAttribute1 = this.CustomAttribute1,
                CustomAttribute2 = this.CustomAttribute2,
                CustomAttribute3 = this.CustomAttribute3,
                CustomAttribute4 = this.CustomAttribute4,
                CustomAttribute5 = this.CustomAttribute5,
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.FixturePackageId:
                    return Id;
                case FieldNames.FixturePackageName:
                    return Name;
                case FieldNames.FixturePackageDescription:
                    return Description;
                case FieldNames.FixturePackageHeight:
                    return Height;
                case FieldNames.FixturePackageWidth:
                    return Width;
                case FieldNames.FixturePackageDepth:
                    return Depth;
                case FieldNames.FixturePackageFixtureCount:
                    return this.FixtureCount;
                case FieldNames.FixturePackageAssemblyCount:
                    return this.AssemblyCount;
                case FieldNames.FixturePackageComponentCount:
                    return this.ComponentCount;
                case FieldNames.FixturePackageLengthUnitsOfMeasure:
                    return LengthUnitsOfMeasure;
                case FieldNames.FixturePackageAreaUnitsOfMeasure:
                    return AreaUnitsOfMeasure;
                case FieldNames.FixturePackageVolumeUnitsOfMeasure:
                    return VolumeUnitsOfMeasure;
                case FieldNames.FixturePackageWeightUnitsOfMeasure:
                    return WeightUnitsOfMeasure;
                case FieldNames.FixturePackageAngleUnitsOfMeasure:
                    return AngleUnitsOfMeasure;
                case FieldNames.FixturePackageCurrencyUnitsOfMeasure:
                    return CurrencyUnitsOfMeasure;
                case FieldNames.FixturePackageDateCreated:
                    return DateCreated;
                case FieldNames.FixturePackageDateLastModified:
                    return DateLastModified;
                case FieldNames.FixturePackageCreatedBy:
                    return CreatedBy;
                case FieldNames.FixturePackageLastModifiedBy:
                    return LastModifiedBy;
                case FieldNames.FixturePackageThumbnailImageData:
                    return ThumbnailImageData;
                case FieldNames.FixturePackageCustomAttribute1:
                    return CustomAttribute1;
                case FieldNames.FixturePackageCustomAttribute2:
                    return CustomAttribute2;
                case FieldNames.FixturePackageCustomAttribute3:
                    return CustomAttribute3;
                case FieldNames.FixturePackageCustomAttribute4:
                    return CustomAttribute4;
                case FieldNames.FixturePackageCustomAttribute5:
                    return CustomAttribute5;

                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
