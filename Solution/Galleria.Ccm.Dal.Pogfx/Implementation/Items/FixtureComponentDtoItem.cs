﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Pogfx.Schema;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;

namespace Galleria.Ccm.Dal.Pogfx.Implementation.Items
{
    public class FixtureComponentDtoItem : FixtureComponentDto, IChildDtoItem<FixtureComponentDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new FixtureComponentDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public FixtureComponentDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureAssemblyItemId));
            FixturePackageId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureComponentFixturePackageId));
            Mesh3DId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureComponentMesh3DId));
            ImageIdFront = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureComponentImageIdFront));
            ImageIdBack = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureComponentImageIdBack));
            ImageIdTop = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureComponentImageIdTop));
            ImageIdBottom = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureComponentImageIdBottom));
            ImageIdLeft = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureComponentImageIdLeft));
            ImageIdRight = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureComponentImageIdRight));
            Name = (String)item.GetItemPropertyValue(FieldNames.FixtureComponentName);
            Height = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureComponentHeight));
            Width = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureComponentWidth));
            Depth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureComponentDepth));
            NumberOfSubComponents = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.FixtureComponentNumberOfSubComponents));
            NumberOfMerchandisedSubComponents = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.FixtureComponentNumberOfMerchandisedSubComponents));
            NumberOfShelfEdgeLabels = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.FixtureComponentNumberOfShelfEdgeLabels));
            IsMoveable = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.FixtureComponentIsMoveable));
            IsDisplayOnly = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.FixtureComponentIsDisplayOnly));
            CanAttachShelfEdgeLabel = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.FixtureComponentCanAttachShelfEdgeLabel));
            RetailerReferenceCode = (String)item.GetItemPropertyValue(FieldNames.FixtureComponentRetailerReferenceCode);
            BarCode = (String)item.GetItemPropertyValue(FieldNames.FixtureComponentBarCode);
            Manufacturer = (String)item.GetItemPropertyValue(FieldNames.FixtureComponentManufacturer);
            ManufacturerPartName = (String)item.GetItemPropertyValue(FieldNames.FixtureComponentManufacturerPartName);
            ManufacturerPartNumber = (String)item.GetItemPropertyValue(FieldNames.FixtureComponentManufacturerPartNumber);
            SupplierName = (String)item.GetItemPropertyValue(FieldNames.FixtureComponentSupplierName);
            SupplierPartNumber = (String)item.GetItemPropertyValue(FieldNames.FixtureComponentSupplierPartNumber);
            SupplierCostPrice = (Single?)item.GetItemPropertyValue(FieldNames.FixtureComponentSupplierCostPrice);
            SupplierDiscount = (Single?)item.GetItemPropertyValue(FieldNames.FixtureComponentSupplierDiscount);
            SupplierLeadTime = (Single?)item.GetItemPropertyValue(FieldNames.FixtureComponentSupplierLeadTime);
            MinPurchaseQty = (Int32?)item.GetItemPropertyValue(FieldNames.FixtureComponentMinPurchaseQty);
            WeightLimit = (Single?)item.GetItemPropertyValue(FieldNames.FixtureComponentWeightLimit);
            Weight = (Single?)item.GetItemPropertyValue(FieldNames.FixtureComponentWeight);
            Volume = (Single?)item.GetItemPropertyValue(FieldNames.FixtureComponentVolume);
            Diameter = (Single?)item.GetItemPropertyValue(FieldNames.FixtureComponentDiameter);
            Capacity = (Int16?)item.GetItemPropertyValue(FieldNames.FixtureComponentCapacity);
            ComponentType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.FixtureComponentComponentType));
            IsMerchandisedTopDown = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.FixtureComponentIsMerchandisedTopDown));
        }

        /// <summary>
        /// Creates a new FixtureComponentDtoItem and populates its properties with values taken from a 
        /// FixtureComponentDto.
        /// </summary>
        public FixtureComponentDtoItem(FixtureComponentDto dto)
        {
            Id = dto.Id;
            FixturePackageId = dto.FixturePackageId;
            Mesh3DId = dto.Mesh3DId;
            ImageIdFront = dto.ImageIdFront;
            ImageIdBack = dto.ImageIdBack;
            ImageIdTop = dto.ImageIdTop;
            ImageIdBottom = dto.ImageIdBottom;
            ImageIdLeft = dto.ImageIdLeft;
            ImageIdRight = dto.ImageIdRight;
            Name = dto.Name;
            Height = dto.Height;
            Width = dto.Width;
            Depth = dto.Depth;
            NumberOfSubComponents = dto.NumberOfSubComponents;
            NumberOfMerchandisedSubComponents = dto.NumberOfMerchandisedSubComponents;
            NumberOfShelfEdgeLabels = dto.NumberOfShelfEdgeLabels;
            IsMoveable = dto.IsMoveable;
            IsDisplayOnly = dto.IsDisplayOnly;
            CanAttachShelfEdgeLabel = dto.CanAttachShelfEdgeLabel;
            RetailerReferenceCode = dto.RetailerReferenceCode;
            BarCode = dto.BarCode;
            Manufacturer = dto.Manufacturer;
            ManufacturerPartName = dto.ManufacturerPartName;
            ManufacturerPartNumber = dto.ManufacturerPartNumber;
            SupplierName = dto.SupplierName;
            SupplierPartNumber = dto.SupplierPartNumber;
            SupplierCostPrice = dto.SupplierCostPrice;
            SupplierDiscount = dto.SupplierDiscount;
            SupplierLeadTime = dto.SupplierLeadTime;
            MinPurchaseQty = dto.MinPurchaseQty;
            WeightLimit = dto.WeightLimit;
            Weight = dto.Weight;
            Volume = dto.Volume;
            Diameter = dto.Diameter;
            Capacity = dto.Capacity;
            ComponentType = dto.ComponentType;
            IsMerchandisedTopDown = dto.IsMerchandisedTopDown;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<FixtureComponentDto>.Id
        {
            get { return Id; }
            set { Id = (Int32)value; }
        }

        Boolean IChildDtoItem<FixtureComponentDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<FixtureComponentDto>.ParentId
        {
            get { return (Int32)FixturePackageId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public FixtureComponentDto CopyDto()
        {
            return new FixtureComponentDto()
            {
                Id = this.Id,
                FixturePackageId = this.FixturePackageId,
                Mesh3DId = this.Mesh3DId,
                ImageIdFront = this.ImageIdFront,
                ImageIdBack = this.ImageIdBack,
                ImageIdTop = this.ImageIdTop,
                ImageIdBottom = this.ImageIdBottom,
                ImageIdLeft = this.ImageIdLeft,
                ImageIdRight = this.ImageIdRight,
                Name = this.Name,
                Height = this.Height,
                Width = this.Width,
                Depth = this.Depth,
                NumberOfSubComponents = this.NumberOfSubComponents,
                NumberOfMerchandisedSubComponents = this.NumberOfMerchandisedSubComponents,
                NumberOfShelfEdgeLabels = this.NumberOfShelfEdgeLabels,
                IsMoveable = this.IsMoveable,
                IsDisplayOnly = this.IsDisplayOnly,
                CanAttachShelfEdgeLabel = this.CanAttachShelfEdgeLabel,
                RetailerReferenceCode = this.RetailerReferenceCode,
                BarCode = this.BarCode,
                Manufacturer = this.Manufacturer,
                ManufacturerPartName = this.ManufacturerPartName,
                ManufacturerPartNumber = this.ManufacturerPartNumber,
                SupplierName = this.SupplierName,
                SupplierPartNumber = this.SupplierPartNumber,
                SupplierCostPrice = this.SupplierCostPrice,
                SupplierDiscount = this.SupplierDiscount,
                SupplierLeadTime = this.SupplierLeadTime,
                MinPurchaseQty = this.MinPurchaseQty,
                WeightLimit = this.WeightLimit,
                Weight = this.Weight,
                Volume = this.Volume,
                Diameter = this.Diameter,
                Capacity = this.Capacity,
                ComponentType = this.ComponentType,
                IsMerchandisedTopDown = this.IsMerchandisedTopDown
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.FixtureComponentId:
                    return Id;
                case FieldNames.FixtureComponentFixturePackageId:
                    return FixturePackageId;
                case FieldNames.FixtureComponentMesh3DId:
                    return Mesh3DId;
                case FieldNames.FixtureComponentImageIdFront:
                    return ImageIdFront;
                case FieldNames.FixtureComponentImageIdBack:
                    return ImageIdBack;
                case FieldNames.FixtureComponentImageIdTop:
                    return ImageIdTop;
                case FieldNames.FixtureComponentImageIdBottom:
                    return ImageIdBottom;
                case FieldNames.FixtureComponentImageIdLeft:
                    return ImageIdLeft;
                case FieldNames.FixtureComponentImageIdRight:
                    return ImageIdRight;
                case FieldNames.FixtureComponentName:
                    return Name;
                case FieldNames.FixtureComponentHeight:
                    return Height;
                case FieldNames.FixtureComponentWidth:
                    return Width;
                case FieldNames.FixtureComponentDepth:
                    return Depth;
                case FieldNames.FixtureComponentNumberOfSubComponents:
                    return NumberOfSubComponents;
                case FieldNames.FixtureComponentNumberOfMerchandisedSubComponents:
                    return NumberOfMerchandisedSubComponents;
                case FieldNames.FixtureComponentNumberOfShelfEdgeLabels:
                    return NumberOfShelfEdgeLabels;
                case FieldNames.FixtureComponentIsMoveable:
                    return IsMoveable;
                case FieldNames.FixtureComponentIsDisplayOnly:
                    return IsDisplayOnly;
                case FieldNames.FixtureComponentCanAttachShelfEdgeLabel:
                    return CanAttachShelfEdgeLabel;
                case FieldNames.FixtureComponentRetailerReferenceCode:
                    return RetailerReferenceCode;
                case FieldNames.FixtureComponentBarCode:
                    return BarCode;
                case FieldNames.FixtureComponentManufacturer:
                    return Manufacturer;
                case FieldNames.FixtureComponentManufacturerPartName:
                    return ManufacturerPartName;
                case FieldNames.FixtureComponentManufacturerPartNumber:
                    return ManufacturerPartNumber;
                case FieldNames.FixtureComponentSupplierName:
                    return SupplierName;
                case FieldNames.FixtureComponentSupplierPartNumber:
                    return SupplierPartNumber;
                case FieldNames.FixtureComponentSupplierCostPrice:
                    return SupplierCostPrice;
                case FieldNames.FixtureComponentSupplierDiscount:
                    return SupplierDiscount;
                case FieldNames.FixtureComponentSupplierLeadTime:
                    return SupplierLeadTime;
                case FieldNames.FixtureComponentMinPurchaseQty:
                    return MinPurchaseQty;
                case FieldNames.FixtureComponentWeightLimit:
                    return WeightLimit;
                case FieldNames.FixtureComponentWeight:
                    return Weight;
                case FieldNames.FixtureComponentVolume:
                    return Volume;
                case FieldNames.FixtureComponentDiameter:
                    return Diameter;
                case FieldNames.FixtureComponentCapacity:
                    return Capacity;
                case FieldNames.FixtureComponentComponentType:
                    return ComponentType;
                case FieldNames.FixtureComponentIsMerchandisedTopDown:
                    return IsMerchandisedTopDown;
                default:
                    return null;
            }
        }

        #endregion

        #endregion



    }
}
