﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Pogfx.Schema;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;

namespace Galleria.Ccm.Dal.Pogfx.Implementation.Items
{
    public class FixtureImageDtoItem : FixtureImageDto, IChildDtoItem<FixtureImageDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new FixtureImageDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public FixtureImageDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureImageId));
            FixturePackageId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureImageFixturePackageId));
            FileName = (String)item.GetItemPropertyValue(FieldNames.FixtureImageFileName);
            Description = (String)item.GetItemPropertyValue(FieldNames.FixtureImageDescription);
            ImageData = (Byte[])item.GetItemPropertyValue(FieldNames.FixtureImageImageData);
        }

        /// <summary>
        /// Creates a new FixtureImageDtoItem and populates its properties with values taken from a 
        /// FixtureImageDto.
        /// </summary>
        public FixtureImageDtoItem(FixtureImageDto dto)
        {
            Id = dto.Id;
            FixturePackageId = dto.FixturePackageId;
            FileName = dto.FileName;
            Description = dto.Description;
            ImageData = dto.ImageData;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<FixtureImageDto>.Id
        {
            get { return Id; }
            set { Id = (Int32)value; }
        }

        Boolean IChildDtoItem<FixtureImageDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<FixtureImageDto>.ParentId
        {
            get { return (Int32)FixturePackageId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public FixtureImageDto CopyDto()
        {
            return new FixtureImageDto()
            {
                Id = this.Id,
                FixturePackageId = this.FixturePackageId,
                FileName = this.FileName,
                Description = this.Description,
                ImageData = this.ImageData
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.FixtureImageId:
                    return Id;
                case FieldNames.FixtureImageFixturePackageId:
                    return FixturePackageId;
                case FieldNames.FixtureImageFileName:
                    return FileName;
                case FieldNames.FixtureImageDescription:
                    return Description;
                case FieldNames.FixtureImageImageData:
                    return ImageData;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
