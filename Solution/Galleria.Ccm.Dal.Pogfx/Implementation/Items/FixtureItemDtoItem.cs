﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
// V8-27558 : L.Ineson
//  Removed IsBay
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Pogfx.Schema;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;

namespace Galleria.Ccm.Dal.Pogfx.Implementation.Items
{
    public class FixtureItemDtoItem : FixtureItemDto, IChildDtoItem<FixtureItemDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new FixtureItemDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public FixtureItemDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureItemId));
            FixturePackageId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureItemFixturePackageId));
            FixtureId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureItemFixtureId));
            X = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureItemX));
            Y = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureItemY));
            Z = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureItemZ));
            Slope = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureItemSlope));
            Angle = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureItemAngle));
            Roll = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureItemRoll));
        }

        /// <summary>
        /// Creates a new FixtureItemDtoItem and populates its properties with values taken from a 
        /// FixtureItemDto.
        /// </summary>
        public FixtureItemDtoItem(FixtureItemDto dto)
        {
            Id = dto.Id;
            FixturePackageId = dto.FixturePackageId;
            FixtureId = dto.FixtureId;
            X = dto.X;
            Y = dto.Y;
            Z = dto.Z;
            Slope = dto.Slope;
            Angle = dto.Angle;
            Roll = dto.Roll;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<FixtureItemDto>.Id
        {
            get { return Id; }
            set { Id = (Int32)value; }
        }

        Boolean IChildDtoItem<FixtureItemDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<FixtureItemDto>.ParentId
        {
            get { return (Int32)FixturePackageId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public FixtureItemDto CopyDto()
        {
            return new FixtureItemDto()
            {
                Id = this.Id,
                FixturePackageId = this.FixturePackageId,
                FixtureId = this.FixtureId,
                X = this.X,
                Y = this.Y,
                Z = this.Z,
                Slope = this.Slope,
                Angle = this.Angle,
                Roll = this.Roll,
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.FixtureItemId:
                    return Id;
                case FieldNames.FixtureItemFixturePackageId:
                    return FixturePackageId;
                case FieldNames.FixtureItemFixtureId:
                    return FixtureId;
                case FieldNames.FixtureItemX:
                    return X;
                case FieldNames.FixtureItemY:
                    return Y;
                case FieldNames.FixtureItemZ:
                    return Z;
                case FieldNames.FixtureItemSlope:
                    return Slope;
                case FieldNames.FixtureItemAngle:
                    return Angle;
                case FieldNames.FixtureItemRoll:
                    return Roll;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
