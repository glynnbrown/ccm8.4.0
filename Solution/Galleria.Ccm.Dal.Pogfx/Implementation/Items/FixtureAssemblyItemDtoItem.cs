﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Pogfx.Schema;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;

namespace Galleria.Ccm.Dal.Pogfx.Implementation.Items
{
    public class FixtureAssemblyItemDtoItem : FixtureAssemblyItemDto, IChildDtoItem<FixtureAssemblyItemDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new FixtureAssemblyItemDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public FixtureAssemblyItemDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureAssemblyItemId));
            FixtureId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureAssemblyItemFixtureId));
            FixtureAssemblyId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureAssemblyItemFixtureAssemblyId));
            X = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAssemblyItemX));
            Y = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAssemblyItemY));
            Z = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAssemblyItemZ));
            Slope = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAssemblyItemSlope));
            Angle = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAssemblyItemAngle));
            Roll = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAssemblyItemRoll));
        }

        /// <summary>
        /// Creates a new FixtureAssemblyItemDtoItem and populates its properties with values taken from a 
        /// FixtureAssemblyItemDto.
        /// </summary>
        public FixtureAssemblyItemDtoItem(FixtureAssemblyItemDto dto)
        {
            Id = dto.Id;
            FixtureId = dto.FixtureId;
            FixtureAssemblyId = dto.FixtureAssemblyId;
            X = dto.X;
            Y = dto.Y;
            Z = dto.Z;
            Slope = dto.Slope;
            Angle = dto.Angle;
            Roll = dto.Roll;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<FixtureAssemblyItemDto>.Id
        {
            get { return Id; }
            set { Id = (Int32)value; }
        }

        Boolean IChildDtoItem<FixtureAssemblyItemDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<FixtureAssemblyItemDto>.ParentId
        {
            get { return (Int32)FixtureId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public FixtureAssemblyItemDto CopyDto()
        {
            return new FixtureAssemblyItemDto()
            {
                Id = this.Id,
                FixtureId = this.FixtureId,
                FixtureAssemblyId = this.FixtureAssemblyId,
                X = this.X,
                Y = this.Y,
                Z = this.Z,
                Slope = this.Slope,
                Angle = this.Angle,
                Roll = this.Roll
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.FixtureAssemblyItemId:
                    return Id;
                case FieldNames.FixtureAssemblyItemFixtureId:
                    return FixtureId;
                case FieldNames.FixtureAssemblyItemFixtureAssemblyId:
                    return FixtureAssemblyId;
                case FieldNames.FixtureAssemblyItemX:
                    return X;
                case FieldNames.FixtureAssemblyItemY:
                    return Y;
                case FieldNames.FixtureAssemblyItemZ:
                    return Z;
                case FieldNames.FixtureAssemblyItemSlope:
                    return Slope;
                case FieldNames.FixtureAssemblyItemAngle:
                    return Angle;
                case FieldNames.FixtureAssemblyItemRoll:
                    return Roll;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
