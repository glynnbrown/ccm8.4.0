﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM CCM830)
// CCM-32524 : L.Ineson
//		Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Pogfx.Schema;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;

namespace Galleria.Ccm.Dal.Pogfx.Implementation.Items
{
    public class FixtureAnnotationDtoItem : FixtureAnnotationDto, IChildDtoItem<FixtureAnnotationDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new FixtureAnnotationDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public FixtureAnnotationDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureAnnotationId));
            FixturePackageId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureAnnotationFixturePackageId));
            FixtureItemId = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.FixtureAnnotationFixtureItemId));
            FixtureAssemblyItemId = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.FixtureAnnotationFixtureAssemblyItemId));
            FixtureComponentItemId = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.FixtureAnnotationFixtureComponentItemId));
            FixtureAssemblyComponentId = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.FixtureAnnotationFixtureAssemblyComponentId));
            FixtureSubComponentId = GalleriaBinaryFileItemHelper.GetInt32Nullable(item.GetItemPropertyValue(FieldNames.FixtureAnnotationFixtureSubComponentId));
            AnnotationType = GalleriaBinaryFileItemHelper.GetByte(item.GetItemPropertyValue(FieldNames.FixtureAnnotationAnnotationType));
            Text = (String)item.GetItemPropertyValue(FieldNames.FixtureAnnotationText);
            X = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAnnotationX));
            Y = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAnnotationY));
            Z = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAnnotationZ));
            Height = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAnnotationHeight));
            Width = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAnnotationWidth));
            Depth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAnnotationDepth));
            BorderColour = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureAnnotationBorderColour));
            BorderThickness = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAnnotationBorderThickness));
            BackgroundColour = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureAnnotationBackgroundColour));
            FontColour = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureAnnotationFontColour));
            FontSize = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAnnotationFontSize));
            FontName = (String)item.GetItemPropertyValue(FieldNames.FixtureAnnotationFontName);
            CanReduceFontToFit = GalleriaBinaryFileItemHelper.GetBoolean(item.GetItemPropertyValue(FieldNames.FixtureAnnotationCanReduceFontToFit));
        }

        /// <summary>
        /// Creates a new FixtureAnnotationDtoItem and populates its properties with values taken from a 
        /// FixtureAnnotationDto.
        /// </summary>
        public FixtureAnnotationDtoItem(FixtureAnnotationDto dto)
        {
            Id = dto.Id;
            FixturePackageId = dto.FixturePackageId;
            FixtureItemId = dto.FixtureItemId;
            FixtureAssemblyItemId = dto.FixtureAssemblyItemId;
            FixtureComponentItemId = dto.FixtureComponentItemId;
            FixtureAssemblyComponentId = dto.FixtureAssemblyComponentId;
            FixtureSubComponentId = dto.FixtureSubComponentId;
            AnnotationType = dto.AnnotationType;
            Text = dto.Text;
            X = dto.X;
            Y = dto.Y;
            Z = dto.Z;
            Height = dto.Height;
            Width = dto.Width;
            Depth = dto.Depth;
            BorderColour = dto.BorderColour;
            BorderThickness = dto.BorderThickness;
            BackgroundColour = dto.BackgroundColour;
            FontColour = dto.FontColour;
            FontSize = dto.FontSize;
            FontName = dto.FontName;
            CanReduceFontToFit = dto.CanReduceFontToFit;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<FixtureAnnotationDto>.Id
        {
            get { return Id; }
            set { Id = (Int32)value; }
        }

        Boolean IChildDtoItem<FixtureAnnotationDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<FixtureAnnotationDto>.ParentId
        {
            get { return (Int32)FixturePackageId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public FixtureAnnotationDto CopyDto()
        {
            return new FixtureAnnotationDto()
            {
                Id = this.Id,
                FixturePackageId = this.FixturePackageId,
                FixtureItemId = this.FixtureItemId,
                FixtureAssemblyItemId = this.FixtureAssemblyItemId,
                FixtureComponentItemId = this.FixtureComponentItemId,
                FixtureAssemblyComponentId = this.FixtureAssemblyComponentId,
                FixtureSubComponentId = this.FixtureSubComponentId,
                AnnotationType = this.AnnotationType,
                Text = this.Text,
                X = this.X,
                Y = this.Y,
                Z = this.Z,
                Height = this.Height,
                Width = this.Width,
                Depth = this.Depth,
                BorderColour = this.BorderColour,
                BorderThickness = this.BorderThickness,
                BackgroundColour = this.BackgroundColour,
                FontColour = this.FontColour,
                FontSize = this.FontSize,
                FontName = this.FontName,
                CanReduceFontToFit = this.CanReduceFontToFit,
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.FixtureAnnotationId: return Id;
                case FieldNames.FixtureAnnotationFixturePackageId: return FixturePackageId;
                case FieldNames.FixtureAnnotationFixtureItemId:  return this.FixtureItemId;
                case FieldNames.FixtureAnnotationFixtureAssemblyItemId:  return this.FixtureAssemblyItemId;
                case FieldNames.FixtureAnnotationFixtureComponentItemId: return this.FixtureComponentItemId;
                case FieldNames.FixtureAnnotationFixtureAssemblyComponentId: return this.FixtureAssemblyComponentId;
                case FieldNames.FixtureAnnotationFixtureSubComponentId:  return this.FixtureSubComponentId;
                case FieldNames.FixtureAnnotationAnnotationType: return this.AnnotationType;
                case FieldNames.FixtureAnnotationText:return Text;
                case FieldNames.FixtureAnnotationHeight: return Height;
                case FieldNames.FixtureAnnotationWidth: return Width;
                case FieldNames.FixtureAnnotationDepth:return Depth;
                case FieldNames.FixtureAnnotationX: return this.X;
                case FieldNames.FixtureAnnotationY: return this.Y;
                case FieldNames.FixtureAnnotationZ: return this.Z;
                case FieldNames.FixtureAnnotationBorderColour: return this.BorderColour;
                case FieldNames.FixtureAnnotationBorderThickness: return this.BorderThickness;
                case FieldNames.FixtureAnnotationBackgroundColour: return this.BackgroundColour;
                case FieldNames.FixtureAnnotationFontColour: return this.FontColour;
                case FieldNames.FixtureAnnotationFontSize: return this.FontSize;
                case FieldNames.FixtureAnnotationFontName: return this.FontName;
                case FieldNames.FixtureAnnotationCanReduceFontToFit: return this.CanReduceFontToFit;

                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
