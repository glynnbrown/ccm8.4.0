﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal.Gbf;
using Galleria.Ccm.Dal.Pogfx.Resources;

namespace Galleria.Ccm.Dal.Pogfx.Implementation
{
    public class FixtureAssemblyDal : DalBase<DalCache>, IFixtureAssemblyDal
    {
        #region IFixtureAssemblyDal Members

        /// <summary>
        /// Fetches all items with a given FixturePackageId
        /// </summary>
        public IEnumerable<FixtureAssemblyDto> FetchByFixturePackageId(Object fixturePackageId)
        {
            return DalCache.FixtureAssemblies.FetchByParentId((Int32)fixturePackageId);
        }

        /// <summary>
        /// Inserts an item, assigning it the next available integer ID.
        /// </summary>
        public void Insert(FixtureAssemblyDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = (Int32)DalCache.FixtureAssemblies.Insert(dto);
        }

        /// <summary>
        /// Finds and updates a fixture assembly by ID, if the fixture exists.
        /// </summary>
        public void Update(FixtureAssemblyDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.FixtureAssemblies.Update(dto);
        }

        /// <summary>
        /// Finds and deletes a fixture assembly by ID, if the fixture exists.
        /// </summary>
        public void DeleteById(Int32 id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.FixtureAssemblies.Delete(id);
        }

        #endregion
    }
}
