﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal.Gbf;
using Galleria.Ccm.Dal.Pogfx.Resources;

namespace Galleria.Ccm.Dal.Pogfx.Implementation
{
    /// <summary>
    /// Implementation of IFixtureItemDal
    /// </summary>
    public class FixtureItemDal : DalBase<DalCache>, IFixtureItemDal
    {
        /// <summary>
        /// Fetches all items with the given FixturePackageId
        /// </summary>
        /// <param name="fixturePackageId"></param>
        /// <returns></returns>
        public IEnumerable<FixtureItemDto> FetchByFixturePackageId(Object fixturePackageId)
        {
            return DalCache.FixtureItems.FetchByParentId((Int32)fixturePackageId);
        }

        /// <summary>
        /// Inserts an item, assigning it the next available integer ID.
        /// </summary>
        public void Insert(FixtureItemDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = (Int32)DalCache.FixtureItems.Insert(dto);
        }

        /// <summary>
        /// Finds and updates an item by ID, if it exists.
        /// </summary>
        public void Update(FixtureItemDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.FixtureItems.Update(dto);
        }

        /// <summary>
        /// Deletes the item with the given id.
        /// </summary>
        /// <param name="id"></param>
        public void DeleteById(Int32 id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.FixtureItems.Delete(id);
        }
    }
}
