﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM CCM830)
// V8-32524 : L.Ineson
// Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal.Gbf;
using Galleria.Ccm.Dal.Pogfx.Schema;
using Galleria.Framework.IO;

namespace Galleria.Ccm.Dal.Pogfx.Scripts
{
    public class Script0004_0000 : IScript
    {
        #region Upgrade
        public void Upgrade(Framework.IO.GalleriaBinaryFile file)
        {
            #region Add Sections
            if (!file.SectionExists((UInt16)SectionType.FixtureAnnotations))
            {
                file.CreateSection((UInt16)SectionType.FixtureAnnotations);
            }
            #endregion


            #region Add Item Properties

            #region FixtureAnnotation

            // Id
            file.AddItemPropertyIfMissing(
                    SectionType.FixtureAnnotations,
                    FieldNames.FixtureAnnotationId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);

            // FixturePackageId
            file.AddItemPropertyIfMissing(
                SectionType.FixtureAnnotations,
                FieldNames.FixtureAnnotationFixturePackageId,
                GalleriaBinaryFile.ItemPropertyType.Int32,
                null);

            // FixtureItemId
            file.AddItemPropertyIfMissing(
                SectionType.FixtureAnnotations,
                FieldNames.FixtureAnnotationFixtureItemId,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                null);

            // FixtureAssemblyId
            file.AddItemPropertyIfMissing(
                SectionType.FixtureAnnotations,
                FieldNames.FixtureAnnotationFixtureAssemblyItemId,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                null);

            // FixtureComponentId
            file.AddItemPropertyIfMissing(
                SectionType.FixtureAnnotations,
                FieldNames.FixtureAnnotationFixtureComponentItemId,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                null);

            // FixtureAssemblyComponentId
            file.AddItemPropertyIfMissing(
                SectionType.FixtureAnnotations,
                FieldNames.FixtureAnnotationFixtureAssemblyComponentId,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                null);


            // FixtureSubComponentId
            file.AddItemPropertyIfMissing(
                SectionType.FixtureAnnotations,
                FieldNames.FixtureAnnotationFixtureSubComponentId,
                GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                null);


            // AnnotationType
            file.AddItemPropertyIfMissing(
                SectionType.FixtureAnnotations,
                FieldNames.FixtureAnnotationAnnotationType,
                GalleriaBinaryFile.ItemPropertyType.Byte,
                null);

            // Text
            file.AddItemPropertyIfMissing(
                SectionType.FixtureAnnotations,
                FieldNames.FixtureAnnotationText,
                GalleriaBinaryFile.ItemPropertyType.String,
                null);

            // X
            file.AddItemPropertyIfMissing(
                SectionType.FixtureAnnotations,
                FieldNames.FixtureAnnotationX,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            // Y
            file.AddItemPropertyIfMissing(
                SectionType.FixtureAnnotations,
                FieldNames.FixtureAnnotationY,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            // Z
            file.AddItemPropertyIfMissing(
                SectionType.FixtureAnnotations,
                FieldNames.FixtureAnnotationZ,
                GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                null);

            // Height
            file.AddItemPropertyIfMissing(
                SectionType.FixtureAnnotations,
                FieldNames.FixtureAnnotationHeight,
                GalleriaBinaryFile.ItemPropertyType.Single,
                null);

            // Width
            file.AddItemPropertyIfMissing(
                SectionType.FixtureAnnotations,
                FieldNames.FixtureAnnotationWidth,
                GalleriaBinaryFile.ItemPropertyType.Single,
                null);

            // Depth
            file.AddItemPropertyIfMissing(
                SectionType.FixtureAnnotations,
                FieldNames.FixtureAnnotationDepth,
                GalleriaBinaryFile.ItemPropertyType.Single,
                null);

            // BorderColour
            file.AddItemPropertyIfMissing(
                SectionType.FixtureAnnotations,
                FieldNames.FixtureAnnotationBorderColour,
                GalleriaBinaryFile.ItemPropertyType.Int32,
                null);

            // BorderThickness
            file.AddItemPropertyIfMissing(
                SectionType.FixtureAnnotations,
                FieldNames.FixtureAnnotationBorderThickness,
                GalleriaBinaryFile.ItemPropertyType.Single,
                null);

            // BackgroundColour
            file.AddItemPropertyIfMissing(
                SectionType.FixtureAnnotations,
                FieldNames.FixtureAnnotationBackgroundColour,
                GalleriaBinaryFile.ItemPropertyType.Int32,
                null);

            // FontColour
            file.AddItemPropertyIfMissing(
                SectionType.FixtureAnnotations,
                FieldNames.FixtureAnnotationFontColour,
                GalleriaBinaryFile.ItemPropertyType.Int32,
                null);

            // FontSize
            file.AddItemPropertyIfMissing(
                SectionType.FixtureAnnotations,
                FieldNames.FixtureAnnotationFontSize,
                GalleriaBinaryFile.ItemPropertyType.Single,
                null);

            // FontName
            file.AddItemPropertyIfMissing(
                SectionType.FixtureAnnotations,
                FieldNames.FixtureAnnotationFontName,
                GalleriaBinaryFile.ItemPropertyType.String,
                null);

            // CanReduceFontToFit
            file.AddItemPropertyIfMissing(
                SectionType.FixtureAnnotations,
                FieldNames.FixtureAnnotationCanReduceFontToFit,
                GalleriaBinaryFile.ItemPropertyType.Boolean,
                null);

            #endregion

            #endregion
        }
        #endregion

        #region Downgrade
        public void Downgrade(Framework.IO.GalleriaBinaryFile file)
        {
            #region Remove Item Properties

            #region FixtureAnnotation

            // Id
            file.RemoveItemPropertyIfPresent(SectionType.FixtureAnnotations, FieldNames.FixtureAnnotationId);

            // FixturePackageId
            file.RemoveItemPropertyIfPresent(SectionType.FixtureAnnotations, FieldNames.FixtureAnnotationFixturePackageId);

            // FixtureItemId
            file.RemoveItemPropertyIfPresent(SectionType.FixtureAnnotations, FieldNames.FixtureAnnotationFixtureItemId);

            // FixtureAssemblyId
            file.RemoveItemPropertyIfPresent(SectionType.FixtureAnnotations, FieldNames.FixtureAnnotationFixtureAssemblyItemId);

            // FixtureComponentId
            file.RemoveItemPropertyIfPresent(SectionType.FixtureAnnotations, FieldNames.FixtureAnnotationFixtureComponentItemId);

            // FixtureAssemblyComponentId
            file.RemoveItemPropertyIfPresent(SectionType.FixtureAnnotations, FieldNames.FixtureAnnotationFixtureAssemblyComponentId);

            // FixtureSubComponentId
            file.RemoveItemPropertyIfPresent(SectionType.FixtureAnnotations, FieldNames.FixtureAnnotationFixtureSubComponentId);

            // AnnotationType
            file.RemoveItemPropertyIfPresent(SectionType.FixtureAnnotations, FieldNames.FixtureAnnotationAnnotationType);

            // Text
            file.RemoveItemPropertyIfPresent(SectionType.FixtureAnnotations, FieldNames.FixtureAnnotationText);

            // X
            file.RemoveItemPropertyIfPresent(SectionType.FixtureAnnotations, FieldNames.FixtureAnnotationX);

            // Y
            file.RemoveItemPropertyIfPresent(SectionType.FixtureAnnotations, FieldNames.FixtureAnnotationY);

            // Z
            file.RemoveItemPropertyIfPresent(SectionType.FixtureAnnotations, FieldNames.FixtureAnnotationZ);

            // Height
            file.RemoveItemPropertyIfPresent(SectionType.FixtureAnnotations, FieldNames.FixtureAnnotationHeight);

            // Width
            file.RemoveItemPropertyIfPresent(SectionType.FixtureAnnotations, FieldNames.FixtureAnnotationWidth);

            // Depth
            file.RemoveItemPropertyIfPresent(SectionType.FixtureAnnotations, FieldNames.FixtureAnnotationDepth);

            // BorderColour
            file.RemoveItemPropertyIfPresent(SectionType.FixtureAnnotations, FieldNames.FixtureAnnotationBorderColour);

            // BorderThickness
            file.RemoveItemPropertyIfPresent(SectionType.FixtureAnnotations, FieldNames.FixtureAnnotationBorderThickness);

            // BackgroundColour
            file.RemoveItemPropertyIfPresent(SectionType.FixtureAnnotations, FieldNames.FixtureAnnotationBackgroundColour);

            // FontColour
            file.RemoveItemPropertyIfPresent(SectionType.FixtureAnnotations, FieldNames.FixtureAnnotationFontColour);

            // FontSize
            file.RemoveItemPropertyIfPresent(SectionType.FixtureAnnotations, FieldNames.FixtureAnnotationFontSize);

            // FontName
            file.RemoveItemPropertyIfPresent(SectionType.FixtureAnnotations, FieldNames.FixtureAnnotationFontName);

            // CanReduceFontToFit
            file.RemoveItemPropertyIfPresent(SectionType.FixtureAnnotations, FieldNames.FixtureAnnotationCanReduceFontToFit);

            #endregion


            #endregion

            #region Remove Sections
            file.RemoveSectionIfPresent(SectionType.FixtureAnnotations);
            #endregion
        }
        #endregion

    }
}
