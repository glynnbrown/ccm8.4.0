﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM801
//V8-29203 L:Ineson
//  Initial version
#endregion
#region Version History: (CCM CCM802)
// V8-29023 : M.Pettit
//  Added Merchandisable Depth with default
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal.Gbf;
using Galleria.Ccm.Dal.Pogfx.Schema;
using Galleria.Framework.IO;

namespace Galleria.Ccm.Dal.Pogfx.Scripts
{
    public class Script0003_0000 : IScript
    {
        #region Upgrade
        public void Upgrade(Framework.IO.GalleriaBinaryFile file)
        {
            #region Add Item Properties

            #region FixtureSubComponent

            // MerchandisableDepth
            file.AddItemPropertyIfMissing(
                SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentMerchandisableDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single, 0F);

            file.AddItemPropertyIfMissing(
                SectionType.FixtureSubComponents,
                FieldNames.FixtureSubComponentIsProductSqueezeAllowed,
                GalleriaBinaryFile.ItemPropertyType.Boolean, true);

            #endregion

            #endregion
        }
        #endregion

        #region Downgrade
        public void Downgrade(Framework.IO.GalleriaBinaryFile file)
        {
            #region Remove Item Properties

            #region PlanogramSubComponent

            file.RemoveItemPropertyIfPresent(
                SectionType.FixtureSubComponents,
                FieldNames.FixtureSubComponentMerchandisableDepth);

            file.RemoveItemPropertyIfPresent(
                SectionType.FixtureSubComponents,
                FieldNames.FixtureSubComponentIsProductSqueezeAllowed);

            #endregion


            #endregion
        }
        #endregion

    }

    #region InternalExtensions

    internal static class Extensions
    {
        /// <summary>
        ///     Adds the given <paramref name="sectionType" /> to the <paramref name="file" /> if it does not exist.
        /// </summary>
        /// <param name="file">The <see cref="GalleriaBinaryFile" /> to add the <see cref="SectionType" /> if it is missing.</param>
        /// <param name="sectionType">The <see cref="SectionType" /> to add to the <see cref="GalleriaBinaryFile" />.</param>
        /// <param name="encrypted">Whether to enable Aes 256 encryption on the <see cref="SectionType" />.</param>
        internal static void AddSectionIfMissing(this GalleriaBinaryFile file, SectionType sectionType,
            Boolean encrypted = false)
        {
            UInt16 type = (UInt16)sectionType;
            if (!file.SectionExists(type)) file.CreateSection(type);
            if (encrypted) file.EnableAes256Encryption(type);
        }

        /// <summary>
        ///     Adds a given <paramref name="fieldName" /> to the <see cref="SectionType" /> in the <paramref name="file" />.
        /// </summary>
        /// <param name="file">The <see cref="GalleriaBinaryFile" /> that will be updated.</param>
        /// <param name="sectionType">The <see cref="SectionType" /> that the item property belongs to.</param>
        /// <param name="fieldName">The name of the item property.</param>
        /// <param name="itemPropertyType">The type of data held in the item property.</param>
        /// <param name="defaultValue">[OPTIONAL] The default value for the item property when it needs to be added.</param>
        internal static void AddItemPropertyIfMissing(this GalleriaBinaryFile file, SectionType sectionType,
            String fieldName,
            GalleriaBinaryFile.ItemPropertyType itemPropertyType, Object defaultValue = null)
        {
            UInt16 type = (UInt16)sectionType;
            if (!file.ItemPropertyExists(type, fieldName))
                file.AddItemProperty(type, fieldName, itemPropertyType, defaultValue);
        }

        /// <summary>
        ///     Adds the given <paramref name="sectionType" /> to the <paramref name="file" /> if it does not exist.
        /// </summary>
        /// <param name="file">The <see cref="GalleriaBinaryFile" /> to add the <see cref="SectionType" /> if it is missing.</param>
        /// <param name="sectionType">The <see cref="SectionType" /> to add to the <see cref="GalleriaBinaryFile" />.</param>
        internal static void RemoveSectionIfPresent(this GalleriaBinaryFile file, SectionType sectionType)
        {
            UInt16 type = (UInt16)sectionType;
            if (file.SectionExists(type)) file.RemoveSection(type);
        }

        /// <summary>
        ///     Removes a given <paramref name="fieldName" /> to the <see cref="SectionType" /> in the <paramref name="file" />.
        /// </summary>
        /// <param name="file">The <see cref="GalleriaBinaryFile" /> that will be updated.</param>
        /// <param name="sectionType">The <see cref="SectionType" /> that the item property belongs to.</param>
        /// <param name="fieldName">The name of the item property.</param>
        internal static void RemoveItemPropertyIfPresent(this GalleriaBinaryFile file, SectionType sectionType,
            String fieldName)
        {
            UInt16 type = (UInt16)sectionType;
            if (!file.ItemPropertyExists(type, fieldName))
                file.RemoveItemProperty(type, fieldName);
        }
    }

    #endregion
}
