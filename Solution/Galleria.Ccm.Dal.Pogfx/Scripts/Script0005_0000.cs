﻿#region Header Information
// Copyright © Galleria RTS Ltd 2016

#region Version History: (CCM CCM831)
// V8-32524 : L.Ineson
// Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal.Gbf;
using Galleria.Ccm.Dal.Pogfx.Schema;
using Galleria.Framework.IO;

namespace Galleria.Ccm.Dal.Pogfx.Scripts
{
    public class Script0005_0000 : IScript
    {

        #region Upgrade
        public void Upgrade(Framework.IO.GalleriaBinaryFile file)
        {
            file.AddItemPropertyIfMissing(
                    SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentDividerObstructionFillColour,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                   -65536);

            file.AddItemPropertyIfMissing(
              SectionType.FixtureSubComponents,
              FieldNames.FixtureSubComponentDividerObstructionFillPattern,
              GalleriaBinaryFile.ItemPropertyType.Byte,
              (Byte)0);

        }
        #endregion

        #region Downgrade
        public void Downgrade(Framework.IO.GalleriaBinaryFile file)
        {
            file.RemoveItemPropertyIfPresent(
                SectionType.FixtureSubComponents,
                FieldNames.FixtureSubComponentDividerObstructionFillColour);

            file.RemoveItemPropertyIfPresent(
               SectionType.FixtureSubComponents,
               FieldNames.FixtureSubComponentDividerObstructionFillPattern);
        }
        #endregion

    }
}
