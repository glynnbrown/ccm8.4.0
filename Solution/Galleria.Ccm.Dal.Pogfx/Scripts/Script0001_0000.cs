﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
// V8-24658 : K.Pickup
//      Refactored GalleriaBinaryFile.
// V8-25873 : A.Probyn
//  Added Custom Attribute 1 - 5
// V8-26091 : L.Ineson
//  Added more face thickness properties to subcomponent.
// V8-27558 : L.Ineson
//  Removed IsBay
#endregion
#region Version History: (CCM CCM802)
// V8-29023 : M.Pettit
//  Removed Merchandisable Depth
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.Pogfx.Schema;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.IO;

namespace Galleria.Ccm.Dal.Pogfx.Scripts
{
    public class Script0001_0000 : IScript
    {
        #region Upgrade
        /// <summary>
        /// Performs the upgrade of a fixture file
        /// </summary>
        public void Upgrade(GalleriaBinaryFile file)
        {
            #region Add Sections

            // Note: Order is important here.  The sections will be written to the file in the order in which they are
            // added.  As a general rule of thumb, therefore, the sections should be added in descending order by
            // anticipated size.  This is because if a section has to be expanded to accomodate new data, all sections
            // following it will have to be moved down, and moving significant number of bytes around in a file takes
            // time.  In one test involving around 5000 images (a reasonable number of images for a Maxima planogram)
            // of 31 KB each (no idea what size the images will be in real life) moving the images section up from
            // last in the file reduced saving time for a small change (adding a new fixture item) from around 2.5 
            // seconds to 167 milliseconds.

            // The Schema Version section is very small, but we also don't anticipate it ever needing to change in 
            // size once written out the first time, so it might as well top the file.
            if (!file.SectionExists((UInt16)SectionType.SchemaVersion))
            {
                file.CreateSection((UInt16)SectionType.SchemaVersion);
            }
            if (!file.SectionExists((UInt16)SectionType.FixtureImages))
            {
                file.CreateSection((UInt16)SectionType.FixtureImages, GalleriaBinaryFile.ItemPropertyType.Int32);
            }
            file.EnableAes256Encryption((UInt16)SectionType.FixtureImages);
            if (!file.SectionExists((UInt16)SectionType.FixtureSubComponents))
            {
                file.CreateSection((UInt16)SectionType.FixtureSubComponents);
            }
            if (!file.SectionExists((UInt16)SectionType.FixtureComponents))
            {
                file.CreateSection((UInt16)SectionType.FixtureComponents);
            }
            if (!file.SectionExists((UInt16)SectionType.FixtureAssemblyItems))
            {
                file.CreateSection((UInt16)SectionType.FixtureAssemblyItems);
            }
            if (!file.SectionExists((UInt16)SectionType.Fixtures))
            {
                file.CreateSection((UInt16)SectionType.Fixtures);
            }
            if (!file.SectionExists((UInt16)SectionType.FixtureItems))
            {
                file.CreateSection((UInt16)SectionType.FixtureItems);
            }
            if (!file.SectionExists((UInt16)SectionType.FixtureComponentItems))
            {
                file.CreateSection((UInt16)SectionType.FixtureComponentItems);
            }
            if (!file.SectionExists((UInt16)SectionType.FixturePackage))
            {
                file.CreateSection((UInt16)SectionType.FixturePackage);
            }
            if (!file.SectionExists((UInt16)SectionType.FixtureAssemblies))
            {
                file.CreateSection((UInt16)SectionType.FixtureAssemblies);
            }
            if (!file.SectionExists((UInt16)SectionType.FixtureAssemblyComponents))
            {
                file.CreateSection((UInt16)SectionType.FixtureAssemblyComponents);
            }

            #endregion

            #region Add Item Properties

            #region Schema Version

            // MajorVersion
            if (!file.ItemPropertyExists((UInt16)SectionType.SchemaVersion, FieldNames.SchemaVersionMajorVersion))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.SchemaVersion,
                    FieldNames.SchemaVersionMajorVersion,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // MinorVersion
            if (!file.ItemPropertyExists((UInt16)SectionType.SchemaVersion, FieldNames.SchemaVersionMinorVersion))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.SchemaVersion,
                    FieldNames.SchemaVersionMinorVersion,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            #endregion

            #region FixturePackage

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // Name
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Description
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageDescription))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageDescription,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            //Height
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //Width
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }


            //Depth
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            //FixtureCount
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageFixtureCount))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageFixtureCount,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            //AssemblyCount
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageAssemblyCount))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageAssemblyCount,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            //ComponentCount
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageComponentCount))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageComponentCount,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            //LengthUnitsOfMeasure
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageLengthUnitsOfMeasure))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageLengthUnitsOfMeasure,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            //AreaUnitsOfMeasure
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageAreaUnitsOfMeasure))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageAreaUnitsOfMeasure,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            //VolumeUnitsOfMeasure
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageVolumeUnitsOfMeasure))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageVolumeUnitsOfMeasure,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            //WeightUnitsOfMeasure
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageWeightUnitsOfMeasure))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageWeightUnitsOfMeasure,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            //AngleUnitsOfMeasure
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageAngleUnitsOfMeasure))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageAngleUnitsOfMeasure,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            //CurrencyUnitsOfMeasure
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageCurrencyUnitsOfMeasure))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageCurrencyUnitsOfMeasure,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            //ThumbnailImageData
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageThumbnailImageData))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageThumbnailImageData,
                    GalleriaBinaryFile.ItemPropertyType.ByteArray,
                    null);
            }

            // DateCreated
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageDateCreated))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageDateCreated,
                    GalleriaBinaryFile.ItemPropertyType.DateTime,
                    null);
            }

            // DateLastModified
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageDateLastModified))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageDateLastModified,
                    GalleriaBinaryFile.ItemPropertyType.DateTime,
                    null);
            }

            // CreatedBy
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageCreatedBy))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageCreatedBy,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // LastModifiedBy
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageLastModifiedBy))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageLastModifiedBy,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Custom Attribute 1
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageCustomAttribute1))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageCustomAttribute1,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Custom Attribute 2
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageCustomAttribute2))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageCustomAttribute2,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Custom Attribute 3
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageCustomAttribute3))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageCustomAttribute3,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Custom Attribute 4
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageCustomAttribute4))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageCustomAttribute4,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Custom Attribute 5
            if (!file.ItemPropertyExists((UInt16)SectionType.FixturePackage, FieldNames.FixturePackageCustomAttribute5))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixturePackage,
                    FieldNames.FixturePackageCustomAttribute5,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            #endregion

            #region FixtureItems

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureItems, FieldNames.FixtureItemId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureItems,
                    FieldNames.FixtureItemId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FixturePackageId
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureItems, FieldNames.FixtureItemFixturePackageId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureItems,
                    FieldNames.FixtureItemFixturePackageId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FixtureId
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureItems, FieldNames.FixtureItemFixtureId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureItems,
                    FieldNames.FixtureItemFixtureId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // X
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureItems, FieldNames.FixtureItemX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureItems,
                    FieldNames.FixtureItemX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Y
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureItems, FieldNames.FixtureItemY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureItems,
                    FieldNames.FixtureItemY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Z
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureItems, FieldNames.FixtureItemZ))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureItems,
                    FieldNames.FixtureItemZ,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Slope
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureItems, FieldNames.FixtureItemSlope))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureItems,
                    FieldNames.FixtureItemSlope,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Angle
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureItems, FieldNames.FixtureItemAngle))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureItems,
                    FieldNames.FixtureItemAngle,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Roll
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureItems, FieldNames.FixtureItemRoll))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureItems,
                    FieldNames.FixtureItemRoll,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            #endregion

            #region Fixtures

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.Fixtures, FieldNames.FixtureId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Fixtures,
                    FieldNames.FixtureId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FixturePackageId
            if (!file.ItemPropertyExists((UInt16)SectionType.Fixtures, FieldNames.FixtureFixturePackageId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Fixtures,
                    FieldNames.FixtureFixturePackageId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // Name
            if (!file.ItemPropertyExists((UInt16)SectionType.Fixtures, FieldNames.FixtureName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Fixtures,
                    FieldNames.FixtureName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Height
            if (!file.ItemPropertyExists((UInt16)SectionType.Fixtures, FieldNames.FixtureHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Fixtures,
                    FieldNames.FixtureHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Width
            if (!file.ItemPropertyExists((UInt16)SectionType.Fixtures, FieldNames.FixtureWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Fixtures,
                    FieldNames.FixtureWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Depth
            if (!file.ItemPropertyExists((UInt16)SectionType.Fixtures, FieldNames.FixtureDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Fixtures,
                    FieldNames.FixtureDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // NumberOfAssemblies
            if (!file.ItemPropertyExists((UInt16)SectionType.Fixtures, FieldNames.FixtureNumberOfAssemblies))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Fixtures,
                    FieldNames.FixtureNumberOfAssemblies,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            // NumberOfMerchandisedSubComponents
            if (!file.ItemPropertyExists((UInt16)SectionType.Fixtures, FieldNames.FixtureNumberOfMerchandisedSubComponents))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Fixtures,
                    FieldNames.FixtureNumberOfMerchandisedSubComponents,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            // TotalFixtureCost
            if (!file.ItemPropertyExists((UInt16)SectionType.Fixtures, FieldNames.FixtureTotalFixtureCost))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.Fixtures,
                    FieldNames.FixtureTotalFixtureCost,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            #endregion

            #region FixtureAssemblyItems

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblyItems, FieldNames.FixtureAssemblyId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblyItems,
                    FieldNames.FixtureAssemblyId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FixtureId
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblyItems, FieldNames.FixtureAssemblyItemFixtureId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblyItems,
                    FieldNames.FixtureAssemblyItemFixtureId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FixtureAssemblyId
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblyItems, FieldNames.FixtureAssemblyItemFixtureAssemblyId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblyItems,
                    FieldNames.FixtureAssemblyItemFixtureAssemblyId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // X
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblyItems, FieldNames.FixtureAssemblyItemX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblyItems,
                    FieldNames.FixtureAssemblyItemX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Y
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblyItems, FieldNames.FixtureAssemblyItemY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblyItems,
                    FieldNames.FixtureAssemblyItemY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Z
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblyItems, FieldNames.FixtureAssemblyItemZ))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblyItems,
                    FieldNames.FixtureAssemblyItemZ,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Slope
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblyItems, FieldNames.FixtureAssemblyItemSlope))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblyItems,
                    FieldNames.FixtureAssemblyItemSlope,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Angle
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblyItems, FieldNames.FixtureAssemblyItemAngle))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblyItems,
                    FieldNames.FixtureAssemblyItemAngle,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Roll
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblyItems, FieldNames.FixtureAssemblyItemRoll))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblyItems,
                    FieldNames.FixtureAssemblyItemRoll,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            #endregion

            #region FixtureComponentItems

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponentItems, FieldNames.FixtureComponentItemId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponentItems,
                    FieldNames.FixtureComponentItemId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FixtureId
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponentItems, FieldNames.FixtureComponentItemFixtureId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponentItems,
                    FieldNames.FixtureComponentItemFixtureId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FixtureComponentId
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponentItems, FieldNames.FixtureComponentItemFixtureComponentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponentItems,
                    FieldNames.FixtureComponentItemFixtureComponentId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // X
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponentItems, FieldNames.FixtureComponentItemX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponentItems,
                    FieldNames.FixtureComponentItemX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Y
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponentItems, FieldNames.FixtureComponentItemY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponentItems,
                    FieldNames.FixtureComponentItemY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Z
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponentItems, FieldNames.FixtureComponentItemZ))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponentItems,
                    FieldNames.FixtureComponentItemZ,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Slope
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponentItems, FieldNames.FixtureComponentItemSlope))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponentItems,
                    FieldNames.FixtureComponentItemSlope,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Angle
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponentItems, FieldNames.FixtureComponentItemAngle))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponentItems,
                    FieldNames.FixtureComponentItemAngle,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Roll
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponentItems, FieldNames.FixtureComponentItemRoll))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponentItems,
                    FieldNames.FixtureComponentItemRoll,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            #endregion

            #region FixtureAssemblies

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblies, FieldNames.FixtureAssemblyId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblies,
                    FieldNames.FixtureAssemblyId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FixturePackageId
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblies, FieldNames.FixtureAssemblyFixturePackageId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblies,
                    FieldNames.FixtureAssemblyFixturePackageId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // Name
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblies, FieldNames.FixtureAssemblyName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblies,
                    FieldNames.FixtureAssemblyName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Height
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblies, FieldNames.FixtureAssemblyHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblies,
                    FieldNames.FixtureAssemblyHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Width
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblies, FieldNames.FixtureAssemblyWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblies,
                    FieldNames.FixtureAssemblyWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Depth
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblies, FieldNames.FixtureAssemblyDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblies,
                    FieldNames.FixtureAssemblyDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // NumberOfComponents
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblies, FieldNames.FixtureAssemblyNumberOfComponents))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblies,
                    FieldNames.FixtureAssemblyNumberOfComponents,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            // TotalComponentCost
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblies, FieldNames.FixtureAssemblyTotalComponentCost))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblies,
                    FieldNames.FixtureAssemblyTotalComponentCost,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            #endregion

            #region FixtureAssemblyComponents

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblyComponents, FieldNames.FixtureAssemblyComponentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblyComponents,
                    FieldNames.FixtureAssemblyComponentId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FixtureAssemblyId
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblyComponents, FieldNames.FixtureAssemblyComponentFixtureAssemblyId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblyComponents,
                    FieldNames.FixtureAssemblyComponentFixtureAssemblyId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FixtureComponentId
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblyComponents, FieldNames.FixtureAssemblyComponentFixtureComponentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblyComponents,
                    FieldNames.FixtureAssemblyComponentFixtureComponentId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // X
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblyComponents, FieldNames.FixtureAssemblyComponentX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblyComponents,
                    FieldNames.FixtureAssemblyComponentX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Y
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblyComponents, FieldNames.FixtureAssemblyComponentY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblyComponents,
                    FieldNames.FixtureAssemblyComponentY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Z
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblyComponents, FieldNames.FixtureAssemblyComponentZ))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblyComponents,
                    FieldNames.FixtureAssemblyComponentZ,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Slope
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblyComponents, FieldNames.FixtureAssemblyComponentSlope))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblyComponents,
                    FieldNames.FixtureAssemblyComponentSlope,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Angle
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblyComponents, FieldNames.FixtureAssemblyComponentAngle))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblyComponents,
                    FieldNames.FixtureAssemblyComponentAngle,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Roll
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureAssemblyComponents, FieldNames.FixtureAssemblyComponentRoll))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureAssemblyComponents,
                    FieldNames.FixtureAssemblyComponentRoll,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            #endregion

            #region FixtureComponents

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FixturePackageId
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentFixturePackageId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentFixturePackageId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // Mesh3DId
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentMesh3DId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentMesh3DId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdFront
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentImageIdFront))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentImageIdFront,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdBack
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentImageIdBack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentImageIdBack,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdTop
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentImageIdTop))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentImageIdTop,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdBottom
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentImageIdBottom))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentImageIdBottom,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdLeft
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentImageIdLeft))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentImageIdLeft,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdRight
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentImageIdRight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentImageIdRight,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // Name
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Height
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Width
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Depth
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // NumberOfSubComponents
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentNumberOfSubComponents))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentNumberOfSubComponents,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            // NumberOfMerchandisedSubComponents
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentNumberOfMerchandisedSubComponents))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentNumberOfMerchandisedSubComponents,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            // NumberOfShelfEdgeLabels
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentNumberOfShelfEdgeLabels))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentNumberOfShelfEdgeLabels,
                    GalleriaBinaryFile.ItemPropertyType.Int16,
                    null);
            }

            // IsMoveable
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentIsMoveable))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentIsMoveable,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsDisplayOnly
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentIsDisplayOnly))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentIsDisplayOnly,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // CanAttachShelfEdgeLabel
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentCanAttachShelfEdgeLabel))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentCanAttachShelfEdgeLabel,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // RetailerReferenceCode
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentRetailerReferenceCode))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentRetailerReferenceCode,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // BarCode
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentBarCode))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentBarCode,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Manufacturer
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentManufacturer))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentManufacturer,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // ManufacturerPartName
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentManufacturerPartName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentManufacturerPartName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // ManufacturerPartNumber
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentManufacturerPartNumber))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentManufacturerPartNumber,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // SupplierName
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentSupplierName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentSupplierName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // SupplierPartNumber
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentSupplierPartNumber))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentSupplierPartNumber,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // SupplierCostPrice
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentSupplierCostPrice))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentSupplierCostPrice,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // SupplierDiscount
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentSupplierDiscount))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentSupplierDiscount,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // SupplierLeadTime
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentSupplierLeadTime))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentSupplierLeadTime,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // MinPurchaseQty
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentMinPurchaseQty))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentMinPurchaseQty,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // WeightLimit
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentWeightLimit))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentWeightLimit,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // Weight
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentWeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentWeight,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // Volume
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentVolume))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentVolume,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // Diameter
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentDiameter))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentDiameter,
                    GalleriaBinaryFile.ItemPropertyType.NullableSingle,
                    null);
            }

            // Capacity
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentCapacity))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentCapacity,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt16,
                    null);
            }

            // ComponentType
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentComponentType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentComponentType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // IsMerchandisedTopDown
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureComponents, FieldNames.FixtureComponentIsMerchandisedTopDown))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureComponents,
                    FieldNames.FixtureComponentIsMerchandisedTopDown,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            #endregion

            #region FixtureSubComponents

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FixtureComponentId
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentFixtureComponentId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentFixtureComponentId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // Mesh3DId
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentMesh3DId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentMesh3DId,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdFront
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentImageIdFront))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentImageIdFront,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdBack
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentImageIdBack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentImageIdBack,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdTop
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentImageIdTop))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentImageIdTop,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdBottom
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentImageIdBottom))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentImageIdBottom,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdLeft
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentImageIdLeft))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentImageIdLeft,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // ImageIdRight
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentImageIdRight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentImageIdRight,
                    GalleriaBinaryFile.ItemPropertyType.NullableInt32,
                    null);
            }

            // Name
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Height
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Width
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Depth
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // X
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Y
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Z
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentZ))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentZ,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Slope
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentSlope))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentSlope,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Angle
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentAngle))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentAngle,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // Roll
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentRoll))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentRoll,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // ShapeType
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentShapeType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentShapeType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // MerchandisableHeight
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentMerchandisableHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentMerchandisableHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // IsVisible
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentIsVisible))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentIsVisible,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // HasCollisionDetection
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentHasCollisionDetection))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentHasCollisionDetection,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // FillPatternTypeFront
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentFillPatternTypeFront))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentFillPatternTypeFront,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // FillPatternTypeBack
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentFillPatternTypeBack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentFillPatternTypeBack,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // FillPatternTypeTop
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentFillPatternTypeTop))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentFillPatternTypeTop,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // FillPatternTypeBottom
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentFillPatternTypeBottom))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentFillPatternTypeBottom,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // FillPatternTypeLeft
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentFillPatternTypeLeft))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentFillPatternTypeLeft,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // FillPatternTypeRight
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentFillPatternTypeRight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentFillPatternTypeRight,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // FillColourFront
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentFillColourFront))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentFillColourFront,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FillColourBack
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentFillColourBack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentFillColourBack,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FillColourTop
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentFillColourTop))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentFillColourTop,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FillColourBottom
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentFillColourBottom))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentFillColourBottom,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FillColourLeft
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentFillColourLeft))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentFillColourLeft,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FillColourRight
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentFillColourRight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentFillColourRight,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // LineColour
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentLineColour))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentLineColour,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // TransparencyPercentFront
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentTransparencyPercentFront))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentTransparencyPercentFront,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // TransparencyPercentBack
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentTransparencyPercentBack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentTransparencyPercentBack,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // TransparencyPercentTop
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentTransparencyPercentTop))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentTransparencyPercentTop,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // TransparencyPercentBottom
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentTransparencyPercentBottom))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentTransparencyPercentBottom,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // TransparencyPercentLeft
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentTransparencyPercentLeft))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentTransparencyPercentLeft,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // TransparencyPercentRight
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentTransparencyPercentRight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentTransparencyPercentRight,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FaceThicknessFront
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentFaceThicknessFront))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentFaceThicknessFront,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // FaceThicknessBack
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentFaceThicknessBack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentFaceThicknessBack,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // FaceThicknessTop
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentFaceThicknessTop))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentFaceThicknessTop,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // FaceThicknessBottom
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentFaceThicknessBottom))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentFaceThicknessBottom,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // FaceThicknessLeft
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentFaceThicknessLeft))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentFaceThicknessLeft,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // FaceThicknessRight
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentFaceThicknessRight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentFaceThicknessRight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // RiserHeight
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentRiserHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentRiserHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // RiserThickness
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentRiserThickness))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentRiserThickness,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // IsRiserPlacedOnFront
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentIsRiserPlacedOnFront))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentIsRiserPlacedOnFront,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsRiserPlacedOnBack
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentIsRiserPlacedOnBack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentIsRiserPlacedOnBack,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsRiserPlacedOnLeft
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentIsRiserPlacedOnLeft))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentIsRiserPlacedOnLeft,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsRiserPlacedOnRight
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentIsRiserPlacedOnRight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentIsRiserPlacedOnRight,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // RiserFillPatternType
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentRiserFillPatternType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentRiserFillPatternType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // RiserColour
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentRiserColour))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentRiserColour,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // RiserTransparencyPercent
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentRiserTransparencyPercent))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentRiserTransparencyPercent,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // NotchStartX
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentNotchStartX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentNotchStartX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // NotchSpacingX
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentNotchSpacingX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentNotchSpacingX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // NotchStartY
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentNotchStartY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentNotchStartY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // NotchSpacingY
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentNotchSpacingY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentNotchSpacingY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // NotchHeight
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentNotchHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentNotchHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // NotchWidth
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentNotchWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentNotchWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // IsNotchPlacedOnFront
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentIsNotchPlacedOnFront))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentIsNotchPlacedOnFront,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsNotchPlacedOnBack
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentIsNotchPlacedOnBack))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentIsNotchPlacedOnBack,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsNotchPlacedOnLeft
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentIsNotchPlacedOnLeft))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentIsNotchPlacedOnLeft,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsNotchPlacedOnRight
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentIsNotchPlacedOnRight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentIsNotchPlacedOnRight,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // NotchStyleType
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentNotchStyleType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentNotchStyleType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // DividerObstructionHeight
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentDividerObstructionHeight))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentDividerObstructionHeight,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // DividerObstructionWidth
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentDividerObstructionWidth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentDividerObstructionWidth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // DividerObstructionDepth
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentDividerObstructionDepth))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentDividerObstructionDepth,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // DividerObstructionStartX
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentDividerObstructionStartX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentDividerObstructionStartX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // DividerObstructionSpacingX
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentDividerObstructionSpacingX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentDividerObstructionSpacingX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // DividerObstructionStartY
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentDividerObstructionStartY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentDividerObstructionStartY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // DividerObstructionSpacingY
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentDividerObstructionSpacingY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentDividerObstructionSpacingY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // DividerObstructionStartZ
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentDividerObstructionStartZ))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentDividerObstructionStartZ,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // DividerObstructionSpacingZ
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentDividerObstructionSpacingZ))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentDividerObstructionSpacingZ,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchConstraintRow1StartX
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentMerchConstraintRow1StartX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentMerchConstraintRow1StartX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchConstraintRow1SpacingX
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentMerchConstraintRow1SpacingX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentMerchConstraintRow1SpacingX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchConstraintRow1StartY
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentMerchConstraintRow1StartY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentMerchConstraintRow1StartY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchConstraintRow1SpacingY
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentMerchConstraintRow1SpacingY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentMerchConstraintRow1SpacingY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchConstraintRow1Height
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentMerchConstraintRow1Height))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentMerchConstraintRow1Height,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchConstraintRow1Width
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentMerchConstraintRow1Width))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentMerchConstraintRow1Width,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchConstraintRow2StartX
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentMerchConstraintRow2StartX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentMerchConstraintRow2StartX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchConstraintRow2SpacingX
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentMerchConstraintRow2SpacingX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentMerchConstraintRow2SpacingX,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchConstraintRow2StartY
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentMerchConstraintRow2StartY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentMerchConstraintRow2StartY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchConstraintRow2SpacingY
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentMerchConstraintRow2SpacingY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentMerchConstraintRow2SpacingY,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchConstraintRow2Height
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentMerchConstraintRow2Height))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentMerchConstraintRow2Height,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchConstraintRow2Width
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentMerchConstraintRow2Width))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentMerchConstraintRow2Width,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // LineThickness
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentLineThickness))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentLineThickness,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // MerchandisingType
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentMerchandisingType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentMerchandisingType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // CombineType
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentCombineType))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentCombineType,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // IsProductOverlapAllowed
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentIsProductOverlapAllowed))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentIsProductOverlapAllowed,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // MerchandisingStrategyX
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentMerchandisingStrategyX))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentMerchandisingStrategyX,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // MerchandisingStrategyY
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentMerchandisingStrategyY))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentMerchandisingStrategyY,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // MerchandisingStrategyZ
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentMerchandisingStrategyZ))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentMerchandisingStrategyZ,
                    GalleriaBinaryFile.ItemPropertyType.Byte,
                    null);
            }

            // LeftOverhang
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentLeftOverhang))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentLeftOverhang,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // RightOverhang
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentRightOverhang))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentRightOverhang,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // FrontOverhang
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentFrontOverhang))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentFrontOverhang,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // BackOverhang
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentBackOverhang))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentBackOverhang,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // TopOverhang
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentTopOverhang))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentTopOverhang,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // BottomOverhang
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentBottomOverhang))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentBottomOverhang,
                    GalleriaBinaryFile.ItemPropertyType.Single,
                    null);
            }

            // IsDividerObstructionAtStart
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentIsDividerObstructionAtStart))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentIsDividerObstructionAtStart,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsDividerObstructionAtEnd
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentIsDividerObstructionAtEnd))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentIsDividerObstructionAtEnd,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            // IsDividerObstructionByFacing
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureSubComponents, FieldNames.FixtureSubComponentIsDividerObstructionByFacing))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureSubComponents,
                    FieldNames.FixtureSubComponentIsDividerObstructionByFacing,
                    GalleriaBinaryFile.ItemPropertyType.Boolean,
                    null);
            }

            #endregion

            #region FixtureImages

            // Id
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureImages, FieldNames.FixtureImageId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureImages,
                    FieldNames.FixtureImageId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FixturePackageId
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureImages, FieldNames.FixtureImageFixturePackageId))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureImages,
                    FieldNames.FixtureImageFixturePackageId,
                    GalleriaBinaryFile.ItemPropertyType.Int32,
                    null);
            }

            // FileName
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureImages, FieldNames.FixtureImageFileName))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureImages,
                    FieldNames.FixtureImageFileName,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // Description
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureImages, FieldNames.FixtureImageDescription))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureImages,
                    FieldNames.FixtureImageDescription,
                    GalleriaBinaryFile.ItemPropertyType.String,
                    null);
            }

            // ImageData
            if (!file.ItemPropertyExists((UInt16)SectionType.FixtureImages, FieldNames.FixtureImageImageData))
            {
                file.AddItemProperty(
                    (UInt16)SectionType.FixtureImages,
                    FieldNames.FixtureImageImageData,
                    GalleriaBinaryFile.ItemPropertyType.ByteArray,
                    null);
            }

            #endregion

            #endregion
        }
        #endregion

        #region Downgrade
        /// <summary>
        /// Performs the downgrade of a fixture file
        /// </summary>
        public void Downgrade(GalleriaBinaryFile file)
        {
            // Do nothing--this is the first version!
        }
        #endregion
    }
}
