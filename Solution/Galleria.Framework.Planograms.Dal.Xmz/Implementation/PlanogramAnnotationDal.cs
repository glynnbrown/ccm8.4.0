﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Xmz.Resources;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramAnnotationDal : DalBase, IPlanogramAnnotationDal
    {
        #region Fetch

        public IEnumerable<PlanogramAnnotationDto> FetchByPlanogramId(object planogramId)
        {
            List<PlanogramAnnotationDto> dtoList = new List<PlanogramAnnotationDto>();
            foreach (PlanogramAnnotationDto dto in this.DalContext.DalCache.PlanogramAnnotationDtoList)
            {
                if (dto.PlanogramId.Equals(planogramId))
                {
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramAnnotationDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalContext.DalCache.PlanogramAnnotationDtoList.Add(dto);
        }

        public void Insert(IEnumerable<PlanogramAnnotationDto> dtos)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            foreach (PlanogramAnnotationDto dto in dtos)
            {
                DalContext.DalCache.PlanogramAnnotationDtoList.Add(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramAnnotationDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramAnnotationDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramAnnotationDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramAnnotationDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
