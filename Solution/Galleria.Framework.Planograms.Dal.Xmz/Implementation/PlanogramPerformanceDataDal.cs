﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion
using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Xmz.Resources;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramPerformanceDataDal : DalBase, IPlanogramPerformanceDataDal
    {
        #region Fetch

        public IEnumerable<PlanogramPerformanceDataDto> FetchByPlanogramPerformanceId(object planogramPerformanceId)
        {
            List<PlanogramPerformanceDataDto> dtoList = new List<PlanogramPerformanceDataDto>();
            foreach (PlanogramPerformanceDataDto dto in this.DalContext.DalCache.PlanogramPerformanceDataDtoList)
            {
                if (dto.PlanogramPerformanceId.Equals(planogramPerformanceId))
                {
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramPerformanceDataDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalContext.DalCache.PlanogramPerformanceDataDtoList.Add(dto);
        }

        public void Insert(IEnumerable<PlanogramPerformanceDataDto> dtos)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            foreach (PlanogramPerformanceDataDto dto in dtos)
            {
                DalContext.DalCache.PlanogramPerformanceDataDtoList.Add(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramPerformanceDataDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramPerformanceDataDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramPerformanceDataDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramPerformanceDataDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
