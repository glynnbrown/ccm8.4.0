﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramAssortmentProductDal : DalBase, IPlanogramAssortmentProductDal
    {
        #region Fetch

        public IEnumerable<PlanogramAssortmentProductDto> FetchByPlanogramAssortmentId(object id)
        {
            return new List<PlanogramAssortmentProductDto>();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramAssortmentProductDto dto)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        public void Insert(IEnumerable<PlanogramAssortmentProductDto> dtos)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        #endregion

        #region Update

        public void Update(PlanogramAssortmentProductDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramAssortmentProductDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramAssortmentProductDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramAssortmentProductDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
