﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820

// V8-30754 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    /// <summary>
    ///     DAL implementation of <see cref="IPlanogramConsumerDecisionTreeNodeDal" /> for a <c>JDA Space Planning</c> file.
    /// </summary>
    /// <remarks>This class is not instantiated directly but is used through the <c>Dependency Injection</c> pattern.</remarks>
    public class PlanogramConsumerDecisionTreeNodeProductDal : DalBase, IPlanogramConsumerDecisionTreeNodeProductDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch the <see cref="PlanogramConsumerDecisionTreeDto"/> that matches the given <paramref name="planogramConsumerDecisionTreeId" />.
        /// </summary>
        /// <param name="planogramConsumerDecisionTreeNodeId">Id of the <c>Planogram CDT Node</c> that the <c>ConsumerDecisionTreenodeProduct</c> instance belongs to.</param>
        /// <returns>A new <c>DTO</c> instance representing the <c>ConsumerDecisionTree</c> in a particular <c>Planogram</c>.</returns>
        /// <remarks>This implementation just creates an empty <c>ConsumerDecisionTree</c> instance for the given <paramerf name="planogramId" />.</remarks>
        public IEnumerable<PlanogramConsumerDecisionTreeNodeProductDto> FetchByPlanogramConsumerDecisionTreeNodeId(Object planogramConsumerDecisionTreeNodeId)
        {
            return  new List<PlanogramConsumerDecisionTreeNodeProductDto>();
            //return DalContext.DalCache.GetPlanogramConsumerDecisionTreeNodeProductDtos(planogramConsumerDecisionTreeNodeId);
        }

        #endregion

        #region IPlanogramConsumerDecisionTreeNodeProductDto Members

        public void Insert(PlanogramConsumerDecisionTreeNodeProductDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(PlanogramConsumerDecisionTreeNodeProductDto dto)
        {
            throw new NotImplementedException();
        }

        public void DeleteById(Object id)
        {
            throw new NotImplementedException();
        }

        void IBatchDal<PlanogramConsumerDecisionTreeNodeProductDto>.Insert(PlanogramConsumerDecisionTreeNodeProductDto dto)
        {
            Insert(dto);
        }

        public void Insert(IEnumerable<PlanogramConsumerDecisionTreeNodeProductDto> dtos)
        {
            //throw new NotImplementedException();
        }

        void IBatchDal<PlanogramConsumerDecisionTreeNodeProductDto>.Update(PlanogramConsumerDecisionTreeNodeProductDto dto)
        {
            Update(dto);
        }

        public void Update(IEnumerable<PlanogramConsumerDecisionTreeNodeProductDto> dtos)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramConsumerDecisionTreeNodeProductDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramConsumerDecisionTreeNodeProductDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}