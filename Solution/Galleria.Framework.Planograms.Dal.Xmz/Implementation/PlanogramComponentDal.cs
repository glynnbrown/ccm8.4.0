﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Xmz.Resources;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramComponentDal : DalBase, IPlanogramComponentDal
    {
        #region Fetch

        public IEnumerable<PlanogramComponentDto> FetchByPlanogramId(object planogramId)
        {
            List<PlanogramComponentDto> dtoList = new List<PlanogramComponentDto>();
            foreach (PlanogramComponentDto dto in this.DalContext.DalCache.PlanogramComponentDtoList)
            {
                if (dto.PlanogramId.Equals(planogramId))
                {
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramComponentDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalContext.DalCache.PlanogramComponentDtoList.Add(dto);
        }

        public void Insert(IEnumerable<PlanogramComponentDto> dtos)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            foreach (PlanogramComponentDto dto in dtos)
            {
                DalContext.DalCache.PlanogramComponentDtoList.Add(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramComponentDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramComponentDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramComponentDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramComponentDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
