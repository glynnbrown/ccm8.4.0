﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramMetadataImageDal : DalBase, IPlanogramMetadataImageDal
    {
        #region Fetch

        public IEnumerable<PlanogramMetadataImageDto> FetchByPlanogramId(object planogramId)
        {
            return new List<PlanogramMetadataImageDto>();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramMetadataImageDto dto)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        public void Insert(IEnumerable<PlanogramMetadataImageDto> dtos)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        #endregion

        #region Update

        public void Update(PlanogramMetadataImageDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramMetadataImageDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramMetadataImageDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramMetadataImageDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
