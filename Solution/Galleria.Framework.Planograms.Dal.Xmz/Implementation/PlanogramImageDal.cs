﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramImageDal : DalBase, IPlanogramImageDal
    {
        #region Fetch

        public IEnumerable<PlanogramImageDto> FetchByPlanogramId(object planogramId)
        {
            return new List<PlanogramImageDto>();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramImageDto dto)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        public void Insert(IEnumerable<PlanogramImageDto> dtos)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        #endregion

        #region Update

        public void Update(PlanogramImageDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramImageDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramImageDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramImageDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
