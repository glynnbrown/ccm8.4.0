﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Xmz.Resources;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramConsumerDecisionTreeNodeDal : DalBase, IPlanogramConsumerDecisionTreeNodeDal
    {
        #region Fetch

        public IEnumerable<PlanogramConsumerDecisionTreeNodeDto> FetchByPlanogramConsumerDecisionTreeId(object planogramConsumerDecisionTreeId)
        {
            List<PlanogramConsumerDecisionTreeNodeDto> dtoList = new List<PlanogramConsumerDecisionTreeNodeDto>();
            foreach (PlanogramConsumerDecisionTreeNodeDto dto in this.DalContext.DalCache.PlanogramConsumerDecisionTreeNodeDtoList)
            {
                if (dto.PlanogramConsumerDecisionTreeId.Equals(planogramConsumerDecisionTreeId))
                {
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramConsumerDecisionTreeNodeDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalContext.DalCache.PlanogramConsumerDecisionTreeNodeDtoList.Add(dto);
        }

        public void Insert(IEnumerable<PlanogramConsumerDecisionTreeNodeDto> dtos)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            foreach (PlanogramConsumerDecisionTreeNodeDto dto in dtos)
            {
                DalContext.DalCache.PlanogramConsumerDecisionTreeNodeDtoList.Add(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramConsumerDecisionTreeNodeDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramConsumerDecisionTreeNodeDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramConsumerDecisionTreeNodeDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramConsumerDecisionTreeNodeDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
