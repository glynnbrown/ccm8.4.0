﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Xmz.Resources;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramDal : DalBase, IPlanogramDal
    {
        #region Fetch

        public IEnumerable<PlanogramDto> FetchByPackageId(object packageId)
        {
            List<PlanogramDto> dtoList = new List<PlanogramDto>();
            foreach (PlanogramDto dto in this.DalContext.DalCache.PlanogramDtoList)
            {
                if (dto.PackageId.Equals(packageId))
                {
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PackageDto packageDto, PlanogramDto planogramDto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalContext.DalCache.PlanogramDtoList.Add(planogramDto);
        }

        public void Insert(PlanogramDto planogramDto)
        {
            this.Insert(null, planogramDto);
        }

        #endregion

        #region Update

        public void Update(PackageDto packageDto, PlanogramDto planogramDto)
        {
            throw new NotImplementedException();
        }

        public void Update(PlanogramDto planogramDto)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
