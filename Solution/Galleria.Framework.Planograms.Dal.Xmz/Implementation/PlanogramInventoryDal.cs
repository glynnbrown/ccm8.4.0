﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion

using System;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramInventoryDal : DalBase, IPlanogramInventoryDal
    {
        #region Fetch

        public PlanogramInventoryDto FetchByPlanogramId(object id)
        {
            return new PlanogramInventoryDto()
            {
                Id = 1,
                PlanogramId = id,
                DaysOfPerformance = 7
            };
        }

        #endregion

        #region Insert

        public void Insert(PlanogramInventoryDto dto)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        public void Insert(System.Collections.Generic.IEnumerable<PlanogramInventoryDto> dtos)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        #endregion

        #region Update

        public void Update(PlanogramInventoryDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(System.Collections.Generic.IEnumerable<PlanogramInventoryDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramInventoryDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(System.Collections.Generic.IEnumerable<PlanogramInventoryDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
