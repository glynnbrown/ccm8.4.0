﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion

using System;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramValidationTemplateDal : DalBase, IPlanogramValidationTemplateDal
    {
        #region Fetch
        
        public PlanogramValidationTemplateDto FetchByPlanogramId(object id)
        {
            return new PlanogramValidationTemplateDto()
            {
                Id = 1,
                PlanogramId = id,
                Name = Message.PlanogramValidationTemplate_Name_Default
            };
        }

        #endregion

        #region Insert

        public void Insert(PlanogramValidationTemplateDto dto)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        public void Insert(System.Collections.Generic.IEnumerable<PlanogramValidationTemplateDto> dtos)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        #endregion

        #region Update

        public void Update(PlanogramValidationTemplateDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(System.Collections.Generic.IEnumerable<PlanogramValidationTemplateDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramValidationTemplateDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(System.Collections.Generic.IEnumerable<PlanogramValidationTemplateDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
