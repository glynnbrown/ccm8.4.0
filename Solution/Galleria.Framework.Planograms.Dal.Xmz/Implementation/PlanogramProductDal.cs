﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Xmz.Resources;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramProductDal : DalBase, IPlanogramProductDal
    {
        #region Fetch

        public IEnumerable<PlanogramProductDto> FetchByPlanogramId(object planogramId)
        {
            List<PlanogramProductDto> dtoList = new List<PlanogramProductDto>();
            foreach (PlanogramProductDto dto in this.DalContext.DalCache.PlanogramProductDtoList)
            {
                if (dto.PlanogramId.Equals(planogramId))
                {
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramProductDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalContext.DalCache.PlanogramProductDtoList.Add(dto);
        }

        public void Insert(IEnumerable<PlanogramProductDto> dtos)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            foreach (PlanogramProductDto dto in dtos)
            {
                DalContext.DalCache.PlanogramProductDtoList.Add(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramProductDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramProductDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramProductDto> dtos)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramProductDto dto)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
