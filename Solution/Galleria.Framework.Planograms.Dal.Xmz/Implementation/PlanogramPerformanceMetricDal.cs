﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Planograms.Dal.Xmz.Resources;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramPerformanceMetricDal : DalBase, IPlanogramPerformanceMetricDal
    {
        #region Fetch

        public IEnumerable<PlanogramPerformanceMetricDto> FetchByPlanogramPerformanceId(object planogramPerformanceId)
        {
            List<PlanogramPerformanceMetricDto> dtoList = new List<PlanogramPerformanceMetricDto>();
            foreach (PlanogramPerformanceMetricDto dto in this.DalContext.DalCache.PlanogramPerformanceMetricDtoList)
            {
                if (dto.PlanogramPerformanceId.Equals(planogramPerformanceId))
                {
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramPerformanceMetricDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalContext.DalCache.PlanogramPerformanceMetricDtoList.Add(dto);
        }

        public void Insert(IEnumerable<PlanogramPerformanceMetricDto> dtos)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            foreach (PlanogramPerformanceMetricDto dto in dtos)
            {
                DalContext.DalCache.PlanogramPerformanceMetricDtoList.Add(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramPerformanceMetricDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramPerformanceMetricDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramPerformanceMetricDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramPerformanceMetricDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
