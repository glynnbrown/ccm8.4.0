﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramBlockingGroupDal : DalBase, IPlanogramBlockingGroupDal
    {
        #region Fetch

        public IEnumerable<PlanogramBlockingGroupDto> FetchByPlanogramBlockingId(object id)
        {
            return new List<PlanogramBlockingGroupDto>();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramBlockingGroupDto dto)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        public void Insert(IEnumerable<PlanogramBlockingGroupDto> dtos)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        #endregion

        #region Update

        public void Update(PlanogramBlockingGroupDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramBlockingGroupDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramBlockingGroupDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramBlockingGroupDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}