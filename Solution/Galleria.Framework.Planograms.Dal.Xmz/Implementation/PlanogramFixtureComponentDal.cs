﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Xmz.Resources;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramFixtureComponentDal : DalBase, IPlanogramFixtureComponentDal
    {
        #region Fetch

        public IEnumerable<PlanogramFixtureComponentDto> FetchByPlanogramFixtureId(object planogramFixtureId)
        {
            List<PlanogramFixtureComponentDto> dtoList = new List<PlanogramFixtureComponentDto>();
            foreach (PlanogramFixtureComponentDto dto in this.DalContext.DalCache.PlanogramFixtureComponentDtoList)
            {
                if (dto.PlanogramFixtureId.Equals(planogramFixtureId))
                {
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramFixtureComponentDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalContext.DalCache.PlanogramFixtureComponentDtoList.Add(dto);
        }

        public void Insert(IEnumerable<PlanogramFixtureComponentDto> dtos)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            foreach (PlanogramFixtureComponentDto dto in dtos)
            {
                DalContext.DalCache.PlanogramFixtureComponentDtoList.Add(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramFixtureComponentDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramFixtureComponentDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramFixtureComponentDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramFixtureComponentDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
