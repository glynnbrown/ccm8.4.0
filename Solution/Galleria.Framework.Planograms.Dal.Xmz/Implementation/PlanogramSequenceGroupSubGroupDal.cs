﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32448 : M.Pettit
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramSequenceGroupSubGroupDal : DalBase, IPlanogramSequenceGroupSubGroupDal
    {
        #region Fetch

        public IEnumerable<PlanogramSequenceGroupSubGroupDto> FetchByPlanogramSequenceGroupId(object planogramSequenceGroupId)
        {
            return new List<PlanogramSequenceGroupSubGroupDto>();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramSequenceGroupSubGroupDto dto)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        public void Insert(IEnumerable<PlanogramSequenceGroupSubGroupDto> dtos)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        #endregion

        #region Update

        public void Update(IEnumerable<PlanogramSequenceGroupSubGroupDto> dtos)
        {
            throw new NotImplementedException();
        }

        public void Update(PlanogramSequenceGroupSubGroupDto dto)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramSequenceGroupSubGroupDto> dtos)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramSequenceGroupSubGroupDto dto)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
