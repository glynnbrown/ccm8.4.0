﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Xmz.Resources;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramConsumerDecisionTreeLevelDal : DalBase, IPlanogramConsumerDecisionTreeLevelDal
    {
        #region Fetch

        public IEnumerable<PlanogramConsumerDecisionTreeLevelDto> FetchByPlanogramConsumerDecisionTreeId(object planogramConsumerDecisionTreeId)
        {
            List<PlanogramConsumerDecisionTreeLevelDto> dtoList = new List<PlanogramConsumerDecisionTreeLevelDto>();
            foreach (PlanogramConsumerDecisionTreeLevelDto dto in this.DalContext.DalCache.PlanogramConsumerDecisionTreeLevelDtoList)
            {
                if (dto.PlanogramConsumerDecisionTreeId.Equals(planogramConsumerDecisionTreeId))
                {
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramConsumerDecisionTreeLevelDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalContext.DalCache.PlanogramConsumerDecisionTreeLevelDtoList.Add(dto);
        }

        public void Insert(IEnumerable<PlanogramConsumerDecisionTreeLevelDto> dtos)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            foreach (PlanogramConsumerDecisionTreeLevelDto dto in dtos)
            {
                DalContext.DalCache.PlanogramConsumerDecisionTreeLevelDtoList.Add(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramConsumerDecisionTreeLevelDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramConsumerDecisionTreeLevelDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramConsumerDecisionTreeLevelDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramConsumerDecisionTreeLevelDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
