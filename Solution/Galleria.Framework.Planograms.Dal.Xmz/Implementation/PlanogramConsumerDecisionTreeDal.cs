﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion

using System;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Xmz.Resources;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramConsumerDecisionTreeDal : DalBase, IPlanogramConsumerDecisionTreeDal
    {
        #region Fetch

        public PlanogramConsumerDecisionTreeDto FetchByPlanogramId(object id)
        {
            PlanogramConsumerDecisionTreeDto dto = this.DalContext.DalCache.PlanogramConsumerDecisionTreeDtoList.Where(p => p.PlanogramId.Equals(id)).FirstOrDefault();
            if (dto == null) throw new DtoDoesNotExistException();
            return dto;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramConsumerDecisionTreeDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalContext.DalCache.PlanogramConsumerDecisionTreeDtoList.Add(dto);
        }

        public void Insert(System.Collections.Generic.IEnumerable<PlanogramConsumerDecisionTreeDto> dtos)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            foreach (PlanogramConsumerDecisionTreeDto dto in dtos)
            {
                DalContext.DalCache.PlanogramConsumerDecisionTreeDtoList.Add(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramConsumerDecisionTreeDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(System.Collections.Generic.IEnumerable<PlanogramConsumerDecisionTreeDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramConsumerDecisionTreeDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(System.Collections.Generic.IEnumerable<PlanogramConsumerDecisionTreeDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
