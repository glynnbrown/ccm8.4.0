﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
// V8-32388 : M.Pettit
//  Aded to exports so that Assemblies can be exported
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Xmz.Resources;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramAssemblyComponentDal : DalBase, IPlanogramAssemblyComponentDal
    {
        #region Fetch

        public IEnumerable<PlanogramAssemblyComponentDto> FetchByPlanogramAssemblyId(object planogramAssemblyId)
        {
            List<PlanogramAssemblyComponentDto> dtoList = new List<PlanogramAssemblyComponentDto>();
            foreach (PlanogramAssemblyComponentDto dto in this.DalContext.DalCache.PlanogramAssemblyComponentDtoList)
            {
                if (dto.PlanogramAssemblyId.Equals(planogramAssemblyId))
                {
                    dtoList.Add(dto);
                }
            }
            return dtoList; 
        }

        #endregion

        #region Insert

        public void Insert(PlanogramAssemblyComponentDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalContext.DalCache.PlanogramAssemblyComponentDtoList.Add(dto);
        }

        public void Insert(IEnumerable<PlanogramAssemblyComponentDto> dtos)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            foreach (PlanogramAssemblyComponentDto dto in dtos)
            {
                DalContext.DalCache.PlanogramAssemblyComponentDtoList.Add(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramAssemblyComponentDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramAssemblyComponentDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramAssemblyComponentDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramAssemblyComponentDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
