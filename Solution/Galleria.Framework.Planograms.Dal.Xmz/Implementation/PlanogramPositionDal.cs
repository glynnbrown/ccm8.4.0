﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Xmz.Resources;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramPositionDal : DalBase, IPlanogramPositionDal
    {
        #region Fetch

        public IEnumerable<PlanogramPositionDto> FetchByPlanogramId(object planogramId)
        {
            List<PlanogramPositionDto> dtoList = new List<PlanogramPositionDto>();
            foreach (PlanogramPositionDto dto in this.DalContext.DalCache.PlanogramPositionDtoList)
            {
                if (dto.PlanogramId.Equals(planogramId))
                {
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramPositionDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalContext.DalCache.PlanogramPositionDtoList.Add(dto);
        }

        public void Insert(IEnumerable<PlanogramPositionDto> dtos)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            foreach (PlanogramPositionDto dto in dtos)
            {
                DalContext.DalCache.PlanogramPositionDtoList.Add(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramPositionDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramPositionDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramPositionDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramPositionDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
