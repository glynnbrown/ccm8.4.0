﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion
using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Xmz.Resources;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramEventLogDal : DalBase, IPlanogramEventLogDal
    {
        #region Fetch

        public IEnumerable<PlanogramEventLogDto> FetchByPlanogramId(object planogramId)
        {
            List<PlanogramEventLogDto> dtoList = new List<PlanogramEventLogDto>();
            foreach (PlanogramEventLogDto dto in this.DalContext.DalCache.PlanogramEventLogDtoList)
            {
                if (dto.PlanogramId.Equals(planogramId))
                {
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramEventLogDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalContext.DalCache.PlanogramEventLogDtoList.Add(dto);
        }

        public void Insert(IEnumerable<PlanogramEventLogDto> dtos)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            foreach (PlanogramEventLogDto dto in dtos)
            {
                DalContext.DalCache.PlanogramEventLogDtoList.Add(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramEventLogDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramEventLogDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramEventLogDto> dtos)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramEventLogDto dto)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
