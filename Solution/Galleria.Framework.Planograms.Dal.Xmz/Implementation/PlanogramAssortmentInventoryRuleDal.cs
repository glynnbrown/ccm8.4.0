﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// CCM-31551 : A.Probyn
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramAssortmentInventoryRuleDal : DalBase, IPlanogramAssortmentInventoryRuleDal
    {
        #region Fetch

        public IEnumerable<PlanogramAssortmentInventoryRuleDto> FetchByPlanogramAssortmentId(object id)
        {
            return new List<PlanogramAssortmentInventoryRuleDto>();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramAssortmentInventoryRuleDto dto)
        {
            //Apollo does not use this data so no need to implement this
        }

        public void Insert(IEnumerable<PlanogramAssortmentInventoryRuleDto> dtos)
        {
            //Apollo does not use this data so no need to implement this
        }

        #endregion

        #region Update

        public void Update(PlanogramAssortmentInventoryRuleDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramAssortmentInventoryRuleDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramAssortmentInventoryRuleDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramAssortmentInventoryRuleDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
