﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion
using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Xmz.Resources;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramFixtureDal : DalBase, IPlanogramFixtureDal
    {
        #region Fetch

        public IEnumerable<PlanogramFixtureDto> FetchByPlanogramId(object planogramId)
        {
            List<PlanogramFixtureDto> dtoList = new List<PlanogramFixtureDto>();
            foreach (PlanogramFixtureDto dto in this.DalContext.DalCache.PlanogramFixtureDtoList)
            {
                if (dto.PlanogramId.Equals(planogramId))
                {
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramFixtureDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalContext.DalCache.PlanogramFixtureDtoList.Add(dto);
        }

        public void Insert(IEnumerable<PlanogramFixtureDto> dtos)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            foreach (PlanogramFixtureDto dto in dtos)
            {
                DalContext.DalCache.PlanogramFixtureDtoList.Add(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramFixtureDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramFixtureDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramFixtureDto> dtos)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramFixtureDto dto)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
