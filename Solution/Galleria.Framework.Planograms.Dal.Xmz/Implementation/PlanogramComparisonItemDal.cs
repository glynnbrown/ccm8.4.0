﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramComparisonItemDal : DalBase, IPlanogramComparisonItemDal
    {
        #region Fetch

        public IEnumerable<PlanogramComparisonItemDto> FetchByPlanogramComparisonResultId(object id)
        {
            throw new DtoDoesNotExistException();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramComparisonItemDto dto)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        public void Insert(IEnumerable<PlanogramComparisonItemDto> dtos)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        #endregion

        #region Update

        public void Update(PlanogramComparisonItemDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramComparisonItemDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramComparisonItemDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramComparisonItemDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
