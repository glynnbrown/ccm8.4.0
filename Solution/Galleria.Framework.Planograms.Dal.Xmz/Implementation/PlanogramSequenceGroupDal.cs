﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion
using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramSequenceGroupDal : DalBase, IPlanogramSequenceGroupDal
    {
        #region Fetch
        public IEnumerable<PlanogramSequenceGroupDto> FetchByPlanogramSequenceId(object planogramSequenceId)
        {
            return new List<PlanogramSequenceGroupDto>();
        }
        #endregion

        #region Insert
        public void Insert(PlanogramSequenceGroupDto dto)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        public void Insert(IEnumerable<PlanogramSequenceGroupDto> dtos)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }
        #endregion

        #region Update
        public void Update(PlanogramSequenceGroupDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramSequenceGroupDto> dtos)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Delete
        public void Delete(IEnumerable<PlanogramSequenceGroupDto> dtos)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramSequenceGroupDto dto)
        {
            throw new NotImplementedException();
        }

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}