﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion

using System;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Planograms.Dal.Xmz.Resources;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramAssortmentDal : DalBase, IPlanogramAssortmentDal
    {
        #region Fetch

        public PlanogramAssortmentDto FetchByPlanogramId(object id)
        {
            return new PlanogramAssortmentDto()
            {
                Id = 1,
                PlanogramId = id,
                Name = Message.PlanogramAssortment_Name_Default
            };
        }

        #endregion

        #region Insert

        public void Insert(PlanogramAssortmentDto dto)
        {
            //Apollo does not use this data so no need to export this
            //if (!DalContext.TransactionInProgress)
            //{
            //    throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            //}
            //DalContext.DalCache.PlanogramAssortmentDtoList.Add(dto);
        }

        public void Insert(System.Collections.Generic.IEnumerable<PlanogramAssortmentDto> dtos)
        {
            //Apollo does not use this data so no need to export this
            //if (!DalContext.TransactionInProgress)
            //{
            //    throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            //}
            //foreach (PlanogramAssortmentDto dto in dtos)
            //{
                //DalContext.DalCache.PlanogramAssortmentDtoList.Add(dto);
            //}
        }

        #endregion

        #region Update

        public void Update(PlanogramAssortmentDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(System.Collections.Generic.IEnumerable<PlanogramAssortmentDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramAssortmentDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(System.Collections.Generic.IEnumerable<PlanogramAssortmentDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
