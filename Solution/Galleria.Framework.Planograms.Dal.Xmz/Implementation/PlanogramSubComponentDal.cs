﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion
using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Xmz.Resources;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramSubComponentDal : DalBase, IPlanogramSubComponentDal
    {
        #region Fetch

        public IEnumerable<PlanogramSubComponentDto> FetchByPlanogramComponentId(object planogramComponentId)
        {
            List<PlanogramSubComponentDto> dtoList = new List<PlanogramSubComponentDto>();
            foreach (PlanogramSubComponentDto dto in this.DalContext.DalCache.PlanogramSubComponentDtoList)
            {
                if (dto.PlanogramComponentId.Equals(planogramComponentId))
                {
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramSubComponentDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalContext.DalCache.PlanogramSubComponentDtoList.Add(dto);
        }

        public void Insert(IEnumerable<PlanogramSubComponentDto> dtos)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            foreach (PlanogramSubComponentDto dto in dtos)
            {
                DalContext.DalCache.PlanogramSubComponentDtoList.Add(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramSubComponentDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramSubComponentDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramSubComponentDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramSubComponentDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
