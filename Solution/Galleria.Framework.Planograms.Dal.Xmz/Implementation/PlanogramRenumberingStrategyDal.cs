﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion
using System;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Xmz.Resources;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramRenumberingStrategyDal : DalBase, IPlanogramRenumberingStrategyDal
    {
        #region Fetch

        public PlanogramRenumberingStrategyDto FetchByPlanogramId(object planogramId)
        {
            return new PlanogramRenumberingStrategyDto()
            {
                Id = 1,
                PlanogramId = planogramId,
                Name = "LG:Language.PlanogramRenumberingStrategy_Name_Default",
                BayComponentXRenumberStrategyPriority = 1,
                BayComponentYRenumberStrategyPriority = 2,
                BayComponentZRenumberStrategyPriority = 3,
                PositionXRenumberStrategyPriority = 1,
                PositionYRenumberStrategyPriority = 2,
                PositionZRenumberStrategyPriority = 3,
                BayComponentXRenumberStrategy = (Byte)PlanogramRenumberingStrategyXRenumberType.LeftToRight,
                BayComponentYRenumberStrategy = (Byte)PlanogramRenumberingStrategyYRenumberType.BottomToTop,
                BayComponentZRenumberStrategy = (Byte)PlanogramRenumberingStrategyZRenumberType.FrontToBack,
                PositionXRenumberStrategy = (Byte)PlanogramRenumberingStrategyXRenumberType.LeftToRight,
                PositionYRenumberStrategy = (Byte)PlanogramRenumberingStrategyYRenumberType.BottomToTop,
                PositionZRenumberStrategy = (Byte)PlanogramRenumberingStrategyZRenumberType.FrontToBack,
                RestartComponentRenumberingPerBay = true,
                IgnoreNonMerchandisingComponents = true,
                RestartPositionRenumberingPerComponent = true,
                UniqueNumberMultiPositionProductsPerComponent = false,
                ExceptAdjacentPositions= false
            };
        }

        #endregion

        #region Insert

        public void Insert(PlanogramRenumberingStrategyDto dto)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        public void Insert(System.Collections.Generic.IEnumerable<PlanogramRenumberingStrategyDto> dtos)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        #endregion

        #region Update

        public void Update(PlanogramRenumberingStrategyDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(System.Collections.Generic.IEnumerable<PlanogramRenumberingStrategyDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramRenumberingStrategyDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(System.Collections.Generic.IEnumerable<PlanogramRenumberingStrategyDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
