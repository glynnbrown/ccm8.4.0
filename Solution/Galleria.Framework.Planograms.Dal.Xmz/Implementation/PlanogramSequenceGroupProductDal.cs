﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramSequenceGroupProductDal : DalBase, IPlanogramSequenceGroupProductDal
    {
        #region Fetch

        public IEnumerable<PlanogramSequenceGroupProductDto> FetchByPlanogramSequenceGroupId(object planogramSequenceGroupId)
        {
            return new List<PlanogramSequenceGroupProductDto>();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramSequenceGroupProductDto dto)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        public void Insert(IEnumerable<PlanogramSequenceGroupProductDto> dtos)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        #endregion

        #region Update

        public void Update(PlanogramSequenceGroupProductDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramSequenceGroupProductDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramSequenceGroupProductDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramSequenceGroupProductDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
