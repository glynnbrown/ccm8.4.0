﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
// V8-32388 : M.Pettit
//  Aded to exports so that Assemblies can be exported
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Xmz.Resources;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramFixtureAssemblyDal : DalBase, IPlanogramFixtureAssemblyDal
    {
        #region Fetch

        public IEnumerable<PlanogramFixtureAssemblyDto> FetchByPlanogramFixtureId(object planogramFixtureId)
        {
            List<PlanogramFixtureAssemblyDto> dtoList = new List<PlanogramFixtureAssemblyDto>();
            foreach (PlanogramFixtureAssemblyDto dto in this.DalContext.DalCache.PlanogramFixtureAssemblyDtoList)
            {
                if (dto.PlanogramFixtureId.Equals(planogramFixtureId))
                {
                    dtoList.Add(dto);
                }
            }
            return dtoList; 
        }

        #endregion

        #region Insert

        public void Insert(PlanogramFixtureAssemblyDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalContext.DalCache.PlanogramFixtureAssemblyDtoList.Add(dto);
        }

        public void Insert(IEnumerable<PlanogramFixtureAssemblyDto> dtos)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            foreach (PlanogramFixtureAssemblyDto dto in dtos)
            {
                DalContext.DalCache.PlanogramFixtureAssemblyDtoList.Add(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramFixtureAssemblyDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramFixtureAssemblyDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramFixtureAssemblyDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramFixtureAssemblyDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
