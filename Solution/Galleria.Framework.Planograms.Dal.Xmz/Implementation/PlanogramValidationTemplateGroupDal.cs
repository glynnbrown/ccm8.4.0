﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion
using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramValidationTemplateGroupDal : DalBase, IPlanogramValidationTemplateGroupDal
    {
        #region Fetch

        public IEnumerable<PlanogramValidationTemplateGroupDto> FetchByPlanogramValidationTemplateId(object planogramValidationTemplateId)
        {
            return new List<PlanogramValidationTemplateGroupDto>();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramValidationTemplateGroupDto dto)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        public void Insert(IEnumerable<PlanogramValidationTemplateGroupDto> dtos)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        #endregion

        #region Update

        public void Update(PlanogramValidationTemplateGroupDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramValidationTemplateGroupDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramValidationTemplateGroupDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramValidationTemplateGroupDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
