﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion

using System;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Planograms.Dal.Xmz.Resources;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramPerformanceDal : DalBase, IPlanogramPerformanceDal
    {
        #region Fetch

        public PlanogramPerformanceDto FetchByPlanogramId(object id)
        {
            return new PlanogramPerformanceDto()
            {
                Id = 1,
                PlanogramId = id,
                Name = Message.PlanogramPerformance_Name_Default
            };
        }

        #endregion

        #region Insert

        public void Insert(PlanogramPerformanceDto dto)
        {
            //if (!DalContext.TransactionInProgress)
            //{
            //    throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            //}
            //DalContext.DalCache.PlanogramPerformanceDtoList.Add(dto);
        }

        public void Insert(System.Collections.Generic.IEnumerable<PlanogramPerformanceDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Update

        public void Update(PlanogramPerformanceDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(System.Collections.Generic.IEnumerable<PlanogramPerformanceDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramPerformanceDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(System.Collections.Generic.IEnumerable<PlanogramPerformanceDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
