﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion
using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramAssemblyDal : DalBase, IPlanogramAssemblyDal
    {
        #region Fetch

        public IEnumerable<PlanogramAssemblyDto> FetchByPlanogramId(object planogramId)
        {
            return new List<PlanogramAssemblyDto>();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramAssemblyDto dto)
        {
            //Apollo does not use this data so no need to implement this
        }

        public void Insert(IEnumerable<PlanogramAssemblyDto> dtos)
        {
            //Apollo does not use this data so no need to implement this
        }

        #endregion

        #region Update

        public void Update(PlanogramAssemblyDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramAssemblyDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramAssemblyDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramAssemblyDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
