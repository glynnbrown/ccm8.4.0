﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramSequenceDal : DalBase, IPlanogramSequenceDal
    {
        #region Fetch
        public PlanogramSequenceDto FetchByPlanogramId(object planogramId)
        {
            return new PlanogramSequenceDto()
            {
                Id = 1,
                PlanogramId = 1
            };
        }
        #endregion

        #region Insert
        public void Insert(PlanogramSequenceDto dto)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }
        public void Insert(IEnumerable<PlanogramSequenceDto> dtos)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }
        #endregion

        #region Update
        public void Update(PlanogramSequenceDto dto)
        {
            throw new NotImplementedException();
        }
        public void Update(IEnumerable<PlanogramSequenceDto> dtos)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Delete
        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramSequenceDto> dtos)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramSequenceDto dto)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}