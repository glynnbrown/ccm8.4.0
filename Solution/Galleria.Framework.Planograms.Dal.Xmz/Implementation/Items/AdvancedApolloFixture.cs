﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
// v8-31256 : M.Brumby
//  Component sizes calculated correctly so that notch numbers are cacluated correctly
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Planograms.External;
using Galleria.Framework.Planograms.External.ApolloStructure;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation.Items
{
    public class AdvancedApolloFixtureComponent
    {
        private IEnumerable<ApolloDataSetSectionShelvesShelf> _shelves;

        private Single? _xPos = null;
        private Single? _yPos = null;
        private Single? _zPos = null;
        private Single _height = 0;
        private Single _width = 0;
        private Single _depth = 0;
        public Int32 Id
        {
            get
            {
                return this.GetHashCode();
            }
        }
        public IEnumerable<ApolloDataSetSectionShelvesShelf> Shelves
        {
            get
            {
                return _shelves;
            }
        }

        public Single XPos
        {
            get
            {
                if (_xPos.HasValue) return _xPos.Value;
                return 0;
            }
        }
        public Single YPos
        {
            get
            {
                if (_yPos.HasValue) return _yPos.Value;
                return 0;
            }
        }
        public Single ZPos
        {
            get
            {
                if (_zPos.HasValue) return _zPos.Value;
                return 0;
            }
        }

        public Single Height
        {
            get
            {
                return _height;
            }
        }
        public Single Width
        {
            get
            {
                return _width;
            }
        }
        public Single Depth
        {
            get
            {
                return _depth;
            }
        }

        private Single _slope = 0;
        public Single Slope
        {
            get
            {
                return _slope;
            }
        }
        private Single _angle = 0;
        public Single Angle
        {
            get
            {
                return _angle;
            }
        }

        private Boolean _rotateSubComponents = true;
        public Boolean RotateSubComponents
        {
            get
            {
                return _rotateSubComponents;
            }
        }
        private ApolloShelfType _componentType = ApolloShelfType.Box;
        public ApolloShelfType ComponentType
        {
            get
            {
                return _componentType;
            }
        }
        public AdvancedApolloFixtureComponent(IEnumerable<ApolloDataSetSectionShelvesShelf> shelves, Galleria.Framework.Planograms.Dal.Xmz.DalCacheItem.UOM uom)
        {
            Boolean convertSuccess = false;
            _shelves = shelves;
            Single angle = 0;
            Single slope = 0;
            Single x2 = 0;
            Single y2 = 0;
            Single z2 = 0;
            ApolloShelfType componentType = ApolloShelfType.Box;
            foreach (ApolloDataSetSectionShelvesShelf shelf in _shelves)
            {
                foreach (ApolloDataSetSectionShelvesShelfShelfData data in shelf.ShelfData)
                {
                    componentType = (ApolloShelfType)Convert.ToByte(data.SH_SHELFTYPE);

                    Single xPos = (Single)SpacePlanningImportHelper.ParseSingle(data.SH_XPOS, false, out convertSuccess);
                    Single yPos = (Single)SpacePlanningImportHelper.ParseSingle(data.SH_YPOS, false, out convertSuccess);
                    Single zPos = (Single)SpacePlanningImportHelper.ParseSingle(data.SH_ZPOS, false, out convertSuccess);
                    if (_xPos.HasValue)
                    {
                        _xPos = Math.Min(_xPos.Value, xPos);
                    }
                    else
                    {
                        _xPos = xPos;
                    }
                    
                    angle = 0;
                    slope = SpacePlanningImportHelper.ConvertToRadians((Single)SpacePlanningImportHelper.ParseSingle(data.SH_SLOPE, false, out convertSuccess));
                    switch (componentType)
                    {
                        case ApolloShelfType.Shelf:
                            yPos -= (Single)SpacePlanningImportHelper.ParseSingle(data.SH_THICKNESS, false, out convertSuccess);
                            break;
                        case ApolloShelfType.HangingBar:
                            angle = SpacePlanningImportHelper.ConvertToRadians((Single)SpacePlanningImportHelper.ParseSingle(data.SH_RESERVED1, false, out convertSuccess) - 90);



                            zPos -= (Single)(((Single)SpacePlanningImportHelper.ParseSingle(data.SH_SHELFDEPTH, false, out convertSuccess) / 2) * Math.Sin(angle));
                            xPos -= (Single)(((Single)SpacePlanningImportHelper.ParseSingle(data.SH_SHELFDEPTH, false, out convertSuccess) / 2) * Math.Cos(angle));

                            break;
                    }
                    if (_yPos.HasValue)
                    {
                        _yPos = Math.Min(_yPos.Value, yPos);
                    }
                    else
                    {
                        _yPos = yPos;
                    }
                    if (_zPos.HasValue)
                    {
                        _zPos = Math.Min(_zPos.Value, zPos);
                    }
                    else
                    {
                        _zPos = zPos;
                    }


                    x2 = Math.Max(xPos + DalCacheItem.DecodeShelfWidth(data, componentType, uom), x2);
                    y2 = Math.Max(yPos + DalCacheItem.DecodeShelfHeight(data, componentType, uom), y2);
                    z2 = Math.Max(zPos + DalCacheItem.DecodeShelfDepth(data, componentType, uom), z2);

                    if ((_componentType == ApolloShelfType.Box || _componentType == ApolloShelfType.BoundingBox || _componentType == ApolloShelfType.FreeHandSurface) &&
                        (componentType != ApolloShelfType.Box || _componentType != ApolloShelfType.BoundingBox || _componentType != ApolloShelfType.FreeHandSurface))
                    {
                        _componentType = componentType;
                    }
                }
            }

            _height = y2 - YPos;
            _width = x2 - XPos;
            _depth = z2 - ZPos;

            if (shelves.Count() == 1)
            {
                _angle = angle;
                _slope = slope;
                _rotateSubComponents = false;
                _componentType = componentType;
            }
        }
    }
}
