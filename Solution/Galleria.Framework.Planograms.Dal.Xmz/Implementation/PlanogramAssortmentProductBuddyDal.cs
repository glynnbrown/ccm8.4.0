﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// CCM-31550 : A.Probyn
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class PlanogramAssortmentProductBuddyDal : DalBase, IPlanogramAssortmentProductBuddyDal
    {
        #region Fetch

        public IEnumerable<PlanogramAssortmentProductBuddyDto> FetchByPlanogramAssortmentId(object id)
        {
            return new List<PlanogramAssortmentProductBuddyDto>();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramAssortmentProductBuddyDto dto)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        public void Insert(IEnumerable<PlanogramAssortmentProductBuddyDto> dtos)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        #endregion

        #region Update

        public void Update(PlanogramAssortmentProductBuddyDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramAssortmentProductBuddyDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramAssortmentProductBuddyDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramAssortmentProductBuddyDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
