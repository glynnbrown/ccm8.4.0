﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
// V8-31456 : N.Foster
//  Added method to retrieve batch of custom attribute data
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Xmz.Resources;

namespace Galleria.Framework.Planograms.Dal.Xmz.Implementation
{
    public class CustomAttributeDataDal : DalBase, ICustomAttributeDataDal
    {
        #region Fetch

        public CustomAttributeDataDto FetchByParentTypeParentId(byte parentType, object parentId)
        {
            foreach (CustomAttributeDataDto dto in this.DalContext.DalCache.CustomAttributeDataDtoList)
            {
                if (dto.ParentType == parentType && dto.ParentId.Equals(parentId))
                {
                    return Clone(dto);
                }
            }

            return new CustomAttributeDataDto() { Id = IdentityHelper.GetNextInt32(), ParentType = parentType, ParentId = parentId };
        }

        public IEnumerable<CustomAttributeDataDto> FetchByParentTypeParentIds(Byte parentType, IEnumerable<Object> parentIds)
        {
            HashSet<Object> parentLookup = new HashSet<Object>();
            foreach (Object parentId in parentIds) parentLookup.Add(parentId);

            List<CustomAttributeDataDto> dtos = new List<CustomAttributeDataDto>();
            foreach (CustomAttributeDataDto dto in this.DalContext.DalCache.CustomAttributeDataDtoList)
            {
                if ((dto.ParentType == parentType) && (parentLookup.Contains(dto.ParentId)))
                {
                    dtos.Add(Clone(dto));
                }
            }
            return dtos;
        }

        #endregion

        #region Insert

        public void Insert(CustomAttributeDataDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalContext.DalCache.CustomAttributeDataDtoList.Add(dto);
        }

        public void Insert(IEnumerable<CustomAttributeDataDto> dtos)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            foreach (CustomAttributeDataDto dto in dtos)
            {
                DalContext.DalCache.CustomAttributeDataDtoList.Add(dto);
            }
        }

        #endregion

        #region Update

        public void Update(CustomAttributeDataDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<CustomAttributeDataDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Upsert

        public void Upsert(IEnumerable<CustomAttributeDataDto> dtoList, CustomAttributeDataIsSetDto isSetDto)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(CustomAttributeDataDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<CustomAttributeDataDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
