﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
// V8-31175 : A.Silva
//  Amended the constructor to be able to determine the correct file path when a planogram name has been appended to the dal factory name.
#endregion

#region Version History: CCM830
// V8-31547 : M.Pettit
//  Added ExportPackage() method
//  Added OldFileBackupPath property and backup file management methods 
//      (CreateExportFileBackup, DeleteExportFileBackup, RestoreExportFileBackup)
// V8-32388 : M.Pettit
//  Added PlanogramFixtureAssemblyDtoList, PlanogramAssemblyComponentDtoList to cache properties
#endregion

#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Xceed.Zip;
using Xceed.FileSystem;
using System.Diagnostics;
using System.IO;
using Galleria.Framework.Planograms.External;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Dal.Xmz
{
    /// <summary>
    /// The DalCache is used to store DTO instances in memory so that they can be fetched/inserted/updated quickly and
    /// so that DAL operations can be commited to the underlying POG file only when a DalContext transaction is 
    /// committed.
    /// </summary>
    public class DalCache : IDisposable
    {
        #region Fields
        private Object _lock = new object(); // object used for locking
        private String _filePath; // holds the full file path
        private MemoryFolder _memoryFolder; // the folder in memory holding any xml files
        private List<DalCacheItem> _packageItems; // holds the root package item
        //private ReadOnlyCollection<PlanogramFieldMappingDto> _fieldMappings; //holds the customisable field mappings to be applied
        //private ReadOnlyCollection<PlanogramMetricMappingDto> _metricMappings; //holds the customisable field mappings to be applied
        private External.ImportContext _context;
        private Boolean _readOnly; // indicates if the file should be opened read only (i.e. we are importing) or as read-write (export)
        private Boolean _loadCacheData = true;
        #endregion

        #region Export-Specific Fields
        Stream _zipStream = null;
        ZipArchive _zipArchive = null;
        private External.ExportContext _exportContext;
        #endregion

        #region Properties
        /// <summary>
        /// Returns the path to the cache file
        /// </summary>
        public String FilePath
        {
            get { return _filePath; }
        }

        /// <summary>
        /// Returns the path to the cache file
        /// </summary>
        public String FileNameWithoutExtension
        {
            get 
            {
                return System.IO.Path.GetFileNameWithoutExtension(_filePath); 
            }
        }

        /// <summary>
        /// Returns the root package item
        /// </summary>
        public List<DalCacheItem> PackageItems
        {
            get
            {
                //// create the package item if required
                //if (_packageItems == null)
                //    LoadCacheData();

                // return the package item
                return _packageItems;
            }
        }

        public External.ImportContext Context
        {
            get
            {
                return _context;
            }
            set
            {
                _context = value;
            }
        }

        /// <summary>
        /// Context used by exports to provide field mappings
        /// </summary>
        public External.ExportContext ExportContext
        {
            get
            {
                return _exportContext;
            }
            set
            {
                _exportContext = value;
            }
        }

        #endregion

        #region Cache Properties

        #region Package
        /// <summary>
        /// PackageDtoList
        /// </summary>
        private List<PackageDto> _packageDtoList = new List<PackageDto>();
        public List<PackageDto> PackageDtoList
        {
            get { return _packageDtoList; }
        }

        #endregion

        #region Planogram
        /// <summary>
        /// PlanogramDtoList
        /// </summary>
        private List<PlanogramDto> _planogramDtoList = new List<PlanogramDto>();
        public List<PlanogramDto> PlanogramDtoList
        {
            get { return _planogramDtoList; }
        }

        #endregion

        #region PlanogramFixtureItem
        /// <summary>
        /// PlanogramFixtureItemDtoList
        /// </summary>
        private List<PlanogramFixtureItemDto> _planogramFixtureItemDtoList = new List<PlanogramFixtureItemDto>();
        public List<PlanogramFixtureItemDto> PlanogramFixtureItemDtoList
        {
            get { return _planogramFixtureItemDtoList; }
        }

        #endregion

        #region PlanogramFixture
        /// <summary>
        /// PlanogramFixtureDtoList
        /// </summary>
        private List<PlanogramFixtureDto> _planogramFixtureDtoList = new List<PlanogramFixtureDto>();
        public List<PlanogramFixtureDto> PlanogramFixtureDtoList
        {
            get { return _planogramFixtureDtoList; }
        }

        #endregion

        #region PlanogramFixtureComponent
        /// <summary>
        /// PlanogramFixtureComponentDtoList
        /// </summary>
        private List<PlanogramFixtureComponentDto> _planogramFixtureComponentDtoList = new List<PlanogramFixtureComponentDto>();
        public List<PlanogramFixtureComponentDto> PlanogramFixtureComponentDtoList
        {
            get { return _planogramFixtureComponentDtoList; }
        }

        #endregion

        #region PlanogramComponent
        /// <summary>
        /// PlanogramComponentDtoList
        /// </summary>
        private List<PlanogramComponentDto> _planogramComponentDtoList = new List<PlanogramComponentDto>();
        public List<PlanogramComponentDto> PlanogramComponentDtoList
        {
            get { return _planogramComponentDtoList; }
        }

        #endregion

        #region PlanogramSubComponent
        /// <summary>
        /// PlanogramSubComponentList
        /// </summary>
        private List<PlanogramSubComponentDto> _planogramSubComponentList = new List<PlanogramSubComponentDto>();
        public List<PlanogramSubComponentDto> PlanogramSubComponentDtoList
        {
            get { return _planogramSubComponentList; }
        }

        #endregion

        #region PlanogramProduct
        /// <summary>
        /// PlanogramDtoList
        /// </summary>
        private List<PlanogramProductDto> _planogramProductDtoList = new List<PlanogramProductDto>();
        public List<PlanogramProductDto> PlanogramProductDtoList
        {
            get { return _planogramProductDtoList; }
        }

        #endregion

        #region PlanogramPerformanceData
        /// <summary>
        /// PlanogramPerformanceData
        /// </summary>
        private List<PlanogramPerformanceMetricDto> _planogramPerformanceMetricDtoList = new List<PlanogramPerformanceMetricDto>();
        public List<PlanogramPerformanceMetricDto> PlanogramPerformanceMetricDtoList
        {
            get { return _planogramPerformanceMetricDtoList; }
        }

        #endregion

        #region PlanogramPerformanceData
        /// <summary>
        /// PlanogramPerformanceData
        /// </summary>
        private List<PlanogramPerformanceDataDto> _planogramPerformanceDataDtoList = new List<PlanogramPerformanceDataDto>();
        public List<PlanogramPerformanceDataDto> PlanogramPerformanceDataDtoList
        {
            get { return _planogramPerformanceDataDtoList; }
        }

        #endregion

        #region PlanogramPosition
        /// <summary>
        /// PlanogramPositionDtoList
        /// </summary>
        private List<PlanogramPositionDto> _planogramPositionDtoList = new List<PlanogramPositionDto>();
        public List<PlanogramPositionDto> PlanogramPositionDtoList
        {
            get { return _planogramPositionDtoList; }
        }

        #endregion

        #region PlanogramAnnotation
        /// <summary>
        /// PlanogramAnnotationDtoList
        /// </summary>
        private List<PlanogramAnnotationDto> _planogramAnnotationDtoList = new List<PlanogramAnnotationDto>();
        public List<PlanogramAnnotationDto> PlanogramAnnotationDtoList
        {
            get { return _planogramAnnotationDtoList; }
        }
        #endregion

        #region PlanogramConsumerDecisionTree
        /// <summary>
        /// PlanogramConsumerDecisionTreeDtoList
        /// </summary>
        private List<PlanogramConsumerDecisionTreeDto> _planogramConsumerDecisionTreeDtoList = new List<PlanogramConsumerDecisionTreeDto>();
        public List<PlanogramConsumerDecisionTreeDto> PlanogramConsumerDecisionTreeDtoList
        {
            get { return _planogramConsumerDecisionTreeDtoList; }
        }
        #endregion

        #region PlanogramConsumerDecisionTreeLevel
        /// <summary>
        /// PlanogramConsumerDecisionTreeLevelDtoList
        /// </summary>
        private List<PlanogramConsumerDecisionTreeLevelDto> _planogramConsumerDecisionTreeLevelDtoList = new List<PlanogramConsumerDecisionTreeLevelDto>();
        public List<PlanogramConsumerDecisionTreeLevelDto> PlanogramConsumerDecisionTreeLevelDtoList
        {
            get { return _planogramConsumerDecisionTreeLevelDtoList; }
        }
        #endregion

        #region PlanogramConsumerDecisionTreeNode
        /// <summary>
        /// PlanogramConsumerDecisionTreeNodeDtoList
        /// </summary>
        private List<PlanogramConsumerDecisionTreeNodeDto> _planogramConsumerDecisionTreeNodeDtoList = new List<PlanogramConsumerDecisionTreeNodeDto>();
        public List<PlanogramConsumerDecisionTreeNodeDto> PlanogramConsumerDecisionTreeNodeDtoList
        {
            get { return _planogramConsumerDecisionTreeNodeDtoList; }
        }
        #endregion

        #region PlanogramEventLog
        /// <summary>
        /// PlanogramEventLogDtoList
        /// </summary>
        private List<PlanogramEventLogDto> _planogramEventLogDtoList = new List<PlanogramEventLogDto>();
        public List<PlanogramEventLogDto> PlanogramEventLogDtoList
        {
            get { return _planogramEventLogDtoList; }
        }
        #endregion

        #region CustomAttributeData
        /// <summary>
        /// CustomAttributeDataDtoList
        /// </summary>
        private List<CustomAttributeDataDto> _customAttributeDataDtoList = new List<CustomAttributeDataDto>();
        public List<CustomAttributeDataDto> CustomAttributeDataDtoList
        {
            get { return _customAttributeDataDtoList; }
        }
        #endregion

        #region PlanogramFixtureAssembly
        /// <summary>
        /// PlanogramFixtureAssemblyDtoList
        /// </summary>
        private List<PlanogramFixtureAssemblyDto> _planogramFixtureAssemblyDtoList = new List<PlanogramFixtureAssemblyDto>();
        public List<PlanogramFixtureAssemblyDto> PlanogramFixtureAssemblyDtoList
        {
            get { return _planogramFixtureAssemblyDtoList; }
        }

        #endregion

        #region PlanogramAssemblyComponent
        /// <summary>
        /// PlanogramAssemblyComponentDtoList
        /// </summary>
        private List<PlanogramAssemblyComponentDto> _planogramAssemblyComponentDtoList = new List<PlanogramAssemblyComponentDto>();
        public List<PlanogramAssemblyComponentDto> PlanogramAssemblyComponentDtoList
        {
            get { return _planogramAssemblyComponentDtoList; }
        }

        #endregion

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public DalCache(DalFactoryConfigElement dalFactoryConfig)
        {
            _filePath = dalFactoryConfig.Name;
            Int32 planNameIndex = _filePath.IndexOf("::", StringComparison.InvariantCultureIgnoreCase);
            if (planNameIndex == -1) return;

            //  Split the plan name and the real file path.
            _filePath = _filePath.Substring(0, planNameIndex);
        }
        #endregion

        #region Import-Specific Methods

        private void LoadCacheData()
        {
            lock (_lock)
            {
                if (_packageItems == null)
                {
                    _packageItems = new List<DalCacheItem>();

                    //make sure we've opened our folder
                    if (_memoryFolder == null) this.Open(_readOnly);

                    foreach (AbstractFile file in _memoryFolder.GetFiles(true))
                    {
                        _packageItems.Add(new DalCacheItem(this, file, _filePath));
                    }

                    //tidy up the memory folder
                    if (_memoryFolder != null && _memoryFolder.Exists) _memoryFolder.Delete();
                    _memoryFolder = null;

                }
            }
        }

        #endregion

        #region Export-Specific Methods

        

        public void ExportPackage()
        {
            PackageDto packageDto = this.PackageDtoList.FirstOrDefault();

            if (packageDto == null) return;

            if (_zipStream == null) return; //The file was not locked so we cannot continue

            lock (_lock)
            {
                try
                {
                    //Get the export context - this holds th field mappings to use for the export
                    if (packageDto.FieldMappings != null)
                    {
                        this.ExportContext = packageDto.FieldMappings as External.ExportContext;
                    }

                    Debug.Assert(this.ExportContext != null, "Export must have a context to provide the field mappings");

                    //create dalcache item
                    DalCacheItem exportItem = new DalCacheItem(this, packageDto, _filePath);

                    //Get the name of the internal xml file that will be created in the zip file
                    String planFileName = String.Format("{0}.xml", System.IO.Path.GetFileNameWithoutExtension(_filePath));

                    //Create and open a new plan file in the zip archive
                    AbstractFile planFile = _zipArchive.GetFile(planFileName);

                    //Close the stream to the zip archive as we need to edit it
                    UnlockExportZipArchive();

                    //Create the plan file - this adds the physical file to the zip archive
                    planFile.Create();

                    using (Stream planStream = planFile.OpenWrite(true, FileShare.None))
                    {
                        //Export the file to the stream
                        exportItem.SerializePlanToStream(planStream);

                        //Close the stream to finalize the operation
                        planStream.Close();
                    }

                    //Delete the old backup file as it is no longer required
                    SpacePlanningExportHelper.DeleteExportFileBackup(_filePath);

                    ClearDtos();
                }
                catch (Exception ex)
                {
                    UnlockExportZipArchive();
                    //Restore the original file, if any, to its location
                    SpacePlanningExportHelper.RestoreExportFileBackup(_filePath);

                    ClearDtos();

                    if (this.ExportContext.ViewErrorLogsOnCompletion)
                    {
                        //Log Errors
                        SpacePlanningExportHelper.CreateErrorEventLogEntry(this.FilePath, this.ExportContext.ExportEvents, ex.Source, ex.Message, PlanogramEventLogAffectedType.Planogram);
                    }
                    else
                    {
                        //re-throw the error so it is handled normally
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Creates a stream on the export zip file to lock it from unwanted actions
        /// while processing the internal xml file which holds the Apollo file data
        /// </summary>
        public void LockExportZipArchive()
        {
            //Open a stream to lock our zip file
            _zipStream = _zipArchive.ZipFile.CreateWrite(FileShare.None);
        }

        /// <summary>
        /// Unlock the parent zip archive file to allow users to access it
        /// </summary>
        public void UnlockExportZipArchive()
        {
            if (_zipStream != null)
            {
                _zipStream.Close();
                _zipStream = null;
            }
        }

        #endregion

        #region Logging Methods
        /// <summary>
        /// Logs a warning message to the Export Context
        /// </summary>
        /// <param name="description">The header</param>
        /// <param name="content">The content</param>
        /// <param name="affectedType">The type of object to which the message refers</param>
        internal void LogExportWarning(String description, String content, PlanogramEventLogAffectedType affectedType)
        {
            LogExportMessage(description, content, affectedType, PlanogramEventLogEntryType.Warning); 
        }

        /// <summary>
        /// Logs an information message to the Export Context
        /// </summary>
        /// <param name="description">The header</param>
        /// <param name="content">The content</param>
        /// <param name="affectedType">The type of object to which the message refers</param>
        internal void LogExportInformation(String description, String content, PlanogramEventLogAffectedType affectedType)
        {
            LogExportMessage(description, content, affectedType, PlanogramEventLogEntryType.Information); 
        }

        /// <summary>
        /// Logs an information message to the Export Context
        /// </summary>
        /// <param name="description">The header</param>
        /// <param name="content">The content</param>
        /// <param name="affectedType">The type of object to which the message refers</param>
        internal void LogExportError(String description, String content, PlanogramEventLogAffectedType affectedType)
        {
            LogExportMessage(description, content, affectedType, PlanogramEventLogEntryType.Error);
        }

        /// <summary>
        /// Logs an information message to the Export Context
        /// </summary>
        /// <param name="description">The header</param>
        /// <param name="content">The content</param>
        /// <param name="affectedType">The type of object to which the message refers</param>
        internal void LogExportMessage(String description, String content, PlanogramEventLogAffectedType affectedType, PlanogramEventLogEntryType type)
        {
            SpacePlanningExportHelper.CreateEventLogEntry(this.FilePath, this.ExportContext.ExportEvents, description, content, type, affectedType);
        }
        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is disposed
        /// </summary>
        public void Dispose()
        {
            this.Close();
        }

        /// <summary>
        /// Opens and locks the file ready for reading
        /// </summary>
        public void Open(Boolean readOnly)
        {
            lock (_lock)
            {
                // remember that the file is readonly
                _readOnly = readOnly;

                if (_readOnly)
                {
                    #region We are importing a file:

                    // attempt open the file stream
                    if (_memoryFolder == null)
                    {
                        // determine if the file already exists
                        // Boolean isNew = !File.Exists(_filePath);

                        // open the file stream
                        Xceed.Zip.Licenser.LicenseKey = "ZIN33BE14GWN1Z8NH2A";
                        _memoryFolder = new MemoryFolder("RAM_Drive", Guid.NewGuid().ToString());

                        ZipArchive zip = new ZipArchive(new DiskFile(_filePath));
                        zip.CopyFilesTo(_memoryFolder, true, true);

                        //LoadCacheData();
                    }
                    #endregion
                }
                else
                {
                    #region we are exporting to a file:

                    //create a backup of the existing file, if any
                    SpacePlanningExportHelper.CreateExportFileBackup(_filePath);

                    //Create the empty zip file that will become our Xmz file
                    //We can only creat this once the original file has been backed up, so there is a possiblility
                    //of a concurrency issue here..
                    Xceed.Zip.Licenser.LicenseKey = "ZIN33BE14GWN1Z8NH2A";
                    _zipArchive = new ZipArchive(new DiskFile(_filePath));
                    //and lock it to prevent other users accessing/changing the file while we are working
                    LockExportZipArchive();
                    #endregion
                }
            }
        }

        /// <summary>
        /// Closes and releases the cached file
        /// </summary>
        public void Close()
        {
            lock (_lock)
            {
                //Clear import-related objects

                // dispose of the file stream
                if (_memoryFolder != null && _memoryFolder.Exists) _memoryFolder.Delete();
                _memoryFolder = null;

                // dispose of the cache items
                if (_packageItems != null)
                {
                    _packageItems.ForEach(i => i.Dispose());                    
                }
                _packageItems = null;

                ClearDtos();

                //Clear export-related objects
                UnlockExportZipArchive();
                if (_zipArchive != null) _zipArchive = null;
            }
        }

        private void ClearDtos()
        {
            _packageDtoList.Clear();
            _planogramDtoList.Clear();
            _planogramFixtureItemDtoList.Clear();
            _planogramFixtureDtoList.Clear();
            _planogramFixtureComponentDtoList.Clear();
            _planogramFixtureAssemblyDtoList.Clear();
            _planogramAssemblyComponentDtoList.Clear();
            _planogramComponentDtoList.Clear();
            _planogramSubComponentList.Clear();
            _planogramProductDtoList.Clear();
            _planogramPositionDtoList.Clear();
            _planogramAnnotationDtoList.Clear();
            _planogramConsumerDecisionTreeDtoList.Clear();
            _planogramConsumerDecisionTreeLevelDtoList.Clear();
            _planogramConsumerDecisionTreeNodeDtoList.Clear();
            _planogramEventLogDtoList.Clear();
            _customAttributeDataDtoList.Clear();
            _planogramPerformanceDataDtoList.Clear();
            _planogramPerformanceMetricDtoList.Clear();
        }

        public void RefreshCache()
        {
            // open the file stream
            if (_packageItems != null)
            {
                ClearDtos();
                foreach (DalCacheItem item in this.PackageItems)
                {
                    item.LoadDtos();
                }
            }
            else
            {
                LoadCacheData();
            }
        }

        #endregion
    }
}
