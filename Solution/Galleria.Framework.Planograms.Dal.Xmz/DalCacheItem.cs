﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//  Created
// V8-30936 : M.Brumby
//  Added slotwall
// V8-31047 : M.Brumby
//  Top caps no longer get units wide if they have no units high or deep
// V8-31052 : M.Brumby
//  Tray values now set if the product has a position that is tray merchandised
// V8-31053 : M.Brumby
//  X-gap mapping added which roughly translates to "Finger Space to the Side"
// V8-31152 : M.Brumby
//  Make use of some generic helpers
// V8-31173 : M.Brumby
//  Corrected non peg pegY positons so they are 0
// V8-31196 : M.Brumby
//  Pegboards and Slotwalls now import with correct facings wide
// V8-31189 : A.Silva
//  Added file version check and warning to LoadDtos().
// v8-31256 : M.Brumby
//  Component sizes calculated correctly so that notch numbers are cacluated correctly
// V8-31285 : M.Brumby
//  Better Gtin handling
// V8-31349 : M.Brumby
//  Risers are no longer transparent and component borders go throught the default UOM code
// V8-31395 : M.Brumby
//  Check Supported versions for versions instead of available versions
// V8-31474 : M.Brumby
//  Duplicate custom attributes no longer created.
#endregion
#region Version History: CCM830
// V8-31531 : A.Heathcote
//  Removed IsTrayProduct 
// V8-31547 : M.Pettit
//  Added ExportToFile functionality
// V8-31444 : M.Brumby
//  Export Plan: Import merch depth
// V8-31431 : M.Brumby
//  Export Plan: Ensure tray count if tray
// V8-32388 : M.Pettit
//  Export Plan: Now supports Assembly component exports
// V8-32452 : M.Pettit
//  Export Plan: Added more mapping fields
// V8-32623 : M.Pettit
//  Export Plan: Added mapping fields for section product sell/retail price
// V8-32620 : M.Pettit
//  Export Plan: Case calculations PR_CASEHEIGHT/WIDTH/DEPTH now take account of multi-sited positions
// V8-32606 : M.Pettit
//  Import Plan: Applied squeeze factors to imported positions from XGap values
// V8-32638 : M.Pettit
//  Import Plan: Turned on IsProductSqueezeAllowed for all components on importing
// V8-32738 : M.Pettit
//  Export Plan: Added missing SECTIONDESCX?? fields to list of mappable fields
//  Import Plan: Added missing SECTIONDESCX?? fields to list of mappable fields
// CCM-18559 : M.Pettit
//  SE_RESERVED5 is now set to enable the Items Span Shelves option
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Model;
using System.Linq;
using System.Reflection;
using Galleria.Framework.Planograms.External;
using Xceed.FileSystem;
using System.Xml;
using System.Xml.Serialization;
using Galleria.Framework.Planograms.Dal.Xmz.Implementation.Items;
using Galleria.Framework.Planograms.Dal.Xmz.Resources;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Planograms.Helpers;
using Xceed.Zip;
using Galleria.Framework.Planograms.External.ApolloStructure;
using Galleria.Framework.Planograms.Merchandising;

namespace Galleria.Framework.Planograms.Dal.Xmz
{
    /// <summary>
    /// Items used to cache data within the dal
    /// </summary>
    public class DalCacheItem : IDisposable
    {
        #region Enums

        public enum UOM
        {
            Metric = 0,
            Imperial = 1,
            Unknown
        }

        #endregion

        #region Fields
        private Guid planogramId = Guid.NewGuid();
        private DalCache _dalCache;
        private static Object _nextIdLock = new Object(); // object used for locking
        private static Int32 _nextId = 0; // holds the next id number
        private String _id; // holds the item id
        private ApolloDataSet _originalDataSet; //the original dataset info
        private UOM _uom = UOM.Metric;

        private ApolloDataSet _exportDataSet;
        private Boolean _exportRetainSourcePlanLookAndFeel = false;
        private Boolean _exportIncludePerformanceData = false;
        #endregion

        #region Properties

        /// <summary>
        /// Returns the item id
        /// </summary>
        public String Id
        {
            get { return _id; }
        }

        /// <summary>
        /// Returns the parent dalcache
        /// </summary>
        public DalCache DalCache
        {
            get { return _dalCache; }
        }

        /// <summary>
        /// if true, the exported plan should try to look as close as possible to the source V8 planogram. 
        /// For example, It should contain extra positions to cater for right caps, which Apollo does not support.
        /// </summary>
        public Boolean RetainSourcePlanLookAndFeel
        {
            get { return _exportRetainSourcePlanLookAndFeel; }
        }

        /// <summary>
        /// Determines whether the exported plan should contain performance data
        /// </summary>
        public Boolean IncludePerformanceData
        {
            get { return _exportIncludePerformanceData; }
        }
        #endregion

        #region Helper Properties

        /// <summary>
        /// 2 cm
        /// </summary>
        private const Single DefaultThicknessMedium = 2F;
       
        /// <summary>
        /// 1 cm 
        /// </summary>
        private const Single DefaultThicknessNormal = 1F;
        
        /// <summary>
        /// 0.5 cm 
        /// </summary>
        private const Single DefaultThicknessSmall = 0.5F;
       
        /// <summary>
        /// 0.01 cm 
        /// </summary>
        private const Single DefaultThicknessTiny = 0.01F;
        
        /// <summary>
        /// 0.5 cm
        /// </summary>
        private const Single DefaultPegSize = 0.2F;
        
        public static Single ConvertUOM(Single value, UOM valueUom, UOM targetUom)
        {
            if (valueUom == UOM.Metric)
            {
                switch (targetUom)
                {
                    case UOM.Imperial:
                        return (Single)(value * 0.393700787);
                    case UOM.Metric:
                    case UOM.Unknown:
                    default:
                        return value;
                }
            }
            else if (valueUom == UOM.Imperial)
            {
                switch (targetUom)
                {
                    case UOM.Metric:
                        return (Single)(value * 2.54);
                    case UOM.Imperial:
                    case UOM.Unknown:
                    default:
                        return value;
                }
            }

            return value;
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public DalCacheItem(DalCache dalCache, AbstractFile file, String id)
        {
            _dalCache = dalCache;
            _id = id;

            try
            {
                DeserializePlan(file);
            }
            catch
            {
                XmlDeserializePlan(file);                
            }
            
            
            if (dalCache.Context != null)
            {
                this.LoadDtos();
            }
        }

        /// <summary>
        /// Constructor for exporting a package to an Apollo file
        /// </summary>
        /// <param name="dalCache"></param>
        /// <param name="packageDto"></param>
        /// <param name="id"></param>
        public DalCacheItem(DalCache dalCache, PackageDto packageDto, String filePath)
        {
            _dalCache = dalCache;
            _id = filePath;
            _exportRetainSourcePlanLookAndFeel = dalCache.ExportContext.RetainSourcePlanLookAndFeel;
            _exportIncludePerformanceData = dalCache.ExportContext.IncludePerformanceData;

            //create the dataset for exporting
            LoadApolloDataSetForExport();
        }

        #endregion

        #region Encoding/Decoding

        #region Package
        /// <summary>
        /// Decodes a package dto
        /// </summary>
        private PackageDto DecodePackageDto(String id)
        {

            ApolloDataSetInfo info = (this._originalDataSet.Items.First(f => f is ApolloDataSetInfo) as ApolloDataSetInfo);

            String name = null;

            if(info != null)
            {
                name = info.ApolloVersion;
            }

            if (name == null || name.Length == 0)
            {
                name = Language.DefaultPlanName;
            }

            return new PackageDto()
            {
                Id = id,
                Name = name                
            };
        }

        /// <summary>
        /// Encodes a package dto
        /// </summary>
        private static String EncodePackageDto(PackageDto dto)
        {
            return null;
        }
        #endregion

        #region Planogram
        /// <summary>
        /// Decodes a planogram dto
        /// </summary>
        private PlanogramDto DecodePlanogramDto(String id, ApolloDataSetInfo info, ApolloDataSetSection section, ApolloDataSetStore store, ApolloDataSetRetailer retailer, IEnumerable<PlanogramFieldMappingDto> fieldMappings)
        {
            Boolean convertSuccess = false;
            UOM uom = UOM.Unknown;
            if (!Enum.TryParse<UOM>(info.Measure, out uom)) uom = UOM.Unknown;
            _uom = uom;
            ApolloDataSetSectionSectionData primarySection = section.SectionData.First();
            Single uprightSpacing = (Single)SpacePlanningImportHelper.ParseSingle(primarySection.SE_UPRIGHT, false, out convertSuccess);


            Single width = section.SectionData.Sum(s => (Single)SpacePlanningImportHelper.ParseSingle(s.SE_WIDTH, false, out convertSuccess));
            Int32 bayCount = 1;

            if (uprightSpacing > 0)
            {
                bayCount = (Int32)(width / uprightSpacing);
            }

            PlanogramDto planogramDto = new PlanogramDto()
            {
                Id = planogramId,
                PackageId = id,
                UniqueContentReference = Guid.NewGuid().ToString(),
                Name = Path.GetFileNameWithoutExtension(id),
                Height = section.SectionData.Max(s => (Single)SpacePlanningImportHelper.ParseSingle(s.SE_HEIGHT, false, out convertSuccess)),
                Width = width,
                Depth = section.SectionData.Max(s => (Single)SpacePlanningImportHelper.ParseSingle(s.SE_DEPTH, false, out convertSuccess)),
                ProductPlacementX = (Byte)PlanogramProductPlacementXType.Manual,
                ProductPlacementY = (Byte)PlanogramProductPlacementYType.Manual,
                ProductPlacementZ = (Byte)PlanogramProductPlacementZType.Manual,
                SourcePath = id,
                VolumeUnitsOfMeasure = (Byte)ConvertUnitsOfMeasureType_Volume(uom),
                WeightUnitsOfMeasure = (Byte)ConvertUnitsOfMeasureType_Weight(uom),
                AreaUnitsOfMeasure = (Byte)ConvertUnitsOfMeasureType_Area(uom),
                LengthUnitsOfMeasure = (Byte)ConvertUnitsOfMeasureType_Length(uom),
                MetaBayCount = bayCount,
            };

            if (fieldMappings != null)
            {
                MapCustomAttributes(planogramDto, fieldMappings, info, CustomAttributeDataPlanogramParentType.Planogram, planogramDto.Id, PlanogramEventLogAffectedType.Planogram);
                MapCustomAttributes(planogramDto, fieldMappings, store, CustomAttributeDataPlanogramParentType.Planogram, planogramDto.Id, PlanogramEventLogAffectedType.Planogram);
                MapCustomAttributes(planogramDto, fieldMappings, retailer, CustomAttributeDataPlanogramParentType.Planogram, planogramDto.Id, PlanogramEventLogAffectedType.Planogram);
                MapCustomAttributes(planogramDto, fieldMappings, primarySection, CustomAttributeDataPlanogramParentType.Planogram, planogramDto.Id, PlanogramEventLogAffectedType.Planogram);
            }

            return planogramDto;
        }

        /// <summary>
        /// Encodes a planogram dto
        /// </summary>
        private static String EncodePlanogramDto(PlanogramDto dto)
        {
            return null;
        }
        #endregion

        #region PlanogramFixtureItem
        /// <summary>
        /// Decodes a planogram Fixture Item dto
        /// </summary>
        private PlanogramFixtureItemDto DecodePlanogramFixtureItemDto(ApolloDataSetSectionSectionData sectionData)
        {
            Boolean convertSuccess = false;
            return new PlanogramFixtureItemDto()
            {
                Id = sectionData.GetHashCode(),
                PlanogramId = planogramId,
                PlanogramFixtureId = sectionData.GetHashCode(),
                X = (Single)SpacePlanningImportHelper.ParseSingle(sectionData.SE_XPOS, false, out convertSuccess),
                Y = (Single)SpacePlanningImportHelper.ParseSingle(sectionData.SE_YPOS, false, out convertSuccess),
                Z = (Single)SpacePlanningImportHelper.ParseSingle(sectionData.SE_ZPOS, false, out convertSuccess),
                Angle = 0,
                Slope = 0,
              //  BaySequenceNumber = GetInt16Value(fixelData, schema, SpacemanFieldHelper.FixelSegment, DalCacheItemType.Fixel)
            };
        }

        /// <summary>
        /// Encodes a planogram dto
        /// </summary>
        private static String EncodePlanogramFixtureItemDto(PlanogramDto dto)
        {
            return null;
        }

        
     
        private void CreateBackboard(ApolloDataSetSectionSectionData sectionData)
        {
            Boolean convertSuccess = false;
            Int32 segment = sectionData.GetHashCode();
            String componentId = GetNextId();
            PlanogramFixtureComponentDto fcDto = new PlanogramFixtureComponentDto()
            {
                Id = componentId,
                PlanogramComponentId = componentId,
                PlanogramFixtureId = segment,
                X = 0,
                Y = 0,
                Z = -ConvertUOM(DefaultThicknessNormal, UOM.Metric, _uom),
                Slope = 0,
                Angle = 0,
                Roll = 0
            };
            PlanogramComponentDto cDto = new PlanogramComponentDto()
            {
                Id = componentId,
                PlanogramId = planogramId,
                Name = "Backboard",
                Height = (Single)SpacePlanningImportHelper.ParseSingle(sectionData.SE_HEIGHT, false, out convertSuccess),
                Width = (Single)SpacePlanningImportHelper.ParseSingle(sectionData.SE_WIDTH, false, out convertSuccess),
                Depth = ConvertUOM(DefaultThicknessNormal, UOM.Metric, _uom),
                ComponentType = (Byte)PlanogramComponentType.Backboard,
                IsMerchandisedTopDown = false
            };

            PlanogramSubComponentDto scDto = new PlanogramSubComponentDto()
            {
                Id = componentId,
                PlanogramComponentId = componentId,
                Name = "Backboard",
                ShapeType = (Byte)PlanogramSubComponentShapeType.Box,
                Height = (Single)SpacePlanningImportHelper.ParseSingle(sectionData.SE_HEIGHT, false, out convertSuccess),
                Width = (Single)SpacePlanningImportHelper.ParseSingle(sectionData.SE_WIDTH, false, out convertSuccess),
                Depth = ConvertUOM(DefaultThicknessNormal, UOM.Metric, _uom),
                MerchandisableHeight = 0,
                FillPatternTypeFront = (Byte)PlanogramSubComponentFillPatternType.Solid,
                FillPatternTypeBack = (Byte)PlanogramSubComponentFillPatternType.Solid,
                FillPatternTypeTop = (Byte)PlanogramSubComponentFillPatternType.Solid,
                FillPatternTypeBottom = (Byte)PlanogramSubComponentFillPatternType.Solid,
                FillPatternTypeLeft = (Byte)PlanogramSubComponentFillPatternType.Solid,
                FillPatternTypeRight = (Byte)PlanogramSubComponentFillPatternType.Solid,
                FillColourFront = -2894893,
                FillColourBack = -2894893,
                FillColourTop = -2894893,
                FillColourBottom = -2894893,
                FillColourLeft = -2894893,
                FillColourRight = -2894893,
                LineColour = -16777216,
                LineThickness = (Single)Math.Round(ConvertUOM(DefaultThicknessSmall, UOM.Metric, _uom), 2),
                NotchStartX = ConvertUOM(DefaultThicknessMedium, UOM.Metric, _uom),
                NotchSpacingX = ConvertUOM(DefaultThicknessMedium, UOM.Metric, _uom),
                NotchStartY = (Single)SpacePlanningImportHelper.ParseSingle(sectionData.SE_FIRSTNOTCH, false, out convertSuccess) > 0 ? (Single)SpacePlanningImportHelper.ParseSingle(sectionData.SE_FIRSTNOTCH, false, out convertSuccess) : ConvertUOM(DefaultThicknessNormal, UOM.Metric, _uom),
                NotchSpacingY = (Single)SpacePlanningImportHelper.ParseSingle(sectionData.SE_NOTCH, false, out convertSuccess) > 0 ? (Single)SpacePlanningImportHelper.ParseSingle(sectionData.SE_NOTCH, false, out convertSuccess) : DefaultThicknessMedium * 2,
                NotchHeight = ConvertUOM(DefaultThicknessNormal, UOM.Metric, _uom),
                NotchWidth = ConvertUOM(DefaultThicknessMedium, UOM.Metric, _uom),
                IsNotchPlacedOnFront = (Single)SpacePlanningImportHelper.ParseSingle(sectionData.SE_NOTCH, false, out convertSuccess) > 0 ? true : false,
                NotchStyleType = (Byte)PlanogramSubComponentNotchStyleType.SingleRectangle,
                MerchandisingType = (Byte)PlanogramSubComponentMerchandisingType.None
            };

            this._dalCache.PlanogramFixtureComponentDtoList.Add(fcDto);
            this._dalCache.PlanogramComponentDtoList.Add(cDto);
            this._dalCache.PlanogramSubComponentDtoList.Add(scDto);

        }

        private void CreateBase(ApolloDataSetSectionSectionData sectionData)
        {
            Boolean convertSuccess = false;
            Int32 fillColour = -5658199;
            Int32 lineColour = -16777216;
            Single lineThickness = 0.3F;
            Int32 segment = sectionData.GetHashCode();
            String componentId = GetNextId();
            PlanogramFixtureComponentDto fcDto = new PlanogramFixtureComponentDto()
            {
                Id = componentId,
                PlanogramComponentId = componentId,
                PlanogramFixtureId = segment,
                X = 0,
                Y = 0,
                Z = 0,
                Slope = 0,
                Angle = 0,
                Roll = 0 
            };
            PlanogramComponentDto cDto = new PlanogramComponentDto()
            {
                Id = componentId,
                PlanogramId = planogramId,
                Name = "Base",
                Height = ConvertUOM(DefaultThicknessNormal, UOM.Metric, _uom) * 10,
                Width = (Single)SpacePlanningImportHelper.ParseSingle(sectionData.SE_WIDTH, false, out convertSuccess),
                Depth = (Single)SpacePlanningImportHelper.ParseSingle(sectionData.SE_DEPTH, false, out convertSuccess),
                ComponentType = (Byte)PlanogramComponentType.Base,
                IsMerchandisedTopDown = false
            };

            PlanogramSubComponentDto scDto = new PlanogramSubComponentDto()
            {
                Id = componentId,
                PlanogramComponentId = componentId,
                Name = "Base",
                ShapeType = (Byte)PlanogramSubComponentShapeType.Box,
                Height = ConvertUOM(DefaultThicknessNormal, UOM.Metric, _uom) * 10,
                Width = (Single)SpacePlanningImportHelper.ParseSingle(sectionData.SE_WIDTH, false, out convertSuccess),
                Depth = (Single)SpacePlanningImportHelper.ParseSingle(sectionData.SE_DEPTH, false, out convertSuccess),
                MerchandisableHeight = 0,
                FillPatternTypeFront = (Byte)PlanogramSubComponentFillPatternType.Solid,
                FillPatternTypeBack = (Byte)PlanogramSubComponentFillPatternType.Solid,
                FillPatternTypeTop = (Byte)PlanogramSubComponentFillPatternType.Solid,
                FillPatternTypeBottom = (Byte)PlanogramSubComponentFillPatternType.Solid,
                FillPatternTypeLeft = (Byte)PlanogramSubComponentFillPatternType.Solid,
                FillPatternTypeRight = (Byte)PlanogramSubComponentFillPatternType.Solid,
                FillColourFront = fillColour,
                FillColourBack = fillColour,
                FillColourTop = fillColour,
                FillColourBottom = fillColour,
                FillColourLeft = fillColour,
                FillColourRight = fillColour,
                LineColour = lineColour,
                LineThickness = lineThickness
                
            };

            this._dalCache.PlanogramFixtureComponentDtoList.Add(fcDto);
            this._dalCache.PlanogramComponentDtoList.Add(cDto);
            this._dalCache.PlanogramSubComponentDtoList.Add(scDto);

        }

        #endregion

        #region PlanogramFixture
        /// <summary>
        /// Decodes a planogram Fixture dto
        /// </summary>
        private PlanogramFixtureDto DecodePlanogramFixtureDto(ApolloDataSetSectionSectionData sectionData, IEnumerable<PlanogramFieldMappingDto> fieldMappings)
        {
            Boolean convertSuccess = false;

            PlanogramFixtureDto planogramFixtureDto = new PlanogramFixtureDto()
            {
                Id = sectionData.GetHashCode(),
                PlanogramId = planogramId,
                Name = sectionData.SE_NAME,
                Height = (Single)SpacePlanningImportHelper.ParseSingle(sectionData.SE_HEIGHT, false, out convertSuccess),
                Width = (Single)SpacePlanningImportHelper.ParseSingle(sectionData.SE_WIDTH, false, out convertSuccess),
                Depth = (Single)SpacePlanningImportHelper.ParseSingle(sectionData.SE_DEPTH, false, out convertSuccess)
            };

            if (fieldMappings != null)
            {
                MapCustomAttributes(planogramFixtureDto, fieldMappings, sectionData, CustomAttributeDataPlanogramParentType.PlanogramProduct, planogramFixtureDto.Id, PlanogramEventLogAffectedType.Planogram);
            }
         
            return planogramFixtureDto;
        }

        /// <summary>
        /// Encodes a planogram fixture dto
        /// </summary>
        private static String EncodePlanogramFixtureDto(PlanogramFixtureDto dto)
        {
            return null;
        }
        #endregion

        #region PlanogramFixtureComponent
        /// <summary>
        /// Decodes a planogram fixture component dto
        /// </summary>
        private PlanogramFixtureComponentDto DecodePlanogramFixtureComponentDto(ApolloDataSetSectionSectionData sectionData, AdvancedApolloFixtureComponent component)
        {
            Boolean convertSuccess = false;
            Single xPos = component.XPos;
            Single yPos = component.YPos;
            Single zPos = component.ZPos;
            Single slope = component.Slope;
            Single angle = component.Angle;
            Int32 segment = sectionData.GetHashCode();
            ApolloDataSetSectionShelvesShelfShelfData primaryShelfData = component.Shelves.First().ShelfData.First();
            return new PlanogramFixtureComponentDto()
            {
                Id = component.Id,
                PlanogramComponentId = component.Id,
                PlanogramFixtureId = segment,
                X = xPos,
                Y = yPos,
                Z = zPos,
                Slope = slope != 0 ? slope : 0,
                Angle = angle != 0 ? angle : 0,
                Roll = 0,
                ComponentSequenceNumber = (Int16?)SpacePlanningImportHelper.ParseInt16(primaryShelfData.SH_SHELF, true, out convertSuccess)
            };
        }

        /// <summary>
        /// Encodes a planogram fixture component dto
        /// </summary>
        private static String EncodePlanogramFixtureComponentDto(PlanogramFixtureComponentDto dto)
        {
            return null;
        }
        #endregion

        #region PlanogramComponent
        /// <summary>
        /// Decodes a planogram component dto
        /// </summary>
        private PlanogramComponentDto DecodePlanogramComponentDto(AdvancedApolloFixtureComponent component, IEnumerable<PlanogramFieldMappingDto> fieldMappings)
        {
            Object fixelId = component.Id;
            ApolloShelfType componentType = component.ComponentType;
            Single height = component.Height;
            Single width = component.Width;
            Single depth = component.Depth;
            ApolloDataSetSectionShelvesShelfShelfData primaryShelfData = component.Shelves.First().ShelfData.First();
            String name = primaryShelfData.SH_DISPLAYNUM.Trim();
            if (name.Length == 0)
            {
                name = fixelId.ToString();
            }
            PlanogramComponentDto planogramComponentDto = new PlanogramComponentDto()
            {
                Id = fixelId,
                PlanogramId = planogramId,
                Name = name,
                Height = height,
                Width = width,
                Depth = depth,
                ComponentType = ConvertComponentType(componentType),
                IsMerchandisedTopDown = GetIsMerchandisedTopDown(componentType)
            };

            if (fieldMappings != null)
            {
                MapCustomAttributes(planogramComponentDto, fieldMappings, primaryShelfData, CustomAttributeDataPlanogramParentType.PlanogramProduct, planogramComponentDto.Id, PlanogramEventLogAffectedType.Planogram);
            }
           
            return planogramComponentDto;
        }


        private static byte ConvertComponentType(ApolloShelfType componentType)
        {
            switch (componentType)
            {
                case ApolloShelfType.Shelf:
                    return (Byte)PlanogramComponentType.Shelf;                 
                case ApolloShelfType.Pegboard:
                    return (Byte)PlanogramComponentType.Peg;                 
                case ApolloShelfType.Coffin:
                    return (Byte)PlanogramComponentType.Chest;                       
                case ApolloShelfType.Sltowall:
                    return (Byte)PlanogramComponentType.SlotWall;
                case ApolloShelfType.Crossbar:
                    return (Byte)PlanogramComponentType.Bar;     
                case ApolloShelfType.Box:
                    return (Byte)PlanogramComponentType.Custom;        
                case ApolloShelfType.HangingBar:
                    return (Byte)PlanogramComponentType.Custom;                   
                case ApolloShelfType.Basket:
                    return (Byte)PlanogramComponentType.Custom;
                default:
                    return 0;
            }
        }

        private static Boolean GetIsMerchandisedTopDown(ApolloShelfType componentType)
        {
            return (componentType == ApolloShelfType.Coffin);
        }

        /// <summary>
        /// Encodes a planogram fixture component dto
        /// </summary>
        private static String EncodePlanogramComponentDto(PlanogramComponentDto dto)
        {
            return null;
        }
        #endregion

        #region PlanogramSubComponent
        /// <summary>
        /// Decodes a planogram sub component dto
        /// </summary>
        private PlanogramSubComponentDto DecodePlanogramSubComponentDto(Int32 parentComponentId, AdvancedApolloFixtureComponent fixtureComponent, ApolloDataSetSectionShelvesShelfShelfData component, Boolean sectionCrunch)
        {

            Boolean convertSuccess = false;
            
            Boolean isVisible = true;
            Object fixelId = component.GetHashCode();
            ApolloShelfType componentType = (ApolloShelfType)SpacePlanningImportHelper.ParseByte(component.SH_SHELFTYPE, false, out convertSuccess);
            String name = component.SH_DISPLAYNUM.Trim();

            if (name.Length == 0)
            {
                name = fixelId.ToString();
            }


            Single height = DecodeShelfHeight(component, componentType, _uom);
            Single width = DecodeShelfWidth(component, componentType, _uom);
            Single depth = DecodeShelfDepth(component, componentType, _uom);
            Single merchandisableHeight = (Single)SpacePlanningImportHelper.ParseSingle(component.SH_MERCHHEIGHT, false, out convertSuccess);
            Single merchandisableDepth = (Single)SpacePlanningImportHelper.ParseSingle(component.SH_MAXMERCHDEPTH, false, out convertSuccess);
            byte spreadMode = (Byte)SpacePlanningImportHelper.ParseByte(component.SH_SPREADMODE, false, out convertSuccess);
            Byte fillPattern = 0;
            Int32 fillColour = -1;
            
            Int32 lineColour = -16777216;
            Single lineThickness = (Single)Math.Round(ConvertUOM(DefaultThicknessSmall, UOM.Metric, _uom), 2);
            Single riserHeight = 0;
            Single riserThickness = 0;
            Int32 riserTransparencyPercent = 0;
            Boolean IsRiserPlacedOnFront = false;
            Single notchStartX = 0;
            Single notchSpacingX = 0;
            Single notchStartY = 0;
            Single notchSpacingY = 0;
            Single notchHeight = 0;
            Single notchWidth = 0;
            Boolean isNotchPlacedOnFront = false;
            Byte notchStyleType = 0;
            Single merchConstraintRow1StartX = 0;
            Single merchConstraintRow1SpacingX = 0;
            Single merchConstraintRow1StartY = 0;
            Single merchConstraintRow1SpacingY = 0;
            Single merchConstraintRow1Height = 0;
            Single merchConstraintRow1Width = 0;
            Byte merchandisingType = GetMerchandisingType(componentType);
            Byte combineType = 0;
            Boolean isProductOverlapAllowed = true;
            Byte merchandisingStrategyX = 0;
            Byte merchandisingStrategyY = 0;
            Byte merchandisingStrategyZ = 0;
            Single leftOverhang = 0;
            Single rightOverhang = 0;
            Single frontOverhang = 0;
            Single backOverhang = 0;
            Single topOverhang = 0;
            Single bottomOverhang = 0;
            Single faceThicknessFront = 0;
            Single faceThicknessBack = 0;
            Single faceThicknessBottom = 0;
            Single faceThicknessLeft = 0;
            Single faceThicknessRight = 0;
            Single faceThicknessTop = 0;
            Single dividerObstructionWidth = 0;
            Single dividerObstructionHeight = 0;
            Single dividerObstructionDepth = 0;
            Single dividerObstructionStartX = 0;
            Single dividerObstructionStartY = 0;
            Single dividerObstructionStartZ = 0;
            Single dividerObstructionSpacingX = 0;
            Single dividerObstructionSpacingY = 0;
            Single dividerObstructionSpacingZ = 0;
            Boolean isDividerObstructionByFacing = false;
            Boolean isProductSqueezeAllowed = true;
            Single xPos = (Single)SpacePlanningImportHelper.ParseSingle(component.SH_XPOS, false, out convertSuccess) - fixtureComponent.XPos;
            Single yPos = (Single)SpacePlanningImportHelper.ParseSingle(component.SH_YPOS, false, out convertSuccess) - fixtureComponent.YPos;
            Single zPos = (Single)SpacePlanningImportHelper.ParseSingle(component.SH_ZPOS, false, out convertSuccess) - fixtureComponent.ZPos;

            switch (componentType)
            {
                case ApolloShelfType.Shelf:
                    if (fillColour == -1) fillColour = _dalCache.Context.PlanSettings.ShelfFillColour;
                    yPos -= height;
                    riserHeight = (Single)SpacePlanningImportHelper.ParseSingle(component.SH_GRILLHEIGHT, false, out convertSuccess);
                    riserThickness = (Single)SpacePlanningImportHelper.ParseSingle(component.SH_GRILLTHICKNESS, false, out convertSuccess);
                    riserTransparencyPercent = 0;
                    if (riserHeight > 0) IsRiserPlacedOnFront = true;
                    leftOverhang = 0;
                    rightOverhang = 0;
                    dividerObstructionStartX = (Single)SpacePlanningImportHelper.ParseSingle(component.SH_DIVINC, false, out convertSuccess);
                    dividerObstructionSpacingX = (Single)SpacePlanningImportHelper.ParseSingle(component.SH_DIVINC, false, out convertSuccess);
                    dividerObstructionWidth = (Single)SpacePlanningImportHelper.ParseSingle(component.SH_DIVTHICK, false, out convertSuccess);
                    if (dividerObstructionWidth > 0)
                    {
                        dividerObstructionDepth = depth;
                        dividerObstructionHeight = Math.Min(2, (Single)SpacePlanningImportHelper.ParseSingle(component.SH_SHELFHEIGHT, false, out convertSuccess));
                    }

                    merchandisingStrategyX = GetMerchandisingStrategyX(spreadMode, sectionCrunch);
                    merchandisingStrategyY = (Byte)PlanogramSubComponentYMerchStrategyType.Bottom;
                    merchandisingStrategyZ = (Byte)PlanogramSubComponentZMerchStrategyType.Front;
                    combineType = 0;// GetCombineType(GetByteValue(fixelData, schema, SpacemanFieldHelper.FixelCombine, DalCacheItemType.Fixel));
                    break;

                case ApolloShelfType.Pegboard:
                    if (fillColour == -1) fillColour = _dalCache.Context.PlanSettings.PegboardFillColour;

                    merchConstraintRow1StartX = (Single)SpacePlanningImportHelper.ParseSingle(component.SH_XINC, false, out convertSuccess);
                    merchConstraintRow1SpacingX = (Single)SpacePlanningImportHelper.ParseSingle(component.SH_XINC, false, out convertSuccess);
                    merchConstraintRow1StartY = (Single)SpacePlanningImportHelper.ParseSingle(component.SH_YINC, false, out convertSuccess);
                    merchConstraintRow1SpacingY = (Single)SpacePlanningImportHelper.ParseSingle(component.SH_YINC, false, out convertSuccess);
                    leftOverhang = 0;
                    rightOverhang = 0;
                    topOverhang = 0;
                    bottomOverhang = 0;
                    merchConstraintRow1Height = ConvertUOM(DefaultPegSize, UOM.Metric, _uom); // defaulted for peg
                    merchConstraintRow1Width = ConvertUOM(DefaultPegSize, UOM.Metric, _uom); // defaulted for peg
                    notchStyleType = (Byte)PlanogramSubComponentNotchStyleType.SingleRectangle;
                    merchandisingStrategyX = (Byte)PlanogramSubComponentYMerchStrategyType.Manual;
                    merchandisingStrategyY = (Byte)PlanogramSubComponentYMerchStrategyType.Manual;
                    merchandisingStrategyZ = (Byte)PlanogramSubComponentZMerchStrategyType.Back;
                    break;

                case ApolloShelfType.Coffin: //CHEST?
                    if (fillColour == -1) fillColour = _dalCache.Context.PlanSettings.ChestFillColour;
                    faceThicknessBottom = Math.Max((Single)SpacePlanningImportHelper.ParseSingle(component.SH_THICKNESS, false, out convertSuccess), ConvertUOM(DefaultThicknessTiny, UOM.Metric, _uom));
                    faceThicknessFront = Math.Max((Single)SpacePlanningImportHelper.ParseSingle(component.SH_THICKNESS, false, out convertSuccess), ConvertUOM(DefaultThicknessTiny, UOM.Metric, _uom));
                    faceThicknessLeft = faceThicknessFront;
                    faceThicknessRight = faceThicknessFront;
                    faceThicknessBack = faceThicknessFront;
                    dividerObstructionStartX = (Single)SpacePlanningImportHelper.ParseSingle(component.SH_DIVINC, false, out convertSuccess);
                    dividerObstructionStartZ = 0;
                    dividerObstructionSpacingX = (Single)SpacePlanningImportHelper.ParseSingle(component.SH_DIVINC, false, out convertSuccess);
                    dividerObstructionSpacingZ = 0;
                    dividerObstructionWidth = (Single)SpacePlanningImportHelper.ParseSingle(component.SH_DIVTHICK, false, out convertSuccess);
                    dividerObstructionDepth = 0;

                    merchandisingStrategyX = (Byte)PlanogramSubComponentXMerchStrategyType.Manual;
                    merchandisingStrategyY = (Byte)PlanogramSubComponentYMerchStrategyType.Bottom;
                    merchandisingStrategyZ = (Byte)PlanogramSubComponentZMerchStrategyType.Manual;
                    break;

                case ApolloShelfType.Crossbar:
                    if (fillColour == -1) fillColour = _dalCache.Context.PlanSettings.BarFillColour;
                    leftOverhang = 0;
                    rightOverhang = 0;

                    merchConstraintRow1StartX = (Single)SpacePlanningImportHelper.ParseSingle(component.SH_XINC, false, out convertSuccess);
                    merchConstraintRow1SpacingX = (Single)SpacePlanningImportHelper.ParseSingle(component.SH_XINC, false, out convertSuccess);
                    merchConstraintRow1StartY = (Single)SpacePlanningImportHelper.ParseSingle(component.SH_YINC, false, out convertSuccess);
                    merchConstraintRow1SpacingY = (Single)SpacePlanningImportHelper.ParseSingle(component.SH_YINC, false, out convertSuccess);
                    merchConstraintRow1Height = ConvertUOM(DefaultPegSize, UOM.Metric, _uom); // defaulted for peg
                    merchConstraintRow1Width = ConvertUOM(DefaultPegSize, UOM.Metric, _uom); // defaulted for peg
                    merchandisingStrategyX = GetMerchandisingStrategyX(spreadMode, sectionCrunch);
                    merchandisingStrategyY = (Byte)PlanogramSubComponentYMerchStrategyType.Top;
                    merchandisingStrategyZ = (Byte)PlanogramSubComponentZMerchStrategyType.Back;
                    combineType = 0;//GetCombineType(GetByteValue(fixelData, schema, SpacemanFieldHelper.FixelCombine, DalCacheItemType.Fixel));
                    break;

                case ApolloShelfType.Sltowall:
                    if (fillColour == -1) fillColour = _dalCache.Context.PlanSettings.SlotwallFillColour;
                    merchConstraintRow1StartX = 0;
                    merchConstraintRow1SpacingX = 0f;
                    merchConstraintRow1StartY = (Single)SpacePlanningImportHelper.ParseSingle(component.SH_YINC, false, out convertSuccess);
                    merchConstraintRow1SpacingY = (Single)SpacePlanningImportHelper.ParseSingle(component.SH_YINC, false, out convertSuccess);
                    leftOverhang = 0;
                    rightOverhang = 0;
                    topOverhang = 0;
                    bottomOverhang = 0;
                    merchConstraintRow1Height = ConvertUOM(DefaultPegSize, UOM.Metric, _uom); // defaulted for peg
                    merchConstraintRow1Width = ConvertUOM(DefaultPegSize, UOM.Metric, _uom);
                    notchStyleType = (Byte)PlanogramSubComponentNotchStyleType.SingleRectangle;
                    merchandisingStrategyX = (Byte)PlanogramSubComponentYMerchStrategyType.Manual;
                    merchandisingStrategyY = (Byte)PlanogramSubComponentYMerchStrategyType.Manual;
                    merchandisingStrategyZ = (Byte)PlanogramSubComponentZMerchStrategyType.Back;
                    break;
                case ApolloShelfType.HangingBar:
                    if (fillColour == -1) fillColour = _dalCache.Context.PlanSettings.RodFillColour;
                    //switch depth and width around
                    merchandisingStrategyX = (Byte)PlanogramSubComponentXMerchStrategyType.Even;
                    merchandisingStrategyY = (Byte)PlanogramSubComponentYMerchStrategyType.Top;
                    merchandisingStrategyZ = (Byte)PlanogramSubComponentZMerchStrategyType.Manual;
                    break;

                case ApolloShelfType.Basket:
                    if (fillColour == -1) fillColour = _dalCache.Context.PlanSettings.ChestFillColour;
                    faceThicknessBottom = Math.Max((Single)SpacePlanningImportHelper.ParseSingle(component.SH_THICKNESS, false, out convertSuccess), ConvertUOM(DefaultThicknessTiny, UOM.Metric, _uom));
                    faceThicknessFront = Math.Max((Single)SpacePlanningImportHelper.ParseSingle(component.SH_THICKNESS, false, out convertSuccess), ConvertUOM(DefaultThicknessTiny, UOM.Metric, _uom));
                    faceThicknessLeft = faceThicknessFront;
                    faceThicknessRight = faceThicknessFront;
                    faceThicknessBack = faceThicknessFront;

                    // GetBasketMerchandisingStrategy(GetByteValue(fixelData, schema, SpacemanFieldHelper.FixelSpreadProducts, DalCacheItemType.Fixel), ref merchandisingStrategyX, ref merchandisingStrategyY, ref merchandisingStrategyZ);
                    merchandisingStrategyY = (Byte)PlanogramSubComponentYMerchStrategyType.Bottom;
                    merchandisingStrategyX = (Byte)PlanogramSubComponentXMerchStrategyType.Manual;
                    merchandisingStrategyZ = (Byte)PlanogramSubComponentZMerchStrategyType.Manual;
                    break;

                case ApolloShelfType.Box:
                    if (fillColour == -1) fillColour = ConvertColour((Byte)180, (Byte)200, (Byte)200);
                    break;

                case ApolloShelfType.FreeHandSurface:
                    isVisible = false;                    
                    if (fillColour == -1) fillColour = ConvertColour((Byte)180, (Byte)200, (Byte)200);  
                    lineColour = 0;
                    lineThickness = 0;
                    faceThicknessBottom = 0;
                    faceThicknessFront = 0;
                    faceThicknessLeft = 0;
                    faceThicknessRight = 0;
                    faceThicknessBack = 0;

                    // GetBasketMerchandisingStrategy(GetByteValue(fixelData, schema, SpacemanFieldHelper.FixelSpreadProducts, DalCacheItemType.Fixel), ref merchandisingStrategyX, ref merchandisingStrategyY, ref merchandisingStrategyZ);
                    merchandisingStrategyY = (Byte)PlanogramSubComponentYMerchStrategyType.Bottom;
                    merchandisingStrategyX = (Byte)PlanogramSubComponentXMerchStrategyType.Manual;
                    merchandisingStrategyZ = (Byte)PlanogramSubComponentZMerchStrategyType.Manual;
                    break;

                default:
                    break;
            }

            Single slope = 0, angle = 0, roll = 0;
            if (fixtureComponent.RotateSubComponents)
            {

                if (componentType == ApolloShelfType.HangingBar)
                {
                    slope = SpacePlanningImportHelper.ConvertToRadians((Single)SpacePlanningImportHelper.ParseSingle(component.SH_SLOPE, false, out convertSuccess));
                    angle = SpacePlanningImportHelper.ConvertToRadians((Single)SpacePlanningImportHelper.ParseSingle(component.SH_RESERVED1, false, out convertSuccess) - 90);
                    //roll = -ConvertToRadians((Single)SpacePlanningImportHelper.ParseSingle(component.SH_SLOPE));
                    
                    zPos -= (Single)((width / 2) * Math.Sin(angle));
                    xPos -= (Single)((width / 2) * Math.Cos(angle));
                   // slope = 0;
                   // roll = 0;
                }
                else
                {
                    slope = SpacePlanningImportHelper.ConvertToRadians((Single)SpacePlanningImportHelper.ParseSingle(component.SH_SLOPE, false, out convertSuccess));
                }
            }

            return new PlanogramSubComponentDto()
            {
                Id = fixelId,
                PlanogramComponentId = parentComponentId,
                Name = name,
                Height = height,
                Width = width,
                Depth = depth,
                X = xPos,
                Y = yPos,
                Z = zPos,
                Slope = slope,
                Angle = angle,
                Roll = roll,
                ShapeType = (Byte)PlanogramSubComponentShapeType.Box,
                MerchandisableHeight = merchandisableHeight,
                MerchandisableDepth = merchandisableDepth,
                IsVisible = isVisible,
                HasCollisionDetection = true,
                FillPatternTypeFront = fillPattern,
                FillPatternTypeBack = fillPattern,
                FillPatternTypeTop = fillPattern,
                FillPatternTypeBottom = fillPattern,
                FillPatternTypeLeft = fillPattern,
                FillPatternTypeRight = fillPattern,
                FillColourFront = fillColour,
                FillColourBack = fillColour,
                FillColourTop = fillColour,
                FillColourBottom = fillColour,
                FillColourLeft = fillColour,
                FillColourRight = fillColour,
                LineColour = lineColour,
                LineThickness = lineThickness,
                RiserFillPatternType = (Byte)PlanogramSubComponentFillPatternType.Crosshatch,
                RiserHeight = riserHeight,
                RiserThickness = riserThickness,
                RiserTransparencyPercent = riserTransparencyPercent,
                IsRiserPlacedOnFront = IsRiserPlacedOnFront,
                NotchStartX = notchStartX,
                NotchSpacingX = notchSpacingX,
                NotchStartY = notchStartY,
                NotchSpacingY = notchSpacingY,
                NotchHeight = notchHeight,
                NotchWidth = notchWidth,
                IsNotchPlacedOnFront = isNotchPlacedOnFront,
                NotchStyleType = notchStyleType,
                MerchConstraintRow1StartX = merchConstraintRow1StartX,
                MerchConstraintRow1SpacingX = merchConstraintRow1SpacingX,
                MerchConstraintRow1StartY = merchConstraintRow1StartY,
                MerchConstraintRow1SpacingY = merchConstraintRow1SpacingY,
                MerchConstraintRow1Height = merchConstraintRow1Height,
                MerchConstraintRow1Width = merchConstraintRow1Width,
                MerchandisingType = merchandisingType,
                CombineType = combineType,
                IsProductOverlapAllowed = isProductOverlapAllowed,
                MerchandisingStrategyX = merchandisingStrategyX,
                MerchandisingStrategyY = merchandisingStrategyY,
                MerchandisingStrategyZ = merchandisingStrategyZ,
                LeftOverhang = leftOverhang,
                RightOverhang = rightOverhang,
                FrontOverhang = frontOverhang,
                BackOverhang = backOverhang,
                TopOverhang = topOverhang,
                BottomOverhang = bottomOverhang,
                FaceThicknessFront = faceThicknessFront,
                FaceThicknessBack = faceThicknessBack,
                FaceThicknessBottom = faceThicknessBottom,
                FaceThicknessLeft = faceThicknessLeft,
                FaceThicknessRight = faceThicknessRight,
                FaceThicknessTop = faceThicknessTop,
                DividerObstructionWidth = dividerObstructionWidth,
                DividerObstructionHeight = dividerObstructionHeight,
                DividerObstructionDepth = dividerObstructionDepth,
                DividerObstructionStartX = dividerObstructionStartX,
                DividerObstructionStartY = dividerObstructionStartY,
                DividerObstructionStartZ = dividerObstructionStartZ,
                DividerObstructionSpacingX = dividerObstructionSpacingX,
                DividerObstructionSpacingY = dividerObstructionSpacingY,
                DividerObstructionSpacingZ = dividerObstructionSpacingZ,
                IsDividerObstructionByFacing = isDividerObstructionByFacing,
                DividerObstructionFillColour = -65536,
                DividerObstructionFillPattern = 0,
                IsProductSqueezeAllowed = isProductSqueezeAllowed
            };
        }

        internal static Single DecodeShelfHeight(ApolloDataSetSectionShelvesShelfShelfData component, ApolloShelfType componentType, UOM uom)
        {
            Boolean convertSuccess;
            switch (componentType)
            {
                case ApolloShelfType.Shelf:
                    return (Single)SpacePlanningImportHelper.ParseSingle(component.SH_THICKNESS, false, out convertSuccess);          
                case ApolloShelfType.Coffin://Hight and depth are switched for coffins
                case ApolloShelfType.Basket://Hight and depth are switched for baskets
                    return (Single)SpacePlanningImportHelper.ParseSingle(component.SH_SHELFDEPTH, false, out convertSuccess);
                case ApolloShelfType.FreeHandSurface:
                    return ConvertUOM(DefaultThicknessTiny, UOM.Metric, uom);
                case ApolloShelfType.Pegboard:
                case ApolloShelfType.Crossbar:
                case ApolloShelfType.Sltowall:
                case ApolloShelfType.HangingBar:
                case ApolloShelfType.Box:
                default:
                    return (Single)SpacePlanningImportHelper.ParseSingle(component.SH_SHELFHEIGHT, false, out convertSuccess);
            }
        }
        internal static Single DecodeShelfWidth(ApolloDataSetSectionShelvesShelfShelfData component, ApolloShelfType componentType, UOM uom)
        {
            Boolean convertSuccess;
            switch (componentType)
            {
                case ApolloShelfType.HangingBar:
                    //Return depth as width
                    return (Single)SpacePlanningImportHelper.ParseSingle(component.SH_SHELFDEPTH, false, out convertSuccess);
                case ApolloShelfType.Shelf:
                case ApolloShelfType.Pegboard:
                case ApolloShelfType.Coffin:
                case ApolloShelfType.Crossbar:
                case ApolloShelfType.Sltowall:
                case ApolloShelfType.Basket:
                case ApolloShelfType.Box:
                case ApolloShelfType.FreeHandSurface:
                default:
                    return (Single)SpacePlanningImportHelper.ParseSingle(component.SH_SHELFWIDTH, false, out convertSuccess);
            }
        }
        internal static Single DecodeShelfDepth(ApolloDataSetSectionShelvesShelfShelfData component, ApolloShelfType componentType, UOM uom)
        {
            Boolean convertSuccess;
            switch (componentType)
            {                
                case ApolloShelfType.Pegboard:
                    return Math.Max((Single)SpacePlanningImportHelper.ParseSingle(component.SH_THICKNESS, false, out convertSuccess), ConvertUOM(DefaultThicknessNormal, UOM.Metric, uom));
                case ApolloShelfType.Coffin:
                    //Hight and depth are switched for coffins!!!!
                    return (Single)SpacePlanningImportHelper.ParseSingle(component.SH_SHELFHEIGHT, false, out convertSuccess);
                case ApolloShelfType.Crossbar:
                    return Math.Max((Single)SpacePlanningImportHelper.ParseSingle(component.SH_THICKNESS, false, out convertSuccess), ConvertUOM(DefaultThicknessNormal, UOM.Metric, uom));
                case ApolloShelfType.Sltowall:
                    return Math.Max((Single)SpacePlanningImportHelper.ParseSingle(component.SH_THICKNESS, false, out convertSuccess), ConvertUOM(DefaultThicknessNormal, UOM.Metric, uom));
                case ApolloShelfType.HangingBar:
                    //Return width as depth
                    return (Single)SpacePlanningImportHelper.ParseSingle(component.SH_SHELFWIDTH, false, out convertSuccess);
                case ApolloShelfType.Basket:
                    //Hight and depth are switched for baskets!!!!
                    return (Single)SpacePlanningImportHelper.ParseSingle(component.SH_SHELFHEIGHT, false, out convertSuccess);                  
                case ApolloShelfType.FreeHandSurface:
                    //return height as depth
                    return (Single)SpacePlanningImportHelper.ParseSingle(component.SH_SHELFHEIGHT, false, out convertSuccess);
                case ApolloShelfType.Shelf:
                case ApolloShelfType.Box:
                default:
                    return (Single)SpacePlanningImportHelper.ParseSingle(component.SH_SHELFDEPTH, false, out convertSuccess);
            }
        }

        private static Byte GetMerchandisingStrategyX(Byte merchandisingStrategyX, Boolean sectionCrunch)
        {
            switch (merchandisingStrategyX)
            {
                case 0: //Section
                    if (sectionCrunch)
                    {
                        return (Byte)PlanogramSubComponentXMerchStrategyType.LeftStacked;
                    }
                    else
                    {
                        return (Byte)PlanogramSubComponentXMerchStrategyType.Manual;
                    }
                case 1: // Right
                    return (Byte)PlanogramSubComponentXMerchStrategyType.RightStacked;
                case 2: // Left
                    return (Byte)PlanogramSubComponentXMerchStrategyType.LeftStacked;                
                case 3: // Center
                    return (Byte)PlanogramSubComponentXMerchStrategyType.Manual;
                case 4: // Even
                    return (Byte)PlanogramSubComponentXMerchStrategyType.Even;
                default:
                    return (Byte)PlanogramSubComponentXMerchStrategyType.Manual;
            }
        }

        private static Byte GetComponentFillPatternType(Byte fillPatternType)
        {
            switch (fillPatternType)
            {
                case 0:
                    return (Byte)PlanogramSubComponentFillPatternType.Solid;
                case 1:
                    return (Byte)PlanogramSubComponentFillPatternType.DiagonalDown;
                case 2:
                    return (Byte)PlanogramSubComponentFillPatternType.DiagonalUp;
                case 3:
                case 6:
                    return (Byte)PlanogramSubComponentFillPatternType.Crosshatch;
                case 4:
                    return (Byte)PlanogramSubComponentFillPatternType.Border;
                case 11:
                    return (Byte)PlanogramSubComponentFillPatternType.Horizontal;
                case 12:
                    return (Byte)PlanogramSubComponentFillPatternType.Vertical;
                default:
                    return (Byte)PlanogramSubComponentFillPatternType.Solid;
            }
        }


        private static Byte GetMerchandisingType(ApolloShelfType componentType)
        {
            switch (componentType)
            {
                case ApolloShelfType.FreeHandSurface:                
                case ApolloShelfType.Basket:
                case ApolloShelfType.Shelf:                
                case ApolloShelfType.Coffin:
                    return (Byte)PlanogramSubComponentMerchandisingType.Stack;

                case ApolloShelfType.Pegboard:
                case ApolloShelfType.Crossbar:
                case ApolloShelfType.Sltowall:
                    return (Byte)PlanogramSubComponentMerchandisingType.Hang;
                case ApolloShelfType.HangingBar:
                    return (Byte)PlanogramSubComponentMerchandisingType.HangFromBottom;
                case ApolloShelfType.Box:
                    return (Byte)PlanogramSubComponentMerchandisingType.None;
                default:
                    return (Byte)PlanogramSubComponentMerchandisingType.None;
            }
        }

        /// <summary>
        /// Encodes a planogram sub component dto
        /// </summary>
        private static String EncodePlanogramSubComponentDto(PlanogramSubComponentDto dto)
        {
            return null;
        }
        #endregion

        #region PlanogramProduct
        /// <summary>
        /// Decodes a planogram product dto
        /// </summary>
        private PlanogramProductDto DecodePlanogramProductDto(ApolloDataSetProductsProduct apolloProduct, Dictionary<String, PlanogramProductDto> productLookup, IEnumerable<PlanogramFieldMappingDto> fieldMappings, List<String> productGtins)
        {
            Boolean convertSuccess = false;
            Single height = (Single)SpacePlanningImportHelper.ParseSingle(apolloProduct.PR_PRODHEIGHT, false, out convertSuccess);
            Single width = (Single)SpacePlanningImportHelper.ParseSingle(apolloProduct.PR_PRODWIDTH, false, out convertSuccess);
            Single depth = (Single)SpacePlanningImportHelper.ParseSingle(apolloProduct.PR_PRODDEPTH, false, out convertSuccess);
                     
            Boolean createNewProductId = false;

            Single SqueezeHeight = 0;// GetSingleValue(fields, schema, SpacemanFieldHelper.ProductMaxVertCrush, DalCacheItemType.Product);
            Single SqueezeWidth = 0;//GetSingleValue(fields, schema, SpacemanFieldHelper.ProductMaxHorizCrush, DalCacheItemType.Product);
            Single SqueezeDepth = 0;//GetSingleValue(fields, schema, SpacemanFieldHelper.ProductMaxDepthCrush, DalCacheItemType.Product);

            Single nestingHeight = 0;
            Single nestingWidth = 0;
            Single nestingDepth = 0;

            nestingHeight = (Single)SpacePlanningImportHelper.ParseSingle(apolloProduct.PR_NESTING, false, out convertSuccess);
            if (nestingHeight > 0)
            {
                nestingHeight = height - nestingHeight;
            }
            // can be negative in Spaceman changes the way that the nest is rendered (always invert to positive for CCM)
            nestingHeight = (nestingHeight < 0) ? -nestingHeight : nestingHeight; // (positive nest above, negative nest below)

            String productId = apolloProduct.PR_UPC;
            String apolloColourString = apolloProduct.X06_MEASRESERVE01;
            Int32 apolloColourInt = 0;

            if (!String.IsNullOrEmpty(apolloColourString))
            {
                //In the great wisdom the the Apollo devs not only is the displayed colour of
                //a product not in the colour field but it is also stored as a Single not an int!

                Int32 decimalIndex = apolloColourString.IndexOf('.');
                if (decimalIndex != -1)
                {
                    apolloColourString = apolloColourString.Substring(0, decimalIndex);
                }

                apolloColourInt = ConvertColour((Int32)SpacePlanningImportHelper.ParseInt32(apolloColourString, false, out convertSuccess));
            }
            else
            {
                //fallback to the PR_Colour if the usual "colour" field is not populated. I think this is
                //a field used for very old versions of apollo that had limited colours.
                apolloColourInt = ConvertColour((ApolloColourType)SpacePlanningImportHelper.ParseInt32(apolloProduct.PR_COLOR, false, out convertSuccess));
            }

            //peg Y starts from the bottom for Apollo so if it is unset we need to move it to the top
            Single pegY = (Single)SpacePlanningImportHelper.ParseSingle(apolloProduct.PR_YPEGHOLE, false, out convertSuccess);
            if (pegY.GreaterThan(0.0F))
            {
                pegY = height - pegY;
            }

            PlanogramProductDto planogramProductDto = new PlanogramProductDto()
            {
                Id = apolloProduct.GetHashCode(),
                PlanogramId = planogramId,
                Gtin = apolloProduct.PR_UPC,
                Name = apolloProduct.PR_LONGDESC,
                Height = height,
                Width = width,
                Depth = depth,

                DisplayHeight = height,
                DisplayWidth = width,
                DisplayDepth = depth,

                NumberOfPegHoles = 0, // overriden by peg position decode but always defaulted to 1 if on a peg!

                PegX = (Single)SpacePlanningImportHelper.ParseSingle(apolloProduct.PR_XPEGHOLE, false, out convertSuccess),
                PegX2 = 0,
                PegX3 = 0,
                PegY = pegY,
                PegY2 = 0,
                PegY3 = 0,

                PegDepth = 0, // overriden by peg position decode, the actual peg depth is contained within peg library which isnt always contained within Spaceman file

                SqueezeHeight = SqueezeHeight,
                SqueezeWidth = SqueezeWidth,
                SqueezeDepth = SqueezeDepth,

                NestingHeight = nestingHeight,
                NestingWidth = nestingWidth,
                NestingDepth = nestingDepth,
                CasePackUnits = (Int16)SpacePlanningImportHelper.ParseInt16(apolloProduct.PR_CASEPACK, false, out convertSuccess),

                CaseHigh = 0,
                CaseWide = 0,
                CaseDeep = 0,
                CaseHeight = (Single)SpacePlanningImportHelper.ParseSingle(apolloProduct.PR_CASEHEIGHT, false, out convertSuccess),
                CaseWidth = (Single)SpacePlanningImportHelper.ParseSingle(apolloProduct.PR_CASEWIDTH, false, out convertSuccess),
                CaseDepth = (Single)SpacePlanningImportHelper.ParseSingle(apolloProduct.PR_CASEDEPTH, false, out convertSuccess),
                MaxStack = (Byte)SpacePlanningImportHelper.ParseByte(apolloProduct.PR_MAXFRONTS, false, out convertSuccess),
                MaxTopCap = (Byte)SpacePlanningImportHelper.ParseByte(apolloProduct.PR_MAXLAYOVERS, false, out convertSuccess),
                MinDeep = 0,
                MaxDeep = (Byte)SpacePlanningImportHelper.ParseByte(apolloProduct.PR_MAXFRONTSDEEP, false, out convertSuccess),
                              
                
                StatusType = (Byte)PlanogramProductStatusType.Active,
                OrientationType = (Byte)PlanogramProductOrientationType.Front0,//GetOrientationType(GetByteValue(fields, schema, SpacemanFieldHelper.ProductStdOrient, DalCacheItemType.Product)),
                MerchandisingStyle = (Byte)PlanogramProductMerchandisingStyle.Unit,//GetMerchandisingStyle(GetByteValue(fields, schema, SpacemanFieldHelper.ProductMerchStyle, DalCacheItemType.Product)),
                IsFrontOnly = false, //GetIsFrontOnly(fields, schema), //seems that its not possible to calculate this from the legal orientations within Spaceman as it seems to break these rules to get top caps on 
                CanBreakTrayUp = true,
                CanBreakTrayDown = true,
                CanBreakTrayBack = true,
                CanBreakTrayTop = true,
                StyleNumber = 0,
                FingerSpaceAbove = (Single)SpacePlanningImportHelper.ParseSingle(apolloProduct.PR_FINGER, false, out convertSuccess),
                ShapeType = (Byte)PlanogramProductShapeType.Box,
                FillPatternType = (Byte)PlanogramProductFillPatternType.Solid,//GetFillPatternType(GetByteValue(fields, schema, SpacemanFieldHelper.ProductFillPattern, DalCacheItemType.Product)),
                FillColour = apolloColourInt
            };

            if (fieldMappings != null)
            {
                MapCustomAttributes(planogramProductDto, fieldMappings, apolloProduct, CustomAttributeDataPlanogramParentType.PlanogramProduct, planogramProductDto.Id, PlanogramEventLogAffectedType.Products);
                               
            }

            if (!productLookup.ContainsKey(productId))
            {
                productLookup.Add(productId, planogramProductDto);
            }

            SpacePlanningImportHelper.ValidatePlanogramProductBusinessRules(_dalCache.PlanogramEventLogDtoList, planogramProductDto, productGtins);

            return planogramProductDto;
        }

        private Byte GetOrientationType(Byte orientationType)
        {
            switch (orientationType)
            {
                case 0:
                    return (Byte)PlanogramProductOrientationType.Front0;
                case 1:
                    return (Byte)PlanogramProductOrientationType.Front90;
                case 2:
                    return (Byte)PlanogramProductOrientationType.Front180;
                case 3:
                    return (Byte)PlanogramProductOrientationType.Front270;
                case 4:
                    return (Byte)PlanogramProductOrientationType.Top0;
                case 5:
                    return (Byte)PlanogramProductOrientationType.Top90;
                case 6:
                    return (Byte)PlanogramProductOrientationType.Top180;
                case 7:
                    return (Byte)PlanogramProductOrientationType.Top270;
                case 8:
                    return (Byte)PlanogramProductOrientationType.Left0;
                case 9:
                    return (Byte)PlanogramProductOrientationType.Left90;
                case 10:
                    return (Byte)PlanogramProductOrientationType.Left180;
                case 11:
                    return (Byte)PlanogramProductOrientationType.Left270;
                case 12:
                    return (Byte)PlanogramProductOrientationType.Back0;
                case 13:
                    return (Byte)PlanogramProductOrientationType.Back90;
                case 14:
                    return (Byte)PlanogramProductOrientationType.Back180;
                case 15:
                    return (Byte)PlanogramProductOrientationType.Back270;
                case 16:
                    return (Byte)PlanogramProductOrientationType.Bottom0;
                case 17:
                    return (Byte)PlanogramProductOrientationType.Bottom90;
                case 18:
                    return (Byte)PlanogramProductOrientationType.Bottom180;
                case 19:
                    return (Byte)PlanogramProductOrientationType.Bottom270;
                case 20:
                    return (Byte)PlanogramProductOrientationType.Right0;
                case 21:
                    return (Byte)PlanogramProductOrientationType.Right90;
                case 22:
                    return (Byte)PlanogramProductOrientationType.Right180;
                case 23:
                    return (Byte)PlanogramProductOrientationType.Right270;
                default:
                    return (Byte)PlanogramProductOrientationType.Front0;
            }
        }

        private Byte GetMerchandisingStyle(Byte merchandisingStyleType)
        {
            switch (merchandisingStyleType)
            {
                case 0:
                    return (Byte)PlanogramProductMerchandisingStyle.Unit;
                case 1:
                    return (Byte)PlanogramProductMerchandisingStyle.Tray;
                case 2:
                    return (Byte)PlanogramProductMerchandisingStyle.Case;
                case 3:
                    return (Byte)PlanogramProductMerchandisingStyle.Display;
                default:
                    return (Byte)PlanogramProductMerchandisingStyle.Unit;
            }
        }

        private static Boolean GetIsTrayProduct(Byte merchandisingStyleType)
        {
            return merchandisingStyleType == 1;
        }

        private static Byte GetFillPatternType(Byte fillPatternType)
        {
            switch (fillPatternType)
            {
                case 0:
                    return (Byte)PlanogramProductFillPatternType.Solid;
                case 1:
                    return (Byte)PlanogramProductFillPatternType.DiagonalDown;
                case 2:
                    return (Byte)PlanogramProductFillPatternType.DiagonalUp;
                case 3:
                case 6:
                    return (Byte)PlanogramProductFillPatternType.Crosshatch;
                case 4:
                    return (Byte)PlanogramProductFillPatternType.Border;
                case 11:
                    return (Byte)PlanogramProductFillPatternType.Horizontal;
                case 12:
                    return (Byte)PlanogramProductFillPatternType.Vertical;
                default:
                    return (Byte)PlanogramProductFillPatternType.Solid;
            }
        }

        public static UInt16 Convert32bitColorto16bit(Int32 color)
        {
            Int32 red = ((((color >> 0x10) & 0xFF) * 0x1F) + 0x7F) / 0xFF;
            Int32 green = ((((color >> 0x8) & 0xFF) * 0x1F) + 0x7F) / 0xFF;
            Int32 blue = (((color & 0xFF) * 0x1F) + 0x7F) / 0xFF;

            return (UInt16)(0x8000 | (red << 0xA) | (green << 0x5) | blue);
        }
        
        
        /// <summary>
        /// Converts Apollo colour to CCM colour
        /// </summary>
        /// <param name="intValue">The colour value</param>
        /// <returns>CCM Colour</returns>
        private static Int32 ConvertColour(ApolloColourType colour)
        {
            Byte b = 0;
            Byte g = 0;
            Byte r = 0;
            Byte a = 255;

            switch (colour)
            {
                case ApolloColourType.White:
                    b = 255;
                    g = 255;
                    r = 255;
                    break;
                case ApolloColourType.Blue:
                    b = 255;
                    g = 0;
                    r = 0;
                    break;
                case ApolloColourType.Green:
                    b = 0;
                    g = 255;
                    r = 0;
                    break;
                case ApolloColourType.Cyan:
                    b = 255;
                    g = 255;
                    r = 0;
                    break;
                case ApolloColourType.Red:
                    b = 0;
                    g = 0;
                    r = 255;
                    break;
                case ApolloColourType.Violet:
                    b = 204;
                    g = 0;
                    r = 204;
                    break;
                case ApolloColourType.Yellow:
                    b = 0;
                    g = 255;
                    r = 255;
                    break;
                case ApolloColourType.Black:
                    b = 0;
                    g = 0;
                    r = 0;
                    break;
                case ApolloColourType.LightBlue:
                    b = 255;
                    g = 102;
                    r = 102;
                    break;
                case ApolloColourType.LightGreen:
                    b = 102;
                    g = 255;
                    r = 102;
                    break;
                case ApolloColourType.LightCyan:
                    b = 135;
                    g = 255;
                    r = 255;
                    break;
                case ApolloColourType.LightRed:
                    b = 102;
                    g = 102;
                    r = 255;
                    break;
                case ApolloColourType.LightViolet:
                    b = 238;
                    g = 130;
                    r = 238;
                    break;
                case ApolloColourType.LightYellow:
                    b = 102;
                    g = 255;
                    r = 255;
                    break;
                case ApolloColourType.Grey:
                    b = 192;
                    g = 192;
                    r = 192;
                    break;
                default:
                    b = 255;
                    g = 255;
                    r = 255;
                    break;
            }


            if (a == 0) { a = 255; }

            var uint32Color = (((UInt32)a) << 24) |
                    (((UInt32)r) << 16) |
                    (((UInt32)g) << 8) |
                    (UInt32)b;

            return (Int32)uint32Color;
        }

        private static Int32 ConvertColour(Int32 intValue)
        {
            const Int32 cTransparentColour = 16777215;
            if (intValue == -1) { return cTransparentColour; }

            Byte b = (Byte)((intValue >> 0x10) & 0xff);
            Byte g = (Byte)((intValue >> 8) & 0xff);
            Byte r = (Byte)(intValue & 0xff);
            Byte a = (Byte)((intValue >> 0x18) & 0xff);

            if (a == 0) { a = 255; }

            var uint32Color = (((UInt32)a) << 24) |
                    (((UInt32)r) << 16) |
                    (((UInt32)g) << 8) |
                    (UInt32)b;

            return (Int32)uint32Color;
        }
        private static Int32 ConvertColour(Byte red, Byte green, Byte blue)
        {
            Byte b = blue;
            Byte g = green;
            Byte r = red;
            Byte a = 255;

            if (a == 0) { a = 255; }

            var uint32Color = (((UInt32)a) << 24) |
                    (((UInt32)r) << 16) |
                    (((UInt32)g) << 8) |
                    (UInt32)b;

            return (Int32)uint32Color;
        }
        /// <summary>
        /// Encodes a planogram product dto
        /// </summary>
        private static String EncodePlanogramProductDto(PlanogramProductDto dto)
        {
            return null;
        }
        #endregion

        #region PlanogramPosition
        /// <summary>
        /// Decodes a planogram position dto
        /// </summary>
        private PlanogramPositionDto DecodePlanogramPositionDto(PositionsPosition position, Int32 currentComponentId, ApolloDataSetSectionSectionData sectionData, 
            ApolloDataSetSectionShelvesShelfShelfData component, PlanogramProductDto product)
        {
            Boolean convertSuccess = false;
            Single xPos = (Single)SpacePlanningImportHelper.ParseSingle(position.PO_XPOS, false, out convertSuccess);
            Single yPos = (Single)SpacePlanningImportHelper.ParseSingle(position.PO_YPOS, false, out convertSuccess);
            Single zPos = (Single)SpacePlanningImportHelper.ParseSingle(position.PO_ZPOS, false, out convertSuccess);
            ApolloShelfType componentType = (ApolloShelfType)SpacePlanningImportHelper.ParseByte(component.SH_SHELFTYPE, false, out convertSuccess);
            ApolloMerchMethodType apolloMerchMethod = (ApolloMerchMethodType)SpacePlanningImportHelper.ParseInt32(position.PO_MERCHANDISINGTYPE, false, out convertSuccess);

            #region front facings
            Byte facingsHigh = (Byte)SpacePlanningImportHelper.ParseByte(position.PO_YFACINGS, false, out convertSuccess);
            Byte facingsWide = (Byte)SpacePlanningImportHelper.ParseByte(position.PO_XFACINGS, false, out convertSuccess);
            Byte facingsDeep = (Byte)SpacePlanningImportHelper.ParseByte(position.PO_ZFACINGS, false, out convertSuccess);
            Byte merchandisingStyle = GetPositionMerchandisingStyle(apolloMerchMethod);
            Byte orientationType = ConvertApolloToCCMPositionOrientation((ApolloOrientationType)SpacePlanningImportHelper.ParseInt32(position.PO_ORIENTATION, false, out convertSuccess));
            #endregion

            #region Top caps
            Byte facingsYHigh = (Byte)SpacePlanningImportHelper.ParseByte(position.PO_LAYOVERS, false, out convertSuccess);
            Byte facingsYWide = facingsWide;
            Byte facingsYDeep = (Byte)SpacePlanningImportHelper.ParseByte(position.PO_LAYOVERSDEEP, false, out convertSuccess);
            Byte merchandisingStyleY = merchandisingStyle;
            Byte orientationTypeY = GetPositionLayoverOrientationType((ApolloOrientationType)SpacePlanningImportHelper.ParseInt32(position.PO_ORIENTATION, false, out convertSuccess));

            if (facingsYHigh <= 0 || facingsYDeep <= 0 || facingsYWide <= 0)
            {
                facingsYHigh = 0;
                facingsYWide = 0;
                facingsYDeep = 0;
            }
            #endregion

            #region right and back caps
            Byte facingsXHigh = 0;
            Byte facingsXWide = 0;
            Byte facingsXDeep = 0;
            Byte merchandisingStyleX = 0;
            Byte orientationTypeX = (Byte)PlanogramPositionOrientationType.Right0;
            Boolean isXPlacedLeft = false;

            Byte facingsZHigh = 0;
            Byte facingsZWide = 0;
            Byte facingsZDeep = 0;
            Byte merchandisingStyleZ = 0;
            Byte orientationTypeZ = 0;
            #endregion

            Byte tempFacings = 0;
            switch (componentType)
            {
                case ApolloShelfType.Pegboard:
                case ApolloShelfType.Sltowall:
                    facingsWide = (Byte)(facingsWide / facingsHigh);
                    if (facingsYWide > 0)
                    {
                        facingsYWide = facingsWide;
                    }
                    break;
                case ApolloShelfType.HangingBar:
                    //tempFacings = facingsWide;
                    //facingsWide = facingsDeep;
                    //facingsDeep = tempFacings;
                    //orientationType = GetPositionRightOrientationType((ApolloOrientation)SpacePlanningImportHelper.ParseInt32(position.PO_ORIENTATION));                    
                    break;
                //chest like components have some values switched about because Apollo hastes us.
                case ApolloShelfType.FreeHandSurface:
                case ApolloShelfType.Basket:
                case ApolloShelfType.Coffin:

                    ApolloOrientationType orientation = (ApolloOrientationType)SpacePlanningImportHelper.ParseInt32(position.PO_ORIENTATION, false, out convertSuccess);
                    Single offset = 0;
                    switch (orientation)
                    {
                        case ApolloOrientationType.FrontEnd:
                            offset = product.Height;
                            break;
                        case ApolloOrientationType.FrontSide:
                           offset = product.Width;
                            break;
                        case ApolloOrientationType.SideFront:
                            offset = product.Depth;
                            break;
                        case ApolloOrientationType.SideEnd:
                            offset = product.Height;
                            break;
                        case ApolloOrientationType.EndFront:
                            offset = product.Width;
                            break;
                        case ApolloOrientationType.EndSide:
                             offset = product.Depth;
                            break;
                        default:
                            offset = product.Height;
                            break;
                    }
                    zPos -= offset; //needed for free hand for shizzle

                    //Notes: Export values - 
                    //this.Position.PO_XFACINGS = (this.PositionDto.FacingsWide * this.PositionDto.FacingsDeep).ToString();
                    //this.Position.PO_YFACINGS = this.PositionDto.FacingsDeep.ToString();
                    //this.Position.PO_ZFACINGS = this.PositionDto.FacingsHigh.ToString();
                    Byte apolloXFacings = facingsWide;
                    Byte apolloYFacings = facingsHigh;
                    Byte apolloZFacings = facingsDeep;
                    facingsWide = (Byte)(apolloXFacings / apolloYFacings);
                    facingsHigh = apolloZFacings;
                    facingsDeep = apolloYFacings;
                    
                    //tempFacings = facingsHigh;
                    //facingsHigh = facingsDeep;
                    //facingsDeep = tempFacings;
                    tempFacings = facingsYHigh;
                    facingsYDeep = tempFacings;
                    orientationType = ConvertApolloCoffinToCCMChestPositionOrientation(orientation);
                    orientationTypeY = ConvertApolloCoffinToCCMChestPositionOrientation(orientation);
                    break;
            }

            //update peg properties
            product.PegDepth = Math.Max(product.PegDepth, (Single)SpacePlanningImportHelper.ParseSingle(position.PO_PEGLENGTH, false, out convertSuccess));
            if (product.PegDepth > 0) product.NumberOfPegHoles = 1;

            //If we are merchandising as a tray then we need to make up some tray
            //values as apollo doesn't have any O_O
            if (apolloMerchMethod == ApolloMerchMethodType.TrayPack)
            {
                Byte trayHigh = 0;
                Byte trayWide = 0;
                Byte trayDeep = 0;
                Single trayThickHeight = 0;
                Single trayThickWidth = 0;
                Single trayThickDepth = 0;
                if (product.CaseHeight > 0)
                {
                    trayHigh = (Byte)Math.Max(product.CaseHeight / product.Height, 1);
                    trayThickHeight = trayHigh > 0 ? Math.Max(product.CaseHeight - (product.Height * trayHigh), 0) : 0;
                }
                if (product.CaseWidth > 0)
                {
                    trayWide = (Byte)Math.Max(product.CaseWidth / product.Width, 1);
                    trayThickWidth = trayWide > 0 ? Math.Max((product.CaseWidth - (product.Width * trayWide)) / 2, 0) : 0;
                }
                if (product.CaseDepth > 0)
                {
                    trayDeep = (Byte)Math.Max(product.CaseDepth / product.Depth, 1);
                    trayThickDepth = trayDeep > 0 ? Math.Max((product.CaseDepth - (product.Depth * trayDeep)) / 2, 0) : 0;
                }

                product.TrayHeight = product.CaseHeight;
                product.TrayWidth = product.CaseWidth;
                product.TrayDepth = product.CaseDepth;
                product.TrayHigh = trayHigh;
                product.TrayWide = trayWide;
                product.TrayDeep = trayDeep;
                product.TrayPackUnits = (Int16)(product.TrayHigh * product.TrayWide * product.TrayDeep);
            }

            #region Squeeze
            ////PO_XGAP closly matches our finger space to the side.
            //product.FingerSpaceToTheSide = (Single)SpacePlanningImportHelper.ParseSingle(position.PO_XGAP, false, out convertSuccess);
            Single xGap = (Single)SpacePlanningImportHelper.ParseSingle(position.PO_XGAP, false, out convertSuccess);
            if (xGap >= 0)
            {
                // +ve PO_XGAP closly matches our finger space to the side.
                product.FingerSpaceToTheSide = (Single)SpacePlanningImportHelper.ParseSingle(position.PO_XGAP, false, out convertSuccess);
            }
            else
            {
                // -ve PO_XGAP relates to squeeze on the position. This may be squeezewidth, squeezeheight or squeezedepth depending on the orientation
                switch ((PlanogramPositionOrientationType)orientationType)
                {
                    case PlanogramPositionOrientationType.Front0:
                    case PlanogramPositionOrientationType.Front180:
                    case PlanogramPositionOrientationType.Top0:
                    case PlanogramPositionOrientationType.Top180:
                    case PlanogramPositionOrientationType.Back0:
                    case PlanogramPositionOrientationType.Back180:
                    case PlanogramPositionOrientationType.Bottom0:
                    case PlanogramPositionOrientationType.Bottom180:
                        product.SqueezeWidth = 1 - ((0 - xGap) / product.Width);
                        break;

                    case PlanogramPositionOrientationType.Left0:
                    case PlanogramPositionOrientationType.Left180:
                    case PlanogramPositionOrientationType.Top90:
                    case PlanogramPositionOrientationType.Top270:
                    case PlanogramPositionOrientationType.Right0:
                    case PlanogramPositionOrientationType.Right180:
                    case PlanogramPositionOrientationType.Bottom90:
                    case PlanogramPositionOrientationType.Bottom270:
                        product.SqueezeDepth = 1 - ((0 - xGap) / product.Depth);
                        break;

                    case PlanogramPositionOrientationType.Front90:
                    case PlanogramPositionOrientationType.Front270:
                    case PlanogramPositionOrientationType.Left90:
                    case PlanogramPositionOrientationType.Left270:
                    case PlanogramPositionOrientationType.Back90:
                    case PlanogramPositionOrientationType.Back270:
                    case PlanogramPositionOrientationType.Right90:
                    case PlanogramPositionOrientationType.Right270:
                        product.SqueezeHeight = 1 - ((0 - xGap) / product.Height);
                        break;
                }
            }
            #endregion

            return new PlanogramPositionDto()
            {
                Id = position.GetHashCode(),
                PlanogramId = planogramId,
                PlanogramFixtureItemId = sectionData.GetHashCode(),
                PlanogramFixtureComponentId = currentComponentId,
                PlanogramSubComponentId = component.GetHashCode(),
                PlanogramProductId = product.Id,
                X = xPos,
                Y = yPos,
                Z = zPos,
                Slope = 0,
                Angle = 0,
                Roll = 0,
                FacingsHigh = facingsHigh,
                FacingsWide = facingsWide,
                FacingsDeep = facingsDeep,
                MerchandisingStyle = merchandisingStyle,
                OrientationType = orientationType,
                FacingsXHigh = facingsXHigh,
                FacingsXWide = facingsXWide,
                FacingsXDeep = facingsXDeep,
                MerchandisingStyleX = merchandisingStyleX,
                OrientationTypeX = orientationTypeX,
                IsXPlacedLeft = isXPlacedLeft,
                FacingsYHigh = facingsYHigh,
                FacingsYWide = facingsYWide,
                FacingsYDeep = facingsYDeep,
                MerchandisingStyleY = merchandisingStyleY,
                OrientationTypeY = orientationTypeY,
                IsYPlacedBottom = false,
                FacingsZHigh = facingsZHigh,
                FacingsZWide = facingsZWide,
                FacingsZDeep = facingsZDeep,
                MerchandisingStyleZ = merchandisingStyleZ,
                OrientationTypeZ = orientationTypeZ,
                IsZPlacedFront = false,
                Sequence = 1,
                SequenceX = 1,
                SequenceY = 1,
                SequenceZ = 1,
                UnitsHigh = 1,
                UnitsWide = 1,
                UnitsDeep = 1,
                TotalUnits = 1,
                DepthSqueeze = 1,
                DepthSqueezeX = 1,
                DepthSqueezeY = 1,
                DepthSqueezeZ = 1,
                HorizontalSqueeze = 1,
                HorizontalSqueezeX = 1,
                HorizontalSqueezeY = 1,
                HorizontalSqueezeZ = 1,
                VerticalSqueeze = 1,
                VerticalSqueezeX = 1,
                VerticalSqueezeY = 1,
                VerticalSqueezeZ = 1,
                PositionSequenceNumber = (Int16?)SpacePlanningImportHelper.ParseInt16(position.PO_POSITION, true, out convertSuccess)
            };
        }

        /// <summary>
        /// Encodes a planogram position dto
        /// </summary>
        private static String EncodePlanogramPositionDto(PlanogramPositionDto dto)
        {
            return null;
        }
        #endregion

        #region PlanogramPerformance

        /// <summary>
        /// Creates a new performance data dto or updates an existing one
        /// </summary>
        /// <param name="planogramProductDto"></param>
        /// <param name="source"></param>
        private void DecodePlanogramPerformanceDto(PlanogramProductDto planogramProductDto, Object source)
        {
            PlanogramPerformanceDataDto planogramPerformanceDataDto = _dalCache.PlanogramPerformanceDataDtoList.FirstOrDefault(p => p.PlanogramProductId.Equals(planogramProductDto.Id));
            Boolean insert = false;
            if (planogramPerformanceDataDto == null)
            {
                insert = true;
                planogramPerformanceDataDto = new PlanogramPerformanceDataDto()
                {
                    Id = IdentityHelper.GetNextInt32(),
                    PlanogramPerformanceId = 1,
                    PlanogramProductId = planogramProductDto.Id
                };
            }
            Type sourceType = source.GetType();
            Type dtoType = typeof(PlanogramPerformanceDataDto);
           // Dictionary<String, PropertyInfo> properties =  typeof(PlanogramPerformanceDataDto).GetProperties().ToDictionary(d => d.Name);
           // Dictionary<String, PropertyInfo> sourceProperties =   sourceType.GetProperties().ToDictionary(d => d.Name);
           
            foreach (PlanogramMetricMappingDto mapping in _dalCache.Context.MetricMappings)
            {
                String[] sourceList = mapping.Source.Split('|');
                if (sourceType.Name.Equals(sourceList[0], StringComparison.InvariantCultureIgnoreCase))
                {
                    String propertyName = "P" + mapping.MetricId.ToString();
                    PropertyInfo property = ApolloImportHelper.GetPropertyInfo(dtoType, propertyName);
                    PropertyInfo sourceProperty = ApolloImportHelper.GetPropertyInfo(sourceType, sourceList[1]);
                    if (property != null &&
                        sourceProperty != null)
                    {
                        MapSetValue(planogramPerformanceDataDto, source, property, sourceProperty, PlanogramEventLogAffectedType.Performance);
                    }
                }
            }

            if (insert)
            {
                _dalCache.PlanogramPerformanceDataDtoList.Add(planogramPerformanceDataDto);
            }
        }

        #endregion
        #endregion

        #region Methods

        /// <summary>
        /// Loads all dtos from file
        /// </summary>
        /// <param name="id"></param>
        public void LoadDtos()
        {
            Boolean convertSuccess = false; 
            IEnumerable<PlanogramFieldMappingDto> planogramFieldMappings = this._dalCache.Context.PlanogramMappings;
            IEnumerable<PlanogramFieldMappingDto> fixtureFieldMappings = this._dalCache.Context.FixtureMappings;
            IEnumerable<PlanogramFieldMappingDto> componentFieldMappings = this._dalCache.Context.ComponentMappings;
            IEnumerable<PlanogramFieldMappingDto> productFieldMappings = this._dalCache.Context.ProductMappings;
            
            Dictionary<String, PlanogramProductDto> productLookup = new Dictionary<String, PlanogramProductDto>();
            List<String> productGtins = new List<string>();
            ApolloDataSetInfo info = this._originalDataSet.Items.FirstOrDefault(s => s is ApolloDataSetInfo) as ApolloDataSetInfo;
            ApolloDataSetStore store = this._originalDataSet.Items.FirstOrDefault(s => s is ApolloDataSetStore) as ApolloDataSetStore;
            ApolloDataSetRetailer retailer = this._originalDataSet.Items.FirstOrDefault(s => s is ApolloDataSetRetailer) as ApolloDataSetRetailer;
            ApolloDataSetSection section = this._originalDataSet.Items.FirstOrDefault(s => s is ApolloDataSetSection) as ApolloDataSetSection;
            ApolloDataSetProducts products = this._originalDataSet.Items.FirstOrDefault(s => s is ApolloDataSetProducts) as ApolloDataSetProducts;


            //  Check the version is the right one...
            if (info != null)
            {
                String apolloVersion = info.ApolloVersion;
                Int32 lastDot = apolloVersion.LastIndexOf(".", StringComparison.Ordinal);
                String fileVersion = apolloVersion.Substring(0, lastDot);
                if (!ApolloImportHelper.SupportedVersions.Any(version => String.Equals(version, fileVersion)))
                    AddImportEventLogEntry(String.Format(Message.Import_VersionMismatch_Description, fileVersion),
                                     String.Format(Message.Import_VersionMismatch_Content, ApolloImportHelper.SupportedVersions.Last()),
                                     PlanogramEventLogAffectedType.Planogram,
                                     PlanogramEventLogEntryType.Information);
            }
            else
            {
                AddImportEventLogEntry(String.Format(Message.Import_VersionMismatch_Description, Language.Import_VersionMismatch_Unknown),
                                 String.Format(Message.Import_VersionMismatch_Content, ApolloImportHelper.SupportedVersions.Last()),
                                 PlanogramEventLogAffectedType.Planogram);
            }

            CreatePlanogramPerformanceMetrics();


            Int32 currentComponentId = 0;
            if (section != null)
            {
                this._dalCache.PackageDtoList.Add(DecodePackageDto(_id));
                this._dalCache.PlanogramDtoList.Add(DecodePlanogramDto(_id, info, section, store, retailer, planogramFieldMappings));

                if (products != null)
                {
                    Dictionary<String, ApolloDataSetSectionSectionProductsSectionProduct> sectionProductLookup = new Dictionary<String, ApolloDataSetSectionSectionProductsSectionProduct>();
                    if (section.SectionProducts != null)
                    {
                        sectionProductLookup = section.SectionProducts.ToDictionary(p => p.PR_UPC);
                    }
                    if (products.Product != null)
                    {
                        foreach (ApolloDataSetProductsProduct product in products.Product)
                        {
                            PlanogramProductDto dto = DecodePlanogramProductDto(product, productLookup, productFieldMappings, productGtins);

                            ApolloDataSetSectionSectionProductsSectionProduct sectionProduct = null;
                            if (productFieldMappings != null && sectionProductLookup.TryGetValue(product.PR_UPC, out sectionProduct))
                            {
                                MapCustomAttributes(dto, productFieldMappings, sectionProduct, CustomAttributeDataPlanogramParentType.PlanogramProduct, dto.Id, PlanogramEventLogAffectedType.Products);
                                DecodePlanogramPerformanceDto(dto, sectionProduct);
                            }

                            this._dalCache.PlanogramProductDtoList.Add(dto);
                            DecodePlanogramPerformanceDto(dto, product);
                        }
                    }
                }

                if (section.SectionData != null)
                {
                    foreach (ApolloDataSetSectionSectionData sectionData in section.SectionData)
                    {
                        Boolean crunchMode = false;
                        if ((Int32)SpacePlanningImportHelper.ParseInt32(sectionData.SE_CRUNCH, false, out convertSuccess) != 0) crunchMode = true;

                        this._dalCache.PlanogramFixtureItemDtoList.Add(DecodePlanogramFixtureItemDto(sectionData));
                        this._dalCache.PlanogramFixtureDtoList.Add(DecodePlanogramFixtureDto(sectionData, fixtureFieldMappings));
                        CreateBackboard(sectionData);

                        //There is no base in apollo plans :/
                        //CreateBase(sectionData);

                        if (section.Shelves == null) continue;
                        foreach (ApolloDataSetSectionShelves shelves in section.Shelves)
                        {
                            if (shelves.Shelf == null) continue;
                            foreach (var shelfGroup in shelves.Shelf.GroupBy(s => s.ShelfData.First().SH_RESERVED5))
                            {
                                AdvancedApolloFixtureComponent fixtureComponent = new AdvancedApolloFixtureComponent(shelfGroup, _uom);
                                currentComponentId = fixtureComponent.Id;
                                this._dalCache.PlanogramFixtureComponentDtoList.Add(DecodePlanogramFixtureComponentDto(sectionData, fixtureComponent));
                                this._dalCache.PlanogramComponentDtoList.Add(DecodePlanogramComponentDto(fixtureComponent, componentFieldMappings));

                                foreach (ApolloDataSetSectionShelvesShelf shelf in fixtureComponent.Shelves)
                                {
                                    if (shelf.ShelfData == null) continue;
                                    foreach (ApolloDataSetSectionShelvesShelfShelfData data in shelf.ShelfData)
                                    {
                                        ApolloShelfType shelfType = (ApolloShelfType)SpacePlanningImportHelper.ParseByte(data.SH_SHELFTYPE, false, out convertSuccess);
                                        //if (shelfType == ShelfType.BoundingBox || shelfType == ShelfType.FreeHandSurface) continue;
                                        if (shelfType == ApolloShelfType.BoundingBox) continue;
                                        this._dalCache.PlanogramSubComponentDtoList.Add(DecodePlanogramSubComponentDto(currentComponentId, fixtureComponent, data, crunchMode));

                                        if (shelf.Positions == null) continue;
                                        foreach (PositionsPosition position in shelf.Positions)
                                        {
                                            PlanogramProductDto product = null;
                                            if (productLookup.TryGetValue(position.PO_UPC, out product))
                                            {
                                                this._dalCache.PlanogramPositionDtoList.Add(DecodePlanogramPositionDto(position, currentComponentId, sectionData, data, product));
                                                DecodePlanogramPerformanceDto(product, position);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Object cdtID = CreatePlanogramConsumerDecisionTreeDto().Id;
            CreatePlanogramConsumerDecisionTreeLevelDtos(cdtID);
            CreatePlanogramConsumerDecisionTreeNodeDtos(cdtID);
        }

        private void AddImportEventLogEntry(String description, String content, PlanogramEventLogAffectedType affectedType, PlanogramEventLogEntryType entryType = PlanogramEventLogEntryType.Warning)
        {
            SpacePlanningImportHelper.CreateErrorEventLogEntry(planogramId, _dalCache.PlanogramEventLogDtoList, description, content, entryType, PlanogramEventLogEventType.Import, affectedType, 1);
        }

        #endregion

        #region Deserialize Methods

        /// <summary>
        /// Deserializes an apollo file into a ApolloDataSet object using the .net deserializer
        /// </summary>
        /// <param name="file"></param>
        private void XmlDeserializePlan(AbstractFile file)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ApolloDataSet));
            serializer.UnknownElement += serializer_UnknownElement;
            using (Stream fileStream = file.OpenRead(FileShare.Read))
            {
                _originalDataSet = serializer.Deserialize(fileStream) as ApolloDataSet;
            }
        }

        private void serializer_UnknownElement(object sender, XmlElementEventArgs e)
        {
            //Do nothing, this is here just so we don't error if a new version adds a field in
        }

        /// <summary>
        /// Custom method to deserialize plan. This is alot faster than the first time XmlDeserializePlan runs
        /// in a session, but a little slower after that as the .net deserializer caches itself after the first run.
        /// </summary>
        /// <param name="file"></param>
        private void DeserializePlan(AbstractFile file)
        {
            using (Stream fileStream = file.OpenRead(FileShare.Read))
            {
                ApolloDataSet apollo = new ApolloDataSet();
                using (XmlReader reader = XmlReader.Create(fileStream))
                {
                    List<Object> apolloItems = new List<Object>();
                    const String infoName = "Info";
                    ApolloDataSetInfo currentInfo;
                    ApolloDataSetRetailer currentRetailer;
                    ApolloDataSetStore currentStore;
                    ApolloDataSetProducts currentProducts;
                    ApolloDataSetSection currentSection = null;
                    List<ApolloDataSetSectionShelves> currentSectionShelfGroups = null;
                    ApolloDataSetSectionShelves currentSectionShelfGroup = null;
                    List<ApolloDataSetSectionShelvesShelf> currentShelfGroupShelves = null;
                    ApolloDataSetSectionShelvesShelf currentShelfGroupShelf = null;
                    List<ApolloDataSetSectionShelvesShelfShelfData> currentShelfGroupShelfData = null;
                    List<PositionsPosition> currentShelfGroupShelfPositions = null;
                    while (reader.Read())
                    {
                        if (reader.IsEmptyElement)
                        {
                            continue;
                        }
                        switch (reader.NodeType)
                        {
                            case System.Xml.XmlNodeType.Element:
                                {
                                    switch (reader.Name)
                                    {
                                        case infoName:
                                            currentInfo = new ApolloDataSetInfo();
                                            apolloItems.Add(currentInfo);

                                            DeserializeItem<ApolloDataSetInfo>(reader, currentInfo, infoName);

                                            break;
                                        case "Retailer":
                                            currentRetailer = new ApolloDataSetRetailer();
                                            apolloItems.Add(currentRetailer);
                                            DeserializeItem<ApolloDataSetRetailer>(reader, currentRetailer, "Retailer");
                                            break;
                                        case "Store":
                                            currentStore = new ApolloDataSetStore();
                                            apolloItems.Add(currentStore);
                                            DeserializeItem<ApolloDataSetStore>(reader, currentStore, "Store");
                                            break;
                                        case "Products":
                                            currentProducts = new ApolloDataSetProducts();
                                            apolloItems.Add(currentProducts);
                                            currentProducts.Product = DeserializeItems<ApolloDataSetProductsProduct>(reader, "Products");
                                            break;
                                        case "Section":
                                            currentSection = new ApolloDataSetSection();
                                            currentSectionShelfGroups = new List<ApolloDataSetSectionShelves>();
                                            apolloItems.Add(currentSection);
                                            break;
                                        case "SectionProducts":
                                            if (currentSection != null)
                                            {
                                                currentSection.SectionProducts = DeserializeItems<ApolloDataSetSectionSectionProductsSectionProduct>(reader, "SectionProducts");
                                            }
                                            break;
                                        case "SectionData":
                                            if (currentSection != null)
                                            {
                                                ApolloDataSetSectionSectionData sectionData = new ApolloDataSetSectionSectionData();
                                                DeserializeItem<ApolloDataSetSectionSectionData>(reader, sectionData, "SectionData");
                                                currentSection.SectionData = new ApolloDataSetSectionSectionData[] { sectionData };
                                            }
                                            break;
                                        case "Shelves":
                                            currentSectionShelfGroup = new ApolloDataSetSectionShelves();
                                            currentShelfGroupShelves = new List<ApolloDataSetSectionShelvesShelf>();
                                            break;
                                        case "Shelf":
                                            currentShelfGroupShelf = new ApolloDataSetSectionShelvesShelf();
                                            currentShelfGroupShelfData = new List<ApolloDataSetSectionShelvesShelfShelfData>();
                                            currentShelfGroupShelfPositions = new List<PositionsPosition>();
                                            break;
                                        case "ShelfData":
                                            if (currentShelfGroupShelf != null)
                                            {
                                                ApolloDataSetSectionShelvesShelfShelfData shelfData = new ApolloDataSetSectionShelvesShelfShelfData();
                                                DeserializeItem<ApolloDataSetSectionShelvesShelfShelfData>(reader, shelfData, "ShelfData");
                                                currentShelfGroupShelfData.Add(shelfData);
                                            }
                                            break;
                                        case "Positions":
                                            if (currentShelfGroupShelf != null)
                                            {
                                                PositionsPosition positionData = new PositionsPosition();

                                                currentShelfGroupShelfPositions.AddRange(DeserializeItems<PositionsPosition>(reader, "Positions"));
                                            }
                                            break;
                                    }
                                }
                                break;
                            case System.Xml.XmlNodeType.EndElement:
                                {
                                    switch (reader.Name)
                                    {
                                        case "Info":
                                            currentInfo = null;
                                            break;
                                        case "Retailer":
                                            currentRetailer = null;
                                            break;
                                        case "Store":
                                            currentStore = null;
                                            break;
                                        case "Products":
                                            currentProducts = null;
                                            break;
                                        case "Section":
                                            currentSection = null;
                                            currentSectionShelfGroups = null;
                                            break;
                                        case "Shelves":

                                            if (currentSectionShelfGroup != null)
                                            {
                                                currentSectionShelfGroup.Shelf = currentShelfGroupShelves.ToArray();

                                                if (currentSection != null)
                                                {
                                                    currentSection.Shelves = new ApolloDataSetSectionShelves[] { currentSectionShelfGroup };
                                                }
                                            }

                                            currentSectionShelfGroup = null;
                                            break;
                                        case "Shelf":

                                            if (currentShelfGroupShelves != null)
                                            {
                                                currentShelfGroupShelf.ShelfData = currentShelfGroupShelfData.ToArray();
                                                currentShelfGroupShelf.Positions = currentShelfGroupShelfPositions.ToArray();
                                                currentShelfGroupShelves.Add(currentShelfGroupShelf);
                                            }



                                            currentShelfGroupShelf = null;
                                            break;
                                    }
                                }
                                break;
                        }
                    }

                    apollo.Items = apolloItems.ToArray();
                }
                // fileStream.Position = 0;
                // _originalDataSet = serializer.Deserialize(fileStream) as ApolloDataSet;
                _originalDataSet = apollo;
            }
        }

        /// <summary>
        /// Continues to read from the xml reader and deserializes items of one type into
        /// a new array of that item type.
        /// </summary>
        /// <typeparam name="K"></typeparam>
        /// <param name="reader"></param>
        /// <param name="callingName">The Node element name that will be used to exit the read operation</param>
        /// <returns></returns>
        private K[] DeserializeItems<K>(XmlReader reader, String callingName)
        {
            List<K> output = new List<K>();
            Type itemType = typeof(K);
            ConstructorInfo itemConstructor = ApolloImportHelper.GetConstructorInfo(itemType);
            Boolean end = false;
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case System.Xml.XmlNodeType.Element:
                        {
                            if (!String.IsNullOrWhiteSpace(reader.Name))
                            {
                                K newItem = (K)itemConstructor.Invoke(null);
                                DeserializeItem<K>(reader, newItem, reader.Name);
                                output.Add(newItem);
                            }
                        }
                        break;
                    case System.Xml.XmlNodeType.EndElement:
                        {
                            if (reader.Name == callingName)
                            {
                                end = true;
                            }
                        }
                        break;
                }

                if (end) break;
            }

            return output.ToArray();
        }

        /// <summary>
        /// Continues to read from the xml reader and deserializes properties from the xml file into
        /// the supplied item. This does not take into account collections of objects.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reader"></param>
        /// <param name="item"></param>
        /// <param name="callingName">The Node element name that will be used to exit the read operation</param>
        private void DeserializeItem<T>(XmlReader reader, T item, String callingName)
        {
            Type type = typeof(T);

            Boolean end = false;
            String propertyInfo = null;
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case System.Xml.XmlNodeType.Element:
                        {
                            if (!String.IsNullOrWhiteSpace(reader.Name))
                            {
                                propertyInfo = reader.Name;// ApolloImportHelper.GetPropertyInfo(type, reader.Name);                               
                            }
                        }
                        break;
                    case System.Xml.XmlNodeType.Text:
                        {
                            if (propertyInfo != null)
                            {
                                if (!String.IsNullOrWhiteSpace(reader.Value))
                                {
                                    ApolloImportHelper.InvokeSetter(type, propertyInfo, item, reader.Value);
                                    //propertyInfo.SetValue(item, reader.Value, null);
                                }
                                else
                                {
                                    ApolloImportHelper.InvokeSetter(type, propertyInfo, item, String.Empty);
                                    //propertyInfo.SetValue(item, String.Empty, null);
                                }
                            }
                        }
                        break;
                    case System.Xml.XmlNodeType.EndElement:
                        {
                            if (reader.Name == callingName)
                            {
                                end = true;
                            }
                            else
                            {
                                propertyInfo = null;
                            }
                        }
                        break;
                }

                if (end) break;
            }
        }

        #endregion

        #region Export To Apollo Methods

        /// <summary>
        /// Load the current package's planogram information into the ApolloDataSet
        /// </summary>
        private void LoadApolloDataSetForExport()
        {
            //create the new dataset to populate
            _exportDataSet = new ApolloDataSet();

            List<Object> apolloItems = new List<Object>();

            PlanogramDto planogramDto = this.DalCache.PlanogramDtoList.First();
            String newSectionId = planogramDto.Id.ToString();

            try
            {
                //Log start
                DalCache.LogExportInformation(
                    Language.ExportLog_StartingExport_Description, String.Format(Language.ExportLog_StartingExport_Content, planogramDto.Name), 
                    PlanogramEventLogAffectedType.Planogram); 
                
                ApolloDataSetSection section = new ApolloDataSetSection();
                
                #region Header Info,Retailer,Store
                ApolloDataSetInfo info = CreateApolloInfo();
                apolloItems.Add(info);

                ApolloDataSetRetailer retailer = CreateApolloRetailer();
                apolloItems.Add(retailer);

                ApolloDataSetStore store = CreateApolloStore();
                apolloItems.Add(store);
                #endregion

                // Products
                ApolloDataSetProducts products = CreateApolloProducts(newSectionId);
                apolloItems.Add(products);

                //Planogram Annotations
                CreateApolloTextboxAnnotations();

                // SectionData
                ApolloDataSetSectionSectionData[] sectionData = CreateApolloSection(newSectionId);
                section.SectionData = sectionData;

                //SectionProducts
                ApolloDataSetSectionSectionProductsSectionProduct[] sectionProductData = CreateApolloSectionProducts();
                section.SectionProducts = sectionProductData;

                //Section Shelves
                ApolloDataSetSectionShelves[] shelvesData = CreateApolloSectionShelves();

                //Section Shelves Shopping Cart
                PositionsPosition[][] shoppingCartPositions = CreateApolloShoppingCart();

                shelvesData[0].ShoppingCart = shoppingCartPositions;

                //Add the Shelves array to the section
                section.Shelves = shelvesData;

                //Add the section to the items array
                apolloItems.Add(section);

                //Set the items array in our export object
                _exportDataSet.Items = apolloItems.ToArray();

                //Log end
                DalCache.LogExportInformation(
                    Language.ExportLog_ExportComplete_Description,
                    String.Format(Language.ExportLog_ExportComplete_Content, planogramDto.Name, this.DalCache.FilePath),
                    PlanogramEventLogAffectedType.Planogram); 
            }
            catch (Exception ex)
            {
                //TODO log error that dataset population failed
                throw;
            }
        }

        /// <summary>
        /// Creates an Apollo dataset Info XML Element from the Planogram being exported
        /// </summary>
        /// <returns>The populated ApolloDataSetInfo XML Element</returns>
        private ApolloDataSetInfo CreateApolloInfo()
        {
            
            ApolloDataSetInfo info = new ApolloDataSetInfo();
            info.ApolloVersion = ApolloExportHelper.ExportVersion;
            info.SectionFeatureVersion = "80";
            info.LoaderVersion = "1.0.0";

            PlanogramDto planogramDto = this._dalCache.PlanogramDtoList.First();
            String uom = ApolloExportHelper.GetApolloInfoMeasure(planogramDto);
            info.Measure = uom;

            info.EANSetting = "0";
            return info;
        }

        /// <summary>
        /// Creates an Apollo dataset Retailer XML Element from the Planogram being exported
        /// </summary>
        /// <returns>The populated ApolloDataSetRetailer XML Element</returns>
        private ApolloDataSetRetailer CreateApolloRetailer()
        {
            ApolloDataSetRetailer item = new ApolloDataSetRetailer();
            item.RE_RETAILER = "0";
            item.RE_NAME = String.Empty;
            item.RE_ADDRESS1 = String.Empty;
            item.RE_ADDRESS2 = String.Empty;
            item.RE_CITY = String.Empty;
            item.RE_STATE = String.Empty;
            item.RE_ZIP = String.Empty;
            item.RE_PHONE = String.Empty;
            item.RE_NUMSTORES = "0";
            item.RE_CONTACT = String.Empty;
            return item;
        }

        /// <summary>
        /// Creates an Apollo dataset Store XML Element from the Planogram being exported
        /// </summary>
        /// <returns>The populated ApolloDataSetStore XML Element</returns>
        private ApolloDataSetStore CreateApolloStore()
        {
            PlanogramDto planogramDto = this._dalCache.PlanogramDtoList.First(); 
            
            ApolloDataSetStore item = new ApolloDataSetStore();
            item.ST_STORE = planogramDto.LocationCode != null ? planogramDto.LocationCode : "0";
            item.ST_NAME = planogramDto.LocationName != null ? planogramDto.LocationName : String.Empty;
            item.ST_ADDRESS1 = String.Empty;
            item.ST_ADDRESS2 = String.Empty;
            item.ST_CITY = String.Empty;
            item.ST_STATE = String.Empty;
            item.ST_ZIP = String.Empty;
            item.ST_PHONE = String.Empty;
            item.ST_USER1 = String.Empty;
            item.ST_USER2 = String.Empty;
            item.ST_USER3 = String.Empty;
            item.ST_USER4 = String.Empty;
            item.ST_CLUSTER = String.Empty;
            item.ST_DISTRICT = "0";
            item.ST_HEIGHT = "0.0";
            item.ST_WIDTH = "0.0";
            item.ST_DEPTH = "0.0";
            item.ST_ACV = "0.0";
            item.ST_CONTACT = String.Empty;
            return item;
        }

        /// <summary>
        /// Creates an Apollo dataset Products XML Element from the Planogram being exported
        /// </summary>
        /// <returns>The populated ApolloDataSetProducts XML Element object</returns>
        private ApolloDataSetProducts CreateApolloProducts(String newSectionId)
        {
            IEnumerable<PlanogramFieldMappingDto> mappings = this._dalCache.ExportContext.ProductMappings;

            List<ApolloDataSetProductsProduct> products = new List<ApolloDataSetProductsProduct>();

            //Add each dto to the array
            foreach (PlanogramProductDto productDto in _dalCache.PlanogramProductDtoList.OrderBy(p => p.Gtin))
            {
                //Only add the product if the position exists - if the plan is saved in Apollo
                //it will strip the unused products anyway
                PlanogramPositionDto positionDto = DalCache.PlanogramPositionDtoList.FirstOrDefault(p => p.PlanogramProductId.Equals(productDto.Id));

                if (positionDto != null)
                {
                    //Find its element type
                    PlanogramSubComponentDto subComponent = DalCache.PlanogramSubComponentDtoList.FirstOrDefault(p => p.Id.Equals(positionDto.PlanogramSubComponentId));
                    ApolloShelfType shelfType = ApolloShelfType.Shelf;
                    shelfType = ApolloExportHelper.GetApolloShelfType(subComponent);

                    //Create the export item
                    ApolloDataSetProductsProduct product = new ApolloDataSetProductsProduct();

                    try
                    {
                        //Get the custom attribute dto for the planogram
                        CustomAttributeDataDto customAttrDataDto = _dalCache.CustomAttributeDataDtoList.First(c =>
                            c.ParentId.Equals(productDto.Id) && c.ParentType == (Byte)CustomAttributeDataPlanogramParentType.PlanogramProduct);

                        #region Create Product to Export
                        //mapped fields
                        product.PR_UPC = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductUPC, productDto, customAttrDataDto, mappings, productDto.Gtin);
                        product.PR_STOCKCODE = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductSTOCKCODE, productDto, customAttrDataDto, mappings);
                        product.PR_LONGDESC = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductLONGDESC, productDto, customAttrDataDto, mappings);

                        product.PR_SIZE = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductSIZE, productDto, customAttrDataDto, mappings, "0.0");
                        product.PR_UOM = productDto.UnitOfMeasure;
                        product.PR_SCREENDESC = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductSCREENDESC, productDto, customAttrDataDto, mappings);
                        product.PR_CHAR = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductCHAR, productDto, customAttrDataDto, mappings);
                        product.PR_PLOTDESC1 = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductPLOTDESC1, productDto, customAttrDataDto, mappings);
                        product.PR_PLOTDESC2 = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductPLOTDESC2, productDto, customAttrDataDto, mappings);
                        product.PR_SYMBOL = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductSYMBOL, productDto, customAttrDataDto, mappings);
                        product.PR_CATEGORY = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductCATEGORY, productDto, customAttrDataDto, mappings);
                        String subCategory = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductSUBCATEGORY, productDto, customAttrDataDto, mappings);
                        product.PR_SUBCATEGORY = subCategory.Length > 5 ? subCategory.Substring(0, 5) : subCategory;
                        product.PR_COMPONENT = String.Empty;
                        product.PR_BRAND = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductBRAND, productDto, customAttrDataDto, mappings);
                        product.PR_MANUFACTURER = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductMANUFACTURER, productDto, customAttrDataDto, mappings);
                        product.PR_PACKAGE = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductPACKAGE, productDto, customAttrDataDto, mappings);
                        product.PR_DISTRIBUTION = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductDISTRIBUTION, productDto, customAttrDataDto, mappings, "0");
                        product.PR_MULTIPACK = "1";
                        product.PR_CASEPACK = productDto.CasePackUnits.ToString();
                        product.PR_MOVEMENTADJ = "0.0";
                        product.PR_PRODHEIGHT = ApolloExportHelper.ParseNumericValue(productDto.Height, false);
                        product.PR_PRODWIDTH = ApolloExportHelper.ParseNumericValue(productDto.Width, false);
                        product.PR_PRODDEPTH = ApolloExportHelper.ParseNumericValue(productDto.Depth, false);

                        //Case values - Apollo uses case dimensions for both trays and cases
                        //It gets more complicated for multi-sited products
                        //if merched as cases and trays, use the case values otherwise use the tray/case values as approriate
                        //1. Get a list of all position for the product
                        List<PlanogramPositionDto> positionDtos = DalCache.PlanogramPositionDtoList.Where(p => p.PlanogramProductId.Equals(productDto.Id)).ToList();
                        Boolean isMerchedInTrays = false;
                        Boolean isMerchedInCases = false;
                        ApolloExportHelper.IsProductMerchandisedInTraysOrCases(productDto, positionDtos, out isMerchedInTrays, out isMerchedInCases);
                        if (isMerchedInTrays && isMerchedInCases)
                        {
                            this.DalCache.LogExportWarning(
                                Language.ExportLog_ProductWarning_TraysAndCases_Description,
                                String.Format(Language.ExportLog_ProductWarning_TraysAndCases_Content, productDto.Gtin, productDto.Name),
                                PlanogramEventLogAffectedType.Products);
                        }
                        product.PR_CASEHEIGHT = ApolloExportHelper.GetApolloProductCaseHeight(productDto, positionDtos).ToString();
                        product.PR_CASEWIDTH = ApolloExportHelper.GetApolloProductCaseWidth(productDto, positionDtos).ToString();
                        product.PR_CASEDEPTH = ApolloExportHelper.GetApolloProductCaseDepth(productDto, positionDtos).ToString();
                        product.PR_CASEWEIGHT = "0.0";

                        //Nesting Height
                        if (productDto.NestingHeight > 0)
                        {
                            product.PR_NESTING = (productDto.Height - productDto.NestingHeight).ToString();
                        }
                        else
                        {
                            product.PR_NESTING = "0.0";
                        }

                        product.PR_OVERHANG = ApolloExportHelper.ParseNumericValue(productDto.FrontOverhang, false);
                        //Peg X position for Apollo is 0 if centred on the product width
                        product.PR_XPEGHOLE = ApolloExportHelper.ParseNumericValue(productDto.PegX, false);
                        Single pegY = PlanogramPositionDetails.GetDefaultPegYOffset(productDto.PegY, PlanogramLengthUnitOfMeasureType.Unknown);
                        product.PR_YPEGHOLE = ApolloExportHelper.ParseNumericValue(productDto.Height - pegY, false);
                        //Finger space above only valid if > 0
                        product.PR_FINGER = ApolloExportHelper.ParseNumericValue(productDto.FingerSpaceAbove, false);

                        #region MAXFRONTS,LAYOVERS,FRONTSDEEP fields
                        //In CCM V8:
                        //  Max stack of 0 = unlimited
                        //  Max Deep of 0 = unlimited
                        //  Max top cap of 0 = no top caps allowed.

                        //Steve R believes we should just let Apollo determine the facings high
                        //Find the positions  - we should use this value to ensure the Apollo plan looks like the V8 one, since Apollo auto-calculates top caps
                        //Int32 maxPositionFacingsHigh = 0;
                        //Int32 maxPositionTopCaps = 0;
                        //List<PlanogramPositionDto> positionDtos = this.DalCache.PlanogramPositionDtoList.
                        //    Where(p => p.PlanogramProductId.Equals(productDto.Id)).ToList();
                        //foreach (PlanogramPositionDto dto in positionDtos)
                        //{
                        //    maxPositionFacingsHigh = dto.FacingsHigh > maxPositionFacingsHigh ? dto.FacingsHigh : maxPositionFacingsHigh;
                        //    maxPositionTopCaps = dto.FacingsYHigh > maxPositionTopCaps ? dto.FacingsYHigh : maxPositionTopCaps;
                        //}
                        //product.PR_MAXFRONTS = (maxPositionFacingsHigh > 0 ? maxPositionFacingsHigh : 99).ToString();
                        //product.PR_MAXLAYOVERS = maxPositionTopCaps.ToString();

                        if (shelfType == ApolloShelfType.Coffin)
                        {
                            //Height and Depth are switched over
                            product.PR_MAXFRONTS = ApolloExportHelper.ParseNumericValue(productDto.MaxDeep, false);
                            if (productDto.MaxDeep == 0) product.PR_MAXFRONTS = "99";

                            product.PR_MAXFRONTSDEEP = ApolloExportHelper.ParseNumericValue(productDto.MaxStack, false);
                            if (productDto.MaxStack == 0) product.PR_MAXFRONTSDEEP = "100"; //In CCM 0 = unlimited
                        }
                        else
                        {
                            product.PR_MAXFRONTS = ApolloExportHelper.ParseNumericValue(productDto.MaxStack, false);
                            if (productDto.MaxStack == 0) product.PR_MAXFRONTS = "99";

                            product.PR_MAXFRONTSDEEP = ApolloExportHelper.ParseNumericValue(productDto.MaxDeep, false);
                            if (productDto.MaxDeep == 0) product.PR_MAXFRONTSDEEP = "100"; //In CCM 0 = unlimited
                        }
                        product.PR_MAXLAYOVERS = ApolloExportHelper.ParseNumericValue(productDto.MaxTopCap, false);


                        #endregion

                        product.PR_NOINNERPACKS = "0";
                        product.PR_NOCASESPALLET = "0";
                        product.PR_NOTRAYSCASE = "0";
                        product.PR_NETDAYSCREDIT = "0";
                        product.PR_WARERECEIVE = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductWARERECEIVE, productDto, customAttrDataDto, mappings, "0");
                        product.PR_ARRIVEAS = "0";
                        product.PR_REPLENISHMETHOD = "0";
                        product.PR_STORERECUNLOAD = "0";
                        product.PR_STOREPRICEMETHOD = "0";
                        product.PR_RACKOPENHT = "0";
                        product.PR_AVRNOCASESDSDSH = "0";
                        product.PR_ADJTOCASECOST = "0.0";
                        product.PR_MINUNITS = "0.0";
                        product.PR_MINDAYS = "0.0";
                        product.PR_MINFACING = "0.0";
                        product.PR_MINCASES = "0.0";
                        product.PR_MAXUNITS = "0.0";
                        product.PR_MAXDAYS = "0.0";
                        product.PR_MAXFACING = "0.0";
                        product.PR_MAXCASES = "0.0";
                        product.PR_RESTOCK = "0.0";
                        product.PR_SERVICE = "0.0";
                        product.PR_VARIANCE = "0.0";
                        product.PR_EXPECTEDMOVEMENT = "0.0";
                        product.PR_MAXHEIGHT = "0.0";
                        product.PR_SHARE = "0.0";
                        product.PR_SECTIONSHARE = "0.0";
                        product.PR_PATTERN = "0";
                        product.PR_SECTIONID = newSectionId;    //This will be updated later when the section is loaded

                        #region PR_COLOR Field
                        Int32 apolloColour = ApolloExportHelper.ConvertColourCCMToApollo(productDto.FillColour);
                        product.PR_COLOR = "0"; //This is an old field used when only 16 colors were available.
                        product.X06_MEASRESERVE01 = String.Format("{0}.0", apolloColour.ToString());
                        if (apolloColour != 0)
                        {
                            ApolloColourType presetColor = ApolloExportHelper.ConvertColourCCMToNearestApolloColourType(productDto.FillColour);
                            product.PR_COLOR = ((Byte)presetColor).ToString();
                        }
                        #endregion

                        #region PR_RESERVED Fields
                        product.PR_RESERVED1 = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductRESERVED1, productDto, customAttrDataDto, mappings, "0.0");
                        product.PR_RESERVED2 = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductRESERVED2, productDto, customAttrDataDto, mappings, "0.0");
                        product.PR_RESERVED3 = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductRESERVED3, productDto, customAttrDataDto, mappings, "0.0");
                        product.PR_RESERVED4 = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductRESERVED4, productDto, customAttrDataDto, mappings, "0.0");
                        product.PR_RESERVED5 = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductRESERVED5, productDto, customAttrDataDto, mappings, "0");
                        product.PR_RESERVED6 = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductRESERVED6, productDto, customAttrDataDto, mappings, "0");
                        #endregion

                        #region PR_USERMEASURE Fields
                        product.PR_USERMEASUREA = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductUSERMEASUREA, productDto, customAttrDataDto, mappings, "0");
                        product.PR_USERMEASUREB = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductUSERMEASUREB, productDto, customAttrDataDto, mappings, "0");
                        product.PR_USERMEASUREC = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductUSERMEASUREC, productDto, customAttrDataDto, mappings, "0");
                        product.PR_USERMEASURED = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductUSERMEASURED, productDto, customAttrDataDto, mappings, "0");
                        product.PR_USERMEASUREE = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductUSERMEASUREE, productDto, customAttrDataDto, mappings, "0");
                        product.PR_USERMEASUREF = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductUSERMEASUREF, productDto, customAttrDataDto, mappings, "0");
                        product.PR_USERMEASUREG = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductUSERMEASUREG, productDto, customAttrDataDto, mappings, "0");
                        product.PR_USERMEASUREH = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductUSERMEASUREH, productDto, customAttrDataDto, mappings, "0");
                        product.PR_USERMEASUREI = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductUSERMEASUREI, productDto, customAttrDataDto, mappings, "0");
                        product.PR_USERMEASUREJ = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductUSERMEASUREJ, productDto, customAttrDataDto, mappings, "0");
                        #endregion

                        #region PR_USERDESC Fields
                        product.PR_USERDESCA = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductUSERDESCA, productDto, customAttrDataDto, mappings, String.Empty);
                        product.PR_USERDESCB = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductUSERDESCB, productDto, customAttrDataDto, mappings, String.Empty);
                        product.PR_USERDESCC = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductUSERDESCC, productDto, customAttrDataDto, mappings, String.Empty);
                        product.PR_USERDESCD = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductUSERDESCD, productDto, customAttrDataDto, mappings, String.Empty);
                        product.PR_USERDESCE = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductUSERDESCE, productDto, customAttrDataDto, mappings, String.Empty);
                        product.PR_USERDESCF = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductUSERDESCF, productDto, customAttrDataDto, mappings, String.Empty);
                        product.PR_USERDESCG = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductUSERDESCG, productDto, customAttrDataDto, mappings, String.Empty);
                        product.PR_USERDESCH = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductUSERDESCH, productDto, customAttrDataDto, mappings, String.Empty);
                        product.PR_USERDESCI = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductUSERDESCI, productDto, customAttrDataDto, mappings, String.Empty);
                        product.PR_USERDESCJ = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductUSERDESCJ, productDto, customAttrDataDto, mappings, String.Empty);
                        #endregion

                        #region X06_DESCX Fields
                        //Note: X06_DESCX01  This is the CSC for Safeway
                        product.X06_DESCX01 = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductX06_DESCX01, productDto, customAttrDataDto, mappings, productDto.Gtin); //looks like its the Gtin here
                        product.X06_DESCX02 = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductX06_DESCX02, productDto, customAttrDataDto, mappings, String.Empty);
                        product.X06_DESCX03 = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductX06_DESCX03, productDto, customAttrDataDto, mappings, String.Empty);
                        product.X06_DESCX04 = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductX06_DESCX04, productDto, customAttrDataDto, mappings, String.Empty);
                        product.X06_DESCX05 = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductX06_DESCX05, productDto, customAttrDataDto, mappings, String.Empty);
                        product.X06_DESCX06 = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductX06_DESCX06, productDto, customAttrDataDto, mappings, String.Empty);
                        product.X06_DESCX07 = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductX06_DESCX07, productDto, customAttrDataDto, mappings, String.Empty);
                        product.X06_DESCX08 = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductX06_DESCX08, productDto, customAttrDataDto, mappings, String.Empty);
                        product.X06_DESCX09 = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductX06_DESCX09, productDto, customAttrDataDto, mappings, String.Empty);
                        product.X06_DESCX10 = ApolloExportHelper.GetFieldMappedValue<PlanogramProductDto>(ApolloImportHelper.ProductX06_DESCX10, productDto, customAttrDataDto, mappings, String.Empty);
                        #endregion

                        #region Unused fields that do not exist in our ApolloDataSet
                        //product.X06_MEASX011 = String.Empty;
                        //product.X06_MEASX012 = String.Empty;
                        //product.X06_MEASX013 = String.Empty;
                        //product.X06_MEASX014 = String.Empty;
                        //product.X06_MEASX015 = String.Empty;
                        //product.X06_MEASX016 = String.Empty;
                        //product.X06_MEASX017 = String.Empty;
                        //product.X06_MEASX018 = String.Empty;
                        //product.X06_MEASX019 = String.Empty;
                        //product.X06_MEASX020 = String.Empty;
                        //product.X06_MEASX021 = String.Empty;
                        //product.X06_MEASX022 = String.Empty;
                        //product.X06_MEASX023 = String.Empty;
                        //product.X06_MEASX024 = String.Empty;
                        //product.X06_MEASX025 = String.Empty;
                        //product.X06_MEASX026 = String.Empty;
                        //product.X06_MEASX027 = String.Empty;
                        //product.X06_MEASX028 = String.Empty;
                        //product.X06_MEASX029 = String.Empty;
                        //product.X06_MEASX030 = String.Empty;
                        //product.X06_MEASX031 = String.Empty;
                        //product.X06_MEASX032 = String.Empty;
                        //product.X06_MEASX033 = String.Empty;
                        //product.X06_MEASX034 = String.Empty;
                        //product.X06_MEASX035 = String.Empty;
                        //product.X06_MEASX036 = String.Empty;
                        //product.X06_MEASX037 = String.Empty;
                        //product.X06_MEASX038 = String.Empty;
                        //product.X06_MEASX039 = String.Empty;
                        //product.X06_MEASX040 = String.Empty;
                        //product.X06_MEASX041 = String.Empty;
                        //product.X06_MEASX042 = String.Empty;
                        //product.X06_MEASX043 = String.Empty;
                        //product.X06_MEASX044 = String.Empty;
                        //product.X06_MEASX045 = String.Empty;
                        //product.X06_MEASX046 = String.Empty;
                        //product.X06_MEASX047 = String.Empty;
                        //product.X06_MEASX048 = String.Empty;
                        //product.X06_MEASX049 = String.Empty;
                        //product.X06_MEASX050 = String.Empty;
                        //product.X06_MEASX051 = String.Empty;
                        //product.X06_MEASX052 = String.Empty;
                        //product.X06_MEASX053 = String.Empty;
                        //product.X06_MEASX054 = String.Empty;
                        //product.X06_MEASX055 = String.Empty;
                        //product.X06_MEASX056 = String.Empty;
                        //product.X06_MEASX057 = String.Empty;
                        //product.X06_MEASX058 = String.Empty;
                        //product.X06_MEASX059 = String.Empty;
                        //product.X06_MEASX060 = String.Empty;
                        //product.X06_MEASX061 = String.Empty;
                        //product.X06_MEASX062 = String.Empty;
                        //product.X06_MEASX063 = String.Empty;
                        //product.X06_MEASX064 = String.Empty;
                        //product.X06_MEASX065 = String.Empty;
                        //product.X06_MEASX066 = String.Empty;
                        //product.X06_MEASX067 = String.Empty;
                        //product.X06_MEASX068 = String.Empty;
                        //product.X06_MEASX069 = String.Empty;
                        //product.X06_MEASX070 = String.Empty;
                        //product.X06_MEASX071 = String.Empty;
                        //product.X06_MEASX072 = String.Empty;
                        //product.X06_MEASX073 = String.Empty;
                        //product.X06_MEASX074 = String.Empty;
                        //product.X06_MEASX075 = String.Empty;
                        //product.X06_MEASX076 = String.Empty;
                        //product.X06_MEASX077 = String.Empty;
                        //product.X06_MEASX078 = String.Empty;
                        //product.X06_MEASX079 = String.Empty;
                        //product.X06_MEASX080 = String.Empty;
                        //product.X06_MEASX081 = String.Empty;
                        //product.X06_MEASX082 = String.Empty;
                        //product.X06_MEASX083 = String.Empty;
                        //product.X06_MEASX084 = String.Empty;
                        //product.X06_MEASX085 = String.Empty;
                        //product.X06_MEASX086 = String.Empty;
                        //product.X06_MEASX087 = String.Empty;
                        //product.X06_MEASX088 = String.Empty;
                        //product.X06_MEASX089 = String.Empty;
                        //product.X06_MEASX090 = String.Empty;
                        //product.X06_MEASX091 = String.Empty;
                        //product.X06_MEASX092 = String.Empty;
                        //product.X06_MEASX093 = String.Empty;
                        //product.X06_MEASX094 = String.Empty;
                        //product.X06_MEASX095 = String.Empty;
                        //product.X06_MEASX096 = String.Empty;
                        //product.X06_MEASX097 = String.Empty;
                        //product.X06_MEASX098 = String.Empty;
                        //product.X06_MEASX099 = String.Empty;
                        //product.X06_MEASX100 = String.Empty;
                        //product.X06_MEASRESERVE05 = String.Empty;
                        //product.X06_MEASRESERVE04 = String.Empty;
                        //product.X06_MEASRESERVE03 = String.Empty;
                        //product.X06_MEASRESERVE02 = String.Empty;
                        //product.X06_DESCX11 = String.Empty;
                        //product.X06_DESCX12 = String.Empty;
                        //product.X06_DESCX13 = String.Empty;
                        //product.X06_DESCX14 = String.Empty;
                        //product.X06_DESCX15 = String.Empty;
                        //product.X06_DESCX16 = String.Empty;
                        //product.X06_DESCX17 = String.Empty;
                        //product.X06_DESCX18 = String.Empty;
                        //product.X06_DESCX19 = String.Empty;
                        //product.X06_DESCX20 = String.Empty;
                        //product.X06_DESCX21 = String.Empty;
                        //product.X06_DESCX22 = String.Empty;
                        //product.X06_DESCX23 = String.Empty;
                        //product.X06_DESCX24 = String.Empty;
                        //product.X06_DESCX25 = String.Empty;
                        //product.X06_DESCX26 = String.Empty;
                        //product.X06_DESCX27 = String.Empty;
                        //product.X06_DESCX28 = String.Empty;
                        //product.X06_DESCX29 = String.Empty;
                        //product.X06_DESCX30 = String.Empty;
                        //product.X06_MEASX001 = String.Empty;
                        //product.X06_MEASX002 = String.Empty;
                        //product.X06_MEASX003 = String.Empty;
                        //product.X06_MEASX004 = String.Empty;
                        //product.X06_MEASX005 = String.Empty;
                        //product.X06_MEASX006 = String.Empty;
                        //product.X06_MEASX007 = String.Empty;
                        //product.X06_MEASX008 = String.Empty;
                        //product.X06_MEASX009 = String.Empty;
                        //product.X06_MEASX010 = String.Empty;
                        //product.X06_MEASX101 = String.Empty;
                        //product.X06_MEASX102 = String.Empty;
                        //product.X06_MEASX103 = String.Empty;
                        //product.X06_MEASX104 = String.Empty;
                        //product.X06_MEASX105 = String.Empty;
                        //product.X06_MEASX106 = String.Empty;
                        //product.X06_MEASX107 = String.Empty;
                        //product.X06_MEASX108 = String.Empty;
                        //product.X06_MEASX109 = String.Empty;
                        //product.X06_MEASX110 = String.Empty;
                        //product.X06_DESCRESERVE01 = String.Empty;
                        //product.X06_DESCRESERVE02 = String.Empty;
                        //product.X06_DESCRESERVE03 = String.Empty;
                        #endregion

                        #endregion

                        #region Set Performance

                        if (this.DalCache.ExportContext.IncludePerformanceData)
                        {
                            String typeMatch = String.Format("{0}|", typeof(ApolloDataSetProductsProduct).ToString());
                            foreach (PlanogramMetricMappingDto metricMappingDto in _dalCache.ExportContext.MetricMappings.Where(m => m.Source.StartsWith(typeMatch)))
                            {
                                PlanogramPerformanceDataDto sourceDataDto = this.DalCache.PlanogramPerformanceDataDtoList.FirstOrDefault(p => p.PlanogramProductId.Equals(productDto.Id));

                                if (sourceDataDto != null)
                                {
                                    ApolloExportHelper.SetPerformanceFieldMappedValue<ApolloDataSetProductsProduct>(
                                        metricMappingDto, product, this.DalCache.PlanogramPerformanceMetricDtoList, sourceDataDto);
                                }
                            }
                        }

                        #endregion
                    }
                    catch (Exception ex)
                    {
                        //Log error
                        this.DalCache.LogExportError(
                            Language.ExportLog_ProductWarning_Error_Description,
                            String.Format(Language.ExportLog_ProductWarning_Error_Content, productDto.Gtin, productDto.Name, ex.Message),
                            PlanogramEventLogAffectedType.Products);

                        //Set product to null so we ignore it
                        product = null;
                    }

                    //Add to list
                    if (product != null)
                    {
                        products.Add(product);
                    }
                }
            }

            //Set product list
            ApolloDataSetProducts apolloDataSetProducts = new ApolloDataSetProducts();
            apolloDataSetProducts.Product = products.ToArray();
            return apolloDataSetProducts;
        }

        /// <summary>
        /// Creates an Apollo dataset Section XML Element from the Planogram being exported
        /// </summary>
        /// <returns>The populated ApolloDataSetSectionSectionData XML Element object</returns>
        private ApolloDataSetSectionSectionData[] CreateApolloSection(String newSectionId)
        {
            Boolean convertSuccess = false;

            //Get the mappings for the planogram - these are used for the Section element
            IEnumerable<PlanogramFieldMappingDto> mappings = this._dalCache.ExportContext.PlanogramMappings;

            //Create the array of apollo sections
            ApolloDataSetSectionSectionData[] sectionDataArray = new ApolloDataSetSectionSectionData[1]; //only ever 1 per section

            //create our section to populate
            ApolloDataSetSectionSectionData sectionData = new ApolloDataSetSectionSectionData();

            //Populate the section object
            try
            {
                //Get the source planogram object to export
                PlanogramDto planogramDto = this._dalCache.PlanogramDtoList.First();

                //Get the custom attribute dto for the planogram
                CustomAttributeDataDto customAttrDataDto =
                    _dalCache.CustomAttributeDataDtoList.First(c =>
                        c.ParentId.Equals(planogramDto.Id) && c.ParentType == (Byte)CustomAttributeDataPlanogramParentType.Planogram);

                #region Create Section to Export
                //Section id must have a value. Empty Apollo files are saved with Section = 0
                String sectionId = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionSECTION, planogramDto, customAttrDataDto, mappings, newSectionId); //CustomText
                sectionData.SE_SECTION = String.IsNullOrWhiteSpace(sectionId) ? "0" : sectionId;
                
                //Section name should be the planogram name. if no name supplied because the plaogram is newly created in V8, subsitute file name
                String sectionName = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionName, planogramDto, customAttrDataDto, mappings, planogramDto.Name);
                sectionName = sectionName.Replace(@"""", "&quot;"); //CustomText2
                sectionData.SE_NAME = String.IsNullOrWhiteSpace(sectionName) ? this._dalCache.FileNameWithoutExtension : sectionName;

                sectionData.SE_VERSION = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionVERSION, planogramDto, customAttrDataDto, mappings, "U"); //CustomText3
                sectionData.SE_VERSIONDESC = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionVERSIONDESC, planogramDto, customAttrDataDto, mappings); 
                sectionData.SE_SECTIONDESC = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionSECTIONDESC, planogramDto, customAttrDataDto, mappings);

                var epoch = new DateTime(1899, 12, 31, 0, 0, 0, DateTimeKind.Utc);
                String currentDateAsApolloString = Convert.ToInt64((DateTime.UtcNow.Date - epoch).TotalDays).ToString();
                sectionData.SE_CREATEDATE = currentDateAsApolloString;
                sectionData.SE_UPDATEDATE = currentDateAsApolloString;
                sectionData.SE_UPDATETIME = DateTime.UtcNow.ToString("HH:mm:ss"); //"00:00:00";
                sectionData.SE_PLOTDESC1 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionPLOTDESC1, planogramDto, customAttrDataDto, mappings); //CustomText6
                sectionData.SE_PLOTDESC2 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionPLOTDESC2, planogramDto, customAttrDataDto, mappings); 
                sectionData.SE_PLOTDESC3 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionPLOTDESC3, planogramDto, customAttrDataDto, mappings);
                sectionData.SE_USER1 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionUSER1, planogramDto, customAttrDataDto, mappings);
                sectionData.SE_USER2 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionUSER2, planogramDto, customAttrDataDto, mappings);

                //Calculate section dimensions
                Single sectionWidth = 0;
                foreach (PlanogramFixtureItemDto fixtureItem in this.DalCache.PlanogramFixtureItemDtoList.OrderBy(f => f.X))
                {
                    PlanogramFixtureDto fixture = this.DalCache.PlanogramFixtureDtoList
                        .First(f => f.Id.Equals(fixtureItem.PlanogramFixtureId));
                    if (fixture == null) continue;

                    //GetBackboardSubComponent
                    PlanogramSubComponentDto backboard = GetBackboardSubComponent(fixture);
                    if (backboard == null) continue;

                    sectionWidth += backboard.Width > fixture.Width ? backboard.Width : fixture.Width;
                }

                sectionData.SE_HEIGHT = planogramDto.Height.ToString();
                sectionData.SE_WIDTH = sectionWidth.ToString();
                sectionData.SE_DEPTH = planogramDto.Depth.ToString();

                PlanogramFixtureItemDto firstFixture = this.DalCache.PlanogramFixtureItemDtoList.OrderBy(f => f.X).FirstOrDefault();
                sectionData.SE_XPOS = firstFixture != null ? firstFixture.X.ToString() : "0.0";
                sectionData.SE_YPOS = firstFixture != null ? firstFixture.Y.ToString() : "0.0";
                sectionData.SE_ZPOS = firstFixture != null ? firstFixture.Z.ToString() : "0.0";

                sectionData.SE_TRAFFIC = "0";
                sectionData.SE_MERCH = String.Empty;
                sectionData.SE_STANDARDLENGTH = "0.0";
                sectionData.SE_UPRIGHT = CalculateApolloUprightSpacing().ToString();
                sectionData.SE_INCREMENT = "0.0";

                //Notches
                Single notchStartY =0;
                Single notchSpacingY = 0;
                ApolloExportHelper.GetApolloSectionNotchValues(this.DalCache.PlanogramComponentDtoList, this.DalCache.PlanogramSubComponentDtoList, out notchStartY, out notchSpacingY);
                sectionData.SE_NOTCH = notchSpacingY.ToString();
                sectionData.SE_FIRSTNOTCH = notchStartY.ToString();

                Single usedLinear = 0;
                Single usedSquare = 0;
                Single usedCubic = 0;
                Int32 numFacings = 0;
                foreach (PlanogramPositionDto dto in this.DalCache.PlanogramPositionDtoList)
                {
                    usedLinear += (Single)(dto.MetaTotalLinearSpace != null ? dto.MetaTotalLinearSpace : 0);
                    usedSquare += (Single)(dto.MetaTotalAreaSpace != null ? dto.MetaTotalAreaSpace : 0);
                    usedCubic += (Single)(dto.MetaTotalVolumetricSpace != null ? dto.MetaTotalVolumetricSpace : 0);
                    numFacings += dto.FacingsWide;
                }
                Single availableLinear = 0;
                Single availableSquare = 0;
                Single availableCubic = 0;
                foreach (PlanogramFixtureItemDto dto in this.DalCache.PlanogramFixtureItemDtoList)
                {
                    availableLinear += (Single)(dto.MetaTotalMerchandisableLinearSpace != null ? dto.MetaTotalMerchandisableLinearSpace : 0);
                    availableSquare += (Single)(dto.MetaTotalMerchandisableAreaSpace != null ? dto.MetaTotalMerchandisableAreaSpace : 0);
                    availableCubic += (Single)(dto.MetaTotalMerchandisableVolumetricSpace != null ? dto.MetaTotalMerchandisableVolumetricSpace : 0);
                }
                sectionData.SE_USEDLINEAR = usedLinear.ToString();
                sectionData.SE_USEDSQUARE = usedSquare.ToString();
                sectionData.SE_USEDCUBIC = usedCubic.ToString();
                sectionData.SE_AVAILABLELINEAR = availableLinear.ToString();
                sectionData.SE_AVAILABLESQUARE = availableSquare.ToString();
                sectionData.SE_AVAILABLECUBIC = availableCubic.ToString();

                sectionData.SE_FACINGS = numFacings.ToString();

                sectionData.SE_SALES = "0.0";
                sectionData.SE_PROFIT = "0.0";
                sectionData.SE_DPP = "0.0";
                sectionData.SE_MOVEMENT = "0.0";
                sectionData.SE_ROII = "0.0";
                sectionData.SE_INVENTORY = "0.0";
                sectionData.SE_CUBICTHROUGHPUT = "0.0";
                sectionData.SE_LOWDAYS = "0.0";
                sectionData.SE_LOWCASES = "0.0";
                sectionData.SE_LOWFACINGS = "0.0";
                sectionData.SE_HIDAYS = "0.0";
                sectionData.SE_HICASES = "1.0";
                sectionData.SE_HIFACINGS = "1.0";
                sectionData.SE_AVERAGECASES = "1.0";
                sectionData.SE_AVERAGEFACINGS = "1.0";
                sectionData.SE_NUMSKUS = this.DalCache.PlanogramPositionDtoList.Count.ToString();

                Int32 numUnits = 0;
                foreach (PlanogramPositionDto dto in this.DalCache.PlanogramPositionDtoList)
                {
                    numUnits += dto.TotalUnits;
                }
                sectionData.SE_CAPACITY = numUnits.ToString();

                sectionData.SE_NUMSHELVES = this.DalCache.PlanogramSubComponentDtoList.Where(s => s.MerchandisingType != (Byte)PlanogramSubComponentMerchandisingType.None).Count().ToString();
                
                sectionData.SE_INVATRETAIL = "0.0";
                //sectionData.SE_CASES>21.83333</SE_CASES>
                
                sectionData.SE_CRUNCH = "0";
                sectionData.SE_SHOWDECO = "0";
                sectionData.SE_DECOXPOS = "0.0";
                sectionData.SE_LOGOPATH = String.Empty;

                switch ( (PlanogramLengthUnitOfMeasureType) planogramDto.LengthUnitsOfMeasure)
                {
                    case PlanogramLengthUnitOfMeasureType.Inches:
                        sectionData.SE_CUBICMULTIPLIER = "1728.0";
                        sectionData.SE_LINEARMULTIPLIER="12.0";
                        sectionData.SE_SQUAREMULTIPLIER = "144.0";
                        break;
                    case PlanogramLengthUnitOfMeasureType.Centimeters:
                        sectionData.SE_CUBICMULTIPLIER = "1000000.0";
                        sectionData.SE_LINEARMULTIPLIER="100.0";
                        sectionData.SE_SQUAREMULTIPLIER = "10000.0";
                        break;
                    default:
                        sectionData.SE_CUBICMULTIPLIER = "1.0";
                        sectionData.SE_LINEARMULTIPLIER="1.0";
                        sectionData.SE_SQUAREMULTIPLIER = "1.0";
                        break;
                }

                sectionData.SE_UPRIGHTFOREGROUND = String.Empty;
                sectionData.SE_FOREGROUND = String.Empty;
                sectionData.SE_UPRIGHTBACKGROUND = String.Empty;
                sectionData.SE_BACKGROUND = String.Empty;
                sectionData.SE_ALLOWOVERLAP = "1";
                sectionData.SE_ARBITRARY = String.Empty;
                sectionData.SE_ALLOWFLOAT = "0";
                sectionData.SE_ALLOWOVERHANG = "1";
                sectionData.SE_DAYSINWEEK = "7.0";
                sectionData.SE_DECOWIDTH = "0.0";
                sectionData.SE_DECOHEIGHT = "0.0";
                sectionData.SE_DECOYPOS = "0.0";

                //SE_RESERVED5 is a word that contain bit flags to turn on some section settings. Setting this to 2 enables the Items Span Shelves option
                sectionData.SE_RESERVED5 = "2"; //Allow Item Span shelves

                sectionData.SE_RESERVED1 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionRESERVED1, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.SE_RESERVED2 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionRESERVED2, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.SE_RESERVED3 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionRESERVED3, planogramDto, customAttrDataDto, mappings, "0");
                sectionData.SE_RESERVED4 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionRESERVED4, planogramDto, customAttrDataDto, mappings, "0");
                sectionData.SE_RESERVED6 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionRESERVED6, planogramDto, customAttrDataDto, mappings, "0");
                sectionData.SE_SECTIONID = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionSECTIONID, planogramDto, customAttrDataDto, mappings, String.Empty);
                
                sectionData.SE_STAGGEREDSHELVES = "0";

                sectionData.SE_MESSAGELINE1 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionMESSAGELINE1, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.SE_MESSAGELINE2 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionMESSAGELINE2, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.SE_MESSAGELINE3 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionMESSAGELINE3, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.SE_MESSAGELINE4 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionMESSAGELINE4, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.SE_MESSAGELINE5 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionMESSAGELINE5, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.SE_DECOTEXT = String.Empty;

                sectionData.SE_SECTIONFLAGSBIT2 = "0";      //These 2 tell Apollo to use millions of colours value for products not the PR_COLOR value
                sectionData.X03_SEMEASRESERVE01 = "1.0";    //These 2 tell Apollo to use millions of colours value for products not the PR_COLOR value

                #region X03_SEMEASRESERVE?? Fields
                sectionData.X03_SEMEASRESERVE02 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASRESERVE02, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASRESERVE03 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASRESERVE03, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASRESERVE04 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASRESERVE04, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASRESERVE05 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASRESERVE05, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASRESERVE06 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASRESERVE06, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASRESERVE07 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASRESERVE07, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASRESERVE08 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASRESERVE08, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASRESERVE09 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASRESERVE09, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASRESERVE10 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASRESERVE10, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASRESERVE11 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASRESERVE11, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASRESERVE12 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASRESERVE12, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASRESERVE13 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASRESERVE13, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASRESERVE14 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASRESERVE14, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASRESERVE15 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASRESERVE15, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASRESERVE16 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASRESERVE16, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASRESERVE17 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASRESERVE17, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASRESERVE18 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASRESERVE18, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASRESERVE19 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASRESERVE19, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASRESERVE20 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASRESERVE20, planogramDto, customAttrDataDto, mappings, "0.0");
                #endregion

                #region X03_SEDESC?? Fields
                sectionData.X03_SEDESCX01 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX01, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX02 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX02, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX03 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX03, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX04 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX04, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX05 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX05, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX06 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX06, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX07 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX07, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX08 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX08, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX09 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX09, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX10 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX10, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX11 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX11, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX12 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX12, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX13 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX13, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX14 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX14, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX15 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX15, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX16 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX16, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX17 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX17, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX18 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX18, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX19 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX19, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX20 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX20, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX21 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX21, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX22 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX22, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX23 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX23, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX24 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX24, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX25 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX25, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX26 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX26, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX27 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX27, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX28 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX28, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX29 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX29, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX30 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX30, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX31 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX31, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX32 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX32, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX33 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX33, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX34 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX34, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX35 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX35, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX36 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX36, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX37 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX37, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX38 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX38, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX39 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX39, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX40 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX40, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX41 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX41, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX42 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX42, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX43 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX43, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX44 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX44, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX45 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX45, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX46 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX46, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX47 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX47, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX48 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX48, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX49 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX49, planogramDto, customAttrDataDto, mappings, String.Empty);
                sectionData.X03_SEDESCX50 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEDESCX50, planogramDto, customAttrDataDto, mappings, String.Empty);
                #endregion

                #region X03_SEMEASX ?? Fields
                sectionData.X03_SEMEASX01 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX01, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX02 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX02, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX03 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX03, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX04 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX04, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX05 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX05, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX06 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX06, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX07 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX07, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX08 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX08, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX09 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX09, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX10 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX10, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX11 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX11, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX12 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX12, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX13 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX13, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX14 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX14, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX15 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX15, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX16 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX16, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX17 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX17, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX18 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX18, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX19 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX19, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX20 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX20, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX21 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX21, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX22 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX22, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX23 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX23, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX24 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX24, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX25 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX25, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX26 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX26, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX27 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX07, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX28 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX28, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX29 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX29, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX30 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX30, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX31 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX31, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX32 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX32, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX33 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX33, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX34 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX34, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX35 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX35, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX36 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX36, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX37 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX37, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX38 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX38, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX39 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX39, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX40 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX40, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX41 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX41, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX42 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX42, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX43 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX43, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX44 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX44, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX45 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX45, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX46 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX46, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX47 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX47, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX48 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX48, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX49 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX49, planogramDto, customAttrDataDto, mappings, "0.0");
                sectionData.X03_SEMEASX50 = ApolloExportHelper.GetFieldMappedValue<PlanogramDto>(ApolloImportHelper.SectionX03_SEMEASX50, planogramDto, customAttrDataDto, mappings, "0.0");
                #endregion

                #region Others
                //sectionData.X03_SEDESCRESERVE01 = String.Empty;
                //sectionData.X03_SEDESCRESERVE02 = String.Empty;
                //sectionData.X03_SEDESCRESERVE03 = String.Empty;
                //sectionData.X03_SEDESCRESERVE04 = String.Empty;
                //sectionData.X03_SEDESCRESERVE05 = String.Empty;
                //sectionData.X03_SEDESCRESERVE06 = String.Empty;
                //sectionData.X03_SEDESCRESERVE07 = String.Empty;
                //sectionData.X03_SEDESCRESERVE08 = String.Empty;
                //sectionData.SE_RECCAPACITY = "0.0";
                //sectionData.SE_RECROII = "0.0";
                //sectionData.SE_RECINVRTL = "0.0";
                //sectionData.SE_RECINVCST = "0.0";
                //sectionData.SE_RECCASES = "0";
                sectionData.Blob = String.Empty;
                #endregion

                #endregion

                convertSuccess = true;
            }
            catch (Exception ex)
            {
                //Log error
                this.DalCache.LogExportError(
                    Language.ExportLog_Section_Error_Description,
                    String.Format(Language.ExportLog_Section_Error_Content, ex.Message),
                    PlanogramEventLogAffectedType.Planogram);
                convertSuccess = false;
            }

            //If populated correctly, add to array
            if (convertSuccess) { sectionDataArray[0] = sectionData; }

            return sectionDataArray;
        }

        /// <summary>
        /// Creates the Section/SectionProduct Xml element
        /// </summary>
        /// <param name="mappings">The mappings to use</param>
        /// <returns></returns>
        private ApolloDataSetSectionSectionProductsSectionProduct[] CreateApolloSectionProducts()
        {
            //Get the mappings for the products - these are used for the section products we are adding
            IEnumerable<PlanogramFieldMappingDto> mappings = this._dalCache.ExportContext.ProductMappings;
            
            //Create the products list
            List<ApolloDataSetSectionSectionProductsSectionProduct> sectionProducts =
                new List<ApolloDataSetSectionSectionProductsSectionProduct>();

            //Add each dto to the array
            foreach (PlanogramProductDto productDto in _dalCache.PlanogramProductDtoList.OrderBy(p => p.Gtin))
            {
                //Find the related position
                PlanogramPositionDto positionDto = _dalCache.PlanogramPositionDtoList.FirstOrDefault(p => p.PlanogramProductId.Equals(productDto.Id));
                //Only add if we have a position to match the product
                if (positionDto != null)
                {

                    //Create the export item
                    ApolloDataSetSectionSectionProductsSectionProduct sectionProduct = new ApolloDataSetSectionSectionProductsSectionProduct();

                    #region Populate the products
                    try
                    {
                        //Get the custom attribute dto for the product
                        CustomAttributeDataDto customAttrDataDto = _dalCache.CustomAttributeDataDtoList.First(c =>
                            c.ParentId.Equals(productDto.Id) && c.ParentType == (Byte)CustomAttributeDataPlanogramParentType.PlanogramProduct);

                        sectionProduct.PR_UPC = ApolloExportHelper.GetFieldMappedValue(ApolloImportHelper.ProductUPC, productDto, mappings, productDto.Gtin);
                        sectionProduct.PR_RETAIL = ApolloExportHelper.GetFieldMappedValue(ApolloImportHelper.ProductRETAIL, productDto, mappings, "0.0");
                        sectionProduct.PR_CASECOST = ApolloExportHelper.GetFieldMappedValue(ApolloImportHelper.ProductCaseCost, productDto, mappings, "0.0");
                        sectionProduct.PR_REALMOVEMENT = ApolloExportHelper.GetFieldMappedValue(ApolloImportHelper.ProductRealMovement, productDto, mappings, "0.0");
                        sectionProduct.PR_VAT = ApolloExportHelper.GetFieldMappedValue(ApolloImportHelper.ProductVAT, productDto, mappings, "0.0");
                        sectionProduct.PR_DPCA = ApolloExportHelper.GetFieldMappedValue(ApolloImportHelper.ProductDPCA, productDto, mappings, "0.0");
                        sectionProduct.PR_PAYDISCOUNT = ApolloExportHelper.GetFieldMappedValue(ApolloImportHelper.ProductPAYDISCOUNT, productDto, mappings, "0.0");
                        sectionProduct.PR_SELLINGPRICE = ApolloExportHelper.GetFieldMappedValue(ApolloImportHelper.ProductSELLINGPRICE, productDto, mappings, "0.0");
                        sectionProduct.PR_COST = ApolloExportHelper.ParseNumericValue(productDto.CostPrice, false);
                        sectionProduct.PR_MARGIN = ApolloExportHelper.GetFieldMappedValue(ApolloImportHelper.ProductMARGIN, productDto, mappings, "100.0");

                        #region Set Performance

                        if (this.DalCache.ExportContext.IncludePerformanceData)
                        {
                            String typeMatch = String.Format("{0}|", typeof(ApolloDataSetSectionSectionProductsSectionProduct).ToString());
                            foreach (PlanogramMetricMappingDto metricMappingDto in _dalCache.ExportContext.MetricMappings.Where(m => m.Source.StartsWith(typeMatch)))
                            {
                                PlanogramPerformanceDataDto sourceDataDto = this.DalCache.PlanogramPerformanceDataDtoList.FirstOrDefault(p => p.PlanogramProductId.Equals(productDto.Id));

                                if (sourceDataDto != null)
                                {
                                    ApolloExportHelper.SetPerformanceFieldMappedValue<ApolloDataSetSectionSectionProductsSectionProduct>(
                                        metricMappingDto, sectionProduct, this.DalCache.PlanogramPerformanceMetricDtoList, sourceDataDto);
                                }
                            }
                        }

                        #endregion
                    }
                    catch (Exception ex)
                    {
                        //Log error
                        this.DalCache.LogExportError(
                            Language.ExportLog_SectionProduct_Error_Description, String.Format(Language.ExportLog_SectionProduct_Error_Content, productDto.Gtin, productDto.Name, ex.Message),
                            PlanogramEventLogAffectedType.Products);
                        sectionProduct = null;
                    }
                    #endregion

                    //Add export item to array
                    if (sectionProduct != null) sectionProducts.Add(sectionProduct);
                }
            }

            return sectionProducts.ToArray();
        }

        /// <summary>
        /// Creates an Apollo dataset Shelves object from the Planogram being exported
        /// </summary>
        /// <returns>The populated ApolloDataSetSectionShelves</returns>
        private ApolloDataSetSectionShelves[] CreateApolloSectionShelves()
        {
            //Create the array of element <Shelves>. 
            ApolloDataSetSectionShelves[] shelvesElementArray = new ApolloDataSetSectionShelves[1];
            //We will only ever have one element in this array so create it
            ApolloDataSetSectionShelves shelvesElement = new ApolloDataSetSectionShelves();

            List<ApolloDataSetSectionShelvesShelf> shelfArray = new List<ApolloDataSetSectionShelvesShelf>();

            //Get the fixtures and Fixture Items in ordered lists
            List<PlanogramFixtureDto> fixtures = _dalCache.PlanogramFixtureDtoList;                                 //fixture template (has height,width, depth)
            List<PlanogramFixtureItemDto> fixtureItems = 
                _dalCache.PlanogramFixtureItemDtoList.OrderBy(f => f.X).ToList();                                   //fixtures (bays) (has x,y,z) -> FixtureId

            //Loop through all fixture items
            foreach (PlanogramFixtureItemDto fixtureItem in fixtureItems)
            {
                PlanogramFixtureDto fixture = fixtures.First(f => f.Id.Equals(fixtureItem.PlanogramFixtureId));

                //Get the backboard component for the fixture - this contains the notch info
                PlanogramSubComponentDto backboard = GetBackboardSubComponent(fixture);

                #region Standard Fixture Components
                List<PlanogramFixtureComponentDto> fixtureComponents = _dalCache.PlanogramFixtureComponentDtoList.  //component instance (has x,y,z) -> PlanogramFixture_Id, PlanCompId
                        Where(c => c.PlanogramFixtureId.Equals(fixture.Id)).
                        OrderBy(c => c.X).ThenBy(c => c.Y).ToList();

                //Loop thorough all fixture components
                if (fixtureComponents.Any())
                {
                    for (Int32 i = 0; i < fixtureComponents.Count; i++)
                    {
                        
                        PlanogramFixtureComponentDto fixtureComponent = fixtureComponents[i];
                        PlanogramComponentDto component = _dalCache.PlanogramComponentDtoList         //component template (has height,width, depth)
                            .First(c => c.Id.Equals(fixtureComponent.PlanogramComponentId));

                        //Ignore backboards and bases
                        if (component.ComponentType == (Byte)PlanogramComponentType.Backboard) continue;
                        if (component.ComponentType == (Byte)PlanogramComponentType.Base) continue;

                        //Get the custom attribute dto for the component
                        CustomAttributeDataDto customAttrDataDto =_dalCache.CustomAttributeDataDtoList.First(
                            c => c.ParentId.Equals(component.Id) && 
                            c.ParentType == (Byte)CustomAttributeDataPlanogramParentType.PlanogramComponent);
                        
              
                        List<PlanogramSubComponentDto> subComponents = _dalCache.PlanogramSubComponentDtoList
                            .Where(s => s.PlanogramComponentId.Equals(component.Id)).ToList();                      //sub-comp(has x,y,z & height,width,depth) -> PlanCompId

                        if (subComponents.Count == 0) continue;

                        #region Populate The Shelf Element
                        //Create our Shelf element - one for each subcomponent
                        Boolean isComplexElement = (subComponents.Count > 1);
                        Int32 newShelfId = shelfArray.Count + 1;
                        switch (isComplexElement)
                        {
                            case false:
                                //Create the single shelf element
                                ApolloDataSetSectionShelvesShelf shelf =
                                    CreateSimpleShelfElement(fixtureItem, fixture, null, null, fixtureComponent, component, subComponents[0], customAttrDataDto, newShelfId, backboard);
                            
                                if (shelf != null)
                                {
                                    shelfArray.Add(shelf);
                                }
                                break;

                            case true:
                                //Create an element for each subcomponent and a bounding box to hold them
                                List<ApolloDataSetSectionShelvesShelf> advancedApolloFixture =
                                    CreateAdvancedShelfElement(fixtureItem, fixture, null, null, fixtureComponent, component, subComponents, customAttrDataDto, newShelfId, backboard);

                                if (advancedApolloFixture.Count > 0)
                                {
                                    shelfArray.AddRange(advancedApolloFixture);
                                }
                                break;
                        }
                        #endregion
                    }
                }
                #endregion

                #region Assembly Components
                List<PlanogramFixtureAssemblyDto> fixtureAssemblies = _dalCache.PlanogramFixtureAssemblyDtoList.  //assembly instance (has x,y,z) -> PlanogramFixture_Id, PlanogramAssembly_Id
                        Where(c => c.PlanogramFixtureId.Equals(fixture.Id)).
                        OrderBy(c => c.X).ThenBy(c => c.Y).ToList();

                foreach (PlanogramFixtureAssemblyDto fixtureAssembly in fixtureAssemblies)
                {
                    List<PlanogramAssemblyComponentDto> assemblyComponents = _dalCache.PlanogramAssemblyComponentDtoList.  //component instance (has x,y,z) -> PlanogramFixture_Id, PlanCompId
                        Where(c => c.PlanogramAssemblyId.Equals(fixtureAssembly.PlanogramAssemblyId)).
                        OrderBy(c => c.X).ThenBy(c => c.Y).ToList();

                    foreach (PlanogramAssemblyComponentDto assemblyComponent in assemblyComponents)
                    {
                        PlanogramComponentDto component = _dalCache.PlanogramComponentDtoList         //component template (has height,width, depth)
                            .First(c => c.Id.Equals(assemblyComponent.PlanogramComponentId));

                        //Ignore backboards and bases
                        if (component.ComponentType == (Byte)PlanogramComponentType.Backboard) continue;
                        if (component.ComponentType == (Byte)PlanogramComponentType.Base) continue;

                        //Get the custom attribute dto for the component
                        CustomAttributeDataDto customAttrDataDto = _dalCache.CustomAttributeDataDtoList.First(
                            c => c.ParentId.Equals(component.Id) &&
                            c.ParentType == (Byte)CustomAttributeDataPlanogramParentType.PlanogramComponent);

                        List<PlanogramSubComponentDto> subComponents = _dalCache.PlanogramSubComponentDtoList
                            .Where(s => s.PlanogramComponentId.Equals(component.Id)).ToList();                      //sub-comp(has x,y,z & height,width,depth) -> PlanCompId

                        if (subComponents.Count == 0) continue;

                        #region Populate the Shelf Element
                        //Create our Shelf element - one for each subcomponent
                        Boolean isComplexElement = (subComponents.Count > 1);
                        Int32 newShelfId = shelfArray.Count + 1;

                        switch (isComplexElement)
                        {
                            case false:
                                //Create the single shelf element
                                ApolloDataSetSectionShelvesShelf shelf = //null;
                                    CreateSimpleShelfElement(fixtureItem, fixture, fixtureAssembly, assemblyComponent, null, component, subComponents[0], customAttrDataDto, newShelfId, backboard);
                                if (shelf != null)
                                {
                                    shelfArray.Add(shelf);
                                }
                                break;

                            case true:
                                //Create an element for each subcomponent and a bounding box to hold them
                                List<ApolloDataSetSectionShelvesShelf> advancedApolloFixture =
                                    CreateAdvancedShelfElement(fixtureItem, fixture, fixtureAssembly, assemblyComponent, null, component, subComponents, customAttrDataDto, newShelfId, backboard);
                                if (advancedApolloFixture.Count > 0)
                                {
                                    shelfArray.AddRange(advancedApolloFixture);
                                }
                                break;
                        }

                        #endregion
                    }
                }

                #endregion
            }

            shelvesElement.Shelf = shelfArray.ToArray();

            //Add the element to the array of elements
            shelvesElementArray[0] = shelvesElement;
            //and return it
            return shelvesElementArray;
        }

        /// <summary>
        /// Create a simple element i.e. a single shelf element object is created for the single sub-component in the ccm component
        /// </summary>
        /// <param name="fixtureItem">The source sub-component's parent fixture item</param>
        /// <param name="fixture">The source sub-component's parent fixture</param>
        /// <param name="fixtureComponent">The source sub-component's parent fixture component</param>
        /// <param name="component">The source sub-component's parent component</param>
        /// <param name="subComponent">The source sub-component from which the shelf element will be created</param>
        /// <param name="customAttrData">The source custom attribute data dto for use when applying mappings</param>
        /// <param name="mappings">The mappings to apply if any</param>
        /// <param name="shelfId">The id for the new sehelf element</param>
        /// <returns>The created shelf element</returns>
        private ApolloDataSetSectionShelvesShelf CreateSimpleShelfElement(PlanogramFixtureItemDto fixtureItem, PlanogramFixtureDto fixture, PlanogramFixtureAssemblyDto fixtureAssembly, PlanogramAssemblyComponentDto assemblyComponent, PlanogramFixtureComponentDto fixtureComponent, PlanogramComponentDto component, PlanogramSubComponentDto subComponent, CustomAttributeDataDto customAttrData, Int32 shelfId, PlanogramSubComponentDto backboardSubComponent)
        {           
            //There is only 1 sub-component so we have a straightforward conversion
            ApolloDataSetSectionShelvesShelf shelf = new ApolloDataSetSectionShelvesShelf();
                            
            try
            {
                //Get the shelf type to export to (shelf, peg etc)
                ApolloShelfType apolloShelfType = ApolloExportHelper.GetApolloShelfType(subComponent);
                shelf.ShelfData =
                    CreateApolloShelfData(apolloShelfType, fixtureItem, fixture, fixtureAssembly, assemblyComponent, fixtureComponent, component, subComponent, customAttrData, shelfId, backboardSubComponent);

                shelf.Positions = CreateApolloShelfPositions(apolloShelfType, fixtureItem, fixture, fixtureAssembly, assemblyComponent, fixtureComponent, component, subComponent);
            }
            catch (Exception ex)
            {
                //Log error
                this.DalCache.LogExportError(
                    Language.ExportLog_ShelfWarning_Error_Description, String.Format(Language.ExportLog_ShelfWarning_Error_Content, subComponent.Name, ex.Message), 
                    PlanogramEventLogAffectedType.Products);
                //Null off the shelf so it doesn't get added to the export file
                shelf = null;
            }

            return shelf;
        }

        /// <summary>
        /// Create an advanced element i.e. a multiple shelf element objects are created for the sub-components in the ccm component
        /// </summary>
        /// <param name="fixtureItem">The source sub-component's parent fixture item</param>
        /// <param name="fixture">The source sub-component's parent fixture</param>
        /// <param name="fixtureComponent">The source sub-component's parent fixture component</param>
        /// <param name="component">The source sub-component's parent component</param>
        /// <param name="subComponent">The source sub-component from which the shelf element will be created</param>
        /// <param name="customAttrData">The source custom attribute data dto for use when applying mappings</param>
        /// <param name="mappings">The mappings to apply if any</param>
        /// <param name="shelfId">The id for the new sehelf element</param>
        /// <returns>The created shelf element</returns>
        private List<ApolloDataSetSectionShelvesShelf> CreateAdvancedShelfElement(PlanogramFixtureItemDto fixtureItem, PlanogramFixtureDto fixture, PlanogramFixtureAssemblyDto fixtureAssembly, PlanogramAssemblyComponentDto assemblyComponent, PlanogramFixtureComponentDto fixtureComponent, PlanogramComponentDto component, List<PlanogramSubComponentDto> subComponents, CustomAttributeDataDto customAttrData, Int32 shelfId, PlanogramSubComponentDto backboardSubComponent)
        {
            Int32 newShelfId = shelfId;

            List<ApolloDataSetSectionShelvesShelf> childShelfElements = new List<ApolloDataSetSectionShelvesShelf>(); //excludes bounding box

            //Inform user
            this.DalCache.LogExportWarning(
                Language.ExportLog_MerchandisingWarning_ComplexComponent_Description, 
                Language.ExportLog_MerchandisingWarning_ComplexComponent_Content, PlanogramEventLogAffectedType.Components);

            foreach (PlanogramSubComponentDto subComponent in subComponents)
            {
                ApolloDataSetSectionShelvesShelf shelf = new ApolloDataSetSectionShelvesShelf();

                try
                {
                    //Get the shelf type to export to (shelf, peg etc)
                    ApolloShelfType apolloShelfType = ApolloExportHelper.GetApolloShelfType(subComponent);
                    shelf.ShelfData =
                        CreateApolloShelfData(apolloShelfType, fixtureItem, fixture, fixtureAssembly, assemblyComponent, fixtureComponent, component, subComponent, customAttrData, newShelfId, backboardSubComponent);

                    shelf.Positions = CreateApolloShelfPositions(apolloShelfType, fixtureItem, fixture, fixtureAssembly, assemblyComponent, fixtureComponent, component, subComponent);

                    childShelfElements.Add(shelf);
                    newShelfId++;
                }
                catch (Exception ex)
                {
                    //Log error
                    this.DalCache.LogExportError(
                        Language.ExportLog_ShelfWarning_Error_Description,
                        String.Format(Language.ExportLog_ShelfWarning_Error_Content, subComponent.Name, ex.Message),
                        PlanogramEventLogAffectedType.Products);
                    shelf = null;
                }
            }

            return childShelfElements;

            #region Unused Code - Creates a Bounding Box but Apollo cannot open it
            //DISUSED CODE - Creates a Bounding Box which in turn forms an Advanced Fixture in Apollo. But bounding boxes also
            //have some BLOB information encoded into them which I cannot currently decode so not sure if this is viable.
            //Currently, using this code to create a bounding box results in a plan not being openable

            //Int32 boundingBoxShelfId = shelfId;
            //Int32 newShelfId = boundingBoxShelfId + 1;
            
            ////Create the empty list to populate
            //List<ApolloDataSetSectionShelvesShelf> advancedComponentsElements = new List<ApolloDataSetSectionShelvesShelf>(); //includes bounding box
            //List<ApolloDataSetSectionShelvesShelf> childShelfElements = new List<ApolloDataSetSectionShelvesShelf>(); //excludes bounding box
            
            //foreach (PlanogramSubComponentDto subComponent in subComponents)
            //{
            //    ApolloDataSetSectionShelvesShelf shelf = new ApolloDataSetSectionShelvesShelf(); 
                
            //    try
            //    {
            //        shelf.ShelfData = CreateApolloShelfData(fixtureItem, fixture, fixtureComponent, component, subComponent, customAttrData, mappings, newShelfId);
            //        shelf.Positions = CreateApolloShelfPositions(fixtureComponent.Id);
            //        childShelfElements.Add(shelf);
            //        newShelfId++;
            //    }
            //    catch
            //    {
            //        //log Error
            //        shelf = null;
            //    }
            //}

            ////Create the bounding box
            //ApolloDataSetSectionShelvesShelf boundingBox = new ApolloDataSetSectionShelvesShelf();
            //try
            //{
            //    //create the bounding box
            //    boundingBox.ShelfData = CreateApolloBoundingBox(childShelfElements, boundingBoxShelfId);
            //    //Add this to the list first
            //    advancedComponentsElements.Add(boundingBox);

            //    //update each child element SH_RESERVED5 field with the bounding box's shelfid
            //    //and then add to the list to return
            //    foreach (ApolloDataSetSectionShelvesShelf childShelf in childShelfElements)
            //    {
            //        childShelf.ShelfData[0].SH_RESERVED5 = boundingBoxShelfId.ToString();
            //        advancedComponentsElements.Add(childShelf);
            //    }
                
            //}
            //catch
            //{
            //    //log Error
            //    boundingBox = null;
            //}

            //return advancedComponentsElements;
            #endregion
        }


        private ApolloDataSetSectionShelvesShelfShelfData[] CreateApolloBoundingBox(List<ApolloDataSetSectionShelvesShelf> elementsToContain, Int32 shelfId)
        {
            ApolloDataSetSectionShelvesShelf boundingBox = new ApolloDataSetSectionShelvesShelf();
            ApolloShelfDataElementConverterBox converter = new ApolloShelfDataElementConverterBox();

            ApolloDataSetSectionShelvesShelfShelfData[] boundingBoxData = new ApolloDataSetSectionShelvesShelfShelfData[1];
            boundingBoxData[0] = converter.CreateBoundingBoxData(elementsToContain, shelfId);

            return boundingBoxData;
        }

        private ApolloDataSetSectionShelvesShelfShelfData[] CreateApolloShelfData(ApolloShelfType apolloShelfType, PlanogramFixtureItemDto fixtureItem, PlanogramFixtureDto fixture, PlanogramFixtureAssemblyDto planogramFixtureAssembly, PlanogramAssemblyComponentDto planogramAssemblyComponent, PlanogramFixtureComponentDto fixtureComponent, PlanogramComponentDto component, PlanogramSubComponentDto subComponent, CustomAttributeDataDto customAttrData, Int32 shelfId, PlanogramSubComponentDto backboardSubComponent)
        {
            //Get the mappings for the components
            IEnumerable<PlanogramFieldMappingDto> mappings = this._dalCache.ExportContext.ComponentMappings;
            
            //Only ever create 1 for each shelf parent element
            ApolloDataSetSectionShelvesShelfShelfData[] shelfElement = new ApolloDataSetSectionShelvesShelfShelfData[1];

            ApolloDataSetSectionShelvesShelfShelfData shelfData = new ApolloDataSetSectionShelvesShelfShelfData();

            ApolloShelfDataElementConverterBase converter = new ApolloShelfDataElementConverterShelf();
            switch (apolloShelfType)
            {
                case ApolloShelfType.Shelf:
                    converter = new ApolloShelfDataElementConverterShelf();
                    break;

                case ApolloShelfType.FreeHandSurface:
                    converter = new ApolloShelfDataElementConverterFreeHandSurface();
                    break;

                case ApolloShelfType.Pegboard:
                    converter = new ApolloShelfDataElementConverterPeg();
                    break;

                case ApolloShelfType.Coffin:
                case ApolloShelfType.Basket:
                    converter = new ApolloShelfDataElementConverterCoffin();
                    break;

                case ApolloShelfType.Sltowall:
                    converter = new ApolloShelfDataElementConverterSlotwall();
                    break;

                case ApolloShelfType.Crossbar:
                    converter = new ApolloShelfDataElementConverterCrossbar();
                    break;

                case ApolloShelfType.HangingBar:
                    converter = new ApolloShelfDataElementConverterHangingBar();
                    break;

                case ApolloShelfType.Box:
                    converter = new ApolloShelfDataElementConverterBox();
                    break;
            }
            converter.InitialiseLogging(this.DalCache.FileNameWithoutExtension, this.DalCache.ExportContext.ExportEvents);
            converter.Initialise((Byte)apolloShelfType, fixtureItem, fixture, planogramFixtureAssembly, planogramAssemblyComponent, fixtureComponent, component, subComponent, customAttrData, mappings, shelfId, backboardSubComponent, this.RetainSourcePlanLookAndFeel);
            
            shelfData = converter.Convert();

            shelfElement[0] = shelfData;
            return shelfElement;
        }

        private PositionsPosition[] CreateApolloShelfPositions(ApolloShelfType apolloShelfType, PlanogramFixtureItemDto fixtureItem, PlanogramFixtureDto fixture, PlanogramFixtureAssemblyDto fixtureAssembly, PlanogramAssemblyComponentDto assemblyComponent, PlanogramFixtureComponentDto fixtureComponent, PlanogramComponentDto component, PlanogramSubComponentDto subComponent)
        {
            PositionsPosition[] positionsElement = new PositionsPosition[1];
            //If no positions on this component, just return empty array
            if (!_dalCache.PlanogramPositionDtoList.Any(p => p.PlanogramSubComponentId.Equals(subComponent.Id))) return positionsElement;

            //Create a list to populate
            List<PositionsPosition> positionsList = new List<PositionsPosition>();

            //Get the mappings for the products
            IEnumerable<PlanogramFieldMappingDto> mappings = this._dalCache.ExportContext.ProductMappings; 

            //Add the positions
            foreach (PlanogramPositionDto positionDto in _dalCache.PlanogramPositionDtoList.
                Where(p => p.PlanogramSubComponentId.Equals(subComponent.Id)).OrderBy(p => p.X).ThenBy(p=>p.Y).ThenBy(p=>p.Z))
            {
                //Get the product associated with the position
                PlanogramProductDto productDto = _dalCache.PlanogramProductDtoList.FirstOrDefault(p => p.Id.Equals(positionDto.PlanogramProductId));
                if (productDto == null) continue;

                //Get the custom attribute dto for the related product
                CustomAttributeDataDto customAttrData = _dalCache.CustomAttributeDataDtoList.First(
                    c => c.ParentId.Equals(productDto.Id) &&
                    c.ParentType == (Byte)CustomAttributeDataPlanogramParentType.PlanogramProduct);

                try
                {
                    #region Get Cappings information
                    Boolean hasLeftCap = ApolloExportHelper.PositionHasSideCappings(positionDto) && positionDto.IsXPlacedLeft;
                    if (hasLeftCap)
                    {
                        String content = String.Empty;

                        if (this.RetainSourcePlanLookAndFeel)
                        {
                            //inform user a second position will be created to retain look and feel
                            content = String.Format(Language.ExportLog_PositionLeftCapsWarning_CapsConverted_Content, productDto.Gtin, productDto.Name);
                        }
                        else
                        {
                            //remove left caps and inform user
                            hasLeftCap = false;
                            content = String.Format(Language.ExportLog_PositionLeftCapsWarning_CapsRemoved_Content, productDto.Gtin, productDto.Name);
                        }
                        this.DalCache.LogExportWarning(Language.ExportLog_PositionLeftCapsWarning_Description, content, PlanogramEventLogAffectedType.Positions);
                    }

                    Boolean hasRightCap = ApolloExportHelper.PositionHasSideCappings(positionDto) && !positionDto.IsXPlacedLeft;
                    if (hasRightCap)
                    {
                        String content = String.Empty;

                        if (this.RetainSourcePlanLookAndFeel)
                        {
                            //inform user a second position will be created to retain look and feel
                            content = String.Format(Language.ExportLog_PositionRightCapsWarning_CapsConverted_Content, productDto.Gtin, productDto.Name);
                        }
                        else
                        {
                            //remove left caps and inform user
                            hasRightCap = false;
                            content = String.Format(Language.ExportLog_PositionRightCapsWarning_CapsRemoved_Content, productDto.Gtin, productDto.Name);
                        }
                        this.DalCache.LogExportWarning(Language.ExportLog_PositionRightCapsWarning_Description, content, PlanogramEventLogAffectedType.Positions);
                    }
                    #endregion

                    PositionsPosition position;
                    ApolloPositionElementConverterBase converter = new ApolloPositionElementConverterShelf(ApolloPositionElementPositionType.Main);

                    #region Left Cap
                    if (hasLeftCap)
                    {
                        position = new PositionsPosition();

                        switch (apolloShelfType)
                        {
                            case ApolloShelfType.Shelf:
                                converter = new ApolloPositionElementConverterShelf(ApolloPositionElementPositionType.LeftCap);
                                break;

                            case ApolloShelfType.FreeHandSurface:
                                converter = new ApolloPositionElementConverterFreeHandSurface(ApolloPositionElementPositionType.LeftCap);
                                break;

                            case ApolloShelfType.Pegboard:
                                converter = new ApolloPositionElementConverterPeg(ApolloPositionElementPositionType.LeftCap);
                                break;

                            case ApolloShelfType.Coffin:
                            case ApolloShelfType.Basket:
                                converter = new ApolloPositionElementConverterCoffin(ApolloPositionElementPositionType.LeftCap);
                                break;

                            case ApolloShelfType.Sltowall:
                                converter = new ApolloPositionElementConverterSlotwall(ApolloPositionElementPositionType.LeftCap);
                                break;

                            case ApolloShelfType.Crossbar:
                                converter = new ApolloPositionElementConverterCrossbar(ApolloPositionElementPositionType.LeftCap);
                                break;

                            case ApolloShelfType.HangingBar:
                                converter = new ApolloPositionElementConverterHangingBar(ApolloPositionElementPositionType.LeftCap);
                                break;
                        }

                        converter.InitialiseLogging(this.DalCache.FilePath, this.DalCache.ExportContext.ExportEvents);
                        converter.Initialise(fixtureItem, fixture, fixtureAssembly, assemblyComponent, fixtureComponent, component, subComponent, customAttrData, productDto, positionDto, mappings, (positionsList.Count + 1), this.RetainSourcePlanLookAndFeel);
                        position = converter.Convert();

                        #region Set Performance
                        if (position != null)
                        {
                            if (this.DalCache.ExportContext.IncludePerformanceData)
                            {
                                String typeMatch = String.Format("{0}|", typeof(PositionsPosition).ToString());
                                foreach (PlanogramMetricMappingDto metricMappingDto in _dalCache.ExportContext.MetricMappings.Where(m => m.Source.StartsWith(typeMatch)))
                                {
                                    PlanogramPerformanceDataDto sourceDataDto = this.DalCache.PlanogramPerformanceDataDtoList.FirstOrDefault(p => p.PlanogramProductId.Equals(productDto.Id));
                                    if (sourceDataDto != null)
                                    {
                                        ApolloExportHelper.SetPerformanceFieldMappedValue<PositionsPosition>(metricMappingDto, position, this.DalCache.PlanogramPerformanceMetricDtoList, sourceDataDto);
                                    }
                                }
                            }
                        }

                        #endregion

                        if (position != null)
                        {
                            positionsList.Add(position);
                        }
                    }
                    #endregion

                    #region Main Facings Block with Top Cap
                    position = new PositionsPosition();

                    switch (apolloShelfType)
                    {
                        case ApolloShelfType.Shelf:
                            converter = new ApolloPositionElementConverterShelf(ApolloPositionElementPositionType.Main);
                            break;

                        case ApolloShelfType.FreeHandSurface:
                            converter = new ApolloPositionElementConverterFreeHandSurface(ApolloPositionElementPositionType.Main);
                            break;

                        case ApolloShelfType.Pegboard:
                            converter = new ApolloPositionElementConverterPeg(ApolloPositionElementPositionType.Main);
                            break;

                        case ApolloShelfType.Coffin:
                        case ApolloShelfType.Basket:
                            converter = new ApolloPositionElementConverterCoffin(ApolloPositionElementPositionType.Main);
                            break;

                        case ApolloShelfType.Sltowall:
                            converter = new ApolloPositionElementConverterSlotwall(ApolloPositionElementPositionType.Main);
                            break;

                        case ApolloShelfType.Crossbar:
                            converter = new ApolloPositionElementConverterCrossbar(ApolloPositionElementPositionType.Main);
                            break;

                        case ApolloShelfType.HangingBar:
                            converter = new ApolloPositionElementConverterHangingBar(ApolloPositionElementPositionType.Main);
                            break;
                    }
                    converter.InitialiseLogging(this.DalCache.FilePath, this.DalCache.ExportContext.ExportEvents);
                    converter.Initialise(fixtureItem, fixture, fixtureAssembly, assemblyComponent, fixtureComponent, component, subComponent, customAttrData, productDto, positionDto, mappings, (positionsList.Count + 1), this.RetainSourcePlanLookAndFeel);
                    position = converter.Convert();

                    #region Set Performance

                    if (this.DalCache.ExportContext.IncludePerformanceData)
                    {
                        String typeMatch = String.Format("{0}|", typeof(PositionsPosition).ToString());
                        foreach (PlanogramMetricMappingDto metricMappingDto in _dalCache.ExportContext.MetricMappings.Where(m => m.Source.StartsWith(typeMatch)))
                        {
                            PlanogramPerformanceDataDto sourceDataDto = this.DalCache.PlanogramPerformanceDataDtoList.FirstOrDefault(p => p.PlanogramProductId.Equals(productDto.Id));
                            if (sourceDataDto != null)
                            {
                                ApolloExportHelper.SetPerformanceFieldMappedValue<PositionsPosition>(metricMappingDto, position, this.DalCache.PlanogramPerformanceMetricDtoList, sourceDataDto);
                            }
                        }
                    }

                    #endregion

                    if (position != null)
                    {
                        positionsList.Add(position);
                    }
                    #endregion

                    #region Right Cap
                    if (hasRightCap)
                    {
                        position = new PositionsPosition();

                        switch (apolloShelfType)
                        {
                            case ApolloShelfType.Shelf:
                                converter = new ApolloPositionElementConverterShelf(ApolloPositionElementPositionType.RightCap);
                                break;

                            case ApolloShelfType.FreeHandSurface:
                                converter = new ApolloPositionElementConverterFreeHandSurface(ApolloPositionElementPositionType.RightCap);
                                break;

                            case ApolloShelfType.Pegboard:
                                converter = new ApolloPositionElementConverterPeg(ApolloPositionElementPositionType.RightCap);
                                break;

                            case ApolloShelfType.Coffin:
                            case ApolloShelfType.Basket:
                                converter = new ApolloPositionElementConverterCoffin(ApolloPositionElementPositionType.RightCap);
                                break;

                            case ApolloShelfType.Sltowall:
                                converter = new ApolloPositionElementConverterSlotwall(ApolloPositionElementPositionType.RightCap);
                                break;

                            case ApolloShelfType.Crossbar:
                                converter = new ApolloPositionElementConverterCrossbar(ApolloPositionElementPositionType.RightCap);
                                break;

                            case ApolloShelfType.HangingBar:
                                converter = new ApolloPositionElementConverterHangingBar(ApolloPositionElementPositionType.RightCap);
                                break;
                        }
                        converter.InitialiseLogging(this.DalCache.FilePath, this.DalCache.ExportContext.ExportEvents);
                        converter.Initialise(fixtureItem, fixture, fixtureAssembly, assemblyComponent, fixtureComponent, component, subComponent, customAttrData, productDto, positionDto, mappings, (positionsList.Count + 1), this.RetainSourcePlanLookAndFeel);
                        position = converter.Convert();

                        #region Set Performance

                        if (this.DalCache.ExportContext.IncludePerformanceData)
                        {
                            String typeMatch = String.Format("{0}|", typeof(PositionsPosition).ToString());
                            foreach (PlanogramMetricMappingDto metricMappingDto in _dalCache.ExportContext.MetricMappings.Where(m => m.Source.StartsWith(typeMatch)))
                            {
                                PlanogramPerformanceDataDto sourceDataDto = this.DalCache.PlanogramPerformanceDataDtoList.FirstOrDefault(p => p.PlanogramProductId.Equals(productDto.Id));
                                if (sourceDataDto != null)
                                {
                                    ApolloExportHelper.SetPerformanceFieldMappedValue<PositionsPosition>(metricMappingDto, position, this.DalCache.PlanogramPerformanceMetricDtoList, sourceDataDto);
                                }
                            }
                        }

                        #endregion

                        if (position != null)
                        {
                            positionsList.Add(position);
                        }
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    //Log error
                    this.DalCache.LogExportError(
                        Language.ExportLog_Position_Error_Description,
                        String.Format(Language.ExportLog_Position_Error_Content, productDto.Gtin, productDto.Name, ex.Message),
                        PlanogramEventLogAffectedType.Positions);
                }
            }
            if (positionsList.Any())
            {
                positionsElement = positionsList.ToArray();
            }

            return positionsElement;
        }

        /// <summary>
        /// Creates an empty shopping cart object to export to dalCache's Apollo file
        /// </summary>
        /// <returns></returns>
        private PositionsPosition[][] CreateApolloShoppingCart()
        {
            //Create an empty element for now as we have no way of knowing which shelf is a car park shelf
            PositionsPosition[][] positionsElement = new PositionsPosition[1][];

            positionsElement[0] = new PositionsPosition[0];

            return positionsElement;
        }

        #endregion

        #region CDT Methods
        /// <summary>
        ///     Instantiates a new instance of <see cref="PlanogramConsumerDecisionTreeDto"/> with the given <paramref name="planogramId" />.
        /// </summary>
        /// <param name="planogramId">The Id of the <c>Planogram</c> this instance of <c>Consumer Decision Tree</c> belongs to.</param>
        /// <returns>A new instance with the provided data and default values.</returns>
        public PlanogramConsumerDecisionTreeDto CreatePlanogramConsumerDecisionTreeDto()
        {
            PlanogramConsumerDecisionTreeDto dto;
            
            dto = new PlanogramConsumerDecisionTreeDto
            {
                Id = IdentityHelper.GetNextInt32(),
                Name = "Default Consumer Decision Tree",
                PlanogramId = planogramId
            };
            this._dalCache.PlanogramConsumerDecisionTreeDtoList.Add(dto);
            return dto;
        }

        /// <summary>
        ///     Instantiates a new list of instances of <see cref="PlanogramConsumerDecisionTreeLevelDto"/> with the given <paramref name="consumerDecisionTreeId" />.
        /// </summary>
        /// <param name="consumerDecisionTreeId">The Id of the <c>Consumer Decision Tree</c> these instances of <c>Consumer Decision Tree Level</c> belong to.</param>
        /// <returns>A new list of instances with the provided data and default values.</returns>
        public IEnumerable<PlanogramConsumerDecisionTreeLevelDto> CreatePlanogramConsumerDecisionTreeLevelDtos(Object consumerDecisionTreeId)
        {
            List<PlanogramConsumerDecisionTreeLevelDto> dtos;
           
            dtos = new List<PlanogramConsumerDecisionTreeLevelDto>
                   {
                       new PlanogramConsumerDecisionTreeLevelDto
                       {
                           Id = IdentityHelper.GetNextInt32(),
                           Name = "Root",
                           PlanogramConsumerDecisionTreeId = consumerDecisionTreeId
                       }
                   };
            this._dalCache.PlanogramConsumerDecisionTreeLevelDtoList.AddRange(dtos);
            return dtos;
        }

        /// <summary>
        ///     Instantiates a new list of instances of <see cref="PlanogramConsumerDecisionTreeLevelDto"/> with the given <paramref name="consumerDecisionTreeId" />.
        /// </summary>
        /// <param name="consumerDecisionTreeId">The Id of the <c>Consumer Decision Tree</c> these instances of <c>Consumer Decision Tree Level</c> belong to.</param>
        /// <returns>A new list of instances with the provided data and default values.</returns>
        public IEnumerable<PlanogramConsumerDecisionTreeNodeDto> CreatePlanogramConsumerDecisionTreeNodeDtos(Object consumerDecisionTreeId)
        {
            List<PlanogramConsumerDecisionTreeNodeDto> dtos;

            List<PlanogramConsumerDecisionTreeLevelDto> levelDtos = this._dalCache.PlanogramConsumerDecisionTreeLevelDtoList.Where(p => p.PlanogramConsumerDecisionTreeId.Equals(consumerDecisionTreeId)).ToList();

            dtos = levelDtos.Select(item => new PlanogramConsumerDecisionTreeNodeDto
                       {
                           Id = IdentityHelper.GetNextInt32(),
                           Name = "All Products",
                           PlanogramConsumerDecisionTreeId = consumerDecisionTreeId,
                           PlanogramConsumerDecisionTreeLevelId = item.Id
                       }).ToList();
            this._dalCache.PlanogramConsumerDecisionTreeNodeDtoList.AddRange(dtos);
            return dtos;
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Informs user that Text box annotations are not supported in Apollo exports
        /// </summary>
        private void CreateApolloTextboxAnnotations()
        {
            if (this.DalCache.PlanogramAnnotationDtoList.Any())
            {
                this.DalCache.LogExportWarning(
                    Language.ExportLog_SectionAnnotationWarning_Description, Language.ExportLog_SectionAnnotationWarning_Content, 
                    PlanogramEventLogAffectedType.Planogram);
            }
        }

        /// <summary>
        /// Determines whether child sub-components of a fixture can snap to notch
        /// </summary>
        /// <param name="fixtureComponent">The parent fixture to check</param>
        private PlanogramSubComponentDto GetBackboardSubComponent(PlanogramFixtureDto fixtureDto)
        {
            List<PlanogramFixtureComponentDto> fixtureComponents = _dalCache.PlanogramFixtureComponentDtoList.
                Where(c => c.PlanogramFixtureId.Equals(fixtureDto.Id)).ToList();

            if (fixtureComponents.Any())
            {
                foreach (PlanogramFixtureComponentDto fixtureComponent in fixtureComponents)
                {
                    PlanogramComponentDto backboardComponent = _dalCache.PlanogramComponentDtoList
                            .Where(c => c.ComponentType == (Byte)PlanogramComponentType.Backboard)
                            .FirstOrDefault(c => c.Id.Equals(fixtureComponent.PlanogramComponentId));

                    if (backboardComponent == null) continue;

                    //get it's subcomponent
                    PlanogramSubComponentDto backboardSubComponent = _dalCache.PlanogramSubComponentDtoList
                    .Where(s => s.PlanogramComponentId.Equals(backboardComponent.Id)).FirstOrDefault();
                    return backboardSubComponent;
                }
            }

            return null;
        }

        /// <summary>
        /// Calculates the upright spacing for a section. 
        /// Note: Apollo sections can have fixed or variable uprights. Variable uprights are stored in the Apollo 
        /// BLOB element and cannot be reproduced. Fixed upright spcing can be calculated but is only valid if all
        /// bays in the source plan hare the same width.
        /// </summary>
        /// <param name="fixtureItemDtos">The source planogram's fixture items</param>
        /// <param name="fixtureDtos">The source planogram's fixtures</param>
        /// <returns>The upright spacing to apply to the apollo file</returns>
        public Single CalculateApolloUprightSpacing()
        {
            Single returnValue = 0;

            //Can only set uprights if all bay are the same width and there is > 1 bay
            if (this.DalCache.PlanogramFixtureItemDtoList.Count == 1) return returnValue;

            Single bayWidth = -1;
            Single bayNumber = 0;
            foreach (PlanogramFixtureItemDto fixtureItem in this.DalCache.PlanogramFixtureItemDtoList.OrderBy(f => f.X))
            {
                PlanogramFixtureDto fixture = this.DalCache.PlanogramFixtureDtoList
                    .FirstOrDefault(f => f.Id.Equals(fixtureItem.PlanogramFixtureId));

                bayNumber++;

                if (fixture != null)
                {
                    PlanogramSubComponentDto backBoard = GetBackboardSubComponent(fixture);

                    // Catch case where the backboard is wider than the parent fixture and this is not the last 
                    // bay in the planogram - this will push out the later bays so tha tthe uprights will not
                    //match up with the bay locations behind the backboards
                    if (backBoard.Width != fixture.Width && (bayNumber < this.DalCache.PlanogramFixtureItemDtoList.Count))
                    {
                        return 0;
                    }

                    if (bayWidth < 0)
                    {
                        //Set first bay value
                        bayWidth = fixture.Width;
                    }
                    else
                    {
                        if (fixture.Width != bayWidth) return returnValue;
                    }
                }
            }

            return bayWidth;
        }


        /// <summary>
        /// Serializes the current ASpolooDataSet bjec to the passed stream
        /// </summary>
        /// <param name="planStream">The stream to serialize to</param>
        public void SerializePlanToStream(Stream planStream)
        {
            //write to the plan file
            XmlSerializer serializer = new XmlSerializer(_exportDataSet.GetType());
            serializer.Serialize(planStream, _exportDataSet);
        }

        /// <summary>
        /// Creates the planogram metric mappings
        /// </summary>
        private void CreatePlanogramPerformanceMetrics()
        {
            foreach (PlanogramMetricMappingDto metric in _dalCache.Context.MetricMappings)
            {
                _dalCache.PlanogramPerformanceMetricDtoList.Add(new PlanogramPerformanceMetricDto()
                {
                    Id = IdentityHelper.GetNextInt32(),
                    PlanogramPerformanceId = 1,
                    Name = metric.Name,
                    Description = metric.Description,
                    MetricId = metric.MetricId,
                    Direction = metric.Direction,
                    MetricType = metric.MetricType,
                    SpecialType = metric.SpecialType,
                    AggregationType = metric.AggregationType
                });
            }
        }

        private void MapCustomAttributes(object dto, IEnumerable<PlanogramFieldMappingDto> fieldMappings, object source, CustomAttributeDataPlanogramParentType customAttributeDataPlanogramParentType, object id, PlanogramEventLogAffectedType affectedType)
        {
            CustomAttributeDataDto customAttributes = _dalCache.CustomAttributeDataDtoList.FirstOrDefault(a => a.ParentId.Equals(id));
            Boolean newCustomAttribute = false;
            Type sourceType = source.GetType();
            Type dtoType = dto.GetType();
            Type customType = typeof(CustomAttributeDataDto);
           // Dictionary<String, PropertyInfo> properties = dto.GetType().GetProperties().ToDictionary(d => d.Name);
           // Dictionary<String, PropertyInfo> custom = typeof(CustomAttributeDataDto).GetProperties().ToDictionary(d => d.Name);
           // Dictionary<String, PropertyInfo> sourceProperties = sourceType.GetProperties().ToDictionary(d => d.Name);

            foreach (PlanogramFieldMappingDto mapping in fieldMappings)
            {
                String[] sourceList = mapping.Source.Split('|');
                if (sourceType.Name.Equals(sourceList[0], StringComparison.InvariantCultureIgnoreCase))
                {

                    PropertyInfo property = ApolloImportHelper.GetPropertyInfo(dtoType, mapping.Target);
                    PropertyInfo sourceProperty = ApolloImportHelper.GetPropertyInfo(sourceType, sourceList[1]);
                    if (!mapping.Target.Contains(PlanogramProduct.CustomAttributesProperty.Name) &&
                        property != null &&
                        sourceProperty != null)
                    {
                        MapSetValue(dto, source, property, sourceProperty, affectedType);
                    }
                    else
                    {
                        if (mapping.Target.Contains(PlanogramProduct.CustomAttributesProperty.Name))
                        {
                            String[] columnMapping = mapping.Target.Split('.');

                            if (columnMapping.Any())
                            {
                                property = ApolloImportHelper.GetPropertyInfo(customType, columnMapping[1]);
                                if (property != null &&
                                    sourceProperty != null)
                                {
                                    if (customAttributes == null)
                                    {
                                        newCustomAttribute = true;
                                        customAttributes = new CustomAttributeDataDto()
                                        {
                                            Id = IdentityHelper.GetNextInt32(),
                                            ParentType = (Byte)customAttributeDataPlanogramParentType,
                                            ParentId = id
                                        };
                                    }

                                    MapSetValue(customAttributes, source, property, sourceProperty, affectedType);
                                }
                            }
                        }
                    }
                }
            }

            if (newCustomAttribute)
            {
                _dalCache.CustomAttributeDataDtoList.Add(customAttributes);
            }
        }

        private void MapSetValue(object dto, object source, PropertyInfo property, PropertyInfo sourceProperty, PlanogramEventLogAffectedType affectedType)
        {
            String value = String.Empty;

            Object temp = sourceProperty.GetValue(source, null);

            value = temp == null ? String.Empty : temp.ToString();

            if (!SpacePlanningImportHelper.SetValue(dto, property, value))
            {
                SpacePlanningImportHelper.CreateErrorEventLogEntry(this.planogramId, this._dalCache.PlanogramEventLogDtoList, String.Format(Language.CustomMappingError, sourceProperty.Name, property.Name), String.Format(Language.CustomMappingError_Description, sourceProperty.Name, property.Name, value), PlanogramEventLogEntryType.Warning, PlanogramEventLogEventType.Import, affectedType);
            }
        }
        
        
        /// <summary>
        /// Returns the next id
        /// </summary>
        private static String GetNextId()
        {
            lock (_nextIdLock)
            {
                _nextId++;
                return _nextId.ToString();
            }
        }
        
        /// <summary>
        /// Takes the global Apollo Units of Measure type and converts it into a ccm UOM for Length
        /// </summary>
        /// <param name="uom"></param>
        /// <returns></returns>
        private static PlanogramLengthUnitOfMeasureType ConvertUnitsOfMeasureType_Length(UOM uom)
        {
            switch(uom)
            {
                case UOM.Metric:
                    return PlanogramLengthUnitOfMeasureType.Centimeters;
                case UOM.Imperial:
                    return PlanogramLengthUnitOfMeasureType.Inches;
                case UOM.Unknown:
                    return PlanogramLengthUnitOfMeasureType.Unknown;
                default:
                    return PlanogramLengthUnitOfMeasureType.Unknown;
            }
        }

        /// <summary>
        /// Takes the global Apollo Units of Measure type and converts it into a ccm UOM for Volume
        /// </summary>
        /// <param name="uom"></param>
        /// <returns></returns>
        private static PlanogramVolumeUnitOfMeasureType ConvertUnitsOfMeasureType_Volume(UOM uom)
        {
            switch (uom)
            {
                case UOM.Metric:
                    return PlanogramVolumeUnitOfMeasureType.Litres;
                case UOM.Imperial:
                    return PlanogramVolumeUnitOfMeasureType.CubicFeet;
                case UOM.Unknown:
                    return PlanogramVolumeUnitOfMeasureType.Unknown;
                default:
                    return PlanogramVolumeUnitOfMeasureType.Unknown;
            }
        }

        /// <summary>
        /// Takes the global Apollo Units of Measure type and converts it into a ccm UOM for Area
        /// </summary>
        /// <param name="uom"></param>
        /// <returns></returns>
        private static PlanogramAreaUnitOfMeasureType ConvertUnitsOfMeasureType_Area(UOM uom)
        {
            switch (uom)
            {
                case UOM.Metric:
                    return PlanogramAreaUnitOfMeasureType.SquareCentimeters;
                case UOM.Imperial:
                    return PlanogramAreaUnitOfMeasureType.SquareInches;
                case UOM.Unknown:
                    return PlanogramAreaUnitOfMeasureType.Unknown;
                default:
                    return PlanogramAreaUnitOfMeasureType.Unknown;
            }
        }

        /// <summary>
        /// Takes the global Apollo Units of Measure type and converts it into a ccm UOM for Weight
        /// </summary>
        /// <param name="uom"></param>
        /// <returns></returns>
        private static PlanogramWeightUnitOfMeasureType ConvertUnitsOfMeasureType_Weight(UOM uom)
        {
            switch (uom)
            {
                case UOM.Metric:
                    return PlanogramWeightUnitOfMeasureType.Kilograms;
                case UOM.Imperial:
                    return PlanogramWeightUnitOfMeasureType.Pounds;
                case UOM.Unknown:
                    return PlanogramWeightUnitOfMeasureType.Unknown;
                default:
                    return PlanogramWeightUnitOfMeasureType.Unknown;
            }
        }

        /// <summary>
        /// Takes an apollo orientation and converts it into a ccm orientation for
        /// the main facing
        /// </summary>
        /// <param name="orientationType"></param>
        /// <returns></returns>
        private static Byte ConvertApolloToCCMPositionOrientation(ApolloOrientationType orientationType)
        {
            switch (orientationType)
            {
                case ApolloOrientationType.FrontEnd:
                    return (Byte)PlanogramPositionOrientationType.Front0;
                case ApolloOrientationType.FrontSide:
                    return (Byte)PlanogramPositionOrientationType.Front90;
                case ApolloOrientationType.SideFront:
                    return (Byte)PlanogramPositionOrientationType.Right90;
                case ApolloOrientationType.SideEnd:
                    return (Byte)PlanogramPositionOrientationType.Right0;
                case ApolloOrientationType.EndFront:
                    return (Byte)PlanogramPositionOrientationType.Top0;
                case ApolloOrientationType.EndSide:
                    return (Byte)PlanogramPositionOrientationType.Top90;
                default:
                    return (Byte)PlanogramPositionOrientationType.Front0;
            }
        }


        /// <summary>
        /// Takes an apollo orientation and converts it into a ccm orientation for
        /// the main facing for Coffins/chests
        /// </summary>
        /// <param name="orientationType"></param>
        /// <returns></returns>
        private static Byte ConvertApolloCoffinToCCMChestPositionOrientation(ApolloOrientationType orientationType)
        {
            switch (orientationType)
            {
                case ApolloOrientationType.FrontEnd:
                    return (Byte)PlanogramPositionOrientationType.Top0;
                case ApolloOrientationType.FrontSide:
                    return (Byte)PlanogramPositionOrientationType.Right90;
                case ApolloOrientationType.SideFront:
                    return (Byte)PlanogramPositionOrientationType.Front90;
                case ApolloOrientationType.SideEnd:
                    return (Byte)PlanogramPositionOrientationType.Top90;
                case ApolloOrientationType.EndFront:
                    return (Byte)PlanogramPositionOrientationType.Front0;
                case ApolloOrientationType.EndSide:
                    return (Byte)PlanogramPositionOrientationType.Right0;
                default:
                    return (Byte)PlanogramPositionOrientationType.Front0;
            }
        }

        /// <summary>
        /// Takes an apollo orientation and converts it into a ccm orientation for
        /// top caps
        /// </summary>
        /// <param name="orientationType"></param>
        /// <returns></returns>
        private static Byte GetPositionLayoverOrientationType(ApolloOrientationType orientationType)
        {
            switch (orientationType)
            {
                case ApolloOrientationType.FrontEnd:
                    return (Byte)PlanogramPositionOrientationType.Top0;
                case ApolloOrientationType.FrontSide:
                    return (Byte)PlanogramPositionOrientationType.Right90;
                case ApolloOrientationType.SideFront:
                    return (Byte)PlanogramPositionOrientationType.Front90;
                case ApolloOrientationType.SideEnd:
                    return (Byte)PlanogramPositionOrientationType.Top90;
                case ApolloOrientationType.EndFront:
                    return (Byte)PlanogramPositionOrientationType.Front0;
                case ApolloOrientationType.EndSide:
                    return (Byte)PlanogramPositionOrientationType.Right90;
                default:
                    return (Byte)PlanogramPositionOrientationType.Top0;
            }
        }

        /// <summary>
        /// Takes an apollo orientation and converts it into a ccm orientation for
        /// right caps
        /// </summary>
        /// <param name="orientationType"></param>
        /// <returns></returns>
        private static Byte GetPositionRightOrientationType(ApolloOrientationType orientationType)
        {
            switch (orientationType)
            {
                case ApolloOrientationType.FrontEnd:
                    return (Byte)PlanogramPositionOrientationType.Right0;
                case ApolloOrientationType.FrontSide:
                    return (Byte)PlanogramPositionOrientationType.Top90;
                case ApolloOrientationType.SideFront:
                    return (Byte)PlanogramPositionOrientationType.Top0;
                case ApolloOrientationType.SideEnd:
                    return (Byte)PlanogramPositionOrientationType.Front0;
                case ApolloOrientationType.EndFront:
                    return (Byte)PlanogramPositionOrientationType.Right90;
                case ApolloOrientationType.EndSide:
                    return (Byte)PlanogramPositionOrientationType.Front90;
                default:
                    return (Byte)PlanogramPositionOrientationType.Right0;
            }
        }

        /// <summary>
        /// Takes a Apollo merchandising method and converts it into a CCM merchandising style
        /// </summary>
        /// <param name="merchandisingStyleType"></param>
        /// <returns></returns>
        private static Byte GetPositionMerchandisingStyle(ApolloMerchMethodType merchandisingStyleType)
        {
            switch (merchandisingStyleType)
            {
                case ApolloMerchMethodType.Basket:
                case ApolloMerchMethodType.Divider:
                case ApolloMerchMethodType.Dump:
                case ApolloMerchMethodType.HalfPyramidLog:
                case ApolloMerchMethodType.PyramidLog:
                case ApolloMerchMethodType.RectangleLog:
                case ApolloMerchMethodType.MultiHand:
                case ApolloMerchMethodType.Hand:
                    return (Byte)PlanogramPositionMerchandisingStyle.Unit;

                case ApolloMerchMethodType.TrayPack:
                    return (Byte)PlanogramPositionMerchandisingStyle.Tray;
                case ApolloMerchMethodType.Case:
                    return (Byte)PlanogramPositionMerchandisingStyle.Case;
                default:
                    return (Byte)PlanogramPositionMerchandisingStyle.Unit;
            }
        }

        #endregion

        #region Dispose
        /// <summary>
        /// Called when this instance is disposing
        /// </summary>
        public void Dispose()
        {
           
        }
        #endregion

    }

}