﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24779 : D.Pleasance
//  Created
#endregion
#endregion

using Galleria.Framework.Dal;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Galleria.Framework.Planograms.Dal.Xmz
{
    public class DalBase : Galleria.Framework.Dal.DalBase
    {
        #region Properties
        /// <summary>
        /// The context for this dal
        /// </summary>
        public DalContext DalContext
        {
            get { return (DalContext)((IDal)this).DalContext; }
            set { ((IDal)this).DalContext = value; }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Creates a clone of an object
        /// </summary>
        /// <typeparam name="T">The type being cloned</typeparam>
        /// <param name="obj">the instance to clone</param>
        /// <returns>a clone of the provided instance</returns>
        protected static T Clone<T>(T obj)
        {
            if (obj == null)
            {
                return default(T);
            }
            IFormatter formatter = new BinaryFormatter();
            using (Stream stream = new MemoryStream())
            {
                formatter.Serialize(stream, obj);
                stream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(stream);
            }
        }

        /// <summary>
        /// Called when disposing of this instance
        /// </summary>
        public override void Dispose()
        {
            // Nothing to do
        }
        #endregion
    }
}