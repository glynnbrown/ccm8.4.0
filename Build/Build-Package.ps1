#################################################
# Build Script: Customer Centric Merchandising

$buildPath = 'Build' # Directory in which build files are located
$sourcePath = 'Source' # Directory in which the source code will be placed
$binariesPath = 'Binaries' # Directory in which the compiled binaries will be placed
$packagesPath = 'Packages' # Directory in which installation packages will be placed
$unitTestsPath = 'Tests' # Directory in which test results will be placed

$buildCommand = 'C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe' # The location of the build tool
$obfuscationCommand = 'C:\Program Files\Red Gate\SmartAssembly 6\SmartAssembly.com' # The location of the obfuscation tool
$packageCommand = 'C:\Program Files (x86)\InstallShield\2012 SAB\System\IsCmdBld.exe' # The location of the package tool
$unitTestCommand = 'C:\Program Files (x86)\NUnit 2.6.4\Bin\nunit-console.exe' # The location of unit test tool
$zipCommandCommand = 'C:\Program Files\7-Zip\7z.exe' # The location of zip tool

$jiraEndpoint = "https://galleria-rts.atlassian.net/rest/api/latest/{0}" # Jira rest api endpoint
$jiraUsername = "jenkins" # Username used to access the Jira rest api
$jiraPassword = "9s6RR#743m4Z" # Password used to access the Jira rest api

$packageDependenciesPath = '\\jupiter\SS-LibraryOutput\MERGEMODULES\MSM\GALLERIA,\\jupiter\SS-LibraryOutput\MERGEMODULES\MSM\THIRDPARTY' # Directory in which package dependencies live
$packageFrameworkPath = 'C:\WINDOWS\Microsoft.NET\Framework\v4.0.30319' # Framework version to use when building a package

# A dictionary of solutions to build (solution file path relative to source code root, binaries output folder name)
$solutions = @{
	'Solution\Galleria.Ccm.sln' = 'Customer Centric Merchandising';
}

# An array of license files that are inserted into the source code (license file name relative to build path)
$licenseFiles = @(
	'Aspose.Total.10.lic'
)

# An array of assemblies to obfuscate (assembly file path relative to binaries output folder)
$obfuscationAssemblies = @(
)

# An array of installation packages to build (installation package project file relative to source code root)
$packages = @(
    'Installation\CCM Cloud Sync Service.ism',
	'Installation\CCM Processor.ism',
	'Installation\CCM Space Automation.ism',
	'Installation\CCM Space Planning.ism'
)

# An array of unit test projects to run (unit test file relative to source code root)
$unitTests = @(
	'Solution\Galleria.Ccm.nunit',
	'Solution\Galleria.Framework.Planograms.nunit'
)

# A dictionary of folders to compress (folder relative to workspace root) and their archive names
$compressFolders = @{
	'Solution' = 'Source';
	'Build' = 'Source';
	'Installation' = 'Source';
	$binariesPath = $binariesPath;
	$packagesPath = $packagesPath;
}

#################################################
# Function: ListEnvironmentVariables
#
# Lists all environment variables for the build
#
function ListEnvironmentVariables()
{
	write-host "***** $(get-date) : Listing Environment Variables *****"
	get-childitem env:
	write-host ""
}

#################################################
# Function: BuildPackages
#
# Builds all packages
#
function BuildPackages()
{
	foreach ($package in $packages)
	{
		$packageFile = '{0}\{1}' -f $env:WORKSPACE, $package
		
		write-host "BUILD PACKAGES $packageFile"
		
		if (test-path $packageFile)
		{
			UpdatePackage $packageFile
			BuildPackage $packageFile
		}
	}
}

#################################################
# Function: UpdatePackage
#
# Updates the specified package file
#
function UpdatePackage($packageFile)
{
	write-host "***** $(get-date) : Updating Package '$($packageFile)' *****"
	write-host ""
	
	$productVersion = '{0}.{1}' -f $env:BUILD_VERSION, $env:PARENT_BUILD
	$productName = '{0} v{1}' -f (get-item $packageFile).Basename, $productVersion

	$project = new-object -comobject ISWiAuto18.ISWiProject		
	$result = $project.OpenProject($packageFile)
	$project.ProductVersion = $productVersion
	$project.ProductName = $productName
	$project.ISWiSISProperties.Item('Title').Value = $productName
	$project.ISWiSISProperties.Item('Subject').Value = $productName

	if ($project.ISWiPathVariables.Count -gt 0)
	{
		for ($i=1; $i -le $project.ISWiPathVariables.Count; $i++)
		{
			$variable = $project.ISWiPathVariables.Item($i)
			if ($variable.Name -eq 'GABS_SOURCE_PATH')
			{
				$variableValue = "{0}" -f $env:WORKSPACE
				write-host "Remapping variable '$($variable.Name)' from '$($variable.Value)' to '$($variableValue)'"
				$variable.Value = $variableValue
			}
			elseif ($variable.Name -eq 'GABS_BINARIES_PATH')
			{
				$variableValue = "{0}\{1}" -f $env:WORKSPACE, $binariesPath
				write-host "Remapping variable '$($variable.Name)' from '$($variable.Value)' to '$($variableValue)'"
				$variable.Value = $variableValue
			}			
			elseif ($variable.Name -eq 'GABS_EXECUTABLES_PATH')
			{
				$variableValue = "{0}\{1}" -f $env:WORKSPACE, $binariesPath
				write-host "Remapping variable '$($variable.Name)' from '$($variable.Value)' to '$($variableValue)'"
				$variable.Value = $variableValue
			}
			$variable = $null
		}
		
		write-host ""
	}	
	
	if ($project.ISWiComponents.Count -gt 0)
	{	
		for ($i=1; $i -le $project.ISWiComponents.Count; $i++)
		{
			$component = $project.ISWiComponents.Item($i)
			for ($j=1; $j -le $component.ISWiFiles.Count; $j++)
			{
				$file = $component.ISWiFiles.Item([ref]$j)
				$fileFullPath = $file.FullPath
				
				write-host "### 1. Re-mapping '$($fileFullPath)'"
				write-host "### Binaries path: '$($binariesPath)'"
				
				if (! ($fileFullPath -like "$env:WORKSPACE\*"))
				{
					$filePathSourcePattern = '.*\\{0}\\[\w\-. ]+\\(.*)' -f $sourcePath
					$fileFullPath = $fileFullPath -replace $filePathSourcePattern, ('{0}\$1' -f $env:WORKSPACE)
					write-host "### 2. Re-mapping '$($fileFullPath)'"
				
					$filePathBinariesPattern = '.*\\{0}\\[\w\-. ]+\\(.*)' -f $binariesPath
					$fileFullPath = $fileFullPath -replace $filePathBinariesPattern, ('{0}\{1}\$1' -f $env:WORKSPACE, $binariesPath)
					write-host "### 3. Re-mapping '$($fileFullPath)'"
				
					$filePathExecutablesPattern = '.*\\Executables\\[\w\-. ]+\\(.*)'
					$fileFullPath = $fileFullPath -replace $filePathExecutablesPattern, ('{0}\{1}\$1' -f $env:WORKSPACE, $binariesPath)
					write-host "### 4. Re-mapping '$($fileFullPath)'"
				
					write-host "Remapping file '$($file.FullPath)' to '$($fileFullPath)'"
				}
			
				if (! (test-path $fileFullPath)) { throw "File '$($fileFullPath))' Does Not Exist" }

				$file.FullPath = $fileFullPath
				$file = $null
			}
			$component = $null
		}
		
		write-host ""			
	}

	write-host "Saving package '$($packageFile)'"
	
	$result = $project.SaveProject()
	$result = $project.CloseProject()
	$project = $null
	
	write-host ""
}

#################################################
# Function: BuildPackage
#
# Builds the specified package
#
function BuildPackage($packageFile)
{
	write-host "***** $(get-date) : Building Package '$($packageFile)' *****"
	write-host ""

	$packageOutputPath = '{0}\InstallShield' -f $env:WORKSPACE
	$packageName = (get-item $packageFile).Basename
	$packageBuildPath = '{0}\{1}' -f $packageOutputPath, $packageName
	$packageBuildFile = '{0}\{1}.ism' -f $packageOutputPath, $packageName

	if (! (test-path $packageBuildPath)) { new-item $packageBuildPath -type directory | out-null }
	
	write-host "Copying '$($packageFile)' to '$($packageBuildFile)'"
	write-host ""
	copy-item $packageFile $packageBuildFile
	
	&$packageCommand /p $packageBuildFile /b $packageBuildPath /o $packageDependenciesPath /t $packageFrameworkPath /c COMP /x
	if ($lastexitcode -eq 1) { throw 'Building Package Failed' }
	
	write-host ""
	
	$files = get-childitem -path $packageBuildPath -recurse -filter '*.msi'
	if ($files.Count -gt 0)
	{
		if (! (test-path $packagesPath)) { new-item $packagesPath -type directory | out-null }			
		
		foreach ($file in $files)
		{
			$fileSource = $file.FullName
			$fileDestination = '{0}\{1} v{2}.{3}.msi' -f $packagesPath, $packageName, $env:BUILD_VERSION, $env:PARENT_BUILD
		
			write-host "Copying '$($fileSource)' to '$($fileDestination)'"
			copy-item $fileSource $fileDestination
		}
	}
	else
	{
		throw "Building Package Failed"
	}
	
	write-host ""	
}

#################################################
# BUILD PROCESS
#
# This is the main build process
#
try
{
	ListEnvironmentVariables
	BuildPackages
}
catch [system.exception]
{
	write-host ""
	write-host $_.exception.message;
	exit -1
}