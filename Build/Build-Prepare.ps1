#################################################
# Build Script: Customer Centric Merchandising

$buildPath = 'Build' # Directory in which build files are located
$sourcePath = '' # Directory in which the source code will be placed
$binariesPath = 'Binaries' # Directory in which the compiled binaries will be placed
$packagesPath = 'Packages' # Directory in which installation packages will be placed
$unitTestsPath = 'Tests' # Directory in which test results will be placed

$buildCommand = 'C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe' # The location of the build tool
$obfuscationCommand = 'C:\Program Files\Red Gate\SmartAssembly 6\SmartAssembly.com' # The location of the obfuscation tool
$packageCommand = 'C:\Program Files (x86)\InstallShield\2012 SAB\System\IsCmdBld.exe' # The location of the package tool
$unitTestCommand = 'C:\Program Files (x86)\NUnit 2.6.4\Bin\nunit-console.exe' # The location of unit test tool
$zipCommandCommand = 'C:\Program Files\7-Zip\7z.exe' # The location of zip tool
$nuGetRestoreCommand = 'D:\Tools\Nuget\Nuget.exe' # The location of the NuGet packages tool

$jiraEndpoint = "https://galleria-rts.atlassian.net/rest/api/latest/{0}" # Jira rest api endpoint
$jiraUsername = "jenkins" # Username used to access the Jira rest api
$jiraPassword = "9s6RR#743m4Z" # Password used to access the Jira rest api

$packageDependenciesPath = '\\jupiter\SS-LibraryOutput\MERGEMODULES\MSM\GALLERIA,\\jupiter\SS-LibraryOutput\MERGEMODULES\MSM\THIRDPARTY' # Directory in which package dependencies live
$packageFrameworkPath = 'C:\WINDOWS\Microsoft.NET\Framework\v4.0.30319' # Framework version to use when building a package

# A dictionary of solutions to build (solution file path relative to source code root, binaries output folder name)
$solutions = @{
	'Solution\Galleria.Ccm.sln' = 'Customer Centric Merchandising';
}

# An array of license files that are inserted into the source code (license file name relative to build path)
$licenseFiles = @(
	'Aspose.Total.10.lic'
)

# An array of assemblies to obfuscate (assembly file path relative to binaries output folder)
$obfuscationAssemblies = @(
)

# An array of installation packages to build (installation package project file relative to source code root)
$packages = @(
    'Installation\CCM Cloud Sync Service.ism',
	'Installation\CCM Processor.ism',
	'Installation\CCM Space Automation.ism',
	'Installation\CCM Space Planning.ism'
)

# An array of unit test projects to run (unit test file relative to source code root)
$unitTests = @(
	'Solution\Galleria.Ccm.nunit',
	'Solution\Galleria.Framework.Planograms.nunit'
)

# An array of folders to compress (folder relative to workspace root)
$compressFolders = @(
	$sourcePath,
	$binariesPath,
	$packagesPath
)

#################################################
# Function: ListEnvironmentVariables
#
# Lists all environment variables for the build
#
function ListEnvironmentVariables()
{
	write-host "***** $(get-date) : Listing Environment Variables *****"
	get-childitem env:
	write-host ""
}

#################################################
# Function: GetWorkItems
#
# Returns a dictionary of all work items that
# are included within this build
#
function GetWorkItems()
{
	write-host "***** $(get-date) : Getting Work Items *****"
	write-host ""
	
	# define an array to hold our results
	$workItems = @{}
	
	# extract the jira project from the build name
	$project = $env:JOB_NAME.Substring(0, 3)
	
	#build required headers
	$headers = @{}
	$authorizationHeader = '{0}:{1}' -f $jiraUsername, $jiraPassword
	$authorizationHeader = [System.Text.Encoding]::UTF8.GetBytes($authorizationHeader)
	$authorizationHeader = [System.Convert]::ToBase64String($authorizationHeader)
	$authorizationHeader = 'Basic {0}' -f $authorizationHeader
	$headers = @{ Authorization = $authorizationHeader }
	
	# loop through a load of builds in order
	# to capture any items that in the release
	# build or a critical patch
	for($i=0; $i -le 99; $i++)
	{
		# build the version string
		$version = $env:JIRA_VERSION
		if ($i -gt 0)
		{
			$version = "0" + $i
			$version = $env:JOB_NAME + 'CP' + $version.Substring($version.Length - 2, 2)
		}
		
		# query jira to return all
		# items awaiting next build
		try
		{
			$jiraAction = 'search?jql=project="{0}" and fixVersion="{1}" and status="{2}"&maxResults=999' -f $project, $version, 'Awaiting Build'
			$jiraUrl = $jiraEndpoint -f $jiraAction
			$jiraResults = Invoke-WebRequest -Uri $jiraUrl -Headers $headers -Method GET -ContentType 'application/json' | ConvertFrom-Json
		}
		catch
		{
			break
		}
		
		# enumerate through each result and
		# add to the dictionary of work items
		foreach($jiraIssue in $jiraResults.issues)
		{
			# locate the custom fields that represent
			# heldesk number and release note description
			$jiraHelpdeskNumber = ''
			$jiraReleaseNotesDescription = ''
			$jiraAction = 'issue/{0}/editmeta' -f $jiraIssue.Key
			$jiraUrl = $jiraEndpoint -f $jiraAction
			$jiraResults = Invoke-WebRequest -Uri $jiraUrl -Headers $headers -Method GET -ContentType 'application/json' | ConvertFrom-Json
			foreach ($property in $jiraResults.fields.PSObject.Properties)
			{
				$field = $jiraResults.fields.($property.Name)
				if ($field.Name -eq 'Helpdesk #')
				{
					$jiraHelpdeskNumber = $jiraIssue.fields.($property.Name)
				}
				elseif ($field.Name -eq 'Release Notes Description')
				{
					$jiraReleaseNotesDescription = $jiraIssue.fields.($property.Name)
				}
			}
			if ($jiraHelpdeskNumber -eq '')
			{
				throw "Helpdesk Number Field Does Not Exist"
			}		
			if ($jiraReleaseNotesDescription -eq '')
			{
				throw "Release Notes Description Field Does Not Exist"
			}
		
			$workItemKey = $jiraIssue.key
			$workItemValue = "{0}|{1}|{2}|{3}" -f $jiraIssue.fields.summary, $jiraHelpdeskNumber, $jiraReleaseNotesDescription, $jiraIssue.self
			$workItems.Add($workItemKey, $workItemValue)
		}
	}
	
	# check that we have some work items ready to build
	if ($workItems.Count -eq 0)
	{
		write-host "No Work Items Awaiting Build"
	}
	else
	{
		foreach ($workItem in $workItems.GetEnumerator())
		{
			write-host $workItem.Key
		}
	}
	
	write-host ""

	return $workItems	
}

#################################################
# Function: MoveSourceCode
#
# Moves the source code from the root of the
# workspace to the Source directory
#
function MoveSourceCode()
{
	$items = get-childitem
	if ($items.Count -gt 0)
	{
		write-host "***** $(get-date) : Moving Source Code *****"
		write-host ""
	
		if (! (test-path $sourcePath)) { new-item $sourcePath -type directory | out-null }
	
		foreach ($item in $items)
		{
			write-host "Moving '$($item.Name)' to '$($sourcePath)'"
			move-item $item.Name $sourcePath
		}
	
		write-host ""
	}
}

#################################################
# Function: UpdateAssemblyVersions
#
# Locates all AssemblyInfo.cs files within the source
# code and set the AssemblyVersion and AssemblyFileVersion
# attributes to the correct version
#
function UpdateAssemblyVersions()
{
	$files = get-childitem -path $sourcePath -recurse -filter 'AssemblyInfo.cs'
	if ($files.Count -gt 0)
	{
		write-host "***** $(get-date) : Updating Assembly Versions *****"
		write-host ""
	
		$assemblyVersionPattern = 'AssemblyVersion\("[0-9]+(\.([0-9]+|\*)){1,3}"\)'
		$assemblyFileVersionPattern = 'AssemblyFileVersion\("[0-9]+(\.([0-9]+|\*)){1,3}"\)'
		$assemblyVersion = 'AssemblyVersion("{0}.{1}")' -f $env:BUILD_VERSION, $env:BUILD_NUMBER
		$assemblyFileVersion = 'AssemblyFileVersion("{0}.{1}")' -f $env:BUILD_VERSION, $env:BUILD_NUMBER
    
		foreach ($file in $files)
		{
			write-host "Updating '$($file.FullName)'"
			(get-content $file.FullName) | foreach-object { 
				% {$_ -replace $assemblyVersionPattern, $assemblyVersion } |
				% {$_ -replace $assemblyFileVersionPattern, $assemblyFileVersion }
			} | out-file $file.FullName -encoding UTF8 -force		
		}

		write-host ""
	}
}

#################################################
# Function: ReplaceLicenseFiles
#
# Replaces license files within the source code
#
function ReplaceLicenseFiles()
{
	if ($licenseFiles.Count -gt 0)
	{
		write-host "***** $(get-date) : Replacing License Files *****"
		write-host ""
		
		foreach ($licenseFile in $licenseFiles)
		{
			$licenseFilePath = '{0}\{1}\{2}\{3}' -f $env:WORKSPACE, $sourcePath, $buildPath, $licenseFile
			
			if (test-path $licenseFilePath)
			{
				$licenseFilesToReplace = get-childitem -path $sourcePath -recurse -filter $licenseFile | where-object {$_.FullName -ne $licenseFilePath}
				foreach ($licenseFileToReplace in $licenseFilesToReplace)
				{
					write-host "Replacing '$($licenseFileToReplace.FullName)'"
					copy-item $licenseFilePath $licenseFileToReplace.FullName
				}
			}
		}
		
		write-host ""
	}
}

#################################################
# Function: BuildSolutions
#
# Builds the all the defined solutions
#
function BuildSolutions()
{	
	foreach ($solution in $solutions.GetEnumerator())
	{
		$solutionFile = '{0}\{1}\{2}' -f $env:WORKSPACE, $sourcePath, $solution.Name
		$solutionOutput = $solution.Value
	
		if (test-path $solutionFile)
		{
			$solutionPath = (get-item $solutionFile).DirectoryName		
			
			BuildSolution $solutionFile $solutionPath
			MoveBinaries $solutionFile $solutionPath $solutionOutput 
		}
	}
}

#################################################
# Function: BuildSolution
#
# Builds the specified solution using MSBuild
#
function BuildSolution($solutionFile, $solutionPath)
{
	write-host "***** $(get-date) : Building Solution '$($solutionFile)' *****"
	write-host ""	
	
	write-host "NuGet restoring"	
	&$nuGetRestoreCommand restore $solutionFile
	write-host "NuGet restored"	
	
	&$buildCommand $solutionFile /property:Configuration=Release
	if (! $?) { throw 'Building Solution Failed' }	
	
	write-host ""
}

#################################################
# Function: MoveBinaries
#
# Moves the binaries output of a solution to the
# correct location under the binaries directory
#
function MoveBinaries($solutionFile, $solutionPath, $solutionOutput)
{
	$binPath = '{0}\Bin' -f $solutionPath
	$configurationPath = '{0}\Release' -f $binPath
	$destinationPath = '{0}\{1}' -f $binariesPath, $solutionOutput
	
	if (test-path $configurationPath)
	{
		write-host "***** $(get-date) : Moving Binaries '$($solutionFile)' *****"
		write-host ""
	
		if (! (test-path $binariesPath)) { new-item $binariesPath -type directory | out-null }
	
		write-host "Moving '$($configurationPath)' to '$($destinationPath)'"
		move-item $configurationPath $destinationPath
		remove-item $binPath -force -recurse
	
		write-host ""
	}
}

#################################################
# Function: ObfuscateAssemblies
#
# Obfuscates all required assemblies within the solution
#
function ObfuscateAssemblies()
{
	foreach ($obfuscationAssembly in $obfuscationAssemblies)
	{
		$obfuscationAssemblyPath = '{0}\{1}\{2}' -f $env:WORKSPACE, $binariesPath, $obfuscationAssembly
		
		if (test-path $obfuscationAssemblyPath)
		{
			ObfuscateAssembly $obfuscationAssemblyPath
		}
	}
}

#################################################
# Function: ObfuscateAssemblies
#
# Obfuscates the specified assembly
#
function ObfuscateAssembly($obfuscationAssemblyPath)
{
	write-host "***** $(get-date) : Obfuscating Assembly '$($obfuscationAssemblyPath)' *****"
	write-host ""
	
	$obfuscationAssemblyName = (get-item $obfuscationAssemblyPath).Basename
	$obfuscationAssemblyTemplatePath = '{0}\{1}\{2}\{3}.saproj' -f $env:WORKSPACE, $sourcePath, $buildPath, $obfuscationAssemblyName

	write-host "Updating '$($obfuscationAssemblyTemplatePath)'"
	(get-content $obfuscationAssemblyTemplatePath) | foreach-object {
		% {$_ -replace '{AssemblyName}', $obfuscationAssemblyName} |
		% {$_ -replace '{AssemblyPath}', $obfuscationAssemblyPath}
	} | out-file $obfuscationAssemblyTemplatePath -encoding UTF8 -force
	write-host ""	

	&$obfuscationCommand /build $obfuscationAssemblyTemplatePath
	if (! $?) { throw 'Obfuscation Failed' }
	
	write-host ""	
}

#################################################
# UpdateWorkItems
#
# Updates the status of all work items to indicate
# that they have been included within this build
#
function UpdateWorkItems($workItems)
{
	if ($workItems.Count -gt 0)
	{
		write-host "***** $(get-date) : Updating Work Items *****"
		write-host ""
		
		# build the basic authentication header
		# in order to be able to access the rest api
		$authorizationHeader = '{0}:{1}' -f $jiraUsername, $jiraPassword
		$authorizationHeader = [System.Text.Encoding]::UTF8.GetBytes($authorizationHeader)
		$authorizationHeader = [System.Convert]::ToBase64String($authorizationHeader)
		$authorizationHeader = "Basic {0}" -f $authorizationHeader
		$headers = @{ Authorization = $authorizationHeader }		
	
		# enumerate through all the work items that require updating
		foreach ($workItem in $workItems.GetEnumerator())
		{
			write-host "Setting $($workItem.Key) to Awaiting Test"
			
			# locate the workflow transition to move items
			# from awaiting build to awaiting test
			$jiraTransitionId = ''
			$jiraAction = 'issue/{0}/transitions' -f $workItem.Key
			$jiraUrl = $jiraEndpoint -f $jiraAction
			$jiraResults = Invoke-WebRequest -Uri $jiraUrl -Headers $headers -Method GET -ContentType 'application/json' | ConvertFrom-Json			
			foreach ($transition in $jiraResults.transitions)
			{
				if ($transition.name -eq 'Assign to Test')
				{
					$jiraTransitionId = $transition.id
				}
			}
			if ($jiraTransitionId -eq '')
			{
				throw "Assign to Test Transition Does Not Exist"
			}
			
			# locate the custom field that
			# represents the fixed build number
			$jiraNextBuildNumberField = ''
			$jiraAction = 'issue/{0}/editmeta' -f $workItem.Key
			$jiraUrl = $jiraEndpoint -f $jiraAction
			$jiraResults = Invoke-WebRequest -Uri $jiraUrl -Headers $headers -Method GET -ContentType 'application/json' | ConvertFrom-Json
			foreach ($property in $jiraResults.fields.PSObject.Properties)
			{
				$field = $jiraResults.fields.($property.Name)
				if ($field.Name -eq 'Fix Build/s')
				{
					$jiraNextBuildNumberField = $property.Name
				}
			}
			if ($jiraNextBuildNumberField -eq '')
			{
				throw "Fix Build/s Field Does Not Exist"
			}
			
			# perform the update by posting data to the issue
			$jiraAction = 'issue/{0}/transitions' -f $workItem.Key
			$jiraUrl = $jiraEndpoint -f $jiraAction
			$jiraData = '{{ "transition": {{ "id": "{0}" }}, "fields": {{ "{1}": "{2}", "assignee": {{ "name": "" }}}}}}' -f $jiraTransitionId, $jiraNextBuildNumberField, $env:BUILD_NUMBER
			$jiraResults = Invoke-WebRequest -Uri $jiraUrl -Headers $headers -Method POST -ContentType 'application/json' -Body $jiraData | ConvertFrom-Json
		}		
		
		write-host ""		
	}
}

#################################################
# OutputWorkItems
#
# Outputs the work items that are included in this
# build to a html file, to be included as an
# artifact for this build
#
function OutputWorkItems($workItems)
{
	write-host "***** $(get-date) : Outputting Work Items *****"
	write-host ""
	
	$outputName = '{0}#{1}.Tickets.html' -f $env:JOB_NAME, $env:BUILD_NUMBER
	
	add-content $outputName '<html><head><style>table, td { border: 1px solid lightgrey; padding: 5px; border-spacing: 0px; border-collapse: collapse }</style></head><body><table>'	
	
	if ($workItems.Count -gt 0)
	{
		add-content $outputName '<tr><td nowrap><b>Jira #</b></td><td width="50%"><b>Jira Description</b></td><td nowrap><b>Helpdesk #</b></td><td width="50%"><b>Release Notes Description</b></td></tr>'
		
		foreach ($workItem in $workItems.GetEnumerator())
		{
			$workItemValues = $workItem.Value.Split('|')
			$workItemDescription = $workItemValues[0];
			$workItemHelpdeskTicketNumber = $workItemValues[1];
			$workItemReleaseNotesDescription = $workItemValues[2];
			$workItemUrl = $workItemValues[3];
			if ($workItemReleaseNotesDescription -eq 'n/a') { $workItemReleaseNotesDescription = '' }
		
			$outputItem = '<tr><td nowrap><a href="{4}" target="_blank">{0}</a></td><td>{1}</td><td nowrap>{2}</td><td>{3}</td></tr>' -f $workItem.Key, $workItemDescription, $workItemHelpdeskTicketNumber, $workItemReleaseNotesDescription, $workItemUrl
			write-host $workItem.Key
			add-content $outputName $outputItem
		}
	}
	else
	{
		add-content $outputName '<tr><td><b>No Work Items Awaiting Next Build</b></td></tr>'
		write-host "No Work Items Awaiting Next Build"
	}
	
	add-content $outputName '</table></body></html>'
	write-host ""
}

#################################################
# BUILD PROCESS
#
# This is the main build process
#
try
{
	ListEnvironmentVariables
	$workItems = GetWorkItems
	UpdateAssemblyVersions
	ReplaceLicenseFiles
	BuildSolutions
	ObfuscateAssemblies
	UpdateWorkItems $workItems
	OutputWorkItems $workItems
}
catch [system.exception]
{
	write-host ""
	write-host $_.exception.message;
	exit -1
}