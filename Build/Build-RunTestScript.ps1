#################################################
# BUILD PROCESS
#
# This is the main build process
#

try
{
    #$ENV:PARENT_NAME= "CCMv8.4.0 With Broken Unit Tests"
    #$ENV:TEST_CATEGORY = "Smoke Dal"

    write-host "***** $(get-date) : Running Tests For '$ENV:PARENT_NAME' *****"

    if(!(Test-Path Z:))
    {
        $workSpacePath = "\\gal-build-001\D$\Jenkins\jobs"
        New-PSDrive -Name "Z" -PSProvider "FileSystem" -Root $workSpacePath
    }

    set-location Z:$($ENV:PARENT_NAME)\workspace
       
    $nunitExecutionPath = "C:\Program Files (x86)\NUnit 2.6.4\bin`\nunit-console.exe"  
    #$argumentList = @("Solution\Ccm.nunit", "/domain:multiple", "--result=`"Tests\Galleria.Ccm_$($ENV:TEST_CATEGORY).tmp`"", "/noshadow", "/include:`"$($ENV:TEST_CATEGORY)`"")
    &$nunitExecutionPath /domain:multiple --result="Tests\Galleria.Ccm_$($ENV:TEST_CATEGORY).tmp" Solution\Ccm.nunit /noshadow /include:$($ENV:TEST_CATEGORY) 

    # Clean out invalid XML characters from the XML files
    Get-Content "Tests\Galleria.Ccm_$($ENV:TEST_CATEGORY).tmp" | Foreach-object {
        $_ -replace "(?<![\uD800-\uDBFF])[\uDC00-\uDFFF]|[\uD800-\uDBFF](?![\uDC00-\uDFFF])|[\x00-\x08\x0B\x0C\x0E-\x1F\x7F-\x9F\uFEFF\uFFFE\uFFFF]", ""
    } | Set-Content "Tests\Galleria.Ccm_$($ENV:TEST_CATEGORY).xml"

    # Remove the tmp file
    remove-item "Tests\Galleria.Ccm_$($ENV:TEST_CATEGORY).tmp"

    exit 0
}
catch [system.exception]
{
	write-host ""
	write-host $_.exception.message;
	exit -1
}

